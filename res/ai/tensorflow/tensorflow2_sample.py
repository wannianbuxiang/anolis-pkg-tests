import tensorflow as tf

# 数据
x_train = [1, 2, 3, 4]
y_train = [0, -1, -2, -3]

# 模型
model = tf.keras.Sequential([
    tf.keras.layers.Dense(units=1, input_shape=[1])
])

# 编译模型
model.compile(optimizer='sgd', loss='mean_squared_error')

# 训练模型
model.fit(x_train, y_train, epochs=1000)

# 输出模型参数
print(model.get_weights())