import tensorflow as tf

@tf.function
def fib(n):
    a, b = tf.constant(1, dtype=tf.int32), tf.constant(1, dtype=tf.int32)
    i = tf.constant(2, dtype=tf.int32)
    while i < n:
        a, b = b, a + b
        i += 1
    return b

for i in tf.range(1, 11):
    print("fib(%d) = %d" % (i, fib(tf.constant(i, dtype=tf.int32))))
