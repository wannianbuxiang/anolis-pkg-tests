import torch

# 设置 PyTorch 随机数种子
torch.manual_seed(1234)

# 创建一个 5x3 的张量，包含随机数
x = torch.randn(5, 3)
print("x =")
print(x)

# 创建一个 3x4 的张量，包含随机数
y = torch.randn(3, 4)
print("y =")
print(y)

# 计算矩阵乘法 x@y
z = x @ y
print("z =")
print(z)
