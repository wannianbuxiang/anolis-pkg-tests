import os
import time

from avocado.core.plugin_interfaces import JobPre, JobPost
from avocado.core.output import LOG_UI
from avocado.utils import process
from os_env import OSType, Env, Image

class AnolisPrePost(JobPre, JobPost):
    """
    pre/post set before/after Job
    """
    name = 'Anolis test PrePost'
    description = 'setup and teardown Anolis test envoriment'
    def __init__(self):
        self.test_engine = os.environ.get("TEST_ENGINE")
        self.image_info = self._get_image_info()
        self.registry = os.environ.get("REGISTRY_ADDR")
        self.container_id = None

    def _get_image_info(self):
        image = Image()
        _, output = self.cmd('find /etc -name "image-id"')
        if output:
            image.env = Env.ECS
            _, output = self.cmd('cat /etc/image-id | grep image_id')
            image.id = output.split('=')[1].strip('"')
        else:
            image.env = Env.PHY
        _,output = self.cmd('cat /etc/os-release')
        for line in output.split('\n'):
            if line.startswith('ID='):
                image.ostype = OSType[line.split('=')[1].strip('"').upper()]
            elif line.startswith('VERSION_ID='):
                image.version = line.split('=')[1].strip('"')
        _, image.arch = self.cmd('arch')
        _, image.kernel = self.cmd('uname -r')
        return image
    
    def create_container(self,container_engine,image,version,container_id):
        http_str=""
        if os.environ.get('TONE_OSS_PROXY_HOST') is not None:
            http_str = "-e http_proxy={0} -e https_proxy={0}".format(os.environ.get('TONE_OSS_PROXY_HOST'))
        if container_engine == 'docker':
            ret_c,ret_o = self.cmd(" rpm -qa  | grep podman ",ignore_status=True)
            if ret_c == 0:
                self.cmd('yum remove podman -y')
            if image.ostype == OSType.ANOLIS:
                self.cmd('yum install -y yum-utils')
                if image.ostype == OSType.ANOLIS and image.version.startswith('23'):
                    self.cmd('yum install docker -y --enablerepo=os')
                else:
                    self.cmd('yum install docker -y --enablerepo=Plus')
            elif image.ostype == OSType.CENTOS or image.ostype == OSType.ALINUX or image.ostype == OSType.NINGOS:
                self.cmd('yum install -y yum-utils')
                self.cmd('yum-config-manager --add-repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo')
                self.cmd('yum install docker-ce -y')
            elif image.ostype == OSType.FEDORA:
                self.cmd('dnf -y install dnf-plugins-core')
                self.cmd('dnf config-manager --add-repo https://mirrors.aliyun.com/docker-ce/linux/fedora/docker-ce.repo')
                self.cmd('dnf update -y')
                self.cmd('dnf install docker-ce -y')
            elif image.ostype == OSType.OPENSUSE_LEAP:
                self.cmd('zypper install -y docker')
            elif image.ostype == OSType.UBUNTU:
                if image.version == '18.04':
                    self.cmd('apt-get update -y')
                    self.cmd('apt-get -y install software-properties-common 1>/dev/null',ignore_status=True)
                self.cmd('curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -')
                self.cmd('add-apt-repository "deb [arch=$(dpkg --print-architecture)] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"')
                self.cmd('apt-get update -y')
                if image.arch == 'aarch64':
                    self.cmd('rm -rf /etc/grub.d/10_linux*')
                    self.cmd('rm -rf /var/lib/dpkg/updates/*')
                self.cmd('DEBIAN_FRONTEND=noninteractive apt-get -y install docker-ce --no-install-recommends 1>/dev/null',ignore_status=True)
                ret_c,output = self.cmd('service docker start',ignore_status=True)
                if 'docker.service is masked' in output:
                    self.cmd('systemctl unmask docker.service && systemctl unmask docker.socket')
                    self.cmd('systemctl start containerd.service')
            elif image.ostype == OSType.DEBIAN:
                self.cmd('apt-get update -y')
                self.cmd('apt-get install apt-transport-https ca-certificates curl gnupg software-properties-common -y 1>/dev/null')
                self.cmd('curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -')
                self.cmd('add-apt-repository "deb [arch=$(dpkg --print-architecture)] https://download.docker.com/linux/debian $(lsb_release -cs) stable"')
                self.cmd('apt-get update -y')
                self.cmd('apt-get -y install docker-ce 1>/dev/null',ignore_status=True)
                self.cmd('cat /etc/containers/registries.conf | grep docker.io',ignore_status=True)
            else:
                self.log.debug("docker install: this operating system does not require testing")
            
            ret_c,containerd_status = self.cmd('systemctl is-active containerd.service',ignore_status=True)
            if containerd_status != 'active':
                self.cmd('systemctl start containerd.service')
            for i in range(30):
                ret_c,docker_status = self.cmd('systemctl is-active '+container_engine,ignore_status=True)
                if docker_status != 'active':
                    time.sleep(20)
                    self.cmd('systemctl reset-failed '+container_engine,ignore_status=True)
                    self.cmd('systemctl start '+container_engine,ignore_status=True)
                else:
                    break
        elif container_engine == 'podman':
            ret_c,ret_o = self.cmd("rpm -qa | grep docker",ignore_status=True)
            if ret_c == 0:
                self.cmd('yum remove docker* -y')
            self.cmd('yum install -y yum-utils')
            self.cmd('yum install podman -y')
            ret_c,ret_o = self.cmd("podman ps -q")
            if ret_o != "":
                self.cmd(f" podman stop {ret_o} && podman rm $(podman ps -a -q) ") 
        
        if 'tar' in version:
            self.cmd('wget '+version+' 1>/dev/null 2>&1', ignore_status=True)
            self.cmd(container_engine+' import '+version.split('/')[-1]+' anolis:23')
            ret_c,image_exist = self.cmd(container_engine+' image ls')
            if not 'anolis' in image_exist:
                LOG_UI.error("import image failed")
                raise SystemExit(1)
            ret_c,container_id = self.cmd(container_engine+' run -dit --privileged=true -v /home:/home anolis:23 bash')
        else:
            http_proxy = os.environ.get('TONE_OSS_PROXY_HOST')
            if http_proxy is not None and container_engine == "docker":
                file_path = '/etc/systemd/system/docker.service.d/http-proxy.conf'
                os.makedirs(os.path.dirname(file_path), exist_ok=True)
                with open(file_path, 'w') as f:
                    f.write('[Service]\n')
                    f.write(f'Environment="HTTP_PROXY={http_proxy}"\n')
                    f.write(f'Environment="HTTPS_PROXY={http_proxy}"\n')
                    f.write('Environment="NO_PROXY=localhost,127.0.0.1"\n')
                self.cmd("systemctl daemon-reload")
                self.cmd("systemctl restart docker")
            self.cmd(container_engine+' pull '+version)
            ret_c,output1 = self.cmd(container_engine+' image ls')
            print("output1:%s" % output1)
            if ':' in version:
                self.image_name = version.split(':')[0]
            else:
                self.image_name = version
            if not self.image_name in output1:
                LOG_UI.error("pull image failed")
                raise SystemExit(1)
            docker_gpu= ""
            ret_gpu, _ = self.cmd('lspci | grep -i nvidia',ignore_status=True)
            if ret_gpu == 0:
                docker_gpu = " --gpus all "
            if 'anolis' or 'alinux' not in self.image_name.split('/')[-1]:
                ret_c1,container_id = self.cmd(container_engine+' run '+http_str+ ' -dit --privileged=true -v /home:/home ' + docker_gpu +version,ignore_status=True)
                command = container_engine +' exec '+container_id+' '+' echo "$LANG" '
                ret_c2,lang_ret = self.cmd(command,ignore_status=True)
                if ret_c1 != 0 or ret_c2 != 0:
                    ret_c,container_id = self.cmd(container_engine+' run '+http_str+ ' -dit --privileged=true ' + docker_gpu +version+' bash')
            else:
                ret_c,container_id = self.cmd(container_engine+' run '+http_str+ ' -dit --privileged=true -v /home:/home ' + docker_gpu +version+' /usr/sbin/init')
        
        ret_c,container_status = self.cmd(container_engine+' ps -a | grep Up -wo')
        if not 'Up' in container_status:
            LOG_UI.error("run image failed")
            raise SystemExit(1)
        return container_id
        
    def destroy_container(self,container_engine,container_id,version,image):
        if container_id:
            self.cmd(container_engine+' stop '+container_id+' && '+container_engine+' container prune -f')
            if 'tar' in version:
                self.cmd(container_engine+' image rm anolis:23')
            else:
                self.cmd(container_engine+' rmi '+version)
        self.cmd(container_engine+' container prune -f')
        if image.ostype == OSType.ANOLIS:
            self.cmd('yum remove -y '+container_engine)
        elif image.ostype == OSType.CENTOS or image.ostype == OSType.ALINUX or image.ostype == OSType.NINGOS:
            if 'docker' == container_engine:
                self.cmd('yum remove -y docker-ce')
            elif 'podman' == container_engine:
                self.cmd('yum remove -y '+container_engine)
        elif image.ostype == OSType.FEDORA:
            if 'docker' == container_engine:
                self.cmd('dnf remove -y docker-ce')
            elif 'podman' == container_engine:
                self.cmd('dnf remove -y '+container_engine)
        elif image.ostype == OSType.OPENSUSE_LEAP:
            self.cmd('zypper remove -y '+container_engine)
        elif image.ostype == OSType.UBUNTU or image.ostype == OSType.DEBIAN:
            if 'docker' == container_engine:
                self.cmd('apt-get -y remove docker-ce')
            elif 'podman' == container_engine:
                self.cmd('apt-get -y remove '+container_engine)

    def cmd(self, command, ignore_status=False):
        if ignore_status:
            result = process.run(command, ignore_status=ignore_status, shell=True)

            if result.exit_status == 0:
                return result.exit_status, result.stdout_text.strip()
            else:
                return result.exit_status, result.stderr_text.strip()
        else:
            try:
                result = process.run(command, ignore_status=ignore_status, shell=True)
                if result.exit_status == 0:
                    return result.exit_status, result.stdout_text.strip()
                else:
                    raise ValueError(result.stderr_text.strip())
            except Exception as e:
                LOG_UI.error(str(e))
                raise SystemExit(1)
        
    def pre(self, job):
        LOG_UI.info("-------do the pre action.-----------")
        if self.test_engine is not None:
            self.container_id = self.create_container(self.test_engine, self.image_info, self.registry, self.container_id)
            os.environ['TEST_CONTAINER_ID'] = self.container_id
        
    def post(self, job):
        LOG_UI.info("-------do the post action.-----------")
        if self.container_id is not None:
            self.destroy_container(self.test_engine, self.container_id, self.registry, self.image_info)

