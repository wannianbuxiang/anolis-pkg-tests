from enum import Enum
class OSType(Enum):
    ALINUX = 'alinux'
    ANOLIS = 'anolis'
    ALIOS = 'alios'
    CENTOS = 'centos'
    FEDORA = 'fedora'
    UBUNTU = 'ubuntu'
    OPENSUSE_LEAP = 'opensuse_leap'
    DEBIAN = 'debian'
    BCLINUX = 'bclinux'
    KOS = 'kos'
    UOS = 'uos'
    CQOS = 'cqos'
    NINGOS = 'ningos'

class Env(Enum):
    ECS = 'ecs'
    PHY = 'phy'


class Image(object):

    def __init__(self):
        self.env = None
        self.id = None
        self.arch = None
        self.kernel = None
        self.ostype = None
        self.version = None
