from setuptools import setup


setup(
    name='anolis_test_plugin',
    version='1.1',
    description='Avocado Plugin for Anolis tests',
    py_modules=['anolis_test_plugin'],
    entry_points={
        'avocado.plugins.job.prepost': ['anolis_test_plugin = anolis_test_plugin:AnolisPrePost'],
    }
)
