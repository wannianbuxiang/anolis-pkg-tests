#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_virt-what_fun_001.py
@Time:      2024/04/26 14:11:50
@Author:    zhixin
@Version:   1.0
@Contact:   zx01468051@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhixin
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_virt-what_fun_001.yaml for details

    :avocado: tags=fix,P3,noarch,local
    """
    PARAM_DIC = {"pkg_name": "virt-what"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        ret_c, ret_o1 = self.cmd('virt-what')
        ret_c, ret_o2 = self.cmd('virt-what --')
        self.assertTrue(ret_o1 == ret_o2, 'check output error1.')
        ret_c, ret_o = self.cmd('virt-what --help')
        self.assertTrue('Options:' in ret_o, 'check output error3.')
        ret_c, ver = self.cmd('virt-what --version')
        ret_c, ver_rpm = self.cmd("rpm -qa |grep ^virt-what |awk -F- '{print $3}'")
        self.assertTrue(ver == ver_rpm, 'check output error4.')
        ret_c, ret_o = self.cmd('virt-what -v')
        self.assertTrue(ret_o == ver_rpm, 'check output error5.')
        self.cmd('virt-what -h > test.log 2>&1', ignore_status=True)
        ret_c, ret_o = self.cmd('cat test.log')
        self.assertTrue("virt-what: unrecognized option '-h'" in ret_o, 'check output error6.')

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf test.log')
