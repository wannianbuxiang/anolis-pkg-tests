#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_virt-what_fun_002.py
@Time:      2024/04/26 11:11:50
@Author:    zhixin
@Version:   1.0
@Contact:   zx01468051@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhixin
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_virt-what_fun_002.yaml for details

    :avocado: tags=fix,P3,noarch,local
    """
    PARAM_DIC = {"pkg_name": "virt-what"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("echo '#!/bin/bash\nuseradd testuser1' > test.sh")
        self.cmd('sh test.sh')
    
    def test(self):
        # run with regular users
        ret_c, ret_o = self.cmd('sudo -u testuser1 virt-what > result.log 2>&1', ignore_status=True)
        self.assertTrue(ret_c is not 0, 'return code should not be 0.')
        ret_c, ret_o = self.cmd('cat result.log')
        self.assertTrue('virt-what: this script must be run as root' in ret_o, 'check output error1.')
        # run with root
        self.cmd('sudo -u root virt-what')
        self.cmd('virt-what')

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("echo '#!/bin/bash\nuserdel testuser1' > test.sh")
        self.cmd('sh test.sh')
        self.cmd('rm -rf test.sh result.log')
