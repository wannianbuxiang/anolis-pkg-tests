#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_virt-what_fun_003.py
@Time:      2024/04/28 11:11:50
@Author:    zhixin
@Version:   1.0
@Contact:   zx01468051@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhixin
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_virt-what_fun_003.yaml for details

    :avocado: tags=fix,P3,noarch,local
    """
    PARAM_DIC = {"pkg_name": "virt-what"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd('dmidecode > dmidecode.log')
    
    def test(self):
        ret_c, ret_virt = self.cmd('virt-what')
        ret_c, ret_o = self.cmd("grep 'Manufacturer: Alibaba' dmidecode.log", ignore_status=True)
        if ret_c is 0:
            self.assertTrue('alibaba_cloud' in ret_virt, 'check output error1.')
        ret_c, ret_o = self.cmd("grep 'Manufacturer: VMware' dmidecode.log", ignore_status=True)
        if ret_c is 0:
            self.assertTrue('vmware' in ret_virt, 'check output error2.')
        ret_c, ret_o = self.cmd("grep 'Manufacturer: innotek GmbH' dmidecode.log", ignore_status=True)
        if ret_c is 0:
            self.assertTrue('virtualbox' in ret_virt, 'check output error3.')
        ret_c, ret_o = self.cmd("grep 'Vendor: BHYVE' dmidecode.log", ignore_status=True)
        if ret_c is 0:
            self.assertTrue('bhyve' in ret_virt, 'check output error4.')
        ret_c, ret_o = self.cmd("grep 'UML' /proc/cpuinfo", ignore_status=True)
        if ret_c is 0:
            self.assertTrue('uml' in ret_virt, 'check output error5.')
        ret_c, ret_o = self.cmd("grep 'Product Name: Google Compute Engine' dmidecode.log", ignore_status=True)
        if ret_c is 0:
            self.assertTrue('google_cloud' in ret_virt, 'check output error6.')
        ret_c, ret_o = self.cmd("grep 'Manufacturer: Red Hat' dmidecode.log", ignore_status=True)
        if ret_c is 0:
            self.assertTrue('redhat' in ret_virt, 'check output error7.')

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf dmidecode.log')
