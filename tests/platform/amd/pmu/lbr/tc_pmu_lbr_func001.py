#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

"""
@File:      tc_pmu_lbr_func001.py
@Time:      2024/07/23 15:11:08
@Author:    Kun(llfl)
@Version:   1.0
@Contact:   llfl@linux.alibaba.com
@License:   Mulan PSL v2
@Modify:    Kun(llfl)
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    :avocado: tags=P1,x86_64,local,amd,
    """
    def test(self):
        ret_c,ret_o = self.cmd('perf record -b -o perf.data -- sleep 5')
        self.assertEqual(ret_c, 0, "Failed: %s" % ret_o)

    def tearDown(self):
        super().tearDown()