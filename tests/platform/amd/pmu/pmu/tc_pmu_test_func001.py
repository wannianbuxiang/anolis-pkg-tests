#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

"""
@File:      tc_pmu_test_func001.py
@Time:      2024/07/23 15:01:11
@Author:    Kun(llfl)
@Version:   1.0
@Contact:   llfl@linux.alibaba.com
@License:   Mulan PSL v2
@Modify:    Kun(llfl)
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    :avocado: tags=P1,x86_64,local,amd,
    """
    def test(self):
        self.skip_non_root_test()
        _, ret_o = self.cmd('perf test -s 17,68,76,81,82', ignore_status=True)
        print(ret_o)
        for line in ret_o.splitlines():
            if "FAILED" in line:
                self.fail(line)

    def tearDown(self):
        super().tearDown()