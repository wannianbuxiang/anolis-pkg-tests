# -*- encoding: utf-8 -*-

"""
@File:      tc_sev_es_tests_fun001.py
@Time:      2024/05/14 10:33:38
@Author:    Kun(llfl)
@Version:   1.0
@Contact:   llfl@linux.alibaba.com
@License:   Mulan PSL v2
@Modify:    Kun(llfl)
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    :avocado: tags=P1,x86_64,local,amd,
    """
    def test(self):
        self.skip_non_root_test()
        suggestion = "grubby --update-kernel=ALL --args=\"mem_encrypt=on kvm_amd.sev=1 kvm_amd.sev_es=1\""
        ret_c, _ = self.cmd("cat /proc/cmdline | grep -q 'kvm_amd.sev_es=1'", ignore_status=True)
        if ret_c != 0:
            self.skip("kernel cmdline \"kvm_amd.sev_es=1\" not found, maybe you can enable it by running '{}'".format(suggestion))
        ret_c, _ = self.cmd("cat /proc/cmdline | grep -q 'kvm_amd.sev=1'", ignore_status=True)
        if ret_c != 0:
            self.skip("kernel cmdline \"kvm_amd.sev=1\" not found, maybe you can enable it by running '{}'".format(suggestion))
        ret_c, _ = self.cmd("cat /proc/cmdline | grep -q 'mem_encrypt=on'", ignore_status=True)
        if ret_c != 0:
            self.skip("kernel cmdline \"mem_encrypt=on\" not found, maybe you can enable it by running '{}'".format(suggestion))

        ret_c, _ = self.cmd("dmesg | grep -i 'sev supported'", ignore_status=True)
        self.assertEqual(ret_c, 0, "Failed: SEV not enabled")

        ret_c, _ = self.cmd("dmesg | grep -i 'sev-es supported'", ignore_status=True)
        self.assertEqual(ret_c, 0, "Failed: SEV-ES not enabled")

        ret_c, _ = self.cmd("dmesg | grep -i sme", ignore_status=True)
        self.assertEqual(ret_c, 0, "Failed: SME not enabled")

        ret_c, ret_o = self.cmd("yum install -y edk2-ovmf qemu-kvm")
        self.assertEqual(ret_c, 0, "Failed: %s" % ret_o)

    def tearDown(self):
        super().tearDown()