#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

"""
@File:      tc_iommu_iopgtb_func001.py
@Time:      2024/07/23 16:00:19
@Author:    Kun(llfl)
@Version:   1.0
@Contact:   llfl@linux.alibaba.com
@License:   Mulan PSL v2
@Modify:    Kun(llfl)
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    :avocado: tags=P1,x86_64,local,amd,
    """
    def test(self):
        suggestion = "grubby --update-kernel=ALL --args=\"iommu=nopt amd_iommu_dump=1\""
        ret_c, _ = self.cmd("cat /proc/cmdline | grep -q 'iommu=nopt'", ignore_status=True)
        if ret_c != 0:
            self.skip("kernel cmdline \"iommu=nopt\" not found, maybe you can enable it by running '{}'".format(suggestion))
        ret_c,ret_o = self.cmd("cat /proc/kallsyms | grep -q 'io_pgtable_amd_iommu_v1_init_fns'")
        self.assertEqual(ret_c, 0, "Failed: %s" % ret_o)

    def tearDown(self):
        super().tearDown()