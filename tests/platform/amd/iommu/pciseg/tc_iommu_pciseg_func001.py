#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

"""
@File:      tc_iommu_pciseg_func001.py
@Time:      2024/07/23 16:29:34
@Author:    Kun(llfl)
@Version:   1.0
@Contact:   llfl@linux.alibaba.com
@License:   Mulan PSL v2
@Modify:    Kun(llfl)
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    :avocado: tags=P1,x86_64,local,amd,
    """
    def test(self):
        suggestion = "grubby --update-kernel=ALL --args=\"iommu=nopt amd_iommu_dump=1\""
        ret_c, _ = self.cmd("cat /proc/cmdline | grep -q 'amd_iommu_dump'", ignore_status=True)
        if ret_c != 0:
            self.skip("kernel cmdline \"amd_iommu_dump=1\" not found, maybe you can enable it by running '{}'".format(suggestion))
        ret_c,ret_o = self.cmd("dmesg | grep -q 'AMD-Vi: PCI segment'")
        self.assertEqual(ret_c, 0, "Failed: %s" % ret_o)

    def tearDown(self):
        super().tearDown()