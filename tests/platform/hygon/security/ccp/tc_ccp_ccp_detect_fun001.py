# -*- encoding: utf-8 -*-

"""
@File:      tc_ccp_ccp_detect_fun001.py
@Time:      2024/05/08 15:47:20
@Author:    Xingrui Yi
@Version:   1.0
@Contact:   yixingrui@linux.alibaba.com
@License:   Mulan PSL v2
@Modify:    Xingrui Yi
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    :avocado: tags=P1,x86_64,local,hygon,hygon_gen2,hygon_gen3,hygon_gen4,ccp
    """
    def test(self):
        result = self.cmd('lsmod | grep -i ccp')
        self.assertTrue(result is not None, 'result should not be None')

    def tearDown(self):
        super().tearDown()
