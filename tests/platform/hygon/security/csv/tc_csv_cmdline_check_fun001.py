# -*- encoding: utf-8 -*-

"""
@File:      tc_csv_csv_detect_fun001.py
@Time:      2024/05/08 15:47:20
@Author:    Xingrui Yi
@Version:   1.0
@Contact:   yixingrui@linux.alibaba.com
@License:   Mulan PSL v2
@Modify:    Xingrui Yi
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    :avocado: tags=P1,x86_64,local,hygon,hygon_gen2,hygon_gen3,hygon_gen4,csv
    """
    def test(self):
        self.skip_non_root_test()
        suggestion = "grubby --update-kernel=ALL --args=\"kvm_amd.sev=1 kvm_amd.sev_es=1 csv_mem_percentage=50\""
        ret_c, _ = self.cmd("cat /proc/cmdline | grep -q 'kvm_amd.sev_es=1'", ignore_status=True)
        if ret_c != 0:
            self.skip("kernel cmdline \"kvm_amd.sev_es=1\" not found, maybe you can enable it by running '{}'".format(suggestion))
        ret_c, _ = self.cmd("cat /proc/cmdline | grep -q 'kvm_amd.sev=1'", ignore_status=True)
        if ret_c != 0:
            self.skip("kernel cmdline \"kvm_amd.sev=1\" not found, maybe you can enable it by running '{}'".format(suggestion))
        ret_c, _ = self.cmd("cat /proc/cmdline | grep -q 'csv_mem_percentage'", ignore_status=True)
        if ret_c != 0:
            self.skip("kernel cmdline \"csv_mem_percentage\" not found, maybe you can enable it by running '{}'".format(suggestion))

    def tearDown(self):
        super().tearDown()
