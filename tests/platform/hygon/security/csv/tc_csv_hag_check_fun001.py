# -*- encoding: utf-8 -*-

"""
@File:      tc_csv_hag_check_fun001.py
@Time:      2024/09/23 11:47:20
@Author:    Xingrui Yi
@Version:   1.0
@Contact:   yixingrui@linux.alibaba.com
@License:   Mulan PSL v2
@Modify:    Xingrui Yi
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    :avocado: tags=P1,x86_64,local,hygon,hygon_gen2,hygon_gen3,hygon_gen4,csv
    """
    def setUp(self):
        super().setUp()
        self.cmd('mkdir csv_hag_check')
        self.cmd('cd csv_hag_check;wget https://gitee.com/anolis/hygon-devkit/raw/master/bin/hag')

    def test(self):
        self.skip_non_root_test()
        self.cmd('cd csv_hag_check;chmod +x ./hag')
        ret_c, _ = self.cmd('cd csv_hag_check;./hag csv platform_status | grep -q CSV', ignore_status=True)
        self.assertEqual(ret_c, 0, "Failed: CSV not enabled")

        ret_c, _ = self.cmd('cd csv_hag_check;./hag csv platform_status | grep -q CSV2', ignore_status=True)
        self.assertEqual(ret_c, 0, "Failed: CSV2 not enabled")

        ret_c, _ = self.cmd('cd csv_hag_check;./hag csv platform_status | grep -q CSV3', ignore_status=True)
        self.assertEqual(ret_c, 0, "Failed: CSV3 not enabled")

        ret_c, _ = self.cmd("cd csv_hag_check;./hag csv platform_status | grep 'HGSC imported:   YES'", ignore_status=True)
        self.assertEqual(ret_c, 0, "Failed: Please import hgsc")

    def tearDown(self):
        super().tearDown()
        self.cmd('rm -rf csv_hag_check')
