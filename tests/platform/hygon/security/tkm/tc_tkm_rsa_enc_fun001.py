# -*- encoding: utf-8 -*-

"""
@File:      tc_tkm_rsa_enc_fun001.py
@Time:      2024/09/23 11:47:20
@Author:    Xingrui Yi
@Version:   1.0
@Contact:   yixingrui@linux.alibaba.com
@License:   Mulan PSL v2
@Modify:    Xingrui Yi
"""

import logging
from common.basetest import LocalTest

progress_log = logging.getLogger("progress")

class Test(LocalTest):
    """
    :avocado: tags=P1,x86_64,local,hygon,hygon_gen2,hygon_gen3,hygon_gen4,tkm
    """
    def setUp(self):
        super().setUp()
        self.cmd('mkdir tkm_rsa_enc')
        self.cmd('cd tkm_rsa_enc;wget https://gitee.com/anolis/hygon-devkit/raw/master/bin/hag')

    def test(self):
        self.skip_non_root_test()
        self.cmd('cd tkm_rsa_enc;chmod +x ./hag')
        self.cmd('cd tkm_rsa_enc;echo 87654321 | xxd -r -ps > intl.auth')
        self.cmd('cd tkm_rsa_enc;echo 12345678 | xxd -r -ps > intl2.auth')
        self.cmd('cd tkm_rsa_enc;echo 11223344 | xxd -r -ps > apk.auth')
        self.cmd('cd tkm_rsa_enc;echo 0123456789abcdeffedcba9876543210 | xxd -r -ps > iv.data')
        self.cmd('cd tkm_rsa_enc;echo 12345678 | xxd -r -ps > sys.auth')
        self.cmd('cd tkm_rsa_enc;./hag tkm rand_get -len 32 -out data32.plain')
        self.cmd('cd tkm_rsa_enc;./hag tkm rand_get -len 64 -out data64.plain')

        ret_c, output = self.cmd('cd tkm_rsa_enc;./hag tkm intl_key_gen -type sm4 -idx 1000 -auth intl.auth', ignore_status=True)
        progress_log.info(output)
        if ret_c != 0:
            self.skip("Please import TKM hgsc_certchain")
        _, output = self.cmd('cd tkm_rsa_enc;./hag tkm app_key_gen -type rsa2048 -phandle 0x20003e8 -pauth intl.auth -out rsa2048_enc_key.bin -pad')
        progress_log.info(output)
        _, output = self.cmd('cd tkm_rsa_enc;./hag tkm key_import -phandle 0x20003e8 -pauth intl.auth -key rsa2048_enc_key.bin -auth apk.auth -pad')
        progress_log.info(output)

        _, output = self.cmd('cd tkm_rsa_enc;./hag tkm pubkey_export -handle  0x90000001 -out rsa_pubkey.bin')
        progress_log.info(output)
        _, output = self.cmd('cd tkm_rsa_enc;./hag tkm rsa_enc -in data32.plain -pub rsa_pubkey.bin -out rsa_enc_data.bin')
        progress_log.info(output)
        _, output = self.cmd('cd tkm_rsa_enc;./hag tkm rsa_dec -handle  0x90000001 -auth apk.auth -in rsa_enc_data.bin -out rsa_dec_data.bin')
        progress_log.info(output)
        self.cmd('cd tkm_rsa_enc;cmp -b data32.plain rsa_dec_data.bin && echo $?')

        _, output = self.cmd('cd tkm_rsa_enc;./hag tkm key_destroy -handle 0x90000001 -auth apk.auth')
        progress_log.info(output)
        _, output = self.cmd('cd tkm_rsa_enc;./hag tkm key_destroy -handle 0x20003e8 -auth intl.auth')
        progress_log.info(output)

    def tearDown(self):
        super().tearDown()
        self.cmd('rm -rf tkm_rsa_enc')
