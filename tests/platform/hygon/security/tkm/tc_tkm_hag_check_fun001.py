# -*- encoding: utf-8 -*-

"""
@File:      tc_tkm_hag_check_fun001.py
@Time:      2024/09/23 11:47:20
@Author:    Xingrui Yi
@Version:   1.0
@Contact:   yixingrui@linux.alibaba.com
@License:   Mulan PSL v2
@Modify:    Xingrui Yi
"""

import logging
from common.basetest import LocalTest

progress_log = logging.getLogger("progress")

class Test(LocalTest):
    """
    :avocado: tags=P1,x86_64,local,hygon,hygon_gen2,hygon_gen3,hygon_gen4,tkm
    """
    def setUp(self):
        super().setUp()
        self.cmd('mkdir tkm_hag_check')
        self.cmd('cd tkm_hag_check;wget https://gitee.com/anolis/hygon-devkit/raw/master/bin/hag')

    def test(self):
        self.skip_non_root_test()
        self.cmd('cd tkm_hag_check;chmod +x ./hag')

        ret_c, output = self.cmd("cd tkm_hag_check;./hag tkm help", ignore_status=True)
        self.assertEqual(ret_c, 0, "Failed: TKM not enable")
        progress_log.info(output)

    def tearDown(self):
        super().tearDown()
        self.cmd('rm -rf tkm_hag_check')
