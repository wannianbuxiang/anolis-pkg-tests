# -*- encoding: utf-8 -*-

"""
@File:      tc_tdm_verify_ak_fun001.py
@Time:      2024/09/23 11:47:20
@Author:    Xingrui Yi
@Version:   1.0
@Contact:   yixingrui@linux.alibaba.com
@License:   Mulan PSL v2
@Modify:    Xingrui Yi
"""

import logging
from common.basetest import LocalTest

progress_log = logging.getLogger("progress")

class Test(LocalTest):
    """
    :avocado: tags=P1,x86_64,local,hygon,hygon_gen2,hygon_gen3,hygon_gen4,tdm
    """
    def setUp(self):
        super().setUp()
        self.cmd('mkdir tdm_verify_ak')
        self.cmd('cd tdm_verify_ak;wget https://gitee.com/anolis/hygon-devkit/raw/master/bin/hag')

    def test(self):
        self.skip_non_root_test()
        self.cmd('cd tdm_verify_ak;chmod +x ./hag')

        _, output = self.cmd("cd tdm_verify_ak;./hag tdm get_ak_cert -out ak.cert")
        progress_log.info(output)
        _, output = self.cmd("cd tdm_verify_ak;./hag tdm parse_ak_cert -in ak.cert")
        progress_log.info(output)
        _, output = self.cmd("cd tdm_verify_ak;./hag tdm verify_ak_cert -in ak.cert")
        progress_log.info(output)

    def tearDown(self):
        super().tearDown()
        self.cmd('rm -rf tdm_verify_ak')
