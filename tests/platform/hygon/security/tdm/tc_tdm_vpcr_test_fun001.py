# -*- encoding: utf-8 -*-

"""
@File:      tc_tdm_vpcr_test_fun001.py
@Time:      2024/09/23 11:47:20
@Author:    Xingrui Yi
@Version:   1.0
@Contact:   yixingrui@linux.alibaba.com
@License:   Mulan PSL v2
@Modify:    Xingrui Yi
"""

import logging
from common.basetest import LocalTest

progress_log = logging.getLogger("progress")

class Test(LocalTest):
    """
    :avocado: tags=P1,x86_64,local,hygon,hygon_gen2,hygon_gen3,hygon_gen4,tdm
    """
    def setUp(self):
        super().setUp()
        self.cmd("mkdir tdm_vpcr_test")
        self.cmd('cd tdm_vpcr_test;wget https://gitee.com/anolis/hygon-devkit/raw/master/bin/hag')
        cmdline='''cat > tdm_vpcr_test/tdm_verify.c <<EOF
/*
 * The Hygon TDM function test driver
 *
 * Copyright (C) 2018 Hygon Info Technologies Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/module.h>
#include <linux/slab.h>
#include <linux/spinlock.h>
#include <linux/spinlock_types.h>
#include <linux/err.h>
#include <asm/current.h>
#include <linux/kthread.h>
#include <linux/list.h>
#include <linux/delay.h>
#include <linux/kfifo.h>
#include <linux/psp-sev.h>
#include <crypto/hash.h>
#include "tdm_hygon.h"

#define ERR_TDM_COND(cond, err, lable, fmt, ...) do {\
        if (cond) {                                     \
                ret = err;                              \
                pr_err("TDM Verify Err: %s: "fmt, __func__, ##__VA_ARGS__);\
                goto lable;                             \
        } } while (0)

static int test_scene = 0;
module_param(test_scene, int, 0644);
MODULE_PARM_DESC(test_scene, "test for different scene");

#define REGISTER_TASK_CNT 3
#define TEST_VMEM_SIZE          (RANGE_CNT_MAX*PAGE_SIZE)
char *vbuf[REGISTER_TASK_CNT] = {0};
char *kbuf[REGISTER_TASK_CNT][RANGE_CNT_MAX] = {0};
struct addr_range_info *addr_range_list[REGISTER_TASK_CNT] = {0};
struct measure_data *mdata_list[REGISTER_TASK_CNT] = {0};
struct authcode_2b *authcode_list[REGISTER_TASK_CNT] = {0};
int assign_task_id[REGISTER_TASK_CNT] = {0};

/*Calculates the hash value of the memory segment */
static int calc_task_kbuf_hash(int index, uint8_t *hash)
{
        int ret = 0;
        int i = 0;
        struct crypto_shash *shash = NULL;

        shash = crypto_alloc_shash("sm3", 0, 0);
        if (IS_ERR(shash)) {
                ret = PTR_ERR(shash);
                return ret;
        }

        {
                SHASH_DESC_ON_STACK(sdesc, shash);

                sdesc->tfm = shash;

                ret = crypto_shash_init(sdesc);
                if (ret) {
                        pr_err("crypto_shash_init failed\\n");
                        ret = -1;
                        goto out;
                }

                if (test_scene == 3) {
                        ret = crypto_shash_update(sdesc, vbuf[index], TEST_VMEM_SIZE);
                        if (ret) {
                                pr_err("crypto_shash_update failed\\n");
                                ret = -1;
                                goto out;
                        }
                }
                else {
                        for (i = 0; i < RANGE_CNT_MAX; i++) {
                                ret = crypto_shash_update(sdesc, kbuf[index][i], (i + 1)*64);
                                if (ret) {
                                        pr_err("crypto_shash_update failed\\n");
                                        ret = -1;
                                        goto out;
                                }
                        }
                }

                ret = crypto_shash_final(sdesc, hash);
                if (ret) {
                        pr_err("crypto_shash_final failed\\n");
                        ret = -1;
                        goto out;
                }
        }

out:
        crypto_free_shash(shash);
        return ret;
}

static int regi_callback_handler(uint32_t task_id) {
        pr_info("ALARM!\\n");
        pr_info("Task:%d, corruption detected!\\n", task_id);
        pr_info("Please check if it's intended, or your machine may be on danger!\\n");

        return 0;
}

//Allocate authorized memory to global pointer
static int task_authcode_memory_allocate(uint16_t count)
{
        int i = 0;
        int ret = 0;
        struct authcode_2b *authcode_d;

        ERR_TDM_COND(count > REGISTER_TASK_CNT, -DYN_BEYOND_MAX, end, "authcode count %d allocate failed!", count);

        for (i = 0; i < count; i++) {
                authcode_d = kzalloc(sizeof(struct authcode_2b) + AUTHCODE_MAX, GFP_KERNEL);
                ERR_TDM_COND(!authcode_d, -DYN_ERR_MEM, free_mem, "authcode_2b kzalloc memory failed\\n");

                authcode_d->len = AUTHCODE_MAX;
                authcode_list[i] = authcode_d;
        }

end:
        return ret;

free_mem:
        for (i = 0; i < count; i++) {
                kfree(authcode_list[i]);
        }

        return ret;
}

//Allocate range and measure_data memory to global pointer
static int task_range_data_memory_allocate(struct addr_range_info **range, struct measure_data **data, int index)
{
        int i = 0, j = 0;
        int ret = 0;
        char *buf;
        struct addr_range_info *addr_range;
        struct measure_data *mdata_ptr;

        //allocate RANGE_CNT_MAX block memory,every block memory is padded the value base it's index
        for (i = 0; i < RANGE_CNT_MAX; i++) {
                buf = kzalloc((i + 1)*64, GFP_KERNEL);
                ERR_TDM_COND(!buf, -DYN_ERR_MEM, free_kbuf, "kbuf kzalloc memory failed\\n");

                for(j = 0; j < (i + 1)*64; j++) {
                        buf[j] = i + index;
                }

                kbuf[index][i] = buf;
        }

        addr_range = kzalloc(sizeof(struct addr_range_info) + \
                        RANGE_CNT_MAX*sizeof(struct addr_info), GFP_KERNEL);
        ERR_TDM_COND(!addr_range, -DYN_ERR_MEM, free_kbuf, "addr_range kzalloc memory failed\\n");

        addr_range->count = RANGE_CNT_MAX;
        for (i = 0; i < RANGE_CNT_MAX; i++) {
                addr_range->addr[i].addr_start = __psp_pa(kbuf[index][i]);
                addr_range->addr[i].length = (i + 1)*64;
        }

        mdata_ptr = kzalloc(sizeof(struct measure_data), GFP_KERNEL);
        ERR_TDM_COND(!mdata_ptr, -DYN_ERR_MEM, free_addr_range, "mdata_ptr kzalloc memory failed\\n");

        //Configuration used by default
        mdata_ptr->hash_algo = HASH_ALGO_SM3;
        mdata_ptr->period_ms = 0;
        mdata_ptr->pcr = 10 + index % 7;
        ret = calc_task_kbuf_hash(index, mdata_ptr->expected_measurement);
        ERR_TDM_COND(ret, -DYN_ERR_MEM, free_addr_range, "calc_task_kbuf_hash failed\\n");

        *range = addr_range;
        *data = mdata_ptr;

        return ret;

free_addr_range:
        kfree(addr_range);

free_kbuf:
        for (i = 0; i < RANGE_CNT_MAX; i++) {
                kfree(kbuf[index][i]);
        }

        return ret;
}

//Allocate virtual memory range and measure_data memory to global pointer
static int task_virtual_range_data_memory_allocate(struct addr_range_info **range, struct measure_data **data, int index)
{
        int ret = 0;
        struct addr_range_info *addr_range;
        struct measure_data *mdata_ptr;

        vbuf[index] = vmalloc(TEST_VMEM_SIZE);
        if (!vbuf[index]) {
                pr_err("vmalloc failed\\n");
                return -1;
        }

        addr_range = kzalloc(sizeof(struct addr_range_info) + sizeof(struct addr_info), GFP_KERNEL);
        ERR_TDM_COND(!addr_range, -DYN_ERR_MEM, free_vbuf, "addr_range kzalloc memory failed\\n");

        addr_range->count = 1;
        addr_range->addr[0].addr_start = (uint64_t)vbuf[index];
        addr_range->addr[0].length = TEST_VMEM_SIZE;

        mdata_ptr = kzalloc(sizeof(struct measure_data), GFP_KERNEL);
        ERR_TDM_COND(!mdata_ptr, -DYN_ERR_MEM, free_addr_range, "mdata_ptr kzalloc memory failed\\n");

        //Configuration used by default
        mdata_ptr->hash_algo = HASH_ALGO_SM3;
        mdata_ptr->period_ms = 0;
        mdata_ptr->pcr = 10 + index % 7;
        ret = calc_task_kbuf_hash(index, mdata_ptr->expected_measurement);
        ERR_TDM_COND(ret, -DYN_ERR_MEM, free_addr_range, "calc_task_kbuf_hash failed\\n");

        *range = addr_range;
        *data = mdata_ptr;

        return ret;

free_addr_range:
        kfree(addr_range);

free_vbuf:
        vfree(vbuf[index]);

        return ret;
}

//free task's related memory
static void task_memory_free(void)
{
        int i = 0, j = 0;

        for (i = 0; i < REGISTER_TASK_CNT; i++) {
                kfree(mdata_list[i]);
                vfree(vbuf[i]);
        }

        for (i = 0; i < REGISTER_TASK_CNT; i++) {
                for (j = 0; j < RANGE_CNT_MAX; j++) {
                        kfree(kbuf[i][j]);
                }
        }

        for (i = 0; i < REGISTER_TASK_CNT; i++) {
                kfree(addr_range_list[i]);
        }

        for (i = 0; i < REGISTER_TASK_CNT; i++) {
                kfree(authcode_list[i]);
        }
}

static void release_task_list(void) {
        int i = 0;
        int ret = 0;
        int task_status = 0;
        struct measure_status task_measure_status;

        for (i = 0; i < REGISTER_TASK_CNT; i++) {
                ret = psp_query_measure_status(assign_task_id[i], &task_measure_status);
                ERR_TDM_COND(ret, ret, end, "After querying assign_task_id: %d status query failed\\n", assign_task_id[i]);

                task_status = task_measure_status.status;
                if (task_status != DYN_STOP) {
                        pr_info("Call psp_startstop_measure_task to stop measuring for task: %d", assign_task_id[i]);
                        task_status = psp_startstop_measure_task(assign_task_id[i], authcode_list[i], false);
                        ERR_TDM_COND(task_status < 0, task_status, end, "task_id %d stop failed\\n", assign_task_id[i]);
                        msleep(40);
                }

                pr_info("Call psp_destroy_measure_task to destroy measuring service for task: %d", assign_task_id[i]);
                ret = psp_destroy_measure_task(assign_task_id[i],authcode_list[i]);
                ERR_TDM_COND(ret, ret, end, "assign_task_id[%d] status destroy failed\\n", assign_task_id[i]);
        }
        return;
end:
        pr_err("release_task failed!\\n");
}

static int __init _module_init(void)
{
        int i = 0;
        int ret = 0;
        int task_status = 0;
        struct measure_update_data update_data;
        struct measure_status task_measure_status;
        struct authcode_2b authcode_err;

        update_data.update_flag = MEASURE_UPDATE_ALGO | MEASURE_UPDATE_EXPECTED_MEASUREMENT;
        update_data.algo = HASH_ALGO_SHA1;
        memset(update_data.expected_measurement, 0, 32);

#if !IS_ENABLED(CONFIG_CRYPTO_SM3)
        pr_err("Please check whether CONFIG_CRYPTO_SM3 is configured for this verify!!!\\n");
        return -1;
#endif

        pr_info("\\n------Victim module: has %d blocks of data measured by PSP--------test_scene:%d\\n", REGISTER_TASK_CNT, test_scene);

        for (i = 0; i < REGISTER_TASK_CNT; i++) {
                if (test_scene == 3)
                        ret = task_virtual_range_data_memory_allocate(&addr_range_list[i], &mdata_list[i], i);
                else
                        ret = task_range_data_memory_allocate(&addr_range_list[i], &mdata_list[i], i);
                ERR_TDM_COND(ret != 0, ret, mem_free, "range data memory allocate failed!");
        }

        ret = task_authcode_memory_allocate(REGISTER_TASK_CNT);
        ERR_TDM_COND(ret != 0, ret, mem_free, "authcode memory allocate failed!");

        for (i = 0; i < REGISTER_TASK_CNT; i++) {
                pr_info("Call psp_create_measure_task to request measuring service.");
                if (test_scene == 3)
                        assign_task_id[i] = psp_create_measure_task(addr_range_list[i], mdata_list[i], TASK_CREATE_VADDR | TASK_SUPPORT_VPCR, authcode_list[i]);
                else if (test_scene == 4)
                        assign_task_id[i] = psp_create_measure_task(addr_range_list[i], mdata_list[i], TASK_SUPPORT_VPCR | TASK_EXCEPTION_CRASH, authcode_list[i]);
                else
                        assign_task_id[i] = psp_create_measure_task(addr_range_list[i], mdata_list[i], TASK_SUPPORT_VPCR, authcode_list[i]);
                ERR_TDM_COND(assign_task_id[i] < 0, assign_task_id[i], mem_free, "task_id: 0x%x,create measurement task failed!", assign_task_id[i]);

                pr_info("Call psp_register_measure_exception_handler to register measuring exception function for task: %d", assign_task_id[i]);
                ret = psp_register_measure_exception_handler(assign_task_id[i], authcode_list[i], regi_callback_handler);
                ERR_TDM_COND(ret, ret, release_task, "task_id %d callback function register failed\\n", assign_task_id[i]);

                pr_info("Call psp_startstop_measure_task to start measuring for task: %d", assign_task_id[i]);
                task_status = psp_startstop_measure_task(assign_task_id[i], authcode_list[i], true);
                ERR_TDM_COND(task_status < 0, task_status, release_task, "task_id %d start failed\\n", assign_task_id[i]);

                msleep(40);
                ret = psp_query_measure_status(assign_task_id[i], &task_measure_status);
                ERR_TDM_COND(ret, ret, release_task, "task: %d status query failed\\n", assign_task_id[i]);
        }

        switch (test_scene) {
        //scene0:
        //create->register->start->query
        case 0:
        //scene3:
        //vaddr create->register->start->query
        case 3:
                break;

        //scene1:
        //create->register->start->query->stop->update->start
        case 1:
        //scene4:
        //create->register->start->query->stop->update->start->exception hang
        case 4:
                msleep(100);
                for (i = 0; i < REGISTER_TASK_CNT; i++) {
                        ret = psp_query_measure_status(assign_task_id[i], &task_measure_status);
                        ERR_TDM_COND(ret, ret, release_task, "After querying assign_task_id: %d status query failed\\n", assign_task_id[i]);

                        task_status = task_measure_status.status;
                        if (task_measure_status.status != DYN_STOP) {
                                pr_info("Call psp_startstop_measure_task to stop measuring for task: %d", assign_task_id[i]);
                                task_status = psp_startstop_measure_task(assign_task_id[i], authcode_list[i], false);
                                ERR_TDM_COND(task_status < 0, task_status, release_task, "task_id %d stop failed\\n", assign_task_id[i]);
                                msleep(40);
                        }

                        pr_info("Call psp_update_measure_task to update measuring for task: %d", assign_task_id[i]);
                        ret = psp_update_measure_task(assign_task_id[i], authcode_list[i], &update_data);
                        ERR_TDM_COND(ret, ret, release_task, "assign_task_id: %d update failed\\n", assign_task_id[i]);

                        pr_info("Call psp_startstop_measure_task to start measuring for task: %d", assign_task_id[i]);
                        task_status = psp_startstop_measure_task(assign_task_id[i], authcode_list[i], true);
                        ERR_TDM_COND(task_status < 0, task_status, release_task, "task_id %d start failed\\n", assign_task_id[i]);
                }

                break;

                //scene2:
                //create->register->start->error_auth->da->check da->check recovery->stop
        case 2:
                authcode_err.len = AUTHCODE_MAX;
                for (i = 0; i < REGISTER_TASK_CNT; i++) {
                        pr_info("Call psp_startstop_measure_task in scene2 to stop measuring for task: %d", assign_task_id[i]);
                        task_status = psp_startstop_measure_task(assign_task_id[i], &authcode_err, false);
                        ERR_TDM_COND(task_status != -DYN_AUTH_FAIL, DYN_ERR_API, release_task, "stop task_id: %d  failed\\n", assign_task_id[i]);
                        msleep(1000);
                        task_status = psp_startstop_measure_task(assign_task_id[i], &authcode_err, false);
                        ERR_TDM_COND(task_status != -DYN_AUTH_FAIL, DYN_ERR_API, release_task, "stop task_id: %d  failed\\n", assign_task_id[i]);
                        task_status = psp_startstop_measure_task(assign_task_id[i], authcode_list[i], false);
                        ERR_TDM_COND(task_status != -DYN_DA_PERIOD, DYN_ERR_API, release_task, "stop task_id: %d  failed\\n", assign_task_id[i]);
                        task_status = psp_startstop_measure_task(assign_task_id[i], authcode_list[i], false);
                        ERR_TDM_COND(task_status != -DYN_DA_PERIOD, DYN_ERR_API, release_task, "stop task_id: %d  failed\\n", assign_task_id[i]);
                        msleep(2000);
                        task_status = psp_startstop_measure_task(assign_task_id[i], authcode_list[i], false);
                        ERR_TDM_COND(task_status < 0, DYN_ERR_API, release_task, "stop task_id: %d  failed\\n", assign_task_id[i]);
                }
                break;

        default:
                pr_err("%s test_scene : %d is invalid \\n", __func__, test_scene);
                ret = -1;
                break;
        }

        return ret;

release_task:
        release_task_list();
        pr_info("\\n-----------------------end----------------------------\\n");
        return ret;

mem_free:
        task_memory_free();
        return ret;
}

static void __exit _module_exit(void)
{
        release_task_list();
        task_memory_free();
        pr_info("\\n-----------------------end----------------------------\\n");
}

MODULE_AUTHOR("niuyongwen@hygon.cn");
MODULE_LICENSE("GPL");
MODULE_VERSION("0.7");
MODULE_DESCRIPTION("Functional testing demo for TDM");

module_init(_module_init);
module_exit(_module_exit);
EOF
'''
        self.cmd(cmdline)
        cmdline='''cat > tdm_vpcr_test/tdm_hygon.h <<EOF
/* SPDX-License-Identifier: GPL-2.0 WITH Linux-syscall-note */
/*
 * The Hygon TDM communication driver
 *
 * Copyright (C) 2022 Hygon Info Technologies Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

/* Change log:
 * Version: 0.7 (fw version 1.4)
 *	1.Adjust the TDM driver to accommodate multiple versions of the kernel.
 * Version: 0.6 (fw version 1.4)
 *	1.remove psp_get_fw_info from hygon_tdm_init, add tdm show device support to ioctl for hag.
 * Version: 0.5 (fw version 1.4)
 *	1.add support for hanging machine when task exception with special attribute.
 * Version: 0.4 (fw version 1.3)
 *	1.add vpcr support.
 *	2.add task create by vaddr.
 * Version: 0.3 (fw version 1.2)
 *	1.add remote authentication support.
 */
#ifndef __TDM_HYGON_H__
#define __TDM_HYGON_H__

#include <linux/sched.h>
#include <linux/version.h>

#define MIN_VPCR				10
#define MAX_VPCR				16

/*Macro definition for measurement*/
#define TDM_MAX_TASK_BITMAP			16
#define TDM_MAX_NONCE_SIZE			32

#define RANGE_CNT_MAX				0x80
#define MEASURE_TASK_MAX			100
#define AUTHCODE_MAX				16
#define AUTH_TRY_DELAY				1

#define HASH_ALGO_SM3				0
#define HASH_ALGO_SHA1				1
#define HASH_ALGO_SHA256			2
#define HASH_ALGO_SHA384			3
#define HASH_ALGO_SHA512			4

#define SM3_256_DIGEST_SIZE			32
#define SHA1_DIGEST_SIZE			20
#define SHA256_DIGEST_SIZE			32
#define SHA384_DIGEST_SIZE			48
#define SHA512_DIGEST_SIZE			64

#define CONTEXT_CHECK_PID			0x1
#define CONTEXT_CHECK_COMM			0x2
#define CONTEXT_CHECK_MODNAME			0x4
#define TASK_ATTR_NO_UPDATE			0x10000
#define TASK_SUPPORT_VPCR			0x20000
#define TASK_CREATE_VADDR			0x40000
#define TASK_EXCEPTION_CRASH			0x80000

#define MEASURE_UPDATE_ALGO			0x1
#define MEASURE_UPDATE_EXPECTED_MEASUREMENT	0x2

/*Macro definition for tdm certificate*/
#define TDM_MAX_CHIP_ID_LEN			40
#define TDM_CURVE_SM2_ID			0x3
#define TDM_PUBKEY_LEN				32
#define TDM_MAX_USER_ID_LEN			126
#define TDM_SIG_LEN				32
#define TDM_HEADER_AND_PUBKEY_LEN		284

/*Macro definition for tdm report*/
#define TDM_TASK_ALL				0xffffffff
#define TDM_REPORT_SUMMARY			0
#define TDM_REPORT_DETAIL			1

/* command declaration */
enum C2P_CMD_TYPE {
	TDM_TASK_CREATE = 0x0,
	TDM_TASK_VERIFY_AUTH,
	TDM_TASK_QUERY,
	TDM_TASK_DESTROY,
	TDM_TASK_UPDATE,
	TDM_TASK_STOP,
	TDM_TASK_START,
	TDM_FW_VERSION,
	TDM_EXPORT_CERT,
	TDM_GET_REPORT,
	TDM_VPCR_AUDIT,
	TDM_MAX_CMD
};

/* User interaction command declaration */
enum USER_CMD_TYPE {
	USER_EXPORT_CERT = 0x80,
	USER_GET_REPORT,
	USER_VPCR_AUDIT,
	USER_SHOW_DEVICE,
	USER_MAX_CMD
};

/*Public usage id definition for tdm certificate*/
typedef enum _tdm_key_usage_id {
	TDM_INVALID_USAGE_ID =			0x1000,
	TDM_CEK_USAGE_ID =			0x1004,
	TDM_AK_USAGE_ID =			0x2001,
	TDM_MAX_USAGE_ID
} tdm_key_usage_t;

/*Public status ans type declaration*/
enum TDM_TASK_STATUS {
	DYN_INIT = 0x0,
	DYN_TO_RUN,
	DYN_RUN,
	DYN_TO_STOP,
	DYN_STOP
};

enum TDM_MEASURE_STATUS {
	MER_NORMAL = 0x0,
	MER_ERR
};

enum DYN_ERROR_TYPE {
	DYN_NORMAL = 0x0,
	DYN_NOT_EXIST,
	DYN_AUTH_FAIL,
	DYN_STATUS_NOT_SUIT,
	DYN_BEYOND_MAX,
	DYN_DA_PERIOD,
	DYN_NULL_POINTER,
	DYN_ERR_API,
	DYN_EEXIST,
	DYN_ERR_MEM,
	DYN_ERR_AUTH_LEN,
	DYN_ERR_KEY_ID,
	DYN_NO_ALLOW_UPDATE,
	DYN_ERR_HASH_ALGO,
	DYN_ERR_REPORT_TYPE,
	DYN_ERR_SIZE_SMALL,
	DYN_ERR_ADDR_MAPPING,
	DYN_ERR_PCR_NUM,
	DYN_ERR_ORIG_TPM_PCR,
	DYN_MAX_ERR_TYPE
};

/*Data structure declaration for measurement*/
struct addr_info {
	uint64_t addr_start;
	uint64_t length;
} __attribute__((packed));

struct addr_range_info {
	uint32_t count;
	struct addr_info addr[0];
} __attribute__((packed));

struct measure_data {
	uint32_t hash_algo;
	uint8_t expected_measurement[32];
	uint32_t period_ms;
	uint32_t pcr;
} __attribute__((packed));

struct authcode_2b {
	uint16_t len;
	uint8_t val[0];
} __attribute__((packed));

struct measure_status {
	uint8_t status;
	uint8_t error;
	uint64_t count;
} __attribute__((packed));

struct measure_update_data {
	uint32_t update_flag;
	uint32_t algo;
	uint8_t expected_measurement[32];
} __attribute__((packed));

struct da_status {
	uint64_t err_time;
	uint16_t interval_time;
	uint16_t err_cnt;
} __attribute__((packed));

struct tdm_version {
	uint8_t api_major;
	uint8_t api_minor;
	uint32_t buildId;
	uint32_t task_max;
	uint32_t range_max_per_task;
} __attribute__((packed));

struct task_selection_2b {
	uint16_t len;
	uint8_t bitmap[0];
};

struct data_2b {
	uint16_t len;
	uint8_t val[0];
};

/*Data structure declaration for vpcr*/
struct pcr_select {
	uint16_t hash;
	uint32_t pcr;
} __attribute__((packed));

union tpmu_ha {
	uint8_t sha1[SHA1_DIGEST_SIZE];
	uint8_t sha256[SHA256_DIGEST_SIZE];
	uint8_t sha384[SHA384_DIGEST_SIZE];
	uint8_t sha512[SHA512_DIGEST_SIZE];
	uint8_t sm3_256[SM3_256_DIGEST_SIZE];
};

struct tpm2b_digest {
	uint16_t size;
	uint8_t buffer[sizeof(union tpmu_ha)];
} __attribute__((packed));

struct tdm_task_data {
	uint32_t task_id;
	uint8_t hash[32];
} __attribute__((packed));

struct tdm_pcr_value_2b {
	uint32_t task_nums;
	struct tdm_task_data task_data[0];
} __attribute__((packed));

/*Data structure declaration for tdm certificate*/
typedef struct _tdm_ecc_pubkey {
	uint32_t curve_id;
	uint8_t pubkey_qx[TDM_PUBKEY_LEN];
	uint8_t pubkey_qy[TDM_PUBKEY_LEN];
	uint16_t user_id_len;
	uint8_t user_id[TDM_MAX_USER_ID_LEN];
} __attribute__((packed)) tdm_ecc_pubkey_t;

typedef struct _tdm_ecc_signature {
	uint8_t sig_r[TDM_SIG_LEN];
	uint8_t sig_s[TDM_SIG_LEN];
} __attribute__((packed)) tdm_ecc_signature_t;

/*
 ************************ Hygon TDM Certificate - ECC256***************************
 *|00h |31:0     |VERSION          |Certificate version. 0.<major>.<minor>.<fix>     |
 *|04h |7:0      |-                |Reserved. Set to zero                            |
 *|06h |7:0      |CHIP_ID_LEN      |                                                 |
 *|08h |319:0    |CHIP_ID          |Unique ID of every chip.                         |
 *|30h |31:0     |KEY_USAGE_ID     |Usage id of the key.                             |
 *|34h |63:0     |-                |Reserved. Set to zero.                           |
 *|3Ch |31:0     |CURVE_ID         |ECC curve id                                     |
 *|40h |255:0    |Qx               |Public key Qx                                    |
 *|60h |255:0    |Qy               |Public key Qy                                    |
 *|80h |7:0      |USER_ID_LEN      |GM user id len                                   |
 *|82h |1007:0   |USER_ID          |GM user id                                       |
 *|100h|223:0    |-                |Reserved. Set to zero.                           |
 *|11Ch|31:0     |SIG1_KEY_USAGE_ID|Key type for sig1.                               |
 *|120h|255:0    |SIG1_R           |Signature R of key1.                             |
 *|140h|255:0    |SIG1_S           |Signature S of key1.                             |
 *|160h|223:0    |-                |Reserved. Set to zero                            |
 *|17Ch|31:0     |SIG2_KEY_USAGE_ID|Key type for sig2.                               |
 *|180h|255:0    |SIG2_R           |Signature R of key2.                             |
 *|1A0h|255:0    |SIG2_S           |Signature S of key2.                             |
 *************************************************************************************
 */
struct tdm_cert {
	uint32_t version;
	uint8_t reserved_0[2];
	uint16_t chip_id_len;
	uint8_t chip_id[TDM_MAX_CHIP_ID_LEN];
	uint32_t key_usage_id;
	uint8_t reserved_1[8];
	tdm_ecc_pubkey_t ecc_pubkey;
	uint8_t reserved_2[28];
	uint32_t sig1_key_usage_id;
	tdm_ecc_signature_t ecc_sig1;
	uint8_t reserved_3[28];
	uint32_t sig2_key_usage_id;
	tdm_ecc_signature_t ecc_sig2;
} __attribute__((packed));

/*Data structure declaration for tdm measurement report*/
/*
 ******************** Hygon TDM Report for Single Task - ECC256***********************
 *|+(00h) |31:0     |TASK_ID                |Measured task ID                          |
 *|+(04h) |31:0     |PERIOD_MS              |Meaured period time for the related task  |
 *|+(08h) |63:0     |MEAURED_COUNT          |Meaured count for the related task        |
 *|+(10h) |31:0     |LAST_MEASURE_ELAPSED_MS|Meaured time for last mesurement.         |
 *|+(14h) |95:0     |-                      |Reserved. Set to zero                     |
 *|+(20h) |255:0    |MEASURED_HASH          |Mesured hash for the related task.        |
 *************************************************************************************
 */
struct tdm_detail_task_status {
	uint32_t task_id;
	uint32_t period_ms;
	uint64_t measured_count;
	uint32_t last_measure_elapsed_ms;
	uint8_t reserved[12];
	uint8_t measured_hash[32];
} __attribute__((packed));

/*
 ************************ Hygon TDM Report - ECC256***************************
 *|00h |31:0     |VERSION            |Certificate version. 0.<major>.<minor>.<fix>     |
 *|04h |31:0     |FW_VERSION         |Firmware verfion,BUILD_ID                        |
 *|08h |7:0      |REPORT_TYPE        |Summary report:0, Detailed report:1              |
 *|09h |39:0     |-                  |Reserved. Set to zero.                           |
 *|0Eh |15:0     |TASK_NUMS          |ALL task numbers.                                |
 *|10h |127:0    |TASK_BITMAP        |ALL task bitmap.                                 |
 *|20h |127:0    |TASK_ERROR_BITMAP  |Bitmap for error tasks                           |
 *|30h |127:0    |TASK_RUNNING_BITMAP|Bitmap for runnint tasks                         |
 *|40h |239:0    |-                  |Reserved. Set to zero.                           |
 *|5Eh |15:0     |USER_DATA_LEN      |User supplied data length.                       |
 *|60h |255:0    |USER_DATA          |User supplied data.                              |
 *|80h |255:0    |AGGREGATE_HASH     |Aggregate hash for tasks                         |
 *************************************************************************************
 */
struct tdm_report {
	uint32_t version;
	uint32_t fw_version;
	uint8_t report_type;
	uint8_t reserved_0[5];
	uint16_t task_nums;
	uint8_t task_bitmap[TDM_MAX_TASK_BITMAP];
	uint8_t task_error_bitmap[TDM_MAX_TASK_BITMAP];
	uint8_t task_running_bitmap[TDM_MAX_TASK_BITMAP];
	uint8_t reserved_1[30];
	uint16_t user_supplied_data_len;
	uint8_t user_supplied_data[TDM_MAX_NONCE_SIZE];
	uint8_t aggregate_hash[32];
	struct tdm_detail_task_status detailed_task_status[0];
} __attribute__((packed));

/*
 ************************ Hygon TDM Report Signature - ECC256*************************
 *|A0h |223:0     |-                |Reserved. Set to zero                            |
 *|BCh |31:0      |SIG_KEY_USAGE_ID |Key type for sig.                                |
 *|C0h |255:0     |SIG_R            |Signature R of key.                              |
 *|E0h |255:0     |SIG_S            |Signature S of key.                              |
 *************************************************************************************
 */
struct tdm_report_sig {
	uint8_t reserved[28];
	uint32_t sig_key_usage_id;
	uint8_t sig_r[TDM_SIG_LEN];
	uint8_t sig_s[TDM_SIG_LEN];
} __attribute__((packed));

/*Data structure declaration for tdm command/response interface*/
/* The following commands use this structure:
 * psp_register_measure_exception_handler
 * psp_destroy_measure_task
 * psp_update_measure_task
 * psp_startstop_measure_task*/
struct tdm_common_cmd {
	uint32_t cmd_type;
	uint32_t task_id;
	uint16_t code_len;
	uint8_t code_val[AUTHCODE_MAX];
	uint8_t context_hash[32];
} __attribute__((packed));

/*TASK_CREATE*/
struct tdm_create_cmd {
	uint32_t cmd_type;
	uint32_t cmd_ctx_flag;
	struct measure_data m_data;
	uint16_t authcode_len;
	uint8_t context_hash[32];
	struct addr_range_info range_info;
} __attribute__((packed));

struct tdm_create_resp {
	uint32_t task_id;
	uint16_t authcode_len;
	uint8_t authcode_val[AUTHCODE_MAX];
} __attribute__((packed));

/*TASK_VERIFY_AUTH*/
struct tdm_register_cmd {
	struct tdm_common_cmd cmd;
} __attribute__((packed));

/*TASK_QUERY*/
struct tdm_query_cmd {
	uint32_t cmd_type;
	uint32_t task_id;
} __attribute__((packed));

struct tdm_query_resp {
	struct measure_status m_status;
} __attribute__((packed));

/*TASK_DESTROY*/
struct tdm_destroy_cmd {
	struct tdm_common_cmd cmd;
} __attribute__((packed));

/*TASK_UPDATE*/
struct tdm_update_cmd {
	struct tdm_common_cmd cmd;
	struct measure_update_data update_data;
} __attribute__((packed));

/*TASK_STOP,TASK_START*/
struct tdm_startstop_cmd {
	struct tdm_common_cmd cmd;
} __attribute__((packed));

struct tdm_startstop_resp {
	struct measure_status m_status;
} __attribute__((packed));

/*TDM_VERSION*/
struct tdm_fw_cmd {
	uint32_t cmd_type;
} __attribute__((packed));

struct tdm_fw_resp {
	struct tdm_version version;
} __attribute__((packed));

/*TDM_EXPORT_CERT*/
struct tdm_export_cert_cmd {
	uint32_t cmd_type;
	uint32_t key_usage_id;
} __attribute__((packed));

struct tdm_export_cert_resp {
	struct tdm_cert cert;
} __attribute__((packed));

/*TDM_GET_REPORT*/
struct tdm_get_report_cmd {
	uint32_t cmd_type;
	uint32_t task_id;
	uint16_t selection_len;
	uint8_t selection_bitmap[TDM_MAX_TASK_BITMAP];
	uint16_t user_data_len;
	uint8_t user_data_val[TDM_MAX_NONCE_SIZE];
	uint8_t report_type;
	uint32_t key_usage_id;
} __attribute__((packed));

/* Resopnse:
 * struct tdm_report measure_report;
 * struct tdm_report_sig measure_report_sig;
 */

struct tdm_user_report_cmd {
	struct tdm_get_report_cmd report_cmd;
	uint32_t needed_length;
} __attribute__((packed));

/*TDM_VPCR_AUDIT*/
struct tdm_get_vpcr_cmd {
	uint32_t cmd_type;
	struct pcr_select pcr;
} __attribute__((packed));

struct tdm_get_vpcr_resp {
	uint32_t pcr;
	struct tpm2b_digest digest;
	struct tdm_pcr_value_2b pcr_values;
} __attribute__((packed));

struct tdm_show_device {
	struct tdm_version version;
} __attribute__((packed));

/*Public api definition for tdm*/
typedef int (*measure_exception_handler_t)(uint32_t task_id);

int psp_get_fw_info(struct tdm_version *version);
int psp_create_measure_task(struct addr_range_info *range, struct measure_data *data, uint32_t flag, struct authcode_2b *code);
int psp_query_measure_status(uint32_t task_id, struct measure_status *status);
int psp_register_measure_exception_handler(uint32_t task_id, struct authcode_2b *code, measure_exception_handler_t handler);
int psp_destroy_measure_task(uint32_t task_id, struct authcode_2b *code);
int psp_update_measure_task(uint32_t task_id, struct authcode_2b *code, struct measure_update_data *data);
int psp_startstop_measure_task(uint32_t task_id, struct authcode_2b *code, bool start);
int tdm_export_cert(uint32_t key_usage_id, struct tdm_cert *cert);
int tdm_get_report(uint32_t task_id, struct task_selection_2b *selection, struct data_2b *user_supplied_data, uint8_t report_type, uint32_t key_usage_id, uint8_t *report_buffer, uint32_t *length);
int tdm_get_vpcr_audit(struct pcr_select pcr, struct tpm2b_digest *digest, struct tdm_pcr_value_2b *pcr_values);

#endif /* __TDM_HYGON_H__*/
EOF
'''
        self.cmd(cmdline)
        cmdline='''cat > tdm_vpcr_test/Makefile <<EOF
PWD := \$(shell pwd)

obj-m := tdm-verify.o
tdm-verify-objs := tdm_verify.o

#if  LOCAL_KERDIR exist,use  LOCAL_KERNEL path
ifeq (\$(LOCAL_KERDIR),)
ifeq ("\$(origin KERNVER)", "command line")
  KBUILD_VERSION = $(KERNVER)
else
  ifeq (\$(filter clean distclean, $(MAKECMDGOALS)),)
    $(error "build log driver needs assign linux kernel !")
  endif
endif
endif

# for ubuntu, module build top tree is the kernel directory in the build source directory
#        e.g.
#             kernel-4.15.4.symlink     -> ./kernel-4.15.4/
# for centos, module build top tree is the kernel directory under ~/rpmbuild/BUILD/kernel-?
#        e.g.
#             kernel-3.10.0-957.symlink -> ~/rpmbuild/BUILD/kernel-3.10.0_957_sev/
#if  LOCAL_KERDIR exist,use  LOCAL_KERNEL path
ifeq (\$(LOCAL_KERDIR),)
KERNELDIR := ../../../../../kernel-\$(KBUILD_VERSION).symlink
else
KERNELDIR=\$(LOCAL_KERDIR)
endif

default:
	\$(MAKE) -C \$(KERNELDIR) M=\$(PWD) modules

clean:
	-rm -rf \$(shell find -L . -name "*.o.cmd" \
		-o -name "*.o" \
		-o -name "*.ko" \
		-o -name "*.o.d" \
		-o -name "*.mod.c" \
		-o -name "*.ko.cmd" \
		-o -name ".tmp_versions")
	-rm -f modules.order Module.symvers

distclean: clean
	-rm -rf \$(shell find -L . -name "*.ko")
EOF
'''
        self.cmd(cmdline)
        self.cmd('cd tdm_vpcr_test;LOCAL_KERDIR=/lib/modules/$(uname -r)/build make')

    def test(self):
        self.skip_non_root_test()
        self.cmd('cd tdm_vpcr_test;chmod +x ./hag')
        self.cmd("yum install tpm2-tools -y")
        self.cmd("cd tdm_vpcr_test;tpm2_pcrread sm3_256")
        self.cmd("cd tdm_vpcr_test;insmod tdm-verify.ko test_scene=1", ignore_status=True)

        _, output = self.cmd("cd tdm_vpcr_test;./hag tdm get_vpcr_audit -audit_file audit.bin -pcr_num 11")
        progress_log.info(output)
        _, output = self.cmd("cd tdm_vpcr_test;./hag tdm parse_vpcr_audit -audit_file audit.bin")
        progress_log.info(output)
        _, output = self.cmd("cd tdm_vpcr_test;./hag tdm replay_vpcr_audit -vpcr_file audit.bin")
        progress_log.info(output)

    def tearDown(self):
        super().tearDown()
        self.cmd('rmmod tdm-verify', ignore_status=True)
        self.cmd('rm -rf tdm_vpcr_test')
