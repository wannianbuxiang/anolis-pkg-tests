#!/bin/bash

quiet=-Q
context_file=/tmp/context
input_file=/tmp/input
sig_file=/tmp/sig
password=123456

onerror() {
    echo "$BASH_COMMAND on line ${BASH_LINENO[0]} failed: $?"
    exit 1
}
trap onerror ERR

cleanup() {
	rm -f ${context_file} ${input_file} ${sig_file}
}
trap cleanup EXIT

echo -n "01234567890123456789012345678901" >${input_file}
tpm2_createprimary ${quiet} -C o -g sm3_256 -G "ecc_sm2:sm2-sm3_256:null" -c ${context_file} -p ${password} -a "sign|noda|sensitivedataorigin|userwithauth|fixedtpm|fixedparent"
tpm2_sign ${quiet} -p ${password} -g sm3_256 -c ${context_file} -o ${sig_file} -s sm2 ${input_file}
tpm2_verifysignature ${quiet} -g sm3_256 -s ${sig_file} -c ${context_file} -m ${input_file}

