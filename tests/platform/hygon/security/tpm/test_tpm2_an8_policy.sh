#!/bin/bash

quiet=-Q
context_file=/tmp/context
context_load_file=/tmp/context_load
pub_file=/tmp/pub
priv_file=/tmp/priv
input_file=/tmp/input
policy_digest_file=/tmp/policy_digest
digest_file=/tmp/digest
session_file=/tmp/session
sig_file=/tmp/sig
name_file=/tmp/name
timeout_file=/tmp/timeout
ticket_file=/tmp/ticket
new_parent_file=/tmp/new_parent
source_parent_file=/tmp/source_parent
new_parent_name_file=/tmp/new_parent_name
duplicable_file=/tmp/duplicable
duplicable_name_file=/tmp/duplicable_name
duplicated_seed_file=/tmp/duplicated_seed
nv_index=0x01500001
password=123456

onerror() {
    echo "$BASH_COMMAND on line ${BASH_LINENO[0]} failed: $?"
    exit 1
}
trap onerror ERR

cleanup() {
	rm -f ${context_file} ${context_load_file} ${pub_file} ${priv_file} ${input_file} ${policy_digest_file} ${digest_file} ${session_file} ${sig_file} ${name_file} ${timeout_file} ${ticket_file} ${new_parent_file} ${source_parent_file} ${new_parent_name_file} ${duplicable_file} ${duplicable_name_file} ${duplicated_seed_file}
	tpm2_nvundefine ${quiet} -C o ${nv_index} 2>/dev/null || true
}
trap cleanup EXIT

cleanup

tpm2_createpolicy ${quiet} --policy-pcr -g sm3_256 -l sm3_256:0,1,8,9 -L ${policy_digest_file}
tpm2_setprimarypolicy ${quiet} -C o -P "" -L ${policy_digest_file} -g sm3_256
tpm2_setprimarypolicy ${quiet} -C o -P pcr:sm3_256:0,1,8,9 -L ${policy_digest_file} -g sm3_256

tpm2_createprimary ${quiet} -C o -g sm3_256 -G eccsm2:null:sm4128cfb -c ${context_file}
tpm2_startauthsession ${quiet} --policy-session -c ${context_file} -g sm3_256 -s sm4128cfb -S ${session_file}
tpm2_policypcr ${quiet} -l sm3_256:0,1,8,9 -S ${session_file} -L ${policy_digest_file}
tpm2_setprimarypolicy ${quiet} -C o -P "" -L ${policy_digest_file} -g sm3_256
tpm2_setprimarypolicy ${quiet} -C o -P session:${session_file} -L ${policy_digest_file} -g sm3_256
tpm2_flushcontext ${session_file}

tpm2_startauthsession ${quiet} --policy-session -g sm3_256 -S ${session_file}
tpm2_policypcr ${quiet} -l sm3_256:0,1,8,9 -S ${session_file} -L ${policy_digest_file}
tpm2_policyor ${quiet} -L ${digest_file} -S ${session_file} -l sm3_256:${policy_digest_file},${policy_digest_file}
tpm2_policypassword ${quiet} -S ${session_file} -L ${policy_digest_file}
tpm2_policycommandcode ${quiet} -S ${session_file} -L ${policy_digest_file} TPM2_CC_PCR_Reset
tpm2_policyrestart ${quiet} -S ${session_file}
tpm2_policytemplate ${quiet} -L ${digest_file} -S ${session_file} --template-hash ${policy_digest_file}
tpm2_policylocality ${quiet} -S ${session_file} -L ${policy_digest_file} 1
tpm2_flushcontext ${session_file}

tpm2_createprimary ${quiet} -C o -g sm3_256 -G "eccsm2:sm2-sm3_256:null" -c ${context_file} -p ${password} -a "sign|noda|sensitivedataorigin|userwithauth|fixedtpm|fixedparent"
tpm2_startauthsession ${quiet} --policy-session -g sm3_256 -S ${session_file}
tpm2_policysigned ${quiet} -S ${session_file} -c ${context_file} --raw-data ${input_file}
tpm2_sign ${quiet} -p ${password} -g sm3_256 -c ${context_file} -o ${sig_file} -s sm2 ${input_file}
tpm2_policysigned ${quiet} -S ${session_file} -g sm3_256 -c ${context_file} -s ${sig_file} -L ${policy_digest_file}
tpm2_flushcontext ${session_file}

echo -n "01234567890123456789012345678901" >${input_file}
tpm2_createprimary ${quiet} -C o -g sm3_256 -G eccsm2:null:sm4128cfb -c ${context_file} -p ${password}
tpm2_create ${quiet} -g sm3_256 -u ${pub_file} -r ${priv_file} -C ${context_file} -i ${input_file} -P ${password}
tpm2_load ${quiet} -C ${context_file} -u ${pub_file} -r ${priv_file} -c ${context_load_file} -P ${password} -n ${name_file}
tpm2_startauthsession ${quiet} --policy-session -g sm3_256 -S ${session_file}
tpm2_policysecret ${quiet} -L ${policy_digest_file} -S ${session_file} -c ${context_load_file} --ticket ${ticket_file} --timeout ${timeout_file} -t -1000000
tpm2_policyticket ${quiet} -L ${policy_digest_file} -S ${session_file} -n ${name_file} --ticket ${ticket_file} --timeout ${timeout_file}
tpm2_policyrestart ${quiet} -S ${session_file}
tpm2_flushcontext ${session_file}

echo -n "01234567890123456789012345678901" >${input_file}
tpm2_startauthsession ${quiet} --policy-session -g sm3_256 -S ${session_file}
tpm2_policynamehash ${quiet} -L ${policy_digest_file} -S ${session_file} -n ${input_file}
tpm2_flushcontext ${session_file}

echo -n "01234567890123456789012345678901" >${input_file}
tpm2_nvdefine ${quiet} -C o -p ${password} ${nv_index} -a "authread|authwrite" -s 34 -g sm3_256
tpm2_startauthsession ${quiet} -S ${session_file} -g sm3_256
tpm2_policypcr ${quiet} -S ${session_file} -l sm3_256:0,1,8,9 -L ${policy_digest_file}
tpm2_flushcontext ${session_file}
echo "0012" | xxd -p -r | cat - ${policy_digest_file} | tpm2_nvwrite ${quiet} -C ${nv_index} -P ${password} ${nv_index} -i- #0012 is sm3_256
tpm2_startauthsession ${quiet} -S ${session_file} --policy-session -g sm3_256
tpm2_policypcr ${quiet} -S ${session_file} -l sm3_256:0,1,8,9 -L ${policy_digest_file}
tpm2_policyauthorizenv ${quiet} -S ${session_file} -C ${nv_index} -P ${password} ${nv_index} -L ${policy_digest_file}
tpm2_createprimary ${quiet} -C o -g sm3_256 -G eccsm2:null:sm4128cfb -c ${context_file} -p ${password}
tpm2_create ${quiet} -g sm3_256 -u ${pub_file} -r ${priv_file} -C ${context_file} -i ${input_file} -P ${password} -L ${policy_digest_file}
tpm2_load ${quiet} -C ${context_file} -u ${pub_file} -r ${priv_file} -c ${context_load_file} -P ${password} -n ${name_file}
tpm2_unseal ${quiet} -c ${context_load_file} -p session:${session_file} >/dev/null
tpm2_evictcontrol ${quiet} -c ${context_load_file} 0x81010003
tpm2_evictcontrol ${quiet} -c 0x81010003 0x81010003
tpm2_flushcontext ${session_file}
tpm2_nvundefine ${quiet} -C o ${nv_index}

policynv_test_case() {
  tpm2_startauthsession ${quiet} -S ${session_file} --policy-session -g sm3_256
  echo $operandB | xxd -r -p | tpm2_policynv ${quiet} -S ${session_file} -i- -P ${password} ${nv_index} $1
  tpm2_flushcontext ${session_file}
}

operandA=0x81
tpm2_nvdefine ${quiet} -C o -p ${password} ${nv_index} -a "authread|authwrite" -s 2 -g sm3_256
echo $operandA | xxd -r -p | tpm2_nvwrite ${quiet} -P ${password} -i- ${nv_index}

# Perform comparison operation "eq"
operandB=0x81
policynv_test_case eq

# Perform comparison operation "neq"
operandB=0x80
policynv_test_case neq

# Perform comparison operation "sgt"
operandB=0x82
policynv_test_case sgt

# Perform comparison operation "ugt"
operandB=0x80
policynv_test_case ugt

# Perform comparison operation "slt"
operandB=0x80
policynv_test_case slt

# Perform comparison operation "ult"
operandB=0x82
policynv_test_case ult

# Perform comparison operation "sge"
operandB=0x82
policynv_test_case sge
operandB=0x81
policynv_test_case sge

# Perform comparison operation "uge"
operandB=0x80
policynv_test_case uge
operandB=0x81
policynv_test_case uge

# Perform comparison operation "sle"
operandB=0x80
policynv_test_case sle
operandB=0x81
policynv_test_case sle

# Perform comparison operation "ule"
operandB=0x82
policynv_test_case ule
operandB=0x81
policynv_test_case ule

# Perform comparison operation "bs"
operandB=0x81
policynv_test_case bs

# Perform comparison operation "bc"
operandB=0x7E
policynv_test_case bc

tpm2_nvundefine ${quiet} -C o ${nv_index}

tpm2_startauthsession ${quiet} -g sm3_256 -S ${session_file} --policy-session
tpm2_policycommandcode ${quiet} -S $session_file TPM2_CC_NV_Write
tpm2_policynvwritten ${quiet} -S ${session_file} -L ${policy_digest_file} c
tpm2_nvdefine ${quiet} -s 1 -a "authread|policywrite" -p ${password} -L ${policy_digest_file} -g sm3_256 ${nv_index}
echo 0xAA | xxd -r -p | tpm2_nvwrite ${quiet} ${nv_index} -i- -P session:${session_file}
echo 0xAA | xxd -r -p | tpm2_nvwrite ${quiet} ${nv_index} -i- -P session:${session_file} 2>/dev/null || true
tpm2_flushcontext ${session_file}
tpm2_nvundefine ${quiet} -C o ${nv_index}

tpm2_createprimary ${quiet} -C n -g sm3_256 -G eccsm2:null:sm4128cfb -c ${new_parent_file}
tpm2_createprimary ${quiet} -C o -g sm3_256 -G eccsm2:null:sm4128cfb -c ${source_parent_file}
tpm2_readpublic ${quiet} -c ${new_parent_file} -n ${new_parent_name_file}
tpm2_startauthsession ${quiet} -S ${session_file} -g sm3_256
tpm2_policyduplicationselect ${quiet} -S ${session_file} -N ${new_parent_name_file} -L ${policy_digest_file}
tpm2_flushcontext ${session_file}
tpm2_create ${quiet} -C ${source_parent_file} -g sm3_256 -G eccsm2:sm2-sm3_256:null -u ${pub_file} -r ${priv_file} -L ${policy_digest_file} -a "sensitivedataorigin|sign" -c ${duplicable_file}
tpm2_readpublic ${quiet} -c ${duplicable_file} -n ${duplicable_name_file}
tpm2_startauthsession ${quiet} -S ${session_file} --policy-session -g sm3_256
tpm2_policyduplicationselect ${quiet} -S ${session_file} -N ${new_parent_name_file} -n ${duplicable_name_file}
tpm2_duplicate ${quiet} -C ${new_parent_file} -c ${duplicable_file} -G null -p session:${session_file} -r ${priv_file} -s ${duplicated_seed_file}
tpm2_flushcontext ${session_file}

