#!/bin/bash

quiet=-Q
context_file=/tmp/context
input_file=/tmp/input
output_file=/tmp/output
password=123456

onerror() {
    echo "$BASH_COMMAND on line ${BASH_LINENO[0]} failed: $?"
    exit 1
}
trap onerror ERR

cleanup() {
	rm -f ${context_file} ${input_file} ${output_file}
}
trap cleanup EXIT

echo -n "01234567890123456789012345678901" >${input_file}
tpm2_createprimary ${quiet} -C o -g sm3_256 -G hmac:sm3_256 -c ${context_file} -p ${password} -a "sign|noda|sensitivedataorigin|userwithauth|fixedtpm|fixedparent"
tpm2_hmac ${quiet} -p ${password} -g sm3_256 -c ${context_file} -o ${output_file} ${input_file}
tpm2_hash ${quiet} -g sm3_256 -o ${output_file} ${input_file}

