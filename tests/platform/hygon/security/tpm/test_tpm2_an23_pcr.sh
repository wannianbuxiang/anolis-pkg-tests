#!/bin/bash

quiet=-Q
input_file=/tmp/input
pcrs_file=/tmp/pcrs

onerror() {
    echo "$BASH_COMMAND on line ${BASH_LINENO[0]} failed: $?"
    exit 1
}
trap onerror ERR

cleanup() {
	rm -f ${input_file} ${pcrs_file}
}
trap cleanup EXIT

echo -n "01234567890123456789012345678901" >${input_file}
tpm2_pcrreset ${quiet} 16 23
tpm2_pcrread ${quiet} sm3_256:0,1,8,9 -o ${pcrs_file}
tpm2_pcrevent ${quiet} ${input_file}
tpm2_pcrextend ${quiet} 16:sm3_256="2b14a1fc49869413b0beb707069cffc0c6b0a51f3fedb9ce072c80709652b3ae"

