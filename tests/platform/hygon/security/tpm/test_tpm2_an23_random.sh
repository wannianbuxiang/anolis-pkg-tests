#!/bin/bash

quiet=-Q
input_file=/tmp/input
output_file=/tmp/output

onerror() {
    echo "$BASH_COMMAND on line ${BASH_LINENO[0]} failed: $?"
    exit 1
}
trap onerror ERR

cleanup() {
	rm -f ${input_file} ${output_file}
}
trap cleanup EXIT

echo -n "01234567890123456789012345678901" >${input_file}
tpm2_stirrandom ${quiet} ${input_file}
tpm2_getrandom ${quiet} -f 32 -o ${output_file}

