# -*- encoding: utf-8 -*-

"""
@File:      tc_tkm_tkm_enc_fun001.py
@Time:      2024/09/23 11:47:20
@Author:    Xingrui Yi
@Version:   1.0
@Contact:   yixingrui@linux.alibaba.com
@License:   Mulan PSL v2
@Modify:    Xingrui Yi
"""

import os
import logging
from common.basetest import LocalTest

abs=os.path.dirname(os.path.abspath(__file__))
progress_log = logging.getLogger("progress")

class Test(LocalTest):
    """
    :avocado: tags=P1,x86_64,local,hygon,hygon_gen2,hygon_gen3,hygon_gen4,tpm
    """

    def test(self):
        self.skip_non_root_test()
        self.cmd("yum install tpm2-tools -y")
        self.cmd("yum install tpm2-abrmd -y")
        self.cmd("yum install tpm2-tss -y")
        ret_c, _ = self.cmd("uname -a | grep -i an23", ignore_status=True)
        if ret_c == 0:
            _, output = self.cmd("cd " + abs + ";sh test_an23.sh", ignore_status=True)
        else:
            _, output = self.cmd("cd " + abs + ";sh test_an8.sh", ignore_status=True)
        progress_log.info(output)
        

    def tearDown(self):
        super().tearDown()
