#!/bin/bash

quiet=-Q
context_file=/tmp/context
context_load_file=/tmp/context_load
pub_file=/tmp/pub
priv_file=/tmp/priv
input_file=/tmp/input

password=123456
pass_change1=123
pass_change2=789

onerror() {
    echo "$BASH_COMMAND on line ${BASH_LINENO[0]} failed: $?"
    exit 1
}
trap onerror ERR

cleanup() {
	rm -f ${context_file} ${context_load_file} ${pub_file} ${priv_file} ${input_file}
}
trap cleanup EXIT

echo -n "01234567890123456789012345678901" >${input_file}
tpm2_createprimary ${quiet} -C o -g sm3_256 -G eccsm2:null:sm4128cfb -c ${context_file} -p ${password}
tpm2_create ${quiet} -g sm3_256 -u ${pub_file} -r ${priv_file} -C ${context_file} -i ${input_file} -P ${password} -p ${pass_change1}
tpm2_load ${quiet} -C ${context_file} -u ${pub_file} -r ${priv_file} -c ${context_load_file} -P ${password} 

tpm2_changeauth ${quiet} -p ${pass_change1} -c ${context_load_file} -C ${context_file} -r ${priv_file} ${pass_change2}
tpm2_load ${quiet} -C ${context_file} -u ${pub_file} -r ${priv_file} -c ${context_load_file} -P ${password}
tpm2_changeauth ${quiet} -p ${pass_change2} -c ${context_load_file} -C ${context_file} -r ${priv_file} ${pass_change1}

