#!/bin/bash

quiet=-Q
context_file=/tmp/context
context_load_file=/tmp/context_load
input_file=/tmp/input
context_attest_file=/tmp/context.att
context_sig_file=/tmp/context.sig
attest_file=/tmp/attest
ticket_file=/tmp/ticket
creation_hash_file=/tmp/creation_hash
sig_file=/tmp/sig
pcr_file=/tmp/pcr

password=123456

onerror() {
    echo "$BASH_COMMAND on line ${BASH_LINENO[0]} failed: $?"
    exit 1
}
trap onerror ERR

cleanup() {
	rm -f ${context_attest_file} ${context_sig_file} ${attest_file} ${sig_file} ${ticket_file} ${creation_hash_file} ${pem_pri_file} ${pem_pub_file} ${pcr_file}
}
trap cleanup EXIT

echo -n "01234567890123456789012345678901" >${input_file}
tpm2_createprimary ${quiet} -C o -g sm3_256 -G hmac:sm3_256 -c ${context_attest_file} -p ${password} -t ${ticket_file} -d ${creation_hash_file} -a "sign|noda|sensitivedataorigin|userwithauth|fixedtpm|fixedparent"
tpm2_createprimary ${quiet} -C o -g sm3_256 -G "eccsm2:sm2-sm3_256:null" -c ${context_sig_file} -p ${password} -a "sign|noda|sensitivedataorigin|userwithauth|fixedtpm|fixedparent"

tpm2_certify ${quiet} -c ${context_attest_file} -C ${context_sig_file} -p ${password} -P ${password} -g sm3_256 -o ${attest_file} -s ${sig_file} -S sm2
tpm2_verifysignature ${quiet} -g sm3_256 -s ${sig_file} -m ${attest_file} -c ${context_sig_file}

tpm2_gettime ${quiet} -p ${password} -g sm3_256 -s sm2 -o ${sig_file} -c ${context_sig_file} --attestation ${input_file}
tpm2_verifysignature ${quiet} -g sm3_256 -m ${input_file} -s ${sig_file} -c ${context_sig_file}

tpm2_certifycreation ${quiet} -C ${context_sig_file} -c ${context_attest_file} -P ${password} -g sm3_256 -s sm2 -d ${creation_hash_file} -t ${ticket_file} -o ${sig_file} --attestation ${attest_file}
tpm2_verifysignature ${quiet} -g sm3_256 -s ${sig_file} -m ${attest_file} -c ${context_sig_file}

tpm2_quote ${quiet} -c ${context_sig_file} -p ${password} -l sm3_256:1,2,3,4,5 -s ${sig_file} -m ${attest_file} -o ${pcr_file} -g sm3_256 -S sm2
tpm2_verifysignature ${quiet} -g sm3_256 -s ${sig_file} -m ${attest_file} -c ${context_sig_file}

pem_pri_file=/tmp/private.ecc.pem
pem_pub_file=/tmp/public.ecc.pem
pcr_file=/tmp/pcr
echo -n "01234567890123456789012345678901" >${input_file}
openssl ecparam -name SM2 -genkey -out ${pem_pri_file} 2>/dev/null
openssl ec -in ${pem_pri_file} -pubout -out ${pem_pub_file} 2>/dev/null
tpm2_loadexternal ${quiet} -G ecc -g sm3_256 -r ${pem_pri_file} -c ${context_load_file}
tpm2_quote ${quiet} -c ${context_load_file} -l sm3_256:1,2,3,4,5 -s ${sig_file} -m ${attest_file} -o ${pcr_file} -q ${input_file} -g sm3_256 -S sm2
tpm2_checkquote ${quiet} -G ecc -g sm3_256 -m ${attest_file} -s ${sig_file} -u ${pem_pub_file} -q ${input_file} -f ${pcr_file}

