#!/bin/bash

quiet=-Q

onerror() {
    echo "$BASH_COMMAND on line ${BASH_LINENO[0]} failed: $?"
    exit 1
}
trap onerror ERR

tpm2_selftest ${quiet} -f 
tpm2_incrementalselftest ${quiet} sm2 sm3_256 sm4
tpm2_gettestresult ${quiet}
