#!/bin/bash

quiet=-Q
input_file=/tmp/input
output_file=/tmp/output
context_sig_file=/tmp/context.sig
attest_file=/tmp/attest
sig_file=/tmp/sig
password=123456
nv_index=0x01500001

onerror() {
    echo "$BASH_COMMAND on line ${BASH_LINENO[0]} failed: $?"
    exit 1
}
trap onerror ERR

cleanup() {
	rm -f ${input_file} ${output_file} ${context_sig_file} ${attest_file} ${sig_file}
	tpm2_nvundefine ${quiet} -C o ${nv_index} 2>/dev/null || true
	
}
trap cleanup EXIT

cleanup

echo -n "01234567890123456789012345678901" >${input_file}
tpm2_nvdefine ${quiet} -C o -p ${password} -a "authread|authwrite" -s 32 -g sm3_256 ${nv_index}
tpm2_nvundefine ${quiet} -C o ${nv_index}
tpm2_nvdefine ${quiet} ${nv_index} -C o -s 32 -a "policyread|policywrite|authread|authwrite|ownerwrite|ownerread|read_stclear|writedefine" -p ${password} -g sm3_256
tpm2_nvreadpublic ${quiet} ${nv_index} > ${output_file}
tpm2_nvwrite ${quiet} ${nv_index} -P ${password} -i ${input_file}
tpm2_nvread ${quiet} -P ${password} ${nv_index} -o ${output_file}
tpm2_nvreadlock ${quiet} -C o ${nv_index}
tpm2_nvwritelock ${quiet} -C o ${nv_index}
tpm2_nvwrite ${quiet} ${nv_index} -P ${password} -i ${input_file} 2>/dev/null || true
tpm2_nvread ${quiet} -P ${password} ${nv_index} 2>/dev/null || true 
tpm2_nvundefine ${quiet} -C o ${nv_index}

tpm2_nvdefine ${quiet} -C o -a "nt=extend|ownerread|policywrite|ownerwrite" ${nv_index} -g sm3_256
echo -n "01234567890123456789012345678901" | tpm2_nvextend ${quiet} -C o -i- ${nv_index}
tpm2_nvundefine ${quiet} -C o ${nv_index}

tpm2_nvdefine ${quiet} -C o -P "" -a "nt=counter|authread|authwrite|ownerread|ownerwrite|writedefine" ${nv_index} -g sm3_256
tpm2_nvincrement ${quiet} -C o -P "" ${nv_index}
tpm2_nvundefine ${quiet} -C o ${nv_index}

bits=0xbadc0de
tpm2_nvdefine ${quiet} -C o -P "" -a "nt=bits|ownerread|policywrite|ownerwrite|writedefine" ${nv_index} -g sm3_256
tpm2_nvsetbits ${quiet} -C o -P "" -i ${bits} ${nv_index}
check=$(tpm2_nvread -C o -P "" ${nv_index} | xxd -p | sed s/'^0*'/0x/)
tpm2_nvundefine ${quiet} -C o ${nv_index}
if [ "${check}" != "${bits}" ];then
		echo "tpm2_nvsetbits fail"
		exit 1
fi

tpm2_nvdefine ${quiet} ${nv_index} -C o -s 32 -a "authread|authwrite|ownerwrite|ownerread" -p ${password} -g sm3_256 -s 32
dd if=/dev/urandom bs=1 count=32 status=none| tpm2_nvwrite ${quiet} -P ${password} -i- ${nv_index}
tpm2_createprimary ${quiet} -C o -g sm3_256 -G "eccsm2:sm2-sm3_256:null" -c ${context_sig_file} -p ${password} -a "sign|noda|sensitivedataorigin|userwithauth|fixedtpm|fixedparent"

tpm2_nvcertify ${quiet} -C ${context_sig_file} -c ${nv_index} -p ${password} -P ${password} -g sm3_256 --attestation ${attest_file} -o ${sig_file} -s sm2 ${nv_index}
tpm2_nvundefine ${quiet} -C o ${nv_index}
tpm2_verifysignature ${quiet} -g sm3_256 -s ${sig_file} -m ${attest_file} -c ${context_sig_file}

