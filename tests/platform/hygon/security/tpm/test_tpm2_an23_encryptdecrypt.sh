#!/bin/bash

quiet=-Q
context_file=/tmp/context
input_file=/tmp/input
input_raw=/tmp/input_raw
output_file=/tmp/output
password=123456

onerror() {
    echo "$BASH_COMMAND on line ${BASH_LINENO[0]} failed: $?"
    exit 1
}
trap onerror ERR

cleanup() {
	rm -f ${context_file} ${input_file} ${input_raw} ${output_file}
}
trap cleanup EXIT

echo -n "01234567890123456789012345678901" >${input_file}
tpm2_createprimary ${quiet} -C o -g sm3_256 -G sm4128cbc -c ${context_file} -p ${password} -a "sign|decrypt|noda|sensitivedataorigin|userwithauth|fixedtpm|fixedparent"
tpm2_encryptdecrypt ${quiet} -p ${password} -c ${context_file} -o ${output_file} ${input_file} 2>/dev/null
tpm2_encryptdecrypt ${quiet} -p ${password} -c ${context_file} -o ${input_raw} -d ${output_file} 2>/dev/null
diff ${input_file} ${input_raw}

