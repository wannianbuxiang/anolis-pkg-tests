#!/bin/bash

quiet=-Q

onerror() {
    echo "$BASH_COMMAND on line ${BASH_LINENO[0]} failed: $?"
    exit 1
}
trap onerror ERR

tpm2_clockrateadjust ${quiet} -c o fff
