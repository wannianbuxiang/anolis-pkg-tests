#!/bin/bash

quiet=-Q
context_file=/tmp/context
context_load_file=/tmp/context_load
pub_file=/tmp/pub
priv_file=/tmp/priv
input_file=/tmp/input
name_file=/tmp/name
password=123456
mkcred_file=/tmp/mkcred
actcred_file=/tmp/actcred
pub_key_file=/tmp/pub_key

onerror() {
    echo "$BASH_COMMAND on line ${BASH_LINENO[0]} failed: $?"
    exit 1
}
trap onerror ERR

cleanup() {
	rm -f ${context_file} ${context_load_file} ${pub_file} ${priv_file} ${input_file} ${name_file} ${mkcred_file} ${actcred_file} ${pub_key_file}
}
trap cleanup EXIT

echo -n "01234567890123456789012345678901" >${input_file}
tpm2_createprimary ${quiet} -C o -g sm3_256 -G eccsm2:null:sm4128cfb -c ${context_file} -p ${password} -k ${pub_key_file}
tpm2_create ${quiet} -C ${context_file} -g sm3_256 -G "eccsm2:sm2-sm3_256:null" -u ${pub_file} -r ${priv_file} -P ${password} -p ${password} -a "sign|noda|sensitivedataorigin|userwithauth|fixedtpm|fixedparent"
tpm2_load ${quiet} -C ${context_file} -u ${pub_file} -r ${priv_file} -c ${context_load_file} -P ${password} -n ${name_file}
loaded_key_name=`cat ${name_file} | xxd -p -c $(ls -l ${name_file} | awk {'print $5'})`
tpm2_makecredential ${quiet} -e ${pub_key_file} -s ${input_file} -n ${loaded_key_name} -o ${mkcred_file}
tpm2_activatecredential ${quiet} -c ${context_load_file} -C ${context_file} -i ${mkcred_file} -o ${actcred_file} -p ${password} -P ${password}

