# -*- encoding: utf-8 -*-

"""
@File:      tc_hct_compute_test_fun001.py
@Time:      2024/09/23 11:47:20
@Author:    Xingrui Yi
@Version:   1.0
@Contact:   yixingrui@linux.alibaba.com
@License:   Mulan PSL v2
@Modify:    Xingrui Yi
"""

import os
import logging
from common.basetest import LocalTest
abs=os.path.dirname(os.path.abspath(__file__))
hct_test_path = abs + "/hct_test-OpenSSL3.0"
hct_lib_path = abs + "/hct-2.0.0-2024-0430-release.x86_64.rpm"
os.chmod(hct_test_path, 0o755)

progress_log = logging.getLogger("progress")

class Test(LocalTest):
    """
    :avocado: tags=P1,x86_64,local,hygon,hygon_gen2,hygon_gen3,hygon_gen4,hct
    """
    def setUp(self):
        super().setUp()

    def test(self):
        self.skip_non_root_test()
        self.cmd('yum install libuuid-devel -y')
        self.cmd('yum install numactl -y')
        self.cmd('yum install openssl -y')
        self.cmd('yum install openssl-libs -y')
        self.cmd('modprobe vfio')
        self.cmd('modprobe vfio-pci')
        self.cmd('modprobe vfio_iommu_type1')
        self.cmd('modprobe mdev')
        self.cmd('modprobe hct')

        ret_c, _ = self.cmd("cat /etc/os-release | grep -i an23", ignore_status=True)
        if ret_c == 0:
            self.cmd('rpm -ivh '+ abs + "/hct-2.0.0-2024-0430-release.x86_64.rpm", ignore_status=True)
            self.cmd("/opt/hygon/hct/hct/script/hctconfig start")
            _, output = self.cmd(hct_test_path)
        else:
            self.cmd('rpm -ivh '+ abs + "/hct-1.1.1-2023-0930-rc.x86_64.rpm", ignore_status=True)
            self.cmd('yum install python2 -y')
            self.cmd("/opt/hygon/hct/hct/script/hctconfig start")
            _, output = self.cmd("/opt/hygon/hct/bin/hct_test")

        progress_log.info(output)

    def tearDown(self):
        super().tearDown()
        ret_c, _ = self.cmd("cat /etc/os-release | grep -i an23", ignore_status=True)
        if ret_c == 0:
            self.cmd('rpm -e hct-2.0.0-1.x86_64')
        else:
            self.cmd('rpm -e hct-1.1.1-1.x86_64')