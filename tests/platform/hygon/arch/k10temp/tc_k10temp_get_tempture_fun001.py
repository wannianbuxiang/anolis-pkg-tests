# -*- encoding: utf-8 -*-

"""
@File:      tc_k10temp_get_tempture_fun001.py
@Time:      2024/09/23 10:32:20
@Author:    Xingrui Yi
@Version:   1.0
@Contact:   yixingrui@linux.alibaba.com
@License:   Mulan PSL v2
@Modify:    Xingrui Yi
"""

import logging
from common.basetest import LocalTest

progress_log = logging.getLogger("progress")

class Test(LocalTest):
    """
    :avocado: tags=P1,x86_64,local,hygon,hygon_gen2,hygon_gen3,hygon_gen4,k10temp
    """
    def test(self):
        self.skip_non_root_test()
        self.cmd('yum install lm_sensors -y')
        _, output = self.cmd('sensors')
        progress_log.info(output)

    def tearDown(self):
        super().tearDown()
