# -*- encoding: utf-8 -*-

"""
@File:      tc_iommu_x2apic_detect_fun001.py
@Time:      2024/09/23 10:32:20
@Author:    Xingrui Yi
@Version:   1.0
@Contact:   yixingrui@linux.alibaba.com
@License:   Mulan PSL v2
@Modify:    Xingrui Yi
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    :avocado: tags=P1,x86_64,local,hygon,hygon_gen2,hygon_gen3,hygon_gen4,x2apic
    """
    def test(self):
        result = self.cmd('dmesg | grep -i x2apic')
        self.assertTrue(result is not None, 'result should not be None')

    def tearDown(self):
        super().tearDown()
