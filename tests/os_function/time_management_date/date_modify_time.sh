#!/bin/bash
#获取当前系统中的时区
  timezone=`timedatectl | awk '/Time zone:/ {print $3}'`
#将时区设置为上海时区
  timedatectl set-timezone Asia/Shanghai
#获取当前时间戳并输出
  timestamp=`date +%s`
  echo $timestamp
#通过date将时间设置为2020-08-31 12:30
  date -s '2020-08-31 12:30'
#获取当前时间并赋值给date
  date=`date`
	#校验date值是否是2020-08-31 12:30，允许1s误差
	  # 允许1s误差
if [[ $date == "Mon Aug 31 12:30:00 CST 2020" || $date == "Mon Aug 31 12:30:01 CST 2020"  || $date == "2020年 08月 31日 星期一 12:30:00 CST" || $date == "2020年 08月 31日 星期一 12:30:01 CST" ]];then
		        echo "[TestResult] date_modify_time PASSED"
			else
				    echo "[TestResult] date_modify_time FAILED"
				    echo "Actual wrong result is $date"
			fi
# 恢复环境
        timedatectl set-timezone $timezone
	expect_date=$(date -d @$[$timestamp+2] +"%Y-%m-%d %H:%M:%S")
	echo $expect_date
	logger "date -s \"$expect_date\""
	systemctl restart chronyd
	sleep 10
	hwclock --systohc


