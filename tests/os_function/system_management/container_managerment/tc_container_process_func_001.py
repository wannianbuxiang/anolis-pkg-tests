#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_container_process_func_001.py
@Time:      2022/9/22 19:06:45
@Author:    yangshunfeng
@Version:   1.0
@License:   Mulan PSL v2
"""
from common.basetest import LocalTest

class ContainProcTest(LocalTest):
    """
    See   tc_container_process_func_001.yaml for details
    :avocado: tags=fix,P2,noarch,local,baseos_container_default,app_container,app_container_default
    """

    def test_container_process(self):
        ret_c,container_pid = self.cmd(self.container_engine+' inspect --format "{{ .State.Pid }}" '+self.container_id)
        self.assertTrue(container_pid is not None,"Application image has no corresponding process")
        if self.container_engine == "docker":
            ret_c,container_proc = self.cmd(self.container_engine+' top '+self.container_id+' | grep '+container_pid)
            self.assertTrue(container_proc is not None,"No valid process started in container")

