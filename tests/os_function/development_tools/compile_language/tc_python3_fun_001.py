# -*- encoding: utf-8 -*-

"""
@File:      tc_python3_fun_001.py
@Time:      2024/03/15 10:12:50
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_python3_fun_001.yaml for details

    :avocado: tags = fix,P0,noarch,local
    """

    PARAM_DIC = {"pkg_name": "python3"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > test.py <<EOF
# -*- coding: UTF-8 -*-
print('hello')

EOF"""
        self.cmd(cmdline)

    def test(self):
        ret_c, ret_o = self.cmd("python3 test.py")
        self.assertTrue("hello" in ret_o, "check output error.")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf test.py")