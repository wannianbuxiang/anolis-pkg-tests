#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_whois_fun_003.py
@Time:      2024/03/20 13:48:50
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_whois_fun_003.yaml for details.

    Test specifically checks the admin-contact information for the IDN domain.
    The domain and expected string are kept in configuration for maintainability.

    :avocado: tags = fixed,P2,noarch,local
    """
    
    PARAM_DIC = {"pkg_name": "whois"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.DOMAIN = 'baidu.com'
        self.SERVICE = 'whois.iana.org'
        self.Ip = '8.8.8.8'
        self.command_list = [
            ['-h', self.SERVICE, self.DOMAIN],
            ['-h', self.SERVICE, '-p', '43', self.DOMAIN],
            ['-q', 'version'],
            ['-t', 'domain'],
            ['-v', 'person'],
            ['-I', self.DOMAIN],
            ['-H', self.DOMAIN],
            ['-c', self.DOMAIN],
            ['--no-recursion', self.DOMAIN],
            ['--help'],
            ['-b', self.Ip],
            ['--verbose', self.DOMAIN],
            ['--version'],
            ['-l', self.DOMAIN],
            ['-L', self.DOMAIN],
            ['-i mnt-by', self.DOMAIN],
            ['-T domain', self.DOMAIN],
            ['-K', self.DOMAIN],
            ['-r', self.DOMAIN],
            ['-R', self.DOMAIN],
            ['-a', self.DOMAIN]
            
        ]
   
    def test(self):
        for item in self.command_list:
            try:
                ret_c, ret_o = self.cmd(f"whois {' '.join(item)}")
                if ret_c != 0:
                    self.fail(f"Command failed with exit code: {ret_c}, Output: {ret_o}")
            except Exception as e:
                self.fail(f"An unexpected error occurred: {str(e)}")    
    
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)