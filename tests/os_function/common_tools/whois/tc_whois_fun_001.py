#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_whois_fun_001.py
@Time:      2024/03/19 13:01:50
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_whois_fun_001.yaml for details

    :avocado: tags = fix,P2,noarch,local
    """
    
    PARAM_DIC = {"pkg_name": "whois"}
    DOMAIN_TEST_DATA = ("baidu.com", )
    IP_TEST_DATA = ("8.8.8.8", )
    
    EXPECTED_DOMAIN_OUTPUT = (
        'Domain Name: baidu.com', 
        'Registrar WHOIS Server: whois.markmonitor.com', 
        'Updated Date', 
        'Registrant State/Province: Beijing'
    )
    
    EXPECTED_IP_OUTPUT = (
        'NetRange:       8.8.8.0 - 8.8.8.255',
        'RegDate',
        'Updated',
        'NetName:        GOGL'
    )
    
    def assertmsg(self, expect_msg, actual_output, identifier=None):
        """Enhanced assertion message with optional identifier for clarity."""
        message = f'{expect_msg} not in {actual_output}'
        if identifier:
            message += f", check {identifier}"
        self.assertTrue(expect_msg in actual_output, message)
    
    def check_output(self, expected_outputs, actual_output, identifier=None):
        """Refactored to iterate over expected outputs for maintainability."""
        for i, expect_msg in enumerate(expected_outputs):
            self.assertmsg(expect_msg, actual_output, identifier=i+1)
    
    PARAM_DIC = {"pkg_name": "whois"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        try:
            for test_data in (self.DOMAIN_TEST_DATA, self.IP_TEST_DATA):
                ret_c, ret_o = self.cmd(f"whois {test_data[0]}")
                
                if test_data is self.DOMAIN_TEST_DATA:
                    expected_output = self.EXPECTED_DOMAIN_OUTPUT
                else:
                    expected_output = self.EXPECTED_IP_OUTPUT
                
                identifier = 'domain' if test_data is self.DOMAIN_TEST_DATA else 'IP'
                message = f'Execute whois {identifier} fail'
                
                if self.assertEqual(ret_c, 0, message):
                    self.check_output(expected_output, ret_o, identifier=identifier)
        except Exception as e:
            self.fail(f"An exception occurred during the test: {e}")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)