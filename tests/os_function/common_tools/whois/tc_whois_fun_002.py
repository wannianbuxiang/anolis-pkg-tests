#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_whois_fun_002.py
@Time:      2024/03/19 13:01:50
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_whois_fun_002.yaml for details.

    Test specifically checks the admin-contact information for the IDN domain.
    The domain and expected string are kept in configuration for maintainability.

    :avocado: tags = fixed,P2,noarch,local
    """
    
    PARAM_DIC = {"pkg_name": "whois"} 
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.DOMAIN = "xn--e1afmkfd.xn--p1ai"  # IDN domain for test
        self.EXPECTED_OUPTUT = "admin-contact: http://www.cctld.ru"
    
    def test(self):
        try:
            ret_c, ret_o = self.cmd(f"whois '{self.DOMAIN}'")
            
            if ret_c != 0:
                self.fail(f"Command failed with exit code: {ret_c}, Output: {ret_o}")
            
            self.assertIn(self.EXPECTED_OUPTUT, ret_o, 'IDN Domain Test fail')
        except Exception as e:
            self.fail(f"An unexpected error occurred: {str(e)}")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)