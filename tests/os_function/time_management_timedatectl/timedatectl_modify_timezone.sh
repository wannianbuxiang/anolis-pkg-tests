#!/bin/bash
#关闭NTP（Network Time Protocol）服务，确保系统不会从网络自动同步时间
  timedatectl set-ntp no
#设置系统的时区为“Asia/Hong_Kong”
  timedatectl set-timezone Asia/Hong_Kong
#执行timedatectl命令并通过管道将输出传递给grep命令，以搜索包含“Time zone”的行。搜索到的行（即包含当前时区信息的行）被赋值给变量timezone
  timezone=`timedatectl|grep "Time zone"`
#检查变量timezone的值是否等于特定的字符串（即系统时区是否已正确设置为“Asia/Hong_Kong”）
  if [[ $timezone = "                Time zone: Asia/Hong_Kong (HKT, +0800)" ]];then
    echo "[TestResult] timedatectl_modify_timezone PASSED"
  else
    echo "[TestResult] timedatectl_modify_timezone FAILED"
    echo "Actual wrong result is $timezone"
  fi
# 恢复环境
  timedatectl set-timezone Asia/Shanghai

