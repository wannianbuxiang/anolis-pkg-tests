#!/bin/bash
# timedatectl查看通过date查看日期、时间和时区
  timedatectl
#判断date命令是否成功执行
  if [[ $? -eq 0 ]];then
    echo "[TestResult] timedatectl_cat_time PASSED"
  else
    echo "[TestResult] timedatectl_cat_time FAILED"
    echo "Actual wrong result is $?"
  fi
