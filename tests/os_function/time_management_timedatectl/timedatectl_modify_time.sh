#!/bin/bash
#获取当前时间戳
  timestamp=`date +%s`
#关闭NTP（Network Time Protocol）服务，确保系统不会从网络自动同步时间
  timedatectl set-ntp no
#使用timedatectl命令手动设置系统时间为'2020-09-01 18:30:21'。
  timedatectl set-time '2020-09-01 18:30:21'
#运行timedatectl命令获取当前的系统时间和日期设置，并将其输出赋值给变量timedate
  timedate=`timedatectl`
  expect_timedate1="               Local time: Tue 2020-09-01 18:30:21 CST
           Universal time: Tue 2020-09-01 10:30:21 UTC
                 RTC time: n/a
                Time zone: Asia/Shanghai (CST, +0800)
System clock synchronized: no
              NTP service: inactive
          RTC in local TZ: no"
  expect_timedate2="               Local time: Tue 2020-09-01 18:30:22 CST
           Universal time: Tue 2020-09-01 10:30:21 UTC
                 RTC time: n/a
                Time zone: Asia/Shanghai (CST, +0800)
System clock synchronized: no
              NTP service: inactive
          RTC in local TZ: no"
  expect_timedate3="               Local time: 二 2020-09-01 18:30:21 CST
           Universal time: 二 2020-09-01 10:30:21 UTC
                 RTC time: n/a
                Time zone: Asia/Shanghai (CST, +0800)
System clock synchronized: no
              NTP service: inactive
          RTC in local TZ: no"
  expect_timedate4="               Local time: 二 2020-09-01 18:30:22 CST
           Universal time: 二 2020-09-01 10:30:21 UTC
                 RTC time: n/a
                Time zone: Asia/Shanghai (CST, +0800)
System clock synchronized: no
              NTP service: inactive
          RTC in local TZ: no"
#比较实际获取的系统时间和日期设置（$timedate）与预期的设置（$expect_timedate1或$expect_timedate2）是否匹配
  # 允许1s误差
  if [[ $timedate == $expect_timedate1 || $timedate == $expect_timedate2 || $timedate == $expect_timedate3 || $timedate == $expect_timedate4 ]];then
    echo "[TestResult] timedatectl_modify_time PASSED"
  else
    echo "[TestResult] timedatectl_modify_time FAILED"
    echo $expect_timedate1
    echo $expect_timedate2
    echo "Actual wrong result is $timedate"
  fi
# 恢复环境
  expect_date=$(date -d @$[$timestamp+2] +"%Y-%m-%d %H:%M:%S")
  logger "date -s \"$expect_date\""
  systemctl restart chronyd
  sleep 10
  hwclock --systohc
