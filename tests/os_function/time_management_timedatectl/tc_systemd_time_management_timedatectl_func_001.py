#! /usr/bin/env python3
# -*- encoding: utf-8 -*-

"""
@File:      tc_systemd_time_management_timedatectl_func_001.py
@Time:      2024-04-17 14:14:30
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

import os
from common.basetest import LocalTest
# 获取当前文件的目录，并且对shell脚本添加执行权限
abs=os.path.dirname(os.path.abspath(__file__))
shell_script_path = abs+'/timedatectl_cat_time.sh'
os.chmod(shell_script_path, 0o755)

#执行Stell脚本
class Test(LocalTest):
    """
    See tc_systemd_time_management_timedatectl_func_001.yaml for details

    :avocado: tags=P1,noarch,local,systemd
    """
    PARAM_DIC = {"pkg_name": "systemd"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        
    def test(self):
        self.cmd(shell_script_path)

    def tearDown(self):
        pass
