# -*- encoding: utf-8 -*-

"""
@File:      gpg.py
@Time:      2024/03/15
@Author:    Liujiang
@Version:   1.0
@Contact:   Liujiang <wb-lj931787@alibaba-inc.com>
@License:   Mulan PSL v2
"""

import os, binascii, re
import subprocess
from common.sysinfo import SysInfo

class Gnupg2Manager(object):
    def __init__(self) -> None:
        self.cmd = SysInfo.cmd()
        self.keypair_id = ""
        self.pubkey_id = ""
        self.subkey_id = ""
        self.seckey_id = ""
        self.ssbkey_id = ""
    
    def get_pubkey_id(self, user, host="local", ignore_status=True):
        '''
        Get public key ID.
        user: Username, user email, or keypair id.
        '''

        _, pubkey_id = self.cmd(
            "gpg --with-colons --list-keys %s |awk -F':' '/pub/ {print $5}'" %(user),
                ignore_status=ignore_status)
        if pubkey_id:
            self.pubkey_id = pubkey_id
        return pubkey_id

    def get_subkey_id(self, user, host="local", ignore_status=True):
        '''
        Get public sub key ID.
        user: Username, user email, or keypair id.
        '''

        _, subkey_id = self.cmd(
            "gpg --with-colons --list-keys %s |awk -F':' '/sub/ {print $5}'" %(user),
                ignore_status=ignore_status)
        if subkey_id:
            self.subkey_id = subkey_id
        return subkey_id
    
    def get_seckey_id(self, user, host="local", ignore_status=True):
        '''
        Get private key ID.
        user: Username, user email, or keypair id.
        '''

        _, seckey_id = self.cmd(
            "gpg --with-colons --list-secret-keys %s |awk -F':' '/sec/ {print $5}'" %(user), 
                ignore_status=ignore_status)
        if seckey_id:
            self.seckey_id = seckey_id
        return seckey_id
    
    def get_ssbkey_id(self, user, host="local", ignore_status=True):
        '''
        get private sub key ID
        user: Username, user email, or keypair id.
        '''

        _, ssbkey_id = self.cmd(
            "gpg --with-colons --list-keys %s |awk -F':' '/ssb/ {print $5}'" %(user),
                ignore_status=ignore_status)
        if ssbkey_id:
            self.seckey_id = ssbkey_id
        return ssbkey_id
        
    def get_keypair_id(self, user, host="local", ignore_status=True):
        '''
        get key pair ID
        user: Username, user email, or keypair id.
        '''
        _, keypair_id = self.cmd(
            "gpg --with-colons --list-keys %s |grep 'pub' -A 1 |awk -F':' '/fpr/ {print $10}'" %(user),
                ignore_status=ignore_status)
        if keypair_id:
            self.keypair_id = keypair_id
        return keypair_id
        
    def generate_fixed_parameter_key_pair(self, name, email, testpasswd, host="local", ignore_status=True):
        '''
        Generate a fixed parameter key pair
        name: User name for generating key pairs
        email: Email for generating key pairs
        testpasswd: Generate password for key pairs
        retrun: Key pair ID list
        '''

        cmdline_generate_params = """cat >key_params.txt<<EOF
Key-Type: RSA
Key-Length: 2048
Subkey-Type: RSA
Subkey-Length: 2048
Name-Real: {_name}
Name-Email: {_email}
Expire-Date: 0
Passphrase: {_passwd}
EOF
""".format(_name = name,
           _email = email,
           _passwd = testpasswd)
        
        self.cmd(cmdline_generate_params)
        _, gpg_result = self.cmd("gpg --batch --gen-key key_params.txt 2>&1")
        hex_pattern = r'[0-9a-fA-F]{10,}'
        self.keypair_id = re.findall(hex_pattern, gpg_result)[0]
        return self.keypair_id

    def delete_key_pair(self, keypair_id, email):
        '''
        Used to delete key pairs
        keypair_id: 
        email: The email address for the key
        return: Boolean(Ture, False)
        '''

        self.cmd("gpg --batch --yes --delete-secret-and-public-keys %s" %keypair_id)
        _, list_keys = self.cmd("gpg --list-keys")
        _, list_secret_keys = self.cmd("gpg --list-secret-keys")
        if email not in list_keys and email not in list_secret_keys:
            return True
        else:
            return False

    def encryption_data(self, recipient, file, host="local", ignore_status=True):
        '''
        Use Gnupg2 data encryption.
        recipient: Username, user email, or key id
        file: Used for encrypted file.
        return: Boolean(Ture, False)
        '''
        try:
            binascii.unhexlify(recipient)
            self.cmd("gpg --encrypt --recipient %s! %s" %(recipient, file), 
                     ignore_status=ignore_status)
        except binascii.Error:
            self.cmd("gpg --encrypt --recipient %s %s" %(recipient, file),
                     ignore_status=ignore_status)
        encrypted_file = "%s.gpg" %file
        if os.path.isfile(encrypted_file) and os.path.getsize(encrypted_file) > 0:
            return True
        else:
            return False
        
    def decryption_data(self, decrypted_file, testpasswd, host="local", ignore_status=True):
        '''
        Use Gnupg2 data dncryption.
        decrypted_file: Decrypted files
        testpasswd: The password for the key
        return: Boolean(Ture, False)
        '''

        file = "%s.txt" %decrypted_file
        self.cmd("gpg --batch --yes --pinentry-mode loopback --passphrase" +
                 " %s --decrypt %s > %s" %(testpasswd, decrypted_file, file))
        if os.path.isfile(file) and os.path.getsize(file) > 0:
            return True
        else:
            return False
        
    def export_public_key(self, keypair_id, publickey_file, host="local", ignore_status=True):
        '''
        Export public key.
        keypair_id: The public key ID that needs to be exported
        publickey_file: The file that needs to save the public key
        return Boolean(Ture, False)
        '''

        self.cmd("gpg --export -a %s > %s" %(keypair_id, publickey_file))
        if os.path.isfile(publickey_file) and os.path.getsize(publickey_file) > 0:
            return True
        else:
            return False

    def import_public_key(self, publickey_file, email, host="local", ignore_status=True):
        '''
        Import public key.
        publickey_file: The public key file that needs to be imported
        email: User email corresponding to public key
        return Boolean(Ture, False)
        '''

        self.cmd("gpg --import %s" %publickey_file)
        code, list_keys = self.cmd("gpg --list-keys")
        if email in list_keys:
            return True
        else:
            return False

    def import_private_key(self, privatekey_file, email, host="local", ignore_status=True):
        '''
        Import private key.
        publickey_file: The private key file that needs to be imported
        email: User email corresponding to public key
        return Boolean(Ture, False)
        '''

        self.cmd("gpg --batch --passphrase 'mysecretpassphrase' --import %s" %privatekey_file)
        code, list_keys = self.cmd("gpg --list-secret-keys")
        if email in list_keys:
            return True
        else:
            return False
    
    def generate_revocation_certificate(self, user, certificate_file, host="local", ignore_status=True):
        '''
        Generate revocation certificate.
        user: Username, user email, or keypair id
        certificate_file: Used to save revoked certificates
        return Boolean(Ture, False)
        '''

        cmdline_generate_revocation_certificate_script = """cat >generate_revocation_certificate.exp<<EOF
#!/usr/bin/expect -f
set arg1 [lindex \$argv 0]
set arg2 [lindex \$argv 1]
spawn /usr/bin/bash
expect {
    "# " {send "gpg --gen-revoke \$arg1 > \$arg2\\r"}
}
expect {
    "Create a revocation certificate for this key? (y/N) " { send "y\\r";}
}
expect {
    "Your decision? " {send "0\\r"}
}
expect {
    "> " {send "\\r"}
}
expect {
    "Is this okay? (y/N) " {send "y\\r"}
}
expect {
    "Passphrase: " {send "mysecretpassphrase\\r"}
}
expect eof
exit
EOF
"""
        self.cmd(cmdline_generate_revocation_certificate_script)
        self.cmd(
            "chmod +x generate_revocation_certificate.exp; " +
            "./generate_revocation_certificate.exp %s %s" %(user, certificate_file))
        _, revoke_certificate = self.cmd("file %s" %certificate_file)
        self.cmd("rm -f generate_revocation_certificate.exp")
        if "PGP public key block Signature" in revoke_certificate:
            return True
        else:
            return False
    
    def revoke_key_with_certificate(self, certificate, host="local", ignore_status=True):
        '''
        Use certificate revocation key
        certificate: Specify revocation certificate
        return Boolean(Ture, False)
        '''

        self.cmd("gpg --import %s" %certificate)
        _, list_keys = self.cmd("gpg --list-secret-keys")
        if  "revoked:" in list_keys:
            return True
        else:
            return False
        
    def make_detached_signature(self, user, testpasswd, signature_file, file, host="local", ignore_status=True):
        '''
        Make detached signature
        user: Username, user email, or keypair id
        testpasswd: The password for the key
        signature_file: Save signed files
        file: Source file
        return Boolean(Ture, False)
        '''

        #gpg --local-user johndoe_001@example.com --batch --yes --pinentry-mode loopback 
        #--passphrase "mysecretpassphrase" --armor --output file.txt.sig --detach-sign file.txt 
        self.cmd(
            "gpg --local-user {_user} --batch --yes --pinentry-mode loopback \
            --passphrase {_passwd} --armor --output {_sig_file} --detach-sign {_file}".format(
                _user = user,
                _passwd = testpasswd,
                _sig_file = signature_file,
                _file = file,
            ), 
            ignore_status=ignore_status
        )
        _, sig_output = self.cmd("file %s" %signature_file)
        if "PGP signature Signature" in sig_output:
            return True
        else:
            return False

    def make_clearsign_signature(self, user, testpasswd, signature_file, file, host="local", ignore_status=True):
        '''
        Make clearsign signature
        user: Username, user email, or keypair id
        testpasswd: The password for the key
        signature_file: Save signed files
        file: Source file
        return Boolean(Ture, False)
        '''

        # gpg --local-user 95DB082E3D0F0E2E5CDA5E312B459F66A03CD415 --batch --yes --pinentry-mode loopback  
        # --passphrase mysecretpassphrase --armor --output file.txt.asc --clearsign file.txt
        self.cmd(
            "gpg --local-user {_user} --batch --yes --pinentry-mode loopback \
            --passphrase {_passwd} --armor --output {_sig_file} --clearsign {_file}".format(
                _user = user,
                _passwd = testpasswd,
                _sig_file = signature_file,
                _file = file,
            ), 
            ignore_status=ignore_status
        )
        _, sig_output = self.cmd("file %s" %signature_file)
        if "PGP signed message" in sig_output:
            return True
        else:
            return False
        
    def make_signature(self, pubkeyid, testpasswd, signature_file, file, host="local", ignore_status=True):
        '''
        Make signature
        pubkeyid: public key id
        testpasswd: The password for the key
        signature_file: Save signed files
        file: Source file
        return Boolean(Ture, False)
        '''

        self.cmd(
            "gpg --local-user {_pubkeyid}! --batch --yes --pinentry-mode loopback \
            --passphrase {_passwd} --armor --output {_sig_file} --sign {_file}".format(
                _pubkeyid = pubkeyid,
                _passwd = testpasswd,
                _sig_file = signature_file,
                _file = file,
            ), 
            ignore_status=ignore_status
        )
        _, sig_output = self.cmd("file %s" %signature_file)
        if "PGP message Compressed" in sig_output:
            return True
        else:
            return False
        
    def sign_other_keys(self, pubkeyid, other_pubkeyid, testpasswd, host="local", ignore_status=True):
        '''
        Authenticate other keys
        pubkeyid: Main public key id
        other_pubkeyid: Authenticated public key ID
        testpasswd: The password for the key
        return tuple(code, sign_output)
        '''

        code, sign_output = self.cmd(
            "gpg --local-user {_pubkeyid}! --batch --yes --pinentry-mode loopback \
            --passphrase {_passwd} --sign-key {_other_pubkeyid}".format(
                _pubkeyid = pubkeyid,
                _passwd = testpasswd,
                _other_pubkeyid = other_pubkeyid,
                 )
        )
        return code, sign_output

    def verify_detached_signature(self, user, signature_file, file, host="local", ignore_status=True):
        '''
        Verify detached signature
        user: Username, user email, or keypair id
        signature_file: Verified file
        file: Source file
        return tuple(code, output)
        '''

        code, verify_detached_signature_output = self.cmd(
            "gpg --local-user %s --batch --yes --verify %s %s" %(user, signature_file, file),
            ignore_status=ignore_status
            )
        return code, verify_detached_signature_output

    def verify_cleartext_signature(self, user, signature_file, host="local", ignore_status=True):
        '''
        Verify cleartext signature
        user: Username, user email, or keypair id
        signature_file: Verified file
        return tuple(code, output)
        '''

        code, verify_cleartext_signature_output = self.cmd(
            "gpg --local-user %s --batch --yes --verify %s" %(user, signature_file),
            ignore_status= ignore_status
            )
        return code, verify_cleartext_signature_output
    
    def symmetric_encryption_messages(self, messages_file, testpasswd, host="local", ignore_status=True):
        '''
        Asynchronous message encryption
        messages_file: Encrypted message file
        testpasswd: The password for the key
        return tuple(code, output)
        '''

        code, symmetric_encryption_messages_output = self.cmd(
            "gpg --batch --yes --passphrase %s --symmetric --cipher-algo AES256 %s" 
            %(testpasswd, messages_file)
            )
        return code, symmetric_encryption_messages_output
    
    def symmetric_decrypt_messages(self, encryption_messages_file, decrypt_messages_file, 
                                   testpasswd, host="local", ignore_status=True):
        '''
        Asynchronous message decryption.
        encryption_messages_file: Specify encrypted message file
        decrypt_messages_file: Save decrypted content
        testpasswd: The password for the key
        return tuple(code, output)
        '''

        code, symmetric_decrypt_messages_output = self.cmd(
            "gpg --batch --yes --passphrase %s --decrypt %s > %s" 
            %(testpasswd, encryption_messages_file, decrypt_messages_file)
            )
        return code, symmetric_decrypt_messages_output
    