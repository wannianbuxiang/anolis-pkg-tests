# -*- encoding: utf-8 -*-

"""
@File:      tc_llvm_llvm_fun001.py
@Time:      2023/1/15 21:57:12
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_llvm_llvm_fun001.yaml for details

    :avocado: tags=P1,noarch,local,llvm
    """
    PARAM_DIC = {"pkg_name": "llvm clang"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("yum install -y llvm")
        self.cmd("touch llvm_test.c")
        self.cmd("echo -e '#include <stdio.h>\nint main() {\nprintf(\"hello llvm\");\nreturn 0;\n}' >> llvm_test.c")

    def test(self):
        self.cmd("clang llvm_test.c -o llvm_test")
        self.cmd("clang -O3 -emit-llvm llvm_test.c -c -o llvm_test.bc")
        code,llvm_result=self.cmd("./llvm_test")
        self.assertIn("hello llvm",llvm_result)
        code,llvm_result=self.cmd("lli llvm_test.bc")
        self.assertIn("hello llvm",llvm_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf llvm_test.c")