# -*- encoding: utf-8 -*-

"""
@File:      tc_dnf_command_deplist_fun001.py
@Time:      2024/06/12 12:36:48
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_dnf_deplist_fun001.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "tree"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        self.cmd("dnf list --available -q | awk '{print $1}' > pkg_list")
        code,result = self.cmd('shuf -n1 pkg_list')
        self.cmd(f'dnf deplist {result}')


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf pkg_list')
