#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_yum_service_repo_func_001.py
@Time:      2022/8/31 10:06:45
@Author:    yangshunfeng.ysf
@Version:   1.0
@Modify:    liuyaqing
@License:   Mulan PSL v2
"""
from common.basetest import RemoteTest


class YumRepoTest(RemoteTest):
    """
    See   tc_yum_service_repo_func_001.yaml for details
    :avocado: tags=fix,P1,noarch,remote,vhd_img
    """
    def setUp(self):
        super().setUp()
        _,ret_o = self.cmd('cat /etc/os-release',container_flag=1)
        if "Ubuntu" in ret_o:
            self.skip("Ubuntu container not support.")

    def test_repo_enable(self):
        curl_err_list = []
        ret_c,output = self.cmd('cat /etc/yum.repos.d/* | grep baseurl | grep -v "#"')
        output1 = output.replace('$releasever',self.image.version).replace('$basearch',self.image.arch)
        for line in output1.split('\n'):
            cmd = line.split('://')[1]
            curl_cmd = 'curl -l -m 10 -o /dev/null -s -w %{http_code} '+cmd
            self.log.debug(curl_cmd)
            ret_c,returncode = self.cmd(curl_cmd)
            # The return code of curl can not be 4xx or 5xx
            if returncode[0] is '4' or  returncode[0] is '5':
                curl_err_list.append(line)
        self.assertEqual(len(curl_err_list),0,'The inaccessable url: {}'.format(curl_err_list))

    def tearDown(self):
        super().tearDown()