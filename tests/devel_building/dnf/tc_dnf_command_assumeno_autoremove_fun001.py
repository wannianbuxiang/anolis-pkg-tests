# -*- encoding: utf-8 -*-

"""
@File:      tc_dnf_command_assumeno_autoremove_fun001.py
@Time:      2024/06/12 12:25:32
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_dnf_command_assumeno_autoremove_fun001.yaml for details

    :avocado: tags=P1,noarch,local,dnf
    """
    PARAM_DIC = {"pkg_name": "dnf"}


    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        self.cmd('dnf --assumeno install tree 2>&1 | grep "Operation aborted"')
        self.cmd('dnf autoremove -y | grep "Complete"')
        self.cmd('dnf -y --nobest install httpd | grep "Complete"')
        self.cmd('dnf makecache | grep "Metadata cache created"')
        self.cmd('dnf -C repoquery kernel | grep "kernel"')
        self.cmd('dnf clean all  | grep "removed"')
        self.cmd('dnf remove tree vim -y')

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
