#!/usr/bin/env python3
#-*- encoding:utf-8 -*-

"""
@File:         tc_yum_yum_fun_001.py
@Time:         2022/07/18 10:46:43
@Author:       yangshunfeng.ysf
@Version:      1.0
@Contact:      lyq01395646@alibaba-inc.com
@License:      Mulan PSL v2
@Modify:      liuyaqing
"""
from common.basetest import RemoteTest

class Test(RemoteTest):
    """
    See   tc_yum_yum_fun_001.yaml for details
    :avocado: tags=fix,P1,noarch,remote,vhd_img,baseos_container_default
    """
    def setUp(self):
      super().setUp()
      _,ret_o = self.cmd('cat /etc/os-release',container_flag=1)
      if "Ubuntu" in ret_o:
          self.skip("Ubuntu container not support.")

    def test(self):
      self.skip_non_root_test()
      self.cmd('yum clean all',container_flag=1)
      self.cmd('yum makecache -y',container_flag=1)
      self.cmd('yum update -y',container_flag=1)

    def tearDown(self):
      super().tearDown()