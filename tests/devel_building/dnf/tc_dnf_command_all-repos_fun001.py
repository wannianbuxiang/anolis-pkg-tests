# -*- encoding: utf-8 -*-

"""
@File:      tc_dnf_command_all-repos_fun001.py
@Time:      2024/06/12 10:35:06
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_dnf_command_all-repos_fun001.yaml for details

    :avocado: tags=P1,noarch,local,dnf
    """
    PARAM_DIC = {"pkg_name": "dnf"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        self.cmd('dnf remove tree vim -y', ignore_status=True)
        ret_c2, ret_o2 = self.cmd("dnf repolist all | grep -i enable | awk '{print $1}'")  
        if ret_c2 == 0:
            repo_list = [line.replace("[stdout] ", "") for line in ret_o2.splitlines()]
            repo_names = '|'.join(repo_list)
            self.log.info(f"Enabled repos: {repo_names}")

        self.cmd('dnf -y  install vim |grep "vim"')
        self.cmd(f'dnf list --installed | grep vim-enhanced | grep -E "{repo_names}"')
        self.cmd('dnf -y  install tree | grep "tree"')
        self.cmd(f'dnf list --installed | grep tree | grep -E "{repo_names}"')     
        self.cmd(f'dnf -y --repo={repo_list[0]} list |grep {repo_list[0]} |wc -l')

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('dnf remove tree vim -y')