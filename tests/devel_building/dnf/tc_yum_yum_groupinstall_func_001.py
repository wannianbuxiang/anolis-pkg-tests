#!/usr/bin/env python3
#-*- encoding:utf-8 -*-

"""
@File:         tc_yum_yum_groupinstall_func_001.py
@Time:         2022/07/18 10:46:43
@Author:       yangshunfeng.ysf
@Version:      1.0
@Contact:      lyq01395646@alibaba-inc.com
@License:      Mulan PSL v2
@Modify:       liuyaqing
"""

from common.basetest import RemoteTest
from common.sysinfo import SysInfo

class YumGroupTest(RemoteTest):
    """
    See   tc_yum_yum_groupinstall_func_001.yaml for details
    :avocado: tags=fix,P1,noarch,remote,baseos_container_default,vhd_img
    """
    def setUp(self):
        super().setUp()
        _,ret_o = self.cmd('cat /etc/os-release',container_flag=1)
        if "Ubuntu" in ret_o:
            self.skip("Ubuntu container not support.")

    def test_yum_devel(self):
        self.skip_non_root_test()
        ret_c, ret_o = self.cmd('cat /etc/os-release | grep ID= | grep -v platform |awk -F "\\\"" "{print \$2}"|xargs',container_flag=1)
        if ret_o == "alinux 3.2304":
            self.cmd('yum install java nodejs golang python3 -y',container_flag=1)
        elif 'anolis 23' in ret_o:
            self.cmd('yum install java nodejs golang python3  maven -y',container_flag=1)
        else:
            self.cmd('yum install java nodejs golang python3 python2 maven -y',container_flag=1)
            
        ret_c,output = self.cmd('yum group list | grep  "Available Groups:" -A 5',container_flag=1)
        group_name = output.split('\n')[-2]
        self.log.debug(group_name.strip())
        if group_name.strip() == "Development Tools":
            group_name = "Development"
        group_inst_cmd = 'yum group install "'+group_name.strip()+'" -y'
        group_rmv_cmd = 'yum group remove "'+group_name.strip()+'" -y'
        self.cmd(group_inst_cmd,container_flag=1)
        self.cmd(group_rmv_cmd,container_flag=1)

    def tearDown(self):
        super().tearDown()