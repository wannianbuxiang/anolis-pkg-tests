# -*- encoding: utf-8 -*-

"""
@File:      tc_dnf_command_alias_fun001.py
@Time:      2024/6/12 09:47:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_dnf_alias_fun001.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "dnf tree"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        self.cmd('dnf alias add rm=remove | grep "rm"')
        self.cmd('dnf alias list | grep rm')
        self.cmd('dnf -y rm tree|grep "tree"')
        code, result1=self.cmd('rpm -q tree|grep "tree"')
        self.assertIn("package tree is not installed", result1)
        self.cmd('dnf alias delete rm | grep "rm"')
        code, result2= self.cmd('dnf alias list | grep -v "rm"')
        self.assertIn("No aliases defined", result2)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
