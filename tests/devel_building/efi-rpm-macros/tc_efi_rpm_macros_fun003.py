# -*- encoding: utf-8 -*-

"""
@File:      tc_efi_rpm_macros_fun003.py
@Time:      2024-04-30 17:53:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
import os
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_efi_rpm_macros_fun003.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "efi-filesystem efi-srpm-macros rpmdevtools"}
    spec_context = r"""
Name:           efi-test-package
Version:        1.0
Release:        1%{?dist}
Summary:        Simple EFI test application
License:        GPL
URL:            http://example.com
Source0:        hello_efi.c
BuildArch:      x86_64

%description
This package is a simple test application for EFI environment.

%prep
# 由于不解压归档文件，%prep 仅清理上一个构建生成的内容
# 并将源文件复制到构建目录。

%build
# 编译程序
gcc -o hello_efi %{SOURCE0}

%install
install -D -m 0755 hello_efi %{buildroot}%{efi_esp_efi}/%{efi_vendor}/hello_efi

%files
%{efi_esp_efi}/%{efi_vendor}/hello_efi

%post
# Post installation script can also perform some EFI related
# diagnostics or configurations if needed.

%changelog
* Wed Mar 16 2022 Your Name <you@example.com> - 1.0-1
- First build of efi-test-package
"""

    file_context = r"""
#include <stdio.h>

int main() {
    printf("Hello, EFI!\n");
    return 0;
}
"""

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.spec_file = "efi-test-package.spec"
        self.test_c_file = "hello_efi.c"
        self.exec_file = "hello_efi"
        self.source_dir = "/root/rpmbuild/SOURCES"
        self.cmd(f"mkdir -p {self.source_dir}")
        self.exec_dir = "/boot/efi/EFI/anolis/"
        cmdline = f"""cat > {self.spec_file} <<EOF
        {self.spec_context}
EOF
"""
        self.cmd(cmdline)
        
        cmdline = f"""cat > {self.source_dir}/{self.test_c_file} <<EOF
        {self.file_context}
EOF
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd(f"rpmbuild -ba {self.spec_file}")
        self.cmd(f"yum remove -y efi-test-package-1.0-1.an23.x86_64", ignore_status=True)
        self.cmd(f"rpm -ivh /root/rpmbuild/RPMS/x86_64/efi-test-package-1.0-1.an23.x86_64.rpm")
        code, result = self.cmd(f"{self.exec_dir}/{self.exec_file}")
        self.assertEqual('Hello, EFI!', result, msg="hello_efi 执行失败")

    def tearDown(self):
        self.cmd(f"rm -rf {self.spec_file} {self.source_dir} {self.exec_dir}/{self.exec_file}")
        self.cmd(f"yum remove -y efi-test-package-1.0-1.an23.x86_64")
        super().tearDown(self.PARAM_DIC)        