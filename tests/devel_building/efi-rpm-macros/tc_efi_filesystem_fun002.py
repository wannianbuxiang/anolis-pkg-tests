# -*- encoding: utf-8 -*-

"""
@File:      tc_efi_rpm_macros_fun001.py
@Time:      2024-04-30 17:53:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

import os
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_efi_rpm_macros_fun001.yaml for details

    :avocado: tags=P0,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "efi-filesystem"}
    
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.expect_list = ['/boot/efi/EFI/BOOT/BOOTX64.EFI', 
                            '/boot/efi/EFI/BOOT/fbx64.efi',
                            '/boot/efi/EFI/anolis/grubx64.efi',
                            '/boot/efi/EFI/anolis/BOOTX64.CSV',
                            '/boot/efi/EFI/anolis/mmx64.efi', 
                            '/boot/efi/EFI/anolis/shim.efi', 
                            '/boot/efi/EFI/anolis/shimx64.efi']        
    def test(self):
        self.log.info("Checking /boot/efi/EFI/ structure...")
        for item in self.expect_list:
            self.assertTrue(os.path.exists(item), msg = f"{item}  is not exist")
                
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)