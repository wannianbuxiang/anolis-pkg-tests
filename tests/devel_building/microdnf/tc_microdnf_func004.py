#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_microdnf_func004.py
@Time:      2024/03/01 18:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""
import subprocess

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_microdnf_func004.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "microdnf"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        self.cmd("microdnf leaves")
        self.cmd("microdnf install -y cloud-utils-growpart")
        code, result = self.cmd("pwd")
        self.log.info(result)
        code, cloud_result1 = self.cmd("microdnf download cloud-utils-growpart")
        self.assertIn("Downloaded", cloud_result1)
        code1, cloud_result2 = self.cmd("microdnf clean all")
        self.assertIn("Complete", cloud_result2)
        self.cmd("rm -rf  %s/cloud-utils-growpart-*" % result)

    def tearDown(self):
        self.cmd("microdnf remove -y cloud-utils-growpart")
        super().tearDown(self.PARAM_DIC)


