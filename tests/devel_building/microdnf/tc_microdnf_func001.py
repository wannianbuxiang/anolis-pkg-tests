#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_microdnf_func001.py
@Time:      2024/03/01 10:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_microdnf_func001.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "microdnf"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        self.cmd("microdnf upgrade -y")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
