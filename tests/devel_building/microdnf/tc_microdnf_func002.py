#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_microdnf_func002.py
@Time:      2024/03/01 11:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_microdnf_func002.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "microdnf"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        ret_c1, ret_o1 = self.cmd("microdnf repolist| wc -l")
        self.count = int(ret_o1) - 1
        ret_c1, ret_o2 = self.cmd("dnf list --installed |grep repo | wc -l")
        self.assertTrue(ret_o2, self.count)
        self.cmd("microdnf repoquery anolis-repos")
        self.cmd("microdnf --disablerepo os update")
        self.cmd("microdnf --enablerepo os update")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
