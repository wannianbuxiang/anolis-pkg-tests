#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_microdnf_func003.py
@Time:      2024/03/01 13:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_microdnf_func003.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "microdnf"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        self.cmd("microdnf install -y cloud-init")
        code, cloud_result = self.cmd("cloud-init status")
        self.assertIn("status", cloud_result)
        code1, cloud_result1 = self.cmd("microdnf remove -y cloud-init")
        self.assertIn("Removing", cloud_result1)
        code2, cloud_result2 = self.cmd("cloud-init status",  ignore_status=True)
        self.assertIn("not found", cloud_result2)
        code3, cloud_result3 = self.cmd("microdnf install -y cloud-init")
        self.assertIn("Installing", cloud_result3)
        code4, cloud_result4 = self.cmd("microdnf reinstall -y cloud-init")
        self.assertIn("Reinstalling", cloud_result4)
        code, cloud_result3 = self.cmd("microdnf reinstall -y cloud-init12",  ignore_status=True)   
        self.assertIn("was empty", cloud_result3)


    def tearDown(self):
        self.cmd("microdnf remove -y cloud-init")
        super().tearDown(self.PARAM_DIC)
