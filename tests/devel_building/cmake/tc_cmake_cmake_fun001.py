# -*- encoding: utf-8 -*-

"""
@File:      tc_cmake_cmake_fun001.py
@Time:      Wed Nov 08 2023 18:01:35
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_cmake_cmake_fun001.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "cmake gcc-c++"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir cmake_test")
        cmdline='''cat > cmake_test/main.c <<EOF
#include <stdio.h>
int main(void)
{
printf("Hello World");
return 0;
}
EOF
'''
        self.cmd(cmdline)
        cmdline='''cat > cmake_test/CMakeLists.txt <<EOF
# CMake 最低版本号要求
cmake_minimum_required(VERSION 2.8)
# 项目信息
project (demo)
# 指定生成目标
add_executable(main main.c)
EOF
'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("cmake --version")
        self.cmd("cd cmake_test;cmake ./")
        self.cmd("cd cmake_test;make")
        code,cmake_result=self.cmd("cd cmake_test;./main")
        self.assertIn("Hello World",cmake_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf cmake_test")