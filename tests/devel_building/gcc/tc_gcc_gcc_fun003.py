# -*- encoding: utf-8 -*-

"""
@File:      tc_gcc_gcc_fun003.py
@Time:      2023/1/13 14:36:12
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_gcc_gcc_fun003.yaml for details

    :avocado: tags=P1,noarch,local,gcc
    """
    PARAM_DIC = {"pkg_name": "gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("yum install -y gcc")
        cmdline = """cat >main.c<<EOF
#include<stdio.h>
int main(void)
{
// Print the string
int i;
printf("The Geek Stuff [%d]", i);
return 0;
}
EOF"""
        self.cmd(cmdline)
        cmdline = """cat >main1.c<<EOF
#include<stdio.h>
int main(void)
{
char c = -10;
// Print the string
printf("The Geek Stuff [%d]", c);
return 0;
}
EOF"""
        self.cmd(cmdline)
        cmdline = """cat >main2.c<<EOF
#include<stdio.h>
int main(void)
{
#ifdef MY_MACRO
printf("Macro defined");
#endif
char c = -10;
// Print the string
printf("The Geek Stuff [%d]", c);
return 0;
}
EOF"""
        self.cmd(cmdline)
        self.cmd("echo '-Wall -o main3' > opt_file")

    def test(self):
        self.cmd("gcc -Wall main.c -o main &>gcc_test.log",ignore_status=True)
        code,gcc_result=self.cmd("cat gcc_test.log")
        self.assertIn("warning",gcc_result)
        self.cmd("gcc -Wall -Werror main.c -o main &>gcc_test.log",ignore_status=True)
        code,gcc_result=self.cmd("cat gcc_test.log")
        self.assertIn("error",gcc_result)
        self.cmd("gcc -save-temps main.c")
        code,gcc_result=self.cmd("ls -l")
        self.assertIn("a.out",gcc_result)
        self.assertIn("main.c",gcc_result)
        self.assertIn("main.i",gcc_result)
        self.assertIn("main.o",gcc_result)
        self.assertIn("main.s",gcc_result)
        self.cmd("gcc -Wall -v main.c -o main")
        code,gcc_result=self.cmd("gcc -Wall -ansi main.c -o main",ignore_status=True)
        self.assertIn("not allowed in ISO C90",gcc_result)
        code,gcc_result=self.cmd("gcc -Wall -funsigned-char main1.c -o main1;./main1")
        self.assertIn("The Geek Stuff [246]",gcc_result)
        code,gcc_result=self.cmd("gcc -Wall -fsigned-char main1.c -o main1;./main1")
        self.assertIn("The Geek Stuff [-10]",gcc_result)
        code,gcc_result=self.cmd("gcc -Wall -DMY_MACRO main2.c -o main2;./main2")
        self.assertIn("Macro defined",gcc_result)
        self.assertIn("The Geek Stuff [-10]",gcc_result)
        self.cmd("gcc main.c @opt_file")
        code,gcc_result=self.cmd("ls -l")
        self.assertIn("main3",gcc_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf main.c main1.c main2.c opt_file a.out main.c main.i main.o main.s main3 gcc_test.log")