# -*- encoding: utf-8 -*-

"""
@File:      tc_gcc_gcc_fun004.py
@Time:      2023/7/5 17:36:12
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_gcc_gcc_fun004.yaml for details

    :avocado: tags=P1,noarch,local,gcc
    """
    PARAM_DIC = {"pkg_name": "gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline='''cat >gcc_testfile.c<<EOF
#include <stdio.h>
int main()
{
printf("Hello World!");
return 0;
}
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -S -o gcc_testfile.s gcc_testfile.c")
        self.cmd("gcc -c -o gcc_testfile.o gcc_testfile.s")
        self.cmd("gcc -o m=n32  gcc_testfile.c")
        code,gcc_result=self.cmd("file gcc_testfile.o")
        self.assertIn("relocatable",gcc_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf gcc_testfile.c gcc_testfile.o gcc_testfile.s")