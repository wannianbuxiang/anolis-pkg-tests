# -*- encoding: utf-8 -*-

"""
@File:      tc_gcc_gcc_fun002.py
@Time:      2023/1/13 15:06:12
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_gcc_gcc_fun002.yaml for details

    :avocado: tags=P1,noarch,local,gcc
    """
    PARAM_DIC = {"pkg_name": "gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("yum install -y gcc")

    def test(self):
        self.cmd("touch gcc_test.c")
        self.cmd("echo -e '#include <stdio.h>\nint main()\n{\nprintf(\"hello world!\");\nreturn 0;\n}' >> gcc_test.c")
        self.cmd("gcc gcc_test.c")
        code,gcc_result=self.cmd("./a.out")
        self.assertEquals("hello world!",gcc_result)
        self.cmd("gcc gcc_test.c -o gcc_test")
        code,gcc_result=self.cmd("./gcc_test")
        self.assertEquals("hello world!",gcc_result)
        self.cmd("gcc -E gcc_test.c -o gcc_test.i")
        self.cmd("gcc -S gcc_test.i")
        self.cmd("gcc -c gcc_test.s")
        self.cmd("gcc gcc_test.o -o gcc_test1")
        code,gcc_result=self.cmd("./gcc_test1")
        self.assertEquals("hello world!",gcc_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf gcc_test.c a.out gcc_test gcc_test1 gcc_test.i gcc_test.s gcc_test.0")