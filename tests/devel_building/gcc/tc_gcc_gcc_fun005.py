# -*- encoding: utf-8 -*-

"""
@File:      tc_gcc_gcc_fun006.py
@Time:      2023/7/5 10:09:12
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_gcc_gcc_fun005.yaml for details

    :avocado: tags=P1,noarch,local,gcc
    """
    PARAM_DIC = {"pkg_name": "gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline='''cat >hello.c<<EOF
#include <stdio.h>
int main()
{
printf("Hello World!");
return 0;
}
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -c -o hello.o hello.c")
        self.cmd("gcc -o hello hello.o")
        self.cmd("file hello")
        code,gcc_result=self.cmd("./hello")
        self.assertIn("Hello World!",gcc_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf hello.o hello.c hello")