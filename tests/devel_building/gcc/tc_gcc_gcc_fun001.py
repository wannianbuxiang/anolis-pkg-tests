# -*- encoding: utf-8 -*-

"""
@File:      tc_gcc_gcc_fun001.py
@Time:      2023/1/13 14:36:12
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_gcc_gcc_fun001.yaml for details

    :avocado: tags=P1,noarch,local,gcc
    """
    PARAM_DIC = {"pkg_name": "gcc-c++"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        self.cmd("mkdir test person")
        self.cmd("touch test/main.cpp person/person.h person/person.cpp")
        self.cmd("echo -e '#include <iostream>\n#include \"person.h\"\nusing namespace std;\nint main(int argc,char* argv[])\n{\nperson p;\ncout<<\"test g++\"<<endl;\nreturn 0;\n}' >> test/main.cpp")
        self.cmd("echo -e 'class person\n{\npublic:\nperson();\n};' >> person/person.h")
        self.cmd("echo -e '#include \"person.h\"\n#include \"stdio.h\"\nperson::person()\n{\nprintf(\"create person\");\n}' >> person/person.cpp")
        self.cmd("cd person;gcc person.cpp -fpic -shared -o libperson.so")
        self.cmd("cd test;g++ main.cpp -o main -I../person -L../person -lperson")
        self.cmd("cd test;export LD_LIBRARY_PATH=../person;./main")
        code,gcc_result=self.cmd("cd test;ldd main")
        self.assertIn("libperson",gcc_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf test person")