# -*- encoding: utf-8 -*-

"""
@File:      tc_gcc_gcc_fun006.py
@Time:      2023/7/6 10:27:12
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_gcc_gcc_fun006.yaml for details

    :avocado: tags=P1,noarch,local,gcc
    """
    PARAM_DIC = {"pkg_name": "gcc zlib zlib-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline='''cat >gcc_testfile.c<<EOF
#include <stdio.h>
#include <zlib.h>
int main()
{
/* 原始数据 */
unsigned char strSrc[] = "hello world! aaaaa bbbbb ccccc ddddd 中文测试 yes";
unsigned char buf[1024] = {0};
unsigned char strDst[1024] = {0};
unsigned long srcLen = sizeof(strSrc);
unsigned long bufLen = sizeof(buf);
unsigned long dstLen = sizeof(strDst);
printf("Src string:%sLength:%ld", strSrc, srcLen);
/* 压缩 */
compress(buf, &bufLen, strSrc, srcLen);
printf("After Compressed Length:%ld", bufLen);
/* 解压缩 */
uncompress(strDst, &dstLen, buf, bufLen);
printf("After UnCompressed Length:%ld",dstLen);
printf("UnCompressed String:%s",strDst);
return 0;
}
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -Wall -o gcc_testfile gcc_testfile.c -lz")
        code,gcc_result=self.cmd("./gcc_testfile")
        self.assertIn("hello world! aaaaa bbbbb ccccc ddddd 中文测试 yes",gcc_result)
        self.assertIn("Length:54",gcc_result)
        self.assertIn("After Compressed Length:53",gcc_result)
        self.assertIn("After UnCompressed Length:54",gcc_result)
        self.assertIn("UnCompressed String:hello world! aaaaa bbbbb ccccc ddddd 中文测试 yes",gcc_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf gcc_testfile.c gcc_testfile")