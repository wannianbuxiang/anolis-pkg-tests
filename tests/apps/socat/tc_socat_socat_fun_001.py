#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

"""
@File:      tc_socat_socat_fun_001.py
@Time:      2024/08/02 14:07:20
@Author:    wangge
@Version:   1.0
@Contact:   YS.wangge@h3c.com
@License:   Mulan PSL v2
@Modify:    wangge
"""
import subprocess
import time
import os
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_socat_socat_fun_001.yaml for details

    :avocado: tags=P0,noarch,local
    """
    PARAM_DIC = {"pkg_name": "socat"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        subprocess.run(["echo 'hello world' > a.txt"], shell=True, check=True)
        

    def test(self):

        version = subprocess.check_output("rpm -q socat --queryformat '%{version}\n'", shell=True, text=True).strip()
        result = subprocess.run(f"socat -V | grep 'socat version {version}'", shell=True)
        self.assertEqual(result.returncode, 0, "Check socat -V failed")

        result = subprocess.run("socat -h | grep -Pz 'Usage:[\\S\\s]*socat \\[options\\]'", shell=True)
        self.assertEqual(result.returncode, 0, "Check socat -h failed")

        result = subprocess.run("socat -hh | grep -Pz 'Usage:[\\S\\s]*socat \\[options\\]'", shell=True)
        self.assertEqual(result.returncode, 0, "Check socat -hh failed")

        result = subprocess.run("socat -hhh | grep -Pz 'Usage:[\\S\\s]*socat \\[options\\]'", shell=True)
        self.assertEqual(result.returncode, 0, "Check socat -hhh failed")

        subprocess.Popen("socat -u open:a.txt tcp-listen:4000,reuseaddr", shell=True)
        time.sleep(5)
        result = subprocess.run("socat -b 8192 tcp:127.0.0.1:4000 open:a.txt,create", shell=True)
        self.assertEqual(result.returncode, 0, "Check socat -b failed")

        subprocess.Popen("socat -u open:a.txt tcp-listen:4001,reuseaddr", shell=True)
        time.sleep(5)
        result = subprocess.run("socat -4 tcp:127.0.0.1:4001 open:a.txt,create", shell=True)
        self.assertEqual(result.returncode, 0, "Check socat -4 failed")

        subprocess.Popen("socat -u open:a.txt tcp-listen:4002,reuseaddr", shell=True)
        time.sleep(5)
        result = subprocess.run("socat -6 tcp:127.0.0.1:4002 open:a.txt,create", shell=True)
        self.assertEqual(result.returncode, 0, "Check socat -6 failed")

        subprocess.Popen("socat -U open:a.txt tcp-listen:4003,reuseaddr", shell=True)
        time.sleep(5)
        result = subprocess.run("socat -U tcp:127.0.0.1:4003 open:a.txt,create", shell=True)
        self.assertEqual(result.returncode, 0, "Check socat -U failed")

        subprocess.Popen("socat -g open:a.txt tcp-listen:4004,reuseaddr", shell=True)
        time.sleep(5)
        result = subprocess.run("socat -g tcp:127.0.0.1:4004 open:a.txt,create", shell=True)
        self.assertEqual(result.returncode, 0, "Check socat -g failed")
        


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        ps = subprocess.Popen(['ps', '-ef'], stdout=subprocess.PIPE)
        grep1 = subprocess.Popen(['grep', 'socat .* open'], stdin=ps.stdout, stdout=subprocess.PIPE)
        ps.stdout.close()
        grep2 = subprocess.Popen(['grep', '-v', 'grep'], stdin=grep1.stdout, stdout=subprocess.PIPE)
        grep1.stdout.close()
        output, _ = grep2.communicate()
        output = output.decode('utf-8')
        pids = [int(line.split()[1]) for line in output.strip().split('\n') if line]
        print(pids)
        for pid in pids:
                os.kill(pid, signal.SIGKILL) #关闭上面开启的socat进程
        subprocess.run(["rm -rf *.txt"],shell=True,check=True)
        
    

 