#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_cloud_utils_growpart_func002.py
@Time:      2024/02/27 10:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

import subprocess
from common.basetest import LocalTest
from common.disk import DiskManager

class Test(LocalTest):
    """
    See tc_cloud_utils_growpart_func002.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "cloud-utils-growpart"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        # 查看节点上磁盘并分区
        self.disks = DiskManager.get_data_disk(self)
        if self.disks is None:
            self.skip('There is no data disk for test.')
        self.test_device = "/dev/" + self.disks
        ret_c1, ret_o1 = self.cmd("echo -e 'n\np\n1\n2048\n2048\nw' | fdisk  %s" % self.test_device)
        self.assertTrue(ret_o1,  self.test_device+"p1")   

        #扩容     
        cloud_result = subprocess.run("growpart %s 1" % self.test_device, capture_output=True, text=True, shell=True)  
        self.log.info(cloud_result.args)
        self.log.info(cloud_result.stdout)
        message2 = "CHANGED"
        self.assertIn(message2, cloud_result.stdout, "Should contain %s" % message2)
        self.cmd("partprobe %s " % self.test_device)
        cloud_result2 = subprocess.run("growpart -N %s 1" % self.test_device, capture_output=True, shell=True, text=True)
        self.log.info(cloud_result2.args)
        self.log.info(cloud_result2.stdout)
        self.assertIn("it cannot be grown", cloud_result2.stdout)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("echo -e 'd\n1\nw' | fdisk  %s" % self.test_device)


