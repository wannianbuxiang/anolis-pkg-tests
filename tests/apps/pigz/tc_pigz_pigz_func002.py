#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_pigz_pigz_func002.py
@Time:      2024/03/12 14:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""
import subprocess

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_pigz_pigz_func002.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "pigz"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        #创建文件
        self.cmd("mkdir /tmp/gaohongmei/")
        self.cmd("dd if=/dev/urandom of=/tmp/gaohongmei/testpigz001.txt bs=512M count=1")
        self.cmd("dd if=/dev/urandom of=/tmp/gaohongmei/testpigz002.sh bs=512M count=1")
        self.cmd("dd if=/dev/urandom of=/tmp/gaohongmei/testpigz003.py bs=512M count=1")
        self.cmd("dd if=/dev/urandom of=/tmp/gaohongmei/testpigz004.c bs=512M count=1")

    def test(self):
        #压缩文件
        ret_c1, ret_o1 = self.cmd("md5sum /tmp/gaohongmei/testpigz001.txt")
        self.cmd("pigz /tmp/gaohongmei/testpigz001.txt")
        self.cmd("pigz -l /tmp/gaohongmei/testpigz001.txt.gz")     
        self.cmd("pigz -d /tmp/gaohongmei/testpigz001.txt.gz")  
        ret_c2, ret_o2 = self.cmd("md5sum /tmp/gaohongmei/testpigz001.txt")
        # confirm file
        if ret_o1 == ret_o2:
            self.log.info('test successfully.')
        else:
            self.log.info('test failed.')       
        #批量压缩
        self.cmd("pigz /tmp/gaohongmei/*")  
        ret_c3, ret_o3 = self.cmd("ls -l /tmp/gaohongmei/ | grep -c '\.gz'")  
        # confirm file
        if int(ret_o3) == 4:
            self.log.info('/tmp/gaohongmei/* file compressed.')
        else:
            self.log.info('Compression failed.')
        self.cmd("pigz -d /tmp/gaohongmei/*")  
        cloud_result4 = subprocess.run("ls -l /tmp/gaohongmei/ | grep -c '\.gz'", capture_output=True, text=True, shell=True)
        # confirm file
        if int(cloud_result4.stdout) == 0:
            self.log.info('/tmp/gaohongmei/*  file has been decompressed.')
        else:
            self.log.info('Decompression failed.')

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/gaohongmei/")