#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_pigz_pigz_func004.py
@Time:      2024/03/12 17:42:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""
import subprocess

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_pigz_pigz_func004.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "pigz"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        #创建文件
        self.cmd("mkdir /tmp/gaohongmei/")
        self.cmd("dd if=/dev/urandom of=/tmp/gaohongmei/testpigz001.txt bs=256M count=1")
        self.cmd("dd if=/dev/urandom of=/tmp/gaohongmei/testpigz002.txt bs=256M count=1")

    def test(self):
        self.cmd("(time pigz /tmp/gaohongmei/testpigz001.txt) 2> time_output1.txt")
        real_time1 = self.cmd( "cat time_output1.txt | awk '/real/{print $2}'")
        time1_seconds = convert_time_to_seconds(real_time1)
        self.cmd("(time gzip /tmp/gaohongmei/testpigz002.txt) 2> time_output2.txt")
        real_time2 = self.cmd( "cat time_output2.txt | awk '/real/{print $2}'")
        time2_seconds = convert_time_to_seconds(real_time2)
        self.assertLess(real_time1, real_time2)
        if time1_seconds < time2_seconds:
            self.log.info("pigz is faster than gzip")
        else:
            self.log.info("gzip is faster than pigz")
        self.cmd("ls -l /tmp/gaohongmei/")
        ret_c3, ret_o3 = self.cmd("du -h /tmp/gaohongmei/testpigz001.txt.gz | awk '{print $1}'")
        ret_c4, ret_o4 = self.cmd("du -h /tmp/gaohongmei/testpigz002.txt.gz | awk '{print $1}'")
        self.assertEqual(ret_o3, ret_o4)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/gaohongmei/  time_output1.txt time_output2.txt ")

def convert_time_to_seconds(time_str):
    minutes, seconds = time_str[1].split('m')
    seconds = float(seconds.rstrip('s'))
    if minutes != '0':
        minutes = float(minutes)
        seconds += minutes * 60
    return seconds