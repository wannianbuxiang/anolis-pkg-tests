#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_pigz_pigz_func003.py
@Time:      2024/03/12 15:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_pigz_pigz_func003.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "pigz"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        #创建文件
        self.cmd("mkdir /tmp/gaohongmei/")
        self.cmd("echo -e 'hello world!' > /tmp/gaohongmei/testpigz001.txt")
        self.cmd("echo -e 'hello world!' > /tmp/gaohongmei/testpigz002.sh")
        self.cmd("echo -e 'hello world!' > /tmp/gaohongmei/testpigz003.py")

    def test(self):
        self.cmd("tar cf - /tmp/gaohongmei/ | pigz > /tmp/gaohongmei.tar.gz")
        self.cmd("rm -rf /tmp/gaohongmei/")
        ret_c1, ret_o1 = self.cmd("ls -l /tmp/gaohongmei.tar.gz")  
        if ret_c1 == 0:
            self.log.info('Compression successful.')
        else:
            self.log.info('Compression failed.')
        self.cmd("pigz -dc /tmp/gaohongmei.tar.gz | tar xf -")
        ret_c2, ret_o2 = self.cmd("ls -l tmp/gaohongmei/ | awk '{print $9}'")  
        if ret_c2 == 0:
            file_list = [line.replace("[stdout] ", "") for line in ret_o2.splitlines()]
            for file_name in file_list:
                ret_c3, ret_o3 = self.cmd("cat tmp/gaohongmei/%s" % file_name)
                self.assertEqual('hello world!', ret_o3)
        else:
            self.log.info('Decompression failed.')

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf tmp/gaohongmei/testpigz00* /tmp/gaohongmei.tar.gz")
        self.cmd("rmdir -p tmp/gaohongmei")