#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_pigz_pigz_func001.py
@Time:      2024/03/12 13:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_pigz_pigz_func001.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "pigz"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        #创建文件
        self.cmd("echo -e 'hello world!' > /tmp/testpigz001.txt")
        self.cmd("echo -e 'hello world!' > /tmp/testpigz002.sh")
        self.cmd("echo -e 'hello world!' > /tmp/testpigz003.py")
        self.cmd("echo -e 'hello world!' > /tmp/testpigz004.c")

    def test(self):
        #压缩文件
        self.log.info("The current version is")
        self.cmd("pigz -V")
        self.cmd("pigz /tmp/testpigz001.txt")     
        self.cmd("pigz -9 /tmp/testpigz002.sh")  
        self.cmd("pigz -1 /tmp/testpigz003.py")  
        self.cmd("pigz -p4 /tmp/testpigz004.c")  

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/testpigz00*")