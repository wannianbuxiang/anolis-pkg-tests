#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_userspace_rcu_func001.py
@Time:      2024/03/27 11:01:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_userspace_rcu_func001.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "userspace-rcu userspace-rcu-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_userspace_rcu.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <urcu.h>
#include <unistd.h>

pthread_mutex_t read_thread_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t read_thread_cond = PTHREAD_COND_INITIALIZER;

struct list_node {
    int value;
    struct list_node *next;
    struct rcu_head rcu;
};

static struct list_node *head = NULL;
static bool stop_threads = false;

struct list_node *alloc_list_node(int val) {
    struct list_node *new_node = malloc(sizeof(*new_node));
    if (!new_node) {
        perror("malloc");
        exit(EXIT_FAILURE);
    }
    new_node->value = val;
    new_node->next = NULL;
    return new_node;
}

void free_list_node(struct rcu_head *head) {
    struct list_node *node = caa_container_of(head, struct list_node, rcu);
    free(node);
}

void insert_to_list(struct list_node **list_head, int value) {
    struct list_node *new_node = alloc_list_node(value);
    new_node->next = *list_head;
    *list_head = new_node;
}

void delete_from_list_rcu(struct list_node **list_head, int value) {
    struct list_node **prev = list_head;
    while (*prev && (*prev)->value != value)
        prev = &(*prev)->next;

    if (*prev) {
        struct list_node *to_delete = *prev;
        *prev = to_delete->next;

        call_rcu(&to_delete->rcu, free_list_node);
    }
}

void *reader_thread(void *arg) {
    while (!stop_threads) {
        rcu_read_lock();
        for (struct list_node *iter = head; iter; iter = iter->next) {
            printf("Value: %d\\n", iter->value);
        }
        rcu_read_unlock();

        pthread_mutex_lock(&read_thread_mutex);
        while (!stop_threads) { // 在循环内部判断stop_threads,并在外部释放锁
            pthread_cond_wait(&read_thread_cond, &read_thread_mutex);
        } 
        pthread_mutex_unlock(&read_thread_mutex);       
    }
    pthread_mutex_unlock(&read_thread_mutex);  
    return NULL;
}

void *writer_thread(void *arg) {
    int i = 0;
    const int max_iterations = 10; //设定最大迭代次数
    while (!stop_threads && i < max_iterations) {
        insert_to_list(&head, i++);
        delete_from_list_rcu(&head, i - 2); // 删除前两个插入的节点

        // 提交更改以便读者线程可见
        synchronize_rcu();

        // 添加适当的休眠或循环控制以模拟并发写入
        sleep(1);
    }
    return NULL;
}

int main() {
    rcu_init();
    pthread_t reader_tid, writer_tid;

    pthread_create(&reader_tid, NULL, reader_thread, NULL);
    pthread_create(&writer_tid, NULL, writer_thread, NULL);

    sleep(5);
    stop_threads = true;
    // 唤醒读线程
    pthread_mutex_lock(&read_thread_mutex);
    pthread_cond_signal(&read_thread_cond); 
    pthread_mutex_unlock(&read_thread_mutex);

    pthread_join(reader_tid, NULL);
    pthread_join(writer_tid, NULL);

    // 等待所有RCU回调完成
    synchronize_rcu();
    printf("userspace-rcu test succeeded.\\n");
    return 0;
}
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o /tmp/ghm/test_userspace_rcu /tmp/ghm/test_userspace_rcu.c -lpthread -lurcu -std=c99 -Wall")
        ret_c, ret_o = self.cmd("/tmp/ghm/test_userspace_rcu")
        self.assertIn("succeeded", ret_o)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")