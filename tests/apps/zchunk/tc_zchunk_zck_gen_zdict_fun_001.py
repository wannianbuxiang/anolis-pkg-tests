#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_zchunk_zck_gen_zdict_fun_001.py 
@Time:      2024/04/12 14:12
@Author:    zhanggaiyan
@Version:   1.0
@Contact:   zhanggaiyan@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhanggaiyan
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_zchunk_zck_gen_zdict_fun_001.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "zchunk"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("dd if=/dev/zero of=mywords.txt bs=1M count=128")
        self.cmd("mkdir chunks")
        self.cmd("dd if=/dev/zero of=chunks/la_words.txt bs=1M count=512")

    def test(self):
        self.cmd("zck mywords.txt")
        self.cmd("ls -l mywords.txt.zck")
        self.cmd("zck_gen_zdict mywords.txt.zck")
        self.cmd("ls -l mywords.txt.zdict")
        self.cmd("zck chunks/la_words.txt")
        self.cmd("ls -l la_words.txt.zck")
        self.cmd("zck_gen_zdict -d chunks/ la_words.txt.zck")
        self.cmd("ls -l la_words.txt.zdict")
        self.cmd("ls -l chunks |grep txt")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -fr mywords.txt* chunks la_words.txt*")
