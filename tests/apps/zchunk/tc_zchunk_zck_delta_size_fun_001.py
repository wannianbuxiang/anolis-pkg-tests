#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_zchunk_zck_delta_size_fun_001.py 
@Time:      2024/04/12 14:12
@Author:    zhanggaiyan
@Version:   1.0
@Contact:   zhanggaiyan@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhanggaiyan
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_zchunk_zck_delta_size_fun_001.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "zchunk"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("echo '12345' > word1.txt")
        self.cmd("echo '678910' > word2.txt")

    def test(self):
        self.cmd("zck -h sha256 word1.txt")
        self.cmd("ls -l word1.txt.zck")
        self.cmd("zck -h sha256 word2.txt")
        self.cmd("ls -l word2.txt.zck")
        ret_c, ret_o = self.cmd("zck_delta_size word1.txt.zck word2.txt.zck")
        self.assertTrue("Would download 161 of 161 bytes" in ret_o, 'check output error')
        self.assertTrue("Matched 1 of 2 chunks" in ret_o, 'check output error')
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -f word1.txt word1.txt.zck word2.txt word2.txt.zck")
