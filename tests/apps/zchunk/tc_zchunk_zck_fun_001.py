#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_zchunk_zck_fun_001.py
@Time:      2024/04/12 14:12
@Author:    zhanggaiyan
@Version:   1.0
@Contact:   zhanggaiyan@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhanggaiyan
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_zchunk_zck_fun_001.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "zchunk"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("echo '12345' > /usr/share/dict/words")

    def test(self):
        self.cmd("zck /usr/share/dict/words")
        self.cmd("ls -l words.zck")
        self.cmd("zck -o /tmp/words.txt.zck /usr/share/dict/words")
        self.cmd("ls -l /tmp/words.txt.zck")
        self.cmd("rm -f words.zck;zck -h sha256 /usr/share/dict/words")
        ret_c, ret_o = self.cmd("zck_read_header words.zck")
        self.assertTrue("Chunk checksum type: SHA-256" in ret_o, 'check output error')
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -f /usr/share/dict/words words.zck /tmp/words.txt.zck")
