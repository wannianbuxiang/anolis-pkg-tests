#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_zchunk_zck_read_header_fun_001.py 
@Time:      2024/04/12 14:12
@Author:    zhanggaiyan
@Version:   1.0
@Contact:   zhanggaiyan@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhanggaiyan
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_zchunk_zck_read_header_fun_001.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "zchunk"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("dd if=/dev/zero of=/tmp/head_words.txt bs=1M count=128")

    def test(self):
        self.cmd("zck /tmp/head_words.txt")
        self.cmd("ls -l head_words.txt.zck")
        ret_c ,ret_o = self.cmd("zck_read_header -f head_words.txt.zck")
        self.assertTrue("Header checksum" in ret_o , "check output error")
        self.assertTrue("Header size" in ret_o , "check output error")
        self.assertTrue("Data size" in ret_o , "check output error")
        self.assertTrue("Chunk count" in ret_o , "check output error")
        ret_c ,ret_o = self.cmd("zck_read_header -c head_words.txt.zck")
        self.assertTrue("Chunk Checksum" in ret_o , "check output error")
        self.assertTrue("Start" in ret_o , "check output error")
        self.assertTrue("Comp size" in ret_o , "check output error")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -f /tmp/head_words.txt head_words.txt.zck")
