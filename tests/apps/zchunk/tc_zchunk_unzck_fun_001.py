#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_zchunk_unzck_fun_001.py
@Time:      2024/04/12 14:12
@Author:    zhanggaiyan
@Version:   1.0
@Contact:   zhanggaiyan@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhanggaiyan
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_zchunk_unzck_fun_001.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "zchunk"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("echo 'abcde' > /usr/share/dict/words")

    def test(self):
        self.cmd("zck /usr/share/dict/words")
        self.cmd("ls -l words.zck")
        self.cmd("unzck words.zck")
        self.cmd("ls -l words")
        self.cmd("rm -f words")
        ret_c, ret_o = self.cmd("unzck -c words.zck")
        self.assertTrue("abcde" in ret_o , "check output error")
        ret_c, ret_o = self.cmd("ls -l words", ignore_status=True)
        self.assertFalse("words" not in ret_o , "check words not exist,as expect")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -f /usr/share/dict/words words.zck")
