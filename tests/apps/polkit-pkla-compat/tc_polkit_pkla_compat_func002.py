#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_polkit_pkla_compat_func002.py
@Time:      2024/05/06 10:11:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_polkit_pkla_compat_func002.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "polkit-pkla-compat"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > /etc/polkit-1/localauthority/50-local.d/allow_testuser_timedatectl.pkla <<EOF	
[Allow testuser to query system time]
Identity=unix-user:testuser
Action=org.freedesktop.timedate.time
ResultActive=yes
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("useradd testuser")
        self.cmd("chmod +755 /etc/polkit-1/localauthority/50-local.d/allow_testuser_timedatectl.pkla")
        self.cmd("systemctl restart polkit.service")
        self.cmd("sleep 2")
        ret_c, ret_o = self.cmd("sudo -u testuser timedatectl status")
        self.log.info(ret_o)
        if ret_c == 0:
            self.log.info("timedatectl status executed successfully.")        
            self.assertIn("Universal time", ret_o)
            self.assertIn("Time zone", ret_o)   
            self.assertIn("System clock synchronized", ret_o)
            self.assertIn("NTP service", ret_o)  
        else:
            self.log.info("Error executing timedatectl status. Exit Code: {ret_c}")
            self.log.info("Error Output: {ret_o}")     

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)  
        self.cmd("pgrep -u testuser | xargs kill -9", ignore_status=True)
        self.cmd("sleep 2")
        self.cmd("userdel -r testuser")
        self.cmd("rm -rf /etc/polkit-1/localauthority/50-local.d/allow_testuser_timedatectl.pkla")
