#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_polkit_pkla_compat_func001.py
@Time:      2024/04/29 14:11:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_polkit_pkla_compat_func001.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "polkit-pkla-compat cloud-init"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > /etc/polkit-1/localauthority/50-local.d/allow_ghm_cloud-init.pkla <<EOF	
[Allow ghm to run cloud-init status]
Identity=unix-user:ghm
Action=org.freedesktop.cloud-init.status
ResultActive=yes
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("useradd ghm")
        self.cmd("chmod +755 /etc/polkit-1/localauthority/50-local.d/allow_ghm_cloud-init.pkla")
        self.cmd("systemctl restart polkit.service")
        self.cmd("sleep 2")
        ret, ret_dir = self.cmd("ls -l /etc/cloud/cloud.cfg.d/aliyun_cloud.cfg | awk  '{print $11}'")
        self.cmd(f"chmod 644 {ret_dir}")
        ret_c, ret_o = self.cmd("sudo -u ghm cloud-init status")
        self.log.info(ret_o)
        if ret_c == 0:
            self.log.info("cloud-init status executed successfully.")           
        else:
            self.log.info(f"Error executing cloud-init status. Exit Code: {ret_c}")
            self.log.info(f"Error Output: {ret_o}")     

    def tearDown(self):
        super().tearDown(self.PARAM_DIC) 
        self.cmd("sleep 2") 
        self.cmd("pgrep -u ghm | xargs kill -9", ignore_status=True)
        self.cmd("userdel -r ghm")
        self.cmd("rm -rf /etc/polkit-1/localauthority/50-local.d/allow_ghm_cloud-init.pkla")
