#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_polkit_pkla_compat_func003.py
@Time:      2024/05/06 14:11:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_polkit_pkla_compat_func003.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "polkit-pkla-compat"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > /etc/polkit-1/localauthority/50-local.d/allow_syslog_viewer_journalctl.pkla <<EOF	
[Allow syslog_viewer to read system journal]
Identity=unix-user:syslog_viewer
Action=org.freedesktop.systemd1.manage-journal
ResultAny=no
ResultInactive=no
ResultActive=yes
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("useradd syslog_viewer")
        self.cmd("chmod +755 /etc/polkit-1/localauthority/50-local.d/allow_syslog_viewer_journalctl.pkla")
        self.cmd("systemctl restart polkit.service")
        self.cmd("sleep 2")
        self.cmd("usermod -aG systemd-journal syslog_viewer")
        ret_c, ret_o = self.cmd("sudo -u syslog_viewer journalctl -n 10 --no-pager")
        self.log.info(ret_o)
        if ret_c == 0:
            self.log.info("journalctl executed successfully.")        
        else:
            self.log.info("Error executing journalctl. Exit Code: {ret_c}")
            self.log.info("Error Output: {ret_o}")     

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)  
        self.cmd("pgrep -u syslog_viewer | xargs kill -9", ignore_status=True)
        self.cmd("sleep 2")
        self.cmd("userdel -r syslog_viewer")
        self.cmd("rm -rf /etc/polkit-1/localauthority/50-local.d/allow_syslog_viewer_journalctl.pkla")
