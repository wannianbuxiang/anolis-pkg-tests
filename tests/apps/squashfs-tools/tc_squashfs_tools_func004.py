#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_squashfs_tools_func004.py
@Time:      2024/03/26 17:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_squashfs_tools_func004.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "squashfs-tools"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        #创建文件
        self.cmd("mkdir /tmp/ghm/")
        self.cmd("echo -e 'hello world!' > /tmp/ghm/testpigz001.txt")
        self.cmd("echo -e 'hello world!' > /tmp/ghm/testpigz002.sh")
        self.cmd("mkdir /tmp/ghm/test01")
        self.cmd("echo -e 'hello world!' > /tmp/ghm/test01/testpigz003.py")
        self.cmd("mkdir /tmp/ghm/test02")
        self.cmd("echo -e 'hello world!' > /tmp/ghm/test02/testpigz004.c")

    def test(self):
        self.cmd("touch /tmp/ghm/log1.log")
        self.cmd("touch /tmp/ghm/loa.log")
        self.cmd("touch /tmp/ghm/log234.log")
        self.cmd("touch /tmp/ghm/test01/log5.log")
        self.cmd("mksquashfs /tmp/ghm/ test_ghm.squashfs -regex -e '^log.*\.log$'")
        ret_c1, ret_o1 = self.cmd("ls")     
        self.assertIn("test_ghm.squashfs", ret_o1)
        ret_c2, ret_o2 = self.cmd("unsquashfs -ls test_ghm.squashfs")
        self.assertNotIn("log1.log", ret_o2)
        self.assertNotIn("log234.log", ret_o2)
        self.assertIn("loa.log", ret_o2)
        self.assertIn("log5.log", ret_o2)

        self.cmd("mkdir /tmp/ghm/unzip/")
        self.cmd("unsquashfs -info -dest /tmp/ghm/unzip test_ghm.squashfs")
        ret_c2, ret_o2 = self.cmd("ls /tmp/ghm/unzip/")
        self.assertNotIn("log1.log", ret_o2)
        self.assertNotIn("log234.log", ret_o2)
        self.assertIn("loa.log", ret_o2)
        for file in ret_o2.splitlines():
            if file == "loa.log":
                self.log.info("%s is exist" % file)
            else:
                ret_c3, ret_o3 = self.cmd("cat /tmp/ghm/unzip/%s" % file, ignore_status=True)
                if ret_c3 == 0:
                    self.assertEqual("hello world!", ret_o3)
                else:
                    self.log.info("/tmp/ghm/unzip/%s is a directory" % file)
                    ret_c4, ret_o4 = self.cmd("ls /tmp/ghm/unzip/%s" % file)
                    for infile in ret_o4.splitlines():
                        if infile == "log5.log":
                            self.log.info("%s is exist" % infile)
                        else:
                            ret_c5, ret_o5 = self.cmd("cat /tmp/ghm/unzip/%s/%s" % (file, infile))
                            self.assertEqual("hello world!", ret_o5)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")
        self.cmd("rm -rf test_ghm.squashfs")


