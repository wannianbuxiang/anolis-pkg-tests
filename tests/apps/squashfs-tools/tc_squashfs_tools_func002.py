#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_squashfs_tools_func002.py
@Time:      2024/03/26 11:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""
import subprocess

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_squashfs_tools_func002.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    def check_squashfs_file(self, dir, squashfs_file):
        self.cmd("mkdir /tmp/ghm/%s/" % dir)
        self.cmd("unsquashfs -dest /tmp/ghm/%s %s" % (dir, squashfs_file))
        ret_c6, ret_o6 = self.cmd("ls /tmp/ghm/" + dir)
        for file in ret_o6.splitlines():
            ret_c7, ret_o7 = self.cmd("cat /tmp/ghm/%s/%s" % (dir, file), ignore_status=True)
            if ret_c7 == 0:
                self.assertEqual("hello world!", ret_o7)
            else:
                self.log.info("/tmp/ghm/%s/%s is a directory" % (dir, file))
                ret_c8, ret_o8 = self.cmd("ls /tmp/ghm/%s/%s" % (dir, file))
                ret_c9, ret_o9 = self.cmd("cat /tmp/ghm/%s/%s/%s" % (dir, file, ret_o8.split()[0]))
                self.assertEqual("hello world!", ret_o9)
        self.log.info(" %s check pass", squashfs_file)    

    PARAM_DIC = {"pkg_name": "squashfs-tools"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        #创建文件
        self.cmd("mkdir /tmp/ghm/")
        self.cmd("echo -e 'hello world!' > /tmp/ghm/testpigz001.txt")
        self.cmd("echo -e 'hello world!' > /tmp/ghm/testpigz002.sh")
        self.cmd("mkdir /tmp/ghm/test01")
        self.cmd("echo -e 'hello world!' > /tmp/ghm/test01/testpigz003.py")
        self.cmd("mkdir /tmp/ghm/test02")
        self.cmd("echo -e 'hello world!' > /tmp/ghm/test02/testpigz004.c")

    def test(self):
        #创建文件系统
        self.cmd("mksquashfs /tmp/ghm/ test_ghm.squashfs")
        self.cmd("mksquashfs /tmp/ghm/ test_ghm_lz4.squashfs -comp lz4")
        self.cmd("mksquashfs /tmp/ghm/ test_ghm_xz.squashfs -comp xz")
        self.cmd("mksquashfs /tmp/ghm/ test_ghm_zstd.squashfs -comp zstd")
        ret_c1, ret_o1 = self.cmd("ls")     
        self.assertIn("test_ghm.squashfs", ret_o1)
        self.assertIn("test_ghm_lz4.squashfs", ret_o1)
        self.assertIn("test_ghm_xz.squashfs", ret_o1)
        self.assertIn("test_ghm_zstd.squashfs", ret_o1)

        #du -sh
        ret_c2, ret_o2 = self.cmd("du -sh test_ghm.squashfs | awk '{print $1}'")
        ret_c3, ret_o3 = self.cmd("du -sh test_ghm_lz4.squashfs | awk '{print $1}'")
        ret_c4, ret_o4 = self.cmd("du -sh test_ghm_xz.squashfs| awk '{print $1}'")
        ret_c5, ret_o5 = self.cmd("du -sh test_ghm_zstd.squashfs| awk '{print $1}'")
        if ret_o2 == ret_o3 == ret_o4 == ret_o5:
            self.log.info("Different squashfs have the same size is %s" % ret_o2)

        output1 = subprocess.check_output(['unsquashfs', '-ls', 'test_ghm.squashfs'], text=True)
        actual_lines = output1.strip().split('\n')
        output2 = subprocess.check_output(['unsquashfs', '-ls', 'test_ghm_lz4.squashfs'], text=True)
        lz4_lines = output2.strip().split('\n')
        output3 = subprocess.check_output(['unsquashfs', '-ls', 'test_ghm_xz.squashfs'], text=True)
        xz_lines = output3.strip().split('\n')
        output4 = subprocess.check_output(['unsquashfs', '-ls', 'test_ghm_zstd.squashfs'], text=True)
        zstd_lines = output4.strip().split('\n')
        if len(actual_lines) == len(lz4_lines) == len(xz_lines) == len(zstd_lines):
            self.log.info("Same number of files is %s" % len(actual_lines))

        #unsquashfs -dest /tmp/ghm/unzip test_ghm.squashfs
        self.dir = "ungzip"
        self.squashfs_file = "test_ghm.squashfs"
        self.check_squashfs_file(self.dir, self.squashfs_file)

        #unsquashfs -dest /tmp/ghm/lz4 test_ghm_lz4.squashfs
        self.dir = "lz4"
        self.squashfs_file = "test_ghm_lz4.squashfs"
        self.check_squashfs_file(self.dir, self.squashfs_file)

        #unsquashfs -dest /tmp/ghm/xz test_ghm_xz.squashfs
        self.dir = "xz"
        self.squashfs_file = "test_ghm_xz.squashfs"
        self.check_squashfs_file(self.dir, self.squashfs_file)

        #unsquashfs -dest /tmp/ghm/zstd test_ghm_zstd.squashfs
        self.dir = "zstd"
        self.squashfs_file = "test_ghm_zstd.squashfs"
        self.check_squashfs_file(self.dir, self.squashfs_file)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")
        self.cmd("rm -rf test_ghm.squashfs test_ghm_lz4.squashfs test_ghm_xz.squashfs test_ghm_zstd.squashfs")


