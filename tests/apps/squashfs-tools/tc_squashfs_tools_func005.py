#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_squashfs_tools_func005.py
@Time:      2024/03/27 10:01:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_squashfs_tools_func005.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "squashfs-tools"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        #创建文件
        self.cmd("mkdir /tmp/ghm/")
        self.cmd("echo -e 'hello world!' > /tmp/ghm/testpigz001.txt")
        self.cmd("echo -e 'hello world!' > /tmp/ghm/testpigz002.sh")
        self.cmd("mkdir /tmp/ghm01/")
        self.cmd("echo -e 'hello world!' > /tmp/ghm01/testpigz003.py")
        self.cmd("mkdir /tmp/ghm02/")
        self.cmd("echo -e 'hello world!' > /tmp/ghm02/testpigz004.c")
        self.cmd("mkdir /tmp/ghm03/")

    def test(self):
        ret_c1, ret_o1 = self.cmd("mksquashfs /tmp/ghm/testno test_ghm.squashfs", ignore_status=True)
        self.fail = 'Cannot stat source directory "/tmp/ghm/testno" because No such file or directory'
        self.assertIn(self.fail, ret_o1)

        self.cmd("mksquashfs /tmp/ghm/ test_ghm.squashfs")
        ret_c2, ret_o2 = self.cmd("ls")     
        self.assertIn("test_ghm.squashfs", ret_o2)
        self.cmd("mksquashfs /tmp/ghm01/ test_ghm.squashfs")
        ret_c3, ret_o3 = self.cmd("ls | grep -c test_ghm.squashfs")     
        self.assertEqual("1", ret_o3)
        ret_c4, ret_o4 = self.cmd("unsquashfs -ls test_ghm.squashfs")
        self.assertIn("testpigz001.txt", ret_o4)
        self.assertIn("testpigz002.sh", ret_o4)
        self.assertIn("testpigz003.py", ret_o4)
        self.cmd("mksquashfs /tmp/ghm02/ test_ghm.squashfs -comp lz4")
        ret_c5, ret_o5 = self.cmd("ls | grep -c test_ghm.squashfs")     
        self.assertEqual("1", ret_o5)
        ret_c6, ret_o6 = self.cmd("unsquashfs -ls test_ghm.squashfs")
        self.assertIn("testpigz001.txt", ret_o6)
        self.assertIn("testpigz002.sh", ret_o6)
        self.assertIn("testpigz003.py", ret_o6)
        self.assertIn("testpigz004.c", ret_o6)
        self.cmd("mksquashfs /tmp/ghm03/ test_ghm.squashfs")
        ret_c7, ret_o7 = self.cmd("ls | grep -c test_ghm.squashfs")     
        self.assertEqual("1", ret_o7)
        self.cmd("unsquashfs -linfo test_ghm.squashfs")
        ret_c8, ret_o8 = self.cmd("ls squashfs-root/")
        for file in ret_o8.splitlines():
            ret_c9, ret_o9 = self.cmd("cat squashfs-root/%s" % file)
            self.assertEqual("hello world!", ret_o9)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm*/ squashfs-root/")
        self.cmd("rm -rf test_ghm.squashfs")


