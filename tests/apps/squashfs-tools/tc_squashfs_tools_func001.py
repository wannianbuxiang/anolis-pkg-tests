#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_squashfs_tools_func001.py
@Time:      2024/03/25 17:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""
import re
import subprocess

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_squashfs_tools_func001.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "squashfs-tools"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        #创建文件
        self.cmd("mkdir /tmp/ghm/")
        self.cmd("echo -e 'hello world!' > /tmp/ghm/testpigz001.txt")
        self.cmd("echo -e 'hello world!' > /tmp/ghm/testpigz002.sh")
        self.cmd("mkdir /tmp/ghm/test01")
        self.cmd("echo -e 'hello world!' > /tmp/ghm/test01/testpigz003.py")
        self.cmd("mkdir /tmp/ghm/test02")
        self.cmd("echo -e 'hello world!' > /tmp/ghm/test02/testpigz004.c")

    def test(self):
        #创建文件系统
        self.cmd("mksquashfs /tmp/ghm/ test_ghm.squashfs")
        ret_c1, ret_o1 = self.cmd("ls")     
        self.assertIn("test_ghm.squashfs", ret_o1)

        #unsquashfs -ls test_ghm.squashfs  
        output1 = subprocess.check_output(['unsquashfs', '-ls', 'test_ghm.squashfs'], text=True)
        expected_file1 = """
squashfs-root
squashfs-root/test01
squashfs-root/test01/testpigz003.py
squashfs-root/test02
squashfs-root/test02/testpigz004.c
squashfs-root/testpigz001.txt
squashfs-root/testpigz002.sh
"""
        actual_lines = output1.strip().split('\n')
        expected_lines = expected_file1.strip().split('\n')
        self.assertEqual(len(actual_lines), len(expected_lines))
        for actual_line, expected_line in zip(actual_lines, expected_lines):
            self.assertEqual(actual_line, expected_line)
            self.log.info("%s is exist" % actual_line)

        #unsquashfs -lls test_ghm.squashfs 
        output2 = subprocess.check_output(['unsquashfs', '-lls', 'test_ghm.squashfs'], text=True)
        timestamp_pattern = r'\d{4}-\d{2}-\d{2} \d{2}:\d{2}'
        expected_file2 = """
drwxr-xr-x root/root                88 
squashfs-root
drwxr-xr-x root/root                37 
squashfs-root/test01
-rw-r--r-- root/root                13 
squashfs-root/test01/testpigz003.py
drwxr-xr-x root/root                36 
squashfs-root/test02
-rw-r--r-- root/root                13 
squashfs-root/test02/testpigz004.c
-rw-r--r-- root/root                13 
squashfs-root/testpigz001.txt
-rw-r--r-- root/root                13 
squashfs-root/testpigz002.sh
"""
        actual_lines2 = re.sub(timestamp_pattern, '', output2)
        expected_lines2 = expected_file2.strip().splitlines()
        for expected_line2 in expected_lines2:
            self.assertIn(expected_line2, actual_lines2)
            self.log.info("%s exist." % expected_line2)

        #unsquashfs -dest /tmp/ghm/unzip test_ghm.squashfs
        self.cmd("mkdir /tmp/ghm/unzip/")
        self.cmd("unsquashfs -dest /tmp/ghm/unzip test_ghm.squashfs")
        ret_c2, ret_o2 = self.cmd("ls /tmp/ghm/unzip/")
        for file in ret_o2.splitlines():
            self.log.info("%s" % file)
            ret_c3, ret_o3 = self.cmd("cat /tmp/ghm/unzip/%s" % file, ignore_status=True)
            if ret_c3 == 0:
                self.assertEqual("hello world!", ret_o3)
            else:
                self.log.info("/tmp/ghm/unzip/%s is a directory" % file)
                ret_c4, ret_o4 = self.cmd("ls /tmp/ghm/unzip/%s" % file)
                ret_c5, ret_o5 = self.cmd("cat /tmp/ghm/unzip/%s/%s" % (file, ret_o4.split()[0]))
                self.assertEqual("hello world!", ret_o5)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")
        self.cmd("rm -rf test_ghm.squashfs")