#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_squashfs_tools_func003.py
@Time:      2024/03/26 16:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_squashfs_tools_func003.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "squashfs-tools"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        #创建文件
        self.cmd("mkdir /tmp/ghm/")
        self.cmd("echo -e 'hello world!' > /tmp/ghm/testpigz001.txt")
        self.cmd("echo -e 'hello world!' > /tmp/ghm/testpigz002.sh")
        self.cmd("mkdir /tmp/ghm/test01")
        self.cmd("echo -e 'hello world!' > /tmp/ghm/test01/testpigz003.py")
        self.cmd("mkdir /tmp/ghm/test02")
        self.cmd("echo -e 'hello world!' > /tmp/ghm/test02/testpigz004.c")

    def test(self):
        #创建文件系统
        self.cmd("mksquashfs /tmp/ghm/ test_ghm.squashfs -e /tmp/ghm/test01/ -e /tmp/ghm/testpigz001.txt")
        ret_c1, ret_o1 = self.cmd("ls")     
        self.assertIn("test_ghm.squashfs", ret_o1)
        ret_c2, ret_o2 = self.cmd("unsquashfs -ls test_ghm.squashfs")
        self.assertNotIn("test01", ret_o2)
        self.assertNotIn("testpigz001.txt", ret_o2)
        self.cmd("rm -rf test_ghm.squashfs")

        #创建文件系统
        self.cmd("gzip /tmp/ghm/testpigz001.txt -c > /tmp/ghm/testpigz001.txt.gz")
        self.cmd("mksquashfs /tmp/ghm/ test_ghm.squashfs -wildcards -e '*.gz'")
        ret_c1, ret_o1 = self.cmd("ls")     
        self.assertIn("test_ghm.squashfs", ret_o1)
        self.cmd("mkdir /tmp/ghm/unzip/")
        self.cmd("unsquashfs -info -dest /tmp/ghm/unzip test_ghm.squashfs")
        ret_c2, ret_o2 = self.cmd("ls /tmp/ghm/unzip/")
        self.assertNotIn("testpigz001.txt.gz", ret_o2)
        for file in ret_o2.splitlines():
            self.log.info("%s" % file)
            ret_c3, ret_o3 = self.cmd("cat /tmp/ghm/unzip/%s" % file, ignore_status=True)
            if ret_c3 == 0:
                self.assertEqual("hello world!", ret_o3)
            else:
                self.log.info("/tmp/ghm/unzip/%s is a directory" % file)
                ret_c4, ret_o4 = self.cmd("ls /tmp/ghm/unzip/%s" % file)
                self.assertNotIn("testpigz001.txt.gz", ret_o4)
                ret_c5, ret_o5 = self.cmd("cat /tmp/ghm/unzip/%s/%s" % (file, ret_o4.split()[0]))
                self.assertEqual("hello world!", ret_o5)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")
        self.cmd("rm -rf test_ghm.squashfs")


