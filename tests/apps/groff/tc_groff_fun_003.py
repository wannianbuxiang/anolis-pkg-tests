#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_groff_fun_003.py
@Time:      2024/04/15 15:16:50
@Author:    zhixin
@Version:   1.0
@Contact:   zx01468051@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhixin
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_groff_fun_003.yaml for details

    :avocado: tags=fix,P3,noarch,local
    """
    PARAM_DIC = {"pkg_name": "groff groff-base groff-doc groff-perl groff-x11"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > test_eqn.ms <<EOF
.EQ
alpha + beta = gamma
.EN
.PP
.EQ
int from 0 to infinity e sup {-x^2} dx = sqrt {pi} over 2
.EN
EOF"""
        self.cmd(cmdline) 
        cmdline = """cat > test_table.ms <<EOF
.TL
A Simple Table
.PP
Here is an example of a simple table:
.TS
tab(@);
l l l.
T1@T2@T3
abc@def@ghi
123@456@789
.TE
EOF"""
        self.cmd(cmdline)
        cmdline = """cat > test_picture.ms <<EOF
.PS
arrow
box "Groff"
.PE
EOF"""
        self.cmd(cmdline)
        cmdline = """cat > test_refer.ms <<EOF
.P
[1] "The Art of Computer Programming" Donald E. Knuth
.P
[2] "The C Programming Language" Kernighan & Ritchie
EOF"""
        self.cmd(cmdline)
        cmdline = """cat > test_chem.ms <<EOF
.CS
chem {
C;up;H;down;H;right;O;up;H
}
.CE
EOF"""
        self.cmd(cmdline)

    def test(self):
        # work with mathematical formulas
        self.cmd('groff -e -Tpdf test_eqn.ms > test_eqn.pdf')
        ret_c, ret_o = self.cmd('file test_eqn.pdf')
        self.assertIn("PDF document", ret_o)
        # work with tables
        self.cmd('groff -t -Tpdf test_table.ms > test_table.pdf')
        ret_c, ret_o = self.cmd('file test_table.pdf')
        self.assertIn("PDF document", ret_o)
        # work with picture
        self.cmd('groff -p -Tpdf test_picture.ms > test_picture.pdf')
        ret_c, ret_o = self.cmd('file test_picture.pdf')
        self.assertIn("PDF document", ret_o)
        # work with refer
        self.cmd('groff -R -Tpdf test_refer.ms > test_refer.pdf')
        ret_c, ret_o = self.cmd('file test_refer.pdf')
        self.assertIn("PDF document", ret_o)
        # work with chemical structure
        self.cmd('groff -c -Tpdf test_chem.ms > test_chem.pdf')
        ret_c, ret_o = self.cmd('file test_chem.pdf')
        self.assertIn("PDF document", ret_o)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf test_*.ms test_*.pdf')
