#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_groff_fun_002.py
@Time:      2024/04/12 16:51:50
@Author:    zhixin
@Version:   1.0
@Contact:   zx01468051@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhixin
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_groff_fun_002.yaml for details

    :avocado: tags=fix,P3,noarch,local
    """
    PARAM_DIC = {"pkg_name": "groff ghostscript groff-perl groff-x11 groff-doc perl-IO-Compress"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > test_ms.ms <<EOF
.TL
This is the title
.AU
Author Name
.PP
This is a paragraph in the -ms macro package.
EOF"""
        self.cmd(cmdline) 
        cmdline = """cat > test_mm.mm <<EOF
.AF
'Company Name'
..
.P
This is a paragraph in the -mm macro package.
..
EOF"""
        self.cmd(cmdline)
        cmdline = """cat > test_me.me <<EOF
.TITLE "Macro Package -me"
.AUTHOR "Author Name"
.P
This is a paragraph in the -me macro package.
EOF"""
        self.cmd(cmdline)
        cmdline = """cat > test_mdoc.mdoc <<EOF
.Dd August 1, 2021
.Dt TEST 1
.Os
.Sh NAME
.Nm test_mdoc
.Nd Test of the -mdoc macro package
EOF"""
        self.cmd(cmdline)
        cmdline = """cat > test_man.man <<EOF
.TH TEST_MAN 1 "August 1, 2021"
.SH NAME
test_man \- Test of the -man macro package
EOF"""
        self.cmd(cmdline)

    def test(self):
        # use -ms parameter
        self.cmd('groff -Tpdf -ms test_ms.ms > test_ms.pdf')
        ret_c, ret_o = self.cmd('file test_ms.pdf')
        self.assertIn("PDF document", ret_o)
        # use -mm parameter
        self.cmd('groff -Tpdf -mm test_mm.mm > test_mm.pdf')
        ret_c, ret_o = self.cmd('file test_mm.pdf')
        self.assertIn("PDF document", ret_o)
        # use -me parameter
        self.cmd('groff -Tpdf -me test_me.me > test_me.pdf')
        ret_c, ret_o = self.cmd('file test_me.pdf')
        self.assertIn("PDF document", ret_o)
        # use -mdoc parameter
        self.cmd('groff -Tpdf -mdoc test_mdoc.mdoc > test_mdoc.pdf')
        ret_c, ret_o = self.cmd('file test_mdoc.pdf')
        self.assertIn("PDF document", ret_o)
        # use -man parameter
        self.cmd('groff -Tpdf -man test_man.man > test_man.pdf')
        ret_c, ret_o = self.cmd('file test_man.pdf')
        self.assertIn("PDF document", ret_o)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf test_m*')
