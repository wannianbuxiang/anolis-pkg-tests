#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_groff_fun_005.py
@Time:      2024/04/16 13:51:50
@Author:    zhixin
@Version:   1.0
@Contact:   zx01468051@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhixin
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_groff_fun_005.yaml for details

    :avocado: tags=fix,P3,noarch,local
    """
    PARAM_DIC = {"pkg_name": "groff ghostscript"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > example.ms <<EOF
.TL
Test Document
.PP
A simple paragraph.
EOF"""
        self.cmd(cmdline) 

        cmdline = """cat > test_ascii.ms <<EOF
.PP
ASCII Encoding Test
.br
The quick brown fox jumps over the lazy dog.
.br
0123456789
EOF"""
        self.cmd(cmdline)
        cmdline = """cat > graphic.ms <<EOF
.G1
graph
max Y 100
min X 0.0 max X 5.0
0.0 0.0 "0%" dot
1.0 20.0 "20%" dot
2.0 40.0 "40%" dot
3.0 60.0 "60%" dot
4.0 80.0 "80%" dot
5.0 100.0 "100%" dot
.G2
EOF"""
        self.cmd(cmdline)

    def test(self):
        self.cmd('groff -Tl -ms example.ms > output.txt 2>&1', ignore_status=True)
        ret_c, ret_o = self.cmd('cat output.txt')
        self.assertTrue("groff: fatal error: cannot load 'DESC' description file for device 'l'" in ret_o, 'check output error1.')

        self.cmd('groff -Tascii -Kutf16 test_ascii.ms > test_ascii.txt 2>&1', ignore_status=True)
        ret_c, ret_o = self.cmd('cat test_ascii.txt')
        self.assertTrue("troff:test_ascii.ms:1: warning: special character 'u502E' not defined" in ret_o, 'check output error2.')

        self.cmd('groff -G -Tascii graphic.ms > test_graphic.txt 2>&1', ignore_status=True)
        ret_c, ret_o = self.cmd('cat test_graphic.txt')
        self.assertTrue("groff: error: couldn't exec grap: No such file or directory" in ret_o, 'check output error3.')

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf example.ms test_ascii.ms graphic.ms output.txt test_*.txt ')
