#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_groff_fun_001.py
@Time:      2024/04/11 16:51:50
@Author:    zhixin
@Version:   1.0
@Contact:   zx01468051@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhixin
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_groff_fun_001.yaml for details

    :avocado: tags=fix,P3,noarch,local
    """
    PARAM_DIC = {"pkg_name": "groff ghostscript"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > example.ms <<EOF
.TL
Test Document
.PP
A simple paragraph.
EOF"""
        self.cmd(cmdline) 

    def test(self):
        self.cmd('groff -Tascii -ms example.ms > output.txt')
        ret_c, ret_o = self.cmd('file output.txt')
        self.assertTrue("ASCII text" in ret_o, 'check output error1.')
        ret_c, ret_o = self.cmd('cat output.txt')
        self.assertIn('Test Document', ret_o)
        self.assertIn('A simple paragraph.', ret_o)
        self.cmd('groff -Tps -ms example.ms | ps2pdf - example.pdf')
        self.cmd('ls example.pdf')
        ret_c, ret_o = self.cmd('file example.pdf')
        self.assertIn('PDF document', ret_o)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf example.ms example.pdf output.txt')
