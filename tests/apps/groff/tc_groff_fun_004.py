#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_groff_fun_004.py
@Time:      2024/04/15 17:16:50
@Author:    zhixin
@Version:   1.0
@Contact:   zx01468051@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhixin
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_groff_fun_004.yaml for details

    :avocado: tags=fix,P3,noarch,local
    """
    PARAM_DIC = {"pkg_name": "groff groff-base"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > test_ascii.ms <<EOF
.PP
ASCII Encoding Test
.br
The quick brown fox jumps over the lazy dog.
.br
0123456789
EOF"""
        self.cmd(cmdline)
        cmdline = """cat > test_latin.ms <<EOF
.PP
Latin-1 Encoding Test
.br
ÀÁÂÄÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÖØÙÚÛÜÝß
.br
àáâäæçèéêëìíîïðñòóôöøùúûüýÿ
EOF"""
        self.cmd(cmdline)
        cmdline = """cat > test_utf-8.ms <<EOF
.PP
UTF-8 Encoding Test
.br
Smiley: ☺
.br
Greek: αβγδεζηθ
.br
Cyrillic: ЖзИйКк
.br
Arabic: ﺎﺒﺘﺜﺠﺤﺧ
.br
Emoji: 😃
EOF"""
        self.cmd(cmdline)
        cmdline = """cat > test_special.ms <<EOF
.PP
Special Characters Test
.br
Math symbols: ∑ ∫ ∈ √ π ∞ ≤ ≥
.br
Currency symbols: $ € £ ¥
.br
Trademark: ™ © ®
EOF"""
        self.cmd(cmdline)

    def test(self):
        # ASCII coding test
        self.cmd('groff -Tascii -Kutf8 test_ascii.ms > test_ascii.txt')
        ret_c, ret_o = self.cmd('cat test_ascii.txt')
        self.assertTrue("ASCII Encoding Test\nThe quick brown fox jumps over the lazy dog.\n0123456789" in ret_o, 'check output error1.')
        # Latin-1 coding test
        self.cmd('groff -Tascii -Kutf8 test_latin.ms > test_latin.txt')
        ret_c, ret_o = self.cmd('cat test_latin.txt')
        self.assertTrue("Latin-1 Encoding Test\nAE\nae" in ret_o, 'check output error2.')
        # UTF-8 coding test
        self.cmd('groff -Tascii -Kutf8 test_utf-8.ms > test_utf-8.txt')
        ret_c, ret_o = self.cmd('cat test_utf-8.txt')
        self.assertTrue("UTF-8 Encoding Test\nSmiley:\nGreek:\nCyrillic:\nArabic:\nEmoji:" in ret_o, 'check output error3.')
        # Special characters coding test
        self.cmd('groff -Tascii -Kutf8 test_special.ms > test_special.txt')
        ret_c, ret_o = self.cmd('cat test_special.txt')
        self.assertTrue("Special Characters Test\nMath symbols:       <= >=\nCurrency symbols: $ EUR\nTrademark:  (C) (R)" in ret_o, 'check output error4.')
 
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf test_*.ms test_*.txt')
