#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_gobject_introspection_func002.py
@Time:      2024/05/08 10:11:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_gobject_introspection_func002.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gobject-introspection-devel glib2-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_gobject_introspection.c <<EOF
#include "girepository.h"

static void
test_field_iterators (void)
{
  GIRepository *repo;
  GITypelib *ret;
  GIStructInfo *class_info;
  GIFieldInfo *field_info;
  GError *error = NULL;
  gint i;

  repo = g_irepository_get_default ();
}

int
main(int argc, char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/girepository/structinfo/field-iterators", test_field_iterators);

  g_test_run ();
}
"""
        self.cmd(cmdline)

    def test(self):  
        self.cmd("gcc -I/usr/include/gobject-introspection-1.0 -I/usr/include/glib-2.0 -I/usr/lib64/glib-2.0/include/ -o /tmp/ghm/test_gobject_introspection /tmp/ghm/test_gobject_introspection.c -L/usr/lib64 -lgirepository-1.0 -lglib-2.0")
        ret_c, ret_o = self.cmd("/tmp/ghm/test_gobject_introspection")       
        self.assertIn("TAP version 13", ret_o) 
        self.assertIn("random seed:", ret_o) 
        self.assertIn("Start of girepository tests", ret_o) 
        self.assertIn("End of girepository tests", ret_o) 

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)  
        self.cmd("rm -rf /tmp/ghm/")