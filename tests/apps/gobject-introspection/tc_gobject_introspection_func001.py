#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_gobject_introspection_func001.py
@Time:      2024/05/07 10:11:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_gobject_introspection_func001.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gobject-introspection-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_gobject_introspection.c <<EOF
#include <girepository.h>
#include <stdio.h>

int main() {
    guint major = GI_MAJOR_VERSION;
    guint minor = GI_MINOR_VERSION;
    guint micro = GI_MICRO_VERSION;

    printf("girepository Version: %d.%d.%d\\n", major, minor, micro);

    return 0;
}
"""
        self.cmd(cmdline)

    def test(self):
        ret_c, ret_o = self.cmd("yum info gobject-introspection | grep 'Version'")
        self.log.info("The current  %s" % ret_o)    
        self.cmd("gcc -I/usr/include/gobject-introspection-1.0 -I/usr/include/glib-2.0 -I/usr/lib64/glib-2.0/include/ -o /tmp/ghm/gobject_introspection /tmp/ghm/test_gobject_introspection.c -lgirepository-1.0 -lglib-2.0")
        ret_c1, ret_o1 = self.cmd("/tmp/ghm/gobject_introspection")  
        self.assertIn("girepository Version: 1.76.1", ret_o1)     

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)  
        self.cmd("rm -rf /tmp/ghm/")