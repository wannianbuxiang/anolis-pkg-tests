#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_system_lsb_func001.py
@Time:      2024/03/05 17:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""
import subprocess

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_system_lsb_func001.yaml for details

    :avocado: tags=P1,noarch,local.fix
    """
    PARAM_DIC = {"pkg_name": "lsb-release"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        self.cmd("lsb_release -a")
        self.cmd("lsb_release -d")
        self.cmd("lsb_release -i -s")
        self.cmd("lsb_release -rcs")
        self.cmd("lsb_release -v")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)

