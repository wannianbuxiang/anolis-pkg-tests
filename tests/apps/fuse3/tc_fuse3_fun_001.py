# -*- encoding: utf-8 -*-

"""
@File:      tc_fuse3_fun_001.py
@Time:      2024-03-26 14:52:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from apps.fuse3 import common_fuse3
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_fuse3_fun_001.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "fuse3 fuse3-devel fuse3-libs"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.c_file = "hello.c"
        self.exec_file = "hello"
        self.mount_point = "/tmp/fuse"
        # 将内容写入 hello.c 文件
        with open(self.c_file, "w", encoding="utf-8") as file:
            file.write(common_fuse3.hello_content)

    def test(self):
        self.cmd(
            f"gcc -Wall {self.c_file} `pkg-config fuse3 --cflags --libs` -o {self.exec_file}"
        )
        code, result = self.cmd(f"file {self.exec_file}")
        self.assertIn("executable", result)
        self.cmd(f"mkdir -p {self.mount_point}")
        try:
            # 挂载文件系统
            self.cmd(f"./{self.exec_file} {self.mount_point}")
        except Exception as e:
            self.log.info(f"{self.exec_file}文件系统挂载失败: {str(e)}")
        code, result = self.cmd(f"ls -l {self.mount_point}")
        self.assertIn(self.exec_file, result)
        code, result = self.cmd(f"cat {self.mount_point}/{self.exec_file}")
        self.assertIn("Hello World!", result)
        code, result = self.cmd(f"./{self.exec_file} -h")
        self.assertIn("usage: ./hello [options] <mountpoint>", result)
        code, result = self.cmd(f"./{self.exec_file} -V")
        self.assertIn("FUSE library version", result)


    def tearDown(self):
        self.cmd(f"umount -l {self.mount_point}")
        self.cmd(f"rm -rf {self.c_file} {self.exec_file} {self.mount_point}")
        super().tearDown(self.PARAM_DIC)
