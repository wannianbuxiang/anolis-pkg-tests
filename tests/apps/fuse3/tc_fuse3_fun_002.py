# -*- encoding: utf-8 -*-

"""
@File:      tc_fuse3_fun_002.py
@Time:      2024-03-26 14:52:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

import time
from apps.fuse import common_fuse
from apps.fuse3 import common_fuse3
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_fuse3_fun_002.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "fuse3 fuse3-devel fuse3-libs"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.c_file = "hello.c"
        self.exec_file = "hello"
        self.mount_point = "/tmp/fuse"
        # 创建非root用户
        code, result = self.cmd(f"id {common_fuse.username}", ignore_status=True)
        if code != 0 and 'no such user' in result:
            self.log.info("用户不存在，正在创建...")
            common_fuse.create_newser(self, common_fuse.username, common_fuse.passwd)
        else:
            self.log.info("用户存在，删除旧用户，重新创建测试用户")
            self.cmd(f"pgrep -u {common_fuse.username} | xargs kill -9", ignore_status=True)
            common_fuse.del_newuser(self, common_fuse.username)
            common_fuse.create_newser(self, common_fuse.username, common_fuse.passwd)
        #以非root用户创建挂载点
        common_fuse.newuser_cmd(self, f"'mkdir -p {self.mount_point}'")
        # 给非root用户增加挂载点的write权限
        common_fuse.add_right(self, common_fuse.username, self.mount_point)        
        # 将内容写入 hello.c 文件
        with open(self.c_file, "w", encoding="utf-8") as file:
            file.write(common_fuse3.hello_content)

    def test(self):
        self.cmd(
            f"gcc -Wall {self.c_file} `pkg-config fuse3 --cflags --libs` -o {self.exec_file}"
        )
        code, result = self.cmd(f"file {self.exec_file}")
        self.assertIn("executable", result)
        self.cmd(f"mkdir -p {self.mount_point}")
        try:
            # 挂载文件系统
            common_fuse.newuser_cmd(self, f"'./{self.exec_file} {self.mount_point}'")
            self.log.info("文件系统挂载成功")
        except Exception as e:
            self.log.error(f"{self.exec_file}文件系统挂载失败: {str(e)}")
        code, result = common_fuse.newuser_cmd(self, f"'ls -l {self.mount_point}'")
        self.assertIn(self.exec_file, result)
        code, result = common_fuse.newuser_cmd(self, f"'cat {self.mount_point}/{self.exec_file}'")
        self.assertIn("Hello World!", result)
        code, result = common_fuse.newuser_cmd(self, f"'./{self.exec_file} -h'")
        self.assertIn("usage: ./hello [options] <mountpoint>", result)
        code, result = common_fuse.newuser_cmd(self, f"'./{self.exec_file} -V'")
        self.assertIn("FUSE library version", result)


    def tearDown(self):
        self.cmd(f"umount -l {self.mount_point}")
        #执行删除newuser操作
        self.cmd(f"pgrep -u {common_fuse.username}| xargs kill -9", ignore_status=True)
        common_fuse.del_newuser(self, common_fuse.username)
        super().tearDown(self.PARAM_DIC)