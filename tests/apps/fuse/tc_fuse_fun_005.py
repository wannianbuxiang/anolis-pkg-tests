# -*- encoding: utf-8 -*-

"""
@File:      tc_fuse_fun_005.py
@Time:      2024-03-28 14:52:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from apps.fuse import common_fuse
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_fuse_fun_005.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "fuse fuse-devel fuse-libs"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.c_file = "fusexmp.c"
        self.exec_file = "fusexmp"
        self.mount_point = "/tmp/fusexmp"
        #打开allow_other配置
        common_fuse.enable_allow_other(self)
        # 创建非root用户
        code, result = self.cmd(f"id {common_fuse.username}", ignore_status=True)
        if code != 0 and 'no such user' in result:
            self.log.info("用户不存在，正在创建...")
            common_fuse.create_newser(self, common_fuse.username, common_fuse.passwd)
        else:
            self.log.info("用户存在，删除旧用户，重新创建测试用户")
            self.cmd(f"pgrep -u {common_fuse.username} | xargs kill -9", ignore_status=True)
            common_fuse.del_newuser(self, common_fuse.username)
            common_fuse.create_newser(self, common_fuse.username, common_fuse.passwd)
        #以非root用户创建挂载点
        common_fuse.newuser_cmd(self, f"'mkdir -p {self.mount_point}'")
        # 给非root用户增加挂载点的write权限
        common_fuse.add_right(self, common_fuse.username, self.mount_point)
        # 将内容写入fusexmp.c文件中
        with open(self.c_file, "w", encoding="utf-8") as file:
            file.write(common_fuse.fusexmp_content)

    def test(self):
        self.cmd(
            f"gcc -Wall {self.c_file} `pkg-config fuse --cflags --libs` -o {self.exec_file}"
        )
        code, result = self.cmd(f"file {self.exec_file}")
        self.assertIn("executable", result)
        try:
            # 挂载文件系统
            common_fuse.newuser_cmd(self, f"'./{self.exec_file} -o allow_other {self.mount_point}'")
            self.log.info("文件系统挂载成功")
        except Exception as e:
            self.log.error(f"{self.exec_file}文件系统挂载失败: {str(e)}")
        #非root用户进行目录读取
        code, newuser_result = common_fuse.newuser_cmd(self, f"'ls -d {self.mount_point}/*'")
        #root用户进行目录读取
        code, root_result = self.cmd(f"ls -d {self.mount_point}/*")
        self.assertEqual(newuser_result, root_result)
     

    def tearDown(self):
        common_fuse.disable_allow_other(self)
        self.cmd(f"umount -l {self.mount_point}")
        #执行删除newuser操作
        self.cmd(f"pgrep -u {common_fuse.username}| xargs kill -9", ignore_status=True)
        common_fuse.del_newuser(self, common_fuse.username)
        self.cmd(f"rm -rf {self.c_file} {self.exec_file} {self.mount_point}")
        super().tearDown(self.PARAM_DIC)
