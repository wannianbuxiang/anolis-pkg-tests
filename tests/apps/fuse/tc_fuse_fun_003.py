# -*- encoding: utf-8 -*-

"""
@File:      tc_fuse_fun_003.py
@Time:      2024-03-28 14:52:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
import os
from apps.fuse import common_fuse
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_fuse_fun_003.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "fuse fuse-devel fuse-libs"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.c_file = "fusexmp.c"
        self.exec_file = "fusexmp"
        self.mount_point = "/tmp/fusexmp"
        self.testfile = "test.txt"
        self.changefile = "changename.txt"
        self.new_file = os.path.join(self.mount_point, self.testfile)
        self.changename_file = os.path.join(self.mount_point, self.changefile)
        # 将内容写入 fusexmp.c文件中
        with open(self.c_file, "w", encoding="utf-8") as file:
            file.write(common_fuse.fusexmp_content)

    def test(self):
        self.cmd(
            f"gcc -Wall {self.c_file} `pkg-config fuse --cflags --libs` -o {self.exec_file}"
        )
        code, result = self.cmd(f"file {self.exec_file}")
        self.assertIn("executable", result)
        self.cmd(f"mkdir -p {self.mount_point}")
        # 挂载文件系统
        try:
            self.cmd(f"./{self.exec_file} {self.mount_point}")
            self.log.info("文件系统挂载成功")
        except Exception as e:
            self.log.error(f"{self.exec_file}文件系统挂载失败: {str(e)}")
        # 创建并写入文件
        self.cmd(f"echo 'Python' > {self.new_file}")
        # 读取文件
        code, result = self.cmd(f"cat {self.new_file}")
        self.assertEqual("Python", result)
        # 获取文件属性
        code, file_size = self.cmd(f"stat -c %s '{self.new_file}'")
        self.assertIn("7", file_size)
        code, file_right = self.cmd(f"stat -c %a '{self.new_file}'")
        self.assertIn("644", file_right)
        code, file_owner = self.cmd(f"stat -c %U '{self.new_file}'")
        self.assertIn("root", file_owner)
        code, file_group = self.cmd(f"stat -c %G '{self.new_file}'")
        self.assertIn("root", file_group)
        code, file_ctime = self.cmd(
            f"stat -c %y '{self.new_file}'|awk -F. '{{print $1}}'"
        )
        code, current_time = self.cmd("echo `date +'%Y-%m-%d %H:%M:%S'`")
        self.assertIn(current_time, file_ctime)
        # 目录读取
        code, result = self.cmd(f"ls -l {self.mount_point}")
        self.assertIn(self.testfile, result)
        # 目录的创建
        self.cmd(f"mkdir -p {self.mount_point}/fuse_dir")
        code, result = self.cmd(f"ls -l {self.mount_point}")
        self.assertIn("fuse_dir", result)
        # 文件截断
        expect_size = 4
        self.cmd(f"truncate -s {expect_size} '{self.new_file}'")
        code, new_size = self.cmd(f"stat -c %s '{self.new_file}'")
        code, new_context = self.cmd(f"cat '{self.new_file}'")
        self.assertIn(str(expect_size), new_size)
        self.assertIn("Pyth", new_context)
        # 文件权限修改
        self.cmd(f"chmod 755 {self.new_file}")
        code, new_right = self.cmd(f"stat -c %a '{self.new_file}'")
        self.assertIn("755", new_right)
        # 创建符号链接
        self.cmd(f"ln -s {self.new_file} {self.mount_point}/symlink")
        # 读取符号链接
        code, link_file = self.cmd(f"ls -l {self.mount_point}/symlink")
        self.assertIn(f"{self.mount_point}/symlink -> {self.new_file}", link_file)
        # 删除符号链接
        self.cmd(f"rm -f {self.mount_point}/symlink")
        code, result = self.cmd(
            f"ls -l  {self.mount_point}/symlink", ignore_status=True
        )
        self.assertIn("No such file or directory", result)
        # 重命名
        self.cmd(f"mv {self.new_file} {self.changename_file}")
        code, result = self.cmd(f"ls -l {self.mount_point}")
        self.assertIn(self.changefile, result)
        # 文件删除
        self.cmd(f"rm -f {self.changename_file}")
        code, result = self.cmd(f"ls -l {self.mount_point}")
        self.assertNotIn(self.changefile, result)
        # 目录删除
        self.cmd(f"rmdir {self.mount_point}/fuse_dir")
        code, result = self.cmd(f"ls -l {self.mount_point}")
        self.assertNotIn("fuse_dir", result)

    def tearDown(self):
        self.cmd(f"umount -l {self.mount_point}")
        self.cmd(f"rm -rf {self.c_file} {self.exec_file} {self.mount_point}")
        super().tearDown(self.PARAM_DIC)