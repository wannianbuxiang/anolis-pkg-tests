# -*- encoding: utf-8 -*-

"""
@File:      tc_fuse_fun_001.py
@Time:      2024-03-28 14:52:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting

"""
from apps.fuse import common_fuse
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_fuse_fun_001.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "fuse fuse-devel fuse-libs"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.c_file = "hello.c"
        self.exec_file = "hello"
        self.mount_point = "/tmp/fuse"
        # 将内容写入 hello.c 文件
        with open(self.c_file, "w", encoding="utf-8") as file:
            file.write(common_fuse.Hello_Content)

    def test(self):
        self.cmd(
            f"gcc -Wall {self.c_file} `pkg-config fuse --cflags --libs` -o {self.exec_file}"
        )
        code, result = self.cmd(f"file {self.exec_file}")
        self.assertIn("executable", result)
        self.cmd(f"mkdir -p {self.mount_point}")
        try:
            # 挂载文件系统
            self.cmd(f"./{self.exec_file} {self.mount_point}")
        except Exception as e:
            self.log.error(f"{self.exec_file}文件系统挂载失败: {str(e)}")
        code, result = self.cmd(f"ls -l {self.mount_point}")
        self.assertIn(self.exec_file, result)
        code, result = self.cmd(f"cat {self.mount_point}/{self.exec_file}")
        self.assertIn("Hello World!", result)
        #因源代码定义usage、helper_help定义的是将信息输出到stderr中，所以此处的命令返回code非0
        code, result = self.cmd(f"./{self.exec_file} -h", ignore_status=True)
        self.assertIn("usage: ./hello mountpoint [options]", result)
        code, result = self.cmd(f"./{self.exec_file} -V", ignore_status=True)
        self.assertIn("FUSE library version", result)
        
    def tearDown(self):
        self.cmd(f"umount -l {self.mount_point}")
        self.cmd(f"rm -rf {self.c_file} {self.exec_file} {self.mount_point}")
        super().tearDown(self.PARAM_DIC)
