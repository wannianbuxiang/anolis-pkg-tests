#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_inih_fun_002.py
@Time:      2024/04/02 16:51:50
@Author:    zhixin
@Version:   1.0
@Contact:   zx01468051@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhixin
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_inih_fun_002.yaml for details

    :avocado: tags=fix,P3,noarch,local
    """
    PARAM_DIC = {"pkg_name": "inih inih-devel gcc-c++"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > myapp.cpp <<EOF
#include <INIReader.h>
#include <iostream>

int main() {
    INIReader reader("example.ini");

    if (reader.ParseError() < 0) {
        std::cout << "Can't load 'example.ini'\\n";
        return 1;
    }

    std::string name = reader.Get("owner", "name", "UNKNOWN");
    std::string organization = reader.Get("owner", "organization", "UNKNOWN");

    std::cout << "Owner name: " << name << "\\n"
              << "Organization: " << organization << std::endl;

    return 0;
}
EOF"""
        self.cmd(cmdline) 
        cmdline = """cat > example.ini << EOF
[owner]
name=John Doe
organization=Acme Widgets Inc.
EOF"""  
        self.cmd(cmdline)

    def test(self):
        self.cmd('g++ myapp.cpp -o myapp -lINIReader')
        ret_c, ret_o = self.cmd('ldd ./myapp')
        self.assertTrue("libINIReader.so.0" in ret_o, 'check output error1.')
        self.assertTrue("libinih.so.0" in ret_o, 'check output error2.')
        ret_c, ret_o = self.cmd('./myapp')
        self.assertTrue("Owner name: John Doe\nOrganization: Acme Widgets Inc." == ret_o, 'check output error3.')
        self.cmd('rm -rf example.ini')
        self.cmd('./myapp > result.log 2>&1', ignore_status=True)
        ret_c, ret_o = self.cmd('cat result.log')
        self.assertIn("Can't load 'example.ini'", ret_o)
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf myapp* example.ini result.log')
