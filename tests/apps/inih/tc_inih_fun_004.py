#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_inih_fun_004.py
@Time:      2024/04/01 14:51:50
@Author:    zhixin
@Version:   1.0
@Contact:   zx01468051@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhixin
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_inih_fun_004.yaml for details

    :avocado: tags=fix,P3,noarch,local
    """
    PARAM_DIC = {"pkg_name": "inih inih-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > print_ini.c <<EOF
#include <ini.h>
#include <stdio.h>

// 定义解析INI文件时的回调函数
static int handler(void* user, const char* section, const char* name, const char* value)
{
    printf("Section [%s] Key %s = %s\\n", section, name, value);
    return 1;  // 返回1表示成功解析
}

int main(int argc, char* argv[])
{
    if (argc < 2) {
        fputs("Usage: print_ini <file.ini>\\n", stderr);
        return 1;
    }

    if (ini_parse(argv[1], handler, NULL) < 0) {
        printf("Can't load '%s' file\\n", argv[1]);
        return 1;
    }
    return 0;
}
EOF"""
        self.cmd(cmdline) 
        cmdline = """cat > file_end_txt.txt << EOF
[owner]
name=John Doe
organization=Acme Widgets Inc.
EOF"""  
        self.cmd(cmdline)
        cmdline = """cat > file_no_header.ini << EOF
name=John Doe
organization=Acme Widgets Inc.
EOF"""
        self.cmd(cmdline)

    def test(self):
        self.cmd('gcc print_ini.c -o print_ini -linih')
        # run with non-existent file
        self.cmd('./print_ini aaa.ini > result.log 2>&1', ignore_status=True)
        ret_c, ret_o = self.cmd('cat result.log')
        self.assertIn("Can't load 'aaa.ini' file", ret_o)
        # run files ending in txt
        ret_c, ret_o = self.cmd('./print_ini file_end_txt.txt')
        self.assertTrue("Section [owner] Key name = John Doe\nSection [owner] Key organization = Acme Widgets Inc." == ret_o, 'check output error1.')
        # run with no header file
        ret_c, ret_o = self.cmd('./print_ini file_no_header.ini')
        self.assertTrue("Section [] Key name = John Doe\nSection [] Key organization = Acme Widgets Inc." == ret_o, 'check output error2.')
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf print_ini* file_end_txt.txt file_no_header.ini result.log')
