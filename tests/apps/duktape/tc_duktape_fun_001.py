#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_duktape_fun_001.py
@Time:      2024/04/18 17:11:50
@Author:    zhixin
@Version:   1.0
@Contact:   zx01468051@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhixin
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_duktape_fun_001.yaml for details

    :avocado: tags=fix,P3,noarch,local
    """
    PARAM_DIC = {"pkg_name": "duktape duktape-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > javascript_sim.c <<EOF
#include "duktape.h"
#include <stdio.h>

int main(void) {
    duk_context *ctx = duk_create_heap_default();
    if (!ctx) {
        printf("Failed to create a Duktape heap.\\n");
        return 1;
    }

    duk_eval_string(ctx, "123 + 456");
    if (duk_is_number(ctx, -1)) {
        printf("123 + 456 = %.6f\\n", duk_get_number(ctx, -1));
    } else {
        printf("The evaluation did not return a number!\\n");
    }

    duk_destroy_heap(ctx);
    return 0;
}
EOF"""
        self.cmd(cmdline)
    
    def test(self):
        self.cmd('gcc -o javascript_sim javascript_sim.c -lduktape')
        ret_c, ret_o = self.cmd('ldd ./javascript_sim')
        self.assertTrue('libduktape.so' in ret_o, 'check output error1.')
        ret_c, ret_o = self.cmd('./javascript_sim')
        self.assertTrue('123 + 456 = 579.000000' == ret_o, 'check output error2.')

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf javascript_sim*')
