#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_duktape_fun_003.py
@Time:      2024/04/25 10:31:50
@Author:    zhixin
@Version:   1.0
@Contact:   zx01468051@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhixin
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_duktape_fun_003.yaml for details

    :avocado: tags=fix,P3,noarch,local
    """
    PARAM_DIC = {"pkg_name": "duktape duktape-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > eval.c <<EOF
#include "duktape.h"
#include <stdio.h>

static duk_ret_t native_print(duk_context *ctx) {
        duk_push_string(ctx, " ");
        duk_insert(ctx, 0);
        duk_join(ctx, duk_get_top(ctx) - 1);
        printf("%s\\n", duk_to_string(ctx, -1));
        return 0;
}

static void usage_exit(void) {
        fprintf(stderr, "Usage: eval <expression> [<expression>] ...\\n");
        fflush(stderr);
        exit(1);
}

int main(int argc, char *argv[]) {
        duk_context *ctx;
        int i;
        const char *res;
        duk_int_t rc;

        if (argc < 2) {
                usage_exit();
        }

        ctx = duk_create_heap_default();

        duk_push_c_function(ctx, native_print, DUK_VARARGS);
        duk_put_global_string(ctx, "print");

        for (i = 1; i < argc; i++) {
                printf("=== eval: '%s' ===\\n", argv[i]);
                duk_push_string(ctx, argv[i]);
                rc = duk_peval(ctx);
                if (rc != 0) {
                        duk_safe_to_stacktrace(ctx, -1);
                } else {
                        duk_safe_to_string(ctx, -1);
                }
                res = duk_get_string(ctx, -1);
                printf("%s\\n", res ? res : "null");
                duk_pop(ctx);
        }

        duk_destroy_heap(ctx);

        return 0;
}
"""
        self.cmd(cmdline)
    
    def test(self):
        self.cmd('gcc -o eval eval.c -lduktape')
        ret_c, ret_o = self.cmd('ldd ./eval')
        self.assertTrue('libduktape.so' in ret_o, 'check output error1.')
        ret_c, ret_o = self.cmd('./eval 1+2')
        self.assertTrue("=== eval: '1+2' ===\n3" == ret_o, 'check output error2.')
        ret_c, ret_o = self.cmd('./eval null')
        self.assertTrue("=== eval: 'null' ===\nnull" == ret_o, 'check output error3.')
        ret_c, ret_o = self.cmd('./eval a+b')
        self.assertTrue("ReferenceError: identifier 'a' undefined" in ret_o, 'check output error4.')

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf eval eval.c')
