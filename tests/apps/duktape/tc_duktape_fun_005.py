#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_duktape_fun_005.py
@Time:      2024/04/25 10:51:50
@Author:    zhixin
@Version:   1.0
@Contact:   zx01468051@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhixin
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_duktape_fun_005.yaml for details

    :avocado: tags=fix,P3,noarch,local
    """
    PARAM_DIC = {"pkg_name": "duktape duktape-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > abnormal.c <<EOF
#include "duktape.h"
#include <stdio.h>

static duk_ret_t my_function(duk_context *ctx) {
    //... 一些错误发生
    return DUK_RET_ERROR;
}

int main(void) {
    duk_context *ctx = duk_create_heap_default();
    if (!ctx) {
        printf("Failed to create a Duktape heap.\\n");
        return 1;
    }

    // 注册函数
    duk_push_c_function(ctx, my_function, 0 /* nparams */);
    duk_put_global_string(ctx, "myFunction");

    // 调用函数
    if (duk_peval_string(ctx, "myFunction();") != 0) {
        printf("Caught an error from myFunction: %s\\n", duk_safe_to_string(ctx, -1));
    } else {
        printf("No error from calling myFunction\\n");
    }

    duk_destroy_heap(ctx);
    return 0;
}
"""
        self.cmd(cmdline)
    
    def test(self):
        self.cmd('gcc -o abnormal abnormal.c -lduktape')
        ret_c, ret_o = self.cmd('ldd ./abnormal')
        self.assertTrue('libduktape.so' in ret_o, 'check output error1.')
        ret_c, ret_o = self.cmd('./abnormal')
        self.assertTrue("Caught an error from myFunction:" in ret_o, 'check output error2.')

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf abnormal abnormal.c')
