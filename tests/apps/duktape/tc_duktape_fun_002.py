#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_duktape_fun_002.py
@Time:      2024/04/18 17:31:50
@Author:    zhixin
@Version:   1.0
@Contact:   zx01468051@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhixin
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_duktape_fun_002.yaml for details

    :avocado: tags=fix,P3,noarch,local
    """
    PARAM_DIC = {"pkg_name": "duktape duktape-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > hello.c <<EOF
#include "duktape.h"

static duk_ret_t native_print(duk_context *ctx) {
        duk_push_string(ctx, " ");
        duk_insert(ctx, 0);
        duk_join(ctx, duk_get_top(ctx) - 1);
        printf("%s\\n", duk_safe_to_string(ctx, -1));
        return 0;
}

static duk_ret_t native_adder(duk_context *ctx) {
        int i;
        int n = duk_get_top(ctx);  /* #args */
        double res = 0.0;

        for (i = 0; i < n; i++) {
                res += duk_to_number(ctx, i);
        }

        duk_push_number(ctx, res);
        return 1;  /* one return value */
}

int main(int argc, char *argv[]) {
        duk_context *ctx = duk_create_heap_default();

        (void) argc; (void) argv;  /* suppress warning */

        duk_push_c_function(ctx, native_print, DUK_VARARGS);
        duk_put_global_string(ctx, "print");
        duk_push_c_function(ctx, native_adder, DUK_VARARGS);
        duk_put_global_string(ctx, "adder");

        duk_eval_string(ctx, "print('Hello world!');");

        duk_eval_string(ctx, "print('2+3=' + adder(2, 3));");
        duk_pop(ctx);  /* pop eval result */

        duk_destroy_heap(ctx);

        return 0;
}
EOF"""
        self.cmd(cmdline)
    
    def test(self):
        self.cmd('gcc -o hello hello.c -lduktape')
        ret_c, ret_o = self.cmd('ldd ./hello')
        self.assertTrue('libduktape.so' in ret_o, 'check output error1.')
        ret_c, ret_o = self.cmd('./hello')
        self.assertTrue('Hello world!\n2+3=5' == ret_o, 'check output error2.')

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf hello*')
