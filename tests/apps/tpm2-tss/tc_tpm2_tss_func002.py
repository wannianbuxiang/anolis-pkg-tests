#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_tpm2_tss_func002.py
@Time:      2024/04/28 15:31:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_tpm2_tss_func002.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "tpm2-tss-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_tpm2_tss.c <<EOF
#include <stdio.h>
#include <tss2/tss2_common.h> // 或其他包含TSS2_ABI_VERSION定义的头文件

int main() {
    // 直接访问TSS2_ABI_VERSION_CURRENT宏来获取当前版本信息
    TSS2_ABI_VERSION currentVersion = TSS2_ABI_VERSION_CURRENT;

    printf("TPM2-TSS Library ABI Version:\\n");
    printf("Creator: %u\\n", currentVersion.tssCreator);
    printf("Family: %u\\n", currentVersion.tssFamily);
    printf("Level: %u\\n", currentVersion.tssLevel);
    printf("Version: %u\\n", currentVersion.tssVersion);

    return 0;
}
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o /tmp/ghm/test_tpm2_tss /tmp/ghm/test_tpm2_tss.c")
        ret_c, ret_o = self.cmd("/tmp/ghm/test_tpm2_tss")
        self.log.info(ret_o)
        self.assertIn("Creator: 1", ret_o)
        self.assertIn("Family: 2", ret_o)
        self.assertIn("Level: 1", ret_o)
        self.assertIn("Version: 108", ret_o)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)  
        self.cmd("rm -rf /tmp/ghm/")
  