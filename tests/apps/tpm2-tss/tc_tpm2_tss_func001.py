#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_tpm2_tss_func001.py
@Time:      2024/04/28 15:31:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

import subprocess
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_tpm2_tss_func001.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "tpm2-tss-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_tpm2_tss.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include <tss2/tss2_tcti.h>
#include <tss2/tss2_tctildr.h>
#include <tss2/tss2_mu.h>
#include <tss2/tss2_sys.h>
#include <tss2/tss2_esys.h>

int main() {
    TSS2_RC rc;
    TSS2_TCTI_CONTEXT *tctiContext = NULL;
    TSS2_SYS_CONTEXT *sysContext = NULL;
    size_t size;

    printf("TPM2-TSS Example Running\\n"); 

    //初始化TCTI
    rc = Tss2_TctiLdr_Initialize("tabrmd", &tctiContext);
    if (rc != TSS2_RC_SUCCESS) {
        fprintf(stderr, "Failed to initialize TCTI: 0x%x\\n", rc);
        return 1;
    }

    // 计算SYS上下文所需大小并分配
    size = Tss2_Sys_GetContextSize(0);
    sysContext = (TSS2_SYS_CONTEXT*)malloc(size);
    if (!sysContext) {
        fprintf(stderr, "Memory allocation failed\\n");
        Tss2_TctiLdr_Finalize(&tctiContext);
        return 1;
    }

    // 初始化SYS上下文
    rc = Tss2_Sys_Initialize(sysContext, size, tctiContext, NULL);
    if (rc != TSS2_RC_SUCCESS) {
        fprintf(stderr, "Failed to initialize Sys Context: 0x%x\\n", rc);
        free(sysContext);
        Tss2_TctiLdr_Finalize(&tctiContext);
        return 1;
    }

    printf("TPM2-TSS Context Initialized Successfully\\n");

    // 清理资源
    Tss2_Sys_Finalize(sysContext);
    free(sysContext);
    Tss2_TctiLdr_Finalize(&tctiContext);

    return 0;
}
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o /tmp/ghm/test_tpm2_tss /tmp/ghm/test_tpm2_tss.c -ltss2-esys -ltss2-tcti-mssim -ltss2-tcti-device -ltss2-tctildr -ltss2-sys")
        ret_o = subprocess.run("/tmp/ghm/test_tpm2_tss", capture_output=True, shell=True)
        self.log.info(ret_o)
        self.assertIn(b'TPM2-TSS Example Running', ret_o.stdout)
        self.assertIn(b'ERROR:tcti', ret_o.stderr)
        self.assertIn(b'Failed to initialize TCTI', ret_o.stderr)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)  
        self.cmd("rm -rf /tmp/ghm/")
  