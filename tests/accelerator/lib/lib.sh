#!/bin/bash

# 用于日志记录和测试结果的公共库

# 设置日志和报告文件
LOG_DIR="logs"
LOG_FILE="$LOG_DIR/test_results.log"
REPORT_FILE="$LOG_DIR/report.txt"

# 设置颜色
NO_COLOR='\033[0m'
GREEN='\033[0;32m'
RED='\033[0;31m'
YELLOW='\033[1;33m'

# 通用的日志函数
log_with_color() {
    local color="$1"
    local type="$2"
    local msg="$3"
    printf "%s [%b%s%b] %s\n" "$(date '+%Y-%m-%d %H:%M:%S')" "$color" "$type" "$NO_COLOR" "$msg"
}

# 打印信息日志
log_info() {
    log_with_color "" "INFO" "$1"
}

# 打印成功日志
log_success() {
    log_with_color "$GREEN" "SUCCESS" "$1"
}

# 打印错误日志
log_error() {
    log_with_color "$RED" "ERROR" "$1" 
}

# 打印警告日志
log_warning() {
    log_with_color "$YELLOW" "WARNING" "$1"
}


# 测试用例结果记录函数
record_test_case_result() {
    local testcase="$1"
    local result="$2"
    local logfile="$3"
    
    echo "${testcase}: ${result}" >> "${logfile}"
}

# 确保日志目录存在
ensure_log_directory() {
    local log_dir="./logs"
    if [ ! -d "$log_dir" ]; then
        mkdir -p "$log_dir"
    fi
}

# 获取日志文件路径
get_log_file() {
    local testcase="$1"
    local custom_log_file="$2"
    local current_script="$3"  # 新增参数

    # 如果函数调用时提供了自定义日志文件路径，则使用该路径
    if [ -n "$custom_log_file" ]; then
        echo "$custom_log_file"
    else
        # 如果没有提供自定义路径，使用默认的 ./logs 目录
        ensure_log_directory
        local log_dir="./logs"
        local script_name="$(basename "$current_script" .sh)"  # 使用当前脚本的文件名
        local log_file="${log_dir}/${script_name}.log"
        echo "$log_file"
    fi
}

# 定义测试结果输出函数
testcase_pass() {
    local testcase="$1"
    local custom_log_file="$2"
    local current_script="$0" 
    local log_file=$(get_log_file "$testcase" "$custom_log_file" "$current_script")
    record_test_case_result "$testcase" "Pass" "$log_file"
    log_success "${testcase}: Pass"
}
testcase_fail() {
    local testcase="$1"
    local custom_log_file="$2"
    local current_script="$0" 
    local log_file=$(get_log_file "$testcase" "$custom_log_file" "$current_script")
    record_test_case_result "$testcase" "Fail" "$log_file"
    log_error "${testcase}: Fail"
}

testcase_skip() {
    local testcase="$1"
    local custom_log_file="$2"
    local current_script="$0" 
    local log_file=$(get_log_file "$testcase" "$custom_log_file" "$current_script")
    record_test_case_result "$testcase" "Skip" "$log_file"
    log_warning "${testcase}: Skip"
}

# 从逗号分隔的URL列表下载所有RPM包到指定目录
download_rpms() {
    local rpm_urls=$1
    local dest_dir=$2

    # 确保目标目录存在
    mkdir -p "${dest_dir}"

    # 分割URL列表，并下载每个RPM包
    IFS=',' read -r -a url_array <<< "${rpm_urls}"
    for url in "${url_array[@]}"; do
        local rpm_name=$(basename "${url}")
        if curl -o "${dest_dir}/${rpm_name}" -fSL "${url}"; then
            log_success "下载RPM包成功: ${rpm_name}"
        else
            log_error "下载RPM包失败: ${url}"
            return 1
        fi
    done
}


