# 测试用例集成指南

本文档提供了如何在`accelerator-test`测试集中集成测试用例的指南。`runtest.sh` 是一个自动化测试执行脚本，用于运行、跳过特定测试用例，执行带有特定标签的用例。

## 测试用例命名规约

所有测试用例文件需要以 `_test.sh` 结束，并放置在项目根目录下的 `tests` 文件夹中。例如：

- 正确：`example_test.sh`
- 错误：`example.sh`, `test_example.sh`

## 测试用例集成步骤

1. 确保您的测试脚本文件名遵循上述命名规约。
2. 将测试脚本放置在项目根目录的 `tests` 文件夹中。
3. 在测试脚本中使用全局变量 `PKG_CI_ABS_RPM_URL` 来引用被测试的 RPM 包。环境变量 `PKG_CI_ABS_RPM_URL` 会在运行测试之前被设置，它包含了被测 RPM 包的 URL，例如：
    ```bash
   PKG_CI_ABS_RPM_URL="https://abs.openanolis.cn/download/OpenAnolis/opa-fm-10.12.1.0.6-1.0.1.an8.src.rpm,https://abs.openanolis.cn/download/OpenAnolis/opa-fm/opa-fm-10.12.1.0.6-1.0.1.an8.x86_64.rpm"
   ```

   ```bash
   # 在测试脚本中使用全局变量
   IFS=',' read -r -a rpm_urls <<< "$PKG_CI_ABS_RPM_URL"
   for rpm_url in "${rpm_urls[@]}"; do
       echo "Testing RPM package: $rpm_url"
       # 根据 rpm_url 进行后续测试动作
       # ...
   done
   ```
5. 测试用例的输出结果需要遵循 `key: value` 的格式规范。对于功能测试，`key` 应为 `测试用例名称`，而 `value` 应为以下之一：
   - `Pass`：测试用例通过。
   - `Fail`：测试用例失败。
   - `Skip`：测试用例跳过。

    例如：

   ```bash
   Test_case_name: Pass
   ```
   请确保测试结果输出到 `logs` 文件夹下对应的日志文件中，文件名应与测试脚本名称一致，例如 `logs/example_test.log`

## 运行测试用例

执行以下命令运行所有测试用例：

```bash
./runtest.sh -d ./tests
```
执行以下命令运行指定测试用例：

```bash
./runtest.sh -d ./tests -t example1_test.sh -t example2_test.sh
```
执行以下命令运行指定标签的测试用例：

```bash
./runtest.sh -d ./tests -o tag1 
