#!/bin/bash

source ./lib/lib.sh

rpm_dir="rpms"

download_rpms "$PKG_CI_ABS_RPM_URL" "$rpm_dir"

yum install -y $rpm_dir/*

check_and_install_dependencies()
{
	for package_name in "$@"; do
		log_info "Checking package: $package_name"
		rpm -q $package_name || yum install -y $package_name
	done
}

dependencies() {
	check_and_install_dependencies kmod-intel-QAT20 intel-QAT20 kmod-udma udma
	log_info "dependencies installed"
}

dependencies

modprobe intel_qat
modprobe udma_drv
service qat_service restart

adf_ctl status
if [[ $? -ne 0 ]]; then
	log_error "adf_ctl status failed"
	testcase_fail "adf_ctl"
	exit 1
fi

testcase_pass "adf_ctl_status"

if [[ ! -z "$(lscpu | grep 'BIOS Model name' | grep -q pc-i440fx)" ]]; then
	log_info "qatzip test runs in a VM"
	testcase_skip "create_vf"
	sed -i 's/SSL/SHIM/' /etc/vqat-adi_dev1.conf
else
	adf_info=$(adf_ctl status | grep 'state: up' | head -n 1)
	log_info "adf_info: $adf_info"
	bdf=$(echo $adf_info | awk '{print $10}' | tr -d ',')
	log_info "bdf: $bdf"
	if [[ -z "$bdf" ]]; then
		log_error "adf_ctl cannot get bdf"
		testcase_fail "adf_ctl"
		exit 1
	fi

	# add vf
	echo 1 > /sys/bus/pci/devices/$bdf/sriov_numvfs

	adfvf_info=$(adf_ctl status | grep 'state: up' | grep 'vf' | head -n 1)
	log_info "adfvf_info: $adfvf_info"
	if [[ -z "$adfvf_info" ]]; then
		log_error "cannot get vfinfo"
		testcase_fail "create_vf"
		exit 1
	fi
	testcase_pass "create_vf"

	vf_dev=$(echo $adfvf_info | awk '{print $1}' | tr -d -c '[:digit:]')
	inst_id=$(echo $adfvf_info | awk '{print $6}' | tr -d ',')

	log_info "inst_id: $inst_id, vf_dev: $vf_dev"
	service_conf=$(cat /etc/4xxx_dev$inst_id.conf | grep ServicesEnabled)
	cat > /etc/4xxxvf_dev$inst_id.conf <<EOF
################################################################
# This file is provided under a dual BSD/GPLv2 license.  When using or
#   redistributing this file, you may do so under either license.
# 
#   GPL LICENSE SUMMARY
# 
#   Copyright(c) 2007-2022 Intel Corporation. All rights reserved.
# 
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of version 2 of the GNU General Public License as
#   published by the Free Software Foundation.
# 
#   This program is distributed in the hope that it will be useful, but
#   WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
#   The full GNU General Public License is included in this distribution
#   in the file called LICENSE.GPL.
# 
#   Contact Information:
#   Intel Corporation
# 
#   BSD LICENSE
# 
#   Copyright(c) 2007-2022 Intel Corporation. All rights reserved.
#   All rights reserved.
# 
#   Redistribution and use in source and binary forms, with or without
#   modification, are permitted provided that the following conditions
#   are met:
# 
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in
#       the documentation and/or other materials provided with the
#       distribution.
#     * Neither the name of Intel Corporation nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
# 
#   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
# 
#  version: QAT20.L.0.9.4-00004
################################################################
[GENERAL]
$service_conf
#ServicesEnabled = asym;dc

ConfigVersion = 2

#Default values for number of concurrent requests*/
CyNumConcurrentSymRequests = 512
CyNumConcurrentAsymRequests = 64

#Statistics, valid values: 1,0
statsGeneral = 1
statsDh = 1
statsDrbg = 1
statsDsa = 1
statsEcc = 1
statsKeyGen = 1
statsDc = 1
statsLn = 1
statsPrime = 1
statsRsa = 1
statsSym = 1

# This flag is to enable SSF features (CNV)
StorageEnabled = 0

# Disable public key crypto and prime number
# services by specifying a value of 1 (default is 0)
PkeServiceDisabled = 0

# This flag is to enable device auto reset on heartbeat error
AutoResetOnError = 0

SVMEnabled = 0

##############################################
# Kernel Instances Section
##############################################
[KERNEL]
NumberCyInstances = 0
NumberDcInstances = 0

##############################################
# User Process Instance Section
##############################################
[SHIM]
NumberCyInstances = 2
NumberDcInstances = 2
NumProcesses = 1
LimitDevAccess = 1

# Crypto - User instance #0
Cy0Name = "SSL0"
Cy0IsPolled = 2
# List of core affinities
Cy0CoreAffinity = 1

# Crypto - User instance #1
Cy1Name = "SSL1"
Cy1IsPolled = 2
# List of core affinities
Cy1CoreAffinity = 2

# Data Compression - User instance #0
Dc0Name = "Dc0"
Dc0IsPolled = 2
# List of core affinities
Dc0CoreAffinity = 1

# Data Compression - User instance #1
Dc1Name = "Dc1"
Dc1IsPolled = 2
# List of core affinities
Dc1CoreAffinity = 2
EOF
	adf_ctl qat_dev$vf_dev restart
fi

data_size=10M
tmpfile=$(mktemp)
head -c $data_size /dev/urandom > $tmpfile
checksum1=$(sha256sum $tmpfile | awk '{print $1}')
log_info "checksum1: $checksum1"
qzip -V

qzip $tmpfile 2>&1 | tee result_compression.tmp
cat result_compression.tmp | grep -q "Error" && testcase_skip "qat_hw_compression" || testcase_pass "qat_hw_compression"
qzip -d $tmpfile.gz 2>&1 | tee result_decompression.tmp
cat result_decompression.tmp | grep -q "Error" && testcase_skip "qat_hw_decompression" || testcase_pass "qat_hw_decompression"

checksum2=$(sha256sum $tmpfile | awk '{print $1}')
log_info "checksum2: $checksum2"
rm -rf $tmpfile
if [[ "$checksum1" == "$checksum2" ]]; then
	log_success "qatzip random test success"
	testcase_pass "qatzip_random"
else
	log_error "qatzip random test failed"
	testcase_fail "qatzip_random"
fi
