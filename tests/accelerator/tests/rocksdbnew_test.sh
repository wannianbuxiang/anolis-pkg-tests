#!/bin/bash
# set -x

# grubby --update-kernel=ALL --args=""
GITHUB_PROXY=https://github.moeyy.xyz/
prepare() {
	
	yum install -y zlib zlib-devel binutils binutils-gold
	yum install -y git accel-config
	git clone --branch a23 https://gitee.com/src-anolis-os/qpl.git
	git clone --branch a23 https://gitee.com/src-anolis-sig/udma.git
	pushd udma
	tar -xzvf udma-0.1.1.tar.gz
	cd udma-0.1.1
	mkdir -p /usr/include/udma
	install -D -m 644 x86_64/usr/include/udma/udma.h /usr/include/udma/udma.h
	install -D -m 755 x86_64/usr/lib64/libudma.so.0.1.1 /usr/lib64/libudma.so.0.1.1
	popd
	yum install -y nasm libuuid libuuid-devel uuid uuid-devel gtest-devel gmock-devel rpm-build cmake g++
	pushd qpl
	sed -i '/udma-devel/d' qpl.spec
	sed -i '/%generate_compatibility_deps/d' qpl.spec
	sed -i '/%{abidir}/d' qpl.spec
	mkdir -p $HOME/rpmbuild
	rm -rf $HOME/rpmbuild/SOURCES
	mkdir -p $HOME/rpmbuild/SOURCES
	cp * $HOME/rpmbuild/SOURCES
	rpmbuild -ba qpl.spec
	popd

	yum install -y /root/rpmbuild/RPMS/x86_64/qpl-1.1.0-1.al8.x86_64.rpm /root/rpmbuild/RPMS/x86_64/qpl-devel-1.1.0-1.al8.x86_64.rpm
	# yum reinstall -y /root/rpmbuild/RPMS/x86_64/qpl-1.1.0-1.al8.x86_64.rpm /root/rpmbuild/RPMS/x86_64/qpl-devel-1.1.0-1.al8.x86_64.rpm
	yum install -y java

	CMAKE_FLAGS=""
	yum install -y snappy snappy-devel
	[[ "$?" == "0" ]] && CMAKE_FLAGS="$CMAKE_FLAGS -DWITH_SNAPPY=1"
	yum install -y zlib zlib-devel
	[[ "$?" == "0" ]] && CMAKE_FLAGS="$CMAKE_FLAGS -DWITH_ZLIB=1"
	yum install -y bzip2 bzip2-devel
	[[ "$?" == "0" ]] && CMAKE_FLAGS="$CMAKE_FLAGS -DWITH_BZ2=1"
	yum install -y lz4-devel
	[[ "$?" == "0" ]] && CMAKE_FLAGS="$CMAKE_FLAGS -DWITH_LZ4=1"
	yum install -y zstd libzstd-devel
	[[ "$?" == "0" ]] && CMAKE_FLAGS="$CMAKE_FLAGS -DWITH_ZSTD=1"

	yum install -y gflags gflags-devel g++
	[[ -d rocksdb ]] || git clone --branch pluggable_compression_filedata "$GITHUB_PROXY"https://github.com/lucagiac81/rocksdb.git
	pushd rocksdb

	git clone "$GITHUB_PROXY"https://github.com/intel/iaa-plugin-rocksdb.git plugin/iaa_compressor
	pushd plugin/iaa_compressor
	git checkout v0.1.1
	sed -i 's/decompression_huffman_table/huffman_table/g' iaa_compressor.cc
	sed -i 's/compression_huffman_table/huffman_table/g' iaa_compressor.cc
	sed -i 's/iaa_compressor_reg$/iaa_compressor_reg -ludma/g' iaa_compressor.mk
	popd

	ROCKSDB_CXX_STANDARD="c++17" DISABLE_WARNING_AS_ERROR=1 DEBUG_LEVEL=2 ROCKSDB_PLUGINS="iaa_compressor" make -j $(nproc) release
	popd

	cat >iax.conf <<EOF
[
  {
    "dev": "iax1",
    "groups": [
      {
        "dev": "group1.0",
        "grouped_workqueues": [
          {
            "dev": "wq1.0",
            "type": "user",
            "mode": "dedicated",
            "size": 16,
            "group_id": 0,
            "priority": 10,
            "max_transfer_size": 4194304,
            "name": "iax10",
            "block_on_fault":0,
            "driver_name": "user"
          }
        ],
        "grouped_engines": [
          {
            "dev": "engine1.0",
            "group_id": 0
          }
        ]
      }
    ]
  }
]
EOF
	accel-config load-config -c iax.conf -ve

	./rocksdb/db_bench --benchmarks=fillseq --compression_type=com.intel.iaa_compressor_rocksdb --compressor_options="execution_path=hw" --threads=1
	./rocksdb/db_bench --benchmarks=fillseq --compression_type=com.intel.iaa_compressor_rocksdb --compressor_options="execution_path=sw" --threads=1
}

compression_test() {
	local alg=$1
	local result=$2
	export TEST_TMPDIR=$tmpbase/test
	# CACHE=268435456
	CACHE=-1
	NUM=5000000
	READS=2000000
	TIME="/usr/bin/time -o $result/"$alg"-time.log -f Usr\tSys\t%%\tWall\tRSS\tMajor\tMinor\tVolun\tInvol\tIn\tOut\n%U\t%S\t%P\t%E\t%M\t%F\t%R\t%w\t%c\t%I\t%O"
	CMD="./rocksdb/db_bench --benchmarks=readseq --cache_size=$CACHE --num=$NUM --reads=$READS --stats_interval_seconds=60 --disable_auto_compactions=1 --use_existing_db=1 --bloom_bits=10 --block_size=4096 --key_size=16 --value_size=32 --threads=8 --compression_ratio=0.25 --verify_checksum=0 --db=$TEST_TMPDIR"
	CMDRAND="./rocksdb/db_bench --benchmarks=readrandom --cache_size=$CACHE --num=$NUM --reads=$READS --stats_interval_seconds=60 --disable_auto_compactions=1 --use_existing_db=1 --bloom_bits=10 --block_size=4096 --key_size=16 --value_size=32 --threads=8 --compression_ratio=0.25 --verify_checksum=0 --db=$TEST_TMPDIR"
	rm -r $TEST_TMPDIR
	if [[ "$alg" == "iaa_s" ]]; then
		options="--compression_type=com.intel.iaa_compressor_rocksdb --compressor_options=execution_path=sw"
	elif [[ "$alg" == "iaa_h" ]]; then
		options="--compression_type=com.intel.iaa_compressor_rocksdb --compressor_options=execution_path=hw"
	else
		options="--compression_type=$alg"
	fi
	POLYZ=$alg
	./rocksdb/db_bench --benchmarks=fillseq --cache_size=$CACHE --num=$NUM --stats_interval_seconds=60 --disable_auto_compactions=1 --block_size=4096 --key_size=16 --value_size=32 --bloom_bits=10 --db=$TEST_TMPDIR $options >>$result/"$alg".log
	du $TEST_TMPDIR >>$result/"$alg".log
	echo "COMPRESS=$alg"
	$TIME $CMD $options >>$result/"$alg".log
	du $TEST_TMPDIR >>$result/"$alg"-du.log
	$CMDRAND $options >>$result/"$alg".log
}

read_compresion_result() {
	local alg=$1
	local result=$2
	local log=$result/"$alg".log
	local dulog=$result/"$alg"-du.log
	local stats=$result/"$alg"-time.log

	local usr=$(cat $stats | awk 'NR==2{print $1}')
	local sys=$(cat $stats | awk 'NR==2{print $2}')
	local total=$(cat $stats | awk 'NR==2{print $3}')
	local walltime=$(cat $stats | awk 'NR==2{print $4}')
	local RSS=$(cat $stats | awk 'NR==2{print $5}')

	local fillseq_ops=$(cat $log | grep fillseq | awk '{print $5}')
	local fillseq_throughput=$(cat $log | grep fillseq | awk '{print $11}')

	local readrandom_ops=$(cat $log | grep readrandom | awk '{print $5}')
	local readrandom_throughput=$(cat $log | grep readrandom | awk '{print $11}')

	local readseq_ops=$(cat $log | grep readseq | awk '{print $5}')
	local readseq_throughput=$(cat $log | grep readseq | awk '{print $11}')

	local size=$(tail -n 1 $dulog | awk 'NR==1{print $1}')
	echo "$alg,$fillseq_ops,$fillseq_throughput,$readrandom_ops,$readrandom_throughput,$readseq_ops,$readseq_throughput,$usr,$sys,$total,$walltime,$RSS,$size"
}

run_comression_test() {
	local result="compression_result"
	[[ -d $result ]] && rm -rf $result
	mkdir $result
	# for alg in none iaa_s iaa_h zstd lz4 snappy zlib bzip2; do
	for alg in iaa_h; do
		echo
		echo "======================" $alg "======================"
		compression_test $alg $result
		echo "======================" $alg "======================"
		echo
	done

	# echo "alg,fillseq_ops,fillseq_throughput,readrandom_ops,readrandom_throughput,readseq_ops,readseq_throughput,usr,sys,total,walltime,RSS,size" | tee summary.csv
	# for alg in none iaa_s iaa_h zstd lz4 snappy zlib bzip2; do
	# 	read_compresion_result $alg $result | tee -a summary.csv
	# done
}

SRAM=True
if [[ "$SRAM" == "True" ]]; then
	tmpbase=/sram
	[[ -d $tmpbase ]] || mkdir $tmpbase
	mount -t tmpfs tmpfs $tmpbase
else
	tmpbase=$(mktemp -d)
fi

prepare
run_comression_test

# grubby --update-kernel=ALL --args="intel_iommu=on,sm_on"


