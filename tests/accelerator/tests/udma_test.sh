#!/bin/bash

source ./lib/lib.sh

rpm_dir="rpms"

download_rpms "$PKG_CI_ABS_RPM_URL" "$rpm_dir"

yum install -y $rpm_dir/*

check_and_install_dependencies()
{
	for package_name in "$@"; do
		log_info "Checking package: $package_name"
		rpm -q $package_name || yum install -y $package_name
	done
}

dependencies() {
	check_and_install_dependencies kmod-udma gcc
	log_info "dependencies installed"
}

dependencies

modprobe udma_drv


cat > main.c << EOF
/*
 *	Copyright(c) 2022-2024 Aliyun Group. All rights reserved
 */
#include "udma.h"
#include <stdio.h>

int main(int argc, char **argv) {
    int ret;
    void* addr;
    unsigned long long phy;

    ret = udma_init();
    if (ret != 0) {
        printf("udma_init failed\n");
        return -1;
    }

    addr = udma_alloc(4096 * 1024, 1024, 0, NORMAL);
    if (addr == NULL) {
        printf("udma_alloc failed\n");
        return -1;
    }

    phy = virt2dma(addr);
    if (phy == 0) {
        printf("virt2dma failed\n");
        return -1;
    }

    printf("phy addr: %llx\n", phy);

    udma_free(addr);
    return 0;
}
EOF
gcc main.c -o main -I/usr/include/udma -L/usr/lib64 -ludma -lpthread
[[ $? -eq 0 ]] && testcase_pass "udma test compiled passed" || testcase_fail "udma test compiled failed"

./main && testcase_pass "udma test passed" || testcase_fail "udma test failed"
