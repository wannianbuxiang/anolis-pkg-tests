#!/bin/bash

source ./lib/lib.sh

rpm_dir="rpms"

download_rpms "$PKG_CI_ABS_RPM_URL" "$rpm_dir"

yum install -y $rpm_dir/*

modprobe udma_drv && testcase_pass "udma_drv probe passed" || testcase_fail "udma_drv probe failed"

[[ -c /dev/udma_drv ]] && testcase_pass "udma_drv device exists" || testcase_fail "udma_drv device does not exist"
