# -*- encoding: utf-8 -*-

"""
@File:      tc_e2fsprogs_mke2fs.py
@Time:      2024/7/19 20:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_e2fsprogs_mke2fs.yaml for details

    :avocado: tags=P1,noarch,local,e2fsprogs
    """
    PARAM_DIC = {"pkg_name": "e2fsprogs"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        
    def test(self):
        self.cmd("cd /tmp")
        self.cmd("dd if=/dev/zero of=/tmp/100k bs=1k count=100")
        code, result1=self.cmd("ls /tmp/100k")
        self.assertIn("/tmp/100k", result1)
        self.cmd("losetup /dev/loop0 /tmp/100k")
        self.cmd("mke2fs -I 128 /tmp/100k")
        self.cmd("file /tmp/100k > /tmp/100k_test1")
        code, result2=self.cmd('grep "ext2 filesystem data" /tmp/100k_test1')
        self.assertIn("ext2 filesystem data", result2)
        self.cmd("mkdir /opt/test")
        self.cmd("mount /tmp/100k /opt/test")
        self.cmd("df -h")
        self.cmd("df -h | grep /opt/test > /tmp/100k_test2")
        code, result3=self.cmd('grep "/dev/loop0" /tmp/100k_test2')
        self.assertIn("/dev/loop0", result3)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("umount /opt/test")
        self.cmd("losetup -d /dev/loop0")
        self.cmd("rm -rf /tmp/100k /tmp/100k_test1 /tmp/100k_test2 /opt/test")
