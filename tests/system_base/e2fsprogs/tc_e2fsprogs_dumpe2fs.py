# -*- encoding: utf-8 -*-

"""
@File:      tc_e2fsprogs_dumpe2fs.py
@Time:      2024/7/19 20:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_selinux_selinux_fun001.yaml for details

    :avocado: tags=P1,noarch,local,e2fsprogs
    """
    PARAM_DIC = {"pkg_name": "e2fsprogs"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        
    def test(self):
        self.cmd("cd /tmp")
        self.cmd("dd if=/dev/zero of=/tmp/dumpe2fs_100k bs=1k count=100")
        self.cmd("mke2fs -I 128 /tmp/dumpe2fs_100k -F")
        self.cmd("file /tmp/dumpe2fs_100k > /tmp/dumpe2fs_100k_test1")
        code, result1=self.cmd("grep 'ext2 filesystem data' /tmp/dumpe2fs_100k_test1")
        self.assertIn("ext2 filesystem data", result1)
        self.cmd("dumpe2fs /tmp/dumpe2fs_100k > /tmp/dumpe2fs_100k_test2")
        code, result2=self.cmd("grep 'Filesystem volume name' /tmp/dumpe2fs_100k_test2")
        self.assertIn("Filesystem volume name", result2)
        self.cmd("dumpe2fs /tmp/dumpe2fs_100k  |grep -i 'inode size' > /tmp/dumpe2fs_100k_test3")
        code, result3=self.cmd("grep '128' /tmp/dumpe2fs_100k_test3")
        self.assertIn("128", result3)
        self.cmd('dumpe2fs /tmp/dumpe2fs_100k  |grep -i "block size" > /tmp/dumpe2fs_100k_test4')
        code, result4=self.cmd("grep '1024' /tmp/dumpe2fs_100k_test4")
        self.assertIn("1024", result4)
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/dumpe2fs_100k /tmp/dumpe2fs_100k_test1 /tmp/dumpe2fs_100k_test2 /tmp/dumpe2fs_100k_test3 /tmp/dumpe2fs_100k_test4")
