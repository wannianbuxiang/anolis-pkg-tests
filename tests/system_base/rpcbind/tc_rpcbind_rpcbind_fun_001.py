# -*- encoding: utf-8 -*-

"""
@File:      tc_rpcbind_rpcbind_fun_001.py
@Time:      2024/7/26 10:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_rpcbind_rpcbind_fun_001.yaml for details

    :avocado: tags=P1,noarch,local,rpcbind
    """
    PARAM_DIC = {"pkg_name": "procps-ng net-tools nfs-utils rpcbind"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        
    def test(self):
        self.cmd("systemctl start rpcbind")
        self.cmd("systemctl start nfs-server")
        code, result1=self.cmd("ps aux | grep rpcbind")
        self.assertIn("/usr/bin/rpcbind", result1)
        code, result2=self.cmd("netstat -tulnp | grep rpc | grep tcp | grep 20048")
        self.assertIn("20048", result2)
        code, result3=self.cmd("rpcinfo -p localhost | grep 20048")
        self.assertIn("20048", result3)
        code, result4=self.cmd("ps aux | grep rpcbind")
        self.assertIn("/usr/bin/rpcbind", result4)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("systemctl stop nfs-server")
