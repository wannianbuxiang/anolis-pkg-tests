# -*- encoding: utf-8 -*-

"""
@File:      tc_iptables_iptables_fun_002.py
@Time:      2024/7/22 17:00:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_iptables_iptables_fun_002.yaml for details

    :avocado: tags=P1,noarch,local,iptables
    """
    PARAM_DIC = {"pkg_name": "iptables"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("iptables-save > /tmp/iptables_backup.txt")

    def test(self):
       self.cmd("iptables -F")        	
       self.cmd("iptables -A INPUT -i lo -j ACCEPT")
       code, result=self.cmd("iptables -vL | grep -A 20 INPUT")
       self.assertIn("lo", result)  
       self.cmd("iptables -F")        	
       self.cmd("iptables -A OUTPUT -o lo -j ACCEPT")
       code, result=self.cmd("iptables -vL | grep -A 20 OUTPUT")
       self.assertIn("lo", result)  
       self.cmd("iptables -F")        	
       self.cmd("iptables -A INPUT -i lo -j DROP")
       code, result=self.cmd("iptables -vL | grep -A 20 DROP")
       self.assertIn("lo", result)        
       self.cmd("iptables-save > /tmp/rules.txt")
       self.cmd("cat /tmp/rules.txt |grep DROP")    

    def tearDown(self):
        # 恢复原来的iptables规则
        self.cmd("iptables-restore < /tmp/iptables_backup.txt")
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/rules.txt /tmp/iptables_backup.txt")
