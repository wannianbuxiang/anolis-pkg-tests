# -*- encoding: utf-8 -*-

"""
@File:      tc_iptables_iptables_fun001.py
@Time:      2024/7/22 17:00:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_iptables_iptables_fun001.yaml for details

    :avocado: tags=P1,noarch,local,iptables
    """
    PARAM_DIC = {"pkg_name": "iptables"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        
    def test(self):        	
        code, result = self.cmd("iptables --version")
        self.assertIn("iptables", result)
        self.cmd("find -help > /tmp/iptables.log", ignore_status=True)
        code, result = self.cmd("cat /tmp/iptables.log", ignore_status=True)
        self.assertIn("Usage:", result)
      
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/iptables.log")
