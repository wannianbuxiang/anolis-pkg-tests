#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

"""
@File:      tc_xz_unxz_fun_003.py
@Time:      2024/07/15 14:07:20
@Author:    wangge
@Version:   1.0
@Contact:   YS.wangge@h3c.com
@License:   Mulan PSL v2
@Modify:    wangge
"""

import os

from common.basetest import LocalTest



class Test(LocalTest):
    """
    See tc_xz_unxz_fun_003.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "xz"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd('echo "hello world" >testxz')
        self.cmd('echo "hello world" >testxz1')
        self.cmd('echo -e "testxz\ntestxz1" >testfile')
        self.cmd("xz -k -f testxz")
        self.cmd("xz -k -f testxz1")
        self.cmd("xz -k -f -F lzma testxz")


    def test(self):
        code, result = self.cmd("unxz -z -k -f -vv -S .SUF testxz")
        self.assertEqual(code, 0, "Test failed with option -S")

        code, _ = self.cmd("test -f testxz.SUF")
        self.assertEqual(0, code, "testxz.SUF does not exist")

        code, result = self.cmd('unxz -k -f -vv --files=testfile 2>&1 | grep -A 10 "testxz" | grep "testxz1"')
        self.assertEqual(code, 0, "Test failed with option --files")
        self.assertIn("testxz1", result, "testxz1 not found in output")

        code, result = self.cmd("unxz -k -f -vv -F lzma testxz.lzma")
        self.assertEqual(code, 0, "Test failed with option -F")
        self.assertTrue(os.path.isfile("testxz"), "testxz.lzma does not exist")

        code, result = self.cmd("unxz -z -k -f -vv -C crc32 testxz")
        self.assertEqual(code, 0, "Test failed with option -C")

        code, result = self.cmd("unxz -z -l testxz.xz | grep CRC32")
        self.assertEqual(code, 0, "Test failed with option -l")
        self.assertIn("CRC32", result)

        code, result = self.cmd('unxz -z -k -f -vv --block-size=1 testxz 2>&1 | grep "392 B / 12 B > 9.999"')
        self.assertEqual(code, 0, "Test failed with option --block-size")
        self.assertIn("392 B / 12 B > 9.999", result)

        code, result = self.cmd('unxz -z -k -f -vv --block-list=1,2,128 testxz 2>&1 | grep "128 B / 12 B > 9.999"')
        self.assertEqual(code, 0, "Test failed with option --block-list")
        self.assertIn("128 B / 12 B > 9.999", result)


        code, result = self.cmd('unxz -z -k -f -vv -F lzma --lzma1=dict=7MiB,lc=1,lp=1,pb=3,mode=normal,nice=128,mf=bt3,depth=1 testxz 2>&1 | grep "lzma1=dict=7MiB,lc=1,lp=1,pb=3,mode=normal,nice=128,mf=bt3,depth=1"')
        self.assertEqual(code, 0, "Test failed with option --lzma1")
        self.assertIn("lzma1=dict=7MiB,lc=1,lp=1,pb=3,mode=normal,nice=128,mf=bt3,depth=1", result)

        code, result = self.cmd('unxz -z -k -f -vv --lzma2=dict=6MiB,lc=2,lp=2,pb=4,mode=fast,nice=256,mf=bt2,depth=2 testxz 2>&1 | grep "lzma2=dict=6MiB,lc=2,lp=2,pb=4,mode=fast,nice=256,mf=bt2,depth=2"')
        self.assertEqual(code, 0, "Test failed with option --lzma2")
        self.assertIn("lzma2=dict=6MiB,lc=2,lp=2,pb=4,mode=fast,nice=256,mf=bt2,depth=2", result)


 
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf testxz* testfile")
        
        