#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

"""
@File:      tc_xz_unxz_fun_001.py
@Time:      2024/07/15 14:07:20
@Author:    wangge
@Version:   1.0
@Contact:   YS.wangge@h3c.com
@License:   Mulan PSL v2
@Modify:    wangge
"""
import os
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_xz_unxz_fun_001.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "xz"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("touch testxz")
        self.cmd("touch testlog")
        self.cmd("echo 'hello world' > testxz")

    def test(self):
        code, result = self.cmd("unxz -z testxz")
        self.assertEquals(0, code, f"123Compression failed with return code {code}")
        
        
        code, result = self.cmd("unxz -k -d testxz.xz")
        self.assertEquals(0, code, f"Decompression failed with return code {code}")
        
        code, result = self.cmd("cat testxz")
        self.assertEquals(0, code, f"Read file failed with return code {code}")
        self.assertIn("hello world", result, "File content mismatch")
        
        
        code, result = self.cmd('unxz -t -vv testxz.xz 2>&1 | grep "68 B / 12 B = 5.667"')
        self.assertEquals(0, code, "Test failed with option -t")
        self.assertIn("68 B / 12 B = 5.667", result, "Test failed with option -t") 

        code, result = self.cmd("unxz -l -vv testxz.xz")
        self.assertEquals(0, code, "Test failed with option -l")
        self.assertTrue("Stream" in result or u"流" in result, "Test failed, 'Stream' or '流' not found in output")
        
        code, result = self.cmd("rm -rf testxz && unxz -k testxz.xz")
        self.assertEquals(0, code, "Test failed with option -k")
        
        code, result = self.cmd("test -f testxz.xz")
        self.assertEquals(0, code, "File retention failed")
        
        code, result = self.cmd("unxz -k -f testxz.xz")
        self.assertEquals(0, code, "Test failed with option -f")
        
        code, result = self.cmd("test -f testxz")
        self.assertEquals(0, code, "File overwrite failed")
        
        code, result = self.cmd("unxz -k -f -c -d testxz.xz")
        self.assertEquals(0, code, "Test failed with option -c")
        self.assertIn("hello world", result, "Missing 'hello world' in decompressed content")
        
        code, result = self.cmd('unxz -z -k -f -e -vv testxz 2>&1 | grep "depth=512"')
        self.assertEquals(0, code, "Test failed with option -e")
        self.assertIn("depth=512", result, "Missing 'depth=512' in verbose output with option -e")

        code, result = self.cmd('unxz -z -k -f -T 0 -vv textxz 2>&1 | grep "nice=64"')
        self.assertEquals(0, code, "Test failed with option -T")
        self.assertIn("nice=64", result, "Missing 'nice=64' in verbose output with option -T")
        
        code, result = self.cmd("unxz -k -f -q testxz.xz >testlog 2>&1")
        self.assertEquals(0, code, "Test failed with option -q")
        
        if os.path.exists("testlog"):
            file_size = os.path.getsize("testlog")
            self.assertEquals(0, file_size, "The file 'testlog' is not empty")
        else:
            self.fail("The file 'testlog' does not exist")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -f testxz testxz.xz testlog")
        
        