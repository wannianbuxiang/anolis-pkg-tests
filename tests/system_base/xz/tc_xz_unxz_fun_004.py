#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

"""
@File:      tc_xz_unxz_fun_004.py
@Time:      2024/07/15 14:07:20
@Author:    wangge
@Version:   1.0
@Contact:   YS.wangge@h3c.com
@License:   Mulan PSL v2
@Modify:    wangge
"""

import subprocess
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_xz_unxz_fun_004.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "xz"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("touch testxz")
        self.cmd("echo 'hello world' > testxz")
        try:
            result = subprocess.run(['rpm', '-qa', 'xz'], capture_output=True, text=True, check=True)
            if result.stdout:
                xz_version = result.stdout.strip().split('-')[1]  # 假设版本号是第2个元素
                self.xz_version = xz_version
            else:
                self.xz_version = None
        except subprocess.CalledProcessError as e:
            print(f"Error occurred: {e}")
            self.xz_version = None
        

    def test(self):
        code, result = self.cmd("unxz -z -k -f -vv testxz 2>&1")
        self.assertEqual(code, 0, "Test failed with option -vv")
        
        code, result = self.cmd('unxz -k -f -v testxz.xz 2>&1')
        self.assertEqual(code, 0, "Test failed with option -v")
        self.assertNotIn("Filter chain", result, "Filter chain should not appear with option -v")

        code, result = self.cmd("unxz -k -f --info-memory testxz")
        self.assertEqual(code, 0, "Test failed with option --info-memory")
        self.assertTrue(any(x in result for x in ["Memory usage limit", u"内存用量限制"]),
                        "Neither 'Memory usage limit' nor u'内存用量限制' found in output of option --memlimit-compress")

        code, result = self.cmd("unxz -h")
        self.assertEqual(code, 0, "Test failed with option -h")
        self.assertTrue(any(substring in result for substring in ["Usage: unxz", "用法：unxz"]),
                        f"Neither 'Usage: unxz' nor '用法： unxz' found in output of option -h. Output was:\n{result}")
        
        code, result = self.cmd("unxz -H")
        self.assertEqual(code, 0, "Test failed with option -H")
        self.assertTrue(any(substring in result for substring in ["Usage: unxz", "用法：unxz"]),
                        f"Neither 'Usage: unxz' nor '用法： unxz' found in output of option -h. Output was:\n{result}")
        self.assertTrue(any(substring in result for substring in ["long options", "长短选项同时适用"]),
                        f"Neither 'long options' nor '长短选项同时适用' found in output of option -H. Output was:\n{result}")

        xz_version = self.xz_version if self.xz_version else ""
        code, result = self.cmd("unxz -V")
        self.assertEqual(code, 0, "Test failed with option -V")
        self.assertIn(xz_version, result, f"{xz_version} not found with option -V")

        code, result = self.cmd("unxz -k -f -M3 --info-memory testxz")
        self.assertEqual(code, 0, "Test failed with option -M")
        self.assertIn("3 B", result, "3 B not found with option -M")


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf testxz testxz.xz")
        
        