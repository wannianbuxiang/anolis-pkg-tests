# -*- encoding: utf-8 -*-

"""
@File:      tc_xz_xz_fun_002.py
@Time:      2024/10/12 16:51:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_xz_xz_fun_002.yaml for details

    :avocado: tags=P1,noarch,local,xz
    """
    PARAM_DIC = {"pkg_name": "xz"}

    def setUp(self):
       super().setUp(self.PARAM_DIC)
       self.cmd("echo 'xz_test' > xz_testfile")

    def test(self):
       self.cmd("rpm -qa |grep xz")
       self.cmd("xz -9 xz_testfile")
       code, result = self.cmd("ls")
       self.assertIn("xz_testfile.xz", result)
       self.cmd("unxz xz_testfile.xz")
       code, result = self.cmd("cat xz_testfile")
       self.assertIn("xz_test", result)
       	 
    def tearDown(self):
       super().tearDown(self.PARAM_DIC)		
       self.cmd("rm -rf xz_testfile")	
