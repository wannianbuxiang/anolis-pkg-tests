# -*- encoding: utf-8 -*-

"""
@File:      tc_xz_xz_fun_001.py
@Time:      2023/03/20 14:07:20
@Author:    pengrui
@Version:   1.0
@Contact:   pengrui@uniontech.com
@License:   Mulan PSL v2
@Modify:    pengrui
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_xz_xz_fun_001.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "xz"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("touch systest_xz_testfile")
        self.cmd("echo 'xz test' > systest_xz_testfile")

    def test(self):
        self.cmd("xz systest_xz_testfile")
        self.cmd("xz -d systest_xz_testfile.xz")
        code,result=self.cmd("cat systest_xz_testfile")
        self.assertEquals("xz test",result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -f systest_xz_testfile")