#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

"""
@File:      tc_xz_unxz_fun_002.py
@Time:      2024/07/15 14:07:20
@Author:    wangge
@Version:   1.0
@Contact:   YS.wangge@h3c.com
@License:   Mulan PSL v2
@Modify:    wangge
"""

from common.basetest import LocalTest



class Test(LocalTest):
    """
    See tc_xz_unxz_fun_002.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "xz"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        with open('testxz', 'w') as f:
            f.write('hello world')


    def test(self):
        code, result = self.cmd("unxz -z -0 -k -f -vv testxz 2>&1")
        self.assertEqual(code, 0, "Test failed with compression level 0")
        self.assertIn("dict=256KiB", result, "dict=256KiB not found in output for compression level 0")

        code, result = self.cmd("unxz -z -1 -k -f -vv testxz 2>&1")
        self.assertEqual(code, 0, "Test failed with compression level 1")
        self.assertIn("dict=1MiB", result, "dict=1MiB not found in output for compression level 1")

        code, result = self.cmd("unxz -z -2 -k -f -vv testxz 2>&1")
        self.assertEqual(code, 0, "Test failed with compression level 2")
        self.assertIn("dict=2MiB", result, "dict=2MiB not found in output for compression level 2")

        code, result = self.cmd("unxz -z -3 -k -f -vv testxz 2>&1")
        self.assertEqual(code, 0, "Test failed with compression level 3")
        self.assertIn("dict=4MiB", result, "dict=4MiB not found in output for compression level 3")
        self.assertIn("nice=273", result, "nice=273 not found in output for compression level 3")

        code, result = self.cmd("unxz -z -4 -k -f -vv testxz 2>&1")
        self.assertEqual(code, 0, "Test failed with compression level 4")
        self.assertIn("dict=4MiB", result, "dict=4MiB not found in output for compression level 4")
        self.assertIn("nice=16", result, "nice=16 not found in output for compression level 4")

        code, result = self.cmd("unxz -z -5 -k -f -vv testxz 2>&1")
        self.assertEqual(code, 0, "Test failed with compression level 5")
        self.assertIn("dict=8MiB", result, "dict=8MiB not found in output for compression level 5")
        self.assertIn("nice=32", result, "nice=32 not found in output for compression level 5")

        code, result = self.cmd("unxz -z -6 -k -f -vv testxz 2>&1")
        self.assertEqual(code, 0, "Test failed with compression level 6")
        self.assertIn("dict=8MiB", result, "dict=8MiB not found in output for compression level 6")
        self.assertIn("nice=64", result, "nice=64 not found in output for compression level 6")

        code, result = self.cmd("unxz -z -7 -k -f -vv testxz 2>&1")
        self.assertEqual(code, 0, "Test failed with compression level 7")
        self.assertIn("dict=16MiB", result, "dict=16MiB not found in output for compression level 7")

        code, result = self.cmd("unxz -z -8 -k -f -vv testxz 2>&1")
        self.assertEqual(code, 0, "Test failed with compression level 8")
        self.assertIn("dict=32MiB", result, "dict=32MiB not found in output for compression level 8")

        code, result = self.cmd("unxz -z -9 -k -f -vv testxz 2>&1")
        self.assertEqual(code, 0, "Test failed with compression level 9")
        self.assertIn("dict=64MiB", result, "dict=64MiB not found in output for compression level 9")
 
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -f testxz testxz.xz")
        
        