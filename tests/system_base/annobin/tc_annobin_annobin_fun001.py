"""
@File:      tc_annobin_annobin_fun001.py
@Time:      2024/7/18 16:00:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""
from common.basetest import LocalTest
import subprocess
import platform

class Test(LocalTest):
    """
    See tc_annobin_annobin_fun001.yaml for details

    :avocado: tags=P1,noarch,local,annobin
    """
    PARAM_DIC = {"pkg_name": "gcc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir -p /tmp/test_anno")
        self.path = '/tmp/test_anno'
        cmdline = f"""cat << EOF > {self.path}/test.c
#include<stdio.h>
int main(void)
{{
printf("Hello，world!");
return 0;
}}

EOF
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("yum install -y annobin")
        ret_c, ret_o = self.cmd("rpm -q annobin", ignore_status=True)
        if ret_c != 0:
            self.cmd("yum install -y annobin-plugin-gcc")
        code, result = self.cmd("rpm -qa | grep annobin")
        self.assertEqual(code, 0, "Return value error")
        self.cmd(f"cd {self.path} && gcc --print-file-name=plugin > {self.path}/test1")
        code,path1 = self.cmd("cat /tmp/test_anno/test1")
        code, result = self.cmd(f"grep '{path1}' {self.path}/test1")
        self.assertFalse(code, "directory file error")
        self.cmd(f"ls '{path1}' > {self.path}/test2")
        code,result=self.cmd(f"grep 'annobin.so' {self.path}/test2")
        self.assertFalse(code, "Library files fails")
        self.cmd(f"cd {self.path} && gcc -fplugin=annobin {self.path}/test.c")
        self.cmd(f"{self.path}/a.out > {self.path}/test3")
        code, result = self.cmd(f"grep 'Hello，world!' {self.path}/test3")
        self.assertFalse(code, "executable file fails")
        self.cmd(f"rm {self.path}/a.out")
        self.cmd(f"cd {self.path} && gcc -fplugin=annobin -fplugin-arg-annobin-verbose {self.path}/test.c")
        code,result=self.cmd(f"test -f {self.path}/a.out")
        self.assertFalse(code, "a.out file fails")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("yum remove -y annobin-plugin-gcc annobin-docs annobin", ignore_status=True)
        self.cmd(f"rm -rf {self.path}")