# -*- encoding: utf-8 -*-

"""
@File:      tc_rpm_rpm_fun001.py
@Time:      2024/5/13 15:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_rpm_rpm_fun001.yaml for details

    :avocado: tags=P1,noarch,local,less
    """
    PARAM_DIC = {"pkg_name": "rpm"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("dnf download lsof")       
        self.cmd("rpm -ivh lsof*.rpm ")

    def test(self):        	
        code, result = self.cmd("rpm -qip lsof*.rpm")
        self.assertIn("lsof", result)
        code, result = self.cmd("rpm -qa | grep -v help")
        self.assertIn("lsof", result)
        code, result = self.cmd("rpm -qi lsof")
        self.assertIn("Name", result)
        code, result = self.cmd("rpm -ql lsof")
        self.assertIn("/usr/bin/lsof", result)
    
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rpm -e lsof")
        self.cmd("rm -rf lsof*.rpm")