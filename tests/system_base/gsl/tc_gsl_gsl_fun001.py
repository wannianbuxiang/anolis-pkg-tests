# -*- encoding: utf-8 -*-

"""
@File:      tc_gsl_gsl_fun001.py
@Time:      2024/5/6 16:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_gsl_gsl_fun001.yaml for details

    :avocado: tags=P1,noarch,local,gsl
    """
    PARAM_DIC = {"pkg_name": "gsl"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)      

    def test(self):	
        self.cmd("gsl-randist 0 10 exppow 1 2 | grep [0-9]")
        self.cmd("gsl-randist 1 10 flat 10 1 | grep [0-9]")
        self.cmd("gsl-randist 1 10 gamma 10 1 | grep [0-9]")
        self.cmd("gsl-randist 1 10 gaussian 10  | grep [0-9]")
        self.cmd("gsl-randist 1 10 geometric 1 | grep [0-9]")
        self.cmd("gsl-randist 1 10 gumbel1 1 2  | grep [0-9]")
    
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
       
