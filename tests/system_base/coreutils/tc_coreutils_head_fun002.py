# -*- encoding: utf-8 -*-

"""
@File:      tc_coreutils_head_fun002.py
@Time:      2024/6/26 16:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_coreutils_head_fun002.yaml for details

    :avocado: tags=P1,noarch,local,coreutils
    """
    PARAM_DIC = {"pkg_name": "coreutils"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)       
        cmdline = """echo -e '1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n' >> /tmp/test.txt"""
        self.cmd(cmdline)

    def test(self):   
        code, result = self.cmd("head -n 5 /tmp/test.txt")
        self.assertIn("1\n2\n3\n4\n5", result) 
        code, result = self.cmd("head --version")
        self.assertIn("head", result)
        self.cmd("head --help > head.log", ignore_status=True)
        code, result = self.cmd("cat head.log", ignore_status=True)
        self.assertIn("head", result)
                
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/test.txt")
       