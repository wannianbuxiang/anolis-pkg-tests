# -*- encoding: utf-8 -*-

"""
@File:      tc_coreutils_sed_fun001.py
@Time:      2024/04/23 10:26:35
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

test_str = '''
1.test
2.delete test
'''

class Test(LocalTest):
    """
    See tc_coreutils_sed_fun001.yaml for details

    :avocado: tags=P1,noarch,local,coreutils
    """
    PARAM_DIC = {"pkg_name": "coreutils"}


    def setUp(self):
        super().setUp(self.PARAM_DIC)
        with open("sed.file",mode="wt",encoding="utf-8")as f:
            f.write(test_str)

    def test(self):
        self.cmd("sed 's/test/TEST/' sed.file")
        self.cmd("sed '/delete/d' sed.file")


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -f sed.file")
