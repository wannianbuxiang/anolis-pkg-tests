# -*- encoding: utf-8 -*-

"""
@File:      tc_coreutils_wc_fun002.py
@Time:      2024/8/7 16:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_coreutils_wc_fun002.yaml for details

    :avocado: tags=P1,noarch,local,coreutils
    """
    PARAM_DIC = {"pkg_name": "coreutils"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

        cmdline = """cat > /tmp/test_wc.txt <<EOF    
The first line
The second line
The third line
EOF"""
        self.cmd(cmdline)

    def test(self):        	
       code, result = self.cmd("wc -l /tmp/test_wc.txt")
       self.assertIn("3", result)
       code, result = self.cmd("wc -w /tmp/test_wc.txt")
       self.assertIn("9", result)  
       code, result = self.cmd("wc -c /tmp/test_wc.txt")
       self.assertIn("46", result)  
     
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/test_wc.txt")
