# -*- encoding: utf-8 -*-

"""
@File:      tc_coreutils_sort_fun001.py
@Time:      2024/8/1 17:00:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_coreutils_sort_fun001.yaml for details

    :avocado: tags=P1,noarch,local,coreutils
    """
    PARAM_DIC = {"pkg_name": "coreutils"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

        cmdline = """cat > sort_test.txt <<EOF    
hello
world
kos
wc
66
88
aaa
EOF"""
        self.cmd(cmdline)

    def test(self):
       code, result = self.cmd("sort sort_test.txt | head -1")
       self.assertIn("66", result)        	
       code, result = self.cmd("sort sort_test.txt | tail -n 1")
       self.assertIn("world", result) 
       code, result = self.cmd("sort -n sort_test.txt | head -3")
       self.assertIn("kos", result) 
      
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf sort_test.txt")
