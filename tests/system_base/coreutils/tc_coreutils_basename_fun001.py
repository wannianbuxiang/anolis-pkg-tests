# -*- encoding: utf-8 -*-

"""
@File:      tc_coreutils_basename_fun001.py
@Time:      2024/07/18 15:23:53
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_coreutils_basename_fun001.yaml for details

    :avocado: tags=P1,noarch,local,basename
    """
    PARAM_DIC = {"pkg_name": "coreutils"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        print("begin test")

    def test(self):
        code1,result=self.cmd('basename /var/log/messages')
        self.assertIn("messages", result)
        code2,result=self.cmd('basename /var/log/boot.log .log')
        self.assertIn("boot", result)
        code3,result=self.cmd('basename /var/log')
        self.assertIn("log", result)
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)