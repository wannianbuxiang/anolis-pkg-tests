# -*- encoding: utf-8 -*-

"""
@File:      tc_coreutils_chmod_fun001.py
@Time:      2024/8/22 17:00:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_coreutils_chmod_fun001.yaml for details

    :avocado: tags=P1,noarch,local,coreutils
    """
    PARAM_DIC = {"pkg_name": "coreutils"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > /tmp/test.txt <<EOF    
root:x:0:0:root:/root:/usr/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync

EOF"""
        self.cmd(cmdline)

    def test(self):

       self.cmd("mkdir -p /tmp/test01")
       code, result = self.cmd("ls -l /tmp | grep 'test01' | awk -F ' ' '{print $1}'")
       self.assertIn("drwxr-xr-x", result)
       self.cmd("chmod 777 /tmp/test01")
       code, result = self.cmd("ls -l /tmp | grep 'test01' | awk -F ' ' '{print $1}'")
       self.assertIn("drwxrwxrwx", result)         	

       code, result = self.cmd("ls -l /tmp | grep 'test.txt' | awk -F ' ' '{print $1}'")
       self.assertIn("-rw-r--r--", result)
       self.cmd("chmod 777 /tmp/test.txt")
       code, result = self.cmd("ls -l /tmp | grep 'test.txt' | awk -F ' ' '{print $1}'")
       self.assertIn("-rwxrwxrwx", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/test01")
        self.cmd("rm -rf /tmp/test.txt")

