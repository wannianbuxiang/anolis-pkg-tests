# -*- encoding: utf-8 -*-

"""
@File:      tc_coreutils_csplit_fun001.py
@Time:      2024/9/26 17:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_coreutils_csplit_fun001.yaml for details

    :avocado: tags=P1,noarch,local,coreutils
    """
    PARAM_DIC = {"pkg_name": "coreutils"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

        cmdline = """cat > csplit.txt <<EOF
hello Linux!  
Linux is a free Unix-type operating system.  
This is a Linux testfile!  
Linux
EOF"""
        self.cmd(cmdline)

    def test(self):        	
       self.cmd("csplit csplit.txt 2")
       code, result = self.cmd('grep "hello Linux!" xx00')
       self.assertIn("hello Linux!", result)  
       code, result = self.cmd('grep "Linux is a free" xx01')
       self.assertIn("Linux is a free", result)  
     
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf csplit.txt xx00 xx01")
