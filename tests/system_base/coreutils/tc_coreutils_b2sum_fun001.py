# -*- encoding: utf-8 -*-

"""
@File:      tc_coreutils_b2sum_fun001.py
@Time:      2024/07/25 15:10:53
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_coreutils_b2sum_fun001.yaml for details

    :avocado: tags=P1,noarch,local,coreutils
    """
    PARAM_DIC = {"pkg_name": "coreutils"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        print("begin test")

    def test(self):
        self.cmd('echo "123456" >testfile1')
        code1,result1=self.cmd('b2sum -b testfile1')
        self.assertIn("*testfile1", result1)
        code2,result2=self.cmd('b2sum -l 8 testfile1')
        self.assertIn("a1", result2)
        code3,result3=self.cmd('b2sum -t testfile1')
        self.assertIn("testfile1", result3)
        code4,result4=self.cmd('b2sum --help')
        self.assertIn("b2sum", result4)
        code5,result5=self.cmd('b2sum --version')
        self.assertIn("b2sum", result5)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf testfile1")