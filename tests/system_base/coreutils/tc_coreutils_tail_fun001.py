# -*- encoding: utf-8 -*-

"""
@File:      tc_coreutils_tail_fun001.py
@Time:      2023/01/04 18:31:20
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_coreutils_tail_fun001.yaml for details

    :avocado: tags=P1,noarch,local,coreutils
    """
    PARAM_DIC = {"pkg_name": "coreutils"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("yum install -y coreutils")
        self.cmd("touch tail_testfile")
        for i in range(1,21):
            self.cmd("echo 'tail test'{} >> tail_testfile".format(str(i)))

    def test(self):
        code,tail_result=self.cmd("tail -n 3 tail_testfile")
        self.assertIn("tail test20",tail_result)
        self.assertIn("tail test19",tail_result)
        self.assertIn("tail test18",tail_result)
        self.assertNotIn("tail test17",tail_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -f tail_testfile")