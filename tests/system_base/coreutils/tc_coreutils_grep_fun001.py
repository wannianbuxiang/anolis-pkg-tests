# -*- encoding: utf-8 -*-

"""
@File:      tc_coreutils_grep_fun001.py
@Time:      2024/04/23 9:19:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

test_str = '''
    This is a testfile for grep basic usage
    1.Ignore case distinctions in patterns and data TEST
    2.Prefix each line of output with the 1-based line number within its input file.
    3.Invert the sense of matching, to select non-matching lines.
    4.Suppress normal output; instead print the name of each input file from which output would normally have been printed.
    5.Regular 
    '''

class Test(LocalTest):
    """
    See tc_coreutils_grep_fun001.yaml for details

    :avocado: tags=P1,noarch,local,coreutils
    """
    PARAM_DIC = {"pkg_name": "coreutils"}


    def setUp(self):
        super().setUp(self.PARAM_DIC)
        with open("grep.file",mode="wt",encoding="utf-8")as f:
            f.write(test_str)

    def test(self):
        self.cmd("grep 'test' grep.file")
        self.cmd("grep -r -n 'test' .")
        self.cmd("grep -i 'test' grep.file")
        self.cmd("grep 'test' grep.file")


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -f grep.file")
