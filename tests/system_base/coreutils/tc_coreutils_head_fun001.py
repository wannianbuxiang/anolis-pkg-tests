# -*- encoding: utf-8 -*-

"""
@File:      tc_coreutils_head_fun001.py
@Time:      2023/01/04 18:21:20
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_coreutils_head_fun001.yaml for details

    :avocado: tags=P1,noarch,local,coreutils
    """
    PARAM_DIC = {"pkg_name": "coreutils"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("yum install -y coreutils")
        self.cmd("touch head_testfile")
        for i in range(1,21):
            self.cmd("echo 'head test'{} >> head_testfile".format(str(i)))

    def test(self):
        code,head_result=self.cmd("head -n 3 head_testfile")
        self.assertIn("head test1",head_result)
        self.assertIn("head test2",head_result)
        self.assertIn("head test3",head_result)
        self.assertNotIn("head test4",head_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -f head_testfile")