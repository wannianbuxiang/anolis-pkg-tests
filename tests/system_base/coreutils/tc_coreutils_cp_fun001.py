# -*- encoding: utf-8 -*-

"""
@File:      tc_coreutils_cp_fun001.py
@Time:      2023/01/04 17:37:20
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_coreutils_cp_fun001.yaml for details

    :avocado: tags=P1,noarch,local,coreutils
    """
    PARAM_DIC = {"pkg_name": "coreutils"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("yum install -y coreutils")
        self.cmd("touch cp_old_testfile")
        self.cmd("echo 'cp test' > cp_old_testfile")

    def test(self):
        self.cmd("cp cp_old_testfile cp_new_testfile")
        code,cp_ls_result=self.cmd("ls -a")
        self.assertIn("cp_old_testfile",cp_ls_result)
        self.assertIn("cp_new_testfile",cp_ls_result)
        code,cp_cat_old_result=self.cmd("cat cp_old_testfile")
        code,cp_cat_new_result=self.cmd("cat cp_new_testfile")
        self.assertEquals("cp test",cp_cat_old_result)
        self.assertEquals("cp test",cp_cat_new_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf cp_old_testfile cp_new_testfile")