# -*- encoding: utf-8 -*-

"""
@File:      tc_coreutils_echo_fun002.py
@Time:      2024/10/9 17:21:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_coreutils_echo_fun002.yaml for details

    :avocado: tags=P1,noarch,local,coreutils
    """
    PARAM_DIC = {"pkg_name": "coreutils"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)      

    def test(self):       
        code, result = self.cmd("echo 'Hello, World'")
        self.assertIn("Hello, World", result)
        self.cmd("echo 'Hello, File' > hello.txt")
        code, result = self.cmd("cat hello.txt")
        self.assertIn("Hello, File", result)
        self.cmd("echo 'Another line' >> hello.txt")
        code, result = self.cmd("cat hello.txt")
        self.assertIn("Another line", result)
	
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf hello.txt")		
