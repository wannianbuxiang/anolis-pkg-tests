# -*- encoding: utf-8 -*-

"""
@File:      tc_coreutils_base64_fun001.py
@Time:      2024/07/19 16:23:53
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_coreutils_base64_fun001.yaml for details

    :avocado: tags=P1,noarch,local,coreutils
    """
    PARAM_DIC = {"pkg_name": "coreutils"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        #code,result=self.cmd("echo $LANG")
        #self.result=result
        #print(result)
        #self.cmd("export LANG=en_US.UTF-8") 
        #code1,result1=self.cmd('echo $LANG')
        #self.assertIn("en_US.UTF-8", result1)
        #print("begin test")

    def test(self):
        code2,result2=self.cmd('echo "password" | base64')
        self.assertIn("cGFzc3dvcmQK", result2)
        code3,result3=self.cmd('echo "cGFzc3dvcmQK" | base64 -d')
        self.assertIn("password", result3)
        code4,result4=self.cmd('echo "cGFzc3dvcmQK" | base64 -i')
        self.assertIn("Y0dGemMzZHZjbVFLCg==", result4)
        code5,result5=self.cmd('base64 --version')
        self.assertIn("Copyright", result5)
        code6,result6=self.cmd('base64 --help')
        self.assertIn("base64", result6)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        #self.cmd(f"export LANG={self.result}")