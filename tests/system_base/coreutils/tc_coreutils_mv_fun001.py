# -*- encoding: utf-8 -*-

"""
@File:      tc_coreutils_mv_fun001.py
@Time:      2023/01/04 18:11:20
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_coreutils_mv_fun001.yaml for details

    :avocado: tags=P1,noarch,local,coreutils
    """
    PARAM_DIC = {"pkg_name": "coreutils"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("yum install -y coreutils")
        self.cmd("touch mv_old_testfile")
        self.cmd("echo 'mv test' > mv_old_testfile")

    def test(self):
        self.cmd("mv mv_old_testfile mv_new_testfile")
        code,mv_ls_result=self.cmd("ls -a")
        self.assertNotIn("mv_old_testfile",mv_ls_result)
        self.assertIn("mv_new_testfile",mv_ls_result)
        code,mv_cat_result=self.cmd("cat mv_new_testfile")
        self.assertEquals("mv test",mv_cat_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -f mv_new_testfile")