# -*- encoding: utf-8 -*-

"""
@File:      tc_coreutils_echo_fun001.py
@Time:      2023/01/04 17:49:20
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_coreutils_echo_fun001.yaml for details

    :avocado: tags=P1,noarch,local,coreutils
    """
    PARAM_DIC = {"pkg_name": "coreutils"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("yum install -y coreutils")
        self.cmd("touch echo_testfile")

    def test(self):
        self.cmd("echo 'echo test' > echo_testfile")
        code,echo_result=self.cmd("cat echo_testfile")
        self.assertEquals("echo test",echo_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm echo_testfile")