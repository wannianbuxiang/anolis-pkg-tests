# -*- encoding: utf-8 -*-

"""
@File:      tc_coreutils_tr_fun001.py
@Time:      2024/10/25 15:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_coreutils_tr_fun001.yaml for details

    :avocado: tags=P1,noarch,local,coreutils
    """
    PARAM_DIC = {"pkg_name": "coreutils"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > tr_test << EOF
aabbccddeeff
gghh
123456
mmnn
EOF"""
        self.cmd(cmdline)

    def test(self):        	
        self.cmd("tr -d 'gghh' <tr_test> tr.result1")
        code,result=self.cmd("cat tr.result1")
        self.assertNotIn("gghh",result)

        self.cmd("tr -d '123456' <tr_test> tr.result2")
        code,result=self.cmd("cat tr.result2")
        self.assertNotIn("123456",result)

        self.cmd("tr -s '[abcdef]' <tr_test> tr.result3")
        code,result=self.cmd("cat tr.result3")
        self.assertIn("abcdef",result)

        self.cmd("grep mmnn tr_test |tr 'a-z' 'A-Z'>tr.result4")
        code,result=self.cmd("cat tr.result4")
        self.assertIn("MMNN",result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf tr.result*  tr_test")