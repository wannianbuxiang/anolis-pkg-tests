#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_container_status_func_001.py
@Time:      2022/9/22 19:06:45
@Author:    yangshunfeng
@Version:   1.0
@License:   Mulan PSL v2
"""
from common.basetest import LocalTest

class ContainStatusTest(LocalTest):
    """
    See   tc_container_status_func_001.yaml for details
    :avocado: tags=fix,P2,noarch,local,baseos_container_default
    """

    def test_container_basic(self):
        if not self.container_engine:
            self.skip("There is no container engine, skip the case.")

        ret_c,container_status = self.cmd(self.container_engine+' inspect '+self.container_id+' | grep Status')
        self.assertTrue('running' in container_status,"The status of the application container is abnormal")
        ret_c,container_run = self.cmd(self.container_engine+' inspect '+self.container_id+' | grep Running')
        self.assertTrue('true' in container_run,"The application container is not running")
