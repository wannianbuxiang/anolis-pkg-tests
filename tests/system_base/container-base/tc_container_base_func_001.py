#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_container_base_func_001.py
@Time:      2022/9/20 19:06:45
@Author:    yangshunfeng
@Version:   1.0
@License:   Mulan PSL v2
"""
from common.basetest import LocalTest
import pexpect
class ContainerBaseTest(LocalTest):
    """
    See   tc_container_base_func_001.yaml for details
    :avocado: tags=fix,P2,noarch,local,container_default
    """

    def test_container_base(self):
        if not self.container_engine:
            self.skip("There is no container engine, skip the case.")

        ret_c,lang = self.cmd('echo "$LANG"',container_flag=1)
        child = pexpect.spawn(self.container_engine + ' exec -it ' + self.container_id + ' /bin/bash')
        child.sendline('locale')
        index = child.expect(["LC_CTYPE", pexpect.EOF, pexpect.TIMEOUT], timeout=10)
        if index == 0:
            output = child.before.decode()
            print(output)
        else:
            self.fail("Command execution failed")
        child.sendline('exit')
        self.assertTrue("POSIX" not in output,"check locale output error")
        ret_c,disk_cap = self.cmd('df -h | grep overlay | awk  {print\$2}',container_flag=1)
        self.assertTrue(disk_cap is not None,"Container missing root directory!")
        ret_c,host_kversion = self.cmd('uname -r')
        ret_c,container_kversion = self.cmd('uname -r',container_flag=1)
        self.assertEqual(host_kversion,container_kversion,"The container kernel version is not as expected!")
