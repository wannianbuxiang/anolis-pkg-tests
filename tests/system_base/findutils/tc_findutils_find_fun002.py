# -*- encoding: utf-8 -*-

"""
@File:      tc_findutils_find_fun002.py
@Time:      2024/7/10 17:00:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_findutils_find_fun002.yaml for details

    :avocado: tags=P1,noarch,local,findutils
    """
    PARAM_DIC = {"pkg_name": "findutils"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir -p /tmp/a")
        self.cmd("touch /tmp/a/{1..3}.log")
        self.cmd("touch /tmp/x.log")
        self.cmd("touch /tmp/b.log")

    def test(self):      	
        code, result = self.cmd("find /tmp -name x.log")
        self.assertIn("/tmp/x.log", result)  
        code, result = self.cmd("find /tmp -path  *a*/1.log")
        self.assertIn("/tmp/a/1.log", result)
        code, result = self.cmd('find /tmp -type d -name "a*"')
        self.assertIn("/tmp/a", result)  
        code, result = self.cmd('find /tmp -type f -name "b*"')
        self.assertIn("/tmp/b.log", result) 
         
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -r /tmp/a")
        self.cmd("rm -rf /tmp/x.log /tmp/b.log")