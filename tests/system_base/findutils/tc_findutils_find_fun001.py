# -*- encoding: utf-8 -*-

"""
@File:      tc_findutils_find_fun001.py
@Time:      2024/7/10 17:00:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_findutils_find_fun001.yaml for details

    :avocado: tags=P1,noarch,local,findutils
    """
    PARAM_DIC = {"pkg_name": "findutils"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        
    def test(self):        	
        code, result = self.cmd("find --version")
        self.assertIn("find", result)
        self.cmd("find -help > /tmp/find.log", ignore_status=True)
        code, result = self.cmd("cat /tmp/find.log", ignore_status=True)
        self.assertIn("Usage:", result)
      
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/find.log")
