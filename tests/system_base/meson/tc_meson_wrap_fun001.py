'''
@File:      tc_meson_wrap_fun001.py
@Time:      2024/7/30 17:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
'''

from common.basetest import LocalTest


class Test(LocalTest):
    '''
    See tc_meson_wrap_fun001.yaml for details

    :avocado: tags=P1,noarch,local,meson
    '''
    PARAM_DIC = {'pkg_name': 'meson'}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        code, result = self.cmd(' meson wrap --help | grep "usage: meson wrap"')
        self.assertFalse(code, 'meson wrap --help failed')
        code, result = self.cmd('meson wrap -h | grep "usage: meson wrap"')
        self.assertFalse(code, 'meson wrap -h failed')
        code, result = self.cmd('meson wrap list --help | grep "usage: meson wrap list"')
        self.assertFalse(code, 'meson wrap list --help failed')
        code, result = self.cmd('meson wrap list -h | grep "usage: meson wrap list"')
        self.assertFalse(code, 'meson wrap list -h failed')
        code, result = self.cmd(' meson wrap search --help | grep "usage: meson wrap search"')
        self.assertFalse(code, 'meson wrap search --help failed')
        code, result = self.cmd('meson wrap search -h | grep "usage: meson wrap search"')
        self.assertFalse(code, 'meson wrap search -h failed')
        code, result = self.cmd('meson wrap install --help | grep "usage: meson wrap install"')
        self.assertFalse(code, 'meson wrap install --help failed')
        code, result = self.cmd('meson wrap install -h | grep "usage: meson wrap install"')
        self.assertFalse(code, 'meson wrap install -h failed')
        code, result = self.cmd('meson wrap update --help | grep "usage: meson wrap update"')
        self.assertFalse(code, 'meson wrap update --help failed')
        code, result = self.cmd(' meson wrap update -h | grep "usage: meson wrap update"')
        self.assertFalse(code, 'meson wrap update -h failed')
        code, result = self.cmd('meson wrap info --help | grep "usage: meson wrap info"')
        self.assertFalse(code, 'meson wrap info --help failed')
        code, result = self.cmd('meson wrap info -h | grep "usage: meson wrap info"')
        self.assertFalse(code, 'meson wrap info -h failed')
        code, result = self.cmd('meson wrap status --help | grep "usage: meson wrap status"')
        self.assertFalse(code, 'meson wrap status --help failed')
        code, result = self.cmd(' meson wrap status -h | grep "usage: meson wrap status"')
        self.assertFalse(code, 'meson wrap status -h failed')
        code, result = self.cmd('meson wrap promote --help | grep "usage: meson wrap promote"')
        self.assertFalse(code, 'meson wrap promote --help failed')
        code, result = self.cmd(' meson wrap promote -h | grep "usage: meson wrap promote"')
        self.assertFalse(code, 'meson wrap promote -h failed')

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
