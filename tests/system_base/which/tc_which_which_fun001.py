# -*- encoding: utf-8 -*-

"""
@File:      tc_which_which_fun001.py
@Time:      2024/06/06 10:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_which_which_fun001.yaml for details

    :avocado: tags=P1,noarch,local,which
    """
    PARAM_DIC = {"pkg_name": "which"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        

    def test(self):        	
        code,result1=self.cmd("rpm -qa| grep which")
        self.assertIn("which", result1)
        code,result2=self.cmd("which -v | grep -E 'GNU which v[0-9]\.[0-9]+'")
        self.assertIn("GNU which v2.21", result2)
        code,result3=self.cmd("which bash | grep '/usr/bin/bash'")
        self.assertIn("/usr/bin/bash", result3)
    
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        
