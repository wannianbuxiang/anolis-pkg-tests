#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_at_command_func001.py
@Time:      2024/05/14 14:46:26
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_at_command_func001.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "at"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        self.cmd('systemctl restart atd')
        self.cmd('at 15:30 -f /usr/bin/ls')
        self.cmd('atq |grep "15:30"')
        self.cmd('echo "hello world" | at 14:30 -m')
        self.cmd('atq | grep "14:30"')
        self.cmd('echo "hello world" | at -q b 15:31')
        self.cmd('atq | grep "15:31" | grep b')
        self.cmd('at -V')

    def tearDown(self):
        # 删除创建的定时任务
        self.cmd('atq | awk "{print \$1}" | xargs atrm', ignore_status=True)
        super().tearDown(self.PARAM_DIC)
