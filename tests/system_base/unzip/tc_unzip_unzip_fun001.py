# -*- encoding: utf-8 -*-

"""
@File:      tc_unzip_unzip_fun001.py
@Time:      2024/06/06 10:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_unzip_unzip_fun001.yaml for details

    :avocado: tags=P1,noarch,local,unzip
    """
    PARAM_DIC = {"pkg_name": "zip unzip"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("cp -r /usr/bin .")
        self.cmd("zip bin.zip bin")
        self.cmd("rm -rf bin")

    def test(self):        	
        code,result1=self.cmd("rpm -qa| grep unzip")
        self.assertIn("unzip", result1)
        self.cmd("unzip bin.zip")
        code,result2=self.cmd("ls")
        self.assertIn("bin", result2)
        self.cmd("unzip bin.zip -d /home")
        code,result3=self.cmd("ls /home")
        self.assertIn("bin", result3)
    
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /home/bin bin.zip bin")
