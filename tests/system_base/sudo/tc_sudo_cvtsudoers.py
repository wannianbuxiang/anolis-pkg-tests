# -*- encoding: utf-8 -*-

"""
@File:      tc_sudo_cvtsudoers.py
@Time:      2024/06/28 17:34:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_sudo_cvtsudoers.yaml for details

    :avocado: tags=P1,noarch,local,sudo
    """
    PARAM_DIC = {"pkg_name": "sudo"}
    
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("cp /etc/sudoers /etc/sudoers_bak")
    
    def test(self):
        self.cmd("cvtsudoers -b ou=SUDOers,dc=my-domain,dc=com -o /tmp/sudoers.ldif /etc/sudoers")
        self.cmd("cvtsudoers -f json -o /tmp/sudoers.json /etc/sudoers")
        self.cmd("cvtsudoers -f sudoers -m user=ambrose,host=hastur /etc/sudoers")
        self.cmd("cvtsudoers -ep -f sudoers -m user=ambrose,host=hastur /etc/sudoers")
        self.cmd("cvtsudoers -i ldif -f sudoers -o /tmp/sudoers.new /tmp/sudoers.ldif")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("mv -f /etc/sudoers_bak /etc/sudoers")
        self.cmd("rm -rf /tmp/sudoers*")
