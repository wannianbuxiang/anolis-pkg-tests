#!/usr/bin/env python3
# -*- coding: gbk -*-

"""
@File:      tc_ant_test_002.py
@Time:      2024/07/16 15:42
@Author:    wangyang
@Version:   1.0
"""

from common.basetest import LocalTest
from avocado.utils import process

class Test(LocalTest):
    """
    See tc_ant_test_002.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "ant"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        content1 = '''<?xml version="1.0"?>
<project name="HelloWorld" default="test" basedir="">
    <target name="test">
        <echo message="ant test" />
    </target>
</project>'''
        with open('./build.xml', 'w') as file:
            file.write(content1)

    def test(self):
        code,result=self.cmd('ant -diagnostics  | grep "Ant diagnostics report"')
        self.assertEquals(0, code, f"test failed with option -diagnostics with return code {code}")
        code,result=self.cmd('ant -quiet | grep -Pz "BUILD SUCCESSFUL"')
        self.assertEquals(0, code, f"test failed with option -quiet with return code {code}")
        code,result=self.cmd('ant -q | grep -Pz "BUILD SUCCESSFUL"')
        self.assertEquals(0, code, f"test failed with option -q with return code {code}")
        code,result=self.cmd('ant -silent | grep -Pz "Buildfile[\S\s]*ant test"')
        self.assertEquals(0, code, f"test failed with option -silent with return code {code}")
        code,result=self.cmd('ant -S | grep -Pz "Buildfile[\S\s]*ant test"')
        self.assertEquals(0, code, f"test failed with option -S with return code {code}")
        code,result=self.cmd('ant -verbose | grep -Pz "BUILD SUCCESSFUL"')
        self.assertEquals(0, code, f"test failed with option -verbose with return code {code}")
        code,result=self.cmd('ant -v | grep -Pz "BUILD SUCCESSFUL"')
        self.assertEquals(0, code, f"test failed with option -v with return code {code}")
        code,result=self.cmd('ant -debug | grep -Pz "BUILD SUCCESSFUL"')
        self.assertEquals(0, code, f"test failed with option -debug with return code {code}")
        code,result=self.cmd('ant -d | grep -Pz "BUILD SUCCESSFUL"')
        self.assertEquals(0, code, f"test failed with option -d with return code {code}")
        code,result=self.cmd('ant -e | grep -Pz "BUILD SUCCESSFUL"')
        self.assertEquals(0, code, f"test failed with option -e with return code {code}")
        code,result=self.cmd('ant -emacs | grep -Pz "BUILD SUCCESSFUL"')
        self.assertEquals(0, code, f"test failed with option -emacs with return code {code}")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf build.xml')

