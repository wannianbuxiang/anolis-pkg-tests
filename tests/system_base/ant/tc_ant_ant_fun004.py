"""
@File:      tc_ant_ant_fun004.py
@Time:      2024/7/22 17:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""
from common.basetest import LocalTest
import subprocess


class Test(LocalTest):
    """
    See tc_ant_ant_fun004.yaml for details

    :avocado: tags=P1,noarch,local,ant
    """
    PARAM_DIC = {"pkg_name": "ant"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir -p testdir")
        self.cmd("mkdir -p $HOME/.ant/lib")
        self.cmd("echo 'rpm_mode=false' > $HOME/.ant/ant.conf")
        self.cmd("cp /usr/share/ant/lib/ant-launcher.jar $HOME/.ant/lib/test_nouserlib.jar")
        cmdline = """cat << EOF > testdir/build.xml
<?xml version="1.0"?>
<project name="Test" default="test" basedir="">
    <target name="test">
        <echo message="yourclasspath:\${java.class.path}" />
    </target>
</project>


EOF
        """
        self.cmd(cmdline)
        self.cmd("cp testdir/build.xml build.xml")
        cmdline2 = """cat << EOF > build_for_D.xml
<?xml version="1.0"?>
<project name="Test" default="test" basedir="">
    <target name="test">
        <echo message="value of property aaa is \${aaa}"/>
    </target>
</project>

EOF
        """
        self.cmd(cmdline2)
        cmdline3 = """cat << EOF > build_for_k.xml
<?xml version="1.0"?>
<project name="Test" default="test1" basedir="">
    <target name="test1">
        <input message="input a num:"/>
    </target>
    <target name="test2">
        <echo message="run test2" />
    </target>
    <target name="test3">
        <echo message="run test3" />
    </target>
    <target name="test4">
        <echo message="run test4" />
    </target>
    <target name="test5">
        <echo message="run test5" />
    </target>
</project>

EOF
        """
        self.cmd(cmdline3)
        cmdline4 = """cat << EOF > antpropertyfile
os=openeuler
testby=mugen


EOF
        """
        self.cmd(cmdline4)
        cmdline5 = """cat << EOF > build_for_propertyfile.xml
<?xml version="1.0"?>
<project name="Test" default="test1" basedir="">
    <target name="test1">
        <echo message="os=\${os}" />
        <echo message="testby=\${testby}" />
    </target>
</project>
  
EOF
        """
        self.cmd(cmdline5)
        cmdline6 = """cat << EOF > build_for_inputhandler.xml
<?xml version="1.0"?>
<project name="Test" default="test" basedir="">
    <target name="test">
        <input message="input: " />
        <echo message="yourclasspath:\${java.class.path}" />
    </target>
</project>

EOF
        """
        self.cmd(cmdline6)
        cmdline7 = """cat << EOF > PrintProperties.java
import java.util.Properties;
import java.util.Set;
public class PrintProperties {
    public static void main(String[] args) {
        Properties p = System.getProperties();
        Set<String> set = p.stringPropertyNames();
        for (String name : set) {
            System.out.println(name + "=" + p.getProperty(name));
        }
    }
}

EOF
        """
        self.cmd(cmdline7)
        cmdline8 = """cat << EOF > build_for_print_prop.xml
<?xml version="1.0"?>
<project name="PrintProp" default="test1" basedir="">
    <target name="test1">
        <javac srcdir="." />
        <java classname="PrintProperties">
            <classpath path="."/>
        </java>
    </target>
</project>

EOF
        """
        self.cmd(cmdline8)
        cmdline9 = """cat << EOF > CustomExitCode.java
public class CustomExitCode extends org.apache.tools.ant.Main {
    protected void exit(int exitCode) {
        System.out.println("all finished...");
    }
}

EOF
        """
        self.cmd(cmdline9)
        cmdline10 = """cat << EOF > MyHandler.java
import org.apache.tools.ant.input.InputRequest;
import org.apache.tools.ant.input.GreedyInputHandler;
import org.apache.tools.ant.BuildException;
public class MyHandler extends GreedyInputHandler {
    @Override
    public void handleInput(InputRequest request) throws BuildException {
        System.out.println("my input handler");
        super.handleInput(request);
    }
}

EOF
        """
        self.cmd(cmdline10)
        self.cmd("javac -cp '/usr/share/ant/lib/*' CustomExitCode.java")
        self.cmd(" javac -cp '/usr/share/ant/lib/*' MyHandler.java")

    def test(self):
        code, result = self.cmd("ant -Daaa=123 -f build_for_D.xml | grep -F 'value of property aaa is 123'")
        self.assertFalse(code, "test failed with option -D")
        command = "ant -k -noinput -f build_for_k.xml test1 test2 test3 test4 test5 > build_output.txt 2>&1 "
        subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        code, result = self.cmd("grep 'run test[2-5]' build_output.txt")
        self.assertFalse(code, "test failed with option -k")
        command = " ant -keep-going  -noinput -f build_for_k.xml test1 test2 test3 test4 test5 > build_output.txt 2>&1 "
        subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        code, result = self.cmd("grep 'run test[2-5]' build_output.txt")
        self.assertFalse(code, "test failed with option -keep-going")
        code, result = self.cmd(
            "ant -propertyfile antpropertyfile -f build_for_propertyfile.xml | grep -Pz '\[echo\] os=openeuler\s+\[echo\] testby=mugen'")
        self.assertFalse(code, "test failed with option -propertyfile")
        code, result = self.cmd(
            " echo 'hello' | CLASSPATH=. ant -f build_for_inputhandler.xml -inputhandler MyHandler | grep 'my input handler'")
        self.assertFalse(code, "test failed with option -inputhandler")
        code, result = self.cmd(" ant -find testdir | grep  'BUILD SUCCESSFUL'")
        self.assertFalse(code, "test failed with option -find")
        code, result = self.cmd("ant -s testdir | grep 'BUILD SUCCESSFUL'")
        self.assertFalse(code, "test failed with option -s")
        code, result = self.cmd("echo -e '1' | ant -nice 2 -f build_for_k.xml test1 test2 test3 test4 test5 | grep -Pz 'test[1-5]:[\s\S]*BUILD SUCCESSFUL'")
        self.assertFalse(code, "test failed with option -nice")
        self.cmd(" ant -nouserlib -f testdir > build_output.txt 2>&1 ")
        command = "grep -F '$HOME/.ant/lib/test_nouserlib.jar' build_output.txt "
        result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.assertEqual(result.returncode, 1, "test failed with option -nouserlib")
        self.cmd("CLASSPATH=aaa ant -noclasspath > build_output.txt 2>&1 ")
        command = "grep 'aaa' build_output.txt"
        result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.assertEqual(result.returncode, 1, "test failed with option -noclasspath")
        code, result = self.cmd("ant -autoproxy -f build_for_print_prop.xml -lib $PWD | grep -F 'java.net.useSystemProxies=true'")
        self.assertFalse(code, "test failed with option -autoproxy")
        code, result = self.cmd("ant -main CustomExitCode -lib . | grep 'yourclasspath'")
        self.assertFalse(code, "test failed with option -main")

    def tearDown(self):
        self.cmd(' rm -rf testdir *.xml *.java *.class $HOME/.ant antpropertyfile build_output.txt')
        super().tearDown(self.PARAM_DIC)
