"""
@File:      tc_ant_ant_fun003.py
@Time:      2024/7/22 17:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""
from common.basetest import LocalTest
import subprocess


class Test(LocalTest):
    """
    See tc_ant_ant_fun003.yaml for details

    :avocado: tags=P1,noarch,local,ant
    """
    PARAM_DIC = {"pkg_name": "ant"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat << EOF > build.xml
<?xml version="1.0"?>
<project name="HelloWorld" default="test" basedir="">
    <target name="test">
        <echo message="yourclasspath:\${java.class.path}" />
    </target>
</project>

EOF
        """
        self.cmd(cmdline)
        cmdline = """cat << EOF > build_for_input.xml
<?xml version="1.0"?>
<project name="HelloWorld" default="test" basedir="">
    <target name="test">
        <input message="input:" addproperty="user_input" defaultvalue="null" />
        <echo message="your input is:\${user_input}"></echo>
    </target>
</project>

EOF
        """
        self.cmd(cmdline)

    def test(self):
        code, result = self.cmd("ant -lib /test_added_lib_path | grep /test_added_lib_path")
        self.assertFalse(code, "test failed with option -lib")
        code, result = self.cmd("ant -logfile ant.1.log && grep 'yourclasspath' ant.1.log")
        self.assertFalse(code, "test failed with option -logfile")
        code,result=self.cmd(" ant -l ant.2.log && grep 'yourclasspath' ant.2.log")
        self.assertFalse(code, "test failed with option -l")
        code, result = self.cmd("ant -logger org.apache.tools.ant.NoBannerLogger | grep 'yourclasspath'")
        self.assertFalse(code, "test failed with option -logger")
        code, result = self.cmd("ant -listener org.apache.tools.ant.XmlLogger | grep 'yourclasspath'")
        self.assertFalse(code, "test failed with option -listener")
        code, result = self.cmd("echo -e 'hello\n' | ant -f build_for_input.xml | grep -F '[echo] your input is:hello'")
        self.assertFalse(code,"test failed with option -f")
        code, result = self.cmd("echo -e 'hello\n' | ant -file build_for_input.xml | grep -F '[echo] your input is:hello'")
        self.assertFalse(code,"test failed with option -file")
        code, result = self.cmd("echo -e 'hello\n' | ant -buildfile build_for_input.xml | grep -F '[echo] your input is:hello'")
        self.assertFalse(code, "test failed with option -buildfile")
        code, result = self.cmd("ant -noinput -f build_for_input.xml 2>&1 | grep  'Failed to read input from Console.'")
        self.assertFalse(code, "test failed with option -noinput")

    def tearDown(self):
        self.cmd('rm -rf *.xml *.log')
        super().tearDown(self.PARAM_DIC)
