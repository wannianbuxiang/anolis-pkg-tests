"""
@File:      tc_ant_ant_fun001.py
@Time:      2024/7/19 17:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""
from common.basetest import LocalTest
import subprocess


class Test(LocalTest):
    """
    See tc_ant_ant_fun001.yaml for details

    :avocado: tags=P1,noarch,local,ant
    """
    PARAM_DIC = {"pkg_name": "ant"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat << EOF > ~/.antrc
export test_value="ant using config antrc"
EOF
"""
        self.cmd(cmdline)
        cmdline2 = """cat << EOF > build.xml
<?xml version="1.0"?>
<project name="HelloWorld" default="test" basedir="">
    <property environment="env" />
    <target name="test">
        <echo message="\${env.test_value}" />
    </target>
</project>

EOF
        """
        self.cmd(cmdline2)
        pass

    def test(self):
        code, result = self.cmd("ant --h | grep -F '[options] [target [target2 [target3] ...]]'")
        self.assertFalse(code, "test failed with option --h")
        code, result = self.cmd("ant --help | grep -F '[options] [target [target2 [target3] ...]]'")
        self.assertFalse(code, "test failed with option --help")
        code,result=self.cmd("ANT_HOME=/usr/share/ant ant --noconfig | grep 'env.test_value'")
        self.assertFalse(code, "test failed with option --noconfig")
        code, result = self.cmd(" ant --usejikes | grep -P 'BUILD SUCCESSFUL'")
        self.assertFalse(code, "test failed with option --usejikes")
        code, result = self.cmd("ant --execdebug | grep -P 'BUILD SUCCESSFUL'")
        self.assertFalse(code,"test failed with option --execdebug")
        code, result = self.cmd("ant -h | grep -F '[options] [target [target2 [target3] ...]]'")
        self.assertFalse(code, "test failed with option -h")
        code, result = self.cmd("ant -help | grep -F '[options] [target [target2 [target3] ...]]'")
        self.assertFalse(code, "test failed with option -help")
        code, result = self.cmd("ant -projecthelp | grep -P 'Default target: test'")
        self.assertFalse(code, "test failed with option -projecthelp")
        code, result = self.cmd("ant -p | grep -P 'Default target: test'")
        self.assertFalse(code, "test failed with option -p")
        code, result = self.cmd("ant -version | grep -P 'version[\S\s]*compiled on[\S\s]*'")
        self.assertFalse(code, "test failed with option -version")

    def tearDown(self):
        self.cmd('rm -rf build.xml ~/.antrc')
        super().tearDown(self.PARAM_DIC)