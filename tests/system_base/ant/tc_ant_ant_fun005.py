"""
@File:      tc_ant_ant_fun005.py
@Time:      2024/7/23 17:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_ant_ant_fun005.yaml for details

    :avocado: tags=P1,noarch,local,ant
    """
    PARAM_DIC = {"pkg_name": "ant"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)


    def test(self):
        cmdline = """cat << EOF > /tmp/build.xml
<?xml version="1.0"?>
<project name="sample" basedir="." default="notify">
<target name="notify">
<java fork="true" failonerror="yes" classname="NotifyAdministrator">
<arg line="admin@test.com"/>
</java>
</target>
</project>

EOF
        """
        self.cmd(cmdline)
        cmdline = """cat << EOF > /tmp/NotifyAdministrator.java
public class NotifyAdministrator
{
public static void main(String[] args)
{
String email = args[0];
notifyAdministratorviaEmail(email);
System.out.println("Administrator "+email+" has been notified");
}
public static void notifyAdministratorviaEmail(String email)
{
//......
}
}

EOF
        """
        self.cmd(cmdline)
        self.cmd("javac /tmp/NotifyAdministrator.java")
        code, result = self.cmd(" test -e /tmp/NotifyAdministrator.class")
        self.assertFalse(code, "Ant failed to execute javac and could not generate the class file")
        code, result = self.cmd("cd /tmp && ant | grep 'BUILD SUCCESSFUL'")
        self.assertFalse(code, "ant run fails")

    def tearDown(self):
        self.cmd('rm -rf /tmp/build.xml /tmp/NotifyAdministrator*')
        super().tearDown(self.PARAM_DIC)
