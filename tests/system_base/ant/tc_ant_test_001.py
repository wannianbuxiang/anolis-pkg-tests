#!/usr/bin/env python3
# -*- coding: gbk -*-

"""
@File:      tc_ant_test_001.py
@Time:      2024/07/16 15:42
@Author:    wangyang
@Version:   1.0
"""

from common.basetest import LocalTest
from avocado.utils import process

class Test(LocalTest):
    """
    See tc_ant_test_001.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "ant"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        content1 = '''<?xml version="1.0"?>
<project name="HelloWorld" default="test" basedir="">
    <property environment="env" />
    <target name="test">
        <echo message="\${env.test_value}" />
    </target>
</project>'''
        with open('./build.xml', 'w') as file:
            file.write(content1)
        content2 = '''export test_value="ant using config antrc"'''
        with open('/root/.antrc', 'w') as file:
            file.write(content2)

    def test(self):
        code,result=self.cmd('ant --h | grep -F "[options] [target [target2 [target3] ...]]"')
        self.assertEquals(0, code, f"test failed with option --h with return code {code}")
        code,result=self.cmd('ant --help | grep -F "[options] [target [target2 [target3] ...]]"')
        self.assertEquals(0, code, f"test failed with option --help with return code {code}")
        code,result=self.cmd('ANT_HOME=/usr/share/ant ant --noconfig | grep "env.test_value"')
        self.assertEquals(0, code, f"test failed with option --noconfig with return code {code}")
        code,result=self.cmd('ant --usejikes |grep -Pz "BUILD SUCCESSFUL"')
        self.assertEquals(0, code, f"test failed with option --usejikes with return code {code}")
        code,result=self.cmd('ant --execdebug | grep -Pz "BUILD SUCCESSFUL"')
        self.assertEquals(0, code, f"test failed with option --execdebug with return code {code}")
        code,result=self.cmd('ant -projecthelp | grep -Pz "Default target: test"')
        self.assertEquals(0, code, f"test failed with option -projecthelp with return code {code}")
        code,result=self.cmd('ant -p | grep -Pz "Default target: test"')
        self.assertEquals(0, code, f"test failed with option -p with return code {code}")
        code,result=self.cmd('ant -version | grep -Pz "Apache Ant\(TM\) version[\S\s]*compiled on[\S\s]*"')
        self.assertEquals(0, code, f"test failed with option -version with return code {code}")


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf build.xml ~/.antrc')

