#!/usr/bin/env python3
# -*- coding: gbk -*-

"""
@File:      tc_ant_test_003.py
@Time:      2024/07/16 15:42
@Author:    wangyang
@Version:   1.0
"""

from common.basetest import LocalTest
from avocado.utils import process

class Test(LocalTest):
    """
    See tc_ant_test_003.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "ant"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        content1 = '''<?xml version="1.0"?>
<project name="HelloWorld" default="test" basedir="">
    <target name="test">
        <echo message="yourclasspath:\${java.class.path}" />
    </target>
</project>'''
        with open('./build.xml', 'w') as file:
            file.write(content1)
        content2 = '''<?xml version="1.0"?>
<project name="HelloWorld" default="test" basedir="">
    <target name="test">
        <input message="input:" addproperty="user_input" defaultvalue="null" />
        <echo message="your input is:\${user_input}"></echo>
    </target>
</project>'''
        with open('./build_for_input.xml', 'w') as file:
            file.write(content2)

    def test(self):
        code,result=self.cmd('ant -lib /test_added_lib_path | grep /test_added_lib_path')
        self.assertEquals(0, code, f"test failed with option -lib with return code {code}")
        code,result=self.cmd('ant -logfile ant.1.log && grep "yourclasspath" ant.1.log')
        self.assertEquals(0, code, f"test failed with option -logfile with return code {code}")
        code,result=self.cmd('ant -l ant.2.log && grep "yourclasspath" ant.2.log')
        self.assertEquals(0, code, f"test failed with option -l with return code {code}")
        code,result=self.cmd('ant -logger org.apache.tools.ant.NoBannerLogger | grep "yourclasspath"')
        self.assertEquals(0, code, f"test failed with option -logger with return code {code}")
        code,result=self.cmd('ant -listener org.apache.tools.ant.XmlLogger | grep "yourclasspath"')
        self.assertEquals(0, code, f"test failed with option -listener with return code {code}")
        code,result=self.cmd('echo -e "hello" | ant -f build_for_input.xml | grep -F "hello"')
        self.assertEquals(0, code, f"test failed with option -f with return code {code}")
        code,result=self.cmd('echo -e "hello\n" | ant -file build_for_input.xml | grep -F "hello"')
        self.assertEquals(0, code, f"test failed with option -file with return code {code}")
        code,result=self.cmd('echo -e "hello\n" | ant -buildfile build_for_input.xml | grep -F "hello"')
        self.assertEquals(0, code, f"test failed with option -buildfile with return code {code}")
        code,result=self.cmd('ant -noinput -f build_for_input.xml 2>&1 | grep -Pz "BUILD FAILED"')
        self.assertEquals(0, code, f"test failed with option -noinput with return code {code}")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf *.xml *.log')

