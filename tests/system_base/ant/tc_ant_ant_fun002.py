"""
@File:      tc_ant_ant_fun002.py
@Time:      2024/7/19 17:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""
from common.basetest import LocalTest
import subprocess


class Test(LocalTest):
    """
    See tc_ant_ant_fun002.yaml for details

    :avocado: tags=P1,noarch,local,ant
    """
    PARAM_DIC = {"pkg_name": "ant"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat << EOF > build.xml
<?xml version="1.0"?>
<project name="HelloWorld" default="test" basedir="">
    <property environment="env" />
    <target name="test">
        <echo message="ant test" />
    </target>
</project>

EOF
        """
        self.cmd(cmdline)
        pass

    def test(self):
        code, result = self.cmd(" ant -diagnostics  | grep 'Ant diagnostics report'")
        self.assertFalse(code, "test failed with option -diagnostics")
        code, result = self.cmd("ant -quiet | grep  '\S*\[echo\] ant test' && ant -quiet | grep  'BUILD SUCCESSFUL'")
        self.assertFalse(code, "test failed with option -quiet")
        code,result=self.cmd("ant -q | grep  '\S*\[echo\] ant test' && ant -quiet | grep  'BUILD SUCCESSFUL'")
        self.assertFalse(code, "test failed with option -q")
        code, result = self.cmd("ant -silent | grep  'Buildfile*'&& ant -silent | grep 'ant test'")
        self.assertFalse(code, "test failed with option -silent")
        code, result = self.cmd("ant -S | grep  'Buildfile*'&& ant -silent | grep 'ant test'")
        self.assertFalse(code, "test failed with option -S")
        code, result = self.cmd("ant -verbose | grep  'BUILD SUCCESSFUL'")
        self.assertFalse(code,"test failed with option -verbose")
        code, result = self.cmd("ant -v | grep  'BUILD SUCCESSFUL'")
        self.assertFalse(code,"test failed with option -v")
        code, result = self.cmd("ant -debug | grep 'BUILD SUCCESSFUL'")
        self.assertFalse(code, "test failed with option -debug")
        code, result = self.cmd("ant -d | grep  'BUILD SUCCESSFUL'")
        self.assertFalse(code, "test failed with option -d")
        code, result = self.cmd("ant -e | grep 'BUILD SUCCESSFUL'")
        self.assertFalse(code, "test failed with option -e")
        code, result = self.cmd("ant -emacs | grep  'BUILD SUCCESSFUL'")
        self.assertFalse(code, "test failed with option -emacs")

    def tearDown(self):
        self.cmd('rm -rf build.xml ')
        super().tearDown(self.PARAM_DIC)