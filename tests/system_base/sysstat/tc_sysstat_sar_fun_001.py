# -*- encoding: utf-8 -*-

"""
@File:      tc_sysstat_sar_fun_001.py
@Time:      2024/8/9 10:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_sysstat_sar_fun_001.yaml for details

    :avocado: tags=P1,noarch,local,sysstat
    """

    PARAM_DIC = {"pkg_name": "sysstat"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        self.cmd("systemctl status sysstat > sysstat.log 2>&1",ignore_status=True)
        code, ret = self.cmd("cat sysstat.log | grep -q 'systemctl daemon-reload'", ignore_status=True)
        if code == 0:
            self.cmd("systemctl daemon-reload")
        self.cmd("systemctl restart sysstat")
        code, result1=self.cmd("systemctl status sysstat | grep -E 'running|active'")
        self.assertIn("active", result1)
        code, result2=self.cmd("sar -b | grep CPU")
        self.assertIn("CPU", result2)
        code, result3=self.cmd("sar -u -o testcpulog 5 3 | grep CPU | grep user")
        self.assertIn("user", result3)
        code, file1=self.cmd("test -f testcpulog && echo $?")
        self.assertIn("0", file1)
        code, result4=self.cmd("sar -r -o testmemlog 5 3 | grep kbmemfree | grep kbmemused")
        self.assertIn("kbmemused", result4)
        code, file2=self.cmd("test -f testmemlog && echo $?")
        self.assertIn("0", file2)
        code, result5=self.cmd("sar -b -o testiolog 5 3 | grep tps | grep rtps")
        self.assertIn("rtps", result5)
        code, file3=self.cmd("test -f testiolog && echo $?")
        self.assertIn("0", file3)
        code, result6=self.cmd("sar --help | grep Usage")
        self.assertIn("Usage", result6)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf testcpulog testmemlog testiolog sysstat.log")
        self.cmd("systemctl stop sysstat")
