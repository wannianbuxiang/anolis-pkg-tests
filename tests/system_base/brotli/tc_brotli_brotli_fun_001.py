# -*- encoding: utf-8 -*-

"""
@File:      tc_brotli_brotli_fun_001.py
@Time:      2024/11/21 17:31:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_brotli_brotli_fun_001.yaml for details

    :avocado: tags=P1,noarch,local,brotli
    """
    PARAM_DIC = {"pkg_name": "brotli"}

    def setUp(self):
       super().setUp(self.PARAM_DIC)
       self.cmd("echo 'Hello, Brotli!' > brotli_testfile")

    def test(self):
       self.cmd("rpm -qa |grep brotli")
       self.cmd("brotli -f brotli_testfile")
       code, result = self.cmd("ls")
       self.assertIn("brotli_testfile.br", result)
       self.cmd("rm -rf brotli_testfile")
       self.cmd("brotli -d  brotli_testfile.br")
       code, result = self.cmd("cat brotli_testfile")
       self.assertIn("Hello, Brotli!", result)
       	 
    def tearDown(self):
       super().tearDown(self.PARAM_DIC)		
       self.cmd("rm -rf brotli_testfile*")	
