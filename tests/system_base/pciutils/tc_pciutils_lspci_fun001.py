# -*- encoding: utf-8 -*-

"""
@File:      tc_pciutils_lspci_fun001.py
@Time:      2024/07/02 16:51:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_pciutils_lspci_fun001.yaml for details

    :avocado: tags=P1,noarch,local,pciutils
    """
    PARAM_DIC = {"pkg_name": "pciutils"}

    def setUp(self):
       super().setUp(self.PARAM_DIC)
              	  	 
    def test(self):
        self.cmd("lspci")
        self.cmd("lspci -k")
        self.cmd("lspci -t")
        self.cmd("lspci | grep -i bridge")
        self.cmd("lspci -n | grep rev")
        self.cmd("lspci -v | grep -A3 -i bridge")   

    def tearDown(self):
       super().tearDown(self.PARAM_DIC)		
       