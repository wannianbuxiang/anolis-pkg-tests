# -*- encoding: utf-8 -*-

"""
@File:      tc_pcre2_pcre2_fun005.py
@Time:      2024/3/4 15:20:45
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

import os
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_pcre2_pcre2_fun005.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "pcre2 pcre2-tools"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        code, result = self.cmd('echo "foobar" | pcre2grep -o "(?<=foo)bar"')
        self.assertEqual("bar", result, "Test failed: Pattern ‘(?<=foo)bar’, did not match in 'foobar'")
        code, result = self.cmd('echo "foobar" | pcre2grep -o "foo(?=bar)"')
        self.assertEqual("foo", result, "Test failed: Pattern ‘foo(?=bar)’, did not match in 'foobar'")
        code, result = self.cmd('echo "foo bar" | pcre2grep -o "\\bbar\\b"')
        self.assertEqual("bar", result, "Test failed: Pattern ‘\bbar\b’, did not match in 'foo bar'")
        code, result = self.cmd('echo "foobarbar" | pcre2grep -o "\\Bbar\\B"')
        self.assertEqual("bar", result, "Test failed: Pattern ‘\Bbar\B’, did not match in 'foobarbar'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
