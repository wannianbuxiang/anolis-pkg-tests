# -*- encoding: utf-8 -*-

"""
@File:      tc_pcre2_pcre2_fun009.py
@Time:      2024/3/1 13:55:01
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_pcre2_pcre2_fun009.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "pcre2 pcre2-devel pcre2-static gcc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >pcre2_api_testfile.c <<"EOF"
#define PCRE2_CODE_UNIT_WIDTH 8
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pcre2.h>

int main()
{
    // 定义正则表达式
    PCRE2_SPTR pattern = (PCRE2_SPTR)"hell[a-z]";
    // 定义输入字符串
    PCRE2_SPTR input = (PCRE2_SPTR) "hellp hello world, PCRE2!";

    // 编译正则表达式
    pcre2_code *re;
    int errorcode;
    PCRE2_SIZE erroroffset;
    re = pcre2_compile(pattern, PCRE2_ZERO_TERMINATED, 0, &errorcode, &erroroffset, NULL);

    // 匹配正则表达式
    pcre2_match_data *match_data = pcre2_match_data_create_from_pattern(re, NULL);

    int rc = pcre2_match(re, input, PCRE2_ZERO_TERMINATED, 0, 0, match_data, NULL);

    PCRE2_SIZE *ovector = pcre2_get_ovector_pointer(match_data);
    // 输出匹配结果
    while (rc > 0)
    {
        printf("Match:\\n");
        PCRE2_SPTR substring_start;
        PCRE2_SIZE substring_length;
        for (int i = 0; i < rc; i++)
        {
            substring_start = input + ovector[2 * i];
            substring_length = ovector[2 * i + 1] - ovector[2 * i];
            printf("%2d: %.*s\\n", i, (int)substring_length, (char *)substring_start);
        }

        input = substring_start + substring_length;
        rc = pcre2_match(re, input, PCRE2_ZERO_TERMINATED, 0, 0, match_data, NULL);

        ovector = pcre2_get_ovector_pointer(match_data);
    }

    // 释放内存
    pcre2_match_data_free(match_data);
    pcre2_code_free(re);

    return 0;
}
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -Wall -o pcre2_api_testfile pcre2_api_testfile.c -lpcre2-8")
        code, result = self.cmd("./pcre2_api_testfile")
        self.assertIn("hello", result)
        self.assertIn("hellp", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf pcre2_api_testfile*")