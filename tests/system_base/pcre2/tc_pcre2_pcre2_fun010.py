# -*- encoding: utf-8 -*-

"""
@File:      tc_pcre2_pcre2_fun009.py
@Time:      2024/3/1 13:55:01
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

import sys
import subprocess
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_pcre2_pcre2_fun009.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "pcre2 pcre2-devel pcre2-static gcc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >pcre2_errorule_testfile.c <<"EOF"
#define PCRE2_CODE_UNIT_WIDTH 8
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pcre2.h>

int main() {
    // 一个错误的正则表达式（缺少闭合的括号）
    PCRE2_SPTR pattern = (PCRE2_SPTR)"(\\\w+$";
    
    // 用来匹配的字符串
    PCRE2_SPTR subject = (PCRE2_SPTR)"Hello, world!";
    
    int errorcode;
    PCRE2_SIZE erroroffset;
    
    // 编译正则表达式
    pcre2_code *re = pcre2_compile(
        pattern,                // 正则表达式
        PCRE2_ZERO_TERMINATED,  // 长度
        0,                      // 编译选项
        &errorcode,             // 错误代码
        &erroroffset,           // 错误偏移
        NULL                    // 编译上下文
    );
    
    // 检查编译结果
    if (re == NULL) {
        PCRE2_UCHAR buffer[120];
        pcre2_get_error_message(errorcode, buffer, sizeof(buffer));
        printf("PCRE2 compilation failed at offset %d: %s\\n", (int)erroroffset, buffer);
        return 1;
    }
    
    // 如果编译成功，创建匹配数据结构
    pcre2_match_data *match_data = pcre2_match_data_create_from_pattern(re, NULL);
    
    // 执行匹配操作
    int rc = pcre2_match(
        re,                     // 编译后的正则表达式
        subject,                // 要匹配的字符串
        PCRE2_ZERO_TERMINATED,  // 字符串长度
        0,                      // 从字符串开始处匹配
        0,                      // 选项
        match_data,             // 匹配数据
        NULL                    // 匹配上下文
    );
    
    // 检查匹配结果
    if (rc < 0) {
        if (rc == PCRE2_ERROR_NOMATCH) {
            printf("No match\\n");
        } else {
            printf("Matching error %d\\n", rc);
        }
        pcre2_match_data_free(match_data);
        pcre2_code_free(re);
        return 1;
    }
    
    // 如果匹配成功
    printf("Match succeeded\\n");
    
    pcre2_match_data_free(match_data);
    pcre2_code_free(re);
    
    return 0;
}
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -Wall -o pcre2_errorule_testfile pcre2_errorule_testfile.c -lpcre2-8")
        code, result = self.cmd("./pcre2_errorule_testfile", ignore_status=True)
        self.assertTrue(code == 1, msg="正则规则异常时，结果与预期不符")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf pcre2_errorule_testfile*")