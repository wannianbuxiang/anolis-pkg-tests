# -*- encoding: utf-8 -*-

"""
@File:      tc_pcre2_pcre2_fun003.py
@Time:      2024/3/1 13:55:01
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_pcre2_pcre2_fun003.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "pcre2 pcre2-tools"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        # 测试具有不同量词的模式 {n}、{n,}、{n,m}
        text = "hello\nhelllo\nhelllllo"
        patterns = ["l{2}", "l{3,}", "l{2,3}"]
        for pattern in patterns:
            code, result = self.cmd(f'echo "{text}"| pcre2grep -e "{pattern}"')
            self.assertTrue(code == 0, msg="Test failed: Pattern '{pattern}' did not match in '{text}'")

        # 测试懒惰量词和贪婪量词
        text = "aaaaaab"
        patterns = ["a*", "a*?"]
        for pattern in patterns:
            code, result = self.cmd(f'echo "{text}" | pcre2grep -o "{pattern}"')
            self.assertTrue(code == 0, msg="Test failed: Pattern 'a*' did not match in 'aaaaaab'")
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
