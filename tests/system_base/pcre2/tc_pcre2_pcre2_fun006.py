# -*- encoding: utf-8 -*-

"""
@File:      tc_pcre2_pcre2_fun006.py
@Time:      2024/3/4 14:08:45
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

import os
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_pcre2_pcre2_fun006.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "pcre2 pcre2-tools glibc-locale-source"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        code, _ = self.cmd("locale -a | grep en_US.UTF-8", ignore_status=True)
        if code != 0:
            self.cmd("localedef -i en_US -f UTF-8 en_US.UTF-8")
        os.environ['LC_ALL'] = 'en_US.UTF-8'
        os.environ['LANG'] = 'en_US.UTF-8'
        env1 = os.getenv('LC_ALL')
        self.assertEqual("en_US.UTF-8", env1, "the env: LC_ALL set failed")
        env2 = os.getenv('LANG')
        self.assertEqual("en_US.UTF-8", env2, "the env: LANG set failed")
        lists = [["Unicode 字母：äöüß", "\p{L}+"],
                 ["希腊脚本：Αλφαβητα", "\p{Greek}+"],
                 ["中文字符：汉字", "\p{Han}+"]]
        for lst in lists:
            code, result = self.cmd(f'echo "{lst[0]}" | pcre2grep --only-matching --utf "{lst[1]}"')
            self.assertTrue(code == 0, f"Test failed: Pattern '{lst[1]}' did not match in '{lst[0]}'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
