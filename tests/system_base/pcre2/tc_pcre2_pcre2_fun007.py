# -*- encoding: utf-8 -*-

"""
@File:      tc_pcre2_pcre2_fun007.py
@Time:      2024/3/4 14:08:45
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

import os
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_pcre2_pcre2_fun007.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "pcre2 pcre2-tools"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        code, result = self.cmd('echo "AbCDe" | pcre2grep -i "abcde"')
        self.assertEqual("AbCDe", result, "Test failed: Pattern ‘不区分大小写（i）’, did not match in 'AbCDe'")
        code, result = self.cmd('printf "start\nmiddle\nend" | pcre2grep -M "^middle$"')
        self.assertEqual("middle", result, "Test failed: Pattern ‘多行（m）匹配’, did not match in 'start\nmiddle\nend'")
        code, result = self.cmd('printf "line1\nline2\nlilne3" | pcre2grep -s "lin.*"')
        self.assertIn("line1", result, "Test failed: Pattern ‘.*匹配所有’, did not match in 'line 1\nline 2\nlilne 3'")
        self.assertIn("line2", result, "Test failed: Pattern ‘.*匹配所有’, did not match in 'line 1\nline 2\nlilne 3'")
        self.assertNotIn("lilne3", result, "Test failed: Pattern ‘.*匹配所有’, did not match in 'line 1\nline 2\nlilne 3'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
