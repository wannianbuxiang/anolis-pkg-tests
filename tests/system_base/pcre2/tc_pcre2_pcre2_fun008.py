# -*- encoding: utf-8 -*-

"""
@File:      tc_pcre2_pcre2_fun008.py
@Time:      2024/3/4 16:40:45
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

import os
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_pcre2_pcre2_fun008.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "pcre2 pcre2-tools"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        code, result = self.cmd('echo "abc123xyz456" | pcre2grep -o "((\w+)(\d+))+"')
        self.assertTrue(code == 0, "Test failed: Pattern ‘((\w+)(\d+))+’, did not match in 'abc123xyz456'")
        pattern = "(?i)(?:(?:\\b(?:a(?:n(?:o(?:t(?:h(?:e(?:r)?)?)?)?)?)?)\\b\s+)+(?:\w+))|(?:\\b\w+(?:\s+\\b\w+\\b(?=.*\\banother\\b)))+"
        code, result = self.cmd(f'echo "another test is another example of a pattern" | pcre2grep -o "{pattern}"')
        self.assertTrue(code == 0, f"Test failed: Pattern {pattern}, did not match in 'another test is another example of a pattern'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
