# -*- encoding: utf-8 -*-

"""
@File:      tc_pcre2_pcre2_fun004.py
@Time:      2024/3/4 10:09:01
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_pcre2_pcre2_fun004.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "pcre2 pcre2-tools"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        # 分组和子模式
        lists = [["The number is 42", "number is (\d+)"],
                 ["This is a test", "This (?:is a) test"],
                 ["repeated repeated words", "(\\b\\w+) \\1\\b"],
                 ["abc", "(a)?(?(1)b|c)"]]
        for lst in lists:
            code, result = self.cmd(f'echo "{lst[0]}" | pcre2grep -o1 "{lst[1]}"')
            self.assertTrue(code == 0, f"Test failed: Pattern '{lst[1]}' did not match in '{lst[0]}'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
