# -*- encoding: utf-8 -*-

"""
@File:      tc_pcre2_pcre2_fun002.py
@Time:      2024/3/1 13:55:01
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_pcre2_pcre2_fun002.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "pcre2 pcre2-tools"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        # 预定义的字符类, \d、\w
        text = "test: 123\ntest: user_rg12g345\n%$@$"
        patterns = ["\\d+", "\\w+"]
        for pattern in patterns:
            code, result = self.cmd(f'echo "{text}"| pcre2grep -e "{pattern}"')
            self.assertTrue(code == 0, msg="Test failed: Pattern '{pattern}' did not match in '{text}'")

        # 自定义字符集[0-9a-z]
        text = "ggAD554\nATJYJYU"
        code, result = self.cmd(f'echo "{text}" | pcre2grep -e [0-9a-z]')
        self.assertIn("ggAD554", result, msg="Test failed: Pattern '[0-9a-z]' did not match in '{text}'")
        self.assertNotIn("ATJYJYU", result, msg="Test failed: Pattern '[0-9a-z]' did not match in '{text}'")
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
