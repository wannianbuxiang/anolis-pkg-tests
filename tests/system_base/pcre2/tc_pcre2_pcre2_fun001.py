# -*- encoding: utf-8 -*-

"""
@File:      tc_pcre2_pcre2_fun001.py
@Time:      2024/3/1 13:55:01
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_pcre2_pcre2_fun001.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "pcre2 pcre2-tools"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        # 简单文本匹配
        pattern = "hello"
        text = "abchelloefg\nhelo"
        code, result = self.cmd(f'echo "{text}" | pcre2grep -e {pattern}')
        self.assertIn("abchelloefg", result, msg=f"Test failed: Pattern '{pattern}' did not match in '{text}'")
        self.assertNotIn("helo", result, msg=f"Test failed: Pattern '{pattern}' did not match in '{text}'")

        # 特殊字符匹配
        pattern = "hello\.world"
        text = "feghello.worldgnhj.few\nhellow.orld"
        code, result = self.cmd(f'echo "{text}" | pcre2grep -e {pattern}')
        self.assertIn("feghello.worldgnhj.few", result, msg=f"Test failed: Pattern '{pattern}' did not match in '{text}'")
        self.assertNotIn("hellow.orld", result, msg=f"Test failed: Pattern '{pattern}' did not match in '{text}'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
