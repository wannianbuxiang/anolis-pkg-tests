# -*- encoding: utf-8 -*-

"""
@File:      tc_zlib_zlib_fun001.py
@Time:      2024/5/15 10:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest
import subprocess

class Test(LocalTest):
    """
    See tc_zlib_zlib_fun001.yaml for details

    :avocado: tags=P1,noarch,local,zlib
    """
    PARAM_DIC = {"pkg_name": "zlib zlib-devel gcc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat>test.c<<EOF    
#include <stdio.h>
#include <zlib.h>
int main()
{
    unsigned char strSrc[] = "hello world! aaaaa bbbbb ccccc ddddd 中文测试 yes";
    unsigned char buf[1024] = {0};
    unsigned char strDst[1024] = {0};
    unsigned long srcLen = sizeof(strSrc);
    unsigned long bufLen = sizeof(buf);
    unsigned long dstLen = sizeof(strDst);
    printf("Src string:%s Length:%ld", strSrc, srcLen);
    printf("\\n");
    compress(buf, &bufLen, strSrc, srcLen);
    printf("After Compressed Length:%ld", bufLen);
    printf("\\n");
    uncompress(strDst, &dstLen, buf, bufLen);
    printf("After UnCompressed Length:%ld",dstLen);
    printf("\\n");
    printf("UnCompressed String:%s",strDst);
    return 0;
}

EOF"""
        self.cmd(cmdline)

    def test(self):        	
        code,result1=self.cmd("rpm -qa")
        self.assertIn("zlib", result1)
        self.cmd("gcc -Wall -o test test.c -lz")
        code,result2=self.cmd("./test")
        self.assertIn("hello world! aaaaa bbbbb ccccc ddddd 中文测试 yes", result2)
    
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf test  test.c")
