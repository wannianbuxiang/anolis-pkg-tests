# -*- encoding: utf-8 -*-

"""
@File:      tc_aspell_aspell_fun001.py
@Time:      2024/8/13 16:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_aspell_aspell_fun001.yaml for details

    :avocado: tags=P1,noarch,local,aspell
    """
    PARAM_DIC = {"pkg_name": "aspell aspell-en"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > test.txt << EOF
This is a smaple text with an error.
EOF"""
        cmdline1 = """cat > test1.txt << EOF
This is a test.
Here is another line with an errror.
EOF"""
        self.cmd(cmdline)
        self.cmd(cmdline1)

    def test(self):        	
        code,result=self.cmd("aspell list -d en_US < test.txt")
        self.assertIn("smaple",result)
        code,result=self.cmd("aspell list -d en_US < test1.txt")
        self.assertIn("errror",result)
        code,result=self.cmd("aspell -a  < test.txt")
        self.assertIn("sample",result)
        code,result=self.cmd("echo 'I am goin to the store.' | aspell -a")
        self.assertIn("going",result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf test.txt test1.txt")