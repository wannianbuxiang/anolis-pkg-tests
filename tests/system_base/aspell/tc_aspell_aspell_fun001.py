# -*- encoding: utf-8 -*-

"""
@File:      tc_aspell_aspell_fun002.py
@Time:      2024/8/13 16:31:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_aspell_aspell_fun002.yaml for details

    :avocado: tags=P1,noarch,local,aspell
    """
    PARAM_DIC = {"pkg_name": "aspell aspell-en"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        
    def test(self):        	
        code, result = self.cmd("aspell version")
        self.assertIn("Ispell Version", result)
        self.cmd("aspell help > aspell.log", ignore_status=True)
        code, result = self.cmd("cat aspell.log", ignore_status=True)
        self.assertIn("Usage:", result)
      
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf aspell.log")
