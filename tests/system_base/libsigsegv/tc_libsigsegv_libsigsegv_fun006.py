# -*- encoding: utf-8 -*-

"""
@File:      tc_libsigsegv_libsigsegv_fun006.py
@Time:      2024/3/11 10:18:20
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

import subprocess
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libsigsegv_libsigsegv_fun006.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libsigsegv libsigsegv-devel gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >libsigsegv_testfile.c<<"EOF"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sigsegv.h>

/* 段错误处理函数 */
void segv_handler(int signum, siginfo_t *info, void *context) {
    printf("Caught segmentation fault! Faulty address is %p\\n", info->si_addr);
    exit(1);
}

int main() {
    struct sigaction sa;

    sa.sa_sigaction = segv_handler; // 设置段错误处理函数
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_SIGINFO;

    /* 安装信号处理程序 */
    if (sigaction(SIGSEGV, &sa, NULL) == -1) {
        perror("Failed to install SIGSEGV signal handler");
        return EXIT_FAILURE;
    }

    /* 字符串常量，存储在只读数据段 */
    char *str = "This is a read-only string";

    /* 故意尝试修改只读字符串 */
    str[0] = 't'; /* 这一行将会触发段错误 */

    /* 如果段错误处理程序可以正常恢复，以下行将不会执行 */
    printf("This line should not be executed.\\n");

    return 0;
}
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o libsigsegv_testfile libsigsegv_testfile.c -lsigsegv")
        result = subprocess.run(["./libsigsegv_testfile"], stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout.decode()
        self.log.info(result)
        self.assertIn("Caught segmentation fault!", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf libsigsegv_testfile*")
