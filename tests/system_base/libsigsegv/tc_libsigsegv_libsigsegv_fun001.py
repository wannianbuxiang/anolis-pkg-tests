# -*- encoding: utf-8 -*-

"""
@File:      tc_libsigsegv_libsigsegv_fun001.py
@Time:      2024/3/11 10:18:20
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

import subprocess
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libsigsegv_libsigsegv_fun001.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libsigsegv libsigsegv-devel gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >libsigsegv_testfile.c<<"EOF"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sigsegv.h>

/* 段错误处理函数 */
static int handle_sigsegv(void *fault_address, int serious)
{
    printf("Segmentation fault at address: %p\\n", fault_address);
    exit(2); /* 退出程序，并返回特定的错误码表示段错误 */
    return 0; /* 这行不会被执行，因为前面已经退出了程序 */
}

int main(void)
{
    /* 安装 libsigsegv 的段错误处理程序 */
    sigsegv_install_handler(handle_sigsegv);

    /* 故意制造段错误 */
    int *p = NULL;
    *p = 42; /* 这里将会触发段错误 */

    /* 如果处理函数可以正常恢复，这行代码将不会执行 */
    printf("This line will not be executed.\\n");

    return 0;
}
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o libsigsegv_testfile libsigsegv_testfile.c -lsigsegv")
        result = subprocess.run(["./libsigsegv_testfile"], stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout.decode()
        self.log.info(result)
        self.assertIn("Segmentation fault at address:", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf libsigsegv_testfile*")