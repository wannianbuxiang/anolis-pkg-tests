# -*- encoding: utf-8 -*-

"""
@File:      tc_libsigsegv_libsigsegv_fun010.py
@Time:      2024/3/12 10:18:20
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

import subprocess
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libsigsegv_libsigsegv_fun010.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libsigsegv libsigsegv-devel gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >libsigsegv_testfile.c<<"EOF"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <setjmp.h>

static sigjmp_buf jump_buffer;

/* 段错误处理函数 */
void segv_handler(int signum) {
    // 输出消息
    printf("Segmentation fault caught! Attempting to recover...\\n");
    // 使用 siglongjmp 跳回到之前的环境，恢复执行
    siglongjmp(jump_buffer, 1);
}

int main() {
    struct sigaction sa;
    sa.sa_handler = segv_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;

    // 安装信号处理程序
    if (sigaction(SIGSEGV, &sa, NULL) == -1) {
        perror("Failed to install SIGSEGV signal handler");
        return EXIT_FAILURE;
    }

    // 设置跳转点，如果 sigsetjmp 返回非零值，表示从信号处理程序返回
    if (sigsetjmp(jump_buffer, 1)) {
        // 从信号处理程序返回到这里
        printf("Program has recovered from segmentation fault.\\n");
    } else {
        // 正常程序执行路径
        // 故意触发段错误
        int *p = NULL;
        *p = 42;
    }

    // 程序恢复执行,预期的结果是程序会捕获段错误，然后从信号处理程序中恢复，并继续执行后续的 printf 语句
    printf("Continuing execution...\\n");

    return 0;
}
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o libsigsegv_testfile libsigsegv_testfile.c -lsigsegv")
        code, result = self.cmd("./libsigsegv_testfile", ignore_status=True)
        self.assertIn("Segmentation fault caught! Attempting to recover", result)
        self.assertIn("Program has recovered from segmentation fault", result)
        self.assertIn("Continuing execution", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf libsigsegv_testfile*")
