# -*- encoding: utf-8 -*-

"""
@File:      tc_libsigsegv_libsigsegv_fun009.py
@Time:      2024/3/12 10:18:20
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

import subprocess
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libsigsegv_libsigsegv_fun009.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libsigsegv libsigsegv-devel gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >libsigsegv_testfile.c<<"EOF"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <setjmp.h>
#include <sigsegv.h>

volatile sig_atomic_t in_handler = 0; // 标记是否已经在信号处理程序中
jmp_buf escape_point; // 用于长跳转以从嵌套的信号处理程序中恢复

/* 段错误处理函数 */
void segv_handler(int signum, siginfo_t *info, void *context) {
    if (!in_handler) {
        // 如果我们已经在处理程序中，使用长跳转来恢复
        siglongjmp(escape_point, 1);
    }
    in_handler = 1; // 标记我们正在处理段错误

    // 执行一些可能触发段错误的操作
    char *p = NULL;
    p[0] = 'a'; // 故意触发另一个段错误
    in_handler = 0; // 重置标记
}

int main() {
    struct sigaction sa;

    sa.sa_sigaction = segv_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_SIGINFO;
    // 安装信号处理程序
    if (sigaction(SIGSEGV, &sa, NULL) == -1) {
        perror("Failed to install SIGSEGV signal handler");
        return EXIT_FAILURE;
    }

    if (sigsetjmp(escape_point, 1) == 0) {
        // 故意触发段错误
        char *p = NULL;
        p[0] = 'a'; // 这会导致第一个段错误
    } else {
        // 如果通过长跳转恢复，执行必要的清理
        printf("Recovered from nested segmentation fault.\\n");
    }

    // 如果处理程序可以正常恢复，以下代码将会执行
    printf("Program recovered and continuing normal execution.\\n");
    return 0;
}
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o libsigsegv_testfile libsigsegv_testfile.c -lsigsegv")
        code, result = self.cmd("./libsigsegv_testfile", ignore_status=True)
        self.assertIn("Recovered from nested segmentation fault.", result)
        self.assertIn("Program recovered and continuing normal execution.", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf libsigsegv_testfile*")
