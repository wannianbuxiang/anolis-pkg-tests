# -*- encoding: utf-8 -*-

"""
@File:      tc_libsigsegv_libsigsegv_fun002.py
@Time:      2024/3/11 10:18:20
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

import subprocess
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libsigsegv_libsigsegv_fun002.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libsigsegv libsigsegv-devel gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >libsigsegv_testfile.c<<"EOF"
#include <stdio.h>
#include <stdlib.h>
#include <sigsegv.h>

/* 栈溢出处理函数 */
static int overflow_handler(void *fault_address, int serious)
{
    fprintf(stderr, "Stack overflow detected at address: %p\\n", fault_address);
    exit(2);
    return 0;
}

/* 一个递归函数，会导致栈溢出 */
void cause_stack_overflow(size_t count)
{
    volatile char buffer[1024 * 1024]; // Allocate a buffer on the stack
    buffer[0] = count; // Use the buffer to avoid optimization out by the compiler
    if (count > 0) {
        cause_stack_overflow(count - 1); // Recurse until the stack overflows
    }
}

int main(void)
{
    /* 安装 libsigsegv 的栈溢出处理程序 */
    sigsegv_install_handler(overflow_handler);

    printf("Causing stack overflow...\\n");
    cause_stack_overflow(1000000); // Start the recursion, adjust the count as needed

    /* 如果栈溢出处理程序可以正常恢复，以下行将不会执行 */
    printf("This line should not be executed.\\n");

    return 0;
}
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o libsigsegv_testfile libsigsegv_testfile.c -lsigsegv")
        # libsigsegv 可以处理很多种内存访问错误，但是栈溢出（stack overflow）可能不总是能被它捕获，因为栈溢出通常是由于无限或深度递归导致的，这种情况下可能没有触发页面错误（page fault），而是直接触发了程序的终止
        # https://bugzilla.openanolis.cn/show_bug.cgi?id=8618
        code, result = self.cmd("./libsigsegv_testfile", ignore_status=True)
        self.assertTrue(code == -11)
        self.assertNotIn("This line should not be executed", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf libsigsegv_testfile*")