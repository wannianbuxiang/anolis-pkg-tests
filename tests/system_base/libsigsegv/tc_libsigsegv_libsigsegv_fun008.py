# -*- encoding: utf-8 -*-

"""
@File:      tc_libsigsegv_libsigsegv_fun008.py
@Time:      2024/3/12 10:18:20
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

import subprocess
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libsigsegv_libsigsegv_fun008.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libsigsegv libsigsegv-devel gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >tempfile<<"EOF"
test
test
test
}
EOF'''
        self.cmd(cmdline)
        cmdline = '''cat >libsigsegv_testfile.c<<"EOF"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sigsegv.h>

int volatile should_exit = 0; // 用于指示程序应该退出
int *volatile dynamic_memory = NULL; // 动态分配的内存
FILE *volatile file_resource = NULL; // 文件资源

/* 段错误处理函数 */
void segv_handler(int signum, siginfo_t *info, void *context) {
    write(STDERR_FILENO, "Caught segmentation fault! Performing cleanup and exiting.\\n", 60);

    // 关闭文件描述符
    if (file_resource != NULL) {
        fclose(file_resource); // 请注意，fclose本身并不是异步信号安全的
        file_resource = NULL;
    }

    // 释放动态分配的内存
    if (dynamic_memory != NULL) {
        free((void*)dynamic_memory); // 同样，free不是异步信号安全的
        dynamic_memory = NULL;
    }
    printf("关闭文件描述符、释放内存等成功.\\n");
    should_exit = 1; // 设置退出标志
    _exit(1); // 退出程序
}

int main() {
    struct sigaction sa;

    sa.sa_sigaction = segv_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_SIGINFO;

    // 安装信号处理程序
    if (sigaction(SIGSEGV, &sa, NULL) == -1) {
        perror("Failed to install SIGSEGV signal handler");
        return EXIT_FAILURE;
    }

    // 打开文件资源
    file_resource = fopen("tempfile", "w+");
    if (!file_resource) {
        perror("Failed to open file");
        return EXIT_FAILURE;
    }

    // 分配动态内存
    dynamic_memory = (int *)malloc(sizeof(int) * 100);
    if (!dynamic_memory) {
        perror("Failed to allocate memory");
        fclose(file_resource);
        return EXIT_FAILURE;
    }

    // 故意触发段错误
    int *p = NULL;
    *p = 42;

    // 段错误处理程序将接管，以下代码应该不会执行
    printf("This line should not be executed.\\n");

    // 清理资源
    fclose(file_resource);
    free(dynamic_memory);

    return 0;
}
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o libsigsegv_testfile libsigsegv_testfile.c -lsigsegv")
        code, result = self.cmd("./libsigsegv_testfile", ignore_status=True)
        self.assertIn("Caught segmentation fault! Performing cleanup and exiting.", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf libsigsegv_testfile* tempfile")
