# -*- encoding: utf-8 -*-

"""
@File:      tc_libsigsegv_libsigsegv_fun007.py
@Time:      2024/3/11 10:18:20
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

import subprocess
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libsigsegv_libsigsegv_fun007.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libsigsegv libsigsegv-devel gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >libsigsegv_testfile.c<<"EOF"
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <signal.h>
#include <sigsegv.h>

/* 段错误处理函数 */
void segv_handler(int signum, siginfo_t *info, void *context) {
    printf("Caught segmentation fault in thread! Faulty address is %p\\n", info->si_addr);
    exit(1);
}

/* 线程工作函数，将导致段错误 */
void* thread_func(void *arg) {
    /* 故意造成段错误 */
    int *p = NULL;
    *p = 42; /* 这一行将会触发段错误 */
    return NULL;
}

int main() {
    pthread_t thread;
    struct sigaction sa;

    sa.sa_sigaction = segv_handler; // 设置段错误处理函数
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_SIGINFO;

    /* 安装信号处理程序 */
    if (sigaction(SIGSEGV, &sa, NULL) == -1) {
        perror("Failed to install SIGSEGV signal handler");
        return EXIT_FAILURE;
    }

    /* 创建一个新线程，线程将执行 thread_func 函数 */
    if (pthread_create(&thread, NULL, thread_func, NULL) != 0) {
        perror("Failed to create thread");
        return EXIT_FAILURE;
    }

    /* 等待线程结束 */
    if (pthread_join(thread, NULL) != 0) {
        perror("Failed to join thread");
        return EXIT_FAILURE;
    }

    printf("This line should not be executed if segmentation fault occurred.\\n");

    return 0;
}
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o libsigsegv_testfile libsigsegv_testfile.c -lsigsegv -pthread")
        result = subprocess.run(["./libsigsegv_testfile"], stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout.decode()
        self.log.info(result)
        self.assertIn("Caught segmentation fault in thread!", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf libsigsegv_testfile*")
