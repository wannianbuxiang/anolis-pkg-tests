# -*- encoding: utf-8 -*-

"""
@File:      tc_libsigsegv_libsigsegv_fun003.py
@Time:      2024/3/11 10:18:20
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

import subprocess
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libsigsegv_libsigsegv_fun003.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libsigsegv libsigsegv-devel gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >libsigsegv_testfile.c<<"EOF"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sigsegv.h>

/* 段错误处理函数 */
static int segv_handler(void *address, int serious)
{
    printf("Invalid memory access at address: %p\\n", address);
    exit(1);
    return 0;
}

int main(void)
{
    /* 安装 libsigsegv 的段错误处理程序 */
    sigsegv_install_handler(segv_handler);

    /* 分配堆内存 */
    int *p = malloc(sizeof(int));
    if (p == NULL) {
        perror("Failed to allocate memory");
        return EXIT_FAILURE;
    }

    /* 释放堆内存 */
    free(p);
    p = NULL;

    /* 尝试访问释放的内存 */
    printf("Attempting to access freed memory...\\n");
    *p = 42; /* 这里将触发段错误 */

    /* 如果段错误处理程序可以正常恢复，以下行将不会执行 */
    printf("This line should not be executed.\\n");

    return 0;
}
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o libsigsegv_testfile libsigsegv_testfile.c -lsigsegv")
        result = subprocess.run(["./libsigsegv_testfile"], stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout.decode()
        self.log.info(result)
        self.assertIn("Attempting to access freed memory", result)
        self.assertIn("Invalid memory access at address", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf libsigsegv_testfile*")