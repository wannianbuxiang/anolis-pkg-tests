# -*- encoding: utf-8 -*-

"""
@File:      tc_lsof_lsof_fun001.py
@Time:      2024/4/25 10:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest
from common.service import ServiceManager

class Test(LocalTest):
    """
    See tc_lsof_lsof_fun001.yaml for details

    :avocado: tags=P1,noarch,local,lsof
    """
    PARAM_DIC = {"pkg_name": "lsof"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
      

    def test(self):	
        self.cmd("command  -v lsof|grep /usr/bin/lsof")
        self.cmd("lsof -u root >testlog &")
        self.cmd("grep -i pid testlog")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf testlog")

