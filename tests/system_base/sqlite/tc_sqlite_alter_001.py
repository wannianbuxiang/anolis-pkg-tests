# -*- encoding: utf-8 -*-

"""
@File:      tc_sqlite_alter_001.py
@Time:      2024/07/04 17:34:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_sqlite_alter_001.yaml for details

    :avocado: tags=P1,noarch,local,sqlite
    """
    PARAM_DIC = {"pkg_name": "sqlite expect"}
    
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline="""expect <<-END
    spawn sqlite3 ./test.db
    send "CREATE TABLE COMPANY１(
        ID INT PRIMARY KEY     NOT NULL,
        NAME           TEXT    NOT NULL,
        AGE            INT     DEFAULT 28
        );\\n"
    expect "sqlite>"
    send "ALTER TABLE COMPANY１ RENAME TO OLD_COMPANY;\\n"
    expect "sqlite>"
    send ".output ./output.txt\\n"
    expect "sqlite>"
    send ".table\\n"
    expect "sqlite>"
    send ".headers on\\n"
    expect "sqlite>"
    send "ALTER TABLE OLD_COMPANY ADD COLUMN SEX char(1);\\n"
    expect "sqlite>"
    send "INSERT INTO OLD_COMPANY VALUES (1, 'Paul', 32, 'female');\\n"
    expect "sqlite>"
    send "select * from OLD_COMPANY;\\n"
    expect "sqlite>"
    send ".quit\\n"
    expect eof
END"""
        self.cmd(cmdline)
        
    def test(self):
        code, result1=self.cmd("grep -cE 'OLD_COMPANY' ./output.txt")
        self.assertIn("1", result1)
        code, result2=self.cmd("grep -cE 'SEX|female' ./output.txt")
        self.assertIn("2", result2)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf ./test.db ./output.txt")
