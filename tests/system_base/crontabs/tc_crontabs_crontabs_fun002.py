# -*- encoding: utf-8 -*-

"""
@File:      tc_crontabs_crontabs_fun002.py
@Time:      2024/6/20 15:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_crontabs_crontabs_fun002.yaml for details

    :avocado: tags=P1,noarch,local,crontabs
    """
    PARAM_DIC = {"pkg_name": "crontabs"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """echo -e "$(date -d +1min +%M) $(date -d +1min +%H) * * * echo 'CrontabTest' >> /tmp/test.txt" > /tmp/task.txt"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("sleep 70")      
        self.cmd("crontab /tmp/task.txt") 
        code, result = self.cmd("crontab -l") 
        self.assertIn("echo 'CrontabTest' >> /tmp/test.txt", result)
        self.cmd("crontab -r")
                 
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/test.txt")
        self.cmd("rm -rf /tmp/task.txt")        