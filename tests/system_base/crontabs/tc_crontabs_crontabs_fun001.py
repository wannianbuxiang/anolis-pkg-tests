# -*- encoding: utf-8 -*-

"""
@File:      tc_crontabs_crontabs_fun001.py
@Time:      2024/6/20 15:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_crontabs_crontabs_fun001.yaml for details

    :avocado: tags=P1,noarch,local,crontabs
    """
    PARAM_DIC = {"pkg_name": "crontabs"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        
    def test(self):        	
        code, result = self.cmd("crontab -V")
        self.assertIn("cron", result)
        self.cmd("man crontab > crontab.log", ignore_status=True)
        code, result = self.cmd("cat crontab.log", ignore_status=True)
        self.assertIn("CRONTAB", result)
      
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf crontab.log")
