# -*- encoding: utf-8 -*-

"""
@File:      tc_lvm2_lvm2_fun001.py
@Time:      2024/8/20 16:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_lvm2_lvm2_fun001.yaml for details

    :avocado: tags=P1,noarch,local,lvm2
    """
    PARAM_DIC = {"pkg_name": "lvm2"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("dd if=/dev/zero of=/tmp/ramdisk bs=1M count=512")
        self.cmd("dd if=/dev/zero of=/tmp/ramdisk1 bs=1M count=512")
        self.cmd("losetup /dev/loop0 /tmp/ramdisk")
        self.cmd("losetup /dev/loop1 /tmp/ramdisk1")
    	
    def test(self): 
        code, result = self.cmd("pvcreate -y /dev/loop0")
        self.assertIn("successfully",result)      
        code, result = self.cmd("pvcreate -y /dev/loop1")
        self.assertIn("successfully",result)        
        code, result = self.cmd("vgcreate test /dev/loop0 /dev/loop1")
        self.assertIn("successfully",result)          
        code, result = self.cmd("vgdisplay")
        self.assertIn("test",result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("losetup -d /dev/loop0")
        self.cmd("losetup -d /dev/loop1")
        self.cmd("rm -f /tmp/ramdisk")
        self.cmd("rm -f /tmp/ramdisk1")