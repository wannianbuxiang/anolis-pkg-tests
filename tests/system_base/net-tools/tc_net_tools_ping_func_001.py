# -*- encoding: utf-8 -*-

"""
@File:      tc_net_tools_ping_func_001.py
@Time:      2024/03/05 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

import time
import pexpect
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_net_tools_ping_func_001.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "net-tools"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code, self.ip = self.cmd("route -n | grep 'UG[ \t]' | awk '{print $2}'")

    def test(self):
        child = pexpect.spawn('ping %s' % self.ip)
        time.sleep(1)
        child.sendcontrol('c')
        child.expect(pexpect.EOF, timeout=1)
        self.assertIn("1 packets transmitted", child.before.decode())
        code, ping_result = self.cmd("ping -c 3 %s" % self.ip)
        self.assertIn("3 packets transmitted", ping_result)
        code, ping_result = self.cmd("ping -c 3 -s 100 %s" % self.ip)
        self.assertIn("108 bytes from %s" % self.ip, ping_result)
        code, ping_result = self.cmd("ping -w 3 %s" % self.ip)
        self.assertIn("3 packets transmitted", ping_result)
        code, ping_result = self.cmd("ping -c 3 -D %s" % self.ip)
        self.assertRegex(ping_result, r"\d{10}\.\d{6}")
        code, ping_result = self.cmd("ping -c 3 -f %s" % self.ip)
        self.assertIn("3 packets transmitted", ping_result)
        code, ping_result = self.cmd("ping -c 3 -i 1 %s" % self.ip)
        self.assertIn("3 packets transmitted", ping_result)
        code, ping_result = self.cmd("ping -c 3 -q %s" % self.ip)
        self.assertIn("3 packets transmitted", ping_result)
        code, ping_result = self.cmd("ping nonexistentdomain.example", ignore_status=True)
        self.assertIn("Name or service not known", ping_result)
        code, ping_result = self.cmd("ping -c notanumber %s" % self.ip, ignore_status=True)
        self.assertIn("invalid argument", ping_result)
        code, ping_result = self.cmd("ping -help", ignore_status=True)
        self.assertIn("Usage", ping_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
