# -*- encoding: utf-8 -*-

"""
@File:      tc_net_tools_arp_func_001.py
@Time:      2024/03/05 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_net_tools_arp_func_001.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "net-tools"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code, self.ip = self.cmd("route -n | grep 'UG[ \t]' | awk '{print $2}'")

    def test(self):
        code, arp_result = self.cmd("arp -a")
        self.assertIn(self.ip, arp_result)
        code, arp_result = self.cmd("arp -a %s"%self.ip)
        self.assertIn(self.ip, arp_result)
        code, arp_result = self.cmd("arp -help")
        self.assertIn("Usage:", arp_result)
        code, arp_result = self.cmd("arp -s 999.999.999.999 00:1C:42:2B:60:5A", ignore_status=True)
        self.assertIn("Unknown host", arp_result)
        code, arp_result = self.cmd("arp -s 192.168.1.100 00:11:22:33:44:ZZ", ignore_status=True)
        self.assertIn("invalid hardware address", arp_result)
        code, arp_result = self.cmd("arp -a 192.168.1.123", ignore_status=True)
        self.assertIn("entries no match found", arp_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
