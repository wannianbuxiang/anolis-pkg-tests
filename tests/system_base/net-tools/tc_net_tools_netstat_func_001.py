# -*- encoding: utf-8 -*-

"""
@File:      tc_net_tools_netstat_func_001.py
@Time:      2024/03/05 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_net_tools_netstat_func_001.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "net-tools"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        ret_c, self.eth_name = self.cmd("ip add | grep 'state UP' | awk '{print $2}' | awk -F ':' 'NR==1{print $1}'")

    def test(self):
        code, netstat_result = self.cmd("netstat -a")
        self.assertIn("LISTENING", netstat_result)
        code, netstat_result = self.cmd("netstat -l")
        self.assertIn("LISTENING", netstat_result)
        code, netstat_result = self.cmd("netstat -s")
        self.assertIn("TCP", netstat_result)
        code, netstat_result = self.cmd("netstat -t")
        self.assertIn("tcp", netstat_result)
        code, netstat_result = self.cmd("netstat -au")
        self.assertIn("udp", netstat_result)
        code, netstat_result = self.cmd("netstat -r")
        self.assertIn(self.eth_name, netstat_result)
        code, netstat_result = self.cmd("netstat -i")
        self.assertIn(self.eth_name, netstat_result)
        code, netstat_result = self.cmd("netstat -w")
        self.assertIn("Active Internet connections (w/o servers)", netstat_result)
        code, netstat_result = self.cmd("netstat -st")
        self.assertIn("TCP", netstat_result)
        code, netstat_result = self.cmd("netstat -lt")
        self.assertIn("LISTEN", netstat_result)
        code, netstat_result = self.cmd("netstat -ln")
        self.assertIn("LISTENING", netstat_result)
        code, netstat_result = self.cmd("netstat -help")
        self.assertIn("usage:", netstat_result)
        code, netstat_result = self.cmd("netstat -z", ignore_status=True)
        self.assertIn("invalid option -- 'z'", netstat_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
