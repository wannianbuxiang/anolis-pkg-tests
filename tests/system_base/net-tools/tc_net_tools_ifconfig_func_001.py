# -*- encoding: utf-8 -*-

"""
@File:      tc_net_tools_ifconfig_func_001.py
@Time:      2024/03/05 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_net_tools_ifconfig_func_001.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "net-tools iproute"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("ip link add test0 type dummy")
        self.cmd("ifconfig test0 up")

    def test(self):
        self.cmd("ifconfig test0 192.168.1.1 netmask 255.255.255.0 broadcast 192.168.1.255 hw ether 00:0c:29:3e:47:4b")
        code, ifconfig_result = self.cmd("ifconfig test0")
        self.assertIn("192.168.1.1", ifconfig_result)
        self.assertIn("255.255.255.0", ifconfig_result)
        self.assertIn("192.168.1.255", ifconfig_result)
        self.assertIn("00:0c:29:3e:47:4b", ifconfig_result)
        self.cmd("ifconfig test0 down")
        self.cmd("ifconfig test0 up")
        self.assertIn("UP", ifconfig_result)
        self.cmd("ip link delete test0")
        code, ifconfig_result = self.cmd("ifconfig -help")
        self.assertIn("Usage:", ifconfig_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
