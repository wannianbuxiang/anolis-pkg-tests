# -*- encoding: utf-8 -*-

"""
@File:      tc_ssm_ssm_fun_001.py
@Time:      2024/06/28 17:34:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ssm_ssm_fun_001.yaml for details

    :avocado: tags=P1,noarch,local,system-storage-manager
    """
    PARAM_DIC = {"pkg_name": "system-storage-manager"}
   
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        
    def test(self):
        code, result1=self.cmd("ssm --help | grep 'ssm'")
        self.assertIn("ssm", result1)
        self.cmd("ssm list")
        self.cmd("ssm list dev")
        self.cmd("ssm list pool")
        self.cmd("ssm list vol")
        self.cmd("mkdir /soft")
        self.cmd("wipefs -a /dev/sdb")
        self.cmd('echo "y" |ssm create -s 1G -n soft --fstype xfs -p mysoft /dev/sdb /soft')
        code, result2=self.cmd("df -h | grep /dev/mapper/mysoft-soft")
        self.assertIn("/dev/mapper/mysoft-soft", result2)
   
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("umount /soft")
        self.cmd('echo "y" |ssm remove /dev/mysoft/soft')
        self.cmd("ssm remove mysoft")
        self.cmd("rm -rf /soft")
