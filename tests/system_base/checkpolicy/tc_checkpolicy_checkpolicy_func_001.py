# -*- encoding: utf-8 -*-

"""
@File:      tc_checkpolicy_checkpolicy_func_001.py
@Time:      2024/03/01 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_checkpolicy_checkpolicy_func_001.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "checkpolicy"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        code, checkpolicy_result = self.cmd("checkpolicy -V")
        self.assertIn("compatibility range", checkpolicy_result)
        self.cmd("checkpolicy -h > checkpolicy.log", ignore_status=True)
        code, checkpolicy_result = self.cmd("cat checkpolicy.log", ignore_status=True)
        self.assertIn("usage:", checkpolicy_result)
        code, checkpolicy_result = self.cmd("checkpolicy -i", ignore_status=True)
        self.assertIn("invalid option", checkpolicy_result)


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf checkpolicy.log")
