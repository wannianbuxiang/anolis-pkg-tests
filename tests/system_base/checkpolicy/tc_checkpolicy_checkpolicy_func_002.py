# -*- encoding: utf-8 -*-

"""
@File:      tc_checkpolicy_checkpolicy_func_002.py
@Time:      2024/03/01 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_checkpolicy_checkpolicy_func_002.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "selinux-policy-devel checkpolicy"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > test_module.te <<EOF
module test_module 1.0;

require {
    type user_t;
    type unlabeled_t;
    class file { read write };
}

# 定义一个新的类型
type my_new_type_t;

# 创建一个新的文件类型
type my_new_file_type_t;

# 定义一条允许规则
allow user_t my_new_file_type_t:file { read write };
EOF"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("checkmodule -M -m -o test_module.mod test_module.te")
        self.cmd("ls -l test_module.mod")
        self.cmd("semodule_package -o test_module.pp -m test_module.mod")
        self.cmd("ls -l test_module.pp")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf test_module.te test_module.mod test_module.pp")
