# -*- encoding: utf-8 -*-

"""
@File:      tc_checkpolicy_checkpolicy_func_003.py
@Time:      2024/03/01 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_checkpolicy_checkpolicy_func_003.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "selinux-policy-devel checkpolicy"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > test_module.te <<EOF
module test_module 1.0;

require {
    type user_t;
    type unlabeled_t;
    class file { read write };
}

# 定义一个新的类型
type my_new_type_t;

# 创建一个新的文件类型
type my_new_file_type_t;

# 故意引入一个语法错误来测试错误处理
allow user_t my_new_file_type_t;
EOF"""
        self.cmd(cmdline)

    def test(self):
        code, checkpolicy_result = self.cmd("checkmodule -M -m -o test_module.mod test_module.te", ignore_status=True)
        self.assertIn("unknown role user_t", checkpolicy_result)


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf test_module.te")
