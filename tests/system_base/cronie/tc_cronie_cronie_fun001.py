# -*- encoding: utf-8 -*-

"""
@File:      tc_cronie_cronie_fun001.py
@Time:      2024/06/24 14:21:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_cronie_cronie_fun001.yaml for details

    :avocado: tags=P1,noarch,local,cronie
    """
    PARAM_DIC = {"pkg_name": "cronie"}

    def setUp(self):
       super().setUp(self.PARAM_DIC)
              	  	 
    def test(self):
       self.cmd("systemctl start crond")
       code, result=self.cmd("systemctl status crond")
       self.assertIn("active (running)", result)
       self.cmd("systemctl restart crond")
       code, result=self.cmd("systemctl status crond")
       self.assertIn("active (running)", result)
       self.cmd("systemctl stop crond")
       self.cmd("systemctl status crond > crond.log", ignore_status=True)
       code, result=self.cmd("cat crond.log", ignore_status=True)
       self.assertIn("inactive (dead)", result)
       	 
    def tearDown(self):
       super().tearDown(self.PARAM_DIC)		
       self.cmd(" rm -rf crond.log")	
