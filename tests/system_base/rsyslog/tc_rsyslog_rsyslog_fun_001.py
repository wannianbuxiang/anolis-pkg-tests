# -*- encoding: utf-8 -*-

"""
@File:      tc_rsyslog_rsyslog_fun_001.py
@Time:      2024/7/24 17:34:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_rsyslog_rsyslog_fun_001.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "rsyslog"}
    
    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        cmdline = """cat>/etc/rsyslog.d/test.conf<<EOF
\$EscapeControlCharactersOnReceive off 
\$template test-template,"%timestamp:::date-rfc3339%  %HOSTNAME% %msgid% %msg%\\n"
local5.* /var/log/test;test-template
EOF"""
        self.cmd(cmdline)
        self.cmd("systemctl status rsyslog > rsyslog.log 2>&1",ignore_status=True)
        code, ret = self.cmd("cat rsyslog.log | grep -q 'systemctl daemon-reload'", ignore_status=True)
        if code == 0:
            self.cmd("systemctl daemon-reload")
        self.cmd("systemctl restart rsyslog")
        self.cmd("logger -t local5 -p local5.info 'test001'")
        self.cmd("sleep 10")
        code, result=self.cmd('cat /var/log/test | grep "test001"')
        self.assertIn("test", result)
        
    def tearDown(self):
        self.cmd("rm -rf /var/log/test /etc/rsyslog.d/test.conf rsyslog.log")
        self.cmd("systemctl restart rsyslog")
        super().tearDown(self.PARAM_DIC)
