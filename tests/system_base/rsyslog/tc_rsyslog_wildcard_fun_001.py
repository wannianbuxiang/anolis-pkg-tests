# -*- encoding: utf-8 -*-

"""
@File:      tc_rsyslog_wildcard_fun_001.py
@Time:      2024/7/24 10:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_rsyslog_wildcard_fun_001.yaml for details

    :avocado: tags=P1,noarch,local,rsyslog
    """
    PARAM_DIC = {"pkg_name": "rsyslog"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        
    def test(self):
        self.cmd("echo 'mail.=info  /var/log/test' >/etc/rsyslog.d/test.conf")
        self.cmd("systemctl status rsyslog > rsyslog.log 2>&1",ignore_status=True)
        code, ret = self.cmd("cat rsyslog.log | grep -q 'systemctl daemon-reload'", ignore_status=True)
        if code == 0:
            self.cmd("systemctl daemon-reload")
        self.cmd("systemctl restart rsyslog")
        self.cmd("sleep 5")
        self.cmd('logger -t mail -p mail.info mailinfo-test001')
        self.cmd("sleep 5")
        code, result1=self.cmd("grep 'mail\[' /var/log/test | grep mailinfo-test001")
        self.assertIn("mailinfo-test001", result1)
        self.cmd("echo 'lpr.error,news.info  /var/log/test' >/etc/rsyslog.d/test.conf")
        self.cmd("systemctl restart rsyslog")
        self.cmd("sleep 5")
        self.cmd('logger -t lpr -p lpr.error lprerror-test003')
        self.cmd('logger -t new -p news.info newsinfo-test004')
        self.cmd("sleep 5")
        code, result3=self.cmd("grep -E 'new\[|lpr\[' /var/log/test | grep -E 'newsinfo-test004|lprerror-test003'")
        self.assertIn("lprerror-test003", result3)
        self.assertIn("newsinfo-test004", result3)
        
    def tearDown(self):
        self.cmd("rm -rf /var/log/test /etc/rsyslog.d/test.conf rsyslog.log")
        self.cmd("systemctl restart rsyslog")
        super().tearDown(self.PARAM_DIC)
