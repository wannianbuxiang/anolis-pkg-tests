# -*- encoding: utf-8 -*-

"""
@File:      tc_file_file_fun001.py
@Time:      2024/7/4 14:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_file_file_fun001.yaml for details

    :avocado: tags=P1,noarch,local,file
    """
    PARAM_DIC = {"pkg_name": "file"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        
    def test(self):        	
        code, result = self.cmd("file -v")
        self.assertIn("file", result)
        self.cmd("file --help > file.log", ignore_status=True)
        code, result = self.cmd("cat file.log", ignore_status=True)
        self.assertIn("Usage: file", result)
      
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf file.log")
