# -*- encoding: utf-8 -*-

"""
@File:      tc_file_file_fun002.py
@Time:      2024/7/4 16:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_file_file_fun002.yaml for details

    :avocado: tags=P1,noarch,local,file
    """
    PARAM_DIC = {"pkg_name": "file"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

        cmdline = """cat > file.txt <<EOF  
  
root:x:0:0:root:/root:/usr/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
games:x:12:100:games:/usr/games:/sbin/nologin
ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
nobody:x:65534:65534:Kernel Overflow User:/:/sbin/nologin
dbus:x:81:81:System message bus:/:/sbin/nologin
systemd-coredump:x:999:997:systemd Core Dumper:/:/sbin/nologin

EOF"""
        self.cmd(cmdline)

    def test(self):        	
       code, result=self.cmd("file file.txt")
       self.assertIn("ASCII text", result)
       code, result=self.cmd("file -b file.txt")
       self.assertNotIn("file.txt", result) 
       code, result=self.cmd("file -i file.txt")
       self.assertIn("charset=us-ascii", result) 
      
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf file.txt")
