# -*- encoding: utf-8 -*-

"""
@File:      tc_binutils_command_objcopy.py
@Time:      2024/04/18 16:04:14
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""
import logging

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_binutils_command_objcopy.yaml for details

    :avocado: tags=P1,noarch,local,binutils
    """
    PARAM_DIC = {"pkg_name": "binutils gcc gcc-c++"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

        self.cmd("mkdir /tmp/test")
        test_str = '''#include<stdio.h>
int divide(int x, int y)
{
return x/y;
}
int main()
{
printf("hello world");
int x = 3;
int y = 0;
int div = divide(x, y);
printf("%d / %d = %d", x, y, div);
return 0;
}

'''
        with open("/tmp/test/test.cpp",mode="wt",encoding="utf-8")as f:
            f.write(test_str)

    def test(self):
        self.cmd("g++ -Wl,-Map=/tmp/test/test.map -g /tmp/test/test.cpp -o /tmp/test/testfile")
        self.cmd('ls /tmp/test | grep -E "testfile|test.map"')
        self.cmd('objcopy -O srec /tmp/test/testfile /tmp/test/test.srec')
        self.cmd('head /tmp/test/test.srec | grep "S"')

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/test")

