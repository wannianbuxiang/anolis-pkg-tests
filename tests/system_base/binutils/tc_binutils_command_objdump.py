# -*- encoding: utf-8 -*-

"""
@File:      tc_binutils_command_objdump.py
@Time:      2024/04/18 16:52:46
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""
import logging

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_binutils_command_objdump.yaml for details

    :avocado: tags=P1,noarch,local,binutils
    """
    PARAM_DIC = {"pkg_name": "binutils gcc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/test")
        main_str = '''int main(int argc,char *argv[])
{
hello();
bye();
return 0;
}'''
        hello_str = """void hello(void)
{
printf("hello!");
}
"""
        bye_str = '''void bye(void)
{
printf("good bye!");
}
'''
        with open("/tmp/test/main.c",mode="wt",encoding="utf-8")as f:
            f.write(main_str)
        with open("/tmp/test/hello.c", mode="wt", encoding="utf-8") as f:
            f.write(hello_str)
        with open("/tmp/test/bye.c", mode="wt", encoding="utf-8") as f:
            f.write(bye_str)

    def test(self):
        self.cmd("cd /tmp/test/ && gcc -Wall -c main.c hello.c bye.c")
        self.cmd("ls -al /tmp/test | grep 'main.o\|hello.o\|bye.o'")
        self.cmd('cd /tmp/test && nm main.o hello.o bye.o > nm.txt')
        self.cmd("cd /tmp/test && test -f nm.txt")
        self.cmd("cd /tmp/test && objdump -d hello.o")


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/test")

