# -*- encoding: utf-8 -*-

"""
@File:      tc_binutils_func_addr2line.py
@Time:      2024/04/18 14:21:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_binutils_func_addr2line.yaml for details

    :avocado: tags=P1,noarch,local,binutils
    """
    PARAM_DIC = {"pkg_name": "binutils gcc gcc-c++"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.str = """#include<stdio.h>
int divide(int x, int y)
{
return x/y;
}
int main()
{
printf("hello world");
int x = 3;
int y = 0;
int div = divide(x, y);
printf("%d / %d = %d", x, y, div);
return 0;
}
"""
        with open("/tmp/test.cpp",mode="wt",encoding="utf-8")as f:
            f.write(self.str)
    def test(self):
        self.cmd("addr2line -v|grep addr2line")
        self.cmd("g++ -Wl,-Map=/tmp/test.map -g /tmp/test.cpp -o /tmp/testfile")
        self.cmd('ls /tmp | grep -E "testfile|test.map"')
        code,address = self.cmd("grep divide /tmp/test.map |awk '{print $1}'")
        self.cmd(f"addr2line {address} -e /tmp/testfile -f -C -s |grep cpp")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -f /tmp/test.cpp")
        self.cmd("rm -f /tmp/test.map")
        self.cmd("rm -f /tmp/testfile")
