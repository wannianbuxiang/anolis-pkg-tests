# -*- encoding: utf-8 -*-

"""
@File:      tc_binutils_command_as.py
@Time:      2024/08/30 15:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_binutils_command_as.yaml for details

    :avocado: tags=P1,noarch,local,binutils
    """
    PARAM_DIC = {"pkg_name": "binutils gcc gcc-c++"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > hello.s << eof
        .global _start

        .text
_start:
        # write(1, message, 12)
        mov     \$1, %rax
        mov     \$1, %rdi
        mov     \$message, %rsi
        mov     \$13, %rdx
        syscall

        # exit(0)
        mov     \$60, %rax
        xor     %rdi, %rdi
        syscall
message:
        .ascii  "Hello, world"
eof
"""
        self.cmd(cmdline)

    def test(self):        	
        self.cmd("as hello.s -o hello.o")
        self.cmd("ld hello.o -o hello")
        code,result=self.cmd("./hello")
        self.assertIn("Hello, world",result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf hello*")