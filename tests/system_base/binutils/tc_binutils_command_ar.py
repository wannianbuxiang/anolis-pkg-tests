# -*- encoding: utf-8 -*-

"""
@File:      tc_binutils_command_ar.py
@Time:      2024/04/18 15:06:12
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""
import logging

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_binutils_command_ar.yaml for details

    :avocado: tags=P1,noarch,local,binutils
    """
    PARAM_DIC = {"pkg_name": "binutils gcc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("touch file1.c file2.c file3.c")

    def test(self):
        self.cmd("gcc -Wall -c file1.c file2.c file3.c")
        self.cmd('ls | grep -E "file[1-3]{1}.o" >test-binutils.txt')
        _,result = self.cmd('cat test-binutils.txt')
        self.cmd("ar rv libNAME.a file1.o file2.o file3.o")


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf file* libNAME.a")
        self.cmd("rm -rf test-binutils.txt")

