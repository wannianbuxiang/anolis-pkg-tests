# -*- encoding: utf-8 -*-

"""
@File:      tc_jq_jq_fun_001.py
@Time:      2024/11/11 16:51:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_jq_jq_fun_001.yaml for details

    :avocado: tags=P1,noarch,local,jq
    """
    PARAM_DIC = {"pkg_name": "jq"}

    def setUp(self):
       super().setUp(self.PARAM_DIC)
       cmdline = """cat > data.json << EOF
{
  "name": "Alice",
  "age": 30,
  "city": "Wonderland"
}
EOF"""
       self.cmd(cmdline)

    def test(self):
       code, result = self.cmd("jq '.name' data.json")
       self.assertIn("Alice", result)
       code, result = self.cmd("jq '.age' data.json")
       self.assertIn("30", result)
       code, result = self.cmd("jq '.city' data.json")
       self.assertIn("Wonderland", result)
       	 
    def tearDown(self):
       super().tearDown(self.PARAM_DIC)		
       self.cmd("rm -rf data.json")	
