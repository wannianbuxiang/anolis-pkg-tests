# -*- encoding: utf-8 -*-

"""
@File:      tc_groff_groff_fun001.py
@Time:      2024/11/25 17:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_groff_groff_fun001.yaml for details

    :avocado: tags=P1,noarch,local,groff
    """
    PARAM_DIC = {"pkg_name": "groff groff-base ghostscript "}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > test.groff << EOF
.TL
My First Groff Document
.AU
Your Name
.DT
2024-11-06
.LP
This is a simple document created using groff.
You can format text in various ways.

.B
This text is bold.

.I
This text is italic.

.R
This text is right-aligned.
EOF"""
        self.cmd(cmdline)

    def test(self):        	
        self.cmd("groff -Tps test.groff > test.ps")
        code,result = self.cmd("ls")
        self.assertIn("test.ps", result)
        self.cmd("gs -dBATCH -dNOPAUSE test.ps")
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf test.groff test.ps")