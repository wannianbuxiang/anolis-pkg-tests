#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_colrm_002.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_colrm_fun_002.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        ret_c, ret_o = self.cmd("echo '123456' > colrm.txt")
        ret_c, ret_o = self.cmd("cat colrm.txt |colrm  3")
        self.assertTrue("12" in ret_o, 'check output error.')

    
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf colrm.txt")
