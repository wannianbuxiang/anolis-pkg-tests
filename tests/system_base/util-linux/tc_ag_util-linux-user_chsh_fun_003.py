#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux-user_chsh_003.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    sunqingwei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux-user_chsh_fun_003.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        # 这里加判断，是由于chsh有的是由util-linux提供，有的系统是由util-linux-user提供
        ret_c, _ = self.cmd("which chsh")
        self.chsh_install = False
        if ret_c != 0:
            self.chsh_install = True
            self.cmd("yum install -y util-linux-user")
        self.cmd("useradd tldr-user")
    
    def test(self):
        ret_c_list, ret_o_list = self.cmd("chsh -l")
        path_list = ret_o_list.split("\n")
        for shell_path in path_list:
            cmdline = "chsh --shell %s tldr-user" % shell_path
            self.cmd(cmdline)
            ret_c_check,ret_o_check = self.cmd("cat /etc/passwd | grep ^tldr-user")
            check_path = ret_o_check.split(":")[-1]
            self.assertTrue(check_path == shell_path, 'Path setting failure')
 
    def tearDown(self):
        if self.chsh_install:
            self.cmd("yum erase -y util-linux-user")
        super().tearDown(self.PARAM_DIC)
        self.cmd("userdel -r tldr-user")
