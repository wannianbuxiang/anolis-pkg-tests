#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_runuser_004.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_runuser_fun_004.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux procps-ng"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("useradd tldr-user")
    
    def test(self):
        ret_c, ret_o = self.cmd("runuser tldr-user -s /bin/sh -c 'echo hello'")
    
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("pgrep -u tldr-user | xargs kill -9", ignore_status=True)
        self.cmd("userdel -r tldr-user")