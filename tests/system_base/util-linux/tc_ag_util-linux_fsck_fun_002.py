#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_fsck_002.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""
import pexpect
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_fsck_fun_002.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        self.cmd("dd if=/dev/zero of=/tmp/fsck_test.img bs=1M count=100")
        self.cmd("mkfs.ext4 /tmp/fsck_test.img")
        fsck = pexpect.spawn("fsck -r /tmp/fsck_test.img")
        ret = fsck.expect(["/tmp/fsck_test.img: clean"], timeout=5)
        self.assertTrue(ret == 0, "expect check")
    
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm /tmp/fsck_test.img")

