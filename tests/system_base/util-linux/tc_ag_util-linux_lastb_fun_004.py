#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_lastb_004.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    meiyou
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_lastb_fun_004.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        self.cmd("lastb --present 00:00")
        self.cmd("lastb --present 2023-01-01 00:00")

        ret_c, ret_o = self.cmd("lastb --present xxxxx", ignore_status=True)
        self.assertTrue("lastb: invalid time value" in ret_o, "Failed to display last logged information")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
