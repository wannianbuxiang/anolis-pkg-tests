#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_namei_002.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_namei_fun_002.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir -p /root/aa/bb/cc")
        self.cmd("mkdir -p /home/aa/bb/cc")

    def test(self):
        ret_c, ret_o = self.cmd("namei --long /root/aa/bb/cc /home/aa/bb/cc")
        self.assertTrue("drwxr" in ret_o, 'check output error.')
        abnormal_situation ={
            "namei --long /root/aa/bb/cc/1.txt":"No such file or directory"
        }
        for abn in abnormal_situation.keys():
            ret_c, ret_o = self.cmd(abn,ignore_status=True)
            self.assertTrue(ret_c is not 0, 'check output error.')
            
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /root/aa/bb/cc")
        self.cmd("rm -rf /home/aa/bb/cc")