#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_nologin_002.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""
import os
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_nologin_fun_002.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux procps-ng"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        # 这里加判断，是由于chsh有的是由util-linux提供，有的系统是由util-linux-user提供
        ret_c, _ = self.cmd("which chsh")
        self.chsh_install = False
        if ret_c != 0:
            self.chsh_install = True
            self.cmd("yum install -y util-linux-user")
        self.cmd("useradd tldr-user")
        self.cmd("chsh -s chsh -s /usr/sbin/nologin tldr-user")

    def test(self):
        self.cmd("echo 'The current user is forbidden to log in' > /etc/nologin.txt")
        cmd_str = os.popen('su -l tldr-user')
        ret_str = cmd_str.read()
        self.assertTrue("forbidden" in ret_str, 'check output error.')

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        if self.chsh_install:
            self.cmd("yum erase -y util-linux-user")
        self.cmd("pgrep -u tldr-user | xargs kill -9", ignore_status=True)
        self.cmd("userdel -r tldr-user")
        self.cmd("rm /etc/nologin.txt")
