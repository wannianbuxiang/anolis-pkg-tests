#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_uuidgen_003.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_uuidgen_fun_003.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        abnormal_situation ={
            "@dns":"www.example.com",
            "@url":"/home",
            "@oid":"iso",
            "@x500":"x.500",

        }
        for  abn in abnormal_situation.keys():
            ret_c, ret_o = self.cmd("uuidgen --sha1 --namespace %s --name %s" % (abn,abnormal_situation[abn]))
    
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
