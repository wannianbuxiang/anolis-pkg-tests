#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux-user_chfn_001.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    sunqingwei 
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux-user_chfn_fun_001.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        # 这里加判断，是由于chfn有的是由util-linux提供，有的系统是由util-linux-user提供
        ret_c, _ = self.cmd("which chfn")
        self.chfn_install = False
        if ret_c != 0:
            self.chfn_install = True
            self.cmd("yum install -y util-linux-user")
        self.cmd("useradd tldr-user",ignore_status=True)
    
    def test(self):
        name_list = ['testuser','TESTUSER','5421014','Anolis-001!','张三']
        for name in name_list:
            ret_c, ret_o = self.cmd(f"chfn -f {name}  tldr-user")
            info_c, info_o = self.cmd("cat /etc/passwd |grep tldr-user")
            info_list = info_o.split(':')[4].split(',')
            self.assertEqual(info_list[0], name)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        if self.chfn_install:
            self.cmd("yum erase -y util-linux-user")
        self.cmd("userdel -r tldr-user")
