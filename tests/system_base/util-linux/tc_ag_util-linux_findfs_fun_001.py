#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_findfs_001.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_findfs_fun_001.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        ret_c,ret_o = self.cmd('systemd-detect-virt',ignore_status=True)
        if ret_c == 1:
            ret_c, ret_o = self.cmd("findfs LABEL=/")
            abnormal_situation ={
                "findfs LABEL=":"unable to resolve",
                "findfs LABEL=xxxxx":" unable to resolve"
            }
            for abn in abnormal_situation.keys():
                ret_c, ret_o = self.cmd(abn,ignore_status=True)
                self.assertTrue(abnormal_situation[abn]  in ret_o, 'check output error.') 
        else:
            ...
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
