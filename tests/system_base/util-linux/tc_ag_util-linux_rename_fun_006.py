#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_rename_006.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    meiyou
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_rename_fun_006.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("touch foo{1..178}")
    
    def test(self):
        self.cmd("rename foo foo00 foo? && rename foo foo0 foo??")
        ret_c, ret_o = self.cmd("ls")
        file_list = ret_o.split()
        for num in range(1,179):
            filename = "foo" + "%03d" %num
            self.assertTrue(filename in file_list, "exist invalid file name %s" % filename)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -f foo???")
