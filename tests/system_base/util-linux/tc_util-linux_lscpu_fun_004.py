# -*- encoding: utf-8 -*-

"""
@File:      tc_util-linux_lscpu_fun_004.py
@Time:      2024/07/19 10:23:53
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_util-linux_lscpu_fun_004.yaml for details

    :avocado: tags=P1,noarch,local,util-linux
    """
    PARAM_DIC = {"pkg_name": "util-linux"}

    def setUp(self):
       super().setUp(self.PARAM_DIC)
              	  	 
    def test(self):
       code1, result=self.cmd("lscpu | grep CPU")
       self.assertIn("CPU op-mode", result)
       code2, result=self.cmd("lscpu -V | grep lscpu")
       self.assertIn("lscpu from", result)
       code3, result=self.cmd("lscpu -e | grep -A 2 CPU")
       self.assertIn("yes", result)
       code4, result=self.cmd("lscpu -e=CPU | grep -A 2 CPU")
       self.assertIn("CPU", result)
       code5, result=self.cmd("lscpu -e=CORE | grep -A 2 CORE")
       self.assertIn("CORE", result)
       code6, result=self.cmd("lscpu -e=NODE | grep -A 2 NODE")
       self.assertIn("NODE", result)
       code7, result=self.cmd("lscpu -e=ADDRESS | grep -A 2 ADDRESS")
       self.assertIn("ADDRESS", result)
       code8, result=self.cmd("lscpu -e=MAXMHZ | grep -A 2 MAXMHZ")
       self.assertIn("MAXMHZ", result)
       code9, result=self.cmd("lscpu -a -e | grep -A 2 CPU")
       self.assertIn("yes", result)
       	 
    def tearDown(self):
       super().tearDown(self.PARAM_DIC)			
