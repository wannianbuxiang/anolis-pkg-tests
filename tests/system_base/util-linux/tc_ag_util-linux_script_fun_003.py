#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_script_003.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""
import pexpect
import time
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_script_fun_002.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        child = pexpect.spawn('script hello.txt')
        child.sendline('echo hello')
        child.sendline('exit')
        time.sleep(2)
        child = pexpect.spawn('script -a hello.txt')
        child.sendline('echo world')
        child.sendline('exit')
        time.sleep(2)
        self.cmd("cat hello.txt | grep hello")
        self.cmd("cat hello.txt | grep world")
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        ret_c,ret_o = self.cmd("rm -rf hello.txt")