# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_lslogins_fun007.py
@Time:      2024/8/26 16:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_lslogins_fun007.yaml for details

    :avocado: tags=P1,noarch,local,util-linux
    """
    PARAM_DIC = {"pkg_name": "util-linux"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("useradd test1")
        self.cmd("echo 'test1:deepin12#$'")
    	
    def test(self): 
        self.cmd("lslogins")
        code, result = self.cmd("lslogins -e")
        self.assertIn("USER",result)
        code, result = self.cmd("lslogins -u test1")
        self.assertIn("test1",result)        
        code, result = self.cmd("lslogins -l test1")
        self.assertIn("test1",result)        
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("userdel -rf test1")
