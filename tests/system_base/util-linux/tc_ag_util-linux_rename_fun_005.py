#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_rename_005.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    meiyou
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_rename_fun_005.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.testdir = "/tmp/rename_test"
        self.cmd("mkdir -p %s;cd %s; touch bar-{1..5}.txt; cd -" % (self.testdir, self.testdir))
        
    def test(self):
        self.cmd("cd {}; rename '' 'foo' *.txt; cd -".format(self.testdir))
        ret_c, ret_o = self.cmd("ls {}".format(self.testdir))
        file_list = ret_o.split()
        for file in file_list:
            self.assertEqual(file[:3], 'foo', 'filename does not start with foo')
    
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf {}".format(self.testdir))
