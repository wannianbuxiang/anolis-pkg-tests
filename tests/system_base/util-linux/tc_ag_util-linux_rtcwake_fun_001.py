#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_rtcwake_001.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""

from common.basetest import LocalTest
from common.sysinfo import SysInfo

class Test(LocalTest):
    """
    See tc_ag_util-linux_rtcwake_fun_001.yaml for details

    :avocado: tags=fix,P1,x86_64,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        SysInfo.skip_arch(self, "aarch64")
        ret_c, ret_o = self.cmd("rtcwake -m show -v")
        self.assertTrue('alarm info' in ret_o, 'check output error.')

    
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
