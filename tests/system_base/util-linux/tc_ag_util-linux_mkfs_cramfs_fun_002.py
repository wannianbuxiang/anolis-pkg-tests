#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_mkfs.cramfs_002.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_mkfs.cramfs_fun_002.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        self.cmd("dd if=/dev/zero of=fsck_test.img bs=1M count=100")
        self.cmd("mkdir mkfs_test")
        ret_c, ret_o = self.cmd("mkfs.cramfs -n fortest mkfs_test fsck_test.img")
    
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf fsck_test.img")
        self.cmd("rm -rf mkfs_test")
