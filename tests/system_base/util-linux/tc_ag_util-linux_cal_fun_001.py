#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_cal_001.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_cal_fun_001.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        ret_c, self.ori_time = self.cmd('date "+%Y-%m-%d %H:%M:%S"')

    def test(self):
        ret_c, self.year = self.cmd('date +%Y')
        ret_c, self.month = self.cmd('date +%B')
        ret_c, ret_o = self.cmd("cal |grep %s" % self.year)
        ret_c, ret_o = self.cmd("cal |grep %s" % self.month)

        set_time_char = "July 2022"
        set_time_num = "07/22/2022"
        self.cmd("date -s %s" % set_time_num)
        self.cmd("cal |grep '%s'" % set_time_char)
    
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('date -s "%s"' % self.ori_time)