#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_taskset_003.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""
import os
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_taskset_fun_003.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux procps-ng"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        self.cmd("chcpu -e 1")
        os.system("taskset -c 1 sleep 10 &")
        ret_c, self.pid = self.cmd('ps aux | grep "sleep 10" | grep -v grep| awk "{print \$2}"|head -1')
        ret_c, ret_o = self.cmd("taskset -c -p %s" % self.pid)
        self.assertTrue("list: 1" in ret_o, 'check output error.')
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("kill -9 %s" % self.pid)
