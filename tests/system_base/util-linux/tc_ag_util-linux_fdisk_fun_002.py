#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_fdisk_002.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""
from socket import timeout
from common.basetest import LocalTest
from common.disk import DiskManager

class Test(LocalTest):
    """
    See tc_ag_util-linux_fdisk_fun_002.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    def test(self):
        #挂载并获取系统分区
        self.cmd("dd if=/dev/zero of=/tmp/mytest.img bs=1M count=1024")
        self.cmd("mkfs.ext4 /tmp/mytest.img")
        self.cmd("mkdir /mymnt")
        self.cmd("mount  /tmp/mytest.img  /mymnt")
        ret_c,sys_part = self.cmd("df -h |grep mymnt |awk '{print $1}'")
        DiskManager.create_new_part(self, sys_part)
        ret_c,ret_o = self.cmd("fdisk -l %s |grep %s|tail -1 |awk '{print $1}'" % (sys_part,sys_part))
        self.assertTrue((sys_part in ret_o) and (sys_part != ret_o), 'check output error.')

        #删除分区
        DiskManager.delete_part(self, sys_part)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("umount  /mymnt")
        self.cmd("rm -rf /mymnt /tmp/mytest.img")

