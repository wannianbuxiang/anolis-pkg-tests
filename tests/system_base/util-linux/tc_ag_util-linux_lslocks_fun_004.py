#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_lslocks_004.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""
import os
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_lslocks_fun_004.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        os.system("flock ./test.lock -c 'sleep 5' &")
        ret_c, ret_o = self.cmd("ps -ef |grep 'test.lock' |grep -v grep| awk '{print $2}'")
        ret_c, ret_o = self.cmd("lslocks --pid %s" % ret_o )
    
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf ./test.lock ")