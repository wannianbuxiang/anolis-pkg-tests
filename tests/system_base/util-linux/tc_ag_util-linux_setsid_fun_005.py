#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_setsid_005.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""

from common.basetest import LocalTest
import time

class Test(LocalTest):
    """
    See tc_ag_util-linux_setsid_fun_005.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        self.cmd("setsid --ctty ping -c 4 127.0.0.1 > pingtest.log 2>&1", ignore_status=True)
        time.sleep(5)
        ret_c, ret_o = self.cmd("cat pingtest.log")
        if "failed to set the controlling terminal" in ret_o:
            self.skip("Skip: Failed to set the controlling terminal")  
        elif "4 packets transmitted" in ret_o:
            print("Received: 4 packets transmitted")
        else:
            self.assertFalse(True, f"Unexpected output: {ret_o}")
    
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf pingtest.log")
