#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_cal_003.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_cal_fun_003.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        ret_c, self.ori_time = self.cmd('date "+%Y-%m-%d %H:%M:%S"')
    def test(self):
        set_time_num = "01/22/2022"
        self.cmd("date -s %s" % set_time_num)
        ret_c, ret_o = self.cmd("cal --monday")
        self.assertTrue("January" in ret_o, 'check output error.')
        abnormal_situation ={
            "cal --monday February":"February",
            "cal --monday February1":"failed to parse"
        }
        for  abn in abnormal_situation.keys():
            ret_c, ret_o = self.cmd(abn,ignore_status=True)
            self.assertTrue(abnormal_situation[abn]  in ret_o, 'check output error.')
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('date -s "%s"' % self.ori_time)