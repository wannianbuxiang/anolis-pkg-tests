#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_taskset_002.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""
import os
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_taskset_fun_002.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux procps-ng"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        os.system('sleep 1000 &')
        ret_c, self.pid = self.cmd('ps aux | grep "sleep 1000" | grep -v grep| awk "{print \$2}"|head -1')
    def test(self):
        self.cmd("chcpu -e 1")
        ret_c, ret_o = self.cmd("taskset -p -c 1 %s" % self.pid)
        self.assertTrue("new affinity" in ret_o, 'check output error.')
        ret_c, ret_o = self.cmd("cat /proc/%s/status |grep Cpus_allowed_list|awk  '{print $2}'" % self.pid)
        self.assertTrue("1" in ret_o, 'check output error.')
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("kill -9 %s" % self.pid)
