#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_raw_003.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""
from common.disk import DiskManager
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_raw_fun_003.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        ret_c, _ = self.cmd("which raw", ignore_status=True)
        if ret_c is not 0:
            self.skip("Do not support raw.")
        block_dev = DiskManager.prepare_datadisk(self)
        self.cmd(" raw /dev/raw/raw1 %s" % block_dev)
        ret_c, ret_o = self.cmd("raw -qa")
        self.assertTrue("/dev/raw/raw1"in ret_o, 'check output error.')
    
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        DiskManager.cleanup_datadisk(self,"/tmp/test100.img","/mnt/testpoint")
