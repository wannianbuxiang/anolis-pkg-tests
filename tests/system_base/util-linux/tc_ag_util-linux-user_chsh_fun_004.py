#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux-user_chsh_004.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    sunqingwei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux-user_chsh_fun_004.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        # 这里加判断，是由于chsh有的是由util-linux提供，有的系统是由util-linux-user提供
        ret_c, _ = self.cmd("which chsh")
        self.chsh_install = False
        if ret_c != 0:
            self.chsh_install = True
            self.cmd("yum install -y util-linux-user")
    
    def test(self):
        shell_list = ['/bin/sh','/bin/bash','/usr/bin/sh','/usr/bin/bash']
        ret_c,ret_o = self.cmd("chsh --list-shells")
        for shell_path in shell_list:
            self.assertTrue(shell_path in ret_o, 'Path does not exist')
    
    def tearDown(self):
        if self.chsh_install:
            self.cmd("yum erase -y util-linux-user")
        super().tearDown(self.PARAM_DIC)
