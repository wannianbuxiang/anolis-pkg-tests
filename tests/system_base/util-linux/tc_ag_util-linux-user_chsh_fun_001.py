#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux-user_chsh_001.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhangtaibo
"""

import pexpect
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux-user_chsh_fun_001.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        # 这里加判断，是由于chsh有的是由util-linux提供，有的系统是由util-linux-user提供
        ret_c, _ = self.cmd("which chsh")
        self.chsh_install = False
        if ret_c != 0:
            self.chsh_install = True
            self.cmd("yum install -y util-linux-user")
        ret_c, self.current_shell = self.cmd("cat /etc/passwd | grep -E '^root' | awk -F ':' '{print $NF}'")

    def test(self):
        ret_c, ret_o = self.cmd("chsh -l")
        chsh_path_list = ret_o.split("\n")
        for chsh_path in chsh_path_list:
            output = "Shell changed."
            if chsh_path == self.current_shell:
                output = "Shell not changed."
            
            chsh = pexpect.spawn("chsh")
            ret = chsh.expect(["New shell"], timeout=5)
            if ret == 0:
                chsh.sendline(chsh_path)
            ret = chsh.expect([output])
            self.assertTrue(ret == 0, "change shell failed.")

            ret_c, ret_o = self.cmd("cat /etc/passwd | grep -E '^root' | awk -F ':' '{print $NF}'")
            self.assertTrue(chsh_path == ret_o, "change shell failed.")
            self.current_shell = chsh_path
    
    def tearDown(self):
        self.cmd("chsh -s %s" % self.current_shell)
        if self.chsh_install:
            self.cmd("yum erase -y util-linux-user")
        super().tearDown(self.PARAM_DIC)
