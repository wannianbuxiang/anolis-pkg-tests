# -*- encoding: utf-8 -*-

"""
@File:      tc_recode_recode_fun001.py
@Time:      2024/9/11 16:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_recode_recode_fun001.yaml for details

    :avocado: tags=P1,noarch,local,recode
    """
    PARAM_DIC = {"pkg_name": "recode"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > page.txt << eof
  "Hello, world"
eof
"""
        self.cmd(cmdline)

    def test(self):        	
        self.cmd("recode -l")
        self.cmd("recode ISO-8859-1..UTF-8 < page.txt > new_page.txt")
        code,result=self.cmd("cat new_page.txt")
        self.assertIn("Hello, world",result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf page.txt")
        self.cmd("rm -rf new_page.txt")
