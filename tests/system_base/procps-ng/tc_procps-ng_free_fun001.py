# -*- encoding: utf-8 -*-

"""
@File:      tc_procps-ng_free_fun001.py
@Time:      2024/09/25 17:21:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_procps-ng_free_fun001.yaml for details

    :avocado: tags=P1,noarch,local,procps-ng
    """
    PARAM_DIC = {"pkg_name": "procps-ng"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)      

    def test(self):     
        self.cmd('free |grep "Mem:"')
        self.cmd('free |grep "Swap:"')
        self.cmd('free -l |grep "Low:"')
        self.cmd('free -l |grep "High:"')
        code, result = self.cmd("free -h | grep Mem | awk '{print $2}'")
        self.assertIn("G", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)

