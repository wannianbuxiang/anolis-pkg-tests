# -*- encoding: utf-8 -*-

"""
@File:      tc_gawk_gawk_fun002.py
@Time:      2024/7/4 16:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_gawk_gawk_fun002.yaml for details

    :avocado: tags=P1,noarch,local,gawk
    """
    PARAM_DIC = {"pkg_name": "gawk"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

        cmdline = """cat > gawk.awk <<EOF    
BEGIN{
    str="hello world!"
    num= 1 + 1
    printf "str=%s,num=%d",str,num 
}
EOF"""
        self.cmd(cmdline)

    def test(self):        	
       code, result=self.cmd("gawk -f gawk.awk")
       self.assertIn("str=hello world!,num=2", result)
      
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf gawk.awk")
