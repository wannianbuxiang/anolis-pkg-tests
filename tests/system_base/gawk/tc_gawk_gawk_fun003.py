# -*- encoding: utf-8 -*-

"""
@File:      tc_gawk_gawk_fun003.py
@Time:      2024/7/12 16:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_gawk_gawk_fun003.yaml for details

    :avocado: tags=P1,noarch,local,gawk
    """
    PARAM_DIC = {"pkg_name": "gawk"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

        cmdline = """cat > gawk.awk <<EOF  
  BEGIN{
    if(num == 0){
        print "0"
    }else if (num > 0){
        print num " > 0"
    }else{
        print num " < 0"
    }
}
EOF"""
        self.cmd(cmdline)

    def test(self):        	
       code, result=self.cmd("gawk -v num=10 -f gawk.awk")
       self.assertIn("10 > 0", result)
       code, result=self.cmd("gawk -v num=-1 -f gawk.awk")
       self.assertIn("-1 < 0", result) 
       code, result=self.cmd("gawk -v num=0 -f gawk.awk")
       self.assertIn("0", result) 
      
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf gawk.awk")
