# -*- encoding: utf-8 -*-

"""
@File:      tc_chrpath_chrpath_fun001.py
@Time:      2024/8/26 10:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_chrpath_chrpath_fun001.yaml for details

    :avocado: tags=P1,noarch,local,chrpath
    """
    PARAM_DIC = {"pkg_name": "chrpath gcc glibc-common"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat>test.c<<EOF
#include<stdio.h>

int main(int argc,char** argv)
{
        printf("hello world\\n");
        return 0;
}
EOF"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o test test.c -Wl,-rpath=/usr/lib64")
        code, result1=self.cmd("chrpath -l test | grep 'RPATH=/usr/lib64'")
        self.assertIn("RPATH=/usr/lib64", result1)

        self.cmd("chrpath -r '/usr/lib' test")

        code, result2=self.cmd("chrpath -l test | grep 'RPATH=/usr/lib'")
        self.assertIn("RPATH=/usr/lib", result2)

        code, result3=self.cmd("chrpath -c test | grep 'RUNPATH=/usr/lib'")
        self.assertIn("RUNPATH=/usr/lib", result3)

        code, result4=self.cmd("ldd test | grep 'lib64'")
        self.assertIn("lib64", result4)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf test.c test")

