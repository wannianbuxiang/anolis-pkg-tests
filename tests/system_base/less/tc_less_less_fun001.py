# -*- encoding: utf-8 -*-

"""
@File:      tc_less_less_fun001.py
@Time:      2024/04/18 14:21:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest
import pexpect

class Test(LocalTest):
    """
    See tc_less_less_fun001.yaml for details

    :avocado: tags=P1,noarch,local,less
    """
    PARAM_DIC = {"pkg_name": "less"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline='''cat > testlog <<EOF
TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256 (secp256r1) - A
TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256 (secp256r1) - A
TLS_ECDHE_RSA_WITH_RC4_128_SHA (secp256r1) - C
TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA (secp256r1) - A
TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA (secp256r1) - A
TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384 (secp256r1) - A
TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384 (secp256r1) - A
TLS_RSA_WITH_AES_128_GCM_SHA256 (rsa 2048) - A
TLS_RSA_WITH_AES_256_GCM_SHA384 (rsa 2048) - A
TLS_RSA_WITH_AES_128_CBC_SHA256 (rsa 2048) - A
TLS_RSA_WITH_AES_128_CBC_SHA (rsa 2048) - A
TLS_RSA_WITH_AES_256_CBC_SHA256 (rsa 2048) - A
TLS_RSA_WITH_AES_256_CBC_SHA (rsa 2048) - A
TLS_RSA_WITH_RC4_128_SHA (rsa 2048) - C
EOF
'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("cat testlog")
        less_te = pexpect.spawn('less -N testlog')
        less_te.setecho(False)
        # 检查从1到10的行号
        for i in range(1, 11):
            less_te.expect(str(i))
        # 退出less
        less_te.sendline('q')
        less_te.expect(pexpect.EOF)
        
        code,result=self.cmd("command -v less | grep /usr/bin/less")
        self.assertIn("/usr/bin/less",result)

        code,result=self.cmd("less --help")
        self.assertIn("pattern",result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf testlog")	
