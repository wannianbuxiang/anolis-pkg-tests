#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_ag_shadow_umask_func_001.py
@Time:      2022/8/31 10:06:45
@Author:    yangshunfeng.ysf
@Modify:    liuyaqing
@Version:   1.0
@Contact:   lyq01395646@alibaba-inc.com
@License:   Mulan PSL v2
"""
from common.basetest import RemoteTest

class UmaskTest(RemoteTest):
    """
    See tc_ag_shadow_umask_func_001.yaml for details
    :avocado: tags=P1,fix,noarch,remote,vhd_img,baseos_container,baseos_container_default
    """
    def setUp(self):
        super().setUp()
        
    def test_user_permit(self):
        if self.container_id:
            self.skip_non_root_test()
            ret_c,output = self.cmd('umask',container_flag=1)
            self.assertEqual(output,'0022',"Default configuration is wrong!")
            self.cmd('rm -rf README.txt',ignore_status=True,container_flag=1)
            self.cmd('touch README.txt',container_flag=1)
            ret_c,output1 = self.cmd('stat -c %a README.txt',container_flag=1)
            self.assertEqual(output1,'644',"Default permission exception for new file!")
            self.cmd('chmod 744 README.txt',container_flag=1)
            ret_c,output2 = self.cmd('stat -c %a README.txt',container_flag=1)
            self.assertEqual(output2,'744',"Permission exception of non file owner!")
        else:
            self.cmd('rm -rf README.txt',ignore_status=True)
            self.cmd('touch README.txt')
            ret_c,ret_o = self.cmd('cat /etc/image-id')
            if 'dengbao' in ret_o:
                ret_c,output = self.cmd('umask')
                self.assertEqual(output,'0027',"Default configuration is wrong!")
                ret_c,output1 = self.cmd('stat -c %a README.txt')
                self.assertEqual(output1,'640',"Default permission exception for new file!")
            elif 'dengbao' not in ret_o:
                ret_c,output = self.cmd('umask')
                self.assertEqual(output,'0022',"Default configuration is wrong!")
                ret_c,output1 = self.cmd('stat -c %a README.txt')
                self.assertEqual(output1,'644',"Default permission exception for new file!")
            self.cmd('chmod 744 README.txt')
            ret_c,output2 = self.cmd('stat -c %a README.txt')
            self.assertEqual(output2,'744',"Permission exception of non file owner!")
            self.cmd('rm -f README.txt')
    def tearDown(self):
        super().tearDown()