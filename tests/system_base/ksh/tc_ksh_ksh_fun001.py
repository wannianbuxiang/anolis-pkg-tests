# -*- encoding: utf-8 -*-

"""
@File:      tc_ksh_ksh_fun001.py
@Time:      2024/11/08 17:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ksh_ksh_fun001.yaml for details

    :avocado: tags=P1,noarch,local,ksh
    """
    PARAM_DIC = {"pkg_name": "ksh"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > testfile << EOF
#!/bin/ksh

function greet {
    echo "Hello, \$1!"
}

greet "Bob"

EOF"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("chmod +x ./testfile")
        self.cmd("./testfile > tmp.log")           	
        code,result=self.cmd("cat tmp.log")
        self.assertIn("Hello, Bob!", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf tmp.log testfile")