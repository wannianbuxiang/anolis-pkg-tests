#! /usr/bin/env python3
# -*- encoding: utf-8 -*-

"""
@File:      tc_dbus_services_dbus_x11_001.py
@Time:      2024/06/06 12:45:21
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_dbus_services_dbus_x11_001.yaml for details

    :avocado: tags=P1,noarch,local,dbus-x11
    """
    PARAM_DIC = {"pkg_name": "dbus-x11 dbus-tools"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        self.cmd('dbus-uuidgen --help 2>&1 | grep "dbus-uuidgen"')
        self.cmd('dbus-uuidgen')
        self.cmd('dbus-uuidgen --ensure=testuuid')
        self.cmd('echo "b1f8d2b3dca133f611cecb5c64b643e4" >testuuid2')
        self.cmd('dbus-uuidgen --get=testuuid2 2>&1 | grep "b1f8d2b3dca133f611cecb5c64b643e4"')
        self.cmd('dbus-launch -h 2>&1 | grep "dbus-launch"')
        self.cmd('dbus-launch --version')
        self.cmd('dbus-launch | grep "guid"')
        self.cmd('dbus-launch --auto-syntax | grep "export"')
        self.cmd('dbus-launch --binary-syntax > dbus_env_vars')
        code,result1=self.cmd("ls")
        self.assertIn("dbus_env_vars", result1)
        self.cmd('dbus-launch --close-stderr')
        self.cmd('dbus-launch --csh-syntax | grep "setenv"')
        self.cmd('dbus-launch --exit-with-session')
        self.cmd('dbus-launch --sh-syntax | grep "export DBUS_SESSION_BUS_ADDRESS"')

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('systemctl enable dbus-broker')
        self.cmd('systemctl restart dbus-broker dbus')
        self.cmd('systemctl restart systemd-logind')
        self.cmd("rm -rf testuuid testuuid2 dbus_env_vars")
        
