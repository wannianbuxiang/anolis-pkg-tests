#! /usr/bin/env python3
# -*- encoding: utf-8 -*-

"""
@File:      tc_dbus_services_dbus_session_001.py
@Time:      2024/06/06 11:21:45
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_dbus_services_dbus_session_001.yaml for details

    :avocado: tags=P1,noarch,local,dbus-daemon
    """
    PARAM_DIC = {"pkg_name": "dbus-daemon"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        self.cmd('dbus-run-session --help 2>&1 | grep "dbus-run-session"')
        self.cmd('dbus-run-session --version')
        self.cmd('dbus-cleanup-sockets --help 2>&1 | grep "dbus-cleanup-sockets"')
        self.cmd('dbus-cleanup-sockets --version')
        self.cmd('dbus-cleanup-sockets /tmp')

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
