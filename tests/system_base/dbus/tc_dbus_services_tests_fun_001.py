#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_dbus_services_tests_fun_001.py
@Time:      2024/03/21 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhangtaibo
"""

from common.basetest import LocalTest
from common.service import ServiceManager

class Test(LocalTest):
    """
    See tc_dbus_services_tests_fun_001.yaml for details

    :avocado: tags=fix,P1,noarch,local,os_fun
    """
    PARAM_DIC = {"pkg_name": "dbus"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        ServiceManager.service_test(self, "dbus")

    def tearDown(self):
        self.cmd('systemctl restart systemd-logind')
        self.cmd('systemctl restart NetworkManager')
        super().tearDown(self.PARAM_DIC)
