# -*- encoding: utf-8 -*-

"""
@File:      tc_cryptsetup_cryptsetup_fun001.py
@Time:      2024/7/24 17:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_cryptsetup_cryptsetup_fun001.yaml for details

    :avocado: tags=P1,noarch,local,cryptsetup
    """
    PARAM_DIC = {"pkg_name": "cryptsetup"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        
    def test(self):        	
        code, result = self.cmd("cryptsetup --version")
        self.assertIn("cryptsetup", result)
        self.cmd("cryptsetup --help > cryptsetup.log", ignore_status=True)
        code, result = self.cmd("cat cryptsetup.log", ignore_status=True)
        self.assertIn("Usage:", result)
      
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf cryptsetup.log")
