# -*- encoding: utf-8 -*-

"""
@File:      tc_cryptsetup_cryptsetup_fun_002.py
@Time:      2024/7/24 17:00:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_cryptsetup_cryptsetup_fun_002.yaml for details

    :avocado: tags=P1,noarch,local,cryptsetup
    """
    PARAM_DIC = {"pkg_name": "cryptsetup"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
       self.cmd("dd if=/dev/zero of=img1 bs=10M count=10")
       self.cmd("losetup /dev/loop5 img1")        	
       self.cmd("mkdir -p /home/encrypted1")
       self.cmd('echo "Huawei12#$" | cryptsetup  luksFormat /dev/loop5')
       self.cmd('echo "Huawei12#$" | cryptsetup luksOpen /dev/loop5 luksdisk')
       self.cmd("mkfs.ext4 /dev/mapper/luksdisk")
       self.cmd("mount /dev/mapper/luksdisk /home/encrypted1")
       self.cmd("dd if=/dev/urandom of=/home/encrypted1/tmp bs=4k count=4096") 
       self.cmd("umount /home/encrypted1")
       self.cmd("cryptsetup luksClose luksdisk")  
       self.cmd("losetup -d /dev/loop5")  

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf img1")
        self.cmd("rm -rf /home/encrypted1/")