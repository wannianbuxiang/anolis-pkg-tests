# -*- encoding: utf-8 -*-

"""
@File:      tc_lsscsi_lsscsi_fun_001.py
@Time:      2023/5/09 17:33:20
@Author:    胡哲源
@Version:   1.0
@Contact:   huzheyuan@uniontech.com
@License:   Mulan PSL v2
@Modify:    huzheyuan
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_lsscsi_lsscsi_fun_001.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "lsscsi"}
    
    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        code, result = self.cmd('lsscsi')
        if result:
            self.assertIn('disk', result)
        code, result = self.cmd('lsscsi -L')
        code, result = self.cmd('lsscsi -s')
        if result:
            self.assertIn('GB', result)
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
