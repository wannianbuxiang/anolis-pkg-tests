# -*- encoding: utf-8 -*-

"""
@File:      tc_dtc_dtc_fun002.py
@Time:      2024/6/6 14:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_dtc_dtc_fun002.yaml for details

    :avocado: tags=P1,noarch,local,dtc
    """
    PARAM_DIC = {"pkg_name": "dtc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > example.dts <<EOF    

/dts-v1/;

/ {

      model = "Example Device";
      compatible = "example,device";

      memory {
              reg = <0x0 0x80000000>;
      };
};
EOF"""
        self.cmd(cmdline)

    def test(self):        	
        self.cmd("dtc -I dts -O dtb -o example.dtb example.dts")
        self.cmd("ls -l example.dtb")
        self.cmd("dtc -I dtb -O dts -o example.dts example.dtb")
        self.cmd("ls -l example.dts")
      
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf  example.dtb example.dts")
