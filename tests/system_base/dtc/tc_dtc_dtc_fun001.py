# -*- encoding: utf-8 -*-

"""
@File:      tc_dtc_dtc_fun001.py
@Time:      2024/6/6 14:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_dtc_dtc_fun001.yaml for details

    :avocado: tags=P1,noarch,local,dtc
    """
    PARAM_DIC = {"pkg_name": "dtc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        
    def test(self):        	
        code, result = self.cmd("dtc -v")
        self.assertIn("DTC", result)
        self.cmd("dtc -h > dtc.log", ignore_status=True)
        code, result = self.cmd("cat dtc.log", ignore_status=True)
        self.assertIn("Usage:", result)
      
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf  dtc.log")
