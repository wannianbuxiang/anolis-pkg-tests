# -*- encoding: utf-8 -*-

"""
@File:      tc_elfutils_elfutils_fun001.py
@Time:      2024/7/30 17:00:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_elfutils_elfutils_fun001.yaml for details

    :avocado: tags=P1,noarch,local,elfutils
    """
    PARAM_DIC = {"pkg_name": "elfutils gcc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

        cmdline = """cat > elfutils.c <<EOF  
  
#include <stdio.h>

void hello_world() {
    printf("Hello, World!");
}

int main() {
    hello_world();
    return 0;
}

EOF"""
        self.cmd(cmdline)

    def test(self):        	
       code, result = self.cmd("gcc -o elfutils elfutils.c")
       code, result = self.cmd("./elfutils")
       self.assertIn("Hello, World!", result)
       code, result = self.cmd("eu-readelf -h elfutils")
       self.assertIn("ELF Header:", result) 
       code, result = self.cmd("eu-readelf -s elfutils")
       self.assertIn("Symbol table", result) 
       code, result = self.cmd("eu-readelf -d elfutils")
       self.assertIn("Dynamic", result)
      
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf elfutils elfutils.c")
