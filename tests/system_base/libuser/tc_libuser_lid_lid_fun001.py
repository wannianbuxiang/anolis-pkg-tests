# -*- encoding: utf-8 -*-

"""
@File:      tc_libuser_lid_lid_fun001.py
@Time:      2024/2/27 15:47:20
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libuser_lid_lid_fun001.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "libuser"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        result = list(self.cmd("cat /etc/passwd| awk -F ':' '{print $1}'", ignore_status=True))[-1]
        if "testuser1" in result:
            self.cmd('luserdel -r testuser1')
        if "testuser2" in result:
            self.cmd('luserdel -r testuser2')
        result = list(self.cmd("cat /etc/group| awk -F ':' '{print $1}'", ignore_status=True))[-1]
        if "testuser1" in result:
            self.cmd('lgroupdel testuser1')

    def test(self):
        code, result = self.cmd('lid --help')
        self.assertIn('lid', result)
        self.cmd('luseradd testuser1')
        self.cmd('luseradd -g testuser1 testuser2')
        code, result = self.cmd('lid -g testuser1')
        self.assertIn('testuser1', result)
        self.assertIn('testuser2', result)
        code, result = self.cmd('lid -n testuser1')
        self.assertIn('testuser1', result)
        self.assertNotIn('testuser2', result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f'luserdel -r testuser2')
        self.cmd(f'luserdel -r testuser1')
