# -*- encoding: utf-8 -*-

"""
@File:      tc_libuser_group_lgroupadd_fun001.py
@Time:      2024/2/22 15:47:20
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libuser_group_lgroupadd_fun001.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "libuser"}
    group_name = 'group_test01'

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        result = list(self.cmd("cat /etc/group| awk -F ':' '{print $1}'", ignore_status=True))[-1]
        if self.group_name in result:
            self.cmd(f'lgroupdel {self.group_name}')

    def test(self):
        code, result = self.cmd('lgroupadd --help')
        self.assertIn('lgroupadd', result)
        self.cmd(f'lgroupadd {self.group_name}')
        code, result = self.cmd('cat /etc/group')
        self.assertIn(f'{self.group_name}', result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f'lgroupdel {self.group_name}')
