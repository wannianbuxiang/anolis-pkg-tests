# -*- encoding: utf-8 -*-

"""
@File:      tc_libuser_lchsh_lchsh_fun001.py
@Time:      2024/3/18 15:34:20
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

import pexpect
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libuser_lchsh_lchsh_fun001.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libuser"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        
    def test(self):
        code, result = self.cmd('lchsh --help')
        self.assertIn('lchsh', result)
        self.cmd('luseradd test_user001')
        code, result = self.cmd('cat /etc/passwd')
        self.assertIn('test_user001', result)

        # check lchsh
        newshell = '/bin/bash_new'
        command = 'lchsh test_user001'
        child = pexpect.spawn(command)
        child.expect('New Shell',timeout=5)
        child.sendline(newshell)
        child.expect(pexpect.EOF)
        code, result = self.cmd('cat /etc/passwd')
        self.assertIn(newshell, result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("userdel -r test_user001")
