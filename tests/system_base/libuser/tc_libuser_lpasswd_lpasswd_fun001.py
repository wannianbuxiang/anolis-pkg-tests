# -*- encoding: utf-8 -*-

"""
@File:      tc_libuser_lpasswd_lpasswd_fun001.py
@Time:      2024/3/18 16:42:20
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

import pexpect
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libuser_lpasswd_lpasswd_fun001.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libuser"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        
    def test(self):
        code, result = self.cmd('lpasswd --help')
        self.assertIn('lpasswd', result)
        self.cmd('luseradd test_user001')
        new_password = 'newpassword'
        command = 'lpasswd test_user001'
        child = pexpect.spawn('/bin/bash', timeout=5)
        child.sendline(command)
        child.expect('New password', timeout=5)
        child.sendline(new_password)
        child.expect('confirm', timeout=5)
        child.sendline(new_password)
        child.expect('Password changed.')
        self.log.info("set passwd soccessed!")
        
        # su 切换用户验证
        self.cmd('luseradd test_user002')
        command = 'su - test_user002'
        child.sendline(command)
        child.expect(['test_user002','\$'], timeout=5)
        child.sendline('whoami')
        child.expect('test_user002', timeout=5)
        self.log.info("root切换普通用户成功!")

        command = 'su - test_user001'
        child.sendline(command)
        child.expect('.*Password:\s*', timeout=10) 
        child.sendline(new_password)
        child.expect(['test_user001','\$ '], timeout=5)
        child.sendline('whoami')
        child.expect('test_user001', timeout=5)
        child.sendline('exit')   # exit test_user001
        child.expect('logout', timeout=5)
        child.sendline('exit')   # exit test_user002
        child.expect('logout', timeout=5)
        self.log.info('密码设置成功生效!')
        child.close()
       
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("luserdel -r test_user001")
        self.cmd("luserdel -r test_user002")
