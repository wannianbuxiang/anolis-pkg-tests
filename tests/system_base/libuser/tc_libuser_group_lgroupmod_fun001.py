# -*- encoding: utf-8 -*-

"""
@File:      tc_libuser_group_lgroupmod_fun001.py
@Time:      2024/2/22 15:47:20
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libuser_group_lgroupmod_fun001.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "libuser"}
    group_name = 'group_test01'

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        result = list(self.cmd("cat /etc/passwd| awk -F ':' '{print $1}'", ignore_status=True))[-1]
        if "test_001" in result:
            self.cmd('luserdel -r test_001')
        result = list(self.cmd("cat /etc/group| awk -F ':' '{print $1}'", ignore_status=True))[-1]
        if self.group_name in result:
            self.cmd(f'lgroupdel {self.group_name}')
        if "group_test02" in result:
            self.cmd('lgroupdel group_test02')

    def test(self):
        code, result = self.cmd('lgroupmod --help')
        self.assertIn('lgroupmod', result)
        self.cmd(f'lgroupadd {self.group_name}')
        self.cmd(f'lgroupmod -g 10001 {self.group_name}')
        code, result = self.cmd('cat /etc/group')
        self.assertIn('10001', result)

        self.cmd(f'lgroupmod -n group_test02 {self.group_name}')
        code, result = self.cmd('cat /etc/group')
        self.assertIn('group_test02', result)
        
        self.cmd(f'luseradd -g group_test02 test_001')
        code, result = self.cmd('cat /etc/passwd', result)
        self.assertIn(f'test_001', result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f'luserdel -r test_001')
        self.cmd(f'lgroupdel group_test02')
