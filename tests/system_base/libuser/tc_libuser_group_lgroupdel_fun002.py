# -*- encoding: utf-8 -*-

"""
@File:      tc_libuser_group_lgroupdel_fun002.py
@Time:      2024/2/22 15:47:20
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libuser_group_lgroupdel_fun002.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libuser"}
    group_name = 'group_test01'

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        result = list(self.cmd("cat /etc/group| awk -F ':' '{print $1}'", ignore_status=True))[-1]
        if self.group_name in result:
            self.cmd(f'lgroupdel {self.group_name}', ignore_status=True)
        result = list(self.cmd("cat /etc/passwd| awk -F ':' '{print $1}'", ignore_status=True))[-1]
        if "testuser_001" in result:
            self.cmd(f'luserdel -r testuser_001', ignore_status=True)

    def test(self):
        code, result = self.cmd('lgroupdel --help')
        self.assertIn('lgroupdel', result)
        self.cmd(f'lgroupadd {self.group_name}')
        self.cmd(f'luseradd -g {self.group_name} testuser_001')
        code, result = self.cmd('cat /etc/passwd')
        self.assertIn('testuser_001', result)

        # try to remove the primary group of user
        # https://bugzilla.openanolis.cn/show_bug.cgi?id=8566
        # https://bugzilla.openanolis.cn/show_bug.cgi?id=8445
        # self.cmd(f'lgroupdel {self.group_name}')
        # code, result = self.cmd('cat /etc/group')
        # self.assertIn(f'{self.group_name}', result)
        # code, result = self.cmd('ls /home/testuser_001')
        # self.assertTrue(code != 0, "用户主目录资源残留")

        self.cmd(f'lgroupdel {self.group_name}')
        code, result = self.cmd('cat /etc/group')
        self.assertNotIn(f'{self.group_name}', result)
        code, result = self.cmd('cat /etc/passwd')
        self.assertIn('testuser_001', result)
    
        self.cmd('luserdel -r testuser_001', ignore_status=True)
        code, result = self.cmd('cat /etc/passwd')
        self.assertNotIn('testuser_001', result)
        self.cmd('rm -rf /home/testuser_001 /var/mail/testuser_001', ignore_status=True)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        