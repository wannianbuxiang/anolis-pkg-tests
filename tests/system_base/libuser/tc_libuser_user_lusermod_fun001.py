# -*- encoding: utf-8 -*-

"""
@File:      tc_libuser_user_lusermod_fun001.py
@Time:      2024/2/22 15:47:20
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libuser_user_lusermod_fun001.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "libuser"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        result = list(self.cmd("cat /etc/passwd| awk -F ':' '{print $1}'", ignore_status=True))[-1]
        if "testuser1" in result or "testuser2" in result:
            if "testuser1" in result:
                self.cmd('luserdel -r testuser1')
            if "testuser2" in result:
                self.cmd('luserdel -r testuser2')
        result = list(self.cmd("cat /etc/group| awk -F ':' '{print $1}'", ignore_status=True))[-1]
        if "testuser1" in result:
            self.cmd('lgroupdel testuser1')

    def test(self):
        code, result = self.cmd('lusermod --help')
        self.assertIn('lusermod', result)
        self.cmd('luseradd testuser1')
        self.cmd('cat /etc/passwd')
        self.cmd('lusermod -l testuser2 testuser1')
        code, result = self.cmd('cat /etc/passwd')
        self.assertIn('testuser2', result)
        self.cmd('lusermod -u 10001 testuser2')
        code, result = self.cmd('lid -g testuser1')
        self.assertIn('10001', result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f'luserdel -r testuser2')
        self.cmd(f'lgroupdel testuser1')
        
