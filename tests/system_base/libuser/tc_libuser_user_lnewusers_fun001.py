# -*- encoding: utf-8 -*-

"""
@File:      tc_libuser_user_lnewusers_fun001.py
@Time:      2024/2/27 10:20:20
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libuser_user_lnewusers_fun001.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "libuser"}
    
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        result = list(self.cmd("cat /etc/passwd| awk -F ':' '{print $1}'", ignore_status=True))[-1]
        if "testuser1" in result:
            self.cmd('luserdel -r testuser1')
        if "testuser2" in result:
            self.cmd('luserdel -r testuser2')
        cmdline = '''cat >newusers.txt <<"EOF"
testuser1:Test1234:10001:10001::/home/testuser1:/bin/bash
testuser2:Test1234:10002:10002::/home/testuser2:/bin/bash
EOF'''
        self.cmd(cmdline)

    def test(self):
        code, result = self.cmd('lnewusers --help')
        self.assertIn('lnewusers', result)
        self.cmd('lnewusers -f newusers.txt')
        code, result = self.cmd('cat /etc/passwd')
        self.assertIn('testuser1', result)
        self.assertIn('testuser2', result)
        self.cmd('ls -ld /home/testuser1')
        self.cmd('ls -ld /home/testuser2')

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('luserdel -r testuser1')
        self.cmd('luserdel -r testuser2')
        self.cmd('rm -rf newusers.txt')

