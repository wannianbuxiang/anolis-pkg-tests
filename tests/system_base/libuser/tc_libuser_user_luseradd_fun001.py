# -*- encoding: utf-8 -*-

"""
@File:      tc_libuser_user_luseradd_fun001.py
@Time:      2024/2/22 15:47:20
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libuser_user_luseradd_fun001.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "libuser"}
    username = 'testuser1'

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        result = list(self.cmd("cat /etc/passwd| awk -F ':' '{print $1}'", ignore_status=True))[-1]
        if self.username in result:
            self.cmd(f'luserdel -r {self.username}')

    def test(self):
        code, result = self.cmd('luseradd --help')
        self.assertIn('luseradd', result)
        self.cmd(f'luseradd {self.username}')
        code, result = self.cmd('cat /etc/passwd')
        self.assertIn(f'{self.username}', result)
        self.cmd(f'ls -ld /home/{self.username}')

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f'luserdel -r {self.username}')
