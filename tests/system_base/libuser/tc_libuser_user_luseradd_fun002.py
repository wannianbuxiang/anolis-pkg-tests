# -*- encoding: utf-8 -*-

"""
@File:      tc_libuser_user_luseradd_fun002.py
@Time:      2024/2/22 15:47:20
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libuser_user_luseradd_fun002.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libuser"}
    username = 'testuser1'

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        result = list(self.cmd("cat /etc/passwd| awk -F ':' '{print $1}'", ignore_status=True))[-1]
        if self.username in result:
            self.cmd(f'luserdel -r {self.username}')

    def test(self):
        code, result = self.cmd('luseradd --help')
        self.assertIn('luseradd', result)
        self.cmd(f'luseradd -u 10001 -d /tmp/{self.username} {self.username}')
        self.cmd(f'ls -ld /tmp/{self.username}')
        code, result = self.cmd(f'lid -g {self.username}')
        self.assertIn('10001', result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f'luserdel -r {self.username}')
