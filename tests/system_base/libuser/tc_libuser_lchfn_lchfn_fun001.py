# -*- encoding: utf-8 -*-

"""
@File:      tc_libuser_lchfn_lchfn_fun001.py
@Time:      2024/3/18 15:34:20
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

import pexpect
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libuser_lchfn_lchfn_fun001.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libuser"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        
    def test(self):
        code, result = self.cmd("lchfn --help")
        self.assertIn("lchfn", result)
        self.cmd("luseradd test_user001")
        code, result = self.cmd('cat /etc/passwd')
        self.assertIn("test_user001", result)

        # check lchfn
        command = "lchfn test_user001"
        child = pexpect.spawn(command)
        child.expect('Full Name',timeout=2)
        child.sendline('new_test_user001\n')
        child.expect('Office',timeout=2)
        child.sendline('')
        child.expect('Home Phone',timeout=2)
        child.sendline('')
        child.expect(pexpect.EOF)
        code, result = self.cmd('cat /etc/passwd')
        self.assertIn("new_test_user001", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("userdel -r test_user001")
