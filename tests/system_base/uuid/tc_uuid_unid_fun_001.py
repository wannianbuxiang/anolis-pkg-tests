#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

"""
@File:      tc_uuid_uuid_fun_001.py
@Time:      2024/07/16 14:07:20
@Author:    wangge
@Version:   1.0
@Contact:   YS.wangge@h3c.com
@License:   Mulan PSL v2
@Modify:    wangge
"""
import subprocess
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_uuid_uuid_fun_001.yaml for details

    :avocado: tags=P0,noarch,local
    """
    PARAM_DIC = {"pkg_name": "uuid"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        

    def test(self):
        code, result = self.cmd('uuid | grep "-"')
        self.assertEquals(0, code)
        self.assertIn("-", result)
        
        code, result = self.cmd('uuid -v4 | grep "-"')
        self.assertEquals(0, code)
        self.assertIn("-", result)

        code, result = self.cmd('uuid -v3 ns:URL http://www.baidu.com | grep "-"')
        self.assertEquals(0, code)
        self.assertIn("-", result)

        code, result = self.cmd('uuid -v5 ns:URL http://www.baidu.com | grep "-"')
        self.assertEquals(0, code)
        self.assertIn("-", result)

        try:
            result = subprocess.run(['uuid'], capture_output=True, text=True, check=True)
            uuid_str = result.stdout.strip()
            print("uuid_str",uuid_str)
            mac_id = uuid_str.split('-')[4]
            print(f"MAC ID: {mac_id}")
        except subprocess.CalledProcessError as e:
            print(f"命令执行失败: {e}")
        except IndexError as ie:
            print(f"解析 UUID 失败: {ie}")
        code, result = self.cmd('uuid -m')
        self.assertEquals(0, code)
        self.assertNotIn(mac_id, result)

        code, result = self.cmd('uuid -n 10 | wc -l | grep 10')
        self.assertEquals(0, code)

        code, result = self.cmd('uuid -n 10 -1')
        self.assertEquals(0, code)

        code, result = self.cmd('uuid -F STR | grep "-"')
        self.assertEquals(0, code)
        self.assertIn("-", result)

        code, result = self.cmd('uuid -F SIV')
        self.assertEquals(0, code)
        self.assertNotIn("-", result)

        self.cmd('uuid -o uuid_file')
        code, result = self.cmd('grep "-" uuid_file')
        self.assertEquals(0, code)

        code, id =self.cmd("cat uuid_file")
        code, result = self.cmd(f"uuid -d {id}")
        self.assertEquals(0, code)


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf uuid_file")
        
    

 