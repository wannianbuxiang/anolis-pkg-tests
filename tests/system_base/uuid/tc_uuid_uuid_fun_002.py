#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

"""
@File:      tc_uuid_uuid_fun_002.py
@Time:      2024/07/16 14:07:20
@Author:    wangge
@Version:   1.0
@Contact:   YS.wangge@h3c.com
@License:   Mulan PSL v2
@Modify:    wangge
"""
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_uuid_uuid_fun_002.yaml for details

    :avocado: tags=P0,noarch,local
    """
    PARAM_DIC = {"pkg_name": "uuid"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("touch 1.txt")

    def test(self):
        code, _ = self.cmd('uuid')
        self.assertEquals(0, code, "Err:uuid")
        
        code, _ = self.cmd('uuid -v1')
        self.assertEquals(0, code, "Err:uuid -v1")

        code, _ = self.cmd('uuid -v3 ns:URL https://kernel.org')
        self.assertEquals(0, code,"Err:uuid -v5 ns:URL https://kernel.org")
    
        code, _ = self.cmd('uuid -v4')
        self.assertEquals(0, code,"Err:uuid -v4")

        code, _ = self.cmd('uuid -v5 ns:URL https://kernel.org')
        self.assertEquals(0, code,"Err:uuid -v5 ns:URL https://kernel.org")
        
        code, _ = self.cmd('uuid -m')
        self.assertEquals(0, code, "Err:uuid -m")

        code, _ = self.cmd(' uuid -n2')
        self.assertEquals(0, code, "Err:uuid -n2")

        code, _ = self.cmd('uuid -n3 -1')
        self.assertEquals(0, code, "Err:uuid -n3 -1")

        code, _ = self.cmd('uuid -F str')
        self.assertEquals(0, code, "Err:uuid -F str")

        code, _ = self.cmd('uuid -F siv')
        self.assertEquals(0, code, "Err:uuid -F siv")

        code, _ = self.cmd('uuid -o 1.txt')
        self.assertEquals(0, code, "Err:uuid command failed")
        code, wc_output = self.cmd('wc -m 1.txt')
        self.assertEquals(0, code, "Err:wc -m command failed")
        self.assertIn("37", wc_output, "Err:uuid -o 1.txt output does not contain 37 characters")
       
        code, test_num =self.cmd("uuid")
        self.assertEquals(0, code, "Err:uuid command failed to generate UUID")
        code,result = self.cmd(f"uuid -d {test_num}")
        self.assertEquals(0, code, "Err:uuid -d output does not contain the expected UUID")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf 1.txt")
        
    

 