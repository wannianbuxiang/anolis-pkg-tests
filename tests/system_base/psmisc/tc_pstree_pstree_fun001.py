# -*- encoding: utf-8 -*-

"""
@File:      tc_pstree_pstree_fun001.py
@Time:      2024/8/28 16:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_pstree_pstree_fun001.yaml for details

    :avocado: tags=P1,noarch,local,psmisc
    """
    PARAM_DIC = {"pkg_name": "psmisc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):

       self.cmd("pstree -T")	
       code, result = self.cmd("pstree -a")
       self.assertIn("systemd", result)
       code, result = self.cmd("pstree -A")
       self.assertIn("systemd-+-", result)  
       code, result = self.cmd("pstree -p")
       self.assertIn("systemd(1)", result)  
     
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)

