# -*- encoding: utf-8 -*-

"""
@File:      tc_numactl_numactl_fun001.py
@Time:      2024/8/19 17:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_numactl_numactl_fun001.yaml for details

    :avocado: tags=P1,noarch,local,numactl
    """
    PARAM_DIC = {"pkg_name": "numactl"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        code, result1=self.cmd("numactl -s | grep policy")
        self.assertIn("policy", result1)
        code, result2=self.cmd("numactl -H | grep 'available: [0-9] nodes'")
        self.assertIn("nodes", result2)
        self.cmd("numactl --hardware")
        self.cmd("numactl --show")
        code, result3=self.cmd("numastat | grep -E 'numa_hit|numa_miss|numa_foreign|interleave_hit|local_node'")
        self.assertIn("numa_hit", result3)
        self.assertIn("numa_miss", result3)
        self.assertIn("numa_foreign", result3)
        self.assertIn("interleave_hit", result3)
        self.assertIn("local_node", result3)
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
