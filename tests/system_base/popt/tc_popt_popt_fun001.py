# -*- encoding: utf-8 -*-

"""
@File:      tc_popt_popt_fun001.py
@Time:      2024/9/14 16:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_popt_popt_fun001.yaml for details

    :avocado: tags=P1,noarch,local,popt
    """
    PARAM_DIC = {"pkg_name": "popt popt-devel gcc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > popt.c << EOF
#include <popt.h>
#include <stdio.h>
#include <stdlib.h>

void usage (poptContext optCon , int exitcode , char *error , char *addl ) {
poptPrintUsage (optCon , stderr , 0 ) ;
if (error ) 
fprintf (stderr , "%s: %s" , error , addl ) ;
exit (exitcode ) ;
}

int main ( int argc , char *argv [ ] ) {
// used for argument parsing
int c;

// used for tracking options  
int i = 0;

// used in argument parsing to set speed  
int speed = 0;
int raw = 0; 
int j;
char buf [BUFSIZ + 1 ];
const char *portname;
poptContext optCon; 
struct poptOption optionsTable [ ] = {

{ "bps" , 'b' , POPT_ARG_INT , &speed , 0,
"signaling rate in bits-per-second" , "BPS" },
{ "crnl" , 'c' , 0 , 0 , 'c',
"expand cr characters to cr/lf sequences" , NULL } ,
{ "hwflow" , 'h' , 0 , 0 , 'h',
"use hardware (RTS/CTS) flow control" , NULL } ,
{ "noflow" , 'n' , 0 , 0 , 'n',
"use no flow control" , NULL },
{ "raw" , 'r' , 0 , &raw , 0,
"don't perform any character conversions" , NULL },
{ "swflow" , 's' , 0 , 0 , 's',
"use software (XON/XOF) flow control" , NULL },
POPT_AUTOHELP
{ NULL , 0 , 0 , NULL , 0 }
};

optCon = poptGetContext (NULL , argc , ( const char ** )argv , optionsTable , 0 );
poptSetOtherOptionHelp (optCon , "[OPTIONS]* <port>" );

if (argc < 2 ) {
poptPrintUsage (optCon , stderr , 0 ) ;
exit ( 1 );
}

// Now do options processing, get portname 
while ( (c = poptGetNextOpt (optCon ) ) >= 0 ) {
switch (c ) {

case 'c':
buf [i ++ ] = 'c';
break ;
case 'h' :
buf [i ++ ] = 'h';
break;
case 's' :
buf [i ++ ] = 's';
break ;
case 'n' :
buf [i ++ ] = 'n';
break;
}
}
portname = poptGetArg (optCon ) ;
if ( (portname == NULL ) || ! (poptPeekArg (optCon ) == NULL ) )
usage (optCon , 1 , "Specify a single port" , ".e.g., /dev/cua0" );

if (c < - 1 ) {

fprintf 
(stderr , "%s: %s" ,poptBadOption (optCon , POPT_BADOPTION_NOALIAS ),
poptStrerror (c ) );
return 1;
}

printf 
( "Options chosen: " );
for (j = 0 ; j < i ; j ++ )
printf ( "-%c " , buf [j ] );
if (raw ) printf ( "-r " );
if (speed ) printf ( "-b %d " , speed );
printf 
( "Portname chosen: %s" , portname );

poptFreeContext (optCon );
exit ( 0 ) ;
}
EOF"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o popt popt.c -lpopt")	
        self.cmd("./popt --help > help.log")
        code, result=self.cmd("cat help.log")
        self.assertIn("Usage:", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf popt.c popt help.log")