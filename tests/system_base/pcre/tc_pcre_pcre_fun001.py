# -*- encoding: utf-8 -*-

"""
@File:      tc_pcre_pcre_fun001.py
@Time:      2024/8/9 16:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_pcre_pcre_fun001.yaml for details

    :avocado: tags=P1,noarch,local,pcre
    """
    PARAM_DIC = {"pkg_name": "pcre pcre-devel gcc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > pcre.c << EOF
#include <pcre.h>
#include <stdio.h>
int main()
{
        printf("aaa");
        return 0;
}
EOF"""
        self.cmd(cmdline)

    def test(self):        	
        code,result = self.cmd("gcc -o pcre pcre.c -lpcre")
        code, result = self.cmd("./pcre")
        self.assertIn("aaa",result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf pcre pcre.c")