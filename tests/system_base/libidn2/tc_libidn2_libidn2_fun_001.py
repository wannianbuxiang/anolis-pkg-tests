#! /usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File:      tc_libidn2_libidn2_fun001.py
@Time:      2024年02月29日16:06:08
@Author:    Liujiang <wb-lj931787@alibaba-inc.com>
@License:   Mulan PSL v2
@Modified:  Liujiang <wb-lj931787@alibaba-inc.com>
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    Verify if libidn2 can correctly convert the internationalized domain name 
    to Punycode and convert it back to Unicode from Punycode.
    See tc_libidn2_libidn2_fun_001.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "libidn2 libidn2-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.IDNs_dict = {
            "中国.中国": "xn--fiqs8s.xn--fiqs8s",
            "مثال.إختبار": "xn--mgbh0fb.xn--kgbechtv",
            "παράδειγμα.δοκιμή": "xn--hxajbheg2az3al.xn--jxalpdlp",
            "пример.тест": "xn--e1afmkfd.xn--e1aybc",
            "उदाहरण.परीक्षा": "xn--p1b6ci4b4b3a.xn--11b5bs3a9aj6g",
            "例え.テスト": "xn--r8jz45g.xn--zckzah",
            "예제.테스트": "xn--2j5b21b.xn--9t4b11yi5a",
            "ตัวอย่าง.ทดสอบ": "xn--72c1a1bt4awk9o.xn--l3cfk7dp",
            "דוגמא.בדיקה": "xn--4dbdci0c.xn--5dbedt4e",
            "пример.тест": "xn--e1afmkfd.xn--e1aybc",
        }
        cmdline_converter_unic_to_puny_func ='''cat > unicode_to_punycode.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include <idn2.h>

int main(int argc, char *argv[]) {
    const char *unicode_domain = argv[1];
    char *ascii_domain = NULL;

    int ret = idn2_to_ascii_8z(unicode_domain, &ascii_domain, 0);

    if (ret != IDN2_OK) {
        fprintf(stderr, "Failed to convert domain: %s", idn2_strerror(ret));
        return EXIT_FAILURE;
    }
    printf("%s", ascii_domain);
    idn2_free(ascii_domain);
    return EXIT_SUCCESS;
}
EOF
'''
        cmdline_converter_puny_to_unic_func = '''cat >punycode_to_unicode.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include <idn2.h>

int main(int argc, char *argv[]) {
    const char *punycode_domain = argv[1];
    char *unicode_domain = NULL;

    int ret = idn2_to_unicode_8z8z(punycode_domain, &unicode_domain, 0);
  
    if (ret != IDN2_OK) {
        fprintf(stderr, "Failed to convert domain: %s", idn2_strerror(ret));
        return EXIT_FAILURE;
    }
    printf("%s", unicode_domain);
    idn2_free(unicode_domain);
    return EXIT_SUCCESS;
}
EOF
'''
        self.cmd(cmdline_converter_unic_to_puny_func)
        self.cmd(cmdline_converter_puny_to_unic_func)

    def test(self):
        self.cmd("gcc -o unicode_to_punycode unicode_to_punycode.c -lidn2")
        code, idn_result = self.cmd("file unicode_to_punycode")
        self.assertIn("dynamically linked", idn_result)
        
        for Unic, Puny in self.IDNs_dict.items():
            code, idn_test_result = self.cmd("./unicode_to_punycode %s" %Unic)
            self.assertEqual(Puny, idn_test_result)
        
        self.cmd("gcc -o punycode_to_unicode punycode_to_unicode.c -lidn2")
        code, idn_result = self.cmd("file punycode_to_unicode")
        self.assertIn("dynamically linked", idn_result)

        for Unic, Puny in self.IDNs_dict.items():
            code, idn_test_result = self.cmd("./punycode_to_unicode %s" %Puny)
            self.assertEqual(Unic, idn_test_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf unicode_to_punycode.c punycode_to_unicode.c unicode_to_punycode punycode_to_unicode")