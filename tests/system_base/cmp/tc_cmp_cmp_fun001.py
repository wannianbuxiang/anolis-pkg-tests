# -*- encoding: utf-8 -*-

"""
@File:      tc_cmp_cmp_fun001.py
@Time:      Thu Nov 09 2023 10:29:14
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_cmp_cmp_fun001.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "diffutils"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline='''	cat > file1 << EOF
aaa
bbb
ccc
EOF
'''
        self.cmd(cmdline)
        cmdline='''cat > file2 << EOF
aaa
bbb
ccd
EOF
'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("cp file1 file3")
        code,cmp_result=self.cmd("cmp -l file1 file3")
        self.assertNotIn("11",cmp_result)
        self.cmd("cmp -l file1 file2 >> cmp.log", ignore_status=True)
        code,cmp_result=self.cmd("cat cmp.log", ignore_status=True)
        self.assertIn("11",cmp_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf file1 file2 file3 cmp.log")