# -*- encoding: utf-8 -*-

"""
@File:      tc_chkconfig_chkconfig_fun001.py
@Time:      2024/6/28 16:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_chkconfig_chkconfig_fun001.yaml for details

    :avocado: tags=P1,noarch,local,chkconfig
    """
    PARAM_DIC = {"pkg_name": "chkconfig"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        
    def test(self):        	
        code, result = self.cmd("chkconfig --version")
        self.assertIn("chkconfig", result)
        self.cmd("man chkconfig > /tmp/chkconfig.log", ignore_status=True)
        code, result = self.cmd("cat /tmp/chkconfig.log", ignore_status=True)
        self.assertIn("SYNOPSIS", result)
      
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/chkconfig.log")
