# -*- encoding: utf-8 -*-

"""
@File:      tc_chkconfig_chkconfig_fun002.py
@Time:      2024/06/28 16:21:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_chkconfig_chkconfig_fun002.yaml for details

    :avocado: tags=P1,noarch,local,chkconfig
    """
    PARAM_DIC = {"pkg_name": "chkconfig network-scripts"}

    def setUp(self):
       super().setUp(self.PARAM_DIC)
              	  	 
    def test(self):
       self.cmd("chkconfig --add network")
       code, result = self.cmd("chkconfig --list")
       self.assertIn("network", result)
       self.cmd("chkconfig --del network")
       code, result = self.cmd("chkconfig --list")
       self.assertNotIn("network", result)
       self.cmd("chkconfig --add network")
       code, result = self.cmd("chkconfig --list")       
       self.assertIn("network", result)
       self.cmd("chkconfig --level 6 network on")
       code, result = self.cmd("chkconfig --list | grep network")
       self.assertIn("6:on", result)       
       self.cmd("chkconfig --level 6 network off")
       code, result = self.cmd("chkconfig --list | grep network")
       self.assertTrue("6:off", result)
       self.cmd("chkconfig --del network")
       code, result = self.cmd("chkconfig --list")
       self.assertNotIn("network", result)
              	 
    def tearDown(self):
       super().tearDown(self.PARAM_DIC)		
       