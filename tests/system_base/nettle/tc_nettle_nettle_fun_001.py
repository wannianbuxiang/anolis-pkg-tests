# -*- encoding: utf-8 -*-

"""
@File:      tc_nettle_nettle_fun001.py
@Time:      2024/11/27 17:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_nettle_nettle_fun001.yaml for details

    :avocado: tags=P1,noarch,local,nettle
    """
    PARAM_DIC = {"pkg_name": "gcc nettle-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > sha256_nettle.c << EOF
#include <stdio.h>
#include <string.h>
#include <nettle/sha.h>

void print_hex(const char *label, const uint8_t *data, size_t length) {
    printf("%s: ", label);
    for (size_t i = 0; i < length; i++) {
        printf("%02x", data[i]);
    }
    printf("\");
}

int main() {
    // yaohaxideshurushuju
    const char *input = "Nettle!";
    size_t input_length = strlen(input);
    
    // SHA-256 haxizhi
    uint8_t hash[SHA256_DIGEST_SIZE];

    // chushihhuaSHA-256 shangxiawen
    struct sha256_ctx ctx;
    sha256_init(&ctx);

    // gengxinhaxishangxiawen
    sha256_update(&ctx, input_length, (uint8_t *)input);

    // jisuanzuizhonghaxizhi
    sha256_digest(&ctx, SHA256_DIGEST_SIZE, hash);

    // dayinhaxizhi
    print_hex("SHA-256 Hash", hash, sizeof(hash));

    return 0;
}
EOF"""

        self.cmd(cmdline)

    def test(self):        	
        self.cmd("gcc sha256_nettle.c -o sha256_nettle -lnettle")
        code,result = self.cmd("./sha256_nettle")
        self.assertIn("SHA-256 Hash: 512ab5cfc88bb692b2faeb706e9769d90dec9059ae1e446364f9e1dbbff9cbba", result)
 
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf sha256_nettle.c sha256_nettle")