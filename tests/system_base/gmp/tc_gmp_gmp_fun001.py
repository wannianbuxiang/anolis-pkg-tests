# -*- encoding: utf-8 -*-

"""
@File:      tc_gmp_gmp_fun001.py
@Time:      2024/11/22 17:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_gmp_gmp_fun001.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "gmp gmp-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > gmp_test.c << EOF
#include <stdio.h>
#include <gmp.h>

int main() {
    // chushihuabianliang
    mpz_t a, b, result;

    // chushihuadashu
    mpz_init(a);
    mpz_init(b);
    mpz_init(result);

    // jiafaceshi
    mpz_set_str(a, "12345678901234567890", 10);
    mpz_set_str(b, "98765432109876543210", 10);
    mpz_add(result, a, b);
    gmp_printf("jiafa: a + b = %Zd", result); 

    // jiafaceshi
    mpz_set_str(a, "98765432109876543210", 10);
    mpz_set_str(b, "12345678901234567890", 10);
    mpz_sub(result, a, b);
    gmp_printf("jianfa: a - b = %Zd", result); 

    // chengfaceshi
    mpz_set_str(a, "12345678901234567890", 10);
    mpz_set_ui(b, 2);
    mpz_mul(result, a, b);
    gmp_printf("chengfa: a * b = %Zd", result); 

    // chufaceshi
    mpz_set_ui(a, 100);
    mpz_set_ui(b, 3);
    mpz_fdiv_q(result, a, b);
    gmp_printf("chufa: a / b = %Zd", result); 

    // qingli
    mpz_clear(a);
    mpz_clear(b);
    mpz_clear(result);

    return 0;
}
EOF"""

        self.cmd(cmdline)

    def test(self):        	
        self.cmd("gcc gmp_test.c -lgmp -o gmp_test")
        code,result = self.cmd("./gmp_test")
        self.assertIn("a + b = 111111111011111111100", result)
        code,result = self.cmd("./gmp_test")
        self.assertIn("a - b = 86419753208641975320", result)
        code,result = self.cmd("./gmp_test")
        self.assertIn("a * b = 24691357802469135780", result)
        code,result = self.cmd("./gmp_test")
        self.assertIn("a / b = 33", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf gmp_test gmp_test.c")