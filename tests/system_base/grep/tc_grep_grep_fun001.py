# -*- encoding: utf-8 -*-

"""
@File:      tc_grep_grep_fun001.py
@Time:      2024/11/20 17:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_grep_grep_fun001.yaml for details

    :avocado: tags=P1,noarch,local,grep
    """
    PARAM_DIC = {"pkg_name": "grep"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > grep.txt << EOF
hello world
Hello grep
goodbye world
EOF"""
        self.cmd(cmdline)

    def test(self):
        code,result=self.cmd("grep hello grep.txt")
        self.assertIn("hello world", result)
        	
        code,result=self.cmd("grep -i hello grep.txt")
        self.assertIn("hello world\nHello grep", result)

        code,result=self.cmd("grep -c -i hello grep.txt")
        self.assertIn("2", result)

        code,result=self.cmd("grep -v -i hello grep.txt")
        self.assertIn("goodbye world", result)


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf grep.txt")