# -*- encoding: utf-8 -*-

"""
@File:      tc_dnsmasq_dnsmasq_func_001.py
@Time:      2024/02/22 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_dnsmasq_dnsmasq_func_001.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "dnsmasq"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        self.cmd("systemctl start dnsmasq")
        code, dnsmasq_result=self.cmd("systemctl status dnsmasq")
        self.assertIn("active (running)", dnsmasq_result)
        self.cmd("systemctl restart dnsmasq")
        code, dnsmasq_result=self.cmd("systemctl status dnsmasq")
        self.assertIn("active (running)", dnsmasq_result)
        self.cmd("ps aux | grep -v grep | grep dnsmasq")
        code, dnsmasq_result = self.cmd("netstat -nlp | grep :53")
        self.assertIn("dnsmasq", dnsmasq_result)
        self.cmd("systemctl stop dnsmasq")
        self.cmd("systemctl status dnsmasq > dnsmasq.log", ignore_status=True)
        code, dnsmasq_result=self.cmd("cat dnsmasq.log", ignore_status=True)
        self.assertIn("inactive (dead)", dnsmasq_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf dnsmasq.log")
