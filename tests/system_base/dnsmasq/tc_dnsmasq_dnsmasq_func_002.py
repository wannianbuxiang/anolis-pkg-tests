# -*- encoding: utf-8 -*-

"""
@File:      tc_dnsmasq_dnsmasq_func_002.py
@Time:      2024/02/22 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_dnsmasq_dnsmasq_func_002.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "dnsmasq bind-utils"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        self.cmd('echo "address=/example.com/192.168.1.1" | sudo tee -a /etc/dnsmasq.conf')
        self.cmd("systemctl restart dnsmasq")
        code, dnsmasq_result=self.cmd("dig @127.0.0.1 example.com")
        self.assertIn("192.168.1.1", dnsmasq_result)
        code, dnsmasq_result = self.cmd("nslookup example.com 127.0.0.1")
        self.assertIn("192.168.1.1", dnsmasq_result)
        self.cmd("systemctl stop dnsmasq")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
