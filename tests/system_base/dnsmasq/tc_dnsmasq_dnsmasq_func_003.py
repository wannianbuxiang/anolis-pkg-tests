# -*- encoding: utf-8 -*-

"""
@File:      tc_dnsmasq_dnsmasq_func_003.py
@Time:      2024/02/22 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_dnsmasq_dnsmasq_func_003.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "dnsmasq"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd('cp /etc/dnsmasq.conf /etc/dnsmasq_test.conf')

    def test(self):
        self.cmd('echo "this-is-a-syntax-error" | sudo tee -a /etc/dnsmasq.conf')
        self.cmd("systemctl restart dnsmasq", ignore_status=True)
        self.cmd("systemctl status dnsmasq > dnsmasq.log", ignore_status=True)
        code, dnsmasq_result=self.cmd("cat dnsmasq.log", ignore_status=True)
        self.assertIn("bad option at line", dnsmasq_result)

    def tearDown(self):
        self.cmd("mv -f /etc/dnsmasq_test.conf /etc/dnsmasq.conf")
        self.cmd("systemctl restart dnsmasq")
        self.cmd("systemctl stop dnsmasq")
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf dnsmasq.log")
