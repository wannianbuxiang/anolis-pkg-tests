# -*- encoding: utf-8 -*-

"""
@File:      tc_selinux_selinux_fun001.py
@Time:      2024/7/5 10:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import RemoteTest

class Test(RemoteTest):
    """
    See tc_selinux_selinux_fun001.yaml for details

    :avocado: tags=P1,noarch,remote,selinux-policy
    """
    PARAM_DIC = {"pkg_name": "selinux-policy libselinux-utils"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.remote.cmd("sed -i 's/SELINUX=disabled/SELINUX=enforcing/g' /etc/selinux/config")
        self.remote.cmd("reboot")
        self.wait_ssh_connect()

    def test(self):        	
        code, result1=self.remote.cmd("getenforce")
        self.assertIn("Enforcing", result1)
        self.remote.cmd("setenforce 0")
        code, result2=self.remote.cmd("getenforce")
        self.assertIn("Permissive", result2)
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.remote.cmd("setenforce 1")
        self.remote.cmd("sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config")
        self.remote.cmd("reboot")
        self.wait_ssh_connect()
