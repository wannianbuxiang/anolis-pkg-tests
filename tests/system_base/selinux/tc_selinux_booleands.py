# -*- encoding: utf-8 -*-

"""
@File:      tc_selinux_booleands.py
@Time:      2024/7/5 10:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import RemoteTest

class Test(RemoteTest):
    """
    See tc_selinux_booleands.yaml for details

    :avocado: tags=P1,noarch,remote,selinux-policy
    """
    PARAM_DIC = {"pkg_name": "selinux-policy libselinux-utils policycoreutils"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.remote.cmd("sed -i 's/SELINUX=disabled/SELINUX=enforcing/g' /etc/selinux/config")
        self.remote.cmd("reboot")
        self.wait_ssh_connect()
        
    def test(self):
        self.remote.cmd("getsebool -a | grep httpd_use_nfs | grep off")
        self.remote.cmd("getsebool -a | grep httpd_use_cifs | grep off")
        self.remote.cmd("setsebool httpd_use_nfs on")
        self.remote.cmd("setsebool httpd_use_cifs on")
        code, result1=self.remote.cmd("getsebool -a | grep httpd_use_nfs | grep on")
        self.assertIn("httpd_use_nfs --> on", result1)
        code, result2=self.remote.cmd("getsebool -a | grep httpd_use_cifs | grep on")
        self.assertIn("httpd_use_cifs --> on", result2)
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.remote.cmd("setsebool httpd_use_nfs off")
        self.remote.cmd("setsebool httpd_use_cifs off")
        self.remote.cmd("sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config")
        self.remote.cmd("reboot")
        self.wait_ssh_connect()
        
