# -*- encoding: utf-8 -*-

"""
@File:      tc_cpio_cpio_fun002.py
@Time:      2024/6/11 15:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_cpio_cpio_fun002.yaml for details

    :avocado: tags=P1,noarch,local,cpio
    """
    PARAM_DIC = {"pkg_name": "cpio"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):     
        self.cmd("touch file1.txt file2.txt")
        self.cmd("echo This is file3.txt > file3.txt")    	
        self.cmd("find . -maxdepth 1 -type f -print0 | cpio --create --null --verbose --format=newc > /tmp/archive.cpio")
        self.cmd("ls -l /tmp/archive.cpio")
        self.cmd("cpio -idmv --directory=/tmp/extracted < /tmp/archive.cpio")
        self.cmd("cat file3.txt")
        self.cmd("cat /tmp/extracted/file3.txt")
        code, result = self.cmd("diff file3.txt /tmp/extracted/file3.txt")
        self.assertTrue("This is file3.txt", result)
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/archive.cpio")
        self.cmd("rm -rf ./file*")
        self.cmd("rm -rf /tmp/extracted*")