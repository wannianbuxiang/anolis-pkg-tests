# -*- encoding: utf-8 -*-

"""
@File:      tc_cpio_cpio_fun001.py
@Time:      2024/6/11 15:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_cpio_cpio_fun001.yaml for details

    :avocado: tags=P1,noarch,local,cpio
    """
    PARAM_DIC = {"pkg_name": "cpio"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        
    def test(self):        	
        code, result = self.cmd("cpio --version")
        self.assertIn("GNU cpio", result)
        self.cmd("cpio --help > cpio.log", ignore_status=True)
        code, result = self.cmd("cat cpio.log", ignore_status=True)
        self.assertIn("Usage:", result)
      
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf  cpio.log")
