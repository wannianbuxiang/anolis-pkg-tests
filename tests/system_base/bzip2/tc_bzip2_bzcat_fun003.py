# -*- encoding: utf-8 -*-

"""
@File:      tc_bzip2_bzcat_fun003.py
@Time:      2024/07/19 10:51:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_bzip2_bzcat_fun003.yaml for details

    :avocado: tags=P1,noarch,local,bzip2
    """
    PARAM_DIC = {"pkg_name": "bzip2"}

    def setUp(self):
       super().setUp(self.PARAM_DIC)
       with open('/tmp/test.txt', 'w') as file:    
           file.write("aabbcc")
       self.cmd("bzip2 -z /tmp/test.txt")
              	  	 
    def test(self):
       code, result=self.cmd("bzcat /tmp/test.txt.bz2 |grep aabbcc")
       self.assertIn("aabbcc", result)
       	 
    def tearDown(self):
       super().tearDown(self.PARAM_DIC)		
       self.cmd("rm -rf /tmp/test.txt.bz2")	
