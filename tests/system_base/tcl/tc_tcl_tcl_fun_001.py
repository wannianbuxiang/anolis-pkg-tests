# -*- encoding: utf-8 -*-

"""
@File:      tc_tcl_tcl_fun_001.py
@Time:      2024/06/14 17:34:20
@Author:    douzhichong
@Version:   1.0
@Contact:   liuyafei@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_tcl_tcl_fun_001.yaml for details

    :avocado: tags=P1,noarch,local,tcl
    """
    PARAM_DIC = {"pkg_name": "tcl"}
    
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """ cat > hello.tcl << EOF
#!/usr/bin/tclsh
puts stdout {Hello, World!}
# 定义两个数字
set num1 10
set num2 20

# 计算和打印总和
set sum [expr \$num1 + \$num2]
puts "The sum of \$num1 and \$num2 is: \$sum"

EOF"""
        self.cmd(cmdline)
        
    def test(self):
        code, result1=self.cmd("ls")
        self.assertIn("hello.tcl", result1)
        code, result2=self.cmd("tclsh hello.tcl")
        self.assertIn("The sum of 10 and 20 is: 30", result2)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf hello.tcl")

