# -*- encoding: utf-8 -*-

"""
@File:      tc_zstd_zstd_fun001.py
@Time:      2024/5/28 17:34:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_zstd_zstd_fun001.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "zstd"}
    
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("echo 'aaa' > file1")
        self.cmd("echo 'bbb' > file2")

    def test(self):
        self.cmd("zstd file1")
        code, zstd_ls1_result=self.cmd("ls")
        self.assertIn("file1.zst", zstd_ls1_result)
        self.cmd("zstd --rm -rf file2")
        code, zstd_ls2_result=self.cmd("ls")
        self.assertIn("file2.zst", zstd_ls2_result)
        code, zstd_cat_result=self.cmd("zstdcat file1.zst | grep aaa")
        self.assertIn("aaa", zstd_cat_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf file1 file1.zst file2 file2.zst")

