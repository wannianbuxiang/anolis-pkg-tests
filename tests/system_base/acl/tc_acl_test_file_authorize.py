#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

"""
@File:      tc_acl_test_file_authorize.py
@Time:      2024/07/15 16:23
@Author:    wangyang
@Version:   1.0
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_acl_test_file_authorize.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "acl"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd('useradd test')
        self.cmd('useradd test1')
        self.cmd('useradd test2')
        self.cmd('useradd test3')
        self.cmd('su - test -c "touch /home/test/testfile1"')
        self.cmd('su - test -c "touch /home/test/testfile2"')

    def test(self):
        code,result=self.cmd('su - test -c "setfacl -m u:test1:r,u:test2:w,u:test3:x testfile1"')
        self.assertEquals(0, code, f"set testfile1 acl fail with return code {code}")
        code,result=self.cmd('su - test -c "getfacl testfile1" | grep "user:test1:r--"')
        self.assertEquals(0, code, f"get testfile1 acl for test1 fail with return code {code}")
        code,result=self.cmd('su - test -c "getfacl testfile1" | grep "user:test2:-w-"')
        self.assertEquals(0, code, f"get testfile1 acl for test2 fail with return code {code}")
        code,result=self.cmd('su - test -c "getfacl testfile1" | grep "user:test3:--x"')
        self.assertEquals(0, code, f"get testfile1 acl for test3 fail with return code {code}")
        code,result=self.cmd('su - test -c "chmod 744 testfile2"')
        self.assertEquals(0, code, f"set testfile2 acl fail with return code {code}")

        code,result=self.cmd('stat -c %a /home/test/testfile1 | grep 674')
        self.assertEquals(0, code, f"testfile1 acl error with return code {code}")
        code,result=self.cmd('stat -c %a /home/test/testfile2 | grep 744')
        self.assertEquals(0, code, f"testfile2 acl error with return code {code}")


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for user in ['test', 'test1', 'test2', 'test3']:
            self.cmd(f"pgrep -u {user}| xargs kill -9", ignore_status=True)
            self.cmd(f"userdel {user}")
