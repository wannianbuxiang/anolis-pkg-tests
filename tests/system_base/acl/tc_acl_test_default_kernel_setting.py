#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

"""
@File:      tc_acl_test_default_kernel_setting.py
@Time:      2024/07/15 14:42
@Author:    wangyang
@Version:   1.0
"""

from common.basetest import LocalTest
from avocado.utils import process

class Test(LocalTest):
    """
    See tc_acl_test_default_kernel_setting.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "acl"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        code,result=self.cmd('grep -i acl /boot/config-$(uname -r) | grep -i CONFIG_XFS_POSIX_ACL=y')
        self.assertEquals(0, code, f"ACL kernel parameter of xfs file system is not enabled with return code {code}")
        code,result=self.cmd('grep -i acl /boot/config-$(uname -r) |  grep -i CONFIG_EXT4_FS_POSIX_ACL=y')
        self.assertEquals(0, code, f"ACL kernel parameter of ext4 file system is not enabled with return code {code}")
        try:
            code,result=self.cmd('grep -i acl /boot/config-$(uname -r) | grep -i "=n"')
        except process.CmdError as err:
            self.assertEquals(0, code, f"ACL kernel default parm setting is correctly with return code {code}")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
