# -*- encoding: utf-8 -*-

"""
@File:      tc_acl_test_backup_recovery.py
@Time:      2024/06/12 11:23:53
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_acl_test_backup_recovery.yaml for details

    :avocado: tags=P1,noarch,local,acl
    """
    PARAM_DIC = {"pkg_name": "acl"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        print("begin test")

    def test(self):
        self.cmd('useradd test1')
        self.cmd('useradd test2')
        self.cmd('mkdir testdir')
        self.cmd('touch testfile')
        self.cmd('chacl -b u::rw,g::rx,o::-,u:test1:rw,g:test1:rw,m::rwx u::rwx,g::r,o::-,u:test1:r,m::rwx testdir')
        self.cmd('chacl u::rw,g::rx,o::-,u:test2:rw,g:test2:rw,m::rwx testfile')
        self.cmd('getfacl testdir > acl1.txt')
        self.cmd('setfacl -b testdir')
        self.cmd('setfacl --set-file=acl1.txt testdir')
        self.cmd('getfacl testdir > acl1-1.txt')
        self.cmd('diff acl1.txt acl1-1.txt')

        self.cmd('getfacl testfile > acl2.txt')
        self.cmd('setfacl -b testfile')
        self.cmd('setfacl --set-file=acl2.txt testfile')
        self.cmd('getfacl testfile > acl2-2.txt')
        self.cmd('diff acl2.txt acl2-2.txt')

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for user in ['test1', 'test2']:
            self.cmd(f"pgrep -u {user}| xargs kill -9", ignore_status=True)
            self.cmd(f"userdel {user}")
        self.cmd('rm -rf testfile testdir')
        self.cmd('rm -rf acl1-1.txt')
        self.cmd('rm -rf acl1.txt')
        self.cmd('rm -rf acl2-2.txt')
        self.cmd('rm -rf acl2.txt')
