# -*- encoding: utf-8 -*-

"""
@File:      tc_bc_bc_fun002.py
@Time:      2024/09/03 16:51:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_bc_bc_fun002.yaml for details

    :avocado: tags=P1,noarch,local,bc
    """
    PARAM_DIC = {"pkg_name": "bc"}

    def setUp(self):
       super().setUp(self.PARAM_DIC)
              	  	 
    def test(self):
       code,result = self.cmd('echo "1+1" | bc')
       self.assertIn("2",result)
       code,result = self.cmd('echo "1-1" | bc')
       self.assertIn("0",result)
       code,result = self.cmd('echo "1*1" | bc')
       self.assertIn("1",result)
       code,result = self.cmd('echo "1/1" | bc')
       self.assertIn("1",result)
                    	 
    def tearDown(self):
       super().tearDown(self.PARAM_DIC)		

