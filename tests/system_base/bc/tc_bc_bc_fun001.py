# -*- encoding: utf-8 -*-

"""
@File:      tc_bc_bc_fun001.py
@Time:      2024/09/03 16:51:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_bc_bc_fun001.yaml for details

    :avocado: tags=P1,noarch,local,bc
    """
    PARAM_DIC = {"pkg_name": "bc"}

    def setUp(self):
       super().setUp(self.PARAM_DIC)
              	  	 
    def test(self):
       code,result = self.cmd('echo "a = 5; b = 3; a + b" | bc')
       self.assertIn("8",result)
       code,result = self.cmd('echo "1/0" | bc 2>&1')
       self.assertIn("Divide by zero",result)
       code,result = self.cmd('echo "(5 + 3) * 2" | bc')
       self.assertIn("16",result)
       code,result = self.cmd('echo "2^100" | bc')
       self.assertIn("1267650600228229401496703205376",result)
                    	 
    def tearDown(self):
       super().tearDown(self.PARAM_DIC)		

