# -*- encoding: utf-8 -*-

"""
@File:      tc_bc_dc_fun001.py
@Time:      2024/8/15 17:31:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_bc_dc_fun001.yaml for details

    :avocado: tags=P1,noarch,local,bc
    """
    PARAM_DIC = {"pkg_name": "bc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        
    def test(self):        	
        code, result = self.cmd("dc --version")
        self.assertIn("dc (GNU bc 1.07.1)", result)
        self.cmd(" dc --help > dc.log", ignore_status=True)
        code, result = self.cmd("cat dc.log", ignore_status=True)
        self.assertIn("Usage:", result)
      
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf dc.log")
