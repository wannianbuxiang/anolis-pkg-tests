# -*- encoding: utf-8 -*-

"""
@File:      tc_bc_dc_fun002.py
@Time:      2024/8/15 17:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_dc_dc_fun002.yaml for details

    :avocado: tags=P1,noarch,local,bc
    """
    PARAM_DIC = {"pkg_name": "bc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > calc.txt << EOF
10 20 + p
EOF"""
        self.cmd(cmdline)

    def test(self):
        code,result=self.cmd("dc -e '5 3 + p'")
        self.assertIn("8",result)
        code,result=self.cmd("dc -e '5 3 - p'")
        self.assertIn("2",result)
        code,result=self.cmd("dc -e '5 3 * p'")
        self.assertIn("15",result)
        code,result=self.cmd("dc -e '10 5 / p'")
        self.assertIn("2",result)          	         	
        code,result=self.cmd("dc -f calc.txt")
        self.assertIn("30",result)       

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf calc.txt")