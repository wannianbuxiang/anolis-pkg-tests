# -*- encoding: utf-8 -*-

"""
@File:      tc_lzop_lzop_fun_001.py
@Time:      2024/11/1 16:51:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_lzop_lzop_fun_001.yaml for details

    :avocado: tags=P1,noarch,local,lzop
    """
    PARAM_DIC = {"pkg_name": "lzop"}

    def setUp(self):
       super().setUp(self.PARAM_DIC)
       self.cmd("echo 'lzop_test' > lzop_testfile")

    def test(self):
       self.cmd("rpm -qa |grep lzop")
       self.cmd("lzop lzop_testfile")
       code, result = self.cmd("ls")
       self.assertIn("lzop_testfile.lzo", result)
       self.cmd("lzop -df lzop_testfile.lzo")
       code, result = self.cmd("cat lzop_testfile")
       self.assertIn("lzop_test", result)
       	 
    def tearDown(self):
       super().tearDown(self.PARAM_DIC)		
       self.cmd("rm -rf lzop_testfile")	
       self.cmd("rm -rf lzop_testfile.lzo")
