#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_unbound_unbound_control_fun_001.py 
@Time:      2024/04/18 14:05
@Author:    zhanggaiyan
@Version:   1.0
@Contact:   zhanggaiyan@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhanggaiyan
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_unbound_unbound_control_fun_001.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "unbound"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
	self.cmd("unbound-control -c /etc/unbound/unbound.conf start")
        ret_c, ret_o = self.cmd("unbound-control -c /etc/unbound/unbound.conf status")
        self.assertTrue("is running" in ret_o, 'check output error')
        ret_c, ret_o = self.cmd("unbound-control -c /etc/unbound/unbound.conf reload")
        self.assertTrue("ok" in ret_o, 'check output error')
        self.cmd("unbound-control -c /etc/unbound/unbound.conf flush_zone .")
        ret_c, ret_o = self.cmd("unbound-control -c /etc/unbound/unbound.conf stats")
        self.log.info(ret_o)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
