#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_unbound_unbound_control_setup_fun_001.py 
@Time:      2024/04/18 14:05
@Author:    zhanggaiyan
@Version:   1.0
@Contact:   zhanggaiyan@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhanggaiyan
"""

from common.basetest import LocalTest
import os

class Test(LocalTest):
    """
    See tc_unbound_unbound_control_setup_fun_001.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "unbound"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir control_dir")
        global control_cert
        global control_key
        control_cert = "control_dir/unbound_control.pem"
        control_key = "control_dir/unbound_control.key"

    def test(self):
        self.cmd("unbound-control-setup -d 'control_dir'")
        self.cmd("ls -l {}".format(control_cert))
        self.cmd("ls -l {}".format(control_key))

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf control_dir")
