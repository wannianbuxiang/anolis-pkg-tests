#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_unbound_unbound_fun_001.py 
@Time:      2024/04/18 14:05
@Author:    zhanggaiyan
@Version:   1.0
@Contact:   zhanggaiyan@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhanggaiyan
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_unbound_unbound_fun_001.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "unbound bind-utils"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("cp /etc/unbound/unbound.conf /etc/unbound/unbound.conf-bak")
        self.cmd("echo 'server:' >> /etc/unbound/unbound.conf")
        self.cmd("echo 'interface: 1.1.1.1' >> /etc/unbound/unbound.conf")
        self.cmd("echo 'interface: ::1' >> /etc/unbound/unbound.conf")
        self.cmd("echo 'access-control: 0.0.0.0/0 allow' >> /etc/unbound/unbound.conf")
        self.cmd("echo 'access-control: ::0/0 allow' >> /etc/unbound/unbound.conf")

    def test(self):
        # start unbound service
        self.cmd("systemctl start unbound")
        self.cmd("systemctl status unbound |grep running")
        ret_c, ret_o = self.cmd("dig @localhost example.com A")
        self.assertTrue("93.184.215.14" in ret_o, 'check output error')
        ret_c, ret_o = self.cmd("dig @localhost example.com NS")
        self.assertTrue("a.iana-servers.net" in ret_o, 'check output error')
        ret_c, ret_o = self.cmd("dig @localhost example.com TXT")
        self.assertTrue("wgyf8z8cgvm2qmxpnbnldrcltvk4xqfn" in ret_o, 'check output error')

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("mv /etc/unbound/unbound.conf-bak /etc/unbound/unbound.conf")
