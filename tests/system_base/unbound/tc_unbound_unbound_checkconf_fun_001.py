#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_unbound_unbound_checkconf_fun_001.py
@Time:      2024/04/18 14:05
@Author:    zhanggaiyan
@Version:   1.0
@Contact:   zhanggaiyan@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhanggaiyan
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_unbound_unbound_checkconf_fun_001.yaml for details

    :avocado: tags=P3,noarch,locali,fix
    """
    PARAM_DIC = {"pkg_name": "unbound"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir temp_dir")
        # Create a valid configuration file
        cmdline_va = """cat > temp_dir/valid.conf <<EOF
server:
    interface: 0.0.0.0
    port: 53
    verbosity: 1
EOF"""
        self.cmd(cmdline_va)

        cmdline_inva = """cat > temp_dir/invalid.conf <<EOF
server:
    interface: 0.0.0.0
    port: 53
    invalid_key: invalid_value
EOF"""
        self.cmd(cmdline_inva)

    def test(self):
        ret_c, ret_o = self.cmd("unbound-checkconf 'temp_dir/valid.conf'", ignore_status=True)
        self.assertTrue("no errors in temp_dir/valid.conf" in ret_o, 'check output error') 
        ret_c, ret_o = self.cmd("unbound-checkconf 'temp_dir/invalid.conf'", ignore_status=True)
        self.assertTrue("unknown keyword" in ret_o, 'check output error')


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf temp_dir")
