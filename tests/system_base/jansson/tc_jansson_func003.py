#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_jansson_func003.py
@Time:      2024/04/01 13:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""
import subprocess
from common.basetest import LocalTest

class Test(LocalTest): 
    """
    See tc_jansson_func003.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "jansson jansson-devel"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_jansson.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <jansson.h>

// 用于报告JSON解析错误的特殊版本
void parse_error_exit(const char *message, int line, const char *text) {
    fprintf(stderr, "%s: On line %d: %s\\n", message, line, text);
    exit(EXIT_FAILURE);
}

int main() {
    // 解析JSON字符串
    const char *json_text = "{\\"name\\":\\"Alice\\",\\"age\\":25}";
    json_error_t error;
    json_t *root = json_loads(json_text, 0, &error); 

    // 检查是否成功解析
    if (!root) {
        parse_error_exit("Failed to parse JSON", error.line, error.text);
    }

    char *serialized_json = json_dumps(root, JSON_INDENT(4));
    // 输出 JSON 字符串到控制台
    printf("Serialized JSON: %s\\n", serialized_json);
    free(serialized_json); 

    // 访问不存在的键
    json_t *nonexistent_value = json_object_get(root, "nonexistent_key");
    if (!nonexistent_value) {
        printf("Key 'nonexistent_key' does not exist.\\n");
    }
    json_decref(root);

    // 解析无效的JSON
    json_text = "{invalid json}";
    root = json_loads(json_text, 0, &error);
    if (!root) {
        parse_error_exit("Failed to parse invalid JSON", error.line, error.text);
    }

    // 此处应当不会执行，因为上面的json_loads应该已经抛出了错误
    json_decref(root);

    return 0;
}
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o /tmp/ghm/test_jansson /tmp/ghm/test_jansson.c -ljansson")
        ret_o1 = subprocess.run("/tmp/ghm/test_jansson", capture_output = True, text = True, shell = True)
        self.assertIn("Serialized JSON", ret_o1.stdout)
        self.assertIn("Key 'nonexistent_key' does not exist.", ret_o1.stdout)
        self.assertIn("Failed to parse invalid JSON: On line 1: string or '}' expected near 'invalid'", ret_o1.stderr)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")