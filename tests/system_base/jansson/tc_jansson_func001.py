#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_jansson_func001.py
@Time:      2024/04/01 17:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_jansson_func001.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "jansson jansson-devel"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_jansson.c <<EOF
#include <stdio.h>
#include <jansson.h>

void test_jansson() {
    // 创建 JSON 对象
    json_t *root = json_object();
    
    // 向 JSON 对象添加键值对
    json_object_set_new(root, "name", json_string("John Doe"));
    json_object_set_new(root, "age", json_integer(30));
    json_object_set_new(root, "skills", json_array());
    
    // 向数组添加元素
    json_t *skills = json_object_get(root, "skills");
    json_array_append_new(skills, json_string("C programming"));
    json_array_append_new(skills, json_string("Networking"));

    // 序列化为 JSON 字符串
    char *serialized_json = json_dumps(root, JSON_INDENT(4));

    // 输出 JSON 字符串到控制台
    printf("Serialized JSON: %s\\n", serialized_json);

    // 解析 JSON 字符串 
    json_error_t error;
    json_t *parsed_root = json_loads(serialized_json, 0, &error);

    // 检查解析是否成功，并访问数据
    if(parsed_root) {
        json_t *name = json_object_get(parsed_root, "name");
        const char *name_str = json_string_value(name);
        printf("Name: %s\\n", name_str);

        json_t *age = json_object_get(parsed_root, "age");
        int age_int = json_integer_value(age);
        printf("Age: %d\\n", age_int);

        json_t *skills_list = json_object_get(parsed_root, "skills");
        if(json_is_array(skills_list)) {
            size_t i, count = json_array_size(skills_list);
            for(i = 0; i < count; i++) {
                json_t *skill = json_array_get(skills_list, i);
                const char *skill_str = json_string_value(skill);
                printf("Skill %lu: %s\\n", i + 1, skill_str);
            }
        }

        // 清理内存
        json_decref(parsed_root);
    } else {
        printf("Error while parsing JSON: %s at line %d column %d\\n",
               error.text, error.line, error.column);
    }

    // 清理创建的 JSON 对象
    json_decref(root);
    free(serialized_json);
}

int main() {
    test_jansson();
    return 0;
}
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o /tmp/ghm/test_jansson /tmp/ghm/test_jansson.c -ljansson")
        ret_c1, ret_o1 = self.cmd("/tmp/ghm/test_jansson")
        self.assertIn("Serialized JSON", ret_o1)
        self.assertIn("Name: John Doe", ret_o1)
        self.assertIn("Age: 30", ret_o1)
        self.assertIn("Skill 1: C programming", ret_o1)
        self.assertIn("Skill 2: Networking", ret_o1)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")