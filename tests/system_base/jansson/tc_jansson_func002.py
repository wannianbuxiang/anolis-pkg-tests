#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_jansson_func002.py
@Time:      2024/04/02 10:00:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_jansson_func002.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "jansson jansson-devel"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_jansson.c <<EOF
#include <stdio.h>
#include <jansson.h>

// 创建并修改嵌套JSON对象和数组
void create_and_modify_json() {
    json_t *root = json_object();
    json_t *array = json_array();

    // 添加一些数组元素
    json_array_append_new(array, json_string("item1"));
    json_array_append_new(array, json_integer(2));
    json_array_append_new(array, json_true());

    // 将数组添加到根对象中
    json_object_set_new(root, "nestedArray", array);

    // 创建嵌套的对象
    json_t *nested_obj = json_object();
    json_object_set_new(nested_obj, "key1", json_string("value1"));
    json_object_set_new(nested_obj, "key2", json_integer(3));

    // 将嵌套对象添加到根对象中
    json_object_set_new(root, "nestedObject", nested_obj);

    // 序列化为 JSON 字符串并输出
    char *serialized_json = json_dumps(root, JSON_INDENT(4)); 
    printf("Serialized JSON: %s\\n", serialized_json);

    // 修改数组和嵌套对象
    json_array_insert_new(array, 1, json_string("Inserted Item"));  // 在数组索引1的位置插入新元素
    json_object_set_new(nested_obj, "key1", json_string("Modified Value"));  // 修改嵌套对象的key1的值

    // 输出修改后的JSON字符串
    const char *json_str = json_dumps(root, JSON_INDENT(4));
    printf("%s\\n", json_str);

    // 释放内存
    json_decref(root);
    json_decref(nested_obj);
}

int main() {
    create_and_modify_json();
    return 0;
}
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o /tmp/ghm/test_jansson /tmp/ghm/test_jansson.c -ljansson")
        ret_c1, ret_o1 = self.cmd("/tmp/ghm/test_jansson")
        self.log.info(ret_o1)
        self.assertIn("value1", ret_o1)
        self.assertIn("Inserted Item", ret_o1)
        self.assertIn("Modified Value", ret_o1)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")