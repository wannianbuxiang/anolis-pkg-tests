#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_jansson_func004.py
@Time:      2024/04/01 14:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest): 
    """
    See tc_jansson_func004.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "jansson jansson-devel"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_jansson.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <jansson.h>

void print_json_value(json_t *value, int indent_level) {
    switch (json_typeof(value)) {
        case JSON_OBJECT:
            const char *key;
            json_t *val;

            json_object_foreach(value, key, val) {
                for (int i = 0; i < indent_level; ++i)
                    printf("  ");

                printf("\\"%s\\": ", key);
                print_json_value(val, indent_level + 1);
            }
            break;

        case JSON_ARRAY:
            size_t array_size = json_array_size(value);
            for (size_t i = 0; i < array_size; ++i) {
                json_t *element = json_array_get(value, i);
                print_json_value(element, indent_level);
            }
            break;

        case JSON_STRING:
            printf("\\"%s\\"\\n", json_string_value(value));
            break;

        case JSON_INTEGER:
            printf("%lld\\n", (long long)json_integer_value(value));
            break;

        case JSON_REAL:
            printf("%f\\n", (double)json_real_value(value));
            break;

        case JSON_TRUE:
            printf("true\\n");
            break;

        case JSON_FALSE:
            printf("false\\n");
            break;

        case JSON_NULL:
            printf("null\\n");
            break;

        default:
            fprintf(stderr, "Unknown JSON type encountered!\\n");
            break;
    }
}

int main() {
    const char *json_text = "{\\"person\\":{\\"name\\":\\"Alice\\",\\"age\\":25},\\"hobbies\\":[\\"Reading\\", \\"Gaming\\"]}";
    json_error_t error;
    json_t *root = json_loads(json_text, 0, &error);

    if (!root) {
        fprintf(stderr, "Error parsing JSON: %s on line %d\\n", error.text, error.line);
        return EXIT_FAILURE;
    }
    print_json_value(root, 0);

    json_decref(root);
    return 0;
}
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o /tmp/ghm/test_jansson /tmp/ghm/test_jansson.c -ljansson")
        ret_c1, ret_o1 = self.cmd("/tmp/ghm/test_jansson")
        self.assertIn("person", ret_o1)
        self.assertIn("name", ret_o1)
        self.assertIn("Alice", ret_o1)
        self.assertIn("age", ret_o1)
        self.assertIn("25", ret_o1)
        self.assertIn("hobbies", ret_o1)
        self.assertIn("Reading", ret_o1)
        self.assertIn("Gaming", ret_o1)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")