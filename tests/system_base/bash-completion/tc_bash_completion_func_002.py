# -*- encoding: utf-8 -*-

"""
@File:      tc_bash_completion_func_002.py
@Time:      2024/02/28 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

import pexpect
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_bash_completion_func_002.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "bash-completion"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir test1_dir test2_dir")

    def test(self):
        child = pexpect.spawn('/bin/bash')
        child.send('cd test')
        child.send('\t\t')
        ret = child.expect('test[12]_dir/', timeout=2)
        self.assertTrue(ret == 0, "automatic completion function is invalid")

        child.send('2')
        child.send('\t')
        child.expect(pexpect.TIMEOUT, timeout=1)
        self.assertIn("cd test2_dir/", child.buffer.decode(), "automatic completion function is invalid")
        
    def tearDown(self):
        self.cmd("rm -rf test1_dir test2_dir")
        super().tearDown(self.PARAM_DIC)
