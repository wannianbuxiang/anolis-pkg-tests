# -*- encoding: utf-8 -*-

"""
@File:      tc_bash_completion_func_004.py
@Time:      2024/02/28 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

import pexpect
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_bash_completion_func_004.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "bash-completion"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("touch \"a file with test.txt\"")

    def test(self):
        child = pexpect.spawn('/bin/bash')
        child.send("cat \"a f")
        child.send('\t')
        child.expect(pexpect.TIMEOUT, timeout=1)
        self.assertIn("cat \"a file with test.txt\"", child.buffer.decode(), "automatic completion function is invalid")
        
    def tearDown(self):
        self.cmd("rm -rf \"a file with test.txt\"")
        super().tearDown(self.PARAM_DIC)
