# -*- encoding: utf-8 -*-

"""
@File:      tc_bash_completion_func_006.py
@Time:      2024/02/28 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

import pexpect
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_bash_completion_func_006.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "bash-completion git"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        child = pexpect.spawn('/bin/bash')
        child.send("cat /ro")
        child.send('\t')
        child.expect(pexpect.TIMEOUT, timeout=1)
        print('111', child.buffer.decode())
        self.assertIn("cat /root/", child.buffer.decode(), "automatic completion function is invalid")
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
