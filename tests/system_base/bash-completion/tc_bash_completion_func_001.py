# -*- encoding: utf-8 -*-

"""
@File:      tc_bash_completion_func_001.py
@Time:      2024/02/28 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

import pexpect
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_bash_completion_func_001.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "bash-completion"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("touch testfile1.txt testfile2.txt")

    def test(self):
        child = pexpect.spawn('/bin/bash')
        child.send('ls testfile')
        child.send('\t\t')
        ret = child.expect('testfile[12].txt', timeout=2)
        self.assertTrue(ret == 0, "automatic completion function is invalid")

        child.send('2')
        child.send('\t')
        child.expect(pexpect.TIMEOUT, timeout=1)
        self.assertIn("ls testfile2.txt", child.buffer.decode(), "automatic completion function is invalid")
        
    def tearDown(self):
        self.cmd("rm -rf testfile1.txt testfile2.txt")
        super().tearDown(self.PARAM_DIC)
