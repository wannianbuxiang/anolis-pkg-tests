# -*- encoding: utf-8 -*-

"""
@File:      tc_bash_completion_func_003.py
@Time:      2024/02/28 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

import pexpect
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_bash_completion_func_003.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "bash-completion"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("touch testfile3.txt testfile4.txt")

    def test(self):
        child = pexpect.spawn('/bin/bash')
        child.send('rm testfile')
        child.send('\t\t')
        ret = child.expect('testfile[34].txt', timeout=2)
        self.assertTrue(ret == 0, "automatic completion function is invalid")

        child.send('4')
        child.send('\t')
        child.expect(pexpect.TIMEOUT, timeout=1)
        self.assertIn("rm testfile4.txt", child.buffer.decode(), "automatic completion function is invalid")
        
    def tearDown(self):
        self.cmd("rm -rf testfile3.txt testfile4.txt")
        super().tearDown(self.PARAM_DIC)
