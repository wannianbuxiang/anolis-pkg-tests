# -*- encoding: utf-8 -*-

"""
@File:      tc_pigz_pigz_fun001.py
@Time:      2024/8/21 17:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_pigz_pigz_fun001.yaml for details

    :avocado: tags=P1,noarch,local,pigz
    """
    PARAM_DIC = {"pkg_name": "pigz"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        self.cmd("mkdir /tmp/pigz")
        self.cmd("echo aaaa > /tmp/pigz/test.txt")
        self.cmd("pigz --force /tmp/pigz/test.txt")
        code, result1=self.cmd("test -e /tmp/pigz/test.txt.gz && echo $?")
        self.assertIn("0", result1)
        self.cmd("pigz -d /tmp/pigz/test.txt.gz")
        self.cmd("pigz --independent /tmp/pigz/test.txt")
        code, result2=self.cmd("test -e /tmp/pigz/test.txt.gz && echo $?")
        self.assertIn("0", result2)
        self.cmd("pigz -d /tmp/pigz/test.txt.gz")
        self.cmd("pigz --stdout /tmp/pigz/test.txt > /tmp/pigz/test.txt.gz")
        code, result3=self.cmd("test -e /tmp/pigz/test.txt.gz && echo $?")
        self.assertIn("0", result3)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/pigz")
