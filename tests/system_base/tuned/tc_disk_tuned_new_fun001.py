# -*- encoding: utf-8 -*-

"""
@File:      tc_disk_tuned_new_fun001.py
@Time:      2024/06/12 10:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_disk_tuned_new_fun001.yaml for details

    :avocado: tags=P1,noarch,local,tuned
    """
    PARAM_DIC = {"pkg_name": "tuned"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /etc/tuned/my-profile")
        cmdline = """echo '[main]
summary=General non-specialized tuned profile
[cpu]
governor=conservative
energy_perf_bias=normal
[audio]
timeout=10
[video]
radeon_powersave=dpm-balanced, auto' >/etc/tuned/my-profile/tuned.conf"""
        self.cmd(cmdline)
        cmdline1 = """tuned-adm active | awk '{print $4}' >/tmp/file"""
        self.cmd(cmdline1)
        
    def test(self):        	
        code,result1=self.cmd("rpm -qa| grep tuned")
        self.assertIn("tuned", result1)
        self.cmd("tuned-adm profile my-profile")
        code,result2=self.cmd("tuned-adm active | grep 'my-profile'")
        self.assertIn("my-profile", result2)
        code,result3=self.cmd("tuned-adm verify | grep 'succeeded'")
        self.assertIn("succeeded", result3)
        self.cmd('tuned-adm profile "$(cat /tmp/file)"')
        

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/file  /etc/tuned/my-profile")
