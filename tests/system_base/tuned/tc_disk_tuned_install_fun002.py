# -*- encoding: utf-8 -*-

"""
@File:      tc_disk_tuned_install_fun002.py
@Time:      2024/06/12 10:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_disk_tuned_install_fun002.yaml for details

    :avocado: tags=P1,noarch,local,tuned
    """
    PARAM_DIC = {"pkg_name": "tuned"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        
        
    def test(self):        	
        code, result1=self.cmd("rpm -qa| grep tuned")
        self.assertIn("tuned", result1)
        self.cmd("systemctl enable --now tuned")
        code, result2=self.cmd("systemctl is-enabled tuned")
        self.assertIn("enabled", result2)

        # Record the initial tuned-adm active profile
        code, initial_active_profile = self.cmd("tuned-adm active")
        self.initial_active_profile = initial_active_profile.strip().split(': ')[1]

        # Record the initial vm.swappiness value
        code, initial_sysctl_conf = self.cmd("sysctl -a | grep vm.swappiness")
        self.initial_swappiness_value = int(initial_sysctl_conf.strip().split('=')[1].strip())

        # Apply 'throughput-performance' profile
        self.cmd("tuned-adm profile throughput-performance")
        self.cmd("systemctl restart tuned")
        
        # Verify the active profile
        code, active_profile = self.cmd("tuned-adm active")
        self.assertRegex(active_profile, "throughput-performance")

        # Set vm.swappiness using sysctl, the value is 10
        swappiness_value = 10
        self.cmd(f"sysctl -w vm.swappiness={swappiness_value}")

        # Verify the current vm.swappiness value
        code, current_swappiness = self.cmd("sysctl -n vm.swappiness")
        self.assertEqual(int(current_swappiness.strip()), swappiness_value)

        # Verify the tuned-adm verify command
        code, result = self.cmd("tuned-adm verify | grep succeeded")
        self.assertIn("succeeded", result)

    def tearDown(self):
        # restore initial state
        self.cmd(f"tuned-adm profile {self.initial_active_profile}")
        self.cmd(f"sysctl -w vm.swappiness={self.initial_swappiness_value}")
        super().tearDown(self.PARAM_DIC)
        
