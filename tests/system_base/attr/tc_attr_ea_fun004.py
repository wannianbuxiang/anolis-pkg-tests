# -*- encoding: utf-8 -*-

"""
@File:      tc_attr_ea_fun004.py
@Time:      2024/03/18 10:12:50
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_attr_ea_fun004.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "attr"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.file = 'attr_testfile'
        self.attribute = 'user.comment'
        self.value = 'Test comment'
        cmdline=f'''cat > {self.file} <<EOF
attr test
EOF
'''
        self.cmd(cmdline)

    def test(self):
        code,selinux_result=self.cmd("sestatus")
        if selinux_result.endswith("disabled"):
            self.cmd(f"echo '{self.value}' | attr -s {self.attribute} {self.file}")
            code, attr_result = self.cmd(f"attr -g {self.attribute} {self.file}")
            self.assertIn(f'{self.value}', attr_result)
            code, attr_result = self.cmd(f"attr -lq {self.file}")
            self.assertIn(f'{self.attribute}', attr_result)
            self.cmd(f"attr -r {self.attribute} {self.file}")
            code, attr_result = self.cmd(f"attr -lq {self.file}")
            self.assertNotIn(f'{self.attribute}', attr_result)
        else:
            self.cmd(f"echo '{self.value}' | attr -s {self.attribute} {self.file}")
            code, attr_result = self.cmd(f"attr -g {self.attribute} {self.file}")
            self.assertIn(f'{self.value}', attr_result)
            code, attr_result = self.cmd(f"attr -lq {self.file}")
            self.assertIn(f'{self.attribute}', attr_result)
            self.assertIn('selinux', attr_result)
            self.cmd(f"attr -r {self.attribute} {self.file}")
            code, attr_result = self.cmd(f"attr -lq {self.file}")
            self.assertNotIn(f'{self.attribute}', attr_result)            

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f"rm -rf {self.file}")
