# -*- encoding: utf-8 -*-

"""
@File:      tc_attr_help_fun001.py
@Time:      Wed Nov 08 2023 17:12:33
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_attr_help_fun001.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "attr"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline='''cat > attr_testfile <<EOF
attr test
EOF
'''
        self.cmd(cmdline)

    def test(self):
        code,selinux_result=self.cmd("sestatus")
        if selinux_result.endswith("disabled"):
            code,attr_result=self.cmd("attr",ignore_status=True)
            self.assertIn("Usage: attr",attr_result)
            self.cmd("getfattr --help")
            self.cmd("setfattr --help")
        else:
            code,attr_result=self.cmd("attr",ignore_status=True)
            self.assertIn("Usage: attr",attr_result)
            self.cmd("getfattr --help")
            self.cmd("setfattr --help")
            code,attr_result=self.cmd("getfattr -m. -d attr_testfile")
            self.assertIn("unconfined_u:object_r:admin_home_t:s0",attr_result)
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf attr_testfile")