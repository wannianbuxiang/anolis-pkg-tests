# -*- encoding: utf-8 -*-

"""
@File:      tc_attr_ea_fun006.py
@Time:      2024/03/18 10:12:50
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_attr_ea_fun006.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "attr"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.file = 'attr_testfile'
        self.attribute = 'security.my_attribute'
        self.value = 'my_policy'
        self.cmd(f"echo 'hello' > {self.file}")

    def test(self):
        self.cmd(f"attr -RS -s {self.attribute} -V '{self.value}' {self.file}")
        code, attr_result = self.cmd(f"attr -RS -g {self.attribute} {self.file}")
        self.assertIn(f'{self.value}', attr_result)
        code, attr_result = self.cmd(f"attr -lRq {self.file}")
        self.assertIn(f'{self.attribute}', attr_result)
        self.cmd(f"attr -RS -r {self.attribute} {self.file}")
        code, attr_result = self.cmd(f"attr -lRq {self.file}")
        self.assertNotIn(f'{self.attribute}', attr_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f"rm -rf {self.file}")