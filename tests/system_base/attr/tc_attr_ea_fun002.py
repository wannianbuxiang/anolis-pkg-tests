# -*- encoding: utf-8 -*-

"""
@File:      tc_attr_ea_fun002.py
@Time:      Wed Nov 08 2023 17:26:22
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_attr_ea_fun002.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "attr"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline='''cat > attr_testfile <<EOF
attr test
EOF
'''
        self.cmd(cmdline)

    def test(self):
        code,selinux_result=self.cmd("sestatus")
        if selinux_result.endswith("disabled"):
            self.cmd("setfattr -n user.foo -v bar attr_testfile")
            code,attr_result=self.cmd("getfattr -n user.foo attr_testfile")
            self.assertIn('user.foo="bar"',attr_result)
            code,attr_result=self.cmd("attr -lq attr_testfile")
            self.assertIn('foo',attr_result)
            code,attr_result=self.cmd('getfattr -d -m ".*" attr_testfile')
            self.assertIn('user.foo="bar"',attr_result)
            code,attr_result=self.cmd("getfattr -m. -e hex -d attr_testfile")
            self.assertIn('user.foo=0x626172',attr_result)
            code,attr_result=self.cmd("getfattr -m. -e base64 -d attr_testfile")
            self.assertIn('user.foo=0sYmFy',attr_result)
            self.cmd("setfattr -x user.foo attr_testfile")
        else:
            self.cmd("setfattr -n user.foo -v bar attr_testfile")
            code,attr_result=self.cmd("getfattr -n user.foo attr_testfile")
            self.assertIn('user.foo="bar"',attr_result)
            code,attr_result=self.cmd("attr -lq attr_testfile")
            self.assertIn('foo',attr_result)
            self.assertIn('selinux',attr_result)
            code,attr_result=self.cmd('getfattr -d -m ".*" attr_testfile')
            self.assertIn('security.selinux',attr_result)
            self.assertIn('user.foo="bar"',attr_result)
            code,attr_result=self.cmd("getfattr -m. -e hex -d attr_testfile")
            self.assertIn('security.selinux',attr_result)
            self.assertIn('user.foo=0x626172',attr_result)
            code,attr_result=self.cmd("getfattr -m. -e base64 -d attr_testfile")
            self.assertIn('security.selinux=',attr_result)
            self.assertIn('user.foo=0sYmFy',attr_result)
            self.cmd("setfattr -x user.foo attr_testfile")
            
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf attr_testfile")