#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

"""
@File:      tc_attr_ea_fun007.py
@Time:      2024/07/12 11:37
@Author:    wangyang
@Version:   1.0
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_attr_ea_fun007.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "attr"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("touch test && ln -s test test.lnk")

    def test(self):
        code,result=self.cmd('attr -s "oe" -V "top" test | grep top')
        self.assertEquals(0, code, f"common attr -s failed with return code {code}")
        code,result=self.cmd('attr -g "oe" test | grep "top"')
        self.assertEquals(0, code, f"common attr -g failed with return code {code}")
        code,result=self.cmd('attr -l test | grep oe')
        self.assertEquals(0, code, f"common attr -l failed with return code {code}")
        code,result=self.cmd('attr -r oe test')
        self.assertEquals(0, code, f"common attr -r failed with return code {code}")
        code,result=self.cmd('attr -Lq -s "oe" -V "top" test.lnk')
        self.assertEquals(0, code, f"common attr -Lq -s failed with return code {code}")
        code,result=self.cmd('attr -Rq -s "oe" -V "betop" test')
        self.assertEquals(0, code, f"common attr -Rq -s failed with return code {code}")
        code,result=self.cmd('attr -Sq -s "oe" -V "beentop" test')
        self.assertEquals(0, code, f"common attr -Sq -s failed with return code {code}")
        code,result=self.cmd('setfattr -n user.oe -v extra -h test')
        self.assertEquals(0, code, f"common attr -n failed with return code {code}")
        code,result=self.cmd('getfattr -hRLP -n user.oe -d test')
        self.assertEquals(0, code, f"common attr -hRLP failed with return code {code}")
        code,result=self.cmd('setfattr -x user.oe -h test')
        self.assertEquals(0, code, f"common attr -x failed with return code {code}")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -f test.lnk test")
