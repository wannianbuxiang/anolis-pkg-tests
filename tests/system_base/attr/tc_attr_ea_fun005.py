# -*- encoding: utf-8 -*-

"""
@File:      tc_attr_ea_fun005.py
@Time:      2024/03/18 10:12:50
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_attr_ea_fun005.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "attr"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.file = 'attr_testfile'
        self.attribute = 'user.comment'
        self.value = 'Test comment'
        self.linkfile = 'test_link'
        self.cmd(f"echo 'hello' > {self.file}")
        self.cmd(f"ln -s {self.file} {self.linkfile}")

    def test(self):
        self.log.info("Start set extended properties:")
        self.cmd(f"attr -L -s {self.attribute} -V '{self.value}' {self.linkfile}")
        self.log.info("Start query extended properties:")
        code, link_result = self.cmd(f"attr -Lg {self.attribute} {self.linkfile}")
        self.assertIn(f'{self.value}', link_result)
        code, attr_result = self.cmd(f"attr -Lg {self.attribute} {self.file}")
        self.assertIn(f'{self.value}', attr_result)
        code, link_result = self.cmd(f"attr -lLq {self.linkfile}")
        self.assertIn(f'{self.attribute}', link_result)
        code, attr_result = self.cmd(f"attr -lLq {self.file}")
        self.assertEqual(attr_result, link_result)
        self.log.info("Start cleaning up extended properties:")
        self.cmd(f"attr -L -r {self.attribute} {self.linkfile}")
        code, link_result = self.cmd(f"attr -lLq {self.linkfile}")
        self.assertNotIn(f'{self.attribute}', link_result)
        code, link_result = self.cmd(f"attr -lLq {self.file}")
        self.assertNotIn(f'{self.attribute}', link_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f"rm -rf {self.file} {self.linkfile}")