# -*- encoding: utf-8 -*-

"""
@File:      tc_storage_repair_xfs.py
@Time:      2024/06/04 17:34:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest
from common.disk import DiskManager

class Test(LocalTest):
    """
    See tc_storage_repair_xfs for details

    :avocado: tags=P1,noarch,local,xfsprogs
    """
    PARAM_DIC = {"pkg_name": "xfsprogs"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.data_disk = DiskManager.get_data_disk(self)
        if self.data_disk is None:
            self.skip('There is no data disk for test.')
        self.data_disk = "/dev/%s" % self.data_disk
        if "nvme" in self.data_disk:
            self.idle_block = self.data_disk + "p1"
        else:
            self.idle_block = self.data_disk + "1"
        self.cmd(f"echo -e 'n\\np\\n1\\n\\n+300M\\np\\nw\\n' | fdisk {self.data_disk}")
        

    def test(self):
        code, xfsprogs_ls1_result=self.cmd(f"ls {self.idle_block}")
        self.assertIn(f"{self.idle_block}", xfsprogs_ls1_result)
        self.cmd(f"mkfs.xfs -f {self.idle_block}")
        code, xfsprogs_ls2_result=self.cmd(f"xfs_repair -n {self.idle_block}")
        self.assertIn("No modify flag set", xfsprogs_ls2_result)
        print("xfs_repair 成功执行")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        DiskManager.delete_part(self, self.data_disk)      


