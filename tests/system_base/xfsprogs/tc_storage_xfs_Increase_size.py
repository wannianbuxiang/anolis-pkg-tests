# -*- encoding: utf-8 -*-

"""
@File:      tc_storage_xfs_Increase_size.py
@Time:      2024/06/04 17:34:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest
from common.disk import DiskManager

class Test(LocalTest):
    """
    See tc_storage_xfs_Increase_size for details

    :avocado: tags=P1,noarch,local,xfsprogs
    """
    PARAM_DIC = {"pkg_name": "xfsprogs lvm2"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.data_disk = DiskManager.get_data_disk(self)
        if self.data_disk is None:
            self.skip('There is no data disk for test.')
        self.data_disk = "/dev/%s" % self.data_disk
        if "nvme" in self.data_disk:
            self.data_disk_part1 = self.data_disk + "p1"
            self.data_disk_part2 = self.data_disk + "p2"
        else:
            self.data_disk_part1 = self.data_disk + "1"
            self.data_disk_part2 = self.data_disk + "2"
        self.cmd(f"echo -e 'n\\np\\n\\n\\n+400M\\nn\\np\\n\\n\\n+40M\\nw\\n' | fdisk {self.data_disk}")
        
    def test(self):
        self.cmd("mkdir /home/pv")
        self.cmd(f"pvcreate '{self.data_disk_part1}' -y")
        self.cmd(f"vgcreate test '{self.data_disk_part1}' -y")
        # 在某些系统上，mkfs.xfs最小需要300MiB
        self.cmd("lvcreate -L 300MiB -n lv1 test -y")
        self.cmd("mkfs.xfs -f /dev/mapper/test-lv1")
        self.cmd("mount /dev/mapper/test-lv1 /home/pv")
        self.cmd(f"pvcreate '{self.data_disk_part2}' -y")
        self.cmd(f"vgextend test '{self.data_disk_part2}' -y")
        self.cmd(f"lvextend -L +10MiB /dev/mapper/test-lv1 '{self.data_disk_part2}' -y")
        code, xfsprogs_ls1_result=self.cmd("xfs_info /home/pv")
        self.assertIn("meta-data=/dev/mapper/test-lv1", xfsprogs_ls1_result)
        self.cmd(f"lvextend -L +10MiB /dev/mapper/test-lv1 '{self.data_disk_part2}' -y")
        # 原有的block size为300*1024*1024/4096=76800, 将它扩大到76900
        code, xfsprogs_ls2_result=self.cmd("xfs_growfs /home/pv -D 76900 | grep 'blocks changed from 76800 to 76900'")
        self.assertIn("blocks changed from 76800 to 76900", xfsprogs_ls2_result)
        # 检查从76900扩展
        code, xfsprogs_ls3_result=self.cmd("xfs_growfs /home/pv | grep 'data blocks changed from 76900 to'")
        self.assertIn("data blocks changed from 76900 to", xfsprogs_ls3_result)

    def tearDown(self):
        self.cmd("umount /home/pv")
        self.cmd("lvremove /dev/test/lv1 -y")
        self.cmd("vgremove /dev/test -y")
        self.cmd(f"pvremove '{self.data_disk_part1}' '{self.data_disk_part2}'")
        self.cmd("rm -rf /home/pv")
        DiskManager.delete_part(self, self.data_disk)
        DiskManager.delete_part(self, self.data_disk)
        super().tearDown(self.PARAM_DIC)
