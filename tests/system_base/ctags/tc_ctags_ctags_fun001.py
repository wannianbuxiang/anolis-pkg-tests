# -*- encoding: utf-8 -*-

"""
@File:      tc_ctags_ctags_fun001.py
@Time:      2024/5/24 16:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ctags_ctags_fun001.yaml for details

    :avocado: tags=P1,noarch,local,ctags
    """
    PARAM_DIC = {"pkg_name": "ctags"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):        	
        code, result = self.cmd("ctags --list-languages")
        self.assertIn("Ant", result)
        code, result = self.cmd("ctags --list-maps")
        self.assertIn("Java     *.java", result)
        code, result = self.cmd("ctags --list-kinds")
        self.assertIn("PHP", result)
        code, result = self.cmd("ctags --list-kinds=Python")
        self.assertIn("c  classes", result)
    
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
