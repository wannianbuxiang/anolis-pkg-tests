# -*- encoding: utf-8 -*-

"""
@File:      tc_lz4_lz4_fun001.py
@Time:      2023/1/15 21:57:12
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_lz4_lz4_fun001.yaml for details

    :avocado: tags=P1,noarch,local,lz4
    """
    PARAM_DIC = {"pkg_name": "lz4"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("yum install -y lz4")
        self.cmd("touch lz4_testfile")
        self.cmd("echo 'lz4 test' > lz4_testfile")

    def test(self):
        self.cmd("lz4 lz4_testfile > lz4_testfile.lz4")
        code,lz4_result=self.cmd("ls -l")
        self.assertIn("lz4_testfile.lz4",lz4_result)
        self.cmd("rm -rf lz4_testfile")
        self.cmd("lz4 -d lz4_testfile.lz4 > lz4_testfile")
        code,lz4_result=self.cmd("cat lz4_testfile")
        self.assertEquals("lz4 test",lz4_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -f lz4_testfile lz4_testfile.lz4")