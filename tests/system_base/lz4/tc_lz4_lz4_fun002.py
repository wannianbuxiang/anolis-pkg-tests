# -*- encoding: utf-8 -*-

"""
@File:      tc_lz4_lz4_fun002.py
@Time:      2024/10/30 17:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_lz4_lz4_fun002.yaml for details

    :avocado: tags=P1,noarch,local,lz4
    """
    PARAM_DIC = {"pkg_name": "lz4"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("dd if=/dev/zero of=big_file bs=1M count=10")
    	
    def test(self): 
        self.cmd("lz4 big_file big_file.lz4")
        code, result = self.cmd("ls -l")
        self.assertIn("big_file.lz4", result)
     
        self.cmd("lz4 -f big_file big_file.lz4 > output.log 2>&1")
        code, result = self.cmd("cat output.log")
        self.assertIn("Compressed", result)        

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -f big_file.lz4")
        self.cmd("rm -f big_file")
        self.cmd("rm -f output.log")