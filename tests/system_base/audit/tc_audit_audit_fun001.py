# -*- encoding: utf-8 -*-

"""
@File:      tc_audit_audit_fun001.py
@Time:      2024/05/30 16:51:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_audit_audit_fun001.yaml for details

    :avocado: tags=P1,noarch,local,audit
    """
    PARAM_DIC = {"pkg_name": "audit"}

    def setUp(self):
       super().setUp(self.PARAM_DIC)
              	  	 
    def test(self):
       self.cmd("auditctl -D")
       self.cmd("auditctl -w /etc/ssh/sshd_config -p warx -k sshd_config")
       self.cmd("auditctl -l")
                    	 
    def tearDown(self):
       super().tearDown(self.PARAM_DIC)		
       	
