# -*- encoding: utf-8 -*-

"""
@File:      tc_sed_sed_fun_002.py
@Time:      2024/07/05 10:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_sed_sed_fun_002.yaml for details

    :avocado: tags=P1,noarch,local,sed
    """
    PARAM_DIC = {"pkg_name": "sed"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat>test_sed<<EOF    
aaaa
bbbb
cccc
dddd
EOF"""
        self.cmd(cmdline)

    def test(self):        	
        code, result1=self.cmd("sed --v | grep sed")
        self.assertIn("sed", result1)
        self.cmd("sed -i '1i testline' test_sed")
        code, result2=self.cmd("grep testline test_sed")
        self.assertIn("testline", result2)
        self.cmd("sed -i '1d' test_sed")
        code, result3=self.cmd("sed -n '1p' test_sed")
        self.assertIn("aaaa", result3)
        code, result4=self.cmd("sed -n '2,+3p' test_sed")
        self.assertIn("bbbb", result4)
        self.assertIn("cccc", result4)
        self.assertIn("dddd", result4)
        self.cmd("sed -i 's/aaaa/eeee/' test_sed")
        code, result5=self.cmd("grep eeee test_sed")
        self.assertIn("eeee", result5)
        self.cmd("sed -i '4a\\ffff' test_sed")
        code, result6=self.cmd("grep ffff test_sed")
        self.assertIn("ffff", result6)
        self.cmd("sed -i '1c hello' test_sed")
        code, result7=self.cmd("cat test_sed")
        self.assertNotIn('eeee', result7)
        self.cmd("sed -i '1,5d' test_sed")
        code, result8=self.cmd("cat test_sed")
        self.assertNotIn('hello', result8)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf test_sed")
