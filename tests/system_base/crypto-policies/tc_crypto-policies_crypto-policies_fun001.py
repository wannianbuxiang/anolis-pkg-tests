# -*- encoding: utf-8 -*-

"""
@File:      tc_crypto-policies_crypto-policies_fun001.py
@Time:      2024/8/5 16:50:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_crypto-policies_crypto-policies_fun001.yaml for details

    :avocado: tags=P1,noarch,local,crypto-policies
    """
    PARAM_DIC = {"pkg_name": "crypto-policies openssl"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)       

    def test(self):   
        code, result = self.cmd("update-crypto-policies --show")
        self.assertIn("DEFAULT", result) 
        code, result = self.cmd("openssl s_client -connect GitHub.com:443 -tls1_3")
        self.assertIn("Verification: OK", result)
        code, result = self.cmd("openssl s_client -connect GitHub.com:443 -tls1_2")
        self.assertIn("Verification: OK", result)
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)

       