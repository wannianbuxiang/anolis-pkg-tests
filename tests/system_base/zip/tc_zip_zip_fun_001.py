# -*- encoding: utf-8 -*-

"""
@File:      tc_zip_zip_fun_001.py
@Time:      2023/3/13 17:34:20
@Author:    liuyafei
@Version:   1.0
@Contact:   liuyafei@uniontech.com
@License:   Mulan PSL v2
@Modify:    liuyafei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_zip_zip_fun_001 for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "zip"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("touch sys_zip_testfile")
        self.cmd("echo 'zip test' > sys_zip_testfile")

    def test(self):
        self.cmd("zip sys_zip_testfile.zip sys_zip_testfile")
        code,zip_ls_result=self.cmd("ls")
        self.assertIn("sys_zip_testfile.zip",zip_ls_result)
        self.cmd("unzip -f sys_zip_testfile.zip")
        code,result=self.cmd("cat sys_zip_testfile")
        self.assertIn("zip test",result)         

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -f sys_zip_testfile sys_zip_testfile.zip")

