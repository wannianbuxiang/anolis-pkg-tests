# -*- encoding: utf-8 -*-

"""
@File:      tc_grub2_mkconfig_fun001.py
@Time:      2024/06/3 17:31:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_grub2_mkconfig_fun001.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "grub2-tools"}

    def setUp(self):
       super().setUp(self.PARAM_DIC)
              	  	 
    def test(self):
      ret_c, _ = self.cmd("cat /etc/os-release | grep -i an23", ignore_status=True)
      if ret_c == 0:
          self.skip("an23 does not support filtering crashkernel")
      else:
          self.cmd("grub2-mkconfig -o tmp_grub_cfg")
          self.cmd("grep crashkernel tmp_grub_cfg >testlog1")
          self.cmd("grep crashkernel /boot/grub2/grub.cfg >testlog2")
          self.cmd("diff testlog1 testlog2|| grep -f testlog1 testlog2")  
                               	 
    def tearDown(self):
       super().tearDown(self.PARAM_DIC)
       self.cmd("rm -rf tmp_grub_cfg testlog1 testlog2")