# -*- encoding: utf-8 -*-

"""
@File:      tc_snappy_snappy_fun001.py
@Time:      2024/11/26 17:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_snappy_snappy_fun001.yaml for details

    :avocado: tags=P1,noarch,local,snappy
    """
    PARAM_DIC = {"pkg_name": "gcc-c++ snappy-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > example.cpp << EOF
#include <iostream>
#include <string>
#include <snappy.h>

int main() {
    std::string original = "Hello, Snappy!";
    std::string compressed;
    std::string uncompressed;

    // yasuo
    snappy::Compress(original.data(), original.size(), &compressed);
    std::cout << "Compressed size: " << compressed.size() << std::endl;

    // jieyasuo
    if (snappy::Uncompress(compressed.data(), compressed.size(), &uncompressed)) {
        std::cout << "Uncompressed: " << uncompressed << std::endl;
    } else {
        std::cerr << "Failed to uncompress!" << std::endl;
    }

    return 0;
}
EOF"""

        self.cmd(cmdline)

    def test(self):        	
        self.cmd("g++ example.cpp -o example -lsnappy")
        code,result = self.cmd("./example")
        self.assertIn("Compressed size: 16\nUncompressed: Hello, Snappy!", result)
 
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf example.cpp example")