# -*- encoding: utf-8 -*-

"""
@File:      tc_curl_curl_fun001.py
@Time:      2024/6/25 17:00:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_curl_curl_fun001.yaml for details

    :avocado: tags=P1,noarch,local,curl
    """
    PARAM_DIC = {"pkg_name": "curl"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        
    def test(self):        	
        code, result = self.cmd("curl --version")
        self.assertIn("curl", result)
        self.cmd("curl --help > curl.log", ignore_status=True)
        code, result = self.cmd("cat curl.log", ignore_status=True)
        self.assertIn("Usage:", result)
      
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf curl.log")
