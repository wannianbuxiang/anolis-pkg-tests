# -*- encoding: utf-8 -*-

"""
@File:      tc_curl_curl_fun002.py
@Time:      2024/06/25 16:51:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_curl_curl_fun002.yaml for details

    :avocado: tags=P1,noarch,local,curl
    """
    PARAM_DIC = {"pkg_name": "curl"}

    def setUp(self):
       super().setUp(self.PARAM_DIC)
              	  	 
    def test(self):
       self.cmd("curl http://www.baidu.com")
       self.cmd("curl -X POST -d param=value https://www.baidu.com")
       self.cmd("curl -L https://www.baidu.com/redirect")
       self.cmd("curl https://www.baidu.com -o output.txt")
       self.cmd("curl -v www.baidu.com")
       self.cmd("curl -v https://www.baidu.com")
       code, result=self.cmd("curl -I https://www.baidu.com")
       self.assertIn("Accept-Ranges", result)
       	 
    def tearDown(self):
       super().tearDown(self.PARAM_DIC)		
       self.cmd(" rm  -rf output.txt")	
