# -*- encoding: utf-8 -*-

"""
@File:      tc_man-db_accessdb_fun_001.py
@Time:      2024/8/16 10:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_man-db_accessdb_fun_001.yaml for details

    :avocado: tags=P1,noarch,local,man-db
    """
    PARAM_DIC = {"pkg_name": "man-db"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        self.cmd("mandb")
        code, result1=self.cmd("accessdb --debug | grep accessdb")
        self.assertIn("accessdb", result1)
        code, result2=self.cmd("accessdb --help|grep accessdb")
        self.assertIn("accessdb", result2)
        code, result3=self.cmd("accessdb --usage|grep accessdb")
        self.assertIn("accessdb", result3)
        code, result4=self.cmd("accessdb --version|grep accessdb")
        self.assertIn("accessdb", result4)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
