"""
@File:      tc_mc_mcdiff_fun001.py
@Time:      2024/7/23 17:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""
import os
import subprocess

from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_mc_mcdiff_fun001.yaml for details

    :avocado: tags=P1,noarch,local,mc
    """
    PARAM_DIC = {"pkg_name": "mc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code,self.local_LANG=self.cmd('echo $LANG')
        os.environ['LANG'] = 'en_US.UTF-8'
        self.cmd('echo $LANG')


    def test(self):
        code, result = self.cmd("mcdiff -h 2>&1 | grep 'help'")
        self.assertFalse(code, "Check mcdiff -h failed")
        code, result = self.cmd("mcdiff --help 2>&1 | grep 'help'")
        self.assertFalse(code, "Check mcdiff --help failed")
        code, result = self.cmd("mcdiff --help-all 2>&1 | grep 'help'")
        self.assertFalse(code, "Check mcdiff --help-all failed")
        code, result = self.cmd("mcdiff --help-terminal 2>&1 | grep mcdiff")
        self.assertFalse(code, "Check mcdiff --help-terminal failed")
        code, result = self.cmd(" mcdiff --help-color 2>&1 | grep 'black,'")
        self.assertFalse(code, "Check mcdiff --help-color failed")
        code, result = self.cmd("mcdiff -V 2>&1 | grep 'GNU Midnight'")
        self.assertFalse(code, "Check mcdiff -V  failed")
        code, result = self.cmd(" mcdiff --version 2>&1 | grep 'GNU Midnight '")
        self.assertFalse(code, "Check mcdiff --version  failed")
        code, result = self.cmd("mcdiff -F 2>&1 | grep 'Home'")
        self.assertFalse(code, "Check mcdiff -F  failed")
        code, result = self.cmd("mcdiff --datadir-info 2>&1 | grep 'Home'")
        self.assertFalse(code, "Check mcdiff --datadir-info  failed")
        code, result = self.cmd(' mcdiff --configure-options 2>&1 | grep "\'--build="')
        self.assertFalse(code, "Check mcdiff --configure-options  failed")
    def tearDown(self):
        self.cmd(f'export LANG={self.local_LANG}')
        super().tearDown(self.PARAM_DIC)
