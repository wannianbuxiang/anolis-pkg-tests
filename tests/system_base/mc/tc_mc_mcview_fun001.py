"""
@File:      tc_mc_mcview_fun001.py
@Time:      2024/7/29 17:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""
import os
import subprocess

from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_mc_mcview_fun001.yaml for details

    :avocado: tags=P1,noarch,local,mc
    """
    PARAM_DIC = {"pkg_name": "mc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code,self.local_LANG=self.cmd('echo $LANG')
        os.environ['LANG'] = 'en_US.UTF-8'
        self.cmd('echo $LANG')


    def test(self):
        code, result = self.cmd("mcview --help 2>&1 | grep 'mcview'")
        self.assertFalse(code, "Check mcview --help failed")
        code, result = self.cmd("mcview -h 2>&1 | grep 'mcview'")
        self.assertFalse(code, "Check mcview -h failed")
        code, result = self.cmd("mcview --help-all 2>&1 | grep 'mcview'")
        self.assertFalse(code, "Check mcview --help-all failed")
        code, result = self.cmd("mcview --help-terminal 2>&1 | grep 'GNU Midnight'")
        self.assertFalse(code, "Check mcview --help-terminal failed")
        code, result = self.cmd("mcview --help-color 2>&1 | grep 'black'")
        self.assertFalse(code, "Check mcview --help-color failed")
        code, result = self.cmd("mcview -V 2>&1 | grep 'GNU Midnight Commander '")
        self.assertFalse(code, "Check mcview -V failed")
        code, result = self.cmd("mcview --version 2>&1 | grep 'GNU Midnight Commander '")
        self.assertFalse(code, "Check mcview --version failed")
        code, result = self.cmd("mcview -F 2>&1 | grep 'Home directory'")
        self.assertFalse(code, "Check mcview -F failed")
        code, result = self.cmd(" mcview --datadir-info 2>&1 | grep 'Home'")
        self.assertFalse(code, "Check mcview --datadir-info failed")
        code, result = self.cmd('mcview --configure-options 2>&1 | grep "\'--build="')
        self.assertFalse(code, "Check mcview --configure-options  failed")
    def tearDown(self):
        self.cmd(f'export LANG={self.local_LANG}')
        super().tearDown(self.PARAM_DIC)
