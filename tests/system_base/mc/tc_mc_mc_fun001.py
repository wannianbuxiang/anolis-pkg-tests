"""
@File:      tc_mc_mc_fun001.py
@Time:      2024/7/23 17:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""
import os
import subprocess

from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_mc_mc_fun001.yaml for details

    :avocado: tags=P1,noarch,local,mc
    """
    PARAM_DIC = {"pkg_name": "mc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code,self.local_LANG=self.cmd('echo $LANG')
        os.environ['LANG'] = 'en_US.UTF-8'
        self.cmd('echo $LANG')


    def test(self):
        code, result = self.cmd("mc -h 2>&1 | grep '--help'")
        self.assertFalse(code, "Check mc -h failed")
        code, result = self.cmd("mc --help 2>&1 | grep '--help'")
        self.assertFalse(code, "Check mc --help failed")
        code, result = self.cmd("mc -V 2>&1 | grep 'GNU Midnight Commander'")
        self.assertFalse(code, "Check mc -V  failed")
        code, result = self.cmd(" mc --version 2>&1 | grep 'GNU Midnight Commander'")
        self.assertFalse(code, "Check mc --version  failed")
        code, result = self.cmd("mc -f 2>&1 | grep '/etc/mc/ (/usr/share/mc/)'")
        self.assertFalse(code, "Check mc -f  failed")
        code, result = self.cmd(" mc --datadir 2>&1 | grep '/etc/mc/ (/usr/share/mc/)'")
        self.assertFalse(code, "Check mc --datadir  failed")
        code, result = self.cmd(" mc -F 2>&1 | grep 'Home directory'")
        self.assertFalse(code, "Check mc -F  failed")
        code, result = self.cmd("mc --datadir-info 2>&1 | grep 'Home'")
        self.assertFalse(code, "Check mc --datadir-info  failed")
        code, result = self.cmd('mc --configure-options 2>&1 | grep "\'--build="')
        self.assertFalse(code, "Check mc --configure-options  failed")
    def tearDown(self):
        self.cmd(f'export LANG={self.local_LANG}')
        super().tearDown(self.PARAM_DIC)