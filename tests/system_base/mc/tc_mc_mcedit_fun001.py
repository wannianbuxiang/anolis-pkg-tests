"""
@File:      tc_mc_mcedit_fun001.py
@Time:      2024/7/29 17:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""
import os
import subprocess

from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_mc_mcedit_fun001.yaml for details

    :avocado: tags=P1,noarch,local,mc
    """
    PARAM_DIC = {"pkg_name": "mc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code,self.local_LANG=self.cmd('echo $LANG')
        os.environ['LANG'] = 'en_US.UTF-8'
        self.cmd('echo $LANG')


    def test(self):
        code, result = self.cmd("mcedit --help 2>&1 | grep 'mcedit'")
        self.assertFalse(code, "Check mcedit --help failed")
        code, result = self.cmd("mcedit -h 2>&1 | grep 'mcedit'")
        self.assertFalse(code, "Check mcedit -h failed")
        code, result = self.cmd("mcedit --help-all 2>&1 | grep 'mcedit'")
        self.assertFalse(code, "Check mcedit --help-all failed")
        code, result = self.cmd("mcedit --help-terminal 2>&1 | grep 'GNU Midnight'")
        self.assertFalse(code, "Check mcedit --help-terminal failed")
        code, result = self.cmd("mcedit --help-color 2>&1 | grep 'black'")
        self.assertFalse(code, "Check mcedit --help-color failed")
        code, result = self.cmd("mcedit -V 2>&1 | grep 'GNU Midnight Commander '")
        self.assertFalse(code, "Check mcedit -V failed")
        code, result = self.cmd("mcedit --version 2>&1 | grep 'GNU Midnight Commander '")
        self.assertFalse(code, "Check mcedit --version failed")
        code, result = self.cmd("mcedit -F 2>&1 | grep 'Home directory'")
        self.assertFalse(code, "Check mcedit -F failed")
        code, result = self.cmd(" mcedit --datadir-info 2>&1 | grep 'Home'")
        self.assertFalse(code, "Check mcedit --datadir-info failed")
        code, result = self.cmd('mcedit --configure-options 2>&1 | grep "\'--build="')
        self.assertFalse(code, "Check mcedit --configure-options  failed")
    def tearDown(self):
        self.cmd(f'export LANG={self.local_LANG}')
        super().tearDown(self.PARAM_DIC)
