# -*- encoding: utf-8 -*-

"""
@File:      tc_gnutls_gnutls_fun002.py
@Time:      2024/7/26 15:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_gnutls_gnutls_fun002.yaml for details

    :avocado: tags=P1,noarch,local,gnutls
    """
    PARAM_DIC = {"pkg_name": "gnutls-utils"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):     
        self.cmd("rpm -qa | grep gnutls")
        code, result = self.cmd("gnutls-cli  www.baidu.com")
        self.assertIn("Processed 3", result)
        code, result = self.cmd("gnutls-cli  --insecure www.baidu.com")
        self.assertIn("Processed 0", result)
        self.cmd("gnutls-cli --debug=99 www.baidu.com:443")
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
