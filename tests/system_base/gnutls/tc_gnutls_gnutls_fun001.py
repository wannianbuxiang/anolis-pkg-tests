# -*- encoding: utf-8 -*-

"""
@File:      tc_gnutls_gnutls_fun001.py
@Time:      2024/7/26 17:00:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_gnutls_gnutls_fun001.yaml for details

    :avocado: tags=P1,noarch,local,gnutls
    """
    PARAM_DIC = {"pkg_name": "gnutls-utils"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        
    def test(self):        	
        code, result = self.cmd("gnutls-cli --version")
        self.assertIn("gnutls-cli", result)
        self.cmd("gnutls-cli --help > /tmp/gnutls.log", ignore_status=True)
        code, result = self.cmd("cat /tmp/gnutls.log", ignore_status=True)
        self.assertIn("Usage:", result)
      
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/gnutls.log")
