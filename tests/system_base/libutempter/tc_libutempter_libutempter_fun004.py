# -*- encoding: utf-8 -*-

"""
@File:      tc_libutempter_libutempter_fun004.py
@Time:      2024/3/8 15:25:20
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libutempter_libutempter_fun004.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libutempter libutempter-devel gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >libutempter_testfile.c<<"EOF"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/resource.h>
#include <utempter.h>

#define NUM_PROCESSES 10

int main() {
    pid_t pids[NUM_PROCESSES];
    int i;

    for (i = 0; i < NUM_PROCESSES; i++) {
        pids[i] = fork();
        if (pids[i] < 0) {
            perror("fork failed");
            exit(EXIT_FAILURE);
        } else if (pids[i] == 0) { // Child process
            char username[32];
            sprintf(username, "testuser%d", i);
            if (utempter_add_record(STDIN_FILENO, username) != 0) {
                fprintf(stderr, "Failed to add utmp record for user %s\\n", username);
                exit(EXIT_FAILURE);
            }
            sleep(1); // Simulate some work
            if (utempter_remove_added_record() != 0) {
                fprintf(stderr, "Failed to remove utmp record for user %s\\n", username);
                exit(EXIT_FAILURE);
            }
            exit(EXIT_SUCCESS);
        }
    }

    // Wait for all children to finish
    for (i = 0; i < NUM_PROCESSES; i++) {
        int status;
        waitpid(pids[i], &status, 0);
        if (status != 0) {
            printf("Process %d exited with error status.\\n", pids[i]);
        }
    }

    printf("All processes finished.\\n");
    return 0;
}
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o libutempter_testfile libutempter_testfile.c -lutempter")
        code, result = self.cmd("./libutempter_testfile")
        self.assertEqual("All processes finished.", result, msg="多线程场景失败！")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf libutempter_testfile*")