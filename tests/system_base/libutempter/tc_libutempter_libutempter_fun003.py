# -*- encoding: utf-8 -*-

"""
@File:      tc_libutempter_libutempter_fun003.py
@Time:      2024/3/8 15:25:20
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libutempter_libutempter_fun003.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libutempter libutempter-devel gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >libutempter_testfile.c<<"EOF"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include<fcntl.h>
#include <sys/resource.h>
#include <utempter.h>

int main() {
    struct rlimit rl;
    int result = getrlimit(RLIMIT_NOFILE, &rl);
    if (result != 0) {
        perror("Failed to get file descriptor limit");
        return 1;
    }

    // 设置软限制为当前打开文件描述符数量
    rl.rlim_cur = rl.rlim_max;
    setrlimit(RLIMIT_NOFILE, &rl);

    // 尝试打开文件直到达到限制
    while (open("/dev/null", O_RDONLY) != -1) {}

    // 测试添加记录时的行为
    if (utempter_add_record(STDIN_FILENO, "testuser") == 0) {
        fprintf(stderr, "Unexpected success when resource limits are reached\\n");
    } else {
        printf("Correctly handled situation when resource limits are reached\\n");
    }

    return 0;
}
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o libutempter_testfile libutempter_testfile.c -lutempter")
        code, result = self.cmd("./libutempter_testfile")
        self.assertTrue(code == 0, msg="测试资源受限场景失败！")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf libutempter_testfile*")