# -*- encoding: utf-8 -*-

"""
@File:      tc_libutempter_libutempter_fun002.py
@Time:      2024/3/8 15:25:20
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libutempter_libutempter_fun002.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libutempter libutempter-devel gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >libutempter_testfile.c<<"EOF"
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <utempter.h>

int main() {
    // 测试提供无效的文件描述符
    if (utempter_add_record(-1, "testuser") == 0) {
        fprintf(stderr, "Unexpected success with invalid file descriptor\\n");
    } else {
        printf("Correctly handled invalid file descriptor\\n");
    }

    // 测试提供NULL作为用户名
    if (utempter_add_record(STDIN_FILENO, NULL) == 0) {
        fprintf(stderr, "Unexpected success with NULL username\\n");
    } else {
        printf("Correctly handled NULL username\\n");
    }

    // 测试提供空字符串作为用户名
    if (utempter_add_record(STDIN_FILENO, "") == 0) {
        fprintf(stderr, "Unexpected success with empty username\\n");
    } else {
        printf("Correctly handled empty username\\n");
    }

    return 0;
}
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o libutempter_testfile libutempter_testfile.c -lutempter")
        code, result = self.cmd("./libutempter_testfile")
        self.assertTrue(code == 0, msg="无效参数验证失败！")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf libutempter_testfile*")