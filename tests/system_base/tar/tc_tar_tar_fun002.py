# -*- encoding: utf-8 -*-

"""
@File:      tc_tar_tar_fun002.py
@Time:      2023/1/12 10:30:20
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_tar_tar_fun002.yaml for details

    :avocado: tags=P1,noarch,local,tar
    """
    PARAM_DIC = {"pkg_name": "tar"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("yum install -y tar")
        self.cmd("mkdir tar_testdir")
        self.cmd("touch tar_testdir/tar_testfile")
        self.cmd("echo 'tar test' > tar_testdir/tar_testfile")

    def test(self):
        self.cmd("tar -cvf tar_test.tar ./tar_testdir")
        self.cmd("rm -rf tar_testdir")
        self.cmd("tar -xvf tar_test.tar ./tar_testdir")
        code,result=self.cmd("cat tar_testdir/tar_testfile")
        self.assertEquals("tar test",result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf tar_test.tar tar_testdir")