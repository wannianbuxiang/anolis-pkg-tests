# -*- encoding: utf-8 -*-

"""
@File:      tc_boost_boost_fun001.py
@Time:      2024/12/04 17:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_boost_boost_fun001.yaml for details

    :avocado: tags=P1,noarch,local,boost
    """
    PARAM_DIC = {"pkg_name": "gcc-c++ boost-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > boost.cpp << EOF

#include <iostream>
#include <boost/multiprecision/cpp_dec_float.hpp>

using namespace std;
using namespace boost::multiprecision;

int main() {
    // dingyigaojingdufudianshuleixing
    typedef cpp_dec_float_50 float50; 

    // chushihuagaojingdufudianshu
    float50 a("1234567890.1234567890");
    float50 b("9876543210.9876543210");

    // jingxingjibenyunsuan
    // jiafa
    float50 sum = a + b;
    // jianfa          
    float50 difference = a - b;
    // chengfa   
    float50 product = a * b;  
   // chufa    
    float50 quotient = a / b;     

    // shezhishuchuweigudinggeshi,bingbaoliu3weixiaoshu
    cout << fixed << setprecision(3); 

    // shuchujieguo
    cout << "Sum: " << sum << endl;
    cout << "Difference: " << difference << endl;
    cout << "Product: " << product << endl;
    cout << "Quotient: " << quotient << endl;

    return 0;
}
EOF"""

        self.cmd(cmdline)

    def test(self):        	
        self.cmd(" g++ -o boost boost.cpp -lboost_system")
        code,result = self.cmd("./boost")
        self.assertIn("Sum: 11111111101.111", result)
        code,result = self.cmd("./boost")
        self.assertIn("Difference: -8641975320.864", result)
        code,result = self.cmd("./boost")
        self.assertIn("Product: 12193263113702179522.375", result)
        code,result = self.cmd("./boost")
        self.assertIn("Quotient: 0.125", result)
 
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf boost.cpp boost")