#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libyaml_fun_002.py
@Time:      2024/04/18 14:05
@Author:    zhanggaiyan
@Version:   1.0
@Contact:   zhanggaiyan@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhanggaiyan
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libyaml_fun_002.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libyaml libyaml-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline1 = """cat > yaml_fail.c <<EOF
#include <stdio.h>
#include <yaml.h>
#include <errno.h>

int main() {
    FILE *fp = fopen("illegal_example.yaml", "r");
    if (!fp) {
        fprintf(stderr, "Failed to open test.yaml: %s\\n", strerror(errno));
        return 1;
    }

    yaml_parser_t parser;
    yaml_event_t event;

    if (!yaml_parser_initialize(&parser)) {
        fprintf(stderr, "Failed to initialize YAML parser\\n");
        fclose(fp);
        return 1;
    }

    yaml_parser_set_input_file(&parser, fp);

    int parsing_successful = 1;
    while (parsing_successful && yaml_parser_parse(&parser, &event)) {
        switch (event.type) {
            case YAML_STREAM_START_EVENT:
                printf("Stream start detected.\\n");
                break;
            case YAML_DOCUMENT_START_EVENT:
                printf("Document start detected.\\n");
                break;
            case YAML_MAPPING_START_EVENT:
                printf("Mapping start detected.\\n");
                break;
            // ... handle other event types ...
            case YAML_STREAM_END_EVENT:
                printf("Stream end detected. Parsing successful.\\n");
                break;
            default:
                printf("Unexpected event type: %d\\n", event.type);
                parsing_successful = 0;
                break;
        }
        yaml_event_delete(&event);
    }

    if (!parsing_successful) {
        fprintf(stderr, "Error occurred during YAML parsing.\\n");
    }

    yaml_parser_delete(&parser);
    fclose(fp);

    return parsing_successful ? 0 : 1;
}
EOF"""

	cmdline2 = """cat > illegal_example.yaml <<EOF
person  //缺少冒号
  name Alice
EOF"""
        self.cmd(cmdline1)
        self.cmd(cmdline2)

    def test(self):
        self.cmd("gcc -o yaml_fail yaml_fail.c -lyaml")
        ret_c,ret_o = self.cmd("./yaml_fail &> yaml.log", ignore_status=True)
        self.log.info(ret_o)
        self.cmd("grep 'Error occurred during YAML parsing' yaml.log")
        self.cmd("grep 'Unexpected event type' yaml.log")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -f illegal_example.yaml test_yaml.c test_yaml yaml.log")
