#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libyaml_fun_001.py
@Time:      2024/04/18 14:05
@Author:    zhanggaiyan
@Version:   1.0
@Contact:   zhanggaiyan@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhanggaiyan
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libyaml_fun_001.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libyaml libyaml-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline1 = """cat > test_yaml.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include <yaml.h>

int main() {
    FILE *yaml_file = fopen("example.yaml", "r");
    if (!yaml_file) {
        fprintf(stderr, "Failed to open YAML file.\\n");
        return EXIT_FAILURE;
    }

    yaml_parser_t parser;
    if (!yaml_parser_initialize(&parser)) {
        fprintf(stderr, "Failed to initialize YAML parser.\\n");
        fclose(yaml_file);
        return EXIT_FAILURE;
    }

    yaml_parser_set_input_file(&parser, yaml_file);

    int done = 0;
    while (!done) {
        yaml_event_t event;
        if (!yaml_parser_parse(&parser, &event)) {
            fprintf(stderr, "Error parsing YAML stream.\\n");
            yaml_event_delete(&event);
            yaml_parser_delete(&parser);
            fclose(yaml_file);
            return EXIT_FAILURE;
        }

        switch (event.type) {
            case YAML_STREAM_START_EVENT:
                printf("Stream start\\n");
                break;
            case YAML_DOCUMENT_START_EVENT:
                printf("Document start\\n");
                break;
            case YAML_MAPPING_START_EVENT:
                printf("Mapping start: ");
                // You can access the mapping's anchor and tag here if needed.
                printf("\\n");
                break;
            case YAML_MAPPING_END_EVENT:
                printf("Mapping end\\n");
                break;
            case YAML_SEQUENCE_START_EVENT:
                printf("Sequence start\\n");
                break;
            case YAML_SEQUENCE_END_EVENT:
                printf("Sequence end\\n");
                break;
            case YAML_SCALAR_EVENT:
                printf("Scalar value: %s\\n", (char *)event.data.scalar.value);
                // You can access scalar's anchor, tag, and style here if needed.
                break;
            case YAML_ALIAS_EVENT:
                printf("Alias: %s\\n", (char *)event.data.alias.anchor);
                break;
            case YAML_DOCUMENT_END_EVENT:
                printf("Document end\\n");
                break;
            case YAML_STREAM_END_EVENT:
                printf("Stream end\\n");
                done = 1;
                break;
            default:
                fprintf(stderr, "Unknown event type %d\\n", event.type);
                break;
        }

        yaml_event_delete(&event);
    }

    yaml_parser_delete(&parser);
    fclose(yaml_file);

    return EXIT_SUCCESS;
}
EOF"""

        cmdline2 = """cat > example.yaml <<EOF
# 这是一个注释
person:
  firstName: Aviel
  lastName: zhang
  age: 30
  # 一个包含多个兴趣的列表
  interests:
    - reading
    - cycling
    - photography
  # 地址信息，使用嵌套字典
  address:
    street: 123 Street
    city: xian
    country: China
EOF"""
        self.cmd(cmdline1)
        self.cmd(cmdline2)

    def test(self):
        self.cmd("gcc -o test_yaml test_yaml.c -lyaml")
        ret_c, ret_o  = self.cmd("./test_yaml")
        self.assertTrue("Aviel" in ret_o, 'check output error')
        self.assertTrue("123 Street" in ret_o, 'check output error')
        self.assertTrue("photography" in ret_o, 'check output error')

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -f example.yaml test_yaml.c test_yaml")
