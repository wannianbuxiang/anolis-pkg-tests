#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_cyrus_sasl_func_001.py
@Time:      2024/04/15 15:00:00
@Author:    liuhaiyang
@Version:   1.0
@Contact:   lhy01509690@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liuhaiyang
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_cyrus_sasl_func_001.yaml for details

    :avocado: tags=fix,P3,noarch,local,
    """
    PARAM_DIC = {"pkg_name": "cyrus-sasl"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        self.cmd("pluginviewer")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        