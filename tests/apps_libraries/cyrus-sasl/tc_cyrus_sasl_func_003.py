#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_cyrus_sasl_func_003.py
@Time:      2024/04/15 15:00:00
@Author:    liuhaiyang
@Version:   1.0
@Contact:   lhy01509690@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liuhaiyang
"""

import pexpect
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_cyrus_sasl_func_003.yaml for details

    :avocado: tags=fix,P3,noarch,local,
    """
    PARAM_DIC = {"pkg_name": "cyrus-sasl cyrus-sasl-devel cyrus-sasl-plain"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        config_file_dict = {
            "sasl-test" : """cat >> /etc/sasl2/test.conf << EOF
pwcheck_method: auxprop
auxprop_plugin: sasldb
mech_list: plain login ntlm cram-md5 digest-md5"""
        }
        self.cmd(config_file_dict["sasl-test"])
        self.cmd("echo 'testpassword' | saslpasswd2 -c -p sasltest")
    
    def test(self):
        _, self.server_pid = self.cmd("nohup sasl2-sample-server -p 9000 -m PLAIN localhost > /dev/null 2>&1 & echo $!")
        child  = pexpect.spawn("sasl2-sample-client -p 9000 -m PLAIN localhost")
        child.expect(["please enter an authentication id:"])
        child.sendline("sasltest")
        child.expect("please enter an authorization id:")
        child.sendline("sasltest")
        child.expect("Password:")
        child.sendline("testpassword")
        ret = child.expect("successful authentication")
        self.assertTrue(ret == 0, "sasl2 connection fail")
        child.expect("closing connection")
        child.close()


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("kill -9 %s" % (self.server_pid))
        self.cmd("rm -rf /etc/sasl2/test.conf")
        