#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_cyrus_sasl_func_002.py
@Time:      2024/04/15 15:00:00
@Author:    liuhaiyang
@Version:   1.0
@Contact:   lhy01509690@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liuhaiyang
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_cyrus_sasl_func_002.yaml for details

    :avocado: tags=fix,P3,noarch,local,
    """
    PARAM_DIC = {"pkg_name": "cyrus-sasl"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code, result = self.cmd(f"id testuser", ignore_status=True)
        if code != 0 and 'no such user' in result:
            self.log.info("用户不存在，正在创建...")
            self.cmd("useradd testuser && echo 'testpassword' | passwd --stdin testuser", ignore_status=True)
        else:
            self.log.info("用户存在，删除旧用户，重新创建测试用户")
            self.cmd(f"pgrep -u  testuser| xargs kill -9", ignore_status=True)
            self.cmd("userdel -r testuser")
            self.cmd("useradd testuser && echo 'testpassword' | passwd --stdin testuser", ignore_status=True)
        self.cmd("systemctl restart saslauthd")
    
    def test(self):
        _, output = self.cmd("systemctl is-active saslauthd")
        self.assertTrue(output == "active", "saslauthd service start failed")
        _, output = self.cmd("testsaslauthd -u testuser -p testpassword -s login")
        self.assertTrue("Success" in output, "authentication failed")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("userdel -r testuser")
        