#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_slang_slsh_func001.py
@Time:      2024/04/15 11:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""
from common.basetest import LocalTest

import subprocess
class Test(LocalTest): 
    """
    See tc_slang_slsh_func001.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "slang slang-slsh"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)

    def test(self):
        self.cmd("slsh --help", ignore_status=True)
        ret_c1, ret_o1 = self.cmd("slsh --version")
        self.assertIn("slsh version", ret_o1)
        self.assertIn("S-Lang version", ret_o1)
        command = "slsh -e 'printf(\"hello\")'"
        ret_o2 = subprocess.run(command, capture_output=True, shell=True, text=True)
        self.log.info(ret_o2)
        self.assertIn("hello", ret_o2.stdout)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)