  #!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_slang_slsh_func005.py
@Time:      2024/04/16 13:42:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""
import subprocess
from common.basetest import LocalTest

class Test(LocalTest): 
    """
    See tc_slang_slsh_func005.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "slang slang-slsh"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_slang.sl <<EOF
define count_lines_in_file (file)
{
   variable fp, line, count;

   fp = fopen (file, "r");    % 以读取模式打开文件
   if (fp == NULL)
     throw OpenError, "$file failed to open"$;

   count = 0;
   while (-1 != fgets (&line, fp))
     count++;

   () = fclose (fp);
   return count;
}

define count_bytes_in_file (file)
{
   variable fp, line, count;

   fp = fopen (file, "r");    
   if (fp == NULL)
     throw OpenError, "$file failed to open"$;

   count = 0;
   while (-1 != fgets (&line, fp))
     count += strlen (line);

   () = fclose (fp);
   return count;
}

variable V1 = count_lines_in_file("/tmp/ghm/test");
print(V1);
variable V2 = count_bytes_in_file("/tmp/ghm/test");
print(V2);
variable V3 = count_bytes_in_file("/tmp/ghm/emptytest");
print(V3);
variable V4 = count_lines_in_file("/tmp/ghm/emptytest");
print(V4);
variable V5 = count_lines_in_file("/tmp/ghm/notexit");
print(V5);
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("echo 'hello\n world' > /tmp/ghm/test")
        self.cmd("touch /tmp/ghm/emptytest")
        self.cmd("wc -m /tmp/ghm/test")
        self.cmd("chmod +x /tmp/ghm/test_slang.sl")
        ret_o1 = subprocess.run("slsh /tmp/ghm/test_slang.sl", capture_output=True, shell=True, text=True)
        self.log.info(ret_o1)
        self.assertIn("2", ret_o1.stdout)
        self.assertIn("13", ret_o1.stdout)
        self.assertIn("0", ret_o1.stdout)
        self.assertIn("failed to open", ret_o1.stderr)
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")