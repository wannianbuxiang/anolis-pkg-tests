  #!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_slang_slsh_func004.py
@Time:      2024/04/16 10:42:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest): 
    """
    See tc_slang_slsh_func004.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "slang slang-slsh"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_slang.sl <<EOF
% 定义初始化数组的函数
define init_array(a) {
    variable i, imax;

    imax = length(a);
    for (i = 0; i < imax; i++) {
        a[i] = 7;
    }
}

%创建并初始化整型数组
variable A = Int_Type[2];
init_array(A);
print(A);

% 创建不同类型的数组
variable D = Double_Type [3];
variable F = Complex_Type [3];
variable G = String_Type [3];
variable H = Ref_Type [4];
print(D);
print(F);

% 创建Any_Type数组并赋值不同类型的数据后取消引用
variable I = Any_Type[3];
I[0] = 1;
I[1] = "string";
I[2] = (1 + 2i);
print(I);

% 数组运算示例
variable X = [0:1*PI:0.1];
variable Y = 2 * sin(X);
print(X);
print(Y);
print("test success");
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("chmod +x /tmp/ghm/test_slang.sl")
        ret_c1, ret_o1 = self.cmd("slsh /tmp/ghm/test_slang.sl")
        self.assertIn("test success", ret_o1)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")