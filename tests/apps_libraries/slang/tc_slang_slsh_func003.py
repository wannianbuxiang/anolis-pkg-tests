  #!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_slang_slsh_func003.py
@Time:      2024/04/16 10:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest): 
    """
    See tc_slang_slsh_func003.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "slang slang-slsh"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_slang.sl <<EOF
define compute_functional_sum (funct)
{
  variable i, s;
  s = 0;
  for (i = 0; i < 10; i++)
   {
      s += (@funct)(i);
   }
  return s;
}

variable sin_sum = compute_functional_sum (&sin);
variable cos_sum = compute_functional_sum (&cos);
print(sin_sum);
print(cos_sum);

define set_xyz (x, y, z)
{
  @x = 1;
  @y = 2;
  @z = 3;
}
variable X, Y, Z;
set_xyz (&X, &Y, &Z);
print(X);
print(Y);
print(Z);
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("chmod +x /tmp/ghm/test_slang.sl")
        ret_c1, ret_o1 = self.cmd("slsh /tmp/ghm/test_slang.sl")
        self.assertIn("1.9552094821073802", ret_o1)
        self.assertIn("0.42162378262054656", ret_o1)
        self.assertIn("1", ret_o1)
        self.assertIn("2", ret_o1)
        self.assertIn("3", ret_o1)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")