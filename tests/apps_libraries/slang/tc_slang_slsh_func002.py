  #!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_slang_slsh_func002.py
@Time:      2024/04/15 15:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

import subprocess
from common.basetest import LocalTest

class Test(LocalTest): 
    """
    See tc_slang_slsh_func002.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "slang slang-slsh"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_slang.sl <<EOF
% declare it for recursion
define factorial ();   
define factorial (n){
    if (n < 2) return 1;
    return n * factorial (n - 1);
}

define concat_3_strings (a, b, c) {
    return a + b + c;
}

variable x, y, z;
x = "Hello, ";
y = "World !";
z = "! How are you?";
variable result = concat_3_strings(x, y, z);
print(factorial (1));
print(factorial (5));
print(result);

"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("chmod +x /tmp/ghm/test_slang.sl")
        ret_c1, ret_o1 = self.cmd("slsh /tmp/ghm/test_slang.sl")
        self.assertIn("1", ret_o1)
        self.assertIn("120", ret_o1)
        self.assertIn("Hello, World !! How are you?", ret_o1)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")