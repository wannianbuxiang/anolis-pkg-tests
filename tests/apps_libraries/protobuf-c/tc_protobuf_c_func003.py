#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_protobuf_c_func003.py
@Time:      2024/03/25 10:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_protobuf_c_func003.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "protobuf-c protobuf-c-devel"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline1 = """cat > /tmp/ghm/my_message.proto <<EOF
syntax = "proto3";
package my_package;
message MyMessage {
    string field1 = 1;
    int32 field2 = 2;
}
"""
        self.cmd(cmdline1)
        cmdline2 = """cat > /tmp/ghm/test_my_message.c <<EOF
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include "my_message.pb-c.h"

int main() {
    // 创建一个MyMessage实例并初始化
    MyPackage__MyMessage msg = MY_PACKAGE__MY_MESSAGE__INIT;
    msg.field1 = strdup("Test Field 1");  // 假设填充字段1
    msg.field2 = 123;                    // 假设填充字段2

    // 计算序列化所需内存大小
    size_t packed_size = my_package__my_message__get_packed_size(&msg);

    if (packed_size == 0) {
        fprintf(stderr, "Failed to calculate packed size.\\n");
        goto cleanup;
    }

    // 分配足够大的缓冲区并序列化，然后模拟复制到较小的缓冲区
    uint8_t *big_buffer = malloc(packed_size);
    if (big_buffer == NULL) {
        perror("Failed to allocate memory for the big buffer.");
        goto cleanup;
    }

    // 序列化到足够大的缓冲区
    if (!my_package__my_message__pack(&msg, big_buffer)) {
        fprintf(stderr, "Failed to serialize to a properly sized buffer.\\n");
        free(big_buffer);
        goto cleanup;
    }

    // 尝试将大缓冲区的内容复制到较小的缓冲区，模拟序列化到不足的缓冲区
    uint8_t *small_buffer = malloc(packed_size - 6);
    if (small_buffer == NULL) {
        perror("Failed to allocate memory for the small buffer.");
        free(big_buffer);
        goto cleanup;
    }
    printf("Expected behavior: Serializing to a buffer smaller than %zu bytes would cause an overflow or unexpected behavior.\\n", packed_size);

    // 在此处释放资源并退出
    free(small_buffer);
    free(big_buffer);
    goto cleanup;

// 清理部分（无需改动）
cleanup:
    free(msg.field1);

    return 0;
}
"""
        self.cmd(cmdline2)
    
    def test(self):
        self.cmd("protoc-c --proto_path=/tmp/ghm/ --c_out=/tmp/ghm/ /tmp/ghm/my_message.proto")
        ret_c1, ret_o1 = self.cmd("ls -l /tmp/ghm/")
        self.assertIn("my_message.pb-c.c", ret_o1)
        self.assertIn("my_message.pb-c.h", ret_o1)
        self.cmd("gcc -o /tmp/ghm/test_my_message /tmp/ghm/test_my_message.c /tmp/ghm/my_message.pb-c.c -lprotobuf-c")
        ret_c2, ret_o2 = self.cmd("/tmp/ghm/test_my_message")
        self.assertIn("Expected behavior: Serializing to a buffer smaller than 16 bytes would cause an overflow or unexpected behavior.", ret_o2)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")