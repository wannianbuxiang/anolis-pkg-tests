#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_protobuf_c_func002.py
@Time:      2024/03/22 16:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_protobuf_c_func002.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "protobuf-c protobuf-c-devel"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline1 = """cat > /tmp/ghm/enums.proto <<EOF
syntax = "proto3";

package MyPackage;

enum Color {
  RED = 0;
  GREEN = 1;
  BLUE = 2;
}
"""
        self.cmd(cmdline1)
        cmdline2 = """cat > /tmp/ghm/my_message.proto <<EOF
syntax = "proto3";

import "enums.proto";

message MyMessage {
  string name = 1;
  oneof custom_data {
    MyPackage.Color favorite_color = 2;
  }
}
"""
        self.cmd(cmdline2)
        cmdline3 = """cat > /tmp/ghm/test_my_message.c <<EOF
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "enums.pb-c.h"
#include "my_message.pb-c.h"

// 自定义的字符串复制函数
static inline char* my_strdup(const char* src) {
    size_t len = strlen(src);
    char* dest = (char*)malloc(len + 1);
    if (dest) {
        memcpy(dest, src, len + 1);
    }
    return dest;
}

int main() {
    // 创建并初始化MyMessage
    MyMessage *message = (MyMessage*) calloc(1, sizeof(MyMessage));
    if (message == NULL) {
        fprintf(stderr, "Failed to allocate memory for MyMessage.\\n");
        return EXIT_FAILURE;
    }

    // 初始化MyMessage结构体
    my_message__init(message);

    // 设置基础字段
    message->name = my_strdup("A Custom Message");
    if (message->name == NULL) {
        fprintf(stderr, "Failed to allocate memory for name field.\\n");
        goto cleanup;
    }

    // 设置oneof字段（模拟扩展）
    message->custom_data_case = MY_MESSAGE__CUSTOM_DATA_FAVORITE_COLOR;
    message->favorite_color = MY_PACKAGE__COLOR__GREEN;

    // 序列化与反序列化
    size_t packed_size = my_message__get_packed_size(message);
    uint8_t *buffer = malloc(packed_size);
    if (!my_message__pack(message, buffer)) {
        fprintf(stderr, "Failed to pack the message.\\n");
        goto cleanup;
    }

    // 反序列化
    MyMessage *parsed_message = my_message__unpack(NULL, packed_size, buffer);
    if (!parsed_message) {
        fprintf(stderr, "Failed to unpack the message.\\n");
        goto cleanup;
    }

    // 验证oneof字段是否正确恢复
    if (parsed_message->custom_data_case == MY_MESSAGE__CUSTOM_DATA_FAVORITE_COLOR) {
        assert(parsed_message->favorite_color == MY_PACKAGE__COLOR__GREEN);
    } else {
        fprintf(stderr, "Favorite color was not correctly deserialized.\\n");
        goto cleanup;
    }

cleanup:
    // 清理资源
    my_message__free_unpacked(parsed_message, NULL);
    my_message__free_unpacked(message, NULL);
    free(buffer);

    // 如果能到达这里，说明测试通过
    printf("MyMessage serialization and deserialization test succeeded.\\n");
    return EXIT_SUCCESS;
}
"""
        self.cmd(cmdline3)
    
    def test(self):
        self.cmd("protoc-c --proto_path=/tmp/ghm/ --c_out=/tmp/ghm/ /tmp/ghm/enums.proto")
        self.cmd("protoc-c --proto_path=/tmp/ghm/ --c_out=/tmp/ghm/ /tmp/ghm/my_message.proto")
        ret_c1, ret_o1 = self.cmd("ls -l /tmp/ghm/")
        self.assertIn("enums.pb-c.c", ret_o1)
        self.assertIn("enums.pb-c.h", ret_o1)
        self.assertIn("my_message.pb-c.c", ret_o1)
        self.assertIn("my_message.pb-c.h", ret_o1)
        self.cmd("gcc -o /tmp/ghm/test_my_message /tmp/ghm/test_my_message.c /tmp/ghm/enums.pb-c.c /tmp/ghm/my_message.pb-c.c -lprotobuf-c")
        ret_c2, ret_o2 = self.cmd("/tmp/ghm/test_my_message")
        self.assertIn("MyMessage serialization and deserialization test succeeded.", ret_o2)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")