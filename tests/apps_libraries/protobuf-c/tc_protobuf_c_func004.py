#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_protobuf_c_func004.py
@Time:      2024/03/25 14:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_protobuf_c_func004.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "protobuf-c protobuf-c-devel"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline1 = """cat > /tmp/ghm/my_message.proto <<EOF
syntax = "proto3";
package my_package;
message MyMessage {
    string field1 = 1;
    int32 field2 = 2;
}
"""
        self.cmd(cmdline1)
        cmdline2 = """cat > /tmp/ghm/test_my_message.c <<EOF
#include <string.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "my_message.pb-c.h"

int main() {
    // 创建一个空的序列化数据缓冲区
    size_t empty_buffer_size = 0;
    uint8_t *empty_buffer = malloc(empty_buffer_size);

    // 尝试从空缓冲区反序列化
    MyPackage__MyMessage *empty_msg = my_package__my_message__unpack(NULL, empty_buffer_size, empty_buffer);
    if (empty_msg == NULL) {
        printf("Deserializing from an empty buffer failed.\\n");
    } else {
        printf("Deserialization from an empty buffer succeeded.\\n");
        my_package__my_message__free_unpacked(empty_msg, NULL);
    }

    free(empty_buffer);

    return 0;
}
"""
        self.cmd(cmdline2)
    
    def test(self):
        self.cmd("protoc-c --proto_path=/tmp/ghm/ --c_out=/tmp/ghm/ /tmp/ghm/my_message.proto")
        ret_c1, ret_o1 = self.cmd("ls -l /tmp/ghm/")
        self.assertIn("my_message.pb-c.c", ret_o1)
        self.assertIn("my_message.pb-c.h", ret_o1)
        self.cmd("gcc -o /tmp/ghm/test_my_message /tmp/ghm/test_my_message.c /tmp/ghm/my_message.pb-c.c -lprotobuf-c")
        ret_c2, ret_o2 = self.cmd("/tmp/ghm/test_my_message")
        self.assertIn("Deserialization from an empty buffer succeeded.", ret_o2)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")