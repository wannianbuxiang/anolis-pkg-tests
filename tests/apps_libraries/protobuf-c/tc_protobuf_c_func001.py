#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_protobuf_c_func001.py
@Time:      2024/03/22 14:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_protobuf_c_func001.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "protobuf-c protobuf-c-devel"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline1 = """cat > /tmp/ghm/my_message.proto <<EOF
syntax = "proto3";
package MyPackage;

message MyMessage {
    string name = 1;
    int32 id = 2;
}
"""
        self.cmd(cmdline1)
        cmdline2 = """cat > /tmp/ghm/test_my_message.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include "my_message.pb-c.h"
#include <string.h>
int main() {
    MyPackage__MyMessage *message = (MyPackage__MyMessage *) malloc(sizeof(MyPackage__MyMessage));
    if (message == NULL) {
        perror("Failed to allocate memory for message\\n");
        return 1;
    }
    // 注意：这里假设MyPackage__MyMessage的descriptor全局可见
    message->base.descriptor = &my_package__my_message__descriptor;
    // 填充字段
    message->name = strdup("Test Message");
    message->id = 42;
    // 计算并分配足够的内存来存储序列化后的数据
    size_t packed_size = my_package__my_message__get_packed_size(message);
    uint8_t *buffer = malloc(packed_size);
    // 序列化到缓冲区
    if (!my_package__my_message__pack(message, buffer)) {
        printf("Failed to pack the message.\\n");
        free(message->name); // 释放之前分配的name字符串
        free(message);
        free(buffer);
        return EXIT_FAILURE;
    }
    // 解码
    MyPackage__MyMessage *parsed_message = my_package__my_message__unpack(NULL, packed_size, buffer);
    assert(parsed_message != NULL); 
    assert(strcmp(parsed_message->name, message->name) == 0);
    assert(parsed_message->id == message->id);
    // 清理资源
    free(message->name);
    free(message);
    my_package__my_message__free_unpacked(parsed_message, NULL);
    free(buffer);

    printf("MyMessage serialization and deserialization test succeeded.\\n");
    return EXIT_SUCCESS;
}
"""
        self.cmd(cmdline2)
    
    def test(self):
        self.cmd("protoc-c --proto_path=/tmp/ghm/ --c_out=/tmp/ghm/ /tmp/ghm/my_message.proto")
        ret_c1, ret_o1 = self.cmd("ls -l /tmp/ghm/")
        self.assertIn("my_message.pb-c.c", ret_o1)
        self.assertIn("my_message.pb-c.h", ret_o1)
        self.cmd("gcc -o /tmp/ghm/test_my_message /tmp/ghm/test_my_message.c /tmp/ghm/my_message.pb-c.c -lprotobuf-c")
        ret_c2, ret_o2 = self.cmd("/tmp/ghm/test_my_message")
        self.assertIn("MyMessage serialization and deserialization test succeeded.", ret_o2)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")