#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libkcapi_func003.py
@Time:      2024/04/18 10:31:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libkcapi_func003.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libkcapi libkcapi-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_libkcapi.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "kcapi.h"

#define PASSWORD "my_password"
#define SALT "a_salt_string"
#define KEY_LEN 32
#define ITERATIONS 10000 // Example iteration count

int main() {
    uint8_t password[strlen(PASSWORD)];
    uint8_t salt[strlen(SALT)];
    uint8_t key[KEY_LEN];

    // Convert strings to binary arrays
    strncpy((char*)password, PASSWORD, sizeof(password));
    password[strlen(PASSWORD)] = '\\0';
    strncpy((char*)salt, SALT, sizeof(salt));
    salt[strlen(SALT)] = '\\0';

    // Call kcapi_pbkdf function
    ssize_t ret = kcapi_pbkdf("hmac(sha256)", password, strlen(PASSWORD), salt, strlen(SALT), ITERATIONS, key, KEY_LEN);
    if (ret < 0) {
        fprintf(stderr, "Error deriving key using kcapi_pbkdf: %ld\\n", (long)ret);
        return EXIT_FAILURE;
    }

    printf("Derived key (%zu bytes): ", KEY_LEN);
    for(size_t i = 0; i < KEY_LEN; ++i)
        printf("%02x", key[i]);
    printf("\\n");

    return EXIT_SUCCESS;
}
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o /tmp/ghm/test_libkcapi /tmp/ghm/test_libkcapi.c -lkcapi")
        ret_c, ret_o = self.cmd("/tmp/ghm/test_libkcapi")
        self.assertIn("Derived key (32 bytes)", ret_o)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")
