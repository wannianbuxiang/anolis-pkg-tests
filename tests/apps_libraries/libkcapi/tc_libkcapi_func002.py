#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libkcapi_func002.py
@Time:      2024/04/17 16:31:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libkcapi_func002.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libkcapi libkcapi-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_libkcapi.c <<EOF
#include <stdio.h>
#include <kcapi.h>

int main(int argc, char *argv[])
{
        char buf[8192];
        struct kcapi_handle *handle;
        struct iovec iov;
        ssize_t ret;
	int i;

        (void)argc;
        (void)argv;

        iov.iov_base = buf;

        ret = kcapi_cipher_init(&handle, "cbc(aes)", 0);
        if (ret)
                return (int)ret;

        ret = kcapi_cipher_setkey(handle, (unsigned char *)"0123456789abcdef", 16);
        if (ret)
                return (int)ret;

        ret = kcapi_cipher_stream_init_enc(handle, (unsigned char *)"0123456789abcdef", NULL, 0);
        if (ret < 0)
                return (int)ret;

	for (i = 0; i < 50; i++) {
		printf("round %d\\n", i);

		iov.iov_len = 6182;
		ret = kcapi_cipher_stream_update(handle, &iov, 1);
		if (ret < 0)
			return (int)ret;

		iov.iov_len = 6182;
		ret = kcapi_cipher_stream_op(handle, &iov, 1);
		if (ret < 0)
			return (int)ret;
	}

        kcapi_cipher_destroy(handle);

        return 0;
}
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o /tmp/ghm/test_libkcapi /tmp/ghm/test_libkcapi.c -lkcapi")
        ret_c, ret_o = self.cmd("/tmp/ghm/test_libkcapi")
        self.assertIn("round 0", ret_o)
        self.assertIn("round 49", ret_o)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")
