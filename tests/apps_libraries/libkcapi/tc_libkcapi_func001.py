#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libkcapi_func001.py
@Time:      2024/04/17 11:31:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libkcapi_func001.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libkcapi libkcapi-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_libkcapi.c <<EOF
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <kcapi.h>

static int hashtest(void)
{
	char *in = "teststring";
	uint8_t out[64];
	ssize_t ret;

	ret = kcapi_md_sha1((uint8_t*)in, strlen(in), out, sizeof(out));
	if (ret != 20) {
		printf("SHA-1 error");
		return 1;
	}

	ret = kcapi_md_sha224((uint8_t*)in, strlen(in), out, sizeof(out));
	if (ret != 28) {
		printf("SHA-224 error");
		return 1;
	}

	ret = kcapi_md_sha256((uint8_t*)in, strlen(in), out, sizeof(out));
	if (ret != 32) {
		printf("SHA-256 error");
		return 1;
	}

	ret = kcapi_md_sha384((uint8_t*)in, strlen(in), out, sizeof(out));
	if (ret != 48) {
		printf("SHA-384 error");
		return 1;
	}

	ret = kcapi_md_sha512((uint8_t*)in, strlen(in), out, sizeof(out));
	if (ret != 64) {
		printf("SHA-512 error");
		return 1;
	}

	return 0;
}

static int hmactest(void)
{
	char *in = "longteststring";
	uint8_t out[64];
	ssize_t ret;

	ret = kcapi_md_hmac_sha1((uint8_t*)in, (uint32_t)strlen(in),
				 (uint8_t*)in, strlen(in), out, sizeof(out));
	if (ret != 20) {
		printf("HMAC SHA-1 error");
		return 1;
	}

	ret = kcapi_md_hmac_sha224((uint8_t*)in, (uint32_t)strlen(in),
				   (uint8_t*)in, strlen(in), out, sizeof(out));
	if (ret != 28) {
		printf("HMAC SHA-224 error");
		return 1;
	}

	ret = kcapi_md_hmac_sha256((uint8_t*)in, (uint32_t)strlen(in),
				   (uint8_t*)in, strlen(in), out, sizeof(out));
	if (ret != 32) {
		printf("HMAC SHA-256 error");
		return 1;
	}

	ret = kcapi_md_hmac_sha384((uint8_t*)in, (uint32_t)strlen(in),
				   (uint8_t*)in, strlen(in), out, sizeof(out));
	if (ret != 48) {
		printf("HMAC SHA-384 error");
		return 1;
	}

	ret = kcapi_md_hmac_sha512((uint8_t*)in, (uint32_t)strlen(in),
				   (uint8_t*)in, strlen(in), out, sizeof(out));
	if (ret != 64) {
		printf("HMAC SHA-512 error");
		return 1;
	}

	return 0;
}

static int ciphertest(void)
{
	uint8_t *origpt = (uint8_t *)"01234567890123450123456789012345";
	uint8_t ct[32];
	uint8_t newpt[32];
	ssize_t ret;

	ret = kcapi_cipher_enc_aes_cbc(origpt, 32, origpt, 32, origpt,
				       ct, sizeof(ct));
	if (ret != sizeof(ct)) {
		printf("AES CBC encrytion error");
		return 1;
	}

	ret = kcapi_cipher_dec_aes_cbc(origpt, 32, ct, sizeof(ct), origpt,
				       newpt, sizeof(newpt));
	if (ret != sizeof(newpt) || memcmp(origpt, newpt, sizeof(newpt))) {
		printf("AES CBC decrytion error");
		return 1;
	}

	ret = kcapi_cipher_enc_aes_ctr(origpt, 32, origpt, 32, origpt, ct,
				       sizeof(ct));
	if (ret != sizeof(ct)) {
		printf("AES CTR encrytion error");
		return 1;
	}

	ret = kcapi_cipher_dec_aes_ctr(origpt, 32, ct, sizeof(ct), origpt,
				       newpt, sizeof(newpt));
	if (ret != sizeof(newpt) || memcmp(origpt, newpt, sizeof(newpt))) {
		printf("AES CTR decrytion error");
		return 1;
	}

	return 0;
}

static int rngtest(void)
{
	uint8_t out[67];
	ssize_t ret;

	ret = kcapi_rng_get_bytes(out, sizeof(out));
	if (ret != sizeof(out)) {
		printf("Random number generation error");
		return 1;
	}

	return 0;
}

int main(int argc, char *argv[])
{
	int ret;

	(void)argc;
	(void)argv;

	ret = hashtest();
  printf("hash success\\n");
	if (ret)
		return ret;

	ret = hmactest();
  printf("hmac success\\n");
	if (ret)
		return ret;

	ret = ciphertest();
  printf("cipher success\\n");
	if (ret)
		return ret;

	ret = rngtest();
  printf("Random number generation success\\n");
	if (ret)
		return ret;

  return 0;
}
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o /tmp/ghm/test_libkcapi /tmp/ghm/test_libkcapi.c -lkcapi")
        ret_c, ret_o = self.cmd("/tmp/ghm/test_libkcapi")
        self.assertIn("hash success", ret_o)
        self.assertIn("hmac success", ret_o)
        self.assertIn("cipher success", ret_o)
        self.assertIn("Random number generation success", ret_o)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")
