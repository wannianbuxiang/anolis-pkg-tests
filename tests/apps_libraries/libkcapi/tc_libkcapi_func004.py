#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libkcapi_func004.py
@Time:      2024/04/18 14:31:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libkcapi_func004.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libkcapi libkcapi-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_libkcapi.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "kcapi.h"

#define PASSWORD "my_password"
#define SALT "a_salt_string"
#define KEY_LEN 32
#define LABEL "ExampleLabel"
#define CONTEXT "SomeContextData"
#define SHARED_SECRET "ThisIsTheSharedSecret"
#define DIGEST_SIZE 32 // 假设使用的哈希函数的digest大小

int main() {
    struct kcapi_handle *handle;
    ssize_t ret;
    uint8_t derived_key[KEY_LEN];
    uint8_t src[strlen(LABEL) + 1 + strlen(CONTEXT) + 1]; // 不需要SHARED_SECRET，因为这是SP800-108的用法
    uint8_t hmac_key[DIGEST_SIZE]; // 创建一个密钥缓冲区，大小等于digest大小

    // 填充密钥（这里仅做示例，实际应用中应从可靠源获取或生成密钥）
    memset(hmac_key, 0x00, DIGEST_SIZE);

    // 将数据填充到src缓冲区
    memcpy(src, LABEL, strlen(LABEL));
    src[strlen(LABEL)] = '\\0'; // 添加0x00分隔符
    memcpy(src + strlen(LABEL) + 1, CONTEXT, strlen(CONTEXT) + 1);

    // 初始化KC-API句柄
    handle = NULL;
    ret = kcapi_md_init(&handle, "hmac(sha256)", 0); // 参数3暂时留空，根据实际需求设置
    if (ret < 0) {
        perror("Failed to initialize kcapi_handle");
        return EXIT_FAILURE;
    }

    // 设置密钥，即使对于SP800-108可能不直接使用
    ret = kcapi_md_setkey(handle, hmac_key, DIGEST_SIZE);
    if (ret < 0) {
        perror("Failed to set key for HMAC-SHA256");
        kcapi_md_destroy(handle);
        return EXIT_FAILURE;
    }

    // 示例：使用kcapi_kdf_ctr()
    ret = kcapi_kdf_ctr(handle, src, strlen(LABEL) + 1 + strlen(CONTEXT) + 1, derived_key, KEY_LEN);
    if (ret < 0) {
        perror("Error deriving key using kcapi_kdf_ctr:");
    } else {
        printf("Derived key using kcapi_kdf_ctr (%zu bytes): ", KEY_LEN);
        for(size_t i = 0; i < KEY_LEN; ++i)
            printf("%02x", derived_key[i]);
        printf("\\n");
    }

    // 释放KC-API句柄资源
    kcapi_md_destroy(handle);

    return EXIT_SUCCESS;
}
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o /tmp/ghm/test_libkcapi /tmp/ghm/test_libkcapi.c -lkcapi")
        ret_c, ret_o = self.cmd("/tmp/ghm/test_libkcapi")
        self.assertIn("Derived key using kcapi_kdf_ctr (32 bytes)", ret_o)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")
