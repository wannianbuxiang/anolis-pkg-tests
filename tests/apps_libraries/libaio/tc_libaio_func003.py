#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libaio_func003.py
@Time:      2024/04/10 13:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""
import subprocess
from common.basetest import LocalTest

class Test(LocalTest): 
    """
    See tc_libaio_func003.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libaio libaio-devel"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_libaio.c <<EOF
#include <sys/mman.h>
#include <unistd.h> 
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <libaio.h>
#include <string.h>
#include <assert.h>

io_context_t	io_ctx;
#define BAD_CTX	((io_context_t)-1)
#define SETUP	aio_setup(1024) 
#define READ		'r'
#define WRITE		'w'
#define READ_SILENT	'R'
#define WRITE_SILENT	'W'
#define READV		'<'
#define WRITEV		'>'

	
void aio_setup(int n)
{
	int res = io_queue_init(n, &io_ctx);
	if (res != 0) {
		printf("io_queue_setup(%d) returned %d (%s)\\n",
			n, res, strerror(-res));
		exit(3);
	}
}

int sync_submit(struct iocb *iocb)
{
	struct io_event event;
	struct iocb *iocbs[] = { iocb };
	int res;
 
	/* 30 second timeout should be enough */
	struct timespec	ts;
	ts.tv_sec = 30;
	ts.tv_nsec = 0;
 
	res = io_submit(io_ctx, 1, iocbs);
	if (res != 1) {
		printf("sync_submit: io_submit res=%d [%s]\\n", res, strerror(-res));
		return res;
	}
 
	res = io_getevents(io_ctx, 0, 1, &event, &ts);
	if (res != 1) {
		printf("sync_submit: io_getevents res=%d [%s]\\n", res, strerror(-res));
		return res;
	}
	return event.res;
}

int attempt_rw(int fd, void *buf, int count, long long pos, int rw, int expect)
{
	struct iocb iocb;
	int res;
	int silent = 0;
 
	switch(rw) {
	case READ_SILENT:
		silent = 1;
	case READ:
		io_prep_pread (&iocb, fd, buf, count, pos);
		break;
	case WRITE_SILENT:
		silent = 1;
	case WRITE:
		io_prep_pwrite(&iocb, fd, buf, count, pos);
		break;
	case WRITEV:
		io_prep_pwritev(&iocb, fd, buf, count, pos);
		break;
	case READV:
		io_prep_preadv(&iocb, fd, buf, count, pos);
		break;
	}
 
	if (!silent) {
		printf("expect %5d: (%c), res = ", expect, rw);
		fflush(stdout);
	}
	res = sync_submit(&iocb);
	if (!silent || res != expect) {
		if (silent)
			printf("expect %5d: (%c), res = ", expect, rw);
		printf("%5d [%s]%s\\n", res,
			(res <= 0) ? strerror(-res) : "Success",
			(res != expect) ? " -- FAILED" : "");
	}
 
	if (res != expect)
		return 1;
 
	return 0;
} 

int main(void)
{
    SETUP;
	int page_size = getpagesize();
#define SIZE 512
	char *buf;
	int rwfd;
	int	status = 0, res;
 
	rwfd = open("/tmp/ghm/rwfile", O_RDWR);		assert(rwfd != -1);
	res = ftruncate(rwfd, 512);			assert(res == 0);
 
	buf = mmap(0, page_size, PROT_READ|PROT_WRITE, MAP_SHARED, rwfd, 0);
	assert(buf != (char *)-1);
 
	status |= attempt_rw(rwfd, buf, SIZE,  0, WRITE, SIZE);
	status |= attempt_rw(rwfd, buf, SIZE,  0,  READ, SIZE);
 
	res = munmap(buf, page_size);			assert(res == 0);
	buf = mmap(0, page_size, PROT_READ|PROT_WRITE, MAP_SHARED, rwfd, 0);
	assert(buf != (char *)-1);
 
	status |= attempt_rw(rwfd, buf, SIZE,  0,  READ, SIZE);
	status |= attempt_rw(rwfd, buf, SIZE,  0, WRITE, SIZE);
 
	res = munmap(buf, page_size);			assert(res == 0);
	buf = mmap(0, page_size, PROT_READ, MAP_SHARED, rwfd, 0);
	assert(buf != (char *)-1);
 
	status |= attempt_rw(rwfd, buf, SIZE,  0, WRITE, SIZE);
	status |= attempt_rw(rwfd, buf, SIZE,  0,  READ, -EFAULT);
 
	res = munmap(buf, page_size);			assert(res == 0);
	buf = mmap(0, page_size, PROT_WRITE, MAP_SHARED, rwfd, 0);
	assert(buf != (char *)-1);
 
	/* Whether PROT_WRITE is readable is arch-dependent.  So compare
	 * against write() result (to make the kernel read from buf). */
	res = write(rwfd, buf, SIZE);
	if (res < 0)
		res = -errno;
	status |= attempt_rw(rwfd, buf, SIZE,  0,  READ, SIZE);
	status |= attempt_rw(rwfd, buf, SIZE,  0, WRITE, res);
 
	return status;
}
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("touch /tmp/ghm/rwfile")
        self.cmd("gcc -o /tmp/ghm/test_libaio /tmp/ghm/test_libaio.c -laio")
        ret_c1, ret_o1 = self.cmd("/tmp/ghm/test_libaio")
        self.expected_re1 = """
expect   512: (w), res =   512 [Success]
expect   512: (r), res =   512 [Success]
expect   512: (r), res =   512 [Success]
expect   512: (w), res =   512 [Success]
expect   512: (w), res =   512 [Success]
expect   -14: (r), res =   -14 [Bad address]
expect   512: (r), res =   512 [Success]
expect   512: (w), res =   512 [Success]
"""
        actual_res1 = ret_o1.strip().splitlines()
        expected_res1 = self.expected_re1.strip().splitlines()
        for expected1 in expected_res1:
            self.assertIn(expected1, actual_res1)
            self.log.info("%s exist." % expected1)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")