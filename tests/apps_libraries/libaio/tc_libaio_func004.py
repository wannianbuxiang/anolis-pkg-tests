#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libaio_func003.py
@Time:      2024/04/10 13:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""
import subprocess
from common.basetest import LocalTest

class Test(LocalTest): 
    """
    See tc_libaio_func003.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libaio libaio-devel"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_libaio.c <<EOF
#include <sys/mman.h>
#include <unistd.h> 
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <assert.h>
#include <libaio.h>
#include <string.h>
	
io_context_t	io_ctx;
#define BAD_CTX	((io_context_t)-1)
#define SETUP	aio_setup(1024) 

void aio_setup(int n)
{
	int res = io_queue_init(n, &io_ctx);
	if (res != 0) {
		printf("io_queue_setup(%d) returned %d (%s)\\n",
			n, res, strerror(-res));
		exit(3);
	}
}
	
int sync_submit(struct iocb *iocb)
{
	struct io_event event;
	struct iocb *iocbs[] = { iocb };
	int res;
 
	struct timespec	ts;
	ts.tv_sec = 30;
	ts.tv_nsec = 0;
 
	res = io_submit(io_ctx, 1, iocbs);
	if (res != 1) {
		printf("sync_submit: io_submit res=%d [%s]\\n", res, strerror(-res));
		return res;
	}
 
	res = io_getevents(io_ctx, 0, 1, &event, &ts);
	if (res != 1) {
		printf("sync_submit: io_getevents res=%d [%s]\\n", res, strerror(-res));
		return res;
	}
	return event.res;
}

#define SIZE	512
#define READ	'r'
#define WRITE	'w'
int attempt(int fd, void *buf, int count, long long pos, int rw, int expect)
{
	struct iocb iocb;
	int res;
 
	switch(rw) {
	case READ:	io_prep_pread (&iocb, fd, buf, count, pos); break;
	case WRITE:	io_prep_pwrite(&iocb, fd, buf, count, pos); break;
	}
 
	printf("expect %3d: (%c), res = ", expect, rw);
	fflush(stdout);
	res = sync_submit(&iocb);
	printf("%3d [%s]%s\\n", res, (res <= 0) ? strerror(-res) : "Success",
		(res != expect) ? " -- FAILED" : "");
	if (res != expect)
		return 1;
 
	return 0;
}
 
int main(void)
{
    SETUP;
	char buf[SIZE];
	int rofd, wofd, rwfd;
	int	status = 0, res;
 
	memset(buf, 0, SIZE);
 
	rofd = open("/tmp/ghm/rofile", O_RDONLY);	assert(rofd != -1);
	wofd = open("/tmp/ghm/wofile", O_WRONLY);	assert(wofd != -1);
	rwfd = open("/tmp/ghm/rwfile", O_RDWR);		assert(rwfd != -1);
 
	status |= attempt(rofd, buf, SIZE,  0, WRITE, -EBADF);
	status |= attempt(wofd, buf, SIZE,  0,  READ, -EBADF);
	status |= attempt(rwfd, buf, SIZE,  0, WRITE, SIZE);
	status |= attempt(rwfd, buf, SIZE,  0,  READ, SIZE);
	status |= attempt(rwfd, buf, SIZE, -1,  READ, -EINVAL);
	status |= attempt(rwfd, buf, SIZE, -1, WRITE, -EINVAL);
 
	rwfd = open("/tmp/ghm/rwfile", O_RDWR|O_APPEND);	assert(rwfd != -1);
	res = ftruncate(rwfd, 0);			assert(res == 0);
	status |= attempt(rwfd, buf,    SIZE, 0,  READ, 0);
	status |= attempt(rwfd, "1234",    4, 0, WRITE, 4);
	status |= attempt(rwfd, "5678",    4, 0, WRITE, 4);
	memset(buf, 0, SIZE);
	status |= attempt(rwfd,    buf, SIZE, 0,  READ, 8);
	printf("read after append: [%s]\\n", buf);
	assert(memcmp(buf, "12345678", 8) == 0);
 
	/* Some architectures map the 0 page.  Ugh. */
#if !defined(__ia64__)
	status |= attempt(rwfd, NULL, SIZE, 0, WRITE, -EFAULT);
#endif
 
	return status;
}
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("touch /tmp/ghm/rofile")
        self.cmd("touch /tmp/ghm/wofile")
        self.cmd("touch /tmp/ghm/rwfile")
        self.cmd("gcc -o /tmp/ghm/test_libaio /tmp/ghm/test_libaio.c -laio")
        ret_c1, ret_o1 = self.cmd("/tmp/ghm/test_libaio")
        self.expected_re1 = """
expect  -9: (w), res = sync_submit: io_submit res=-9 [Bad file descriptor]
 -9 [Bad file descriptor]
expect  -9: (r), res = sync_submit: io_submit res=-9 [Bad file descriptor]
 -9 [Bad file descriptor]
expect 512: (w), res = 512 [Success]
expect 512: (r), res = 512 [Success]
expect -22: (r), res = sync_submit: io_submit res=-22 [Invalid argument]
-22 [Invalid argument]
expect -22: (w), res = sync_submit: io_submit res=-22 [Invalid argument]
-22 [Invalid argument]
expect   0: (r), res =   0 [Success]
expect   4: (w), res =   4 [Success]
expect   4: (w), res =   4 [Success]
expect   8: (r), res =   8 [Success]
read after append: [12345678]
expect -14: (w), res = -14 [Bad address]
"""
        actual_res1 = ret_o1.strip().splitlines()
        expected_res1 = self.expected_re1.strip().splitlines()
        for expected1 in expected_res1:
            self.assertIn(expected1, actual_res1)
            self.log.info("%s exist." % expected1)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")