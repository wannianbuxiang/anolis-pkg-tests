#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libaio_func002.py
@Time:      2024/04/10 10:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""
import subprocess
from common.basetest import LocalTest

class Test(LocalTest): 
    """
    See tc_libaio_func002.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libaio libaio-devel"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_libaio.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include <libaio.h>
#include <errno.h>
#include <string.h>
#include <stddef.h>
#include <time.h>

io_context_t	io_ctx;
#define BAD_CTX	((io_context_t)-1)

void aio_setup(int n)
{
    int res = io_queue_init(n, &io_ctx);
    if (res != 0) {
        printf("io_queue_setup(%d) returned %d (%s)\\n",
               n, res, strerror(-res));
        exit(3);
    }
}

int attempt_io_submit(io_context_t ctx, long nr, struct iocb *ios[], int expect)
{
    int res;

    printf("expect %3d: io_submit(%10p, %3ld, %10p) = ", expect, ctx, nr, ios);
    fflush(stdout);
    res = io_submit(ctx, nr, ios);
    printf("%3d [%s]%s\\n", res, (res <= 0) ? strerror(-res) : "",
           (res != expect) ? " -- FAILED" : "");
    if (res != expect)
        return 1;

    return 0;
}

int main(void)
{
    aio_setup(10);
    struct iocb a, b;
    struct iocb *good_ios[] = { &a, &b };
    struct iocb *bad1_ios[] = { NULL, &b };
    // 替换 KERNEL_RW_POINTER 为已知无效的指针地址
    struct iocb *bad2_ios[] = { (struct iocb *)-1, &a };
    int status = 0;

    status |= attempt_io_submit(BAD_CTX, 1,   good_ios, -EINVAL);
    status |= attempt_io_submit( io_ctx, 0,   good_ios,       0);
    status |= attempt_io_submit( io_ctx, 1,       NULL, -EFAULT);
    status |= attempt_io_submit( io_ctx, 1, (void *)-1, -EFAULT);
    status |= attempt_io_submit( io_ctx, 2,   bad1_ios, -EFAULT);
    status |= attempt_io_submit( io_ctx, 2,   bad2_ios, -EFAULT);
    status |= attempt_io_submit( io_ctx, -1,  good_ios, -EINVAL);

    return status;
}
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o /tmp/ghm/test_libaio /tmp/ghm/test_libaio.c -laio")
        ret_c1, ret_o1 = self.cmd("/tmp/ghm/test_libaio", ignore_status=True)
        self.assertIn("0 [Success]", ret_o1)
        self.assertIn("-14 [Bad address]", ret_o1)
        self.assertIn("-22 [Invalid argument]", ret_o1)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")