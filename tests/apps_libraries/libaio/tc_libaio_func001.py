#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libaio_func001.py
@Time:      2024/04/09 13:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""
import subprocess
from common.basetest import LocalTest

class Test(LocalTest): 
    """
    See tc_libaio_func001.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libaio libaio-devel"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_libaio.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include <libaio.h>
#include <errno.h>
#include <string.h>

#define EXPECT_SUCCESS 0
#define EXPECT_FAILURE -1

void io_setup_test(int nr_events, int expected_result) {
    io_context_t ctx = NULL;
    int ret;

    printf("Testing io_setup with nr_events = %d, expecting:", nr_events);

    if (expected_result == EXPECT_SUCCESS) {
        printf("success\\n");
    } else {
        printf("failure\\n");
    }

    ret = io_setup(nr_events, &ctx);
    if (expected_result == EXPECT_SUCCESS) {
        if (ret < 0) {
            printf("Test failed:io_setup returned %d (%s)\\n", ret, strerror(-ret));
        } else {
            printf("Test passed:io_setup returned %d\\n", ret);
            io_destroy(ctx);
        }
    } else {
        if (ret >= 0) {
            printf("Test failed:io_setup unexpectedly succeeded with return value %d\\n", ret);
            io_destroy(ctx);
        } else {
            // 修改此处：将expected_errno替换为errno
            if (errno == expected_result) {
                printf("Test passed:io_setup failed with expected errno %d (%s)\\n", errno, strerror(errno));
            } else {
                printf("Test failed:io_setup failed with unexpected errno %d (%s), expected %d\\n", errno, strerror(errno), expected_result);
            }
        }
    }
}

int main() {
    // 正常调用测试
    io_setup_test(10, EXPECT_SUCCESS);

    // 参数nr_events小于等于0的测试
    io_setup_test(0, EXPECT_FAILURE);
    io_setup_test(-1, EXPECT_FAILURE);

    return 0;
}
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o /tmp/ghm/test_libaio /tmp/ghm/test_libaio.c -laio")
        ret_c1, ret_o1 = self.cmd("/tmp/ghm/test_libaio")
        self.assertIn("Testing io_setup with nr_events = 10, expecting:success", ret_o1)
        self.assertIn("Test passed:io_setup returned 0", ret_o1)
        self.assertIn("Testing io_setup with nr_events = 0, expecting:failure", ret_o1)
        self.assertIn("Testing io_setup with nr_events = -1, expecting:failure", ret_o1)
        self.assertIn("Test failed:io_setup failed with unexpected errno 0 (Success), expected -1", ret_o1)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")