# -*- encoding: utf-8 -*-qintingting

"""
@File:      tc_libnetfilter_conntrack_fun002.py
@Time:      2024-04-25 15:10:38
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libnetfilter_conntrack_fun002.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    PARAM_DIC = {"pkg_name": "libnetfilter_conntrack conntrack-tools"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.test_src_ip="1.2.3.4"
        self.test_dest_ip="8.8.8.8"
        self.test_src_port="12345"
        self.test_dest_port="53"

    def test(self):
        # 新建一个连接跟踪条目，使用 udp 协议
        self.cmd(f"conntrack -I -p udp --orig-src {self.test_src_ip} --orig-dst {self.test_dest_ip} --sport {self.test_src_port} --dport {self.test_dest_port} --timeout 60")
        # 修改刚才建立的连接跟踪条目的状态为ASSURED
        ret_code, conntrack_output = self.cmd(f"conntrack -U -p udp --orig-src {self.test_src_ip} --orig-dst {self.test_dest_ip} --sport {self.test_src_port} --dport {self.test_dest_port}  -u ASSURED")
        self.assertIn("ASSURED", conntrack_output,"state修改失败")                                           

    def tearDown(self):
        self.cmd(f"conntrack -D -p udp --orig-src {self.test_src_ip} --orig-dst {self.test_dest_ip} --sport {self.test_src_port} --dport {self.test_dest_port}")
        super().tearDown(self.PARAM_DIC)