# -*- encoding: utf-8 -*-

"""
@File:      tc_libnetfilter_conntrack_fun004.py
@Time:      2024-04-24 14:22:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libnetfilter_conntrack_fun004.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    PARAM_DIC = {"pkg_name": "libnetfilter_conntrack libnetfilter_conntrack-devel"}
    file_context = r"""
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <libnetfilter_conntrack/libnetfilter_conntrack.h>

int main() {
    // Allocate a new conntrack object
    struct nf_conntrack *ct = nfct_new();
    if (ct == NULL) {
        fprintf(stderr, "Error: Could not allocate new conntrack object\n");
        return EXIT_FAILURE;
    }

    // Create example IPv4 source and destination addresses
    struct in_addr ipv4_src, ipv4_dst;
    inet_pton(AF_INET, "192.168.1.100", &ipv4_src);
    inet_pton(AF_INET, "192.168.1.200", &ipv4_dst);

    // Set the IPv4 source and destination addresses in the conntrack object
    nfct_set_attr(ct, ATTR_IPV4_SRC, &ipv4_src);
    nfct_set_attr(ct, ATTR_IPV4_DST, &ipv4_dst);

    // Retrieve and print the IPv4 source and destination addresses
    char src_addr_str[INET_ADDRSTRLEN], dst_addr_str[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, nfct_get_attr(ct, ATTR_IPV4_SRC), src_addr_str, INET_ADDRSTRLEN);
    inet_ntop(AF_INET, nfct_get_attr(ct, ATTR_IPV4_DST), dst_addr_str, INET_ADDRSTRLEN);

    printf("IPv4 Source Address: %s\n", src_addr_str);
    printf("IPv4 Destination Address: %s\n", dst_addr_str);

    // Destroy the conntrack object
    nfct_destroy(ct);

    return EXIT_SUCCESS;
}
"""

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.c_file = "libnetfilter_conntrack_fun_004.c"
        self.exec_file = "libnetfilter_conntrack_fun_004"
        cmdline = f"""cat > {self.c_file } <<EOF
        {self.file_context}
EOF
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd(f"gcc -o {self.exec_file} {self.c_file} -lnetfilter_conntrack")
        code, result = self.cmd(f"file {self.exec_file}")
        self.assertIn("executable", result)
        code, result = self.cmd(f"./{self.exec_file}")
        self.assertIn("IPv4 Source Address: 192.168.1.100", result, "读取conntrack 表中的网络连接跟踪条目失败")
        self.assertIn("IPv4 Destination Address: 192.168.1.200", result, "读取conntrack 表中的网络连接跟踪条目失败")


    def tearDown(self):
        self.cmd(f"rm -rf {self.c_file} {self.exec_file}")
        super().tearDown(self.PARAM_DIC)

