# -*- encoding: utf-8 -*-

"""
@File:      tc_libnetfilter_conntrack_fun001.py
@Time:      2024-04-25 11:22:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libnetfilter_conntrack_fun001.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    PARAM_DIC = {"pkg_name": "libnetfilter_conntrack libnetfilter_conntrack-devel conntrack-tools"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.test_src_ip="1.2.3.4"
        self.test_dest_ip="8.8.8.8"
        self.test_src_port="12345"
        self.test_dest_port="53"

    def test(self):
        # 新建一个连接跟踪条目，使用 udp 协议
        self.cmd(f"conntrack -I -p udp --orig-src {self.test_src_ip} --orig-dst {self.test_dest_ip} --sport {self.test_src_port} --dport {self.test_dest_port} --timeout 60")
        # 检索刚才建立的连接跟踪条目看是否真的被添加进去了
        ret_code, conntrack_output = self.cmd(f"conntrack -L -p udp --orig-dst {self.test_dest_ip} --dport {self.test_dest_port}")
        self.assertIn("src=8.8.8.8", conntrack_output,"连接条目添加失败")
                                           

    def tearDown(self):
        self.cmd(f"conntrack -D -p udp --orig-src {self.test_src_ip} --orig-dst {self.test_dest_ip} --sport {self.test_src_port} --dport {self.test_dest_port}")
        super().tearDown(self.PARAM_DIC)
