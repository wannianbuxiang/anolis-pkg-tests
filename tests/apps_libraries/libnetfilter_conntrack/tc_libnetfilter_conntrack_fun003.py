# -*- encoding: utf-8 -*-

"""
@File:      tc_libnetfilter_conntrack_fun003.py
@Time:      2024-04-24 10:22:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libnetfilter_conntrack_fun003.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    PARAM_DIC = {"pkg_name": "libnetfilter_conntrack libnetfilter_conntrack-devel conntrack-tools"}
    file_context = r"""
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <libnetfilter_conntrack/libnetfilter_conntrack.h>

static int cb(enum nf_conntrack_msg_type type, struct nf_conntrack *ct, void *data) {
    char src_ip_str[INET_ADDRSTRLEN], dst_ip_str[INET_ADDRSTRLEN];
    u_int16_t src_port, dst_port;
    u_int8_t protocol;
    u_int32_t src_ip, dst_ip;
    
    // 获取并打印五元组信息
    protocol = nfct_get_attr_u8(ct, ATTR_L4PROTO);
    src_port = ntohs(nfct_get_attr_u16(ct, ATTR_PORT_SRC));
    dst_port = ntohs(nfct_get_attr_u16(ct, ATTR_PORT_DST));
    src_ip   = nfct_get_attr_u32(ct, ATTR_IPV4_SRC);
    dst_ip   = nfct_get_attr_u32(ct, ATTR_IPV4_DST);
    inet_ntop(AF_INET, &src_ip, src_ip_str, sizeof(src_ip_str));
    inet_ntop(AF_INET, &dst_ip, dst_ip_str, sizeof(dst_ip_str));

    printf("Protocol: %u Src: %s:%u Dst: %s:%u\n", protocol, src_ip_str, src_port, dst_ip_str, dst_port);

    return NFCT_CB_CONTINUE;
}

int main() {
    struct nfct_handle *h;
    struct nf_conntrack *ct;
    int ret;

    // 打开连接跟踪句柄
    h = nfct_open(CONNTRACK, NFCT_ALL_CT_GROUPS);
    if (h == NULL) {
        fprintf(stderr, "Error: can't open conntrack handle\n");
        exit(EXIT_FAILURE);
    }

    ct = nfct_new();
    if (ct == NULL) {
        fprintf(stderr, "Error: can't create conntrack object\n");
        nfct_close(h);
        exit(EXIT_FAILURE);
    }

    // 注册回调函数
    nfct_callback_register(h, NFCT_T_ALL, cb, NULL);

    // 查询并打印所有跟踪条目
    ret = nfct_query(h, NFCT_Q_DUMP, ct);
    if (ret == -1) {
        fprintf(stderr, "Error: query failed\n");
        nfct_close(h);
        nfct_destroy(ct);
        exit(EXIT_FAILURE);
    }

    nfct_close(h);
    nfct_destroy(ct);

    return EXIT_SUCCESS;
}
"""

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.test_src_ip="1.2.3.4"
        self.test_dest_ip="8.8.8.8"
        self.test_dest_port="53"
        self.c_file = "libnetfilter_conntrack_fun_003.c"
        self.exec_file = "libnetfilter_conntrack_fun_003"
        cmdline = f"""cat > {self.c_file } <<EOF
        {self.file_context}
EOF
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd(f"gcc -o {self.exec_file} {self.c_file} -lnetfilter_conntrack")
        code, result = self.cmd(f"file {self.exec_file}")
        self.assertIn("executable", result)
        # 新建一个连接跟踪条目，使用 udp 协议
        self.cmd(f"conntrack -I -p udp --orig-src {self.test_src_ip} --orig-dst {self.test_dest_ip} --sport 12345 --dport {self.test_dest_port} --timeout 60")
        code, result = self.cmd(f"./{self.exec_file}")
        self.assertIn(f"Protocol: 17 Src: {self.test_src_ip}:12345 Dst: {self.test_dest_ip}:{self.test_dest_port}", result, "读取conntrack 表中的网络连接跟踪条目失败")


    def tearDown(self):
        self.cmd(f"conntrack -D -p udp --orig-src {self.test_src_ip} --orig-dst {self.test_dest_ip} --sport 12345 --dport {self.test_dest_port}")
        self.cmd(f"rm -rf {self.c_file} {self.exec_file}")
        super().tearDown(self.PARAM_DIC)

