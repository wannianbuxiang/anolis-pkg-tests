# -*- encoding: utf-8 -*-

"""
@File:      tc_libnetfilter_conntrack_fun007.py
@Time:      2024-04-15 16:22:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libnetfilter_conntrack_fun007.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    PARAM_DIC = {"pkg_name": "libnetfilter_conntrack libnetfilter_conntrack-devel libmnl-devel conntrack-tools"}
    file_context = r"""
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <arpa/inet.h>
#include <libmnl/libmnl.h>
#include <libnetfilter_conntrack/libnetfilter_conntrack.h>
#include <linux/netfilter/nf_conntrack_tcp.h>

// 回调函数，处理接收到的每条消息
static int data_cb(const struct nlmsghdr *nlh, void *data)
{
    struct nf_conntrack *ct;
    char buf[4096];

    // 创建新的 conntrack 对象
    ct = nfct_new();
    if (ct == NULL) {
        perror("nfct_new failed");
        return MNL_CB_ERROR;
    }

    // 解析Netlink消息，将其转换为 conntrack 对象
    nfct_nlmsg_parse(nlh, ct);

    // 将 conntrack 对象格式化为字符串并打印
    nfct_snprintf(buf, sizeof(buf), ct, NFCT_T_UNKNOWN, NFCT_O_DEFAULT, 0);
    printf("%s\n", buf);

    // 销毁conntrack对象
    nfct_destroy(ct);

    return MNL_CB_OK;
}

int main(void)
{
    struct mnl_socket *nl;
    struct nlmsghdr *nlh;
    struct nfgenmsg *nfh;
    char buf[MNL_SOCKET_BUFFER_SIZE];
    unsigned int seq, portid;
    int ret;

    // 打开Netlink套接字
    nl = mnl_socket_open(NETLINK_NETFILTER);
    if (nl == NULL) {
        perror("mnl_socket_open failed");
        exit(EXIT_FAILURE);
    }

    // 绑定套接字
    if (mnl_socket_bind(nl, 0, MNL_SOCKET_AUTOPID) < 0) {
        perror("mnl_socket_bind failed");
        mnl_socket_close(nl);
        exit(EXIT_FAILURE);
    }

    portid = mnl_socket_get_portid(nl);

    // 创建并设置Netlink消息头
    nlh = mnl_nlmsg_put_header(buf);
    nlh->nlmsg_type = (NFNL_SUBSYS_CTNETLINK << 8) | IPCTNL_MSG_CT_GET;
    nlh->nlmsg_flags = NLM_F_REQUEST | NLM_F_DUMP;
    nlh->nlmsg_seq = seq = time(NULL);
 
    nfh = mnl_nlmsg_put_extra_header(nlh, sizeof(struct nfgenmsg));
    nfh->nfgen_family = AF_INET;
    nfh->version = NFNETLINK_V0;
    nfh->res_id = 0;

    // 发送消息
    ret = mnl_socket_sendto(nl, nlh, nlh->nlmsg_len);
    if (ret == -1) {
        perror("mnl_socket_sendto failed");
        mnl_socket_close(nl);
        exit(EXIT_FAILURE);
    }

    // 循环接收Netlink消息并调用回调函数处理
    do {
        ret = mnl_socket_recvfrom(nl, buf, sizeof(buf));
        if (ret == -1) {
            perror("mnl_socket_recvfrom failed");
            break;
        }
        // 处理每个接收到的消息
        ret = mnl_cb_run(buf, ret, seq, portid, data_cb, NULL);
    } while (ret > 0);

    // 出错情况下的处理
    if (ret == -1) {
        perror("Error processing netlink messages");
    }

    // 清理资源
    mnl_socket_close(nl);

    return (ret == -1 ? EXIT_FAILURE : EXIT_SUCCESS);
}
"""

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.c_file = "libnetfilter_conntrack_fun_007.c"
        self.exec_file = "libnetfilter_conntrack_fun_007"
        cmdline = f"""cat > {self.c_file } <<EOF
        {self.file_context}
EOF
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd(f"gcc -o {self.exec_file} {self.c_file} -lmnl -lnetfilter_conntrack")
        code, result = self.cmd(f"file {self.exec_file}")
        self.assertIn("executable", result)
        # 新建一个连接跟踪条目，使用TCP协议
        self.cmd(f"conntrack -I -s 1.1.1.1 -d 2.2.2.2 -p tcp --sport 12345 --dport 80 -t 60 --state  ESTABLISHED")
        code, result = self.cmd(f"./{self.exec_file}")
        self.assertIn("src=1.1.1.1", result, "连接条目查询失败")

    def tearDown(self):
        self.cmd(f"conntrack -D -s 1.1.1.1 -d 2.2.2.2 -p tcp --sport 12345 --dport 80", ignore_status=True)
        self.cmd(f"rm -rf {self.c_file} {self.exec_file}")
        super().tearDown(self.PARAM_DIC)


