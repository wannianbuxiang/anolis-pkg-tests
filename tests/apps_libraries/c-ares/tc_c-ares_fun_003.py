#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_c-ares_fun_003.py
@Time:      2024/04/10 14:31:50
@Author:    zhixin
@Version:   1.0
@Contact:   zx01468051@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhixin
"""

from common.basetest import LocalTest
import re

class Test(LocalTest):
    """
    See tc_c-ares_fun_003.yaml for details

    :avocado: tags=fix,P3,noarch,local
    """
    PARAM_DIC = {"pkg_name": "c-ares c-ares-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > query_dns.c <<EOF
#include <ares.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <arpa/nameser.h>

// A记录的回调
void a_record_callback(void* arg, int status, int timeouts, unsigned char* abuf, int alen) {
    struct hostent* host = NULL;
    char* query = (char*) arg;

    if(status == ARES_SUCCESS) {
        status = ares_parse_a_reply(abuf, alen, &host, NULL, NULL);
        if(status == ARES_SUCCESS) {
            printf("A record for %s: %s\\n", query, inet_ntoa(*(struct in_addr*)host->h_addr));
            free(host);
        }
    }
    if(status != ARES_SUCCESS) {
        fprintf(stderr, "Failed to find A record for %s with status: %s\\n", query, ares_strerror(status));
    }
}

// AAAA记录的回调
void aaaa_record_callback(void* arg, int status, int timeouts, unsigned char* abuf, int alen) {
    struct hostent* host = NULL;
    char ip[INET6_ADDRSTRLEN];
    char* query = (char*) arg;

    if(status == ARES_SUCCESS) {
        status = ares_parse_aaaa_reply(abuf, alen, &host, NULL, NULL);
        if(status == ARES_SUCCESS) {
            inet_ntop(AF_INET6, host->h_addr, ip, INET6_ADDRSTRLEN);
            printf("AAAA record for %s: %s\\n", query, ip);
            free(host);
        }
    }
    if(status != ARES_SUCCESS) {
        fprintf(stderr, "Failed to find AAAA record for %s with status: %s\\n", query, ares_strerror(status));
    }
}

// MX记录的回调
void mx_record_callback(void* arg, int status, int timeouts, unsigned char* abuf, int alen) {
    struct ares_mx_reply* mx_reply = NULL;
    char* query = (char*) arg;

    if(status == ARES_SUCCESS) {
        status = ares_parse_mx_reply(abuf, alen, &mx_reply);
        if(status == ARES_SUCCESS) {
            printf("MX record for %s: %s with priority %u\\n", query, mx_reply->host, mx_reply->priority);
            ares_free_data(mx_reply);
        }
    }
    if(status != ARES_SUCCESS) {
        fprintf(stderr, "Failed to find MX record for %s with status: %s\\n", query, ares_strerror(status));
    }
}

int main(int argc, char* argv[]) {
    if(argc != 2) {
        printf("Usage: %s <domain>\\n", argv[0]);
        exit(1);
    }

    ares_channel channel;
    int status = ares_library_init(ARES_LIB_INIT_ALL);
    if(status != ARES_SUCCESS) return 1;

    status = ares_init(&channel);
    if(status != ARES_SUCCESS) return 1;

    // 查询指定的DNS记录类型
    ares_query(channel, argv[1], ns_c_in, ns_t_a, a_record_callback, strdup(argv[1]));
    ares_query(channel, argv[1], ns_c_in, ns_t_aaaa, aaaa_record_callback, strdup(argv[1]));
    ares_query(channel, argv[1], ns_c_in, ns_t_mx, mx_record_callback, strdup(argv[1]));

    // 处理响应
    for(;;) {
        struct timeval* tvp, tv;
        fd_set read_fds, write_fds;
        FD_ZERO(&read_fds);
        FD_ZERO(&write_fds);
        int nfds = ares_fds(channel, &read_fds, &write_fds);
        if(nfds == 0)
            break;
        tvp = ares_timeout(channel, NULL, &tv);
        select(nfds, &read_fds, &write_fds, NULL, tvp);
        ares_process(channel, &read_fds, &write_fds);
    }

    // 清理
    ares_destroy(channel);
    ares_library_cleanup();
    return 0;
}
EOF"""
        self.cmd(cmdline)
    
    def test(self):
        self.cmd('gcc -o query_dns query_dns.c -lcares')
        # run with valid domain
        self.cmd('./query_dns www.example.com > result.log 2>&1', ignore_status=True)
        ret_c, ret_o = self.cmd('cat result.log')
        search_ip = "A record for www.example.com:\s+[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+"
        search_ipv6 = "AAAA record for www.example.com:\s[0-9]+:[0-9]+"
        self.assertTrue(re.compile(search_ipv6).search(ret_o), 'check output error1')
        self.assertTrue(re.compile(search_ip).search(ret_o), 'check output error2')
        self.assertTrue('Failed to find MX record for www.example.com with status: DNS server returned answer with no data' in ret_o, 'check output error3')
        # run with invalid domain
        self.cmd('./query_dns invalid_domain > result.log 2>&1', ignore_status=True)
        ret_c, ret_o = self.cmd('cat result.log')
        self.assertIn('Failed to find AAAA record for invalid_domain with status: Domain name not found', ret_o)
        self.assertIn('Failed to find MX record for invalid_domain with status: Domain name not found', ret_o)
        self.assertIn('Failed to find A record for invalid_domain with status: Could not contact DNS servers', ret_o)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf query_dns* result.log')
