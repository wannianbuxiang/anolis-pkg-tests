#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_c-ares_fun_002.py
@Time:      2024/04/09 14:31:50
@Author:    zhixin
@Version:   1.0
@Contact:   zx01468051@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhixin
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_c-ares_fun_002.yaml for details

    :avocado: tags=fix,P3,noarch,local
    """
    PARAM_DIC = {"pkg_name": "c-ares c-ares-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > nonvalid.c <<EOF
#include <ares.h>
#include <stdio.h>
#include <arpa/nameser.h>

void query_callback(void *arg, int status, int timeouts, unsigned char *abuf, int alen) {
    char* domain = (char*)arg;
    if (status != ARES_SUCCESS) {
        fprintf(stderr, "Domain %s: %s\\n", domain, ares_strerror(status));
    } else {
        fprintf(stderr, "Domain %s: Unexpected successful response\\n", domain);
    }
}

int main(void) {
    ares_channel channel;
    int status;

    status = ares_library_init(ARES_LIB_INIT_ALL);
    if (status != ARES_SUCCESS) {
        fprintf(stderr, "ares_library_init failed: %s\\n", ares_strerror(status));
        return 1;
    }

    status = ares_init(&channel);
    if (status != ARES_SUCCESS) {
        fprintf(stderr, "ares_init failed: %s\\n", ares_strerror(status));
        return 1;
    }

    // 准备一组无效域名进行测试
    const char* invalid_domains[] = {
        "invalid_domain",            // 格式错误
        "unlikely-exist-123457.com", // 很可能不存在
        "no-this-site-abcdefg.net"   // 很可能不存在
    };

    for (int i = 0; i < sizeof(invalid_domains)/sizeof(invalid_domains[0]); ++i) {
        ares_query(channel, invalid_domains[i], ns_c_in, ns_t_a, query_callback,
                   (void*)invalid_domains[i]);
    }

    // 事件循环处理DNS响应
    for (;;) {
        struct timeval *tvp, tv;
        fd_set read_fds, write_fds;

        FD_ZERO(&read_fds);
        FD_ZERO(&write_fds);
        int nfds = ares_fds(channel, &read_fds, &write_fds);
        if (nfds == 0)
            break;

        tvp = ares_timeout(channel, NULL, &tv);
        select(nfds, &read_fds, &write_fds, NULL, tvp);
        ares_process(channel, &read_fds, &write_fds);
    }

    ares_destroy(channel);
    ares_library_cleanup();
    return 0;
}
EOF"""

        self.cmd(cmdline)
    
    def test(self):
        self.cmd('gcc -o nonvalid nonvalid.c -lcares')
        self.cmd('./nonvalid > result.log 2>&1', ignore_status=True)
        ret_c, ret_o = self.cmd('cat result.log')
        self.assertTrue('Domain no-this-site-abcdefg.net: Domain name not found' in ret_o, 'check output error1.')
        self.assertTrue('Domain unlikely-exist-123457.com: Domain name not found' in ret_o, 'check output error2.')
        self.assertTrue('Domain invalid_domain: Could not contact DNS servers' in ret_o, 'check output error3.')

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf nonvalid* result.log')
