#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_c-ares_fun_004.py
@Time:      2024/04/10 17:31:50
@Author:    zhixin
@Version:   1.0
@Contact:   zx01468051@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhixin
"""

from common.basetest import LocalTest
import re

class Test(LocalTest):
    """
    See tc_c-ares_fun_004.yaml for details

    :avocado: tags=fix,P3,noarch,local
    """
    PARAM_DIC = {"pkg_name": "c-ares c-ares-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > query.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include <ares.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <arpa/nameser.h>

void callback(void *arg, int status, int timeouts, struct hostent *host) {
    if(!host || status != ARES_SUCCESS) {
        printf("Failed to lookup %s\\n", ares_strerror(status));
        return;
    }

    printf("Found address name %s\\n", host->h_name);
    for (int i = 0; host->h_addr_list[i]; ++i) {
        char ip[INET6_ADDRSTRLEN];
        inet_ntop(host->h_addrtype, host->h_addr_list[i], ip, sizeof(ip));
        printf("%s\\n", ip);
    }
}

int main(int argc, char **argv)
{
    if(argc != 2) {
        printf("Usage: %s <domain>\\n", argv[0]);
        exit(1);
    }

    ares_channel channel;
    int status, nfds, count;
    fd_set read_fds, write_fds;
    struct timeval *tvp, tv;

    /* 初始化 c-ares 库 */
    status = ares_library_init(ARES_LIB_INIT_ALL);
    if (status != ARES_SUCCESS) {
        fprintf(stderr, "ares_library_init: %s\\n", ares_strerror(status));
        return 1;
    }

    /* 创建 channel */
    status = ares_init(&channel);
    if (status != ARES_SUCCESS) {
        fprintf(stderr, "ares_init: %s\\n", ares_strerror(status));
        return 1;
    }

    /* 设置不同的地址类型 */
    ares_gethostbyname(channel, argv[1], AF_INET, callback, NULL);
    ares_gethostbyname(channel, argv[1], AF_INET6, callback, NULL);

    /* 主循环处理 */
    while (1) {
        FD_ZERO(&read_fds);
        FD_ZERO(&write_fds);
        nfds = ares_fds(channel, &read_fds, &write_fds);
        if (nfds == 0) {
            break;
        }
        tvp = ares_timeout(channel, NULL, &tv);
        count = select(nfds, &read_fds, &write_fds, NULL, tvp);
        if (count <= 0) {
            continue;
        }
        ares_process(channel, &read_fds, &write_fds);
    }

    ares_destroy(channel);
    ares_library_cleanup();
    return 0;
}
EOF"""
        self.cmd(cmdline)
    
    def test(self):
        self.cmd('gcc -o query query.c -lcares')
        # run with valid domain
        self.cmd('./query www.example.com > result.log 2>&1')
        ret_c, ret_o = self.cmd('cat result.log')
        search_ip = "Found address name www.example.com\\n[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+"
        search_ipv6 = "Found address name www.example.com\\n[0-9]+:[0-9]+"
        self.assertTrue(re.compile(search_ipv6).search(ret_o), 'check output error1')
        self.assertTrue(re.compile(search_ip).search(ret_o), 'check output error2')
        # run with invalid domain
        self.cmd('./query invalid_domain > result.log 2>&1', ignore_status=True)
        ret_c, ret_o = self.cmd('cat result.log')
        self.assertIn('Failed to lookup Misformatted domain name', ret_o)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf query* result.log')
