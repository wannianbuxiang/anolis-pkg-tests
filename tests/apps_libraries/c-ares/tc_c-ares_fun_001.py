#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_c-ares_fun_001.py
@Time:      2024/04/08 14:51:50
@Author:    zhixin
@Version:   1.0
@Contact:   zx01468051@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    zhixin
"""

from common.basetest import LocalTest
import re

class Test(LocalTest):
    """
    See tc_c-ares_fun_001.yaml for details

    :avocado: tags=fix,P3,noarch,local
    """
    PARAM_DIC = {"pkg_name": "c-ares c-ares-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > dnsquery.c <<EOF
#include <stdio.h>
#include <ares.h>
#include <netdb.h>
#include <arpa/inet.h>

void callback(void *arg, int status, int timeouts, struct hostent *host) {
    if(!host || status != ARES_SUCCESS) {
        printf("Failed to lookup: %s\\n", ares_strerror(status));
        return;
    }

    printf("Found address name %s\\n", host->h_name);
    for (int i = 0; host->h_addr_list[i]; ++i) {
        struct in_addr addr;
        addr.s_addr = *(in_addr_t *) host->h_addr_list[i];
        printf("Found address: %s\\n", inet_ntoa(addr));
    }
}

int main(int argc, char **argv) {
    ares_channel channel;
    int status;

    // 初始化 c-ares
    status = ares_library_init(ARES_LIB_INIT_ALL);
    if (status != ARES_SUCCESS) {
        printf("ares_library_init: %s\\n", ares_strerror(status));
        return 1;
    }

    // 创建 c-ares 通道
    status = ares_init(&channel);
    if(status != ARES_SUCCESS) {
        printf("ares_init: %s\\n", ares_strerror(status));
        return 1;
    }

    // 开始解析
    ares_gethostbyname(channel, "www.example.com", AF_INET, callback, NULL);

    // 等待所有解析完成
    for(;;) {
        struct timeval *tvp, tv;
        fd_set read_fds, write_fds;
        int nfds;

        FD_ZERO(&read_fds);
        FD_ZERO(&write_fds);
        nfds = ares_fds(channel, &read_fds, &write_fds);

        if(nfds == 0) {
            break;
        }

        tvp = ares_timeout(channel, NULL, &tv);
        select(nfds, &read_fds, &write_fds, NULL, tvp);

        // 处理事件
        ares_process(channel, &read_fds, &write_fds);
    }

    // 清理
    ares_destroy(channel);
    ares_library_cleanup();
    return 0;
}
EOF"""

        self.cmd(cmdline)
    
    def test(self):
        self.cmd('gcc -o dnsquery dnsquery.c -lcares')
        ret_c, ret_o = self.cmd('ldd ./dnsquery')
        self.assertTrue('libcares.so.2' in ret_o, 'check output error1.')
        ret_c, ret_o = self.cmd('./dnsquery')
        self.assertTrue('Found address name www.example.com' in ret_o, 'check output error2.')
        search_ip = "Found address:\s+[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+"
        self.assertTrue(re.compile(search_ip).search(ret_o), 'check output error3.')

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf dnsquery*')
