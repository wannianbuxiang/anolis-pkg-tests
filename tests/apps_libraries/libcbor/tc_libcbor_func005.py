#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libcbor_func005.py
@Time:      2024/04/08 16:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""
import subprocess
from common.basetest import LocalTest

class Test(LocalTest): 
    """
    See tc_libcbor_func005.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libcbor libcbor-devel"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_libcbor.c <<EOF
#include <stdlib.h>
#include "cbor.h"

void usage(void) {
  printf("Usage: streaming_array <N>\\n");
  printf("Prints out serialized array [0, ..., N-1]\\n");
  exit(1);
}

#define BUFFER_SIZE 8
unsigned char buffer[BUFFER_SIZE];
FILE* out;

void flush(size_t bytes) {
  if (bytes == 0) exit(1);  // All items should be successfully encoded
  if (fwrite(buffer, sizeof(unsigned char), bytes, out) != bytes) exit(1);
  if (fflush(out)) exit(1);
}


int main(int argc, char* argv[]) {
  if (argc != 2) usage();
  long n = strtol(argv[1], NULL, 10);
  out = freopen(NULL, "wb", stdout);
  if (!out) exit(1);

  // Start an indefinite-length array
  flush(cbor_encode_indef_array_start(buffer, BUFFER_SIZE));
  printf("\\ncbor \\n");
  for (size_t i = 0; i < n; i++) {
    flush(cbor_encode_uint32(i, buffer, BUFFER_SIZE));   
  }

  // Close the array
  flush(cbor_encode_break(buffer, BUFFER_SIZE));
  printf("\\nthe input is <%d>\\n", n);
  if (fclose(out)) exit(1); 
}
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o /tmp/ghm/test_libcbor /tmp/ghm/test_libcbor.c -lcbor")
        ret_o1 = subprocess.run("/tmp/ghm/test_libcbor", capture_output=True, shell=True)
        self.re = b'Usage: streaming_array <N>'
        self.assertIn(self.re, ret_o1.stdout)
        ret_o2 = subprocess.run("/tmp/ghm/test_libcbor 8", capture_output=True, shell=True)
        self.re1 = b'the input is <8>'
        self.re2 = b'cbor'
        self.assertIn(self.re1, ret_o2.stdout)
        self.assertIn(self.re2, ret_o2.stdout)
        self.log.info(ret_o2.stdout)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")