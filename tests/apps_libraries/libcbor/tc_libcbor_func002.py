#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libcbor_func002.py
@Time:      2024/04/03 18:02:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest): 
    """
    See tc_libcbor_func002.yaml for details

 
    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libcbor libcbor-devel"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_libcbor.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include "cbor.h"

int compareUint(const void *a, const void *b) {
  uint8_t av = cbor_get_uint8(*(cbor_item_t **)a),
          bv = cbor_get_uint8(*(cbor_item_t **)b);

  if (av < bv)
    return -1;
  else if (av == bv)
    return 0;
  else
    return 1;
}

int main(void) {
  cbor_item_t *array = cbor_new_definite_array(4);
  bool success = cbor_array_push(array, cbor_move(cbor_build_uint8(4)));
  success &= cbor_array_push(array, cbor_move(cbor_build_uint8(3)));
  success &= cbor_array_push(array, cbor_move(cbor_build_uint8(1)));
  success &= cbor_array_push(array, cbor_move(cbor_build_uint8(2)));
  if (!success) return 1;

  qsort(cbor_array_handle(array), cbor_array_size(array), sizeof(cbor_item_t *),
        compareUint);

  cbor_describe(array, stdout);
  fflush(stdout);
}
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o /tmp/ghm/test_libcbor /tmp/ghm/test_libcbor.c -lcbor")
        ret_c1, ret_o1 = self.cmd("/tmp/ghm/test_libcbor")
        expected_result1 = """
 [CBOR_TYPE_ARRAY] Definite, size: 4
    [CBOR_TYPE_UINT] Width: 1B, Value: 1
    [CBOR_TYPE_UINT] Width: 1B, Value: 2
    [CBOR_TYPE_UINT] Width: 1B, Value: 3
    [CBOR_TYPE_UINT] Width: 1B, Value: 4
"""
        actual_lines = ret_o1.strip().split('\n')
        expected_lines = expected_result1.strip().split('\n')
        self.assertEqual(len(actual_lines), len(expected_lines))
        for actual_line, expected_line in zip(actual_lines, expected_lines):
            self.assertEqual(actual_line, expected_line)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")