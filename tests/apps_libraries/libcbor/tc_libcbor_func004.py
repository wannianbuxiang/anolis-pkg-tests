  #!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libcbor_func004.py
@Time:      2024/04/08 13:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

import subprocess
from common.basetest import LocalTest

class Test(LocalTest): 
    """
    See tc_libcbor_func004.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libcbor libcbor-devel"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_libcbor.c <<EOF
#include <stdio.h>
#include "cbor.h"

void usage(void) {
  printf("Usage: readfile [input file]\\n");
  exit(1);
}

int main(int argc, char* argv[]) {
  if (argc != 2) usage();
  FILE* f = fopen(argv[1], "rb");
  if (f == NULL) usage();
  fseek(f, 0, SEEK_END);
  size_t length = (size_t)ftell(f);
  fseek(f, 0, SEEK_SET);
  unsigned char* buffer = malloc(length);
  fread(buffer, length, 1, f);

  struct cbor_load_result result;
  cbor_item_t* item = cbor_load(buffer, length, &result);
  free(buffer);

  if (result.error.code != CBOR_ERR_NONE) {
    printf(
        "There was an error while reading the input near byte %zu (read %zu "
        "bytes in total): ",
        result.error.position, result.read);
    switch (result.error.code) {
      case CBOR_ERR_MALFORMATED: {
        printf("Malformed data\\n");
        break;
      }
      case CBOR_ERR_MEMERROR: {
        printf("Memory error -- perhaps the input is too large?\\n");
        break;
      }
      case CBOR_ERR_NODATA: {
        printf("The input is empty\\n");
        break;
      }
      case CBOR_ERR_NOTENOUGHDATA: {
        printf("Data seem to be missing -- is the input complete?\\n");
        break;
      }
      case CBOR_ERR_SYNTAXERROR: {
        printf("Syntactically malformed data -- see\\n");
        break;
      }
      case CBOR_ERR_NONE: {
        break;
      }
    }
    exit(1);
  }

  cbor_describe(item, stdout);
  fflush(stdout);
  cbor_decref(&item);

  fclose(f);
}
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("echo 'ehello' > /tmp/ghm/test01.cbor")
        self.cmd("touch /tmp/ghm/test02.cbor")
        self.cmd("echo 't2024-04-08T20:04:00Z' > /tmp/ghm/test03.cbor")
        self.cmd("echo 't1234566' > /tmp/ghm/test04.cbor")
        self.cmd("gcc -o /tmp/ghm/test_libcbor /tmp/ghm/test_libcbor.c -lcbor")
        ret_c1, ret_o1 = self.cmd("/tmp/ghm/test_libcbor /tmp/ghm/test01.cbor")
        self.assertIn("CBOR_TYPE_STRING", ret_o1)
        self.assertIn("hello", ret_o1)
        ret_o2 = subprocess.run("/tmp/ghm/test_libcbor /tmp/ghm/test02.cbor", capture_output=True, shell=True, text=True)
        self.assertIn("The input is empty", ret_o2.stdout)
        self.log.info(ret_o2.stdout)
        ret_c3, ret_o3 = self.cmd("/tmp/ghm/test_libcbor /tmp/ghm/test03.cbor")
        self.assertIn("2024-04-08T20:04:00", ret_o3)
        ret_o4 = subprocess.run("/tmp/ghm/test_libcbor /tmp/ghm/test04.cbor", capture_output=True, shell=True, text=True)
        self.assertIn("Data seem to be missing -- is the input complete?", ret_o4.stdout)
        self.log.info(ret_o4.stdout)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")