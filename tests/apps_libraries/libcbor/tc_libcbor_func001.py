#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libcbor_func001.py
@Time:      2024/04/03 16:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""
from common.basetest import LocalTest

class Test(LocalTest): 
    """
    See tc_libcbor_func001.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libcbor libcbor-devel"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_libcbor.c <<EOF
#include <stdio.h>
#include "cbor.h"

int main(void) {
  printf("Hello from libcbor %s\\n", CBOR_VERSION);
  printf("Pretty-printer support: %s\\n", CBOR_PRETTY_PRINTER ? "yes" : "no");
  printf("Buffer growth factor: %f\\n", (float)CBOR_BUFFER_GROWTH);
}
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o /tmp/ghm/test_libcbor /tmp/ghm/test_libcbor.c -lcbor")
        ret_c1, ret_o1 = self.cmd("/tmp/ghm/test_libcbor")
        self.assertIn("Hello from libcbor", ret_o1)
        self.assertIn("Pretty-printer support", ret_o1)
        self.assertIn("Buffer growth factor", ret_o1)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")