#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libcbor_func003.py
@Time:      2024/04/07 10:02:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

import subprocess
from common.basetest import LocalTest

class Test(LocalTest): 
    """
    See tc_libcbor_func003.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libcbor libcbor-devel"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_libcbor.c <<EOF
#include <stdio.h>
#include "cbor.h"

int main(void) {
  cbor_item_t* root = cbor_new_definite_map(2);
  bool success = cbor_map_add(
      root, (struct cbor_pair){
                .key = cbor_move(cbor_build_string("Is CBOR awesome?\\n")),
                .value = cbor_move(cbor_build_bool(true))});
  success &= cbor_map_add(
      root, (struct cbor_pair){
                .key = cbor_move(cbor_build_uint8(42)),
                .value = cbor_move(cbor_build_string("Is the answer\\n"))});
  if (!success) return 1;
  unsigned char* buffer;
  size_t buffer_size;
  cbor_serialize_alloc(root, &buffer, &buffer_size);

  fwrite(buffer, 1, buffer_size, stdout);
  free(buffer);

  fflush(stdout);
  cbor_decref(&root);
}
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o /tmp/ghm/test_libcbor /tmp/ghm/test_libcbor.c -lcbor")
        ret_o1 = subprocess.run("/tmp/ghm/test_libcbor", capture_output=True, shell=True)
        self.log.info(type(ret_o1.stdout))
        self.a = b'Is CBOR awesome?'
        self.b = b'Is the answer'
        self.assertIn(self.a, ret_o1.stdout)
        self.assertIn(self.b, ret_o1.stdout)
        self.log.info(ret_o1.stdout)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")