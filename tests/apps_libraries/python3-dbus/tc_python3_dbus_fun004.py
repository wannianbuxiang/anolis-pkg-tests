# -*- encoding: utf-8 -*-

"""
@File:      tc_python3_dbus_fun004.py
@Time:      2024-04-19 16:37:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

import time
import subprocess
import os
import dbus
import dbus.service
import dbus.mainloop.glib
from gi.repository import GLib
from multiprocessing import Process
from common.basetest import LocalTest


# D-Bus Service
SERVICE_BUS_NAME = "com.test.TestSignal"
SERVICE_OBJECT_PATH = "/com/test/TestSignal"
SERVICE_INTERFACE_NAME = "com.test.TestSignal.Interface"

# Initialize the main loop
dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

class TestSignalService(dbus.service.Object):
    def __init__(self, bus, object_path):
        dbus.service.Object.__init__(self, bus, object_path)

    @dbus.service.signal(SERVICE_INTERFACE_NAME)
    def Greeting(self, message: str):
        """
        This signal is emitted whenever the service sends a greeting
        """
        pass

    def emit_greeting(self, message):
        """
        Method to emit the 'Greeting' signal
        """
        self.Greeting(message)

def signal_handler(message):
    global received_message
    received_message = message
    print(f"Received signal with message: {message}")

def run_dbus_service_loop():
    bus = dbus.SessionBus()
    name = dbus.service.BusName(SERVICE_BUS_NAME, bus)
    service = TestSignalService(bus, SERVICE_OBJECT_PATH)
    
    # Set up some code to send signal after a short wait
    GLib.timeout_add(2000, service.emit_greeting, "Hello World from signal!")
    
    loop = GLib.MainLoop()
    loop.run()

class Test(LocalTest):
    """
    See tc_python3_dbus_fun004.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    PARAM_DIC = {"pkg_name": "python3-dbus dbus-x11 xorg-x11-server-Xvfb"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        #模拟显示器
        self.xvfb_process = subprocess.Popen(['Xvfb', ':99', '-screen', '0', '1024x768x16'])
        time.sleep(2)
        os.environ['DISPLAY'] = ':99'
        global received_message
        received_message = None
        self.process = Process(target=run_dbus_service_loop)
        self.process.start()
        time.sleep(1)

    def test(self):
        bus = dbus.SessionBus()
        bus.add_signal_receiver(signal_handler,
                                dbus_interface=SERVICE_INTERFACE_NAME,
                                signal_name="Greeting")

        # Run the main loop for a short while to wait for the signal
        loop = GLib.MainLoop()
        GLib.timeout_add(3000, loop.quit)  # End the loop after 3 seconds
        loop.run()

        self.assertIsNotNone(received_message, "Signal was not received.")
        self.assertEqual(received_message, "Hello World from signal!")
        
        if self.xvfb_process:
            try:
                self.xvfb_process.terminate()
                self.xvfb_process.wait(timeout=5)  # 等待进程退出，最多等待 5 秒
                print("Xvfb 已停止")
            except subprocess.TimeoutExpired:
                print("Xvfb 未在 5 秒内响应终止信号，强制终止...")
                self.xvfb_process.kill()
                self.xvfb_process.wait()  # 确保进程已完全退出
                print("Xvfb 已强制停止")
        else:
            print("Xvfb 未启动")  

    def tearDown(self):
        self.process.terminate()
        self.process.join()
        super().tearDown(self.PARAM_DIC)
        os.unsetenv('DISPLAY')
        self.cmd("systemctl enable dbus-broker")
        if not self.cmd("systemctl is-active --quiet dbus-broker", ignore_status=True):
            self.cmd("systemctl restart dbus-broker")
        else:
            self.log.info("dbus-broker.service is not active")
        self.cmd("systemctl restart dbus")

