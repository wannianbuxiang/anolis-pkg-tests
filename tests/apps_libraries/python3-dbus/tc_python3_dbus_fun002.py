# -*- encoding: utf-8 -*-

"""
@File:      tc_python3_dbus_fun002.py
@Time:      2024-04-19 16:37:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

import time
import dbus
import subprocess
import os
import time
import dbus.service
import dbus.mainloop.glib
from gi.repository import GLib
from multiprocessing import Process
from common.basetest import LocalTest


# D-Bus Service
SERVICE_BUS_NAME = "com.test.HelloWorld"
SERVICE_OBJECT_PATH = "/com/test/HelloWorld"
SERVICE_INTERFACE_NAME = "com.test.HelloWorld.Interface"

class HelloWorldService(dbus.service.Object):
    @dbus.service.method(SERVICE_INTERFACE_NAME, in_signature="", out_signature="s")
    def SayHello(self):
        return "Hello from D-Bus Service!"


# Run the DBus service loop in a separate process
def run_dbus_service_loop():
    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
    bus = dbus.SessionBus()
    name = dbus.service.BusName(SERVICE_BUS_NAME, bus)
    service = HelloWorldService(bus, SERVICE_OBJECT_PATH)
    loop = GLib.MainLoop()
    loop.run()


# Method to check whether the D-Bus service is up and can be found by clients
def check_service_registration():
    bus = dbus.SessionBus()
    try:
        # 获取 D-Bus 总线守护进程的对象
        bus_object = bus.get_object('org.freedesktop.DBus', '/org/freedesktop/DBus')
        # 使用 D-Bus 接口调用 ListNames 方法
        dbus_interface = dbus.Interface(bus_object, 'org.freedesktop.DBus')
        services_list = dbus_interface.ListNames()
        print(services_list)
        if SERVICE_BUS_NAME in services_list:
            print("Success: Service has been found on D-Bus.")
        return True
    except dbus.exceptions.DBusException as e:
        print(f"Failure: Service could not be found on D-Bus: {e}")
        return False
    
  

class Test(LocalTest):
    """
    See tc_python3_dbus_fun002.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    PARAM_DIC = {"pkg_name": "python3-dbus dbus-x11 xorg-x11-server-Xvfb"}
    

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        #模拟显示器
        self.xvfb_process = subprocess.Popen(['Xvfb', ':99', '-screen', '0', '1024x768x16'])
        time.sleep(2)
        os.environ['DISPLAY'] = ':99'
        # 创建并启动 D-Bus 服务进程
        self.dbus_service_process = Process(target=run_dbus_service_loop)
        self.dbus_service_process.start()
        # 等待服务启动
        time.sleep(1)
        
        

    def test(self):
        service_found = check_service_registration()
        self.assertTrue(
            service_found,
            "DBus service is not successfully registered on the session bus.",
        )
        if self.xvfb_process:
            try:
                self.xvfb_process.terminate()
                self.xvfb_process.wait(timeout=5)  # 等待进程退出，最多等待 5 秒
                print("Xvfb 已停止")
            except subprocess.TimeoutExpired:
                print("Xvfb 未在 5 秒内响应终止信号，强制终止...")
                self.xvfb_process.kill()
                self.xvfb_process.wait()  # 确保进程已完全退出
                print("Xvfb 已强制停止")
        else:
            print("Xvfb 未启动")

    def tearDown(self):
        self.dbus_service_process.terminate()
        self.dbus_service_process.join()
        super().tearDown(self.PARAM_DIC)
        os.unsetenv('DISPLAY')
        self.cmd("systemctl enable dbus-broker")
        if not self.cmd("systemctl is-active --quiet dbus-broker", ignore_status=True):
            self.cmd("systemctl restart dbus-broker")
        else:
            self.log.info("dbus-broker.service is not active")
        self.cmd("systemctl restart dbus")
