# -*- encoding: utf-8 -*-

"""
@File:      tc_python3_dbus_fun007.py
@Time:      2024-04-19 16:37:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

import time
import subprocess
import os
import dbus
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_python3_dbus_fun007.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    PARAM_DIC = {"pkg_name": "python3-dbus dbus-x11 xorg-x11-server-Xvfb"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        #模拟显示器
        self.xvfb_process = subprocess.Popen(['Xvfb', ':99', '-screen', '0', '1024x768x16'])
        time.sleep(2)
        os.environ['DISPLAY'] = ':99'
        self.bus = dbus.SessionBus()
        
    def test(self):
        service_name = "org.freedesktop.DBus"
        self.assertTrue(self.bus.name_has_owner(service_name), f"The service '{service_name}' is not registered on the D-Bus.")
        
        if self.xvfb_process:
            try:
                self.xvfb_process.terminate()
                self.xvfb_process.wait(timeout=5)  # 等待进程退出，最多等待 5 秒
                print("Xvfb 已停止")
            except subprocess.TimeoutExpired:
                print("Xvfb 未在 5 秒内响应终止信号，强制终止...")
                self.xvfb_process.kill()
                self.xvfb_process.wait()  # 确保进程已完全退出
                print("Xvfb 已强制停止")
        else:
            print("Xvfb 未启动")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("systemctl enable dbus-broker")
        if not self.cmd("systemctl is-active --quiet dbus-broker", ignore_status=True):
            self.cmd("systemctl restart dbus-broker")
        else:
            self.log.info("dbus-broker.service is not active")
        self.cmd("systemctl restart dbus")

