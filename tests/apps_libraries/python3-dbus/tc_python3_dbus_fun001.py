# -*- encoding: utf-8 -*-

"""
@File:      tc_python3_dbus_fun001.py
@Time:      2024-04-19 10:22:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
import time
import dbus
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_python3_dbus_fun001.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """


    PARAM_DIC = {"pkg_name": "python3-dbus"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        max_retry = 3
        retry_count = 0
        while retry_count < max_retry:
            code, output = self.cmd("systemctl is-active NetworkManager",  ignore_status=True)
            if code == 0 :
                self.log.info("NetworkManager is active")
                break
            else:
                self.log.info("NetworkManager is not active, restarting...")
                self.cmd("systemctl restart NetworkManager")
                retry_count += 1
                time.sleep(1)
        else:
            self.fail("Failed to activate NetworkManager after multiple retries")
        # 获取系统总线
        self.system_bus = dbus.SystemBus()

    def test(self):
        # 获取 NetworkManager 服务的对象
        nm = self.system_bus.get_object('org.freedesktop.NetworkManager', '/org/freedesktop/NetworkManager')
        # 接口描述
        nm_iface = dbus.Interface(nm, dbus_interface='org.freedesktop.NetworkManager')
        # 等待网络重新连接成功
        max_wait_time = 10
        start_time = time.time()
        while (time.time() - start_time) < max_wait_time:
            state = nm_iface.state()
            if state == 70:
                self.log.info(f"NetworkManager state: {state}")
                return
            time.sleep(1)
        # 调用对象的方法并打印返回的值
        self.assertEqual(70, state, msg="未成功连接，并且无法访问互联网")


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)

