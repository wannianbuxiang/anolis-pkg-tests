# -*- encoding: utf-8 -*-

"""
@File:      tc_python3_dbus_fun006.py
@Time:      2024-04-19 16:37:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
import dbus
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_python3_dbus_fun006.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """
    pkg_name = 'python3-dbus'
    PARAM_DIC = {"pkg_name": pkg_name}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        
    def test(self):
        dbus_ver = dbus.__version__
        code, Rpm_Ver = self.cmd(f"rpm -qa|grep -i {self.pkg_name}|awk -F- '{{print $3}}'")
        if dbus_ver is not None and Rpm_Ver is not None:
            self.assertEqual(dbus_ver, Rpm_Ver)
        else:
            print("One of the commands executed failed or did not retrieve version information. Please check!")


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
