# -*- encoding: utf-8 -*-

"""
@File:      tc_python3_dbus_fun003.py
@Time:      2024-04-19 10:22:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
import time
import subprocess
import os
import dbus
import logging
import dbus.service
import dbus.mainloop.glib
from gi.repository import GLib
from multiprocessing import Process
from common.basetest import LocalTest


# 具体的 D-Bus 服务设置
SERVICE_BUS_NAME = 'com.test.HelloWorld'
SERVICE_OBJECT_PATH = '/com/test/HelloWorld'
SERVICE_INTERFACE_NAME = 'com.test.HelloWorld.Interface'


class HelloDBusService(dbus.service.Object):
    @dbus.service.method(SERVICE_INTERFACE_NAME, in_signature='', out_signature='s')
    def SayHello(self):
        print('SayHello method was called')
        return "Hello from D-Bus Service!"


def run_dbus_service_loop():
    try:
        dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
        bus = dbus.SessionBus()
        name = dbus.service.BusName(SERVICE_BUS_NAME, bus)
        service = HelloDBusService(bus, SERVICE_OBJECT_PATH)
        loop = GLib.MainLoop()
        loop.run()
    except Exception as e:
        logging.error(f"Error in D-Bus service loop: {e}")

class Test(LocalTest):
    """
    See tc_python3_dbus_fun003.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    PARAM_DIC = {"pkg_name": "python3-dbus dbus-x11 xorg-x11-server-Xvfb"}
  
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        #模拟显示器
        self.xvfb_process = subprocess.Popen(['Xvfb', ':99', '-screen', '0', '1024x768x16'])
        time.sleep(2)
        os.environ['DISPLAY'] = ':99'
        # 创建并启动 D-Bus 服务进程
        self.dbus_service_process = Process(target=run_dbus_service_loop)
        self.dbus_service_process.start()
        # 等待服务启动
        self.wait_for_service_to_start()

    def wait_for_service_to_start(self):
        bus = dbus.SessionBus()
        start_time = time.time()
        while True:
            try:
                proxy_object = bus.get_object(SERVICE_BUS_NAME, SERVICE_OBJECT_PATH)
                dbus.Interface(proxy_object, dbus_interface=SERVICE_INTERFACE_NAME)
                break  # 服务已就绪
            except dbus.exceptions.DBusException as e:
                if time.time() - start_time > 10:  # 超时设置为10秒
                    raise e
                time.sleep(0.5)  # 等待时间减小到0.5秒

    def test(self):
        try:
            bus = dbus.SessionBus()
            proxy_object = bus.get_object(SERVICE_BUS_NAME, SERVICE_OBJECT_PATH)
            dbus_interface = dbus.Interface(proxy_object, dbus_interface=SERVICE_INTERFACE_NAME)
            hello_message = dbus_interface.SayHello()
            self.assertEqual("Hello from D-Bus Service!", hello_message)
            self.log.info(hello_message)
        except Exception as e:
            logging.error(f"Error during test execution: {e}")
        if self.xvfb_process:
            try:
                self.xvfb_process.terminate()
                self.xvfb_process.wait(timeout=5)  # 等待进程退出，最多等待 5 秒
                print("Xvfb 已停止")
            except subprocess.TimeoutExpired:
                print("Xvfb 未在 5 秒内响应终止信号，强制终止...")
                self.xvfb_process.kill()
                self.xvfb_process.wait()  # 确保进程已完全退出
                print("Xvfb 已强制停止")
        else:
            print("Xvfb 未启动")   
        

    def tearDown(self):
        # 关闭 DBus 服务进程
        self.dbus_service_process.terminate()
        self.dbus_service_process.join()
        super().tearDown(self.PARAM_DIC)
        os.unsetenv('DISPLAY')
        self.cmd("systemctl enable dbus-broker")
        if not self.cmd("systemctl is-active --quiet dbus-broker", ignore_status=True):
            self.cmd("systemctl restart dbus-broker")
        else:
            self.log.info("dbus-broker.service is not active")
        self.cmd("systemctl restart dbus")
