# -*- encoding: utf-8 -*-

"""
@File:      tc_libfastjson_libfastjson_func_002.py
@Time:      2024/04/01 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libfastjson_libfastjson_func_002.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "libfastjson libfastjson-devel gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >fjson_test2.c<<"EOF"
#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include <libfastjson/json.h>
int main() {
    // 包含格式错误的 JSON 字符串（缺失括号）
    const char *json_string = "{\\"key\\": \\"value\\"";
    fjson_object *parsed_json;
    struct fjson_tokener *tokener;
    enum fjson_tokener_error error;
    // 初始化 JSON 解析器（tokener）
    tokener = fjson_tokener_new();
    // 尝试解析 JSON 字符串
    parsed_json = fjson_tokener_parse_ex(tokener, json_string, strlen(json_string));
    // 检查解析器是否遇到错误
    error = fjson_tokener_get_error(tokener);
    if (error != fjson_tokener_success) {
        if (error == fjson_tokener_continue) {
            fprintf(stderr, "JSON 解析错误: JSON数据不完整\\n");
        } else {
            // 报告其他错误信息而不是崩溃
            fprintf(stderr, "JSON 解析错误: %s\\n", fjson_tokener_error_desc(error));
        }
        fjson_tokener_free(tokener);
        if (parsed_json != NULL) {
            fjson_object_put(parsed_json);
        }
        return EXIT_FAILURE;
    }
    // 如果没有错误，打印 JSON 结构
    printf("解析的 JSON 结构：%s\\n", fjson_object_to_json_string_ext(parsed_json, FJSON_TO_STRING_PRETTY));
    // 释放解析器和 JSON 对象的内存
    fjson_tokener_free(tokener);
    fjson_object_put(parsed_json);
    return 0;
}
EOF'''
        self.cmd(cmdline)


    def test(self):
        self.cmd("gcc -o fjson_test2 fjson_test2.c -lfastjson")
        code, fjson_result = self.cmd("./fjson_test2", ignore_status=True)
        self.assertIn('JSON 解析错误: JSON数据不完整', fjson_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf fjson_test2.c fjson_test2")
