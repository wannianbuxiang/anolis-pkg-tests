#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_protobuf_c_func001.py
@Time:      2024/04/01 10:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_protobuf_c_func001.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "snappy snappy-devel"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline1 = """cat > /tmp/ghm/test_snappy_null.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include "snappy-c.h"

int main() {
    // 定义一个空字符串
    char empty_input[1] = {0};
    size_t input_len = 0;

    // 获取压缩后的最大长度（对空输入而言，这个值应该很小，但我们仍然计算它）
    size_t compressed_len = snappy_max_compressed_length(input_len);

    // 分配压缩缓冲区
    char* compressed_buffer = malloc(compressed_len);
    if (compressed_buffer == NULL) {
        fprintf(stderr, "Failed to allocate memory for compressed data.\\n");
        return EXIT_FAILURE;
    }

    // 压缩空字符串
    snappy_status compress_status = snappy_compress(empty_input, input_len, compressed_buffer, &compressed_len);
    if (compress_status != SNAPPY_OK) {
        fprintf(stderr, "Failed to compress empty input properly: %d\\n", compress_status);
        free(compressed_buffer);
        return EXIT_FAILURE;
    }

    // 因为snappy_compress在实际压缩空输入时会更新compressed_length，所以这里可以直接使用
    // 但也可以选择保留原样，因为对空输入来说，压缩后的长度总是0

    // 计算解压缩所需长度
    size_t decompressed_length;
    snappy_status status = snappy_uncompressed_length(compressed_buffer, compressed_len, &decompressed_length);
    // 注意：这里使用了正确的变量名 compressed_len 而不是之前错误的 compressed_length

    if (status != SNAPPY_OK) {
        fprintf(stderr, "Failed to get decompressed length for empty input: %d\\n", status);
        free(compressed_buffer);
        return EXIT_FAILURE;
    }

    // 验证解压缩长度为0
    if (decompressed_length != 0) {
        fprintf(stderr, "Unexpected decompressed length for empty input: %zu (expected 0)\\n", decompressed_length);
        free(compressed_buffer);
        return EXIT_FAILURE;
    }

    printf("Successfully compressed and decompressed empty input.\\n");

    // 无需解压缩，因为我们知道结果应为空

    free(compressed_buffer);
    return 0;
}
"""
        self.cmd(cmdline1)
    
    def test(self):
        self.cmd("gcc -o /tmp/ghm/test_snappy_null /tmp/ghm/test_snappy_null.c -lsnappy")
        ret_c1, ret_o1 = self.cmd("/tmp/ghm/test_snappy_null")
        self.assertIn("Successfully compressed and decompressed empty input.",ret_o1)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")