#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_protobuf_c_func002.py
@Time:      2024/03/29 10:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_protobuf_c_func002.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "snappy snappy-devel"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_snappy02.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "snappy-c.h"

#define BLOCK_SIZE (1024 * 1024 * 256)
#define INPUT_FILE "/tmp/ghm/snappy.txt"
#define COMPRESSED_OUTPUT_FILE "/tmp/ghm/snappy_compressed.snappy"
#define DECOMPRESSED_OUTPUT_FILE "/tmp/ghm/snappy_decompressed.txt"

void test_snappy_512mb_file(void) {
    FILE *in_file = fopen(INPUT_FILE, "rb");
    if (!in_file) {
        perror("Failed to open input file");
        exit(EXIT_FAILURE);
    }

    FILE *out_compressed_file = fopen(COMPRESSED_OUTPUT_FILE, "wb");
    if (!out_compressed_file) {
        perror("Failed to open compressed output file");
        fclose(in_file);
        exit(EXIT_FAILURE);
    }

    FILE *out_decompressed_file = fopen(DECOMPRESSED_OUTPUT_FILE, "wb");
    if (!out_decompressed_file) {
        perror("Failed to open decompressed output file");
        fclose(in_file);
        fclose(out_compressed_file);
        exit(EXIT_FAILURE); 
    }

    char *input_block = malloc(BLOCK_SIZE);
    char *compressed_block = malloc(snappy_max_compressed_length(BLOCK_SIZE));
    size_t compressed_length;

    // 分块读取、压缩并写入压缩文件
    while (1) {
        size_t read_length = fread(input_block, 1, BLOCK_SIZE, in_file);
        compressed_length = snappy_max_compressed_length(read_length);
        if (read_length == 0) {
            if (feof(in_file)) {
                break;  // 到达文件末尾
            } else {
                perror("Error reading from input file");
                exit(EXIT_FAILURE);
            }
        }

        snappy_status status = snappy_compress(input_block, read_length, compressed_block, &compressed_length);
        if (status != SNAPPY_OK) {
            fprintf(stderr, "Compression failed with status: %d\\n", status);
            exit(EXIT_FAILURE);
        }
        printf("Original size: %zu, compressed actual size: %zu\\n", read_length, compressed_length);
        fwrite(compressed_block, 1, compressed_length, out_compressed_file);
    }

    // 关闭并重新打开压缩文件以进行解压缩
    fclose(out_compressed_file);
    out_compressed_file = fopen(COMPRESSED_OUTPUT_FILE, "rb");

    size_t decompressed_length;
    char *decompressed_block = malloc(BLOCK_SIZE);

    while (1) {
        size_t read_length = fread(compressed_block, 1, BLOCK_SIZE, out_compressed_file);
        if (read_length == 0) {
            if (feof(out_compressed_file)) {
                break;  // 到达文件末尾
            } else {
                perror("Error reading from compressed file");
                exit(EXIT_FAILURE);
            }
        }

        snappy_uncompressed_length(compressed_block, read_length, &decompressed_length);
        decompressed_block = realloc(decompressed_block, decompressed_length);
        printf("Original size: %zu, Decompressed size: %zu\\n", BLOCK_SIZE, decompressed_length);
        snappy_status status = snappy_uncompress(compressed_block, read_length, decompressed_block, &decompressed_length);
        if (status != SNAPPY_OK) {
            fprintf(stderr, "Decompression failed with status: %d\\n", status);
            exit(EXIT_FAILURE);
        }
        printf("Original size: %zu, Decompressed actual size: %zu\\n", BLOCK_SIZE, decompressed_length);
        fwrite(decompressed_block, 1, decompressed_length, out_decompressed_file);
    }
        // 检查解压后的数据是否与原始数据一致
    if (decompressed_length == BLOCK_SIZE && memcmp(input_block, decompressed_block, BLOCK_SIZE) == 0) {
        printf("Compression and Decompression successful, data matches!\\n");
    } else {
        printf("Decompressed data does not match the original data.\\n");
    }

    // 清理资源
    free(input_block);
    free(compressed_block);
    free(decompressed_block);
    fclose(in_file);
    fclose(out_compressed_file);
    fclose(out_decompressed_file);
}

int main() {
    test_snappy_512mb_file();
    return EXIT_SUCCESS;
}
"""
        self.cmd(cmdline)
    
    def test(self):
        self.cmd("yes a | head -c 268435456 > /tmp/ghm/snappy.txt")
        self.cmd("gcc -o /tmp/ghm/test_snappy02 /tmp/ghm/test_snappy02.c -lsnappy")
        ret_c1, ret_o1 = self.cmd("/tmp/ghm/test_snappy02")
        self.assertIn("Compression and Decompression successful, data matches!", ret_o1)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")