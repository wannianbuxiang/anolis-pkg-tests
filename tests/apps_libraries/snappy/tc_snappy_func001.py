#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_protobuf_c_func001.py
@Time:      2024/03/28 14:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_protobuf_c_func001.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "snappy snappy-devel"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_snappy.c <<EOF
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "snappy-c.h"

//TEST_STRING "This is a test string for Snappy compression."
// 定义多行字符串常量
#define TEST_STRING "This is snappy test \
..........\
000000\
success"
#define MAX_COMPRESSION_RATIO 1.1 /* 假设最大压缩比为1.1倍，用于避免解压缓冲区过小 */

int main(void) {

    // 定义原始数据和缓冲区
    const char* original_data = TEST_STRING;
    size_t original_len = strlen(TEST_STRING);
    size_t compressed_len = snappy_max_compressed_length(original_len);
    char* compressed = malloc(compressed_len);
    if (!compressed) {
        fprintf(stderr, "Failed to allocate memory for compressed data.\\n");
        return EXIT_FAILURE;
    }
    size_t decompressed_len = original_len * MAX_COMPRESSION_RATIO;
    char* decompressed = malloc(decompressed_len);
    if (!decompressed) {
        fprintf(stderr, "Failed to allocate memory for decompressed data.\\n");
        free(compressed);
        return EXIT_FAILURE;
    }

    // 压缩原始数据
    snappy_status compress_status = snappy_compress(original_data, original_len, compressed, &compressed_len);
    if (compress_status != SNAPPY_OK) {
        fprintf(stderr, "Failed to compress data");
        goto cleanup;
    }
    printf("Original size: %zu, Compressed size: %zu\\n", original_len, compressed_len);

    // 获取解压缩后的长度
    snappy_status len_status = snappy_uncompressed_length(compressed, compressed_len, &decompressed_len);
    if (len_status != SNAPPY_OK) {
        goto cleanup;
    }
    printf("Original size: %zu, Deompressed size: %zu\\n", original_len, decompressed_len);

    // 解压缩数据
    snappy_status uncompress_status = snappy_uncompress(compressed, compressed_len, decompressed, &decompressed_len);
    if (uncompress_status != SNAPPY_OK) {
        fprintf(stderr, "Failed to decompress data");
        goto cleanup;
    }
    printf("Original size: %zu, Decompressed actual size: %zu\\n", original_len, decompressed_len);

    // 检查解压后的数据是否与原始数据一致
    if (decompressed_len == original_len && memcmp(original_data, decompressed, original_len) == 0) {
        printf("Compression and Decompression successful, data matches!\\n");
    } else {
        printf("Decompressed data does not match the original data.\\n");
    }

cleanup:
    free(compressed);
    free(decompressed);
    return uncompress_status == SNAPPY_OK ? EXIT_SUCCESS : EXIT_FAILURE;
}
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o /tmp/ghm/test_snappy /tmp/ghm/test_snappy.c -lsnappy")
        ret_c1, ret_o1 = self.cmd("/tmp/ghm/test_snappy")
        self.assertIn("Compression and Decompression successful, data matches!", ret_o1)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")