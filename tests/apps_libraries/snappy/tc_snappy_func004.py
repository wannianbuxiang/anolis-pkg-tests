#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_protobuf_c_func001.py
@Time:      2024/04/01 15:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_protobuf_c_func001.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "snappy snappy-devel"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline1 = """cat > /tmp/ghm/test_snappy_small.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "snappy-c.h"

#define TEST_DATA "This is a test string for Snappy buffer size test."
#define TEST_DATA_LEN (strlen(TEST_DATA))

int main(void) {
    // 压缩数据
    char compressed[snappy_max_compressed_length(TEST_DATA_LEN)];
    size_t compressed_len;
    snappy_status compress_status = snappy_compress(TEST_DATA, TEST_DATA_LEN, compressed, &compressed_len);
    if (compress_status != SNAPPY_OK) {
        fprintf(stderr, "Failed to compress data\\n");
        return EXIT_FAILURE;
    }

    // 获取解压缩后的预期长度
    size_t expected_decompressed_len;
    snappy_status len_status = snappy_uncompressed_length(compressed, compressed_len, &expected_decompressed_len);
    if (len_status != SNAPPY_OK) {
        fprintf(stderr, "Failed to get decompressed length\\n");
        return EXIT_FAILURE;
    }

    // 尝试使用比预期长度小的缓冲区解压缩
    size_t small_buffer_len = expected_decompressed_len - 1;
    char small_decompressed[small_buffer_len];

    snappy_status uncompress_status = snappy_uncompress(compressed, compressed_len, small_decompressed, &small_buffer_len);
    if (uncompress_status != SNAPPY_BUFFER_TOO_SMALL) {
        fprintf(stderr, "Expected SNAPPY_BUFFER_TOO_SMALL error, but got: %d\\n", uncompress_status);
        return EXIT_FAILURE;
    }

    printf("Successfully tested that snappy returns SNAPPY_BUFFER_TOO_SMALL when provided with a too small buffer.\\n");

    return EXIT_SUCCESS;
}
"""
        self.cmd(cmdline1)
    
    def test(self):
        self.cmd("gcc -o /tmp/ghm/test_snappy_small /tmp/ghm/test_snappy_small.c -lsnappy")
        ret_c1, ret_o1 = self.cmd("/tmp/ghm/test_snappy_small")
        self.assertIn("Successfully tested that snappy returns SNAPPY_BUFFER_TOO_SMALL when provided with a too small buffer.",ret_o1)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")