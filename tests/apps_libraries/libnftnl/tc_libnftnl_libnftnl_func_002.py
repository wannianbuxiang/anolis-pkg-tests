# -*- encoding: utf-8 -*-

"""
@File:      tc_libnftnl_libnftnl_func_002.py
@Time:      2024/04/15 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libnftnl_libnftnl_func_002.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "libnftnl libnftnl-devel libmnl-devel gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >nft_test2.c<<"EOF"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <linux/netfilter.h>
#include <linux/netfilter/nf_tables.h>
#include <libnftnl/table.h>
#include <libnftnl/chain.h>
#include <libnftnl/rule.h>
#include <libnftnl/expr.h>
#include <libmnl/libmnl.h>

int main() {
    struct mnl_socket *nl;
    struct nftnl_table *table;
    struct nftnl_chain *chain;
    struct nftnl_rule *rule;
    struct nftnl_expr *expr;
    char buf[MNL_SOCKET_BUFFER_SIZE];
    struct nlmsghdr *nlh;
    int ret, seq, family;

    // 使用 libmnl 初始化 netlink
    nl = mnl_socket_open(NETLINK_NETFILTER);
    if (nl == NULL) {
        perror("mnl_socket_open");
        exit(EXIT_FAILURE);
    }

    if (mnl_socket_bind(nl, 0, MNL_SOCKET_AUTOPID) < 0) {
        perror("mnl_socket_bind");
        exit(EXIT_FAILURE);
    }
    seq = (int)time(NULL);
    family = NFPROTO_IPV4;

    // 创建表
    table = nftnl_table_alloc();
    nftnl_table_set_str(table, NFTNL_TABLE_NAME, "filter");
    nftnl_table_set_u32(table, NFTNL_TABLE_FAMILY, family);

    // 创建链
    chain = nftnl_chain_alloc();
    nftnl_chain_set_str(chain, NFTNL_CHAIN_TABLE, "filter");
    nftnl_chain_set_str(chain, NFTNL_CHAIN_NAME, "INPUT");
    nftnl_chain_set_u32(chain, NFTNL_CHAIN_FAMILY, family);
    nftnl_chain_set_u32(chain, NFTNL_CHAIN_HOOKNUM, NF_INET_PRE_ROUTING);
    nftnl_chain_set_u32(chain, NFTNL_CHAIN_PRIO, 0);
    nftnl_chain_set_str(chain, NFTNL_CHAIN_TYPE, "filter");

    // 创建规则
    rule = nftnl_rule_alloc();
    nftnl_rule_set_str(rule, NFTNL_RULE_TABLE, "filter");
    nftnl_rule_set_str(rule, NFTNL_RULE_CHAIN, "INPUT");
    nftnl_rule_set_u32(rule, NFTNL_RULE_FAMILY, family);

    // 创建一个接受所有流量的表达式
    expr = nftnl_expr_alloc("counter");
    nftnl_expr_set_u32(expr, NFTNL_EXPR_CTR_BYTES, 0);
    nftnl_expr_set_u32(expr, NFTNL_EXPR_CTR_PACKETS, 0);
    nftnl_rule_add_expr(rule, expr);

    // 序列化表对象，为向内核发送准备
    nlh = nftnl_table_nlmsg_build_hdr(buf, NFT_MSG_NEWTABLE, family, NLM_F_CREATE|NLM_F_EXCL|NLM_F_ACK, seq++);
    nftnl_table_nlmsg_build_payload(nlh, table);
    ret = mnl_socket_sendto(nl, nlh, nlh->nlmsg_len);
    if (ret == -1) {
        perror("mnl_socket_send");
        exit(EXIT_FAILURE);
    }

    // 序列化链对象，为向内核发送准备
    nlh = nftnl_chain_nlmsg_build_hdr(buf, NFT_MSG_NEWCHAIN, family, NLM_F_CREATE|NLM_F_EXCL|NLM_F_ACK, seq++);
    nftnl_chain_nlmsg_build_payload(nlh, chain);
    ret = mnl_socket_sendto(nl, nlh, nlh->nlmsg_len);
    if (ret == -1) {
        perror("mnl_socket_send");
        exit(EXIT_FAILURE);
    }

    // 序列化规则对象，为向内核发送准备
    nlh = nftnl_rule_nlmsg_build_hdr(buf, NFT_MSG_NEWRULE, family, NLM_F_CREATE|NLM_F_EXCL|NLM_F_ACK, seq++);
    nftnl_rule_nlmsg_build_payload(nlh, rule);
    ret = mnl_socket_sendto(nl, nlh, nlh->nlmsg_len);
    if (ret == -1) {
        perror("mnl_socket_send");
        exit(EXIT_FAILURE);
    }
 
    printf("自定义规则和默认规则创建成功\\n");

    // 释放资源
    nftnl_rule_free(rule);
    nftnl_chain_free(chain);
    nftnl_table_free(table);
    mnl_socket_close(nl);

    return 0;
}
EOF'''
        self.cmd(cmdline)


    def test(self):
        self.cmd("gcc -o nft_test2 nft_test2.c -lnftnl -lmnl")
        code, nft_result = self.cmd("./nft_test2")
        self.assertIn('自定义规则和默认规则创建成功', nft_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf nft_test2.c nft_test2")
