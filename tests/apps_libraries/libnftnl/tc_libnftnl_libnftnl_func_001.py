# -*- encoding: utf-8 -*-

"""
@File:      tc_libnftnl_libnftnl_func_001.py
@Time:      2024/04/12 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libnftnl_libnftnl_func_001.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "libnftnl libnftnl-devel libmnl-devel gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >nft_test1.c<<"EOF"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <unistd.h>
#include <libnftnl/table.h>
#include <libnftnl/chain.h>
#include <libnftnl/rule.h>
#include <libmnl/libmnl.h>
#include <linux/netfilter.h>
#include <linux/netfilter/nf_tables.h>

#ifndef NFPROTO_IPV4
#define NFPROTO_IPV4 2 
#endif

#ifndef NF_NETDEV_INGRESS
#define NF_NETDEV_INGRESS 0
#endif

int main() {
    struct mnl_socket *nl;
    struct nftnl_table *table;
    struct nftnl_chain *chain;
    struct nftnl_rule *rule;
    char buf[MNL_SOCKET_BUFFER_SIZE];
    struct nlmsghdr *nlh;
    int ret;
    uint32_t seq = 0, portid, family = NFPROTO_IPV4;

    // 打开 Netlink 通信
    nl = mnl_socket_open(NETLINK_NETFILTER);
    if (nl == NULL) {
        perror("mnl_socket_open");
        exit(EXIT_FAILURE);
    }

    if (mnl_socket_bind(nl, 0, MNL_SOCKET_AUTOPID) < 0) {
        perror("mnl_socket_bind");
        exit(EXIT_FAILURE);
    }
    portid = mnl_socket_get_portid(nl);
    
    // 创建表
    table = nftnl_table_alloc();
    nftnl_table_set_u32(table, NFTNL_TABLE_FAMILY, family);
    nftnl_table_set_str(table, NFTNL_TABLE_NAME, "mytable");

    nlh = nftnl_table_nlmsg_build_hdr(buf, NFT_MSG_NEWTABLE, 
                      family, NLM_F_CREATE | NLM_F_EXCL | NLM_F_ACK, seq++);
    nftnl_table_nlmsg_build_payload(nlh, table);
    ret = mnl_socket_sendto(nl, nlh, nlh->nlmsg_len);
    if (ret < 0) {
        perror("mnl_socket_sendto");
        exit(EXIT_FAILURE);
    }

    // 创建链
    chain = nftnl_chain_alloc();
    nftnl_chain_set_str(chain, NFTNL_CHAIN_TABLE, "mytable");
    nftnl_chain_set_str(chain, NFTNL_CHAIN_NAME, "mychain");
    nftnl_chain_set_u32(chain, NFTNL_CHAIN_HOOKNUM, NF_NETDEV_INGRESS);
    nftnl_chain_set_u32(chain, NFTNL_CHAIN_PRIO, 0);
    nftnl_chain_set_u32(chain, NFTNL_CHAIN_FAMILY, family);

    nlh = nftnl_chain_nlmsg_build_hdr(buf, NFT_MSG_NEWCHAIN, 
                      family, NLM_F_CREATE | NLM_F_EXCL | NLM_F_ACK, seq++);
    nftnl_chain_nlmsg_build_payload(nlh, chain);
    ret = mnl_socket_sendto(nl, nlh, nlh->nlmsg_len);
    if (ret < 0) {
        perror("mnl_socket_sendto");
        exit(EXIT_FAILURE);
    }

    // 创建规则
    rule = nftnl_rule_alloc();
    nftnl_rule_set_u32(rule, NFTNL_RULE_FAMILY, family);
    nftnl_rule_set_str(rule, NFTNL_RULE_TABLE, "mytable");
    nftnl_rule_set_str(rule, NFTNL_RULE_CHAIN, "mychain");
    
    nlh = nftnl_rule_nlmsg_build_hdr(buf, NFT_MSG_NEWRULE, 
                     family, NLM_F_APPEND | NLM_F_CREATE | NLM_F_ACK, seq++);
    nftnl_rule_nlmsg_build_payload(nlh, rule);
    ret = mnl_socket_sendto(nl, nlh, nlh->nlmsg_len);
    if (ret < 0) {
        perror("mnl_socket_sendto");
        exit(EXIT_FAILURE);
    }

    // 清理资源
    nftnl_rule_free(rule);
    nftnl_chain_free(chain);
    nftnl_table_free(table);
    mnl_socket_close(nl);

    printf("创建表、链和规则成功\\n");
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(cmdline)


    def test(self):
        self.cmd("gcc -o nft_test1 nft_test1.c -lnftnl -lmnl")
        code, nft_result = self.cmd("./nft_test1")
        self.assertIn('创建表、链和规则成功', nft_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf nft_test1.c nft_test1")
