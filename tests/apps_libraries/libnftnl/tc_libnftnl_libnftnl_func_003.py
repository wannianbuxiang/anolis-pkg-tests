# -*- encoding: utf-8 -*-

"""
@File:      tc_libnftnl_libnftnl_func_003.py
@Time:      2024/04/15 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libnftnl_libnftnl_func_003.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "libnftnl libnftnl-devel gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >nft_test3.c<<"EOF"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <libnftnl/table.h>
#include <libnftnl/chain.h>
#include <linux/netfilter.h>
#include <linux/netfilter/nf_tables.h>

#define TABLE_FAMILY     NFPROTO_IPV4  // 定义表族为IPv4
#define TABLE_NAME       "filter"  // 表名称
#define CHAIN_NAME       "input"   // 链名称
#define CHAIN_PRIORITY   INT32_MIN // 链优先级设为最小整数值，即最高优先级

// 开始测试
int main(int argc, char *argv[])
{
    struct nftnl_table *t;
    struct nftnl_chain *c;
    int32_t priority;

    // 初始化 netfilter 表
    t = nftnl_table_alloc();
    if (!t) {
        perror("无法分配表");
        exit(EXIT_FAILURE);
    }
    // 设置表的族和名称
    nftnl_table_set_u32(t, NFTNL_TABLE_FAMILY, TABLE_FAMILY);
    nftnl_table_set_str(t, NFTNL_TABLE_NAME, TABLE_NAME);

    // 初始化 netfilter 链
    c = nftnl_chain_alloc();
    if (!c) {
        perror("无法分配链");
        exit(EXIT_FAILURE);
    }
    // 设置链的表、名称和优先级
    nftnl_chain_set_str(c, NFTNL_CHAIN_TABLE, TABLE_NAME);
    nftnl_chain_set_str(c, NFTNL_CHAIN_NAME, CHAIN_NAME);
    nftnl_chain_set_s32(c, NFTNL_CHAIN_PRIO, CHAIN_PRIORITY);

    // 获取链的优先级
    priority = nftnl_chain_get_s32(c, NFTNL_CHAIN_PRIO);

    printf("链 \\"%s\\" 的优先级为: %d\\n", CHAIN_NAME, priority);

    // 清理资源
    nftnl_chain_free(c);
    nftnl_table_free(t);

    return 0;
}
EOF'''
        self.cmd(cmdline)


    def test(self):
        self.cmd("gcc -o nft_test3 nft_test3.c -lnftnl")
        code, nft_result = self.cmd("./nft_test3")
        self.assertIn('链 "input" 的优先级为: -2147483648', nft_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf nft_test3.c nft_test3")
