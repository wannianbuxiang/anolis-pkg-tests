# -*- encoding: utf-8 -*-

"""
@File:      tc_libpipeline_fun005.py
@Time:      2024-04-15 15:34:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libpipeline_fun005.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    file_context = r"""
#include <pipeline.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main() {
    // 创建一个新的管道
    pipeline *p = pipeline_new();
    
    // 创建一个新的管道命令
    pipecmd *cmd = pipecmd_new_args("sh", "-c", "echo $TEST3", NULL);
    
    // 使用 pipecmd_setenv 设置 TEST3 环境变量为 "foo"
    pipecmd_setenv(cmd, "TEST3", "foo");

    // 向管道添加命令
    pipeline_command(p, cmd);

    // 需要 pipeline 捕获输出
    pipeline_want_out(p, -1);

    // 执行 pipeline
    pipeline_start(p);

    // 读取输出验证环境变量设置成功
    const char *line = pipeline_readline(p);
    if (line == NULL || strcmp(line, "foo\n") != 0) {
        fprintf(stderr, "setenv failed: expected 'foo', got '%s'\n", line ? line : "null");
        pipeline_free(p);
        return EXIT_FAILURE;
    }
    printf("TEST3 set successfully\n");

    // 销毁上一个 pipeline
    pipeline_free(p);

    // 创建一个新的管道和命令来测试环境变量的清除
    p = pipeline_new();
    cmd = pipecmd_new_args("sh", "-c", "echo $TEST3", NULL);

    // 使用 pipecmd_clearenv 清除管道命令中的所有环境变量
    pipecmd_clearenv(cmd);

    // 向管道添加命令
    pipeline_command(p, cmd);

    // 需要 pipeline 捕获输出
    pipeline_want_out(p, -1);

    // 执行 pipeline
    pipeline_start(p);

    // 读取输出验证环境变量清除成功
    line = pipeline_readline(p);
    if (line == NULL || strcmp(line, "\n") != 0) {
        fprintf(stderr, "clearenv failed: expected empty line, got '%s'\n", line ? line : "null");
        pipeline_free(p);
        return EXIT_FAILURE;
    }
    printf("TEST3 cleared successfully\n");

    // 销毁 pipeline
    pipeline_free(p);

    return EXIT_SUCCESS;
}
"""

    PARAM_DIC = {"pkg_name": "libpipeline libpipeline-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.c_file = "libpipeline_fun_005.c"
        self.exec_file = "libpipeline_fun_005"
        cmdline = f"""cat > {self.c_file } <<'EOF'
        {self.file_context}
EOF
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd(f"gcc -o {self.exec_file} {self.c_file} -lpipeline")
        code, result = self.cmd(f"file {self.exec_file}")
        self.assertIn("executable", result)
        code, result = self.cmd(f"./{self.exec_file}")
        expected_output = (
            "TEST3 set successfully\n"
            "TEST3 cleared successfully\n"
        )
        self.assertEquals(
            expected_output.replace(" ", "").strip(),
            result.replace(" ", "").strip(),
            msg=f"{expected_output}输出结果与{result}实际预期结果不一致",
        )

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f"rm -rf {self.c_file} {self.exec_file}")
