# -*- encoding: utf-8 -*-

"""
@File:      tc_libpipeline_fun007.py
@Time:      2024-04-16 10:34:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libpipeline_fun007.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    file_context = r"""
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <pipeline.h>
#include <string.h>  

void add_pipeline_command_argv(pipeline *p, const char *file, ...) {
    va_list args;
    va_start(args, file);
    pipeline_command_argv(p, file, args);
    va_end(args);
}

int main() {
    // Create a new pipeline
    pipeline *p = pipeline_new();
    pipeline_want_out(p, -1);

    // Add commands with variable arguments to the pipeline
    add_pipeline_command_argv(p, "echo", "Hello", "World!", NULL);

    // Start the pipeline
    pipeline_start(p);

    // Immediately get the output FILE stream
    FILE *output = pipeline_get_outfile(p);  // 使用了正确的函数
    if (!output) {
        perror("pipeline_get_outfile failed");
        pipeline_free(p);
        return EXIT_FAILURE;
    }

    // Read the output from the pipeline
    char buffer[128];
    if (!fgets(buffer, sizeof(buffer), output)) {
        perror("Failed to read from stream");
        pipeline_free(p);
        return EXIT_FAILURE;
    }

    // Wait for the pipeline to finish
    int status = pipeline_wait(p);
    if (status != 0) {
        fprintf(stderr, "Pipeline failed with exit status %d\n", status);
    }
    pipeline_free(p); // Cleanup the pipeline

    // Verify the output
    if (strcmp(buffer, "Hello World!\n") != 0) {
        fprintf(stderr, "Unexpected output: %s", buffer);
        return EXIT_FAILURE;
    }

    // If output is as expected, the test is successful
    printf("Received expected output: %s", buffer);
    return EXIT_SUCCESS;
}
"""

    PARAM_DIC = {"pkg_name": "libpipeline libpipeline-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.c_file = "libpipeline_fun_007.c"
        self.exec_file = "libpipeline_fun_007"
        cmdline = f"""cat > {self.c_file } <<'EOF'
        {self.file_context}
EOF
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd(f"gcc -o {self.exec_file} {self.c_file} -lpipeline")
        code, result = self.cmd(f"file {self.exec_file}")
        self.assertIn("executable", result)
        code, result = self.cmd(f"./{self.exec_file}")
        self.assertIn("Hello World!", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f"rm -rf {self.c_file} {self.exec_file}")
