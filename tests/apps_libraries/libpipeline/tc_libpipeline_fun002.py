# -*- encoding: utf-8 -*-

"""
@File:      tc_libpipeline_fun002.py
@Time:      2024-04-15 11:22:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libpipeline_fun002.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    file_context = r"""
#include <pipeline.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
    pipeline *p;
    const char *line;

    // 创建并初始化 pipeline
    p = pipeline_new();
    pipeline_want_out(p, -1);

    // 添加 printf 命令以输出 "Hello, World!"，注意使用 \n 分割为两行
    pipeline_command_args(p, "printf", "Hello\nWorld\n", NULL);

    // 添加 grep 命令来过滤输出中包含 "World" 的行
    pipeline_command_args(p, "grep", "World", NULL);

    // 启动 pipeline
    pipeline_start(p);
    
    // 读取从 pipeline 输出的行，预期只有一行 "World"
    while ((line = pipeline_readline(p)) != NULL) {
        // 输出捕获的 pipeline 内容，并按预期校验输出
        if (strstr(line, "World")) {
            printf("%s", line); // 输出应仅包含 "World"
        }
    }

    // 等待 pipeline 结束，并检查返回状态
    if (pipeline_wait(p) != 0) {
        fprintf(stderr, "'pipeline' did not return 0\n");
        exit(EXIT_FAILURE);
    }

    // 释放 pipeline
    pipeline_free(p);

    return 0;
}
"""

    PARAM_DIC = {"pkg_name": "libpipeline libpipeline-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.c_file = "libpipeline_fun_002.c"
        self.exec_file = "libpipeline_fun_002"
        cmdline = f"""cat > {self.c_file } <<EOF
        {self.file_context}
EOF
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd(f"gcc -o {self.exec_file} {self.c_file} -lpipeline")
        code, result = self.cmd(f"file {self.exec_file}")
        self.assertIn("executable", result)
        code, result = self.cmd(f"./{self.exec_file}")
        self.assertEqual("World", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f"rm -rf {self.c_file} {self.exec_file}")
