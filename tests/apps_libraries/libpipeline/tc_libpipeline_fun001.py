# -*- encoding: utf-8 -*-

"""
@File:      tc_libpipeline_fun001.py
@Time:      2024-04-15 11:22:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libpipeline_fun001.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    file_context = r"""
#include <pipeline.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

int main() {
    // 创建 pipeline
    pipeline *p = pipeline_new();
    
    // 使用 printf 代替 echo，因为 echo 在末尾添加了换行符
    pipeline_command_args(p, "echo", "Test", NULL);
    pipeline_want_out(p, -1); // 指示我们想要捕获输出
    
    // 启动 pipeline
    pipeline_start(p);

    // 读取 pipeline 执行结果的第一行
    const char *output = pipeline_readline(p);
    
    // 断言输出不为空
    assert(output && "No output received from pipeline");
    
    printf("%s", output);
    // 清理
    pipeline_free(p);

    return 0;
}
"""

    PARAM_DIC = {"pkg_name": "libpipeline libpipeline-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.c_file = "libpipeline_fun_001.c"
        self.exec_file = "libpipeline_fun_001"
        cmdline = f"""cat > {self.c_file } <<EOF
        {self.file_context}
EOF
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd(f"gcc -o {self.exec_file} {self.c_file} -lpipeline")
        code, result = self.cmd(f"file {self.exec_file}")
        self.assertIn("executable", result)
        code, result = self.cmd(f"./{self.exec_file}")
        self.assertEqual("Test", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f"rm -rf {self.c_file} {self.exec_file}")
