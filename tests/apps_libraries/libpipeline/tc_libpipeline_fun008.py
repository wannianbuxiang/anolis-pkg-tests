# -*- encoding: utf-8 -*-

"""
@File:      tc_libpipeline_fun008.py
@Time:      2024-04-16 14:34:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libpipeline_fun008.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    file_context = r"""
#include <stdio.h>
#include <stdlib.h>
#include <pipeline.h>

int main() {
    // Create a new pipeline
    pipeline *p = pipeline_new();
    if (!p) {
        fprintf(stderr, "Failed to create new pipeline\n");
        return EXIT_FAILURE;
    }

    // Add a command that should succeed
    pipeline_command_args(p, "echo", "Hello world", NULL);

    // Add a command that does not exist, this will fail
    pipeline_command_args(p, "nonexistent_command", NULL);

    // Start the pipeline
    pipeline_start(p);

    // Wait for all commands in the pipeline to finish and get their exit status
    int status = pipeline_wait(p);
    pipeline_free(p);  // Clean up the pipeline structure

    // Any failure within the pipeline should provide a non-zero status code
    if (status != 0) {
        // WEXITSTATUS macro retrieves the exit status of the child process
        printf("As expected, the pipeline returned a non-zero status code: %d\n", WEXITSTATUS(status));
        return EXIT_SUCCESS;  // Return success as this is the expected test result
    } else {
        fprintf(stderr, "The pipeline succeeded when an error was expected\n");
        return EXIT_FAILURE;
    }
}
"""

    PARAM_DIC = {"pkg_name": "libpipeline libpipeline-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.c_file = "libpipeline_fun_008.c"
        self.exec_file = "libpipeline_fun_008"
        cmdline = f"""cat > {self.c_file } <<'EOF'
        {self.file_context}
EOF
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd(f"gcc -o {self.exec_file} {self.c_file} -lpipeline")
        code, result = self.cmd(f"file {self.exec_file}")
        self.assertIn("executable", result)
        code, result = self.cmd(f"./{self.exec_file}")
        self.assertIn("As expected, the pipeline returned a non-zero status code: 0", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f"rm -rf {self.c_file} {self.exec_file}")
