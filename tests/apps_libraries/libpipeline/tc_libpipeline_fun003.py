# -*- encoding: utf-8 -*-

"""
@File:      tc_libpipeline_fun003.py
@Time:      2024-04-15 14:00:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libpipeline_fun003.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    file_context = r"""
#include <pipeline.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> // for unlink()

int main() {
    // 输入文件名
    const char *input_filename = "input.txt";
    
    // 创建并写入测试数据到 input.txt
    FILE *file = fopen(input_filename, "w");
    if (file == NULL) {
        perror("Cannot open file for writing");
        return EXIT_FAILURE;
    }
    fprintf(file, "This is a test file.\n");
    fprintf(file, "It contains multiple lines.\n");
    fprintf(file, "Some of these lines contain the word error.\n");
    fprintf(file, "This line does not have the word we are looking for.\n");
    fprintf(file, "Another line with an error here.\n");
    fprintf(file, "The previous line should be sorted and only appear once.\n");
    fprintf(file, "This is the last error line.\n");
    fclose(file);

    // 创建 pipeline
    pipeline *p = pipeline_new();
    pipeline_want_infile(p, input_filename);  // 设置输入文件
    pipeline_want_out(p, -1); // 准备捕获到标准输出的内容

    // 构建管道命令
    pipeline_command_args(p, "cat", input_filename, NULL);
    pipeline_command_args(p, "grep", "error", NULL);
    pipeline_command_args(p, "sort", NULL);
    pipeline_command_args(p, "uniq", "-c", NULL);
    
    // 启动 pipeline
    pipeline_start(p);

    // 读取和打印管道输出
    const char *line;
    while ((line = pipeline_readline(p)) != NULL) {
        printf("%s", line);  // line already contains a newline
    }
    
    // 等待管道结束
    int status = pipeline_wait(p);
    if (status != 0) {
        fprintf(stderr, "Pipeline failed with status %d\n", status);
        pipeline_free(p);
        unlink(input_filename); // Delete the file even if the pipeline failed
        return EXIT_FAILURE;
    }

    // 清理
    pipeline_free(p);
    unlink(input_filename); // 删除创建的文件
    
    return EXIT_SUCCESS;
}
"""

    PARAM_DIC = {"pkg_name": "libpipeline libpipeline-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.c_file = "libpipeline_fun_003.c"
        self.exec_file = "libpipeline_fun_003"
        cmdline = f"""cat > {self.c_file } <<EOF
        {self.file_context}
EOF
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd(f"gcc -o {self.exec_file} {self.c_file} -lpipeline")
        code, result = self.cmd(f"file {self.exec_file}")
        self.assertIn("executable", result)
        code, result = self.cmd(f"./{self.exec_file}")
        expected_output = (
            "      1 Another line with an error here.\n"
            "      1 Some of these lines contain the word error.\n"
            "      1 This is the last error line."
        )
        self.assertEquals(
            expected_output.replace(" ", "").strip(),
            result.replace(" ", "").strip(),
            msg=f"{expected_output}输出结果与{result}实际预期结果不一致",
        )

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f"rm -rf {self.c_file} {self.exec_file}")
