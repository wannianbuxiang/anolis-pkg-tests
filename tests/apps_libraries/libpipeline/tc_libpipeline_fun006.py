# -*- encoding: utf-8 -*-

"""
@File:      tc_libpipeline_fun006.py
@Time:      2024-04-15 15:34:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libpipeline_fun006.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    file_context = r"""
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <pipeline.h>

int main() {
    // 创建一个 pipecmd 对象，指定要执行的命令及其参数
    pipecmd *cmd = pipecmd_new_args("echo", "Hello, World!", NULL);

    if (!cmd) {
        fprintf(stderr, "Failed to create pipecmd\n");
        return 1;
    }

    // 创建子进程
    pid_t pid = fork();
    if (pid < 0) {
        // fork 失败
        perror("fork");
        pipecmd_free(cmd);
        return 1;
    } else if (pid == 0) {
        // 子进程中，执行 pipecmd_exec
        pipecmd_exec(cmd);
        
        // 如果 pipecmd_exec 失败，它将返回到这里
        perror("pipecmd_exec");
        _exit(1); // 子进程失败时使用 _exit 而不是 exit
    } else {
        // 父进程中，等待子进程结束
        int status;
        waitpid(pid, &status, 0);
        if (WIFEXITED(status)) {
            int exit_status = WEXITSTATUS(status);
            printf("Child process exited with status %d\n", exit_status);
        } else {
            fprintf(stderr, "Child process did not exit normally\n");
        }

        // 释放 pipecmd 对象
        pipecmd_free(cmd);

        // 根据子进程的退出状态返回
        return WIFEXITED(status) && (WEXITSTATUS(status) == 0) ? 0 : 1;
    }
}
"""

    PARAM_DIC = {"pkg_name": "libpipeline libpipeline-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.c_file = "libpipeline_fun_006.c"
        self.exec_file = "libpipeline_fun_006"
        cmdline = f"""cat > {self.c_file } <<'EOF'
        {self.file_context}
EOF
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd(f"gcc -o {self.exec_file} {self.c_file} -lpipeline")
        code, result = self.cmd(f"file {self.exec_file}")
        self.assertIn("executable", result)
        code, result = self.cmd(f"./{self.exec_file}")
        self.assertIn("Hello, World!", result)
        self.assertIn("Child process exited with status 0", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f"rm -rf {self.c_file} {self.exec_file}")
