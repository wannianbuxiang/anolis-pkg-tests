# -*- encoding: utf-8 -*-

"""
@File:      tc_libpipeline_fun004.py
@Time:      2024-04-15 15:34:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libpipeline_fun004.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    file_context = r"""
#include <pipeline.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main() {
    pipeline *p;
    pipecmd *cmd;

    // 初始化 pipeline 和设置环境变量 TEST
    p = pipeline_new();
    cmd = pipecmd_new_args("sh", "-c", "echo $TEST", NULL);
    pipecmd_setenv(cmd, "TEST", "foo");
    pipeline_command(p, cmd);
    pipeline_want_out(p, -1);
    pipeline_start(p);
    
    // 读取并检查输出
    const char *line = pipeline_readline(p);
    if (line && strcmp(line, "foo\n") == 0) {
        printf("TEST variable set properly: %s", line);
    } else {
        fprintf(stderr, "Failed to set TEST variable\n");
        pipeline_free(p);
        return EXIT_FAILURE;
    }

    // 清理释放 pipeline，注意这里不再进行后续操作
    pipeline_free(p); // 释放第一次运行的 pipeline 对象

    // 初始化新的 pipeline 用于验证环境变量清除
    p = pipeline_new();
    cmd = pipecmd_new_args("sh", "-c", "echo $TEST", NULL);
    pipecmd_unsetenv(cmd, "TEST");
    pipeline_command(p, cmd);
    pipeline_want_out(p, -1);
    pipeline_start(p);

    // 读取并检查输出，确认环境变量已清除
    line = pipeline_readline(p);
    if (line && strcmp(line, "\n") == 0) {
        printf("TEST variable unset properly\n");
    } else {
        fprintf(stderr, "Failed to unset TEST variable. Output was: %s\n", line ? line : "(null)");
        pipeline_free(p);
        return EXIT_FAILURE;
    }

    // 清理释放新的 pipeline 对象
    pipeline_free(p);

    return EXIT_SUCCESS;
}
"""

    PARAM_DIC = {"pkg_name": "libpipeline libpipeline-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.c_file = "libpipeline_fun_004.c"
        self.exec_file = "libpipeline_fun_004"
        cmdline = f"""cat > {self.c_file } <<'EOF'
        {self.file_context}
EOF
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd(f"gcc -o {self.exec_file} {self.c_file} -lpipeline")
        code, result = self.cmd(f"file {self.exec_file}")
        self.assertIn("executable", result)
        code, result = self.cmd(f"./{self.exec_file}")
        expected_output = (
            "TEST variable set properly: foo\n"
            "TEST variable unset properly\n"
        )
        self.assertEquals(
            expected_output.replace(" ", "").strip(),
            result.replace(" ", "").strip(),
            msg=f"{expected_output}输出结果与{result}实际预期结果不一致",
        )

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f"rm -rf {self.c_file} {self.exec_file}")
