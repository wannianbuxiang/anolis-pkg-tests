# -*- encoding: utf-8 -*-

"""
@File:      tc_lzo_lzo_func_003.py
@Time:      2024/03/25 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_lzo_lzo_func_003.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "lzo lzo-devel gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >lzo_test3.c<<"EOF"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <lzo/lzo1x.h>

/* 使用此函数初始化LZO库 */
static void lzo_initialize() {
    if (lzo_init() != LZO_E_OK) {
        fprintf(stderr, "LZO library initialization failed.\\n");
        exit(EXIT_FAILURE);
    }
}

/* 填充缓冲区 with some test data */
static void fill_buffer_with_test_data(lzo_bytep buffer, size_t size) {
    size_t i;
    for (i = 0; i < size; i++) {
        buffer[i] = (lzo_byte)((rand() % 255) + 1); // 避免零，防止压缩时未使用的数据被压缩
    }
}

/* 用于损坏数据出于测试目的 */
static void corrupt_data(lzo_bytep buffer, size_t size) {
    buffer[rand() % size] ^= 0xFF; // 随机翻转一个字节的所有位
}

int main() {
    // 初始化LZO库
    lzo_initialize();
    
    // 分配内存缓冲区
    lzo_bytep in = malloc(LZO1X_1_MEM_COMPRESS);
    lzo_bytep out = malloc(LZO1X_1_MEM_COMPRESS);
    lzo_bytep decompressed = malloc(LZO1X_1_MEM_COMPRESS);
    lzo_voidp wrkmem = malloc(LZO1X_1_MEM_COMPRESS);
    if (in == NULL || out == NULL || decompressed == NULL || wrkmem == NULL) {
        fprintf(stderr, "Not enough memory.\\n");
        return EXIT_FAILURE;
    }

    // 填充缓冲区 with test data
    fill_buffer_with_test_data(in, LZO1X_1_MEM_COMPRESS);
    
    // 压缩数据
    lzo_uint compressed_size;
    if (lzo1x_1_compress(in, LZO1X_1_MEM_COMPRESS, out, &compressed_size, wrkmem) != LZO_E_OK) {
        fprintf(stderr, "Compression failed.\\n");
        return EXIT_FAILURE;
    }

    // 故意损坏输出缓冲区以测试容错性能力
    corrupt_data(out, LZO1X_1_MEM_COMPRESS);

    // 解压数据
    lzo_uint decompressed_size = LZO1X_1_MEM_COMPRESS;
    if (lzo1x_decompress(out, compressed_size, decompressed, &decompressed_size, NULL) != LZO_E_OK) {
        fprintf(stderr, "Decompression failed.\\n");
        return EXIT_FAILURE;
    }

    // 检查解压数据是否相同
    if (memcmp(in, decompressed, LZO1X_1_MEM_COMPRESS) != 0) {
        fprintf(stderr, "Data does not match original.\\n");
        return EXIT_FAILURE;
    }

    //  数据检查通过
    printf("Data successfully decompressed and verified.\\n");

    // 释放内存
    free(in);
    free(out);
    free(decompressed);
    free(wrkmem);

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(cmdline)


    def test(self):
        self.cmd("gcc -o lzo_test3 lzo_test3.c -llzo2")
        code, lzo_result = self.cmd("./lzo_test3", ignore_status=True)
        self.assertIn("Data does not match original.", lzo_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf lzo_test3.c lzo_test3")
