# -*- encoding: utf-8 -*-

"""
@File:      tc_lzo_lzo_func_001.py
@Time:      2024/03/25 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_lzo_lzo_func_001.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "lzo lzo-devel gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >lzo_test1.c<<"EOF"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <lzo/lzo1x.h>

// 确保这些长度足够大以容纳压缩和解压缩的数据
#define IN_LEN  (64*1024L)
#define OUT_LEN (IN_LEN + IN_LEN / 16 + 64 + 3)

int main() {
    // LZO库的初始化必须成功
    if (lzo_init() != LZO_E_OK) {
        printf("lzo_init() failed\\n");
        return 1;
    }

    // 分配内存空间用于输入数据、压缩输出和解压缩输出
    lzo_byte *in = (lzo_byte *)malloc(IN_LEN);
    lzo_byte *out = (lzo_byte *)malloc(OUT_LEN);
    lzo_byte *decompressed = (lzo_byte *)malloc(IN_LEN);
    lzo_voidp workMem = (lzo_voidp)malloc(LZO1X_1_MEM_COMPRESS);

    if (!in || !out || !decompressed || !workMem) {
        printf("Not enough memory\\n");
        free(in);
        free(out);
        free(decompressed);
        free(workMem);
        return 1;
    }

    // 填充输入数据
    for (int i = 0; i < IN_LEN; i++) {
        in[i] = (lzo_byte)(i & 0xFF);
    }

    lzo_uint out_len = OUT_LEN;
    lzo_uint decompressed_len = IN_LEN;

    // 数据压缩
    int res = lzo1x_1_compress(in, IN_LEN, out, &out_len, workMem);
    if (res != LZO_E_OK) {
        printf("Compression failed\\n");
        free(in);
        free(out);
        free(decompressed);
        free(workMem);
        return 1;
    }
    printf("Compressed from %d to %d bytes\\n", IN_LEN, out_len);

    // 数据解压缩
    res = lzo1x_decompress(out, out_len, decompressed, &decompressed_len, NULL);
    if (res != LZO_E_OK) {
        printf("Decompression failed\\n");
        free(in);
        free(out);
        free(decompressed);
        free(workMem);
        return 1;
    }
    printf("Decompressed from %d to %d bytes\\n", out_len, decompressed_len);

    // 比较原始和解压缩后的数据是否相同
    if (IN_LEN != decompressed_len || memcmp(in, decompressed, IN_LEN) != 0) {
        printf("Data does not match\\n");
        free(in);
        free(out);
        free(decompressed);
        free(workMem);
        return 1;
    } else {
        printf("Data verified, decompression successful\\n");
    }

    // 清理内存
    free(in);
    free(out);
    free(decompressed);
    free(workMem);

    return 0;
}
EOF'''
        self.cmd(cmdline)


    def test(self):
        self.cmd("gcc -o lzo_test1 lzo_test1.c -llzo2")
        code, lzo_result = self.cmd("./lzo_test1")
        self.assertIn("Compressed from 65536 to 823 bytes", lzo_result)
        self.assertIn("Decompressed from 823 to 65536 bytes", lzo_result)
        self.assertIn("Data verified, decompression successful", lzo_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf lzo_test1.c lzo_test1")
