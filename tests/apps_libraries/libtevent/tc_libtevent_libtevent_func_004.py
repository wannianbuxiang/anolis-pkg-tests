#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libtevent_libtevent_func_004.py
@Time:      2024/04/23 14:51:50
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest
import re

class Test(LocalTest):
    """
    See tc_libtevent_libtevent_func_004.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libtevent libtevent-devel libtalloc-devel gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > tevent_test4.c <<EOF
#include <stdio.h>
#include <tevent.h>

// 立即事件回调函数
static void immediate_event_handler(struct tevent_context *ev,
                                    struct tevent_immediate *im,
                                    void *private_data)
{
    // 打印消息以表明立即事件已触发
    printf("立即事件已触发。\\n");
}

int main(void)
{
    // 创建 tevent 上下文
    struct tevent_context *ev_ctx = tevent_context_init(NULL);
    if (ev_ctx == NULL) {
        fprintf(stderr, "无法初始化 tevent 上下文\\n");
        return -1;
    }

    // 创建立即事件并注册事件处理函数
    struct tevent_immediate *im = tevent_create_immediate(ev_ctx);
    if (im == NULL) {
        fprintf(stderr, "无法创建立即事件\\n");
        TALLOC_FREE(ev_ctx);
        return -1;
    }
    tevent_schedule_immediate(im, ev_ctx, immediate_event_handler, NULL);

    // 打印消息以表明我们将要开始事件循环
    printf("开始事件循环，并等待立即事件触发...\\n");

    // 运行事件循环，等待立即事件触发
    tevent_loop_wait(ev_ctx);

    // 事件循环结束后清理资源
    TALLOC_FREE(ev_ctx);

    printf("事件循环结束，退出程序。\\n");

    return 0;
}
EOF"""

        self.cmd(cmdline)
    
    def test(self):
        self.cmd('gcc -o tevent_test4 tevent_test4.c -ltevent -ltalloc')
        code, tevent_result = self.cmd("./tevent_test4")
        self.assertIn('立即事件已触发', tevent_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf tevent_test4.c tevent_test4')
