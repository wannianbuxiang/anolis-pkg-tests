#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libtevent_libtevent_func_002.py
@Time:      2024/04/23 14:51:50
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest
import re

class Test(LocalTest):
    """
    See tc_libtevent_libtevent_func_002.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libtevent libtevent-devel libtalloc-devel gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > tevent_test2.c <<EOF
#include <stdio.h>
#include <tevent.h>
#include <unistd.h> // 引入 sleep 函数

// 定义了一个全局变量来记录事件处理函数是否被调用
static bool event_triggered = false;

// 时间事件处理函数
static void timer_event_handler(struct tevent_context *ev,
                                struct tevent_timer *te,
                                struct timeval current_time,
                                void *private_data)
{
    // 设置事件已被触发
    event_triggered = true;
    printf("定时器事件已触发!\\n");
}

int main(void)
{
    // 创建 tevent 事件上下文
    struct tevent_context *ev_ctx = tevent_context_init(NULL);
    if (ev_ctx == NULL) {
        fprintf(stderr, "无法初始化 tevent 上下文\\n");
        return -1;
    }

    printf("创建定时器事件，将在2秒后触发...\\n");

    // 创建定时器事件，设置3秒后触发
    struct timeval timeout = tevent_timeval_current_ofs(2, 0); // 当前时间加2秒
    struct tevent_timer *timer = tevent_add_timer(ev_ctx, NULL, timeout, timer_event_handler, NULL);
    if (timer == NULL) {
        fprintf(stderr, "无法创建定时器事件\\n");
        TALLOC_FREE(ev_ctx);
        return -1;
    }

    // 等待1秒，此时定时器事件不应该被触发
    sleep(1);
    if (event_triggered) {
        fprintf(stderr, "定时器事件提前触发\\n");
    } else {
        printf("经过1秒等待，定时器事件尚未触发，一切正常\\n");
    }

    // 调用事件循环的函数以等待事件触发
    printf("开始事件循环，等待定时器事件...\\n");
    while (!event_triggered && tevent_loop_once(ev_ctx) == 0) {
        // 在此处等待事件被触发
    }

    // 验证定时器事件是否确实已经触发
    if (event_triggered) {
        printf("定时器事件已成功触发且行为符合预期\\n");
    } else {
        fprintf(stderr, "定时器事件没有触发，行为不符合预期\\n");
    }

    // 释放事件上下文资源
    TALLOC_FREE(ev_ctx);

    return 0;
}
EOF"""

        self.cmd(cmdline)
    
    def test(self):
        self.cmd('gcc -o tevent_test2 tevent_test2.c -ltevent -ltalloc')
        code, tevent_result = self.cmd("./tevent_test2")
        self.assertIn('定时器事件已成功触发且行为符合预期', tevent_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf tevent_test2.c tevent_test2')
