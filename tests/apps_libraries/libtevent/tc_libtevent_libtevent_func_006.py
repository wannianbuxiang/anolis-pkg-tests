#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libtevent_libtevent_func_006.py
@Time:      2024/04/23 14:51:50
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest
import re

class Test(LocalTest):
    """
    See tc_libtevent_libtevent_func_006.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libtevent libtevent-devel libtalloc-devel gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > tevent_test6.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include <tevent.h>

// 定时器事件回调函数
static void timer_event_handler(struct tevent_context *ev,
                                struct tevent_timer *te,
                                struct timeval current_time,
                                void *private_data)
{
    // 打印消息以表明定时器事件已触发
    printf("定时器事件已触发。\\n");

    // 可以在这里执行异步任务的回调逻辑

    // 设置一个标志以便主循环知道任务已完成
    *(bool *)private_data = true;
}

int main(void)
{
    // 创建 tevent 上下文
    struct tevent_context *ev_ctx = tevent_context_init(NULL);
    if (ev_ctx == NULL) {
        fprintf(stderr, "无法初始化 tevent 上下文\\n");
        return EXIT_FAILURE;
    }

    // 在 tevent 上下文中创建一个定时器事件
    struct timeval now, time_to_trigger;
    struct tevent_timer *te;
    bool event_triggered = false;

    // 获取当前时间
    now = tevent_timeval_current_ofs(0, 0);
    // 设置定时器事件到期的未来时间（比如2秒后）
    time_to_trigger = tevent_timeval_add(&now, 2, 0);

    // 创建定时器事件并注册事件处理函数
    te = tevent_add_timer(ev_ctx, NULL, time_to_trigger, timer_event_handler, &event_triggered);
    if (te == NULL) {
        fprintf(stderr, "无法创建定时器事件\\n");
        TALLOC_FREE(ev_ctx);
        return EXIT_FAILURE;
    }

    // 开始事件循环
    printf("开始事件循环，等待定时器触发...\\n");
    while (!event_triggered) {
        if (tevent_loop_once(ev_ctx) != 0) {
            fprintf(stderr, "tevent_loop_once() 失败\\n");
            break;
        }
    }

    // 事件循环结束后清理资源
    TALLOC_FREE(ev_ctx);

    printf("事件循环结束，退出程序。\\n");
    return EXIT_SUCCESS;
}
EOF"""

        self.cmd(cmdline)
    
    def test(self):
        self.cmd('gcc -o tevent_test6 tevent_test6.c -ltevent -ltalloc')
        code, tevent_result = self.cmd("./tevent_test6")
        self.assertIn('事件循环结束，退出程序', tevent_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf tevent_test6.c tevent_test6')
