#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libtevent_libtevent_func_005.py
@Time:      2024/04/23 14:51:50
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest
import re

class Test(LocalTest):
    """
    See tc_libtevent_libtevent_func_005.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libtevent libtevent-devel libtalloc-devel gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > tevent_test5.c <<EOF
#include <stdio.h>
#include <tevent.h>
#include <stdbool.h>

// 立即事件回调函数的声明
static void high_priority_immediate_handler(struct tevent_context *ev,
                                            struct tevent_immediate *im,
                                            void *private_data);
static void low_priority_immediate_handler(struct tevent_context *ev,
                                           struct tevent_immediate *im,
                                           void *private_data);

// 追踪事件状态的结构体
struct event_state {
    bool high_priority_done;
    bool low_priority_done;
};

int main(void)
{
    // 创建 tevent 上下文
    struct tevent_context *ev_ctx = tevent_context_init(NULL);
    if (ev_ctx == NULL) {
        fprintf(stderr, "无法初始化 tevent 上下文\\n");
        return -1;
    }

    struct event_state state = { .high_priority_done = false, .low_priority_done = false };

    // 创建并调度模拟的高优先级立即事件
    struct tevent_immediate *high_im = tevent_create_immediate(ev_ctx);
    if (high_im == NULL) {
        fprintf(stderr, "无法创建立即事件\\n");
        TALLOC_FREE(ev_ctx);
        return -1;
    }
    tevent_schedule_immediate(high_im, ev_ctx, high_priority_immediate_handler, &state);

    // 创建并调度模拟的低优先级立即事件
    struct tevent_immediate *low_im = tevent_create_immediate(ev_ctx);
    if (low_im == NULL) {
        fprintf(stderr, "无法创建立即事件\\n");
        TALLOC_FREE(ev_ctx);
        return -1;
    }
    tevent_schedule_immediate(low_im, ev_ctx, low_priority_immediate_handler, &state);

    // 运行事件循环
    printf("开始事件循环...\\n");
    while (!(state.high_priority_done && state.low_priority_done)) {
        if (tevent_loop_once(ev_ctx) < 0) {
            fprintf(stderr, "事件循环出错\\n");
            break;
        }
    }

    // 清理资源
    TALLOC_FREE(ev_ctx);
    printf("事件循环结束，退出程序。\\n");

    return 0;
}

// 立即事件回调函数的实现
static void high_priority_immediate_handler(struct tevent_context *ev,
                                            struct tevent_immediate *im,
                                            void *private_data)
{
    struct event_state *state = private_data;
    printf("高优先级立即事件已触发。\\n");
    state->high_priority_done = true;
}

static void low_priority_immediate_handler(struct tevent_context *ev,
                                           struct tevent_immediate *im,
                                           void *private_data)
{
    struct event_state *state = private_data;
    printf("低优先级立即事件已触发。\\n");
    state->low_priority_done = true;
}
EOF"""

        self.cmd(cmdline)
    
    def test(self):
        self.cmd('gcc -o tevent_test5 tevent_test5.c -ltevent -ltalloc')
        code, tevent_result = self.cmd("./tevent_test5")
        self.assertIn('事件循环结束，退出程序', tevent_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf tevent_test5.c tevent_test5')
