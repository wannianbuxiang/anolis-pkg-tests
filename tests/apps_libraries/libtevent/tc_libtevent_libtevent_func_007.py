#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libtevent_libtevent_func_007.py
@Time:      2024/04/23 14:51:50
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest
import re

class Test(LocalTest):
    """
    See tc_libtevent_libtevent_func_007.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libtevent libtevent-devel libtalloc-devel gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > tevent_test7.c <<EOF
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <tevent.h>

// 定时器事件回调函数
static void timer_event_handler(struct tevent_context *ev,
                                struct tevent_timer *timer,
                                struct timeval current_time,
                                void *private_data)
{
    printf("定时器事件触发，准备发送 SIGUSR1 信号...\\n");
    kill(getpid(), SIGUSR1);  // 向自己发送 SIGUSR1 信号
}

// 信号处理函数
static void signal_handler(struct tevent_context *ev,
                           struct tevent_signal *se,
                           int signum,
                           int count,
                           void *siginfo,
                           void *private_data)
{
    printf("接收到信号: %d\\n", signum);
    *(bool *)private_data = true;  // 设置事件触发标志
}

int main(int argc, char *argv[])
{
    // 创建 tevent 上下文
    struct tevent_context *ev_ctx = tevent_context_init(NULL);
    if (ev_ctx == NULL) {
        fprintf(stderr, "初始化 tevent 上下文失败\\n");
        return -1;
    }

    // 创建处理 SIGUSR1 的信号事件
    struct tevent_signal *signal_event;
    bool signal_received = false;

    signal_event = tevent_add_signal(ev_ctx, NULL, SIGUSR1, 0,
                                     signal_handler, &signal_received);
    if (signal_event == NULL) {
        fprintf(stderr, "创建信号事件失败\\n");
        TALLOC_FREE(ev_ctx);
        return -1;
    }

    // 创建定时器事件
    struct tevent_timer *timer;
    struct timeval now, timeout;
    now = tevent_timeval_current();  // 获取当前时间
    timeout = tevent_timeval_add(&now, 2, 0);  // 设置 2 秒后触发

    timer = tevent_add_timer(ev_ctx, NULL, timeout, timer_event_handler, NULL);
    if (timer == NULL) {
        fprintf(stderr, "创建定时器事件失败\\n");
        TALLOC_FREE(ev_ctx);
        return -1;
    }

    printf("开始事件循环，等待信号和定时器...\\n");

    // 开始事件循环，直到接收到信号
    while (!signal_received) {
        int ret = tevent_loop_once(ev_ctx);
        if (ret != 0) {
            fprintf(stderr, "tevent_loop_once() 失败\\n");
            break;
        }
    }

    // 事件循环结束后清理资源
    TALLOC_FREE(ev_ctx);

    printf("事件循环结束，程序结束。\\n");
    return 0;
}
EOF"""

        self.cmd(cmdline)
    
    def test(self):
        self.cmd('gcc -o tevent_test7 tevent_test7.c -ltevent -ltalloc')
        code, tevent_result = self.cmd("./tevent_test7")
        self.assertIn('接收到信号: 10', tevent_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf tevent_test7.c tevent_test7')
