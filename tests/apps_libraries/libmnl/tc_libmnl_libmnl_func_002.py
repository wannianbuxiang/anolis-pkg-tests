# -*- encoding: utf-8 -*-

"""
@File:      tc_libmnl_libmnl_func_002.py
@Time:      2024/03/29 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libmnl_libmnl_func_002.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "libmnl libmnl-devel gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >libmnl_test2.c<<"EOF"
#include <stdio.h>
#include <stdlib.h>
#include <libmnl/libmnl.h>
#include <linux/netlink.h>

int main() {
    struct mnl_socket *nl;
    char buf[MNL_SOCKET_BUFFER_SIZE];
    int ret;
    
    nl = mnl_socket_open(NETLINK_GENERIC);
    if (nl == NULL) {
        perror("mnl_socket_open");
        exit(EXIT_FAILURE);
    }

    // 关闭套接字后尝试发送消息
    mnl_socket_close(nl);

    ret = mnl_socket_sendto(nl, buf, sizeof(buf));
    if (ret == -1) {
        perror("Send on closed socket");
        exit(EXIT_FAILURE);
    } else {
        printf("Sent message on closed socket, ret=%d\\n", ret);
    }

    return 0;
}
EOF'''
        self.cmd(cmdline)


    def test(self):
        self.cmd("gcc -o libmnl_test2 libmnl_test2.c -lmnl")
        code, libmnl_result = self.cmd("./libmnl_test2", ignore_status=True)
        self.assertIn("Send on closed socket: Bad file descriptor", libmnl_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf libmnl_test2.c libmnl_test2")
