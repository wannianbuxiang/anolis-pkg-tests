# -*- encoding: utf-8 -*-

"""
@File:      tc_libmnl_libmnl_func_004.py
@Time:      2024/03/29 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libmnl_libmnl_func_004.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "libmnl libmnl-devel gcc valgrind"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >libmnl_test4.c<<"EOF"
#include <stdio.h>
#include <stdlib.h>
#include <libmnl/libmnl.h>
#include <linux/netlink.h>

int main() {
    struct mnl_socket *nl;

    // Open a netlink socket with the NETLINK_GENERIC protocol
    nl = mnl_socket_open(NETLINK_GENERIC);
    if (nl == NULL) {
        perror("mnl_socket_open");
        exit(EXIT_FAILURE);
    }

    // Close the netlink socket
    mnl_socket_close(nl);

    return 0;
}
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o libmnl_test4 libmnl_test4.c -lmnl")
        self.cmd("valgrind --leak-check=full ./libmnl_test4 > libmnl.log 2>&1")
        code, libmnl_result = self.cmd("cat libmnl.log", ignore_status=True)
        self.assertIn("All heap blocks were freed -- no leaks are possible", libmnl_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf libmnl_test4.c libmnl_test4 libmnl.log")
