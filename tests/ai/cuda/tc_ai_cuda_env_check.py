#!/usr/bin/python
# -*- encoding: utf-8 -*-

import os
from common.basetest import LocalTest
class Test(LocalTest):
    """
    See tc_ai_cuda_env_check.yaml for details

    :avocado: tags=fix,P1,noarch,local,cuda_ai_container
    """

    def setUp(self):
        super().setUp()
        self.cuda_version = os.environ.get('CUDA_VERSION')
        if not self.cuda_version:
            self.cuda_version = "12.1.1"
        self.os_type = os.environ.get('OS_TYPE')
        if not self.os_type:
            self.os_type = "ubi9"
        self.image_add = os.environ.get('REGISTRY_ADDR')
        self.image_test = "base"
        if "devel" in self.image_add:
            self.image_test = "devel"
        elif "runtime" in self.image_add:
            self.image_test = "runtime"
        self.cmd("git clone https://gitlab.com/nvidia/container-images/cuda.git cuda")


    def test(self):
        envs = set()
        filepath = os.path.join("cuda/dist", self.cuda_version, self.os_type, self.image_test, "Dockerfile")
        with open(filepath) as file:
            line = file.readline()
            while line:
                words = line.split()
                if len(words) >= 2:
                    if words[0] == "ENV":
                        envs.add(words[1])
                line = file.readline()
        for env in envs:
            _, output = self.cmd("echo %s" % env,container_flag=1)
            self.assertTrue(output != "", "Cuda image env check failed, Please check env "+env)

    def tearDown(self):
        super().tearDown()
        self.cmd("rm -rf cuda")
