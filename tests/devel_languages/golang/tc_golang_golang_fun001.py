# -*- encoding: utf-8 -*-

"""
@File:      tc_golang_golang_fun001.py
@Time:      2024/5/14 15:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_golang_golang_fun001.yaml for details

    :avocado: tags=P1,noarch,local,golang
    """
    PARAM_DIC = {"pkg_name": "golang"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > hello.go << EOF
package main
        import (
            "fmt"
               )
func main() {
        fmt.Println("Hello, World!")
}

EOF"""
        self.cmd(cmdline)

    def test(self):        	
        code,result=self.cmd("go run hello.go ")
        self.assertIn("Hello, World!",result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf hello.go")