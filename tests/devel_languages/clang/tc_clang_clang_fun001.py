# -*- encoding: utf-8 -*-

"""
@File:      tc_clang_clang_fun001.py
@Time:      2024/05/14 16:21:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_clang_clang_fun001.yaml for details

    :avocado: tags=P1,noarch,local,clang
    """
    PARAM_DIC = {"pkg_name": "clang"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)      
        self.str = """#include<stdio.h>
int main(void) {
  printf("HelloWorld!");
  return 0;
}
"""
        with open("/tmp/test.c",mode="wt",encoding="utf-8")as f:
            f.write(self.str)

    def test(self):       
        self.cmd("clang -O3 -S -fobjc-arc -emit-llvm /tmp/test.c -o /tmp/test.ll")	
        self.cmd("test -f /tmp/test.ll")
        self.cmd("clang -fmodules -fsyntax-only -Xclang -ast-dump /tmp/test.c")
        self.cmd("clang -S -fobjc-arc /tmp/test.c -o /tmp/test.s")
        self.cmd("test -f /tmp/test.s")	
        self.cmd("clang -fmodules -c /tmp/test.c -o /tmp/test.o")
        self.cmd("test -f /tmp/test.o")
	
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(" rm -rf /tmp/test.c")
        self.cmd(" rm -rf /tmp/test.s")
        self.cmd(" rm -rf /tmp/test.o")
        self.cmd(" rm -rf /tmp/test.ll")		
