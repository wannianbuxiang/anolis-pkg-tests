# -*- encoding: utf-8 -*-

"""
@File:      tc_glibc_glibc_fun001.py
@Time:      2024/5/24 17:10:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_glibc_glibc_fun001.yaml for details

    :avocado: tags=P1,noarch,local,glibc
    """
    PARAM_DIC = {"pkg_name": "gcc glibc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > hello.c << EOF
#include <stdio.h>
int main() {
    printf("Hello World! Welcome to Glibc!");
    return 0;
}
EOF"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o hello hello.c")        	
        code, result=self.cmd("./hello")
        self.assertIn("Hello World! Welcome to Glibc!", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf hello.c hello")