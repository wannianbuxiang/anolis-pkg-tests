# -*- encoding: utf-8 -*-

"""
@File:      tc_glibc_glibc_fun002.py
@Time:      2024/9/23 17:10:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_glibc_glibc_fun001.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "glibc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat<<EOF>input.txt
Hello, World!
EOF"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("iconv -l")
        self.cmd("iconv -f UTF-8 -t GB2312 input.txt -o output.txt")        	
        code, result=self.cmd("cat output.txt")
        self.assertIn("Hello, World!", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf output.txt input.txt ")