#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_lua_lua_fun007.py
@Time:      2024/04/19 10:42:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_lua_lua_fun007.yaml for details

    :avocado: tags=P2,noarch,local,lua,fix
    """
    PARAM_DIC = {"pkg_name": "lua"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/table.lua <<EOF
-- 定义节点类
local Node = {}
Node.__index = Node

function Node.new(key, value)
    return setmetatable({key = key, value = value, prev = nil, next = nil}, Node)
end

-- 定义LRUCache类
local LRUCache = {}
LRUCache.__index = LRUCache

function LRUCache.new(capacity)
    local cache = setmetatable({
        capacity = capacity or 0,
        count = 0,
        head = Node.new(nil, nil),
        tail = Node.new(nil, nil),
        storage = {},
    }, LRUCache)

    cache.head.next = cache.tail
    cache.tail.prev = cache.head

    -- 将节点移动到链表头部
    function cache:moveToHead(node)
        node.prev.next = node.next
        node.next.prev = node.prev
        node.prev = self.head
        node.next = self.head.next
        self.head.next.prev = node
        self.head.next = node
    end

    -- 移除尾部节点
    function cache:removeTail()
        local tail = self.tail.prev
        tail.prev.next = self.tail
        self.tail.prev = tail.prev
        self.storage[tail.key] = nil
        self.count = self.count - 1
    end

    -- 设置或获取键对应的值，更新LRU状态
    function cache:updateOrSet(key, value)
        if self.capacity == 0 then
            return nil
        end
        
        local node = self.storage[key]

        if node then
            node.value = value
            self:moveToHead(node)
        else
            if self.count == self.capacity then
                self:removeTail()
            end
            self.count = self.count + 1

            node = Node.new(key, value)
            self.storage[key] = node
            node.prev = self.head
            node.next = self.head.next
            self.head.next.prev = node
            self.head.next = node
        end
        return value
    end

    -- 获取键对应的值，更新LRU状态
    function cache:get(key)
        if self.capacity == 0 then
            return nil
        end

        local node = self.storage[key]
        if node then
            self:moveToHead(node)
            return node.value
        end
        return nil
    end

    return cache
end

-- 使用示例：
local cache = LRUCache.new(3)  -- 创建一个容量为3的LRU缓存

-- 插入四个元素，注意观察当容量满时的行为
cache:updateOrSet("one", 1)
cache:updateOrSet("two", 2)
cache:updateOrSet("three", 3)
cache:updateOrSet("four", 4)     -- 这会淘汰"one"

-- 访问已存在的元素，然后插入新元素，观察淘汰机制
print(cache:get("one"))   -- 输出：nil
print(cache:get("two"))   -- 输出：2
print(cache:get("three")) -- 输出：3
print(cache:get("four"))   -- 输出：4
cache:updateOrSet("five", 5)     

-- 访问被淘汰的元素
print(cache:get("one"))   -- 输出：nil，因为"one"已经被淘汰
print(cache:get("three")) -- 输出：3
print(cache:get("two"))   -- 输出：nil，因为"two"已经被淘汰
print(cache:get("four"))   -- 输出：4
print(cache:get("five"))   -- 输出：5

-- 插入元素，观察行为
cache:updateOrSet("six", 6)      -- 这会淘汰"three"

-- 再次访问被淘汰的元素
print(cache:get("three"))  -- 输出：nil，因为"four"已经被淘汰
print(cache:get("two"))   -- 输出：nil，因为"two"已经被淘汰
print(cache:get("six"))   -- 输出：6
"""
        self.cmd(cmdline)

    def test(self):
        code, result = self.cmd("lua /tmp/ghm/table.lua")
        self.assertIn("nil\n2\n3\n4\nnil\n3\nnil\n4\n5\nnil\nnil\n6", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")
