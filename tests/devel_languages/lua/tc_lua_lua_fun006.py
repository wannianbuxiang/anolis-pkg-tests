#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_lua_lua_fun006.py
@Time:      2024/04/19 09:42:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_lua_lua_fun006.yaml for details

    :avocado: tags=P2,noarch,local,lua,fix
    """
    PARAM_DIC = {"pkg_name": "lua"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/table.lua <<EOF
-- 读取文件内容并统计单词出现次数
function countWords(file_path)
    local counts = {}
    local io_file = io.open(file_path, "r")

    if io_file then
        for line in io.lines(file_path) do
            -- 分割每行成单词并转换为小写
            for word in string.gmatch(line:lower(), "%w+") do
                counts[word] = (counts[word] or 0) + 1
            end
        end
        io_file:close()
        
        -- 找出出现次数最多的单词及其出现次数
        local max_count, most_common_word = 0, nil
        for word, count in pairs(counts) do
            if count > max_count then
                max_count = count
                most_common_word = word
            end
        end
        
        return most_common_word, max_count
    else
        return nil, "File not found"
    end
end

-- 示例使用
local file_path = arg[1] -- 从命令行参数获取文件路径
local most_common_word, occurrences = countWords(file_path)

if most_common_word then
    print("The most common word is:", most_common_word)
    print("It appears", occurrences, "times.")
else
    print(most_common_word)
end
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("echo '123456\n hello\n 456789\n world\n hello 123abc\n' > /tmp/ghm/testfile.txt")
        code, result = self.cmd("lua /tmp/ghm/table.lua /tmp/ghm/testfile.txt")
        self.assertIn("The most common word is:", result)
        self.assertIn("hello", result)
        self.assertIn("It appears", result)
        self.assertIn("2", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")
