#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_lua_lua_fun004.py
@Time:      2024/04/16 16:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_lua_lua_fun004.yaml for details

    :avocado: tags=P2,noarch,local,lua,fix
    """
    PARAM_DIC = {"pkg_name": "lua"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/file.lua <<EOF
-- 首先，我们需要引入io库，它提供了与操作系统进行输入/输出交互的设施
local io = require("io")
local os = require("os")

-- 创建文件
function createFile(filename)
    local file, error_message = io.open(filename, "w")
    if not file then
        print("Error creating file: ", error_message)
    else
        -- 写入一些内容（可选）
        file:write("This is the content of the new file.")
        file:close()
        print("File created successfully.")
    end
end

-- 查询文件（判断文件是否存在）
function checkFileExists(filename)
    local file_exists = os.rename(filename, filename) and true or false
    if file_exists then
        print(filename, " exists.")
    else
        print(filename, " does not exist.")
    end
    return file_exists
end

-- 修改文件（追加内容）
function appendToFile(filename, content)
    local file, error_message = io.open(filename, "a")
    if not file then
        print("Error opening file: ", error_message)
    else
        file:write(content)
        file:close()
        print("Content appended to the file.")
    end
end

-- 读取并验证文件内容
function readAndVerifyFile(filename, expectedContent)
    local file, error_message = io.open(filename, "r")
    if not file then
        print("Error opening file for reading: ", error_message)
        return false
    end

    local actualContent = file:read("*a")  -- 读取整个文件内容
    file:close()

    if actualContent == expectedContent then
        print("File content matches the expected content.")
        return true
    else
        print("File content does not match the expected content.")
        return false
    end
end

-- 删除文件
function deleteFile(filename)
    local result, error_message = os.remove(filename)
    if result then
        print(filename, " has been deleted.")
    else
        print("Error deleting file: ", error_message)
    end
end

local filename = "/tmp/ghm/file.txt"
local expectedContent = "This is the content of the new file.\\nNew line added."

createFile(filename) -- 创建文件
checkFileExists(filename) -- 查询文件是否存在
appendToFile(filename, "\\nNew line added.") -- 追加内容到文件
readAndVerifyFile(filename, expectedContent)
deleteFile(filename) -- 删除文件
"""
        self.cmd(cmdline)

    def test(self):
        code, result = self.cmd("lua /tmp/ghm/file.lua")
        self.assertIn("File created successfully.", result)
        self.assertIn("exists.", result)
        self.assertIn("Content appended to the file", result)
        self.assertIn("File content matches the expected content.", result)
        self.assertIn("has been deleted.", result)
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")
