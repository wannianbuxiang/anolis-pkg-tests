# -*- encoding: utf-8 -*-

"""
@File:      tc_lua_lua_fun002.py
@Time:      Tue Sep 19 2023 17:53:18
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_lua_lua_fun002.yaml for details

    :avocado: tags=P2,noarch,local,lua
    """
    PARAM_DIC = {"pkg_name": "lua"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)


    def test(self):
        self.cmd("lua -v")
        code,result=self.cmd("lua -h",ignore_status=True)
        self.assertIn("unrecognized option", result)
        code,result=self.cmd("lua -e'a=666' -e 'print(a)'")
        self.assertIn("666", result)
        

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
