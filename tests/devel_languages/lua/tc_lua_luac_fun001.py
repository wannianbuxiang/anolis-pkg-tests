# -*- encoding: utf-8 -*-

"""
@File:      tc_lua_luac_fun001.py
@Time:      2024/9/19 17:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_lua_luac_fun001.yaml for details

    :avocado: tags=P1,noarch,local,lua
    """
    PARAM_DIC = {"pkg_name": "lua"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > script.lua << EOF
print('Hello, world')
EOF"""

        self.cmd(cmdline)

    def test(self):        	
        self.cmd("luac -o script.luac script.lua")
        code,result = self.cmd("lua script.luac")
        self.assertIn("Hello, world", result)
        code,result = self.cmd("luac -l script.lua")
        self.assertIn("script.lua", result)
        self.cmd("luac -p script.lua")
        self.cmd("luac -s -o script.luac script.lua")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf script.lua script.luac luac.out")