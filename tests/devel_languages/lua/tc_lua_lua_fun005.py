#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_lua_lua_fun005.py
@Time:      2024/04/17 09:42:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_lua_lua_fun005.yaml for details

    :avocado: tags=P2,noarch,local,lua,fix
    """
    PARAM_DIC = {"pkg_name": "lua"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/table.lua <<EOF
-- 创建一个空表
local myTable = {}
myTable = {1, 2, 3, 4, 5}  -- 初始化一个包含整数元素的"数组"

-- 访问和打印数组元素
print(myTable[1])  -- 输出：1
print(myTable[3])  -- 输出：3

-- 创建一个混合表（既有数字索引也有字符串索引）
myTable["name"] = "Lua Table Example"
myTable[6] = "sixth element"

-- 访问和打印混合表元素
print(myTable["name"])  -- 输出："Lua Table Example"
print(myTable[6])  -- 输出："sixth element"

-- 遍历表中的所有元素（注意，遍历会按照插入顺序进行，不会跳过nil值）
for key, value in pairs(myTable) do
    print(key, value)
end

-- 向表中添加元素
table.insert(myTable, 7, "seventh element")  -- 在索引7之前插入元素，使得原索引7及其之后的元素依次后移
print(myTable[7])  -- 输出："seventh element"

-- 删除表中的元素
table.remove(myTable, 3)  -- 删除索引为3的元素

-- 计算表的长度（只计算数字连续索引部分的长度）
print(#myTable)  -- 输出：6，因为在索引7处插入元素，但并没有改变最大连续数字索引

-- 创建一个新的键值对
myTable["fruit"] = {"apple", "banana", "cherry"}

-- 访问嵌套表的元素
print(myTable.fruit[1])  -- 输出："apple"
"""
        self.cmd(cmdline)

    def test(self):
        code, result = self.cmd("lua /tmp/ghm/table.lua")
        self.assertIn("1", result)
        self.assertIn("3", result)
        self.assertIn("Lua Table Example", result)
        self.assertIn("sixth element", result)
        self.assertIn("6", result)
        self.assertIn("apple", result)
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")
