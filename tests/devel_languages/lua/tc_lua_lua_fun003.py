#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_lua_lua_fun003.py
@Time:      2024/04/16 17:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_lua_lua_fun003.yaml for details

    :avocado: tags=P2,noarch,local,lua,fix
    """
    PARAM_DIC = {"pkg_name": "lua"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/func.lua <<EOF
-- 定义四个函数分别对应加减乘除四种运算
function addNumbers(a, b)
    return a + b
end

function subtractNumbers(a, b)
    return a - b
end

function multiplyNumbers(a, b)
    return a * b
end

function divideNumbers(a, b)
    if b == 0 then
        return "Error: Division by zero is not allowed."
    else
        return a / b
    end
end

-- 测试这些函数
local resultAdd = addNumbers(10, 20)
print("Addition: ", resultAdd)

local resultSubtract = subtractNumbers(30, 20)
print("Subtraction: ", resultSubtract)

local resultMultiply = multiplyNumbers(5, 7)
print("Multiplication: ", resultMultiply)

local resultDivide = divideNumbers(40, 5)
print("Division: ", resultDivide)

-- 尝试除以零以演示错误处理
local resultDivideByZero = divideNumbers(10, 0)
print("Division by zero: ", resultDivideByZero)
"""
        self.cmd(cmdline)

    def test(self):
        code, result = self.cmd("lua /tmp/ghm/func.lua")
        self.assertIn("30", result)
        self.assertIn("10", result)
        self.assertIn("35", result)
        self.assertIn("8.0", result)
        self.assertIn("Error: Division by zero is not allowed.", result)
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")
