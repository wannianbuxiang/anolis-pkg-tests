# -*- encoding: utf-8 -*-

"""
@File:      tc_c_c_fun002.py
@Time:      Fri Jul 07 2023 18:37:59
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_c_c_fun002.yaml for details

    :avocado: tags=P1,noarch,local,glibc
    """
    PARAM_DIC = {"pkg_name": "glibc gcc-c++"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline='''cat >language_c.c<<EOF
#include <stdio.h>
int main()
{
printf("Hello World!");
return 0;
}
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o language_c language_c.c")
        self.cmd("gcc -S -o language_c.s language_c.c")
        self.cmd("gcc -c -o language_c.o language_c.s")
        code,gcc_result=self.cmd("file language_c.o")
        self.assertIn("relocatable",gcc_result)
        code,gcc_result=self.cmd("./language_c")
        self.assertIn("Hello World!",gcc_result)
        self.cmd("rm -rf language_c language_c.o language_c.s")
        code,gcc_result=self.cmd("ls")
        self.assertNotIn("language_cs",gcc_result)
        self.assertNotIn("language_c.s",gcc_result)
        self.assertNotIn("language_c.o",gcc_result)
        self.cmd("gcc -c -o language_c.o language_c.c")
        self.cmd("gcc -o language_c language_c.o")
        code,gcc_result=self.cmd("file language_c")
        self.assertIn("executable",gcc_result)
        code,gcc_result=self.cmd("./language_c")
        self.assertIn("Hello World!",gcc_result)
        

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf language_c.c language_c language_c.o language_c.s")


