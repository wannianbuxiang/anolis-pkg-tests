# -*- encoding: utf-8 -*-

"""
@File:      tc_c_c_fun001.py
@Time:      Fri Jul 07 2023 18:38:18
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_c_c_fun001.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "gcc-c++"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline='''cat > language_c.c <<EOF
#include <stdio.h>
int main()
{
        printf("Hello World!");
        return 0;
}
EOF
'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o language_c language_c.c")
        code,glibc_result=self.cmd("file language_c")
        self.assertIn("executable",glibc_result)
        code,glibc_result=self.cmd("./language_c")
        self.assertIn("Hello World!",glibc_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf language_c language_c.c")