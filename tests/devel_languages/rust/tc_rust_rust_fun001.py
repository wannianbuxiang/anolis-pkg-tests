# -*- encoding: utf-8 -*-

"""
@File:      tc_rust_rust_fun001.py
@Time:      2024/7/10 10:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_rust_rust_fun001.yaml for details

    :avocado: tags=P1,noarch,local,rust
    """

    PARAM_DIC = {"pkg_name": "rust"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline1 = """cat>test.rs<<EOF    
fn main() {
    println!("Hello, world!");
} 
EOF"""
        cmdline2 = """cat>hello.rs<<EOF
fn main() {
    if cfg!(hello) {
        println!("world!");
    }
}
EOF"""
        cmdline3 = """cat>lib.rs<<EOF
pub fn foo() {}
EOF""" 
        cmdline4 = """cat>war.rs<<EOF
fn main() {
    100u8 << 10;
}
EOF"""
        self.cmd(cmdline1)
        self.cmd(cmdline2)
        self.cmd(cmdline3)
        self.cmd(cmdline4)

    def test(self):        	
        code, result1=self.cmd("rustdoc -h | grep 'Options'")
        self.assertIn("Options", result1)
        self.cmd("rustdoc -V | grep -E '[0-9]'")
        code, result2=self.cmd("rustdoc -v -V | grep -E 'release|version|[0-9]'")
        self.assertIn("rustdoc", result2)
        self.cmd("rustdoc test.rs -o doc --crate-name mycrate")
        code, result3=self.cmd("test -d doc/mycrate && echo $?")
        self.assertIn("0", result3)
        self.cmd("rustdoc test.rs -L doc/")
        code, result4=self.cmd("test -d doc/test && echo $?")
        self.assertIn("0", result4)
        self.cmd("rustdoc hello.rs --cfg hello")
        code, result5=self.cmd("test -d doc/hello && echo $?")
        self.assertIn("0", result5)
        self.cmd("rustdoc test.rs --extern doc")
        self.cmd("rustdoc test.rs -C target_feature=+avx")
        self.cmd("rustdoc --document-private-items test.rs")
        code, result6=self.cmd("test -f doc/test/fn.main.html && echo $?")
        self.assertIn("0", result6)
        code, result7=self.cmd("rustdoc test.rs --test | grep -E 'running|tests'")
        self.assertIn("running", result7)
        self.cmd("rustdoc test.rs --html-in-header doc/hello/all.html")
        self.cmd("rustdoc test.rs --html-after-content doc/hello/all.html")
        self.cmd("rustdoc test.rs --markdown-no-toc")
        cmdline5 = """cat>doc/dark.css<<EOF
body {
    background-color: #1f1f1f;
    color: #ffffff;
    }
    h1 {
    color: #ff9900;
    }
EOF"""
        self.cmd(cmdline5)
        self.cmd("rustdoc test.rs -e doc/dark.css")
        code, result8=self.cmd("test -f doc/theme.css && echo $?")
        self.assertIn("0", result8)
        code, result9=self.cmd("rustdoc lib.rs --crate-type=lib -W missing-docs 2>&1 | grep 'warning'")
        self.assertIn("warning", result9)
        self.cmd("rustdoc lib.rs --crate-type=lib -A missing-docs -o hello_A")
        code, result10=self.cmd("test -d hello_A && echo $?")
        self.assertIn("0", result10)
        code, result11=self.cmd("rustdoc lib.rs --crate-type=lib -D missing-docs 2>&1 | grep 'error'")
        self.assertIn("error", result11)
        self.cmd("rustdoc war.rs --cap-lints warn")
        code, result12=self.cmd("test -d doc/war && echo $?")
        self.assertIn("0", result12)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf ./*.rs doc* hello*")
