# -*- encoding: utf-8 -*-

"""
@File:      tc_gdb_gdb_fun001.py
@Time:      2024/5/15 10:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_gdb_gdb_fun001.yaml for details

    :avocado: tags=P1,noarch,local,gdb
    """
    PARAM_DIC = {"pkg_name": "gdb"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat>/tmp/test.c<<EOF    
#include <stdio.h>
int add(int a,int b)
{
return a+b;
}
int main()
{
int sum[10];
int i;
int array1[10] =
{
48,56,77,33,33,11,226,544,78,90
};
int array2[10] =
{
85,99,66,0x199,393,11,1,2,3,4
};
for(i=0; i<10; i++)
{
sum[i] = add(array1[i],array2[i]);
}
return 0;
}

EOF"""
        self.cmd(cmdline)

    def test(self):        	
        code,result=self.cmd("rpm -qa")
        self.assertIn("gdb", result)
        self.cmd("cd /tmp && gcc test.c -g -o test")
        self.cmd("ls -l /tmp/test")
        cmdline = """gdb /tmp/test<< EOF >/tmp/test.log 2>&1
break main
break 5
info breakpoints
info break 2
disable breakpoints 2
enable breakpoints 2
delete 2
q
EOF"""
        self.cmd(cmdline)
        self.cmd("grep breakpoint /tmp/test.log")
      
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf  /tmp/test.log /tmp/test /tmp/test.c")
