# -*- encoding: utf-8 -*-

"""
@File:      tc_llvm_llvm_fun001.py
@Time:      2024/7/8 16:10:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_llvm_llvm_fun001.yaml for details

    :avocado: tags=P1,noarch,local,llvm
    """
    PARAM_DIC = {"pkg_name": "llvm clang"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

        self.str = """#include<stdio.h>
int main(void) 
{
  printf("Hello, llvm!");
  return 0;
}
"""
        with open("/tmp/llvm_test.c",mode="wt",encoding="utf-8")as f:
            f.write(self.str)

    def test(self):
        self.cmd("clang /tmp/llvm_test.c -o /tmp/test")
        code, result=self.cmd("cd /tmp&& ./test")
        self.assertIn("Hello, llvm!", result)
        self.cmd("clang -o3 -emit-llvm /tmp/llvm_test.c -c -o /tmp/test.bc")
        code, result=self.cmd("lli /tmp/test.bc")
        self.assertIn("Hello, llvm!", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/test /tmp/test.bc /tmp/llvm_test.c")