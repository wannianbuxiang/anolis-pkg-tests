# -*- encoding: utf-8 -*-

"""
@File:      tc_glib2_glib2_fun001.py
@Time:      2024/7/16 17:10:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_glib2_glib2_fun001.yaml for details

    :avocado: tags=P1,noarch,local,glib2
    """
    PARAM_DIC = {"pkg_name": "glib2 glib2-devel gcc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat<<EOF>test.c
#include <glib.h>
int main(void)
{
g_print("Hello, glib2!");
return 0;
}
EOF"""
        self.cmd(cmdline)

    def test(self):
        self.cmd('gcc test.c $(pkg-config --cflags --libs glib-2.0) -o hello')      	
        code, result=self.cmd("./hello")
        self.assertIn("Hello, glib2!", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf hello test.c")