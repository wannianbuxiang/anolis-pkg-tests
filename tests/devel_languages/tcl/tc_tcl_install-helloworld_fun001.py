# -*- encoding: utf-8 -*-

"""
@File:      tc_tcl_install-helloworld_fun001.py
@Time:      2023/6/29 16:36:12
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_tcl_install-helloworld_fun001.yaml for details

    :avocado: tags=P2,noarch,local,tcl
    """
    PARAM_DIC = {"pkg_name": "tcl"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline='''cat >tcl_testfile<<EOF
#!/usr/bin/tclsh
puts stdout {Hello, World!}
EOF'''
        self.cmd(cmdline)

    def test(self):
        code,tcl_result=self.cmd("tclsh tcl_testfile")
        self.assertIn("Hello, World!",tcl_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf tcl_testfile")