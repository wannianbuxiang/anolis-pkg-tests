# -*- encoding: utf-8 -*-

"""
@File:      tc_g++_g++_fun001.py
@Time:      2024/11/12 17:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_g++_g++_fun001.yaml for details

    :avocado: tags=P1,noarch,local,gcc-c++
    """
    PARAM_DIC = {"pkg_name": "gcc-c++"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat >  hello.cpp << EOF
#include <iostream>
int main(int argc,char *argv[])
{
std::cout << "hello, world" << std::endl;
return(0);
}
EOF"""
        self.cmd(cmdline)

    def test(self):        	
        self.cmd("g++ hello.cpp -o hello")
        code,result=self.cmd("./hello")
        self.assertIn("hello, world", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf hello.cpp hello")