# -*- encoding: utf-8 -*-

"""
@File:      tc_python_dateutil_func_002.py
@Time:      2024/03/15 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_python_dateutil_func_002.yaml for details

    :avocado: tags=P2,noarch,local,fix,python
    """
    PARAM_DIC = {"pkg_name": "python3-dateutil"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat > dateutil_test2.py  << EOF
#!/usr/bin/python
# -- coding: UTF-8 --

from dateutil import tz
import datetime

f = open('dateutil_result2.txt', 'w')

test_date = datetime.datetime(2024, 3, 13, 14, 25, 30)
utc_time = test_date.replace(tzinfo=datetime.timezone.utc)
f.write(f"UTC时间: {utc_time} ")
local_time = utc_time.astimezone(tz.tzlocal())
f.write(f"本地时间: {local_time}")
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("python3 dateutil_test2.py")
        code, dateutil_result = self.cmd("cat dateutil_result2.txt")
        self.assertIn("UTC时间: 2024-03-13 14:25:30+00:00", dateutil_result)
        self.assertIn("本地时间: 2024-03-13 22:25:30+08:00", dateutil_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf dateutil_test2.py dateutil_result2.txt")