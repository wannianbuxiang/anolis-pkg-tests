# -*- encoding: utf-8 -*-

"""
@File:      tc_python_pip_func_001.py
@Time:      2024/03/18 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_python_pip_func_001.yaml for details

    :avocado: tags=P2,noarch,local,fix,python
    """
    PARAM_DIC = {"pkg_name": "python3-markupsafe"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > pip_test.py  << EOF
#!/usr/bin/python
# -- coding: UTF-8 --

import requests

response = requests.get('https://gitee.com/')
with open('pip_result.text', 'w') as f:
    f.write(str(response.status_code))
EOF"""
        self.cmd(cmdline)

    def test(self):
        code, pip_result = self.cmd("pip -V")
        self.assertIn("pip 23.3.1", pip_result)
        self.cmd("pip install --user requests==2.20.0")
        code, requests_version1 = self.cmd("pip show requests|grep 'Version' | awk '{print $2}'")
        self.assertTrue('2.20.0' == requests_version1)
        self.cmd("pip install --user --upgrade requests")
        code, requests_version2 = self.cmd("pip show requests|grep 'Version' | awk '{print $2}'")
        self.assertTrue(requests_version2 > requests_version1)
        self.cmd("python3 pip_test.py")
        code, pip_result = self.cmd("cat pip_result.text")
        self.assertIn("200", pip_result)
        self.cmd("pip freeze > requirements_pip.txt")
        self.cmd("ls -l requirements_pip.txt")
        self.cmd("pip uninstall -y requests")
        code, pip_result = self.cmd("pip list")
        self.assertRegex(pip_result, r"Package\s+Version")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf pip_test.py pip_result.text requirements_pip.txt")