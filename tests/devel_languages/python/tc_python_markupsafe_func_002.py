# -*- encoding: utf-8 -*-

"""
@File:      tc_python_markupsafe_func_002.py
@Time:      2024/03/19 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_python_markupsafe_func_002.yaml for details

    :avocado: tags=P2,noarch,local,fix,python
    """
    PARAM_DIC = {"pkg_name": "python3-markupsafe"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > markupsafe_test2.py  << EOF
#!/usr/bin/python
# -- coding: UTF-8 --

from markupsafe import Markup

markup_string = Markup('<em>Safe HTML</em>')

with open('markupsafe_result2.text', 'w') as f:
    f.write(markup_string)
EOF"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("python3 markupsafe_test2.py")
        code, markupsafe_result = self.cmd("cat markupsafe_result2.text")
        self.assertTrue("<em>Safe HTML</em>" == markupsafe_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf markupsafe_test2.py markupsafe_result2.text")