# -*- encoding: utf-8 -*-

"""
@File:      tc_python_pyudev_fun_001.py
@Time:      2024-04-08 16:53:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from common.basetest import LocalTest
from pyudev import version

class Test(LocalTest):
    """
    See tc_python_pyudev_fun_001.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    PARAM_DIC = {"pkg_name": "python3-pyudev"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)


    def test(self):
        pyudev_version = version.__version__
        self.log.info(f"python-pyudev version is '{pyudev_version}'")
        code, rpm_version = self.cmd("rpm -qi python3-pyudev|grep -i  version|awk -F: '{print $2}'")
        self.assertEqual(pyudev_version, rpm_version)
           
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)