# -*- encoding: utf-8 -*-

"""
@File:      tc_python_pyudev_fun_002.py
@Time:      2024-04-09 17:53:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
import os
from common.basetest import LocalTest
from pyudev import Context, Device


class Test(LocalTest):
    """
    See tc_python_pyudev_fun_002.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    # 根据名称查询设备
    def find_device_by_name(self):
        device = Device.from_name(self.context, self.type, self.device_name)
        self.assertEqual(self.device_name, device.sys_name, "设备名称不匹配")
        self.assertEqual(self.device_num, device.sys_number, "设备编号不匹配")
        return device
    
    # 根据文件中查询设备
    def find_device_from_devicefile(self):
        device = Device.from_device_file(self.context, f"/dev/{self.device_name}")
        self.assertEqual(self.device_name, device.sys_name, "设备名称不匹配")
        self.assertEqual(self.device_num, device.sys_number, "设备编号不匹配")

    # 根据设备号查询设备
    def find_device_from_devicenumber(self, device_info):
        device = Device.from_device_number(
            self.context,
            self.type,
            os.makedev(
                os.major(device_info.device_number),
                os.minor(device_info.device_number),
            ),
        )
        self.assertEqual(
            self.device_name,
            device.sys_name,
            f"设备节点名称不是'{self.device_name}'",
        )
        self.assertEqual(self.device_num, device.sys_number, "设备编号不匹配")

    # 从地址中获取设备
    def find_device_from_path(self, device_info):
        path = device_info.sys_path
        device = Device.from_path(self.context, path)
        self.assertEqual(
            self.device_name,
            device.sys_name,
            f"设备节点名称不是'{self.device_name}'",
        )
        self.assertEqual(self.device_num, device.sys_number, "设备编号不匹配")


    PARAM_DIC = {"pkg_name": "python3-pyudev"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.context = Context()
        self.device_name = "nvme0n1"
        self.device_num = "1"
        self.type = "block"

    def test(self):
        # 列出所有块设备
        devices = list(self.context.list_devices(subsystem = self.type))
        self.log.info(devices)
        found_nvme0 = any(
            device.device_node.endswith(self.device_name) for device in devices
        )
        # 当存在nvme0n1块设备的时候开始测试
        if found_nvme0:
            nvme0n1_device = self.find_device_by_name()
            self.find_device_from_devicefile()
            self.find_device_from_devicenumber(nvme0n1_device)
            self.find_device_from_path(nvme0n1_device)
        else:
            self.skip("nvme0n1 device not found")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
