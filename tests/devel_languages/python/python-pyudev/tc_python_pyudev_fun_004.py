# -*- encoding: utf-8 -*-

"""
@File:      tc_python_pyudev_fun_004.py
@Time:      2024-04-10 16:53:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from common.basetest import LocalTest
from pyudev import Context, Enumerator


class Test(LocalTest):
    """
    See tc_python_pyudev_fun_004.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    # 检测nvme0n1设备可以被过滤到
    def is_nvme_detected(self, device_name, devices):
        devlist = [device.sys_name for device in devices]
        if device_name not in devlist:
            self.skip(f"Device '{device_name}' not exist,test skipped.")
        else:
            pass


    # 通过match_subsystem过滤设备
    def find_device_match_subsystem(self, subsystem):
        sub_devices = self.enumerator.match_subsystem(subsystem)
        self.is_nvme_detected(self.device_name, sub_devices)


    # 通过sys_name过滤设备
    def find_device_match_sys_name(self, sysname):
        sub_devices = self.enumerator.match_sys_name(sysname)
        self.is_nvme_detected(self.device_name, sub_devices)


    # 通过tag过滤设备
    def find_device_match_tag(self, tags):
        sub_devices = self.enumerator.match_tag(tags)
        self.is_nvme_detected(self.device_name, sub_devices)


    # 通过parent过滤设备
    """待确认,通过父设备nvme0n1只能查到nvme0n1,查不到nvme0n1p1和nvme0n1p2设备"""

    def find_device_match_parent(self):
        parent_device = list(self.context.list_devices(subsystem=self.subsystem))
        dev_list = [device.sys_name for device in parent_device]
        self.log.info(dev_list)
        child_devices = self.enumerator.match_parent(parent_device[0])
        self.log.info([device.sys_name for device in child_devices])
        self.is_nvme_detected(self.device_name, child_devices)

    # 通过property过滤设备
    def find_device_match_property(self):
        # 设定期望匹配的属性及值
        property_name = "DEVNAME"
        property_value = "/dev/nvme0n1"
        property_sub_devices = self.enumerator.match_property(
            property_name, property_value
        )
        self.log.info([device.sys_name for device in property_sub_devices])
        self.is_nvme_detected(self.device_name, property_sub_devices)

    PARAM_DIC = {"pkg_name": "python3-pyudev"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.subsystem = "block"
        self.device_type = "disk"
        self.device_name = "nvme0n1"
        self.device_tags = "systemd"
        self.context = Context()
        self.enumerator = Enumerator(self.context)

    def test(self):
        # 测试通过match_subsystem过滤设备
        self.find_device_match_subsystem(self.subsystem)
        # 测试通过match_sys_name()过滤设备
        self.find_device_match_sys_name(self.device_name)
        # 测试通过match_tag()过滤设备
        self.find_device_match_tag(self.device_tags)
        # 测试通过match_parent()过滤设备
        self.find_device_match_parent()
        # 测试通过match_property()过滤设备
        self.find_device_match_property()

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
