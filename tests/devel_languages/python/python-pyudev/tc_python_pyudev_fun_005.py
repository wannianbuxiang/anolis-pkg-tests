# -*- encoding: utf-8 -*-

"""
@File:      tc_python_pyudev_fun_005.py
@Time:      2024-04-10 16:53:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
import os
import time
from common.basetest import LocalTest
from pyudev import Context, Monitor, MonitorObserver


class Test(LocalTest):
    """
    See tc_python_pyudev_fun_005.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    def simulate_input_event(self, device_node):
        """
        通过系统命令模拟触发input事件
        需要root权限

        :param device_node: 设备节点名称，用于指定要模拟事件的设备
        """
        command = f"sudo udevadm trigger --action=add --subsystem-match=input --sysname-match={device_node}"
        os.system(command)

    def print_device_event(self, device):
        """
        打印输入设备事件

        :param device: 事件发生的设备信息
        """
        if device.action in ["add", "remove"]:
            print(f"Event captured: {device.action.upper()} on {device.device_path}")
            self.event_detected = True

    PARAM_DIC = {"pkg_name": "python3-pyudev"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.context = Context()
        self.enumerator = Monitor.from_netlink(self.context)
        self.enumerator.filter_by(subsystem="input")
        self.event_detected = False

    def test(self):
        """
        监控输入设备事件并验证
        """
        observer = MonitorObserver(
            self.enumerator, callback=self.print_device_event, name="monitor-observer"
        )
        observer.start()
        print("Start monitoring input devices...")
        time.sleep(3)  # Provide time to capture events
        # 模拟一个输入事件，需要root权限
        self.simulate_input_event("event2")
        time.sleep(1)
        print("Stop monitoring")
        observer.stop()

        # 使用assert断言来校验是否成功检测到事件
        self.assertTrue(self.event_detected, "No input event was detected.")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
