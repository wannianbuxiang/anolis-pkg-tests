# -*- encoding: utf-8 -*-

"""
@File:      tc_python_pyudev_fun_003.py
@Time:      2024-04-09 16:53:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from common.basetest import LocalTest
from pyudev import Context, Device, Monitor


class Test(LocalTest):
    """
    See tc_python_pyudev_fun_003.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    PARAM_DIC = {"pkg_name": "python3-pyudev"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.context = Context()
        self.device_name = "nvme0n1"
        self.type = "block"
        self.attr_list = [
            "sys_name",
            "sys_number",
            "device_links",
            "device_node",
            "device_number",
            "device_path",
            "device_type",
            "subsystem",
        ]

    def test(self):
        """Check if all specified attributes exist and are not None for the device."""
        devices = list(self.context.list_devices(subsystem = self.type))
        found_nvme0 = any(
            device.device_node.endswith(self.device_name) for device in devices
        )
        if found_nvme0:
            nvme0n1_device = Device.from_name(self.context, self.type, self.device_name)
            for item in self.attr_list:
                if not hasattr(nvme0n1_device, item):
                    self.fail(f"Device attribute '{item}' does not exist.")
                attr_value = getattr(nvme0n1_device, item)
                if attr_value is None:
                    self.fail(f"Attribute '{item}' is None, expected a non-None value.")
                self.assertIsNotNone(str(attr_value))
        else:
            self.skip(f"Device '{self.device_name}' not exist,test skipped.")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
