# -*- encoding: utf-8 -*-

"""
@File:      tc_python_pyudev_fun_006.py
@Time:      2024-04-10 16:53:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from common.basetest import LocalTest
from pyudev import Context, Device
from pyudev import _errors


class Test(LocalTest):
    """
    See tc_python_pyudev_fun_006.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    def device_not_found_at_path_error(self):
        """ 
        测试指定路径下设备不存在时抛DeviceNotFoundAtPathError
        """
        with self.assertRaises(getattr(_errors, self.error_list[0])):
            # 路径无效
            invalid_path = "/dev/nonexistent"
            Device.from_path(self.context, invalid_path)

    def device_not_found_by_file_error(self):
        """ 
        测试通过无效文件路径查找设备时抛DeviceNotFoundByFileError
        """
        with self.assertRaises(getattr(_errors, self.error_list[1])):
            # 无效文件
            invalid_file = "/sys/nonexistent"
            Device.from_device_file(self.context, invalid_file)

    def device_not_found_by_name_error(self):
        """ 
        测试指定设备名不存在时抛DeviceNotFoundByNameError的异常
        """
        with self.assertRaises(getattr(_errors, self.error_list[2])):
            # 设备名无效
            invalid_name = "nonexistentdevice"
            Device.from_name(self.context, "block", invalid_name)

    def device_not_found_by_number_error(self):
        """ 
        测试指定设备号不存在时抛DeviceNotFoundByNumberError的异常
        """
        with self.assertRaises(getattr(_errors, self.error_list[3])):
            # 假定设备号无效
            invalid_type = "block"
            invalid_number = 999
            Device.from_device_number(self.context, invalid_type, invalid_number)

    def device_not_found_error(self):
        """ 
        测试获取不存在的设备时抛DeviceNotFoundError的异常
        """
        with self.assertRaises(getattr(_errors, self.error_list[4])):
            # 尝试从不存在的UDEV_DEVICE环境变量获取设备
            Device.from_environment(self.context)
            
            
    PARAM_DIC = {"pkg_name": "python3-pyudev"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.context = Context()
        self.error_list = [
            "DeviceNotFoundAtPathError",
            "DeviceNotFoundByFileError",
            "DeviceNotFoundByNameError",
            "DeviceNotFoundByNumberError",
            "DeviceNotFoundError",
        ]

    def test(self):
        self.device_not_found_at_path_error()
        self.device_not_found_by_file_error()
        self.device_not_found_by_name_error()
        self.device_not_found_by_number_error()
        self.device_not_found_error()

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
