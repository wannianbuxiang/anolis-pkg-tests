# -*- encoding: utf-8 -*-

"""
@File:      tc_python_python_fun_001.py
@Time:      Thu Sep 21 2023 16:41:51
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_python_python_fun_001.yaml for details

    :avocado: tags=P1,noarch,local,python
    """
    PARAM_DIC = {"pkg_name": ""}
    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        cmdline = '''cat > hello.py  << EOF
#!/usr/bin/python
# Filename : hello.py
print ("hello world!")
EOF'''
        self.cmd(cmdline)
        code,result=self.cmd("python3 hello.py")
        self.assertIn("hello world!",result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf hello.py")