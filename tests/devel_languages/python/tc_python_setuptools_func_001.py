# -*- encoding: utf-8 -*-

"""
@File:      tc_python_setuptools_func_001.py
@Time:      2024/03/20 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_python_setuptools_func_001.yaml for details

    :avocado: tags=P2,noarch,local,fix,python
    """
    PARAM_DIC = {"pkg_name": "python3-setuptools"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd('pip install wheel')
        # 创建项目目录结构
        self.cmd('mkdir -p setuptools_test_project/src/test_package')
        self.cmd('mkdir -p setuptools_test_project/scripts')
        self.cmd('mkdir -p setuptools_test_project/data')
        # 创建 Python 包文件
        self.cmd('touch setuptools_test_project/src/test_package/__init__.py')
        cmdline = """cat > setuptools_test_project/src/test_package/module.py  << EOF
#!/usr/bin/python
# -- coding: UTF-8 --

def main():
    print("Hello from test_package.module!")
EOF"""
        self.cmd(cmdline)
        # 创建脚本文件
        cmdline = """cat > setuptools_test_project/scripts/test_script  << EOF
#!/usr/bin/python
# -- coding: UTF-8 --

from test_package.module import main

if __name__ == '__main__':
    main()
EOF"""
        self.cmd(cmdline)
        # 创建数据文件
        self.cmd('echo "Sample data contents" > setuptools_test_project/data/data_file.dat')
        # 创建 setup.py 文件
        cmdline = """cat > setuptools_test_project/setup.py  << EOF
#!/usr/bin/python
# -- coding: UTF-8 --

from setuptools import setup, find_packages, Command

class CustomCommand(Command):
    description = 'A custom command to print a message'
    user_options = []  # 必须有一个名为user_options的属性

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        print("Running custom command!")

setup(
    name='setuptools_test_project',
    version='0.2',
    packages=find_packages(where="src"),
    package_dir={"": "src"},
    include_package_data=True,
    install_requires=[
        'requests',
    ],
    extras_require={
        'dev': [
            'pytest',
        ],
    },
    entry_points={
        'console_scripts': [
            'test-script=test_package.module:main',
        ],
    },
    cmdclass={
        'mycmd': CustomCommand,
    },
    scripts=['scripts/test_script']
)
EOF"""
        self.cmd(cmdline)
        # 创建 MANIFEST.in文件
        self.cmd('echo "include data/*" > setuptools_test_project/MANIFEST.in')
        # 创建 README.md 文件
        cmdline = """cat > setuptools_test_project/README.md  << EOF
# Your Project

This is a description of your project.
EOF"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("cd setuptools_test_project/;python setup.py sdist bdist_wheel")
        self.cmd("cd setuptools_test_project/;pip install .[dev]")
        code, setuptools_result = self.cmd("cd setuptools_test_project/;python setup.py mycmd")
        self.assertIn('Running custom command!', setuptools_result)
        code, setuptools_result = self.cmd("cd setuptools_test_project/;test-script")
        self.assertIn('Hello from test_package.module!', setuptools_result)
        code, setuptools_result = self.cmd("cd setuptools_test_project/;python setup.py test")
        self.assertIn('running test', setuptools_result)
        self.cmd("cd setuptools_test_project/;python setup.py clean --all")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('pip uninstall -y wheel')
        self.cmd("rm -rf setuptools_test_project")