# -*- encoding: utf-8 -*-

"""
@File:      tc_python_markdown_func_001.py
@Time:      2024/03/14 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_python_markdown_func_001.yaml for details

    :avocado: tags=P2,noarch,local,fix,python
    """
    PARAM_DIC = {"pkg_name": "python3-markdown"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > markdown_test.py  << 'EOF'
#!/usr/bin/python
# -- coding: UTF-8 --

import markdown

# 定义一个Markdown文本
md_text = '''
# 标题 H1

## 标题 H2

这是一个段落的文本。以下是一个引用：

> 这是一个引用段落。
>
> 包含多行文本。

### 无序列表

- 列表项一
- 列表项二
  - 子列表项一
  - 子列表项二

### 有序列表

1. 列表项一
2. 列表项二
   1. 子列表项一
   2. 子列表项二

### 图片

![Python Logo](https://www.python.org/static/community_logos/python-logo.png)

### 链接

这是一个链接：[Python官网](https://www.python.org)

### 强调

**这是粗体文本**

*这是斜体文本*

### 表格

| 列表头一 | 列表头二 | 列表头三 |
|---------|---------|---------|
| 单元格1 | 单元格2 | 单元格3 |
| 单元格4 | 单元格5 | 单元格6 |

### 代码

这是一个`行内代码`的例子。
```
python
# 这是一个代码块
def hello():
    print("Hello, World!")
```
'''

# 使用markdown库将Markdown文本转换为HTML
with open('markdown_test.md', 'w') as f:
    html = markdown.markdown(md_text, extensions=['extra', 'codehilite'])
    f.write(html)
EOF"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("python3 markdown_test.py")
        code, markdown_result = self.cmd("cat markdown_test.md")
        # 设定一些我们期望找到的HTML标签
        expected_tags = ['<h1>', '<h2>', '<blockquote>', '<strong>', '<em>', '<table>', '<code>', '<pre', '<ul>', '<ol>', '<li>', '<a', '<img']
        # 检查是否每个标签都在HTML输出中
        for tag in expected_tags:
            self.assertIn(tag, markdown_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf markdown_test.py markdown_test.md")