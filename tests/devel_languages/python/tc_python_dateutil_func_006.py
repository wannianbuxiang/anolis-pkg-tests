# -*- encoding: utf-8 -*-

"""
@File:      tc_python_dateutil_func_006.py
@Time:      2024/03/15 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_python_dateutil_func_006.yaml for details

    :avocado: tags=P2,noarch,local,fix,python
    """
    PARAM_DIC = {"pkg_name": "python3-dateutil"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat > dateutil_test6.py  << EOF
#!/usr/bin/python
# -- coding: UTF-8 --

import datetime
from dateutil.relativedelta import relativedelta

f = open('dateutil_result6.txt', 'w')

current_date = datetime.datetime(2024, 3, 13)
f.write(f"当前日期: {current_date}")
two_weeks_later = current_date + relativedelta(weeks=2)
f.write(f"两周后: {two_weeks_later}")
two_hours_before = current_date - relativedelta(hours=2)
f.write(f"两小时前: {two_hours_before}")
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("python3 dateutil_test6.py")
        code, dateutil_result = self.cmd("cat dateutil_result6.txt")
        self.assertIn("当前日期: 2024-03-13 00:00:00", dateutil_result)
        self.assertIn("两周后: 2024-03-27 00:00:00", dateutil_result)
        self.assertIn("两小时前: 2024-03-12 22:00:00", dateutil_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf dateutil_test6.py dateutil_result6.txt")