# -*- encoding: utf-8 -*-

"""
@File:      tc_python_dateutil_func_004.py
@Time:      2024/03/15 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_python_dateutil_func_004.yaml for details

    :avocado: tags=P2,noarch,local,fix,python
    """
    PARAM_DIC = {"pkg_name": "python3-dateutil"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat > dateutil_test4.py  << EOF
#!/usr/bin/python
# -- coding: UTF-8 --

import datetime
from dateutil.rrule import rrule, DAILY

f = open('dateutil_result4.txt', 'w')

start_date = datetime.datetime(2024, 3, 13)
rrule_dates = list(rrule(DAILY, count=10, dtstart=start_date))
f.write(f"从 {start_date} 开始连续10天的日期: ")
for date in rrule_dates:
    f.write(f"{date.date()} ")
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("python3 dateutil_test4.py")
        code, dateutil_result = self.cmd("cat dateutil_result4.txt")
        self.assertIn("2024-03-13", dateutil_result)
        self.assertIn("2024-03-22", dateutil_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf dateutil_test4.py dateutil_result4.txt")