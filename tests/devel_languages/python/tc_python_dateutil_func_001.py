# -*- encoding: utf-8 -*-

"""
@File:      tc_python_dateutil_func_001.py
@Time:      2024/03/15 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_python_dateutil_func_001.yaml for details

    :avocado: tags=P2,noarch,local,fix,python
    """
    PARAM_DIC = {"pkg_name": "python3-dateutil"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat > dateutil_test1.py  << EOF
#!/usr/bin/python
# -- coding: UTF-8 --

from dateutil import parser

f = open('dateutil_result1.txt', 'w')
date_strings = ["2024-03-13", "March 13, 2024", "13/03/2024"]

for date_string in date_strings:
    parsed_date = parser.parse(date_string)
    f.write(f"{date_string} 解析为 {parsed_date} ")
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("python3 dateutil_test1.py")
        code, dateutil_result = self.cmd("cat dateutil_result1.txt")
        self.assertIn("2024-03-13 解析为 2024-03-13 00:00:00", dateutil_result)
        self.assertIn("March 13, 2024 解析为 2024-03-13 00:00:00", dateutil_result)
        self.assertIn("13/03/2024 解析为 2024-03-13 00:00:00", dateutil_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf dateutil_test1.py dateutil_result1.txt")