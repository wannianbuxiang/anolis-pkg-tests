# -*- encoding: utf-8 -*-

"""
@File:      tc_python_mako_func_001.py
@Time:      2024/03/19 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_python_mako_func_001.yaml for details

    :avocado: tags=P2,noarch,local,fix,python
    """
    PARAM_DIC = {"pkg_name": "python3-mako"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > mako_test.py  << 'EOF'
#!/usr/bin/python
# -- coding: UTF-8 --

from mako.template import Template
from mako.runtime import Context
from io import StringIO

template_mako = '''
<%!
    def greeting(name):
        return "Hello, " + name + "!"
%>

<html>
<body>
    <h1>${greeting("World")}</h1>
</body>
</html>
'''

# 创建一个模板对象
mytemplate = Template(template_mako)

# 准备一个上下文环境，它带有 name 参数
buf = StringIO()
ctx = Context(buf, name="User")

# 渲染模板
mytemplate.render_context(ctx)

with open('mako_test.html', 'w') as f:
    f.write(buf.getvalue())
EOF"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("python3 mako_test.py")
        code, mako_result = self.cmd("cat mako_test.html")
        self.assertIn("Hello, World!", mako_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf mako_test.py mako_test.html")