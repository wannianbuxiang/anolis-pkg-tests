# -*- encoding: utf-8 -*-

"""
@File:      tc_python_dateutil_func_003.py
@Time:      2024/03/15 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_python_dateutil_func_003.yaml for details

    :avocado: tags=P2,noarch,local,fix,python
    """
    PARAM_DIC = {"pkg_name": "python3-dateutil"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat > dateutil_test3.py  << EOF
#!/usr/bin/python
# -- coding: UTF-8 --

from dateutil.relativedelta import relativedelta
import datetime

f = open('dateutil_result3.txt', 'w')

current_date = datetime.datetime(2024, 3, 13, 14, 25, 30)
f.write(f"当前日期: {current_date} ")
next_month = current_date + relativedelta(months=1)
f.write(f"下个月的日期: {next_month}")
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("python3 dateutil_test3.py")
        code, dateutil_result = self.cmd("cat dateutil_result3.txt")
        self.assertIn("当前日期: 2024-03-13 14:25:30", dateutil_result)
        self.assertIn("下个月的日期: 2024-04-13 14:25:30", dateutil_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf dateutil_test3.py dateutil_result3.txt")