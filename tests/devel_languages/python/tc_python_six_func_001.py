# -*- encoding: utf-8 -*-

"""
@File:      tc_python_six_func_001.py
@Time:      2024/03/21 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_python_six_func_001.yaml for details

    :avocado: tags=P2,noarch,local,fix,python
    """
    PARAM_DIC = {"pkg_name": "python3-six"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmdline1 = """cat > six_test.py  << 'EOF'
import six

# 测试字符串类型的检查
result = "String type check normal" if isinstance(u"hello", six.integer_types) else "String type check exception"

with open('six_result.text', 'w') as f:
    f.write(result)
EOF"""

        self.cmdline2 = """cat > six_test.py  << 'EOF'
import six

# 测试列表迭代
result = "List iteration normal" if list(six.moves.range(3)) == [0, 1, 2] else "List iteration exception"

with open('six_result.text', 'w') as f:
    f.write(result)
EOF"""

        self.cmdline3 = """cat > six_test.py  << 'EOF'
import six

# 测试字典迭代
test_dict = {'one': 1, 'two': 2}
result1 = result2 = False
for key, value in six.iteritems(test_dict):
    result1 = True if key in test_dict else False
    result2 = True if value == test_dict[key] else False
result = "Dict iteration normal" if result1 and result2 else "Dict iteration exception"

with open('six_result.text', 'w') as f:
    f.write(result)
EOF"""

        self.cmdline4 = """cat > six_test.py  << 'EOF'
import six

# 测试字典映射
test_dict = {'one': 1, 'two': 2}
result = "Dict mapping normal" if list(six.iterkeys(test_dict)) == ['one', 'two'] else "Dict mapping exception"

with open('six_result.text', 'w') as f:
    f.write(result)
EOF"""

        self.cmdline5 = """cat > six_test.py  << 'EOF'
import six

# 示例功能函数
def divide(a, b):
    if six.PY2:
        return a / b
    else:
        return a // b  # 为 Python 3 明确使用整数除法

# 测试divide函数以确保它在Python 2和3中行为一致
result = divide(10, 2)

with open('six_result.text', 'w') as f:
    f.write(str(result))
EOF"""

    def test(self):
        cmdlines = [self.cmdline1, self.cmdline2, self.cmdline3, self.cmdline4, self.cmdline5]
        for cmdline in cmdlines:
            self.cmd(cmdline)
            self.cmd("python3 six_test.py")
            code, six_result1 = self.cmd("cat six_result.text")
            self.cmd(cmdline, container_flag=1)
            code, pwd = self.cmd("pwd")
            self.cmd("python %s/six_test.py"%pwd, container_flag=1)
            code, six_result2 = self.cmd("cat %s/six_result.text"%pwd, container_flag=1)
            self.assertTrue(six_result1 == six_result2)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf six_test.py six_result.text")