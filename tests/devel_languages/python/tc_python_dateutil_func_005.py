# -*- encoding: utf-8 -*-

"""
@File:      tc_python_dateutil_func_005.py
@Time:      2024/03/15 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_python_dateutil_func_005.yaml for details

    :avocado: tags=P2,noarch,local,fix,python
    """
    PARAM_DIC = {"pkg_name": "python3-dateutil"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat > dateutil_test5.py  << EOF
#!/usr/bin/python
# -- coding: UTF-8 --

import datetime
from dateutil.rrule import rrule, MONTHLY

f = open('dateutil_result5.txt', 'w')

start_date = datetime.datetime(2024, 3, 13)
# 获取从开始日期后6个星期一的日期，0 表示星期一
rule = rrule(MONTHLY, byweekday=0, count=6, dtstart=start_date)
for date in rule:
    f.write(f"{date.date()} ")
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("python3 dateutil_test5.py")
        code, dateutil_result = self.cmd("cat dateutil_result5.txt")
        self.assertIn("2024-03-18", dateutil_result)
        self.assertIn("2024-04-22", dateutil_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf dateutil_test5.py dateutil_result5.txt")