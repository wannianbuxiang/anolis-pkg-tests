# -*- encoding: utf-8 -*-

"""
@File:      tc_python_linux_procfs_cpuinfo_001.py
@Time:      2024-04-02 14:52:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_python_linux_procfs_cpuinfo_001.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    PARAM_DIC = {"pkg_name": "python3-linux-procfs"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def nr_cpus(self):
        self.assertIsInstance(self.cpus.nr_cpus, int)
        self.assertGreater(self.cpus.nr_cpus, 0)
        self.log.info(f"nr_cpus的值为{self.cpus.nr_cpus}")
        return self.cpus.nr_cpus

    def nr_sockets(self):
        self.assertIsInstance(self.cpus.nr_sockets, int)
        self.assertGreaterEqual(self.cpus.nr_sockets, 1)
        self.log.info(f"nr_sockets的值为{self.cpus.nr_sockets}")
        return self.cpus.nr_sockets

    def nr_cores(self):
        self.assertIsInstance(self.cpus.nr_cores, int)
        self.assertGreaterEqual(self.cpus.nr_cores, 1)
        self.log.info(f"nr_cores的值为{self.cpus.nr_cores}")
        return self.cpus.nr_cores

    def socket_ids(self):
        self.assertIsInstance(self.cpus.sockets, list)
        self.log.info(f"cpu插槽id列表为{self.cpus.sockets}")
        return self.cpus.sockets

    def common_cpu_info(self):
        model_name = self.cpus["model name"]
        cache_size = self.cpus["cache size"]

        self.assertIsInstance(model_name, str)
        self.assertIsInstance(cache_size, str)
        self.assertGreater(len(model_name), 0)
        self.assertRegex(cache_size, r"\d+ KB")
        self.log.info(f"model_name为{model_name},cache_size为{cache_size}")
        return model_name, cache_size

    def manual_parse_cpuinfo(self, filename="/proc/cpuinfo"):
        """手动解析 /proc/cpuinfo 文件以获取 CPU 相关信息"""
        # 初始化统计数据
        nr_cpus = 0
        nr_cores = 0
        cores_per_socket = {}
        sockets = set()
        model_name = None
        cache_size = None
        with open(filename) as f:
            for line in f:
                if not line.strip():
                    # 如果行为空（表示一个处理器段的结束），就重置核心数
                    nr_cores = 0
                    continue
                key, value = map(str.strip, line.strip().split(":", 1))
                if key == "processor":
                    nr_cpus += 1
                elif key == "physical id":
                    sockets.add(value)
                elif key == "cpu cores":
                    if nr_cores == 0:  # 确保只取第一次出现的值
                        nr_cores = int(value)
                        # 将得到的核心数添加到对应的socket下
                        cores_per_socket[value] = nr_cores
                elif key == "model name":
                    model_name = value
                elif key == "cache size":
                    cache_size = value

        # 计算socket数
        nr_sockets = len(sockets)
        # 从cores_per_socket获取总核心数
        total_cores = sum(cores_per_socket.values())
        self.log.info(
            f"重新解析出来的nr_cpus为{nr_cpus},nr_sockets为{nr_sockets},total_cores为{total_cores},sockets为{list(sockets)},model_name为{model_name},cache_size为{cache_size}"
        )
        return nr_cpus, nr_sockets, total_cores, list(sockets), model_name, cache_size

    def test(self):
        from procfs import cpuinfo
        self.cpus = cpuinfo()
        # 获取调用python_linux_procfs解析出来的值
        nr_cpus_procfs = self.nr_cpus()
        nr_sockets_procfs = self.nr_sockets()
        nr_cores_procfs = self.nr_cores()
        sockets_procfs = self.socket_ids()
        model_name_procfs, cache_size_procfs = self.common_cpu_info()

        # 获取手动解析的值
        (
            nr_cpus_manual,
            nr_sockets_manual,
            nr_cores_manual,
            sockets_manual,
            model_name_manual,
            cache_size_manual,
        ) = self.manual_parse_cpuinfo()

        # 比较 CPU 相关数目是否一致
        self.assertEqual(nr_cpus_manual, nr_cpus_procfs, "Mismatch in CPU count.")
        self.assertEqual(
            nr_sockets_manual, nr_sockets_procfs, "Mismatch in socket count."
        )
        self.assertEqual(nr_cores_manual, nr_cores_procfs, "Mismatch in core count.")
        self.assertEqual(
            sockets_manual, sockets_procfs, "Mismatch in socket identifiers."
        )
        self.assertEqual(
            model_name_manual, model_name_procfs, "Mismatch in model name."
        )
        self.assertEqual(
            cache_size_manual, cache_size_procfs, "Mismatch in cache size."
        )

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
