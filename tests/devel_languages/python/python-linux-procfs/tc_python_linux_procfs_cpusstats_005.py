# -*- encoding: utf-8 -*-

"""
@File:      tc_python_linux_procfs_cpusstats_005.py
@Time:      2024-04-03 14:52:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_python_linux_procfs_cpusstats_005.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    PARAM_DIC = {"pkg_name": "python3-linux-procfs"}

    def get_user_value(self, user):
        return user.lstrip("<").rstrip(">").split(",")[0]
    
    def get_user_value_num(self, user_value):
        # 提取 user: 后面的数值部分，并去除前后空格
        parts = user_value.split(':')
        if len(parts) == 2:
            return parts[1].strip()
        return ""

    def setUp(self):
        super().setUp(self.PARAM_DIC)


    def test(self):
        from procfs import cpusstats
        from procfs import cpustat
        self.cpuSTATS = cpusstats()
        stats = [f"{self.cpuSTATS[cpu]}" for cpu in self.cpuSTATS]
        cmd_output = self.cmd("grep '^cpu' /proc/stat")[1]
        cmd_lines = cmd_output.strip().split("\n")
        stats_repr_cpustat = [repr(cpustat(line.split())) for line in cmd_lines]
        # 比较列表长度
        self.assertEqual(len(stats), len(stats_repr_cpustat), "The lengths of the statistics lists do not match.")
        #比较列表中user的值
        for stat, stats_repr_cpustat in zip(stats, stats_repr_cpustat):
            user_value1 = self.get_user_value(stat)
            user_value2 = self.get_user_value(stats_repr_cpustat)

            # 增加容错机制
            if user_value1 != user_value2:
                try:
                    user_value1_int = int(self.get_user_value_num(user_value1))
                    user_value2_int = int(self.get_user_value_num(user_value2))
                    tolerance = 1  # 容差范围
                    if abs(user_value1_int - user_value2_int) > tolerance:
                        raise AssertionError(f"User values differ by more than the tolerance: {user_value1} with {user_value2}")
                except ValueError:
                    raise AssertionError(f"User values do not match: {user_value1} with {user_value2}")
                
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)