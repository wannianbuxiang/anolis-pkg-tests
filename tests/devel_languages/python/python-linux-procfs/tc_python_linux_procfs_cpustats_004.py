# -*- encoding: utf-8 -*-

"""
@File:      tc_python_linux_procfs_cpustats_004.py
@Time:      2024-04-03 14:52:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_python_linux_procfs_cpustats_004.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    PARAM_DIC = {"pkg_name": "python3-linux-procfs"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        from procfs import cpustat
        # 获取/proc/stat文件中包含cpu的数据行
        cmdline = self.cmd("grep '^cpu '  /proc/stat")
        self.cpu_cmd = cmdline[1].split()[0:8]
        self.cpuSTAT = cpustat(self.cpu_cmd)
        # 获取cpu数据行中的name、user、nice、system、idle、iowait、irq、softirq参数
        attr = ["name", "user", "nice", "system", "idle", "iowait", "irq", "softirq"]
        for i, item in enumerate(attr):
            attr_value = getattr(self.cpuSTAT, item)
            self.assertEqual(
                str(attr_value), self.cpu_cmd[i], f"{str(attr_value)}和{self.cpu_cmd[i]}不相等"
            )

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
