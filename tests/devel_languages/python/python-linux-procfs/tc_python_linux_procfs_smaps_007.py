# -*- encoding: utf-8 -*-

"""
@File:      tc_python_linux_procfs_smaps_007.py
@Time:      2024-04-03 14:52:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from common.basetest import LocalTest



class Test(LocalTest):
    """
    See tc_python_linux_procfs_smaps_007.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    PARAM_DIC = {"pkg_name": "python3-linux-procfs"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)


    def test(self):
        from procfs import pidstats
        from procfs import smaps
        processes = pidstats()
        sshd = processes.find_by_name('sshd')
        s = smaps(int(sshd[0]))
        found_entries = s.find_by_name_fragment('sshd')
        if not found_entries:
            raise ValueError("found_entries is empty")
        try:
            all_entries_contain_ssh = all('sshd' in entry.name for entry in found_entries)
        except AttributeError:
            raise ValueError("An entry or entry.name is missing")     
        if all_entries_contain_ssh:
            self.log.info("All entry names contain 'sshd'.")
        else:
            self.log.info("Not all entry names contain 'sshd'. Some may be missing or do not meet the criteria.")
           
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)