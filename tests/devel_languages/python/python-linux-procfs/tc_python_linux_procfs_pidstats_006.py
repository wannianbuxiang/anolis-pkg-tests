# -*- encoding: utf-8 -*-

"""
@File:      tc_python_linux_procfs_pidstats_006.py
@Time:      2024-04-07 10:52:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
import re
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_python_linux_procfs_pidstats_006.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    PARAM_DIC = {"pkg_name": "python3-linux-procfs"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        from procfs import pidstats
        self.pidStats = pidstats()
        #使用find_by_name函数获取chronyd的pid
        chronyd_pids = self.pidStats.find_by_name("chronyd")
        if chronyd_pids is None:
            self.fail("chronyd pid is None")
        elif not isinstance(chronyd_pids, list):
            self.fail("chronyd pid is not list")
        elif not chronyd_pids:
            self.fail("chronyd pid list is empty")
        else:
            self.log.info("chronyd pid is %s" % chronyd_pids)
            
        #使用find_by_cmdline_regex获取systemd-userwork的pid
        systemd_userwork_regex = re.compile(r'systemd-userwork')
        systemd_userwork_pids = self.pidStats.find_by_cmdline_regex(systemd_userwork_regex)
        code, systemd_userwork_outputs = self.cmd("ps -ef|grep -i systemd-userwork|grep -v grep|awk '{print $2}'")
        systemd_userwork_outputs = [int(item) for item in systemd_userwork_outputs.split("\n")]
        self.assertEqual(systemd_userwork_outputs, systemd_userwork_pids)
   

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
