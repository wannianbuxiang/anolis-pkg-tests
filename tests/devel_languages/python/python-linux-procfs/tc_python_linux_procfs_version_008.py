# -*- encoding: utf-8 -*-

"""
@File:      tc_python_linux_procfs_version_008.py
@Time:      2024-04-08 16:30:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_python_linux_procfs_version_008.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    PARAM_DIC = {"pkg_name": "python3-linux-procfs"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)


    def test(self):
        import procfs
        procfs_version = procfs.VERSION
        code, rpm_version = self.cmd("rpm -qi python3-linux-procfs|grep -i  version|awk -F: '{print $2}'")
        self.assertEqual(procfs_version, rpm_version)
           
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)