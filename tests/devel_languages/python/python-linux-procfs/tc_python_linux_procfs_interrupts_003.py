# -*- encoding: utf-8 -*-

"""
@File:      tc_python_linux_procfs_interrupts_003.py
@Time:      2024-04-03 14:52:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
import re
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_python_linux_procfs_interrupts_003.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    PARAM_DIC = {"pkg_name": "python3-linux-procfs"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)


    def test(self):
        from procfs import interrupts
        self.interrupts = interrupts()
        #通过find_by_user获取ttyS0的数
        ttyS0_irq = self.interrupts.find_by_user("ttyS0")
        if ttyS0_irq is None:
            self.skip("ttyS0 的中断信息未找到")
        if ttyS0_irq not in self.interrupts:
            self.skip(f"ttyS0 的中断号 {ttyS0_irq} 未在 /proc/interrupts 中找到")            
        thunderbolt = self.interrupts[ttyS0_irq]     
        self.assertIn('IO-APIC', thunderbolt['type'], msg=f"/proc/interrupts 未解析成功，解析结果为{thunderbolt}")
        affinity = self.interrupts.parse_affinity(ttyS0_irq)
        self.assertEqual(affinity, thunderbolt['affinity'])
        
        #通过find_by_user_regex函数获取interrupts数据
        regex = re.compile(r'^virtio0')
        result = self.interrupts.find_by_user_regex(regex)
        if not result:
            self.skip("未找到匹配 virtio0 的中断信息")
        thunderbolt_regex = self.interrupts[result[0]]
        self.assertIn('PCI-MSI', thunderbolt_regex['type'], msg=f"/proc/interrupts未解析成功,解析结果为{thunderbolt_regex}")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)