# -*- encoding: utf-8 -*-

"""
@File:      tc_python_linux_procfs_cmdline_002.py
@Time:      2024-04-02 14:52:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_python_linux_procfs_cmdline_002.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    PARAM_DIC = {"pkg_name": "python3-linux-procfs"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)


    def test(self):
        from procfs import cmdline
        self.kcmd = cmdline()
        self.assertIn("BOOT_IMAGE", self.kcmd.keys())
        cmd = "cat /proc/cmdline|grep 'BOOT_IMAGE'|awk '{{print $1}}'|awk -F= '{{print $2}}'"
        code, BOOT_IMAGE = self.cmd(cmd)
        self.assertEqual(BOOT_IMAGE, self.kcmd["BOOT_IMAGE"])

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
