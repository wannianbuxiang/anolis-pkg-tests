# -*- encoding: utf-8 -*-

"""
@File:      tc_python_markupsafe_func_001.py
@Time:      2024/03/19 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_python_markupsafe_func_001.yaml for details

    :avocado: tags=P2,noarch,local,fix,python
    """
    PARAM_DIC = {"pkg_name": "python3-markupsafe"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > markupsafe_test1.py  << EOF
#!/usr/bin/python
# -- coding: UTF-8 --

from markupsafe import escape

unsafe_string = '<script>alert("xss")</script>'
safe_string = escape(unsafe_string)

with open('markupsafe_result1.text', 'w') as f:
    f.write(safe_string)
EOF"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("python3 markupsafe_test1.py")
        code, markupsafe_result = self.cmd("cat markupsafe_result1.text")
        self.assertTrue("&lt;script&gt;alert(&#34;xss&#34;)&lt;/script&gt;" == markupsafe_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf markupsafe_test1.py markupsafe_result1.text")