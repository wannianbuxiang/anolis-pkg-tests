# -*- encoding: utf-8 -*-

"""
@File:      tc_yasm_yasm_fun001.py
@Time:      2024/11/05 15:30:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_yasm_yasm_fun001.yaml for details

    :avocado: tags=P1,noarch,local,yasm
    """
    PARAM_DIC = {"pkg_name": "yasm"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > hello.asm << EOF
section .data
    hello db 'Hello, World!',0

section .text
    global _start

_start:
    ; write "Hello, World!" to stdout
    mov eax, 4         ; syscall number for sys_write
    mov ebx, 1         ; file descriptor 1 (stdout)
    mov ecx, hello    ; address of the string
    mov edx, 13        ; length of the string
    int 0x80           ; make the syscall

    ; exit the program
    mov eax, 1         ; syscall number for sys_exit
    xor ebx, ebx       ; return code 0
    int 0x80           ; make the syscall
EOF"""
        self.cmd(cmdline)

    def test(self):        	
        self.cmd("yasm -f elf64 hello.asm -o hello.o")
        self.cmd("ld hello.o -o hello")
        code,result=self.cmd("./hello")
        self.assertIn("Hello, World!",result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf hello.asm hello.o hello ")