# -*- encoding: utf-8 -*-

"""
@File:      tc_java_java_fun001.py
@Time:      2023/3/06 
@Author:    geyaning
@Version:   1.0
@Contact:   geyaning@uniontech.com
@License:   Mulan PSL v2
@Modify:    geyaning
"""
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_java_java_fun001.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "java-1.8.0-openjdk java-1.8.0-openjdk-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("yum install -y java-1.8.0-openjdk java-1.8.0-openjdk-devel")
        exc_cmd = r'''cat > HelloWorld.java <<EOF
public class HelloWorld {
public static void main(String[] args) {
System.out.println("Hello, World");
}
}
EOF
'''
        self.cmd(exc_cmd)

    def test(self):
        self.cmd("javac HelloWorld.java")

        code, result = self.cmd("java HelloWorld")
        self.assertIn("Hello, World", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf HelloWorld*")
