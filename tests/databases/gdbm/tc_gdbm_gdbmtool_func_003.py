#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_gdbm_gdbmtool_func_003.py
@Time:      2024/04/15 15:00:00
@Author:    liuhaiyang
@Version:   1.0
@Contact:   lhy01509690@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liuhaiyang
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_gdbm_gdbmtool_func_003.yaml for details

    :avocado: tags=fix,P3,noarch,local
    """
    db_path = '/tmp/gdbm-test.db'
    PARAM_DIC = {"pkg_name": "gdbm"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd('for i in $(seq 1 10); do echo "store key$i value$i"; done | gdbmtool %s' % (self.db_path))
    
    def test(self):
        _, output = self.cmd("echo 'fetch key6' | gdbmtool %s" % (self.db_path))
        self.assertTrue(output == "value6", "fetch data fail")
        _, output = self.cmd("echo 'count' | gdbmtool %s | awk -F ' ' '{print$3}'" % (self.db_path))
        self.assertTrue(int(output) == 10, "count data fail")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf %s" % (self.db_path))
        
