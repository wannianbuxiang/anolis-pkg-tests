#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_gdbm_gdbmtool_func_001.py
@Time:      2024/04/15 15:00:00
@Author:    liuhaiyang
@Version:   1.0
@Contact:   lhy01509690@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liuhaiyang
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_gdbm_gdbmtool_func_001.yaml for details

    :avocado: tags=fix,P3,noarch,local
    """
    db_path = '/tmp/gdbm-test.db'
    PARAM_DIC = {"pkg_name": "gdbm"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        self.cmd("echo 'store test_key1 test_value1' | gdbmtool %s" % (self.db_path))
        _, output = self.cmd("echo 'list' | gdbmtool %s" % (self.db_path))
        self.assertTrue('test_key1' in output, "add data fail")
        self.cmd("echo 'store test_key1 update_value' | gdbmtool %s" % (self.db_path))
        _, output = self.cmd("echo 'fetch test_key1' | gdbmtool %s" % (self.db_path))
        self.assertTrue('update_value' in output, "update data fail")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf %s" % (self.db_path))
        
