#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_gdbm_gdbmtool_func_004.py
@Time:      2024/04/15 15:00:00
@Author:    liuhaiyang
@Version:   1.0
@Contact:   lhy01509690@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liuhaiyang
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_gdbm_gdbmtool_func_004.yaml for details

    :avocado: tags=fix,P3,noarch,local
    """
    db_path = '/tmp/gdbm-test.db'
    PARAM_DIC = {"pkg_name": "gdbm"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd('for i in $(seq 1 3); do echo "store key$i value$i"; done | gdbmtool %s' % (self.db_path))
        self.cmd("ls %s && rm -rf %s" % (str(self.db_path.split(".")[0]), str(self.db_path.split(".")[0])), ignore_status=True)
    
    def test(self):
        self.cmd("echo 'export %s' | gdbmtool %s" % (str(self.db_path.split(".")[0]), self.db_path))
        self.cmd("ls %s" % (str(self.db_path.split(".")[0])))
        _, output = self.cmd("cat %s | grep '#:file' | awk -F '=' '{print$2}'" % (str(self.db_path.split(".")[0])))
        self.assertTrue(output == self.db_path, "export data fail")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf %s" % (self.db_path))
        self.cmd("rm -rf %s" % (str(self.db_path.split(".")[0])))
        
