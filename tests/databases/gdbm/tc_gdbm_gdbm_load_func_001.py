#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_gdbm_gdbm_load_func_001.py
@Time:      2024/04/15 15:00:00
@Author:    liuhaiyang
@Version:   1.0
@Contact:   lhy01509690@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liuhaiyang
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_gdbm_gdbm_load_func_001.yaml for details

    :avocado: tags=fix,P3,noarch,local
    """
    db_path = '/tmp/gdbm-test.db'
    PARAM_DIC = {"pkg_name": "gdbm"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        config_file_dict = {
            "gdbm-test" : """cat >> %s << EOF
# GDBM dump file created by GDBM version 1.23. 04/02/2022 (built Mar 27 2023 00:00:00) on Mon Apr 15 16:49:03 2024
#:version=1.1
#:file=/tmp/gdbm-test.db
#:uid=0,user=root,gid=0,group=root,mode=644
#:format=standard
# End of header
#:len=4
a2V5Mg==
#:len=6
dmFsdWUy
#:len=4
a2V5Mw==
#:len=6
dmFsdWUz
#:len=4
a2V5MQ==
#:len=6
dmFsdWUx
#:count=3
# End of data""" 
        % (str(self.db_path.split(".")[0]))
        }
        self.cmd("ls %s && rm -rf %s" % (str(self.db_path.split(".")[0]), str(self.db_path.split(".")[0])), ignore_status=True)
        self.cmd(config_file_dict["gdbm-test"])

    def test(self):
        self.cmd("gdbm_load %s" % (str(self.db_path.split(".")[0])))
        self.cmd("ls %s" % (self.db_path))
        _, output = self.cmd("echo 'count' | gdbmtool %s | awk -F ' ' '{print$3}'" % (self.db_path))
        self.assertTrue(int(output) == 3, "count data fail")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf %s" % (self.db_path))
        self.cmd("rm -rf %s" % (str(self.db_path.split(".")[0])))
