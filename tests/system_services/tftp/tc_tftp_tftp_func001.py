# -*- encoding: utf-8 -*-

"""
@File:      tc_tftp_tftp_func001.py
@Time:      Wed Oct 25 2023 16:40:34
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_tftp_tftp_func001.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "tftp-server"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        self.cmd("systemctl start tftp")
        code,tftp_result=self.cmd("systemctl status tftp")
        self.assertIn("active (running)",tftp_result)
        self.cmd("systemctl restart tftp")
        code,tftp_result=self.cmd("systemctl status tftp")
        self.assertIn("active (running)",tftp_result)
        self.cmd("systemctl stop tftp")
        self.cmd("systemctl status tftp > tftp.log",ignore_status=True)
        code,tftp_result=self.cmd("cat tftp.log",ignore_status=True)
        self.assertIn("inactive (dead)",tftp_result)


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf tftp.log")
