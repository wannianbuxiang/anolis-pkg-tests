# -*- encoding: utf-8 -*-

"""
@File:      tc_acpid_acpid_fun001.py
@Time:      2024/05/16 14:21:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_acpid_acpid_fun001.yaml for details

    :avocado: tags=P1,noarch,local,acpid
    """
    PARAM_DIC = {"pkg_name": "acpid"}

    def setUp(self):
       super().setUp(self.PARAM_DIC)
              	  	 
    def test(self):
       self.cmd("systemctl start acpid")
       code, result=self.cmd("systemctl status acpid")
       self.assertIn("active (running)", result)
       self.cmd("systemctl restart acpid")
       code, result=self.cmd("systemctl status acpid")
       self.assertIn("active (running)", result)
       self.cmd("systemctl stop acpid")
       self.cmd("systemctl status acpid > acpid.log", ignore_status=True)
       code, result=self.cmd("cat acpid.log", ignore_status=True)
       self.assertIn("inactive (dead)", result)
       	 
    def tearDown(self):
       super().tearDown(self.PARAM_DIC)		
       self.cmd(" rm  -rf acpid.log")	
