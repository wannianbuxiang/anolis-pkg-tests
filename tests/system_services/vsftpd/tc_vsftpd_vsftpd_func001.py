# -*- encoding: utf-8 -*-

"""
@File:      tc_vsftpd_vsftpd_func001.py
@Time:      Wed Oct 25 2023 17:26:57
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_vsftpd_vsftpd_func001.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "vsftpd"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        self.cmd("systemctl start vsftpd")
        code,vsftpd_result=self.cmd("systemctl status vsftpd")
        self.assertIn("active (running)",vsftpd_result)
        self.cmd("systemctl restart vsftpd")
        code,vsftpd_result=self.cmd("systemctl status vsftpd")
        self.assertIn("active (running)",vsftpd_result)
        self.cmd("systemctl stop vsftpd")
        self.cmd("systemctl status vsftpd > vsftpd.log",ignore_status=True)
        code,vsftpd_result=self.cmd("cat vsftpd.log",ignore_status=True)
        self.assertIn("inactive (dead)",vsftpd_result)


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf vsftpd.log")
