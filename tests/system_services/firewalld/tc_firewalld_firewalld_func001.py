# -*- encoding: utf-8 -*-

"""
@File:      tc_firewalld_firewalld_fun001.py
@Time:      2023/1/12 14:30:20
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_firewalld_firewalld_fun001.yaml for details

    :avocado: tags=P1,noarch,local,firewalld
    """
    PARAM_DIC = {"pkg_name": "firewalld"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("yum install -y firewalld iproute")
        code,firewalld_status=self.cmd("systemctl status firewalld", ignore_status=True)
        global flag
        if 'active (running)' not in firewalld_status:
            flag=0
        else:
            flag=1

    def test(self):
        self.cmd("systemctl stop firewalld")
        code,firewalld_status=self.cmd("systemctl status firewalld", ignore_status=True)
        self.assertNotIn("active (running)",firewalld_status)
        self.assertNotIn("error",firewalld_status)
        self.assertNotIn("fail",firewalld_status)
        self.assertNotIn("warning",firewalld_status)
        self.cmd("systemctl start firewalld")
        code,firewalld_status=self.cmd("systemctl status firewalld", ignore_status=True)
        self.assertIn("active (running)",firewalld_status)
        self.assertNotIn("error",firewalld_status)
        self.assertNotIn("fail",firewalld_status)
        self.assertNotIn("warning",firewalld_status)
        self.cmd("systemctl restart firewalld")
        code,firewalld_status=self.cmd("systemctl status firewalld", ignore_status=True)
        self.assertIn("active (running)",firewalld_status)
        self.assertNotIn("error",firewalld_status)
        self.assertNotIn("fail",firewalld_status)
        self.assertNotIn("warning",firewalld_status)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        if flag==1:
            self.cmd("systemctl start firewalld")
        else:
            self.cmd("systemctl stop firewalld")
