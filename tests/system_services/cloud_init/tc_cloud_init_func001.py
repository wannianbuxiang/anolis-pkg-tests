# -*- encoding: utf-8 -*-

"""
@File:      tc_cloud_init_func001.py
@Time:      2024/2/23 10:30:20
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_cloud_init_func001.yaml for detail

    :avocado: tags=P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "cloud-init"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        code, cloud_result = self.cmd("cloud-init status")
        self.assertIn("status", cloud_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
