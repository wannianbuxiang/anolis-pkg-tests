# -*- encoding: utf-8 -*-

"""
@File:      tc_numad_numad_func_001.py
@Time:      2024/03/08 14:33:20
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_numad_numad_func_001.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "numad"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        self.cmd("service numad start")
        code, numad_result = self.cmd("service numad status")
        self.assertIn("active (running)", numad_result)
        self.cmd("service numad restart")
        code, numad_result = self.cmd("service numad status")
        self.assertIn("active (running)", numad_result)
        code, numad_result = self.cmd("numad -V")
        self.assertIn("numad version:", numad_result)
        code, numad_result = self.cmd("numad -h", ignore_status=True)
        self.assertIn("Usage:", numad_result)
        self.cmd("service numad stop")
        self.cmd("service numad status > numad.log", ignore_status=True)
        code, numad_result = self.cmd("cat numad.log", ignore_status=True)
        self.assertIn("inactive (dead)", numad_result)


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf numad.log")
