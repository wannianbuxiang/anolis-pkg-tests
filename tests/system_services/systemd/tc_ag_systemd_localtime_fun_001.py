#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_ag_systemd_localtime_fun_001.py
@Time:      2022/8/31 10:06:45
@Author:    liuyaqing
@Version:   1.0
@Contact:   lyq01395646@alibaba-inc.com
@License:   Mulan PSL v2
"""
from common.basetest import RemoteTest

class TimezoneTest(RemoteTest):
    """
    See   tc_ag_systemd_localtime_fun_001.yaml for details
    :avocado: tags=fix,P1,noarch,remote,vhd_img,baseos_container_default
    """

    def test_timezone(self):
        _,ret_o = self.cmd('cat /etc/os-release | grep ^ID=',container_flag=1)
        if "ubuntu" in ret_o:
            self.skip("Ubuntu container not support.")
        meaningless,output = self.cmd('ls /etc/localtime -l',container_flag=1)
        if self.container_id is None:
            self.assertEqual(output.split('/')[-1],'Shanghai',"timezone is wrong!")
        else:
            if "alinux" in ret_o:
                self.assertEqual(output.split('/')[-1],'UTC',"timezone is wrong!")
            elif "anolis" in ret_o:
                self.assertEqual(output.split('/')[-1],'Shanghai',"timezone is wrong!")
            else
                self.skip("current container not support.")
                
    def tearDown(self):
        super().tearDown()