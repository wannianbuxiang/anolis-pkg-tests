#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_systemd_enable_service.py
@Time:      2024年10月18日14:44:12
@Author:    liangjian
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""

from common.basetest import LocalTest

class Test(LocalTest):   
    """
    See tc_systemd_enable_service.yaml for details

    :avocado: tags=P1,noarch,loacl
    """
    PARAM_DIC = {"pkg_name": "systemd"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat > /usr/lib/systemd/system/TestA.service <<EOF
[Unit]
Description=Test Service A
[Service]
ExecStart=/usr/bin/sleep 100
[Install]
WantedBy=multi-user.target
EOF'''
        self.cmd(cmdline)
        self.cmd("systemctl daemon-reload")

    def test(self):
        #服务在系统启动时自动启动，可以启用服务
        self.cmd('systemctl enable TestA.service')
        ret_c, ret_o = self.cmd('systemctl is-enabled TestA.service')
        self.assertTrue( "enabled" in ret_o, 'enable startup failed')
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("systemctl disable TestA.service")
        self.cmd("rm -rf /usr/lib/systemd/system/TestA.service")
