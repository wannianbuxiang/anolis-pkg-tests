#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_systemd_systemd-analyze.py
@Time:      2024年10月18日14:44:12
@Author:    liangjian
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""

from common.basetest import LocalTest

class Test(LocalTest):   
    """
    See tc_systemd_systemd-analyze.yaml for details

    :avocado: tags=P1,noarch,loacl
    """
    PARAM_DIC = {"pkg_name": "systemd"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        #Systemd 内建命令
        self.cmd('systemd-analyze time')
        ret_c, ret_o = self.cmd('systemd-analyze blame')
  
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)