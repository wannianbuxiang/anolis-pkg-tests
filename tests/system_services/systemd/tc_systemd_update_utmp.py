#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_systemd_update_utmp.py
@Time:      2022/06/20 11:12:50
@Author:    liangjian
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_systemd_update_utmp.yaml for details

    :avocado: tags=P1,noarch,loacl
    """
    PARAM_DIC = {"pkg_name": "systemd"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        #systemd-update-utmp-runlevel.service通常是用于更新UTMP（User Tracking Multi-Process）记录的服务，它负责记录当前系统的运行级别等信息
        #确认systemd-update-utmp-runlevel.service是否被配置为在救援模式下启动
        ret_c, ret_o = self.cmd('ls -l /usr/lib/systemd/system/rescue.target.wants/ | grep "systemd-update-utmp-runlevel.service"')
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
