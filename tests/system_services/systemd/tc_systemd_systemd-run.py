#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_systemd_systemd-run.py
@Time:      2024年10月18日14:44:12
@Author:    liangjian
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""

from common.basetest import LocalTest
import subprocess
class Test(LocalTest):   
    """
    See tc_systemd_systemd-run.yaml for details

    :avocado: tags=P1,noarch,loacl
    """
    PARAM_DIC = {"pkg_name": "systemd"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        #systemd-run将任意命令作为服务启动，并将其注册到systemd中，使其能够在后台运行
        command = "nohup systemd-run --unit=MyBackgroundService --scope sleep 3600 &"
        subprocess.Popen(f"{command}", shell=True)
        self.cmd('systemctl status MyBackgroundService.scope |grep running')

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('systemctl stop MyBackgroundService.scope')
