#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_systemd_dependency_service.py
@Time:      2024年10月18日14:44:12
@Author:    liangjian
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""

from common.basetest import LocalTest

class Test(LocalTest):   
    """
    See tc_systemd_dependency_service.yaml for details

    :avocado: tags=P1,noarch,loacl
    """
    PARAM_DIC = {"pkg_name": "systemd"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat > /usr/lib/systemd/system/TestA.service <<EOF
[Unit]
Description=Test Service A
Requires=TestB.service
[Service]
Environment="MY_VARIABLE=1"
ExecStart=/bin/sh -c 'echo "MY_VARIABLE=$MY_VARIABLE"; /usr/bin/sleep 100'
EOF'''
        self.cmd(cmdline)
        cmdline = '''cat > /usr/lib/systemd/system/TestB.service <<EOF
[Unit]
Description=Test Service B
[Service]
ExecStart=/usr/bin/sleep 100
EOF'''
        self.cmd(cmdline)
        self.cmd("systemctl daemon-reload")

    def test(self):
        #测试服务依赖和变量设置
        self.cmd('systemctl start TestA.service')
        self.cmd('systemctl status TestA.service | grep running')
        self.cmd('systemctl status TestB.service | grep running')
        self.cmd('journalctl -u TestA.service | grep "MY_VARIABLE=1"')
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("systemctl stop TestA")
        self.cmd("systemctl stop TestB")
        self.cmd("rm -rf /usr/lib/systemd/system/TestA.service /usr/lib/systemd/system/TestB.service ")
