#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_systemd_sysinfo_failed_func_001.py
@Time:      2022/8/31 10:06:45
@Author:    yangshunfeng.ysf
@Version:   1.0
@Modify:    liuyaqing
@License:   Mulan PSL v2
"""
from common.basetest import RemoteTest

class ServiceTest(RemoteTest):
    """
    See   tc_systemd_sysinfo_failed_func_001.yaml for details
    :avocado: tags=fix,P1,noarch,remote,vhd_img
    """
    
    """
     AuthenticAMD models, CPU family ≥ 16, are not supported
     https://bugzilla.openanolis.cn/show_bug.cgi?id=1677
    """
    def setUp(self):
         super().setUp()

    def test_failed_service(self):
        if self.container_id:
            ret_c,ret_o = self.cmd('cat /etc/os-release',container_flag=1)
            if any(container_os in ret_o for container_os in ["alinux", "Ubuntu"]):
                self.skip("This container not support.")
            ret_c,fail_service = self.cmd('systemctl --failed | grep failed',ignore_status=True,container_flag=1)
            failed_services = [line.split(' ')[1]
                for line in fail_service.split('\n') if line]
            self.assertEqual(len(failed_services), 0, 'The failed system services: {}'.format(' '.join(failed_services)))
        else:
            ret_c, fail_service_num = self.cmd('systemctl --failed | grep failed | wc -l')
            ret_c, fail_service = self.cmd('systemctl --failed | grep failed',ignore_status=True)
            if int(fail_service_num) == 1 and fail_service.split(' ')[1] == 'mcelog.service':
                ret_c, family_num = self.cmd('lscpu | grep "CPU family:" | awk "{print \$3}"')
                ret_c, fail_log = self.cmd("systemctl status mcelog.service | grep 'AMD'",ignore_status=True)
                self.assertTrue(int(family_num) >= 16,'The cpu family num is not as expected')
                self.assertTrue('mcelog does not support this processor' in fail_log,'Failure reason of mcelog service')
                self.assertTrue(family_num in fail_log,"The mcelog.service failure is not a CPU problem")
            else:
                failed_services = [line.split(' ')[1]
                        for line in fail_service.split('\n') if line]
                self.assertEqual(len(failed_services), 0, 'The failed system services: {}'.format(' '.join(failed_services)))
    def tearDown(self):
         super().tearDown()