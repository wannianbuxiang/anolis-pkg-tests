#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_systemd_service_on_fail_restart.py
@Time:      2024年10月18日14:44:12
@Author:    liangjian
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""

from common.basetest import LocalTest
import time

class Test(LocalTest):
    """
    See tc_systemd_service_on_fail_restart.yaml for details

    :avocado: tags=P1,noarch,loacl
    """
    PARAM_DIC = {"pkg_name": "systemd"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat > /usr/lib/systemd/system/TestA.service <<EOF
[Unit]
Description=Test Service A
[Service]
Restart=on-failure
RestartSec=5s
ExecStart=/usr/bin/sleep 100
EOF'''
        self.cmd(cmdline)
        self.cmd("systemctl daemon-reload")

    def test(self):
        #服务在遇到错误时能否自动恢复,设置策略为当服务失败时重启，服务将在失败后5秒钟重启
        self.cmd('systemctl start TestA.service')
        ret_c, ret_o = self.cmd("systemctl show -p MainPID TestA.service | awk -F '=' '{print $2}'")
        self.cmd('kill -9 %s ' % ret_o)
        self.cmd('systemctl status TestA.service | grep auto-restart')
        time.sleep(6)
        self.cmd('systemctl status TestA.service | grep running')
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("systemctl stop TestA")
        self.cmd("rm -rf /usr/lib/systemd/system/TestA.service")
