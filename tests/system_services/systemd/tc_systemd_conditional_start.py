#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_systemd_conditional_start.py
@Time:      2024年10月18日14:44:12
@Author:    liangjian
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""

from common.basetest import LocalTest

class Test(LocalTest):   
    """
    See tc_systemd_conditional_start.yaml for details

    :avocado: tags=P1,noarch,loacl
    """
    PARAM_DIC = {"pkg_name": "systemd"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat > /usr/lib/systemd/system/TestA.service <<EOF
[Unit]
Description=Test Service A
ConditionFileIsExecutable=/root/script.sh
[Service]
ExecStart=/usr/bin/sleep 100
[Install]
WantedBy=multi-user.target
EOF'''
        self.cmd(cmdline)
        self.cmd("systemctl daemon-reload")

    def test(self):
        #条件启动
        self.cmd('systemctl start TestA.service')
        ret_c, ret_o = self.cmd('systemctl status TestA.service |grep running',ignore_status=True)
        self.assertTrue( ret_c != '0', 'Expected return value to be non-zero,the file does not exist ')
        cmdline = '''cat > /root/script.sh <<EOF
echo hello
EOF'''
        self.cmd(cmdline) 
        self.cmd('systemctl start TestA.service')
        ret_c, ret_o = self.cmd('systemctl status TestA.service |grep running',ignore_status=True)
        self.assertTrue( ret_c != '0', 'Expected return value to be non-zero; the file is not executable')

        self.cmd("chmod +x /root/script.sh")
        self.cmd('systemctl start TestA.service')
        ret_c, ret_o = self.cmd('systemctl status TestA.service |grep running')
    
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("systemctl stop TestA.service")
        self.cmd("rm -rf /usr/lib/systemd/system/TestA.service /root/script.sh")
