# -*- encoding: utf-8 -*-

"""
@File:      tc_at_at_fun001.py
@Time:      2024/05/28 16:51:20
@Author:    douzhichong
@Version:   1.0
@Contact:   douzhichong@inspur.com
@License:   Mulan PSL v2
@Modify:    douzhichong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_at_at_fun001.yaml for details

    :avocado: tags=P1,noarch,local,at
    """
    PARAM_DIC = {"pkg_name": "at"}

    def setUp(self):
       super().setUp(self.PARAM_DIC)
              	  	 
    def test(self):
       self.cmd("systemctl start atd")
       code, result=self.cmd("systemctl status atd")
       self.assertIn("active (running)", result)
       self.cmd("systemctl restart atd")
       code, result=self.cmd("systemctl status atd")
       self.assertIn("active (running)", result)
       self.cmd("systemctl stop atd")
       self.cmd("systemctl status atd > atd.log", ignore_status=True)
       code, result=self.cmd("cat atd.log", ignore_status=True)
       self.assertIn("inactive (dead)", result)
       	 
    def tearDown(self):
       super().tearDown(self.PARAM_DIC)		
       self.cmd(" rm  -rf atd.log")	
