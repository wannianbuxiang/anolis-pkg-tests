#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

"""
@File:      tc_ipset_ipset_fun_001.py
@Time:      2024/10/08 18:07:20
@Author:    wangge
@Version:   1.0
@Contact:   YS.wangge@h3c.com
@License:   Mulan PSL v2
@Modify:    wangge
"""
import subprocess
import random
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ipset_ipset_fun_001.yaml for details

    :avocado: tags=P0,noarch,local
    """
    PARAM_DIC = {"pkg_name": "ipset"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        

    def test(self):
        self.random_number = random.randint(1000, 9999)

        # Create IP set
        result = subprocess.run(f'ipset create aa{self.random_number} hash:net maxelem 1000000', shell=True)
        self.assertEqual(result.returncode, 0, "Failed to create set named aa")
        
        result = subprocess.run(f'ipset add aa{self.random_number} 10.7.10.10', shell=True)
        self.assertEqual(result.returncode, 0, f"Failed to add entry to set aa{self.random_number}")
        
        result = subprocess.run(f'ipset test aa{self.random_number} 10.7.10.10', shell=True)
        self.assertEqual(result.returncode, 0, f"Test entry aa{self.random_number} failed")

        result = subprocess.run(f'ipset create bb{self.random_number} hash:net maxelem 1000000', shell=True)
        self.assertEqual(result.returncode, 0, "Failed to create set named bb")
        
        result = subprocess.run(f'ipset add bb{self.random_number} 10.7.10.11', shell=True)
        self.assertEqual(result.returncode, 0, f"Failed to add entry to set bb{self.random_number}")
        
        result = subprocess.run(f'ipset list bb{self.random_number} | grep "10.7.10.11"', shell=True)
        self.assertEqual(result.returncode, 0, f"Entry bb{self.random_number} not found in list")
        
        result = subprocess.run(f'ipset rename aa{self.random_number} cc{self.random_number}', shell=True)
        self.assertEqual(result.returncode, 0, "Failed to rename set")
        
        result = subprocess.run(f'ipset list cc{self.random_number} | grep "10.7.10.10"', shell=True)
        self.assertEqual(result.returncode, 0, f"Entry cc{self.random_number} not found in list")
        
        result = subprocess.run(f'ipset swap cc{self.random_number} bb{self.random_number}', shell=True)
        self.assertEqual(result.returncode, 0, "Failed to swap sets")
        
        result = subprocess.run(f'ipset list cc{self.random_number} | grep "10.7.10.11"', shell=True)
        self.assertEqual(result.returncode, 0, f"Entry cc{self.random_number} not found after swap")
        
        result = subprocess.run(f'ipset list bb{self.random_number} | grep "10.7.10.10"', shell=True)
        self.assertEqual(result.returncode, 0, f"Entry bb{self.random_number} not found after swap")
        
        result = subprocess.run('ipset flush', shell=True)
        self.assertEqual(result.returncode, 0, "Failed to flush sets")
        
        result = subprocess.run(f'ipset list cc{self.random_number} | grep "10.7.10.11"', shell=True)
        self.assertEqual(result.returncode, 1, f"Entry cc{self.random_number} remains after flush")
        
        result = subprocess.run(f'ipset list bb{self.random_number} | grep "10.7.10.10"', shell=True)
        self.assertEqual(result.returncode, 1, f"Entry bb{self.random_number} remains after flush")


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        result = subprocess.run(f'ipset destroy bb{self.random_number}', shell=True)
        if result.returncode != 0:
            print(f"Failed to delete bb{self.random_number}")

        result = subprocess.run(f'ipset destroy cc{self.random_number}', shell=True)
        if result.returncode != 0:
            print(f"Failed to delete cc{self.random_number}")