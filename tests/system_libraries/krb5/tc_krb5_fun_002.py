# -*- encoding: utf-8 -*-

"""
@File:      tc_krb5_fun_002.py
@Time:      2024-03-12 14:53:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from system_libraries.krb5.krb5 import Krb5Test
from common.service import ServiceManager
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_krb5_fun_002.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "krb5-server krb5-workstation krb5-libs"}
    def setUp(self):
        self.servicename = 'krb5kdc.service'
        super().setUp(self.PARAM_DIC)
        Krb5Test.create_krb5conf(self)
        Krb5Test.create_kdc_conf(self)
        Krb5Test.create_kadm5_acl(self)
        Krb5Test.init_database(Krb5Test.realms, Krb5Test.database_password)

    def test(self):
        ServiceManager.service_test(self, self.servicename)
    def tearDown(self):
        Krb5Test.del_env(self)
        super().tearDown(self.PARAM_DIC)
        