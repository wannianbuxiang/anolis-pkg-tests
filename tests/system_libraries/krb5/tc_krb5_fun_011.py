# -*- encoding: utf-8 -*-

"""
@File:      tc_krb5_fun_011.py
@Time:      2024-03-12 14:53:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
import datetime
import time
from system_libraries.krb5.krb5 import Krb5Test
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_krb5_fun_011.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "krb5-server krb5-workstation krb5-libs"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline='''cat > /etc/krb5.conf <<EOF
[logging]
 default = FILE:/var/log/krb5libs.log
 kdc = FILE:/var/log/krb5kdc.log
 admin_server = FILE:/var/log/kadmind.log

[libdefaults]
 default_realm = MYDOMAIN.LOCAL
 dns_lookup_realm = false
 dns_lookup_kdc = false
 ticket_lifetime = 10s
 renew_lifetime = 10s
 forwardable = true

[realms]
 MYDOMAIN.LOCAL = {
  kdc = localhost
  admin_server = localhost
 }

[domain_realm]
 .mydomain.local = MYDOMAIN.LOCAL
 mydomain.local = MYDOMAIN.LOCAL
EOF
'''
        self.cmd(cmdline)
        Krb5Test.create_kdc_conf(self)
        Krb5Test.create_kadm5_acl(self)
        Krb5Test.init_database(Krb5Test.realms, Krb5Test.database_password)
        Krb5Test.start_krb5kdcservice(self)
        ret_c, _ = self.cmd("cat /etc/os-release | grep -i an23", ignore_status=True)
        if ret_c == 0:
            self.time_format = "%m/%d/%y %H:%M:%S"
        else:
            self.time_format = "%m/%d/%Y %H:%M:%S"
 
    def test(self):
        #create user
        Krb5Test.create_user(self, Krb5Test.user_name, Krb5Test.user_password)
        #Obtain TGT ticket
        Krb5Test.init_tgt(self, Krb5Test.user_name, Krb5Test.user_password, Krb5Test.realms)
        code, result = self.cmd(f'kvno {Krb5Test.user_name}')
        #get expire time
        code, expire_str = self.cmd("klist -A|grep 'krbtgt'|awk '{print $3,$4}'")
        expire_time = datetime.datetime.strptime(expire_str, self.time_format)
        #calculate the remaining validity period of the ticket
        remaining_time = (expire_time - datetime.datetime.now()).total_seconds()
        # wait for it to expire
        if 0 < remaining_time < 10:
            print(f"Ticket expires at: {expire_time.strftime('%Y-%m-%d %H:%M:%S')},Remaining time: {remaining_time:.0f} seconds")
            print("The ticket will expire in less than 10 seconds. Waiting for expiration...")
            time.sleep(remaining_time + 5) 
            print("The ticket cache should now be expired.")
        else:
            print("The ticket is already expired.")
        code, result = self.cmd(f'kvno {Krb5Test.user_name}', ignore_status=True)
        self.assertEqual(f'kvno: Ticket expired while getting credentials for {Krb5Test.user_name}@{Krb5Test.realms}', result)
        self.log.info("Ticket expired and failed to execute kvno command")
        code, result = self.cmd('kinit -R', ignore_status=True)
        self.assertEqual('kinit: Ticket expired while renewing credentials', result)
        self.log.info("Ticket expired and failed to execute 'kinit -R' command")
        #generate a new valid TGT
        Krb5Test.init_tgt(self, Krb5Test.user_name, Krb5Test.user_password, Krb5Test.realms)
        #get a new expire_time
        code, new_expire_str = self.cmd("klist -A|grep 'krbtgt'|awk '{print $3,$4}'")
        new_expire_time = datetime.datetime.strptime(new_expire_str, self.time_format)
        self.assertGreater(new_expire_time, expire_time)

    def tearDown(self):
        Krb5Test.destory_ticket(self, Krb5Test.user_name)
        Krb5Test.del_user(self, Krb5Test.user_name)
        Krb5Test.del_env(self)
        super().tearDown(self.PARAM_DIC)