# -*- encoding: utf-8 -*-

"""
@File:      tc_krb5_fun_012.py
@Time:      2024-03-12 14:53:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
import pexpect
from system_libraries.krb5.krb5 import Krb5Test
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_krb5_fun_012.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "krb5-server krb5-workstation krb5-libs"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.incorrect_password = 'Wrong@123'
        Krb5Test.create_krb5conf(self)
        Krb5Test.create_kdc_conf(self)
        Krb5Test.create_kadm5_acl(self)
        Krb5Test.init_database(Krb5Test.realms, Krb5Test.database_password)
        Krb5Test.start_krb5kdcservice(self)
 

 
    def test(self):
        #create user
        Krb5Test.create_user(self, Krb5Test.user_name, Krb5Test.user_password)
        #Enter incorrect password
        child = pexpect.spawn(f'kinit {Krb5Test.user_name}')
        child.expect(f'Password for {Krb5Test.user_name}@{Krb5Test.realms}')
        child.sendline(self.incorrect_password)
        child.wait()
        child.expect('kinit: Password incorrect while getting initial credentials')

    def tearDown(self):
        Krb5Test.del_user(self, Krb5Test.user_name)
        Krb5Test.del_env(self)
        super().tearDown(self.PARAM_DIC)
        