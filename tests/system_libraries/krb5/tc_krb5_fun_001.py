# -*- encoding: utf-8 -*-

"""
@File:      tc_krb5_fun_001.py
@Time:      2024-03-11 17:53:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_krb5_fun_001.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "krb5-server krb5-workstation krb5-libs"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
 
    def test(self):
        code, klist_ver = self.cmd("klist -V|awk '{print $4}'")
        code, Rpm_Ver = self.cmd("rpm -q krb5-server|awk -F- '{print $3}'")
        if klist_ver is not None and Rpm_Ver is not None:
            self.assertEqual(klist_ver, Rpm_Ver)
        else:
            print("其中一个命令执行失败或未获取到版本信息,请检查。")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
