import logging
import pexpect

class Krb5Test():
    database_password = 'MYDOMAIN@123'
    user_name = 'user'
    user_password = 'user@123'
    realms = 'MYDOMAIN.LOCAL'
    Keytab_path = '/var/kerberos/krb5kdc/kadm5.keytab'
    
    @staticmethod
    def create_krb5conf(self):
        cmdline='''cat > /etc/krb5.conf <<EOF
[logging]
 default = FILE:/var/log/krb5libs.log
 kdc = FILE:/var/log/krb5kdc.log
 admin_server = FILE:/var/log/kadmind.log

[libdefaults]
 default_realm = MYDOMAIN.LOCAL
 dns_lookup_realm = false
 dns_lookup_kdc = false
 ticket_lifetime = 24h
 renew_lifetime = 7d
 forwardable = true

[realms]
 MYDOMAIN.LOCAL = {
  kdc = localhost
  admin_server = localhost
 }

[domain_realm]
 .mydomain.local = MYDOMAIN.LOCAL
 mydomain.local = MYDOMAIN.LOCAL
EOF
'''
        self.cmd(cmdline)
        
        
        
        
    @staticmethod
    def create_kdc_conf(self):
        cmdline='''cat > /var/kerberos/krb5kdc/kdc.conf <<EOF
[kdcdefaults]
 kdc_ports = 88
 kdc_tcp_ports = 88

[realms]
 MYDOMAIN.LOCAL = {
  # master_key_type在现代Kerberos安装中通常是aes256-cts
  master_key_type = aes256-cts
  acl_file = /var/kerberos/krb5kdc/kadm5.acl
  dict_file = /usr/share/dict/words
  admin_keytab = /var/kerberos/krb5kdc/kadm5.keytab
  supported_enctypes = aes256-cts:normal aes128-cts:normal
  max_renewable_life = 7d
  default_principal_flags = +preauth 
}
EOF
'''
        self.cmd(cmdline)
        
        
        
    @staticmethod
    def create_kadm5_acl(self):
        cmdline='''cat > /var/kerberos/krb5kdc/kadm5.acl <<EOF
*/admin@MYDOMAIN.LOCAL  *
EOF
'''
        self.cmd(cmdline)
    
    @classmethod
    def init_database(cls, realms, password):
        child = pexpect.spawn(f'kdb5_util create -s -r {realms}')
        child.expect('Enter KDC database master key:')
        child.sendline(password)
        child.expect('Re-enter KDC database master key to verify:')
        child.sendline(password)
        child.wait()
        if child.exitstatus != 0:
            logging.info("初始化 kerberos 数据库失败")
            
    @classmethod
    def create_user(cls, self, username, password):
        self.cmd(f'kadmin.local addprinc -pw {password} {username}')
            
    @classmethod
    def create_user_policy(cls, self, username, password, policy):
        self.cmd(f'kadmin.local addprinc -policy {policy} -pw {password} {username}')
    
    @classmethod
    def change_pwd(cls, self, username, password):
        self.cmd(f"kadmin.local -q 'cpw -pw {password} {username}'")
    
    @classmethod
    def init_tgt(cls, self, username, password, realms):
        child = pexpect.spawn(f'kinit {username}')
        child.expect(f'Password for {username}@{realms}')
        child.sendline(password)
        child.wait()
        if child.exitstatus != 0:
            self.log.info("获取 TGT 失败")
            
    @classmethod
    def create_keytab(cls, self, Keytab_path, username):
        self.cmd(f"kadmin.local ktadd -k {Keytab_path} {username}")
        
    @classmethod
    def list_keytab(cls, self, Keytab_path):
        code, result = self.cmd(f"klist -kt {Keytab_path}")
        return result
    
    @classmethod
    def kinit_keytab(cls, self, Keytab_path, username, realms):
        self.cmd(f"kinit -kt {Keytab_path} {username}@{realms}")
         
    
    @classmethod
    def del_keytab(cls, self, Keytab_path):
        self.cmd(f"rm -rf {Keytab_path}")
    
    @staticmethod
    def start_krb5kdcservice(self):
        code, result = self.cmd("systemctl status krb5kdc.service|grep -i active|awk -F: '{print $2}'")
        if 'dead' in result:
            self.cmd('sudo systemctl restart krb5kdc.service')
            
    @classmethod
    def del_user(cls, self, username):
        self.cmd(f"echo delprinc -force {username} | kadmin.local")
        
    @classmethod
    def destory_ticket(cls, self, username):
        self.cmd(f'kdestroy -p {username}')
    

    @staticmethod
    def del_env(self):
        self.cmd('systemctl stop krb5kdc.service')
        self.cmd('rm -f /etc/krb5.conf /var/kerberos/krb5kdc/kdc.conf')
        self.cmd("echo '*/admin@EXAMPLE.COM     *' > /var/kerberos/krb5kdc/kadm5.acl")
        self.cmd('rm -f /var/kerberos/krb5kdc/principal*')
    
