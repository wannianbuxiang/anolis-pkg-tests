# -*- encoding: utf-8 -*-

"""
@File:      tc_krb5_fun_004.py
@Time:      2024-03-12 14:53:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from system_libraries.krb5.krb5 import Krb5Test
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_krb5_fun_004.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    def execute_and_check_kvno(self, command):
        """
        执行 kvno 命令并检查结果
        """
        try:
            code, result = self.cmd(command)
            if "kvno = 1" in result:
                self.log.info("获取服务票据成功")
            else:
                self.log.error("服务票据获取失败，未找到票据")
        except Exception as e:
            self.log.error(f"执行 kvno 时出错: {e}")
    
    PARAM_DIC = {"pkg_name": "krb5-server krb5-workstation krb5-libs"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        Krb5Test.create_krb5conf(self)
        Krb5Test.create_kdc_conf(self)
        Krb5Test.create_kadm5_acl(self)
        Krb5Test.init_database(Krb5Test.realms, Krb5Test.database_password)
        Krb5Test.start_krb5kdcservice(self)

 
    def test(self):
        #create user
        Krb5Test.create_user(self, Krb5Test.user_name, Krb5Test.user_password)
        #Obtain TGT ticket
        Krb5Test.init_tgt(self, Krb5Test.user_name, Krb5Test.user_password, Krb5Test.realms)
        #Check whether the command is executed successfully
        self.execute_and_check_kvno(f'kvno {Krb5Test.user_name}')
        self.execute_and_check_kvno(f'kvno krbtgt/{Krb5Test.realms}@{Krb5Test.realms}')

    def tearDown(self):
        Krb5Test.destory_ticket(self, Krb5Test.user_name)
        Krb5Test.del_user(self, Krb5Test.user_name)
        Krb5Test.del_env(self)
        super().tearDown(self.PARAM_DIC)
        