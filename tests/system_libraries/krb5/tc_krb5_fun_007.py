# -*- encoding: utf-8 -*-

"""
@File:      tc_krb5_fun_007.py
@Time:      2024-03-13 10:38:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
import pexpect
from system_libraries.krb5.krb5 import Krb5Test
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_krb5_fun_007.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "krb5-server krb5-workstation krb5-libs"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.user_name = 'root/admin'
        self.policy = 'admin'
        Krb5Test.create_krb5conf(self)
        Krb5Test.create_kdc_conf(self)
        Krb5Test.create_kadm5_acl(self)
        Krb5Test.init_database(Krb5Test.realms, Krb5Test.database_password)
        Krb5Test.start_krb5kdcservice(self)
        #create user with admin policy
        Krb5Test.create_user_policy(self, self.user_name, Krb5Test.user_password, self.policy)
 
    def test(self):
        #obtain TGT
        Krb5Test.init_tgt(self, self.user_name, Krb5Test.user_password, Krb5Test.realms)     
        #loggin kadmin.local
        child = pexpect.spawn('kadmin.local')
        child.expect(f"Authenticating as principal {self.user_name}@{Krb5Test.realms} with password.")
        child.sendline('q')        
                   
    def tearDown(self):
        Krb5Test.del_user(self, self.user_name)
        Krb5Test.del_env(self)
        super().tearDown(self.PARAM_DIC)
        