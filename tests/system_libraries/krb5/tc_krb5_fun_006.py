# -*- encoding: utf-8 -*-

"""
@File:      tc_krb5_fun_006.py
@Time:      2024-03-13 10:38:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from system_libraries.krb5.krb5 import Krb5Test
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_krb5_fun_006.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "krb5-server krb5-workstation krb5-libs"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        Krb5Test.create_krb5conf(self)
        Krb5Test.create_kdc_conf(self)
        Krb5Test.create_kadm5_acl(self)
        Krb5Test.init_database(Krb5Test.realms, Krb5Test.database_password)
        Krb5Test.start_krb5kdcservice(self)
        self.user_list = ['root/admin', 'test', 'user']
        self.policy_list = ['admin', 'user']
 
    def test(self):
        #create user with admin policy
        Krb5Test.create_user_policy(self, self.user_list[0], Krb5Test.user_password, self.policy_list[0])
        #create user with user policy
        Krb5Test.create_user_policy(self, self.user_list[1], Krb5Test.user_password, self.policy_list[1])
        #create user without policy
        Krb5Test.create_user(self, self.user_list[2], 'test@123')
        
                   
    def tearDown(self):
        for item in self.user_list:
            Krb5Test.del_user(self, item)
        Krb5Test.del_env(self)
        super().tearDown(self.PARAM_DIC)
        