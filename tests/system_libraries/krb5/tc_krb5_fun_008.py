# -*- encoding: utf-8 -*-

"""
@File:      tc_krb5_fun_008.py
@Time:      2024-03-13 10:38:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
import time
from datetime import datetime
from system_libraries.krb5.krb5 import Krb5Test
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_krb5_fun_008.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "krb5-server krb5-workstation krb5-libs"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        Krb5Test.create_krb5conf(self)
        Krb5Test.create_kdc_conf(self)
        Krb5Test.create_kadm5_acl(self)
        Krb5Test.init_database(Krb5Test.realms, Krb5Test.database_password)
        Krb5Test.start_krb5kdcservice(self)
        ret_c, _ = self.cmd("cat /etc/os-release | grep -i an23", ignore_status=True)
        if ret_c == 0:
            self.time_format = "%m/%d/%y %H:%M:%S"
        else:
            self.time_format = "%m/%d/%Y %H:%M:%S"

 
    def test(self):
        #create user
        Krb5Test.create_user(self, Krb5Test.user_name, Krb5Test.user_password)
        #Obtain TGT ticket
        Krb5Test.init_tgt(self, Krb5Test.user_name, Krb5Test.user_password, Krb5Test.realms)
        code, old_start_time = self.cmd("klist|grep 'krbtgt'|awk '{print $1,$2}'")
        time.sleep(2)
        #renew Valid start time
        self.cmd(f"kinit -R {Krb5Test.user_name}@{Krb5Test.realms}")
        code, new_start_time = self.cmd("klist|grep 'krbtgt'|awk '{print $1,$2}'")
        start_time = datetime.strptime(old_start_time, self.time_format)
        renew_time = datetime.strptime(new_start_time, self.time_format)
        self.assertGreater(renew_time, start_time)
      
                   
    def tearDown(self):
        Krb5Test.destory_ticket(self, Krb5Test.user_name)
        Krb5Test.del_user(self, Krb5Test.user_name)
        Krb5Test.del_env(self)
        super().tearDown(self.PARAM_DIC)
        