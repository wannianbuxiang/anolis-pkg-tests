# -*- encoding: utf-8 -*-

"""
@File:      tc_libcomps_libcomps_fun001.py
@Time:      2024/4/2 14:20:05
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

import os
import shutil
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libcomps_libcomps_fun001.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libcomps libcomps-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

        cmdline = '''cat >check_brtree.c<<"EOF"
#include <stdio.h>
#include <stdlib.h>
#include "libcomps/comps_bradix.h"

struct _key {
    char *key;
    unsigned int keylen;
};

struct _teststruct {
    char *testdata;
};

struct _teststruct* make_test_struct(char *testdata) {
    struct _teststruct *ret;
    ret = malloc(sizeof(struct _teststruct));
    ret->testdata = malloc(sizeof(char)*(strlen(testdata)+1));
    memcpy(ret->testdata, testdata, sizeof(char)*(strlen(testdata)+1));
    return ret;
}

void destroy_test_struct(void *ts) {
    if (!ts) return;
    free(((struct _teststruct*)ts)->testdata);
    free(ts);
}


void print_key(struct _key *key){
    unsigned char x;
    for (x = 0; x<key->keylen-1; x++) {
        printf("%hhu|",key->key[x]);
        if (x>10) break;
    }
    printf("%hhu\\n",key->key[x]);
}

void key_destroy(void *key) {
    free(((struct _key*)key)->key);
    free(key);
}

void* key_clone(void *key, unsigned int len) {
    struct _key * ret;
    //unsigned int x;

    ret = malloc(sizeof(struct _key));
    ret->key = malloc(sizeof(char)*len);
    ret->key = memcpy(ret->key, ((struct _key*)key)->key, sizeof(char)*len);
    ret->keylen = len;
    return ret;
}

void* make_key(void *x) {
    struct _key *ret;
    unsigned char i;
    ret = malloc(sizeof(struct _key));
    ret->key = malloc(sizeof(char)*sizeof(x));
    for (i = 0; i<sizeof(x); i++) {
        ret->key[i] = ((char*)&x)[sizeof(x)-i-1];
    }
    ret->keylen = sizeof(x);
    return ret;
}

unsigned int key_cmp(void *key1, void *key2,
                     unsigned int offset1, unsigned int offset2,
                     unsigned int len, char *ended) {
    unsigned int x;//, min_offset;
    char locended = 0;
    for (x = 0; x < len; x++) {
        if (((struct _key*)key1)->keylen == x+offset1) locended += 1;
        if (((struct _key*)key2)->keylen == x+offset2) locended += 2;
        if (locended != 0) break;
        /*printf("part%d-key1=%hhu part%d-key2=%hhu\\n",x,
                ((char*)((struct _key*)key1)->key)[x+offset1],x,
                ((char*)((struct _key*)key2)->key)[x+offset2]);*/
        if (((char*)((struct _key*)key1)->key)[x+offset1]
         != ((char*)((struct _key*)key2)->key)[x+offset2]) break;
    }

    //printf("x: %d ended:%d\\n", x, locended);
    *ended = locended;
    return x+1;//sizeof(int*)-1-x+offset1;
}

void* subkey(void *key, unsigned int offset, unsigned int len) {
    struct _key * ret;

    ret = malloc(sizeof(struct _key));
    //ret->key = ((struct _key*)key)->key;
    ret->key = malloc(sizeof(char)*len);
    //memset(((char*)&ret->key), 0, offset);

    memcpy(ret->key, ((struct _key*)key)->key+offset, len-offset);
    //printf("subkey offset:%d len:%d\\n", offset, len);
    ret->keylen = len-offset;

    return ret;
}

unsigned int key_len(void *key) {
    return ((struct _key*)key)->keylen;
}

void* key_concat(void *key1, void *key2) {
    struct _key * ret;
    unsigned int i, len;
    ret = malloc(sizeof(struct _key));
    ret->key = malloc(sizeof(char)*(((struct _key*)key1)->keylen +\
                                    ((struct _key*)key2)->keylen));

    for (i = 0; i < ((struct _key*)key1)->keylen; i++)
        ret->key[i] = ((struct _key*)key1)->key[i];
    ret->keylen = i; 
    len = i;
    for (i = 0; i < ((struct _key*)key2)->keylen; i++)
        ret->key[i+len] = ((struct _key*)key2)->key[i];
    ret->keylen += i;
    return ret;
}

void print_intp_data(void * data, const char * key) {
    if (data == NULL) printf("data for '%s': NULL\\n", key);
    else printf("data for '%s': %d\\n", key, *(int*)data);
}

void print_intp_hslist(COMPS_HSList * list) {
    COMPS_HSListItem * it;
    if (list == NULL)
        return;
    for (it = list->first; it != NULL; it = it->next) {
        printf("%d ", *(int*)it->data);
    }
    printf("\\n");
}

void* int_cloner(void * int_v) {
    void * ret = malloc(sizeof(int));
    memcpy(ret, int_v, sizeof(int));
    return ret;
}

void* str_cloner(void *str) {
    void *ret = malloc(sizeof(char)* (strlen((char*)str)+1));
    memcpy(ret, str, (strlen((char*)str)+1));
    return ret;
}

void print_all(COMPS_BRTree *rt) {
    COMPS_HSList *pairlist;
    COMPS_HSListItem *it;
    int *x;

    printf("-------- print pairs --------------\\n");
    pairlist = comps_brtree_pairs(rt);

    for (it = pairlist->first; it != NULL; it = it->next) {
        x = ((COMPS_BRTreePair*)it->data)->data;
        printf("%d \\n", *x);

    }
    comps_hslist_destroy(&pairlist);
    printf("-----------------------------------\\n");
}

int main(int argc, char * argv[]) {
    COMPS_BRTree * tree;//, *clonned;
    int *ret;
    struct _key *key;
    struct _teststruct *test1;
        (void)argc;
    (void)argv;

    tree = comps_brtree_create(NULL, &int_cloner, &free, &key_clone,
                               &key_destroy, &key_cmp, &key_len, &subkey,
                               &key_concat);

    int a = 1, value1 = 100;
    int b = 2, value2 = 200;
    int c = 3, value3 = 300;

    key = make_key(&a);
    comps_brtree_set(tree, key, int_cloner(&value1));
    key_destroy(key);
    key = make_key(&b);
    comps_brtree_set(tree, key, int_cloner(&value2));
    key_destroy(key);
    key = make_key(&c);
    comps_brtree_set(tree, key, int_cloner(&value3));
    key_destroy(key);
    print_all(tree);

    key = make_key(&a);
    ret = comps_brtree_get(tree, key);
    key_destroy(key);

    if (ret)
        printf("The value for key %d is %d\\n", a, *ret);
    else 
        printf("NULL\\n");

    comps_brtree_destroy(tree);

    return 0;   
}
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o check_brtree check_brtree.c $(pkg-config --cflags --libs libcomps libxml-2.0)")
        code, result = self.cmd("./check_brtree")
        self.assertIn("The value for key 1 is 100", result)
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf check_brtree.* /usr/include/libcomps/comps_types.*")