# -*- encoding: utf-8 -*-

"""
@File:      tc_libcomps_libcomps_fun003.py
@Time:      2024/5/9 14:20:05
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

import os
import shutil
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libcomps_libcomps_fun003.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libcomps libcomps-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        # libcomps not found comps_types.h, copy from Source code
        # https://bugzilla.openanolis.cn/show_bug.cgi?id=8966
        if not os.path.exists("/usr/include/libcomps/comps_types.h"):
            file_dir = os.path.dirname(__file__)
            shutil.copy(f"{file_dir}/comps_types.h", "/usr/include/libcomps")

        cmdline = '''cat >verify_comps_writing.c<<"EOF"
#include <stdio.h>
#include <stdlib.h>
#include "libcomps/comps_doc.h"
#include "libcomps/comps_parse.h"
#include "libcomps/comps_validate.h"
#include "libcomps/comps_docpackage.h"

int main() {
    printf("### start test_comps_doc_xml\\n");
    const char *groups_ids[] = {"g1", "g2", "g3", "g4"};
    const char *groups_names[] = {"group1", "group2", "group3", "group4"};
    const char *group_mpackages[][4] = {{"pkg1","pkg2","pkg3","pkg4"},
                                      {"p1", "p2", "p3", "pkg4"},
                                      {"package1", "package2", "pkg3", "p4"},
                                      {"pack1", "pack2","p3","pkg4"}};
    const char *group_opackages[][4] = {{"opkg1","opkg2","opkg3","opkg4"},
                                      {"op1", "op2", "op3", "opkg4"},
                                      {"opackage1", "opackage2", "opkg3", "op4"},
                                      {"opack1", "opack2","op3","opkg4"}};
    const char *cats_ids[] = {"c1", "c2", "c3"};
    const char *cats_names[] = {"cat1", "cat2", "cat3"};
    const char *cat_gids[][3] = {{"g1","g2","g3"},
                                 {"g2", "g3", "g4"},
                                 {"g1", "g2", "g4"}};

    COMPS_Doc *doc;
    COMPS_DocGroup *g;
    COMPS_DocCategory *c;
    COMPS_DocGroupPackage *p;
    COMPS_DocGroupId *gid;
    //COMPS_List *tmplist;
    COMPS_Str *str;
    str = comps_str("UTF-8");
    doc = (COMPS_Doc*)comps_object_create(&COMPS_Doc_ObjInfo,
                                          (COMPS_Object*[]){(COMPS_Object*)str});
    COMPS_OBJECT_DESTROY(str);
    for (int i=0; i<2; i++) {
        g = (COMPS_DocGroup*)comps_object_create(&COMPS_DocGroup_ObjInfo, NULL);
        comps_docgroup_set_id(g, (char*)groups_ids[i], 1);
        comps_docgroup_set_name(g, (char*)groups_names[i], 1);
        for (int x=0; x<4; x++) {
            p = (COMPS_DocGroupPackage*)
                comps_object_create(&COMPS_DocGroupPackage_ObjInfo, NULL);
            comps_docpackage_set_name(p, (char*)group_mpackages[i][x], 1);
            comps_docpackage_set_type(p, COMPS_PACKAGE_MANDATORY, false);
            comps_docgroup_add_package(g, p);
            p = (COMPS_DocGroupPackage*)
                comps_object_create(&COMPS_DocGroupPackage_ObjInfo, NULL);
            comps_docpackage_set_name(p, (char*)group_opackages[i][x], 1);
            comps_docpackage_set_type(p, COMPS_PACKAGE_OPTIONAL, false);
            comps_docgroup_add_package(g, p);
        }
        comps_doc_add_group(doc, g);
    }

    for (int i=0; i<3; i++) {
        c = (COMPS_DocCategory*)comps_object_create(&COMPS_DocCategory_ObjInfo,
                                                    NULL);
        comps_doccategory_set_id(c, (char*)cats_ids[i], 1);
        comps_doccategory_set_name(c, (char*)cats_names[i], 1);
        for (int x=0; x<3; x++) {
            gid = (COMPS_DocGroupId*)comps_object_create(&COMPS_DocGroupId_ObjInfo, NULL);
            comps_docgroupid_set_name(gid, (char*)cat_gids[i][x], 1);
            comps_doccategory_add_groupid(c, gid);
        }
        comps_doc_add_category(doc, c);
    }
    comps2xml_f(doc, "testfile.xml", 1, NULL, NULL);
    COMPS_OBJECT_DESTROY(doc);
}
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o verify_comps_writing verify_comps_writing.c $(pkg-config --cflags --libs libcomps libxml-2.0)")
        code, result = self.cmd("./verify_comps_writing")
        self.assertTrue(os.path.exists("testfile.xml"), msg="xml文件生成失败")
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf verify_comps_writing* /usr/include/libcomps/comps_types.* testfile.xml")
