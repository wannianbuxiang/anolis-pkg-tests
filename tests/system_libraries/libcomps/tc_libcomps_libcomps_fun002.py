# -*- encoding: utf-8 -*-

"""
@File:      tc_libcomps_libcomps_fun002.py
@Time:      2024/5/8 14:20:05
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

import os
import shutil
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libcomps_libcomps_fun002.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libcomps libcomps-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        # libcomps not found comps_types.h, copy from Source code
        # https://bugzilla.openanolis.cn/show_bug.cgi?id=8966
        if not os.path.exists("/usr/include/libcomps/comps_types.h"):
            file_dir = os.path.dirname(__file__)
            shutil.copy(f"{file_dir}/comps_types.h", "/usr/include/libcomps")

        cmdline = '''cat >verify_comps_parsing.xml<<"EOF"
<?xml version="1.0"?>
<comps>
  <group>
    <id>office</id>
    <name>Office</name>
    <description>An office software group</description>
    <default>true</default>
    <uservisible>true</uservisible>
    <packagelist>
      <packagereq type="default">libreoffice</packagereq>
      <packagereq type="optional">calligra</packagereq>
    </packagelist>
  </group>

  <category>
    <id>desktop-applications</id>
    <name>Desktop Applications</name>
    <description>Standard desktop applications</description>
    <grouplist>
      <groupid>office</groupid>
    </grouplist>
  </category>

  <environment>
    <id>gnome-desktop</id>
    <name>GNOME Desktop</name>
    <description>The GNOME desktop environment</description>
    <grouplist>
      <groupid>office</groupid>
    </grouplist>
    <optionlist>
      <groupid>games</groupid>
    </optionlist>
  </environment>
</comps>
EOF '''
        self.cmd(cmdline)

        cmdline = '''cat >verify_comps_parsing.c<<"EOF"
#include <stdio.h>
#include <stdlib.h>
#include "libcomps/comps_doc.h"
#include "libcomps/comps_parse.h"
#include "libcomps/comps_validate.h"
#include "libcomps/comps_docpackage.h"

int main() {
    COMPS_Doc *doc;
    COMPS_Parsed *parsed;
    COMPS_ObjList *groups, *categories, *environments;
    COMPS_Object *tmpobj, *tmpobj2;
    COMPS_ObjListIt *it;
    // 创建解析器对象
    parsed = comps_parse_parsed_create();
    
    // 初始化解析器对象
    if (!comps_parse_parsed_init(parsed, "UTF-8", 0)) {
        fprintf(stderr, "Failed to initialize parse object\\n");
        comps_parse_parsed_destroy(parsed);
        return 1;
    }
    // 打开并解析 COMPS XML 文件
    FILE *fp = fopen("verify_comps_parsing.xml", "r");
    if (!fp) {
        fprintf(stderr, "Failed to open COMPS XML file\\n");
        comps_parse_parsed_destroy(parsed);
        return 1;
    }
    
    if (!comps_parse_file(parsed, fp, NULL)) {
        fprintf(stderr, "Parsing COMPS XML file failed\\n");
        fclose(fp);
        comps_parse_parsed_destroy(parsed);
        return 1;
    }

    // 获取文档对象
    doc = parsed->comps_doc;
    if (!doc) {
        fprintf(stderr, "Parsed document not obtained\\n");
        comps_parse_parsed_destroy(parsed);
        return 1;
    }

    printf("Parsing COMPS XML file success\\n");
    // 解析并打印组、类别、环境的数量验证解析结果
    groups = comps_doc_groups(doc);
    categories = comps_doc_categories(doc);
    environments = comps_doc_environments(doc);
    printf("Parsed %d groups, %d categories, %d environments\\n",
          groups ? groups->len : 0,
          categories ? categories->len : 0,
          environments ? environments->len : 0);
    
    // 打印解析的组信息
    it = groups->first;
    tmpobj = comps_docgroup_get_id((COMPS_DocGroup*)it->comps_obj);
    printf("id： %d\\n", tmpobj);
    COMPS_OBJECT_DESTROY(tmpobj);
    tmpobj = comps_docgroup_get_name((COMPS_DocGroup*)it->comps_obj);
    printf("name： %d\\n", tmpobj);
    COMPS_OBJECT_DESTROY(tmpobj);
    tmpobj = comps_docgroup_get_desc((COMPS_DocGroup*)it->comps_obj);
    printf("desc： %d\\n", tmpobj);
    COMPS_OBJECT_DESTROY(tmpobj);

    // 清理资源
    COMPS_OBJECT_DESTROY(groups);
    COMPS_OBJECT_DESTROY(categories);
    COMPS_OBJECT_DESTROY(environments);
    comps_parse_parsed_destroy(parsed);
    
    return 0;
}
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o verify_comps_parsing verify_comps_parsing.c $(pkg-config --cflags --libs libcomps libxml-2.0)")
        code, result = self.cmd("./verify_comps_parsing")
        self.assertIn("Parsing COMPS XML file success", result)
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf verify_comps_parsing* /usr/include/libcomps/comps_types.*")
