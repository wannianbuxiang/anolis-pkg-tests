#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libedit_libedit_func_005.py
@Time:      2024/03/15 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
import pexpect
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libedit_libedit_func_005.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libedit libedit-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_libedit_el_insertstr.c << EOF
#include <histedit.h>
#include <stdio.h>
#include <stdlib.h>

// Prompt function
const char *prompt(EditLine *e) {
    return "Input> ";
}

int main() {
    // Initialize EditLine
    EditLine *el = el_init("myprog", stdin, stdout, stderr);
    if (!el) {
        fprintf(stderr, "Failed to initialize EditLine\n");
        return EXIT_FAILURE;
    }

    // Set the prompt function
    el_set(el, EL_PROMPT, prompt);

    // Insert a string at the current cursor position
    const char *insert_str = "Hello, ";
    if (el_insertstr(el, insert_str) == -1) {
        fprintf(stderr, "Failed to insert string\n");
        el_end(el);
        return EXIT_FAILURE;
    }

    // Process user input
    int count;
    const char *line;
    while ((line = el_gets(el, &count)) != NULL) {
        if (count > 0) {
            printf("You entered: %s", line);
        } else {
            // User pressed Ctrl+D on an empty line
            printf("\nCtrl+D pressed. Exiting.\n");
            break;
        }
    }

    // Clean up
    el_end(el);
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_libedit_el_insertstr test_libedit_el_insertstr.c -ledit")

    def test(self):
        self.cmd("echo 11111111 | ./test_libedit_el_insertstr")
        self.cmd("echo 'hello world' | ./test_libedit_el_insertstr")
        self.cmd("./test_libedit_el_insertstr <<<'this a test about libedit'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_libedit_el_insertstr', 'test_libedit_el_insertstr.c']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")