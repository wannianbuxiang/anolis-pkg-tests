#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libedit_libedit_func_002.py
@Time:      2024/03/15 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libedit_libedit_func_002.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libedit libedit-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_libedit_readline.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <editline/readline.h>

int main() {
    // Initialize readline (optional)
    rl_initialize();

    char *input;
    // Prompt the user for input
    input = readline("Enter some text: ");
    // Check if EOF was reached (Ctrl+D)
    if (input == NULL) {
        printf("\nEOF reached.\n");
        return EXIT_SUCCESS;
    }
    // Check for empty input
    if (input && *input) {
        // Output the entered line
        printf("You entered: '%s'\n", input);
    }
    // Free the memory allocated by readline
    free(input);
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_libedit_readline test_libedit_readline.c -ledit")

    def test(self):
        ret_c, _ = self.cmd("echo hello |./test_libedit_readline", ignore_status=True)
        self.assertEqual(0, ret_c)
        ret_c, _ = self.cmd("echo 111 |./test_libedit_readline", ignore_status=True)
        self.assertEqual(0, ret_c)
        ret_c, _ = self.cmd("./test_libedit_readline <<<'龙蜥'", ignore_status=True)
        self.assertEqual(0, ret_c)
        self.cmd("./test_libedit_readline <<<'this a test for readline,0.0'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_libedit_readline', 'test_libedit_readline.c']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")
