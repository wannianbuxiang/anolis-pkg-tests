#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libedit_libedit_func_004.py
@Time:      2024/03/15 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
import pexpect
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libedit_libedit_func_004.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libedit libedit-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_libedit_el_push.c << EOF
#include <histedit.h>
#include <stdio.h>
#include <stdlib.h>

const char *prompt(EditLine *e) {
    return "myprog> ";
}

int main(int argc, char **argv) {
    // Check if an argument is provided
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <string_to_process>\n", argv[0]);
        return EXIT_FAILURE;
    }
    // The string to push onto the input stream is the first argument
    const char *str_to_push = argv[1];
    // Initialize EditLine
    EditLine *el = el_init("myprog", stdin, stdout, stderr);
    if (!el) {
        fprintf(stderr, "EditLine initialization failed\n");
        return EXIT_FAILURE;
    }
    // Set the prompt function
    el_set(el, EL_PROMPT, prompt);
    // Push the external string onto the input stream
    el_push(el, str_to_push);
    // Add a newline to mark the end of the input
    el_push(el, "\n");
    // Process the input that was pushed
    int count;
    const char *line;
    if ((line = el_gets(el, &count)) != NULL && count > 0) {
        printf("You entered: %s", line); // Echo the line back
        // In a real application, you would parse and execute the command here
    }
    // Clean up
    el_end(el);
    // Explicitly exit the program
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_libedit_el_push test_libedit_el_push.c -ledit")

    def test(self):
        res,code = pexpect.run("./test_libedit_el_push 'hello world\n'", withexitstatus=True)
        [ print(l) for l in res.decode("utf-8").strip().split('\n\n') ]
        self.assertEqual(code, 0)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_libedit_el_push', 'test_libedit_el_push.c']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")