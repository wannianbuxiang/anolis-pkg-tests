#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libedit_libedit_func_008.py
@Time:      2024/03/15 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
import pexpect
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libedit_libedit_func_008.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libedit libedit-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_libedit_el_wpush.c << EOF
#include <histedit.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>

// Set the locale to the user\'s default (usually specified in the LANG environment variable)
void set_locale() {
    if (setlocale(LC_ALL, "") == NULL) {
        fwprintf(stderr, L"Unable to set locale. Make sure your LANG environment variable is set correctly.\n");
        exit(EXIT_FAILURE);
    }
}

// Prompt function
const wchar_t *prompt(EditLine *e) {
    return L"Input> ";
}

int main() {
    // Set the locale
    set_locale();

    // Initialize EditLine
    EditLine *el = el_init("myprog", stdin, stdout, stderr);
    if (!el) {
        fwprintf(stderr, L"Failed to initialize EditLine\n");
        return EXIT_FAILURE;
    }

    // Set the prompt function
    el_set(el, EL_PROMPT_ESC, prompt, L'\1');
    // Since we are using `el_wgets`, the `EditLine` library expects an ENTER key
    // to process the input. We simulate this by pushing a newline character.
    el_wpush(el, L"\n");

    // Process the input that was pushed
    int count;
    const wchar_t *line = el_wgets(el, &count);
    if (line != NULL && count > 0) {
        wprintf(L"You entered: %ls\n", line); // Echo the line back
    }

    // Clean up
    el_end(el);
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_libedit_el_wpush test_libedit_el_wpush.c -ledit")

    def test(self):
        self.cmd("echo -e '0123456789abcdefghijklmnopqrstuvwxyz' | ./test_libedit_el_wpush")
        self.cmd("echo -e 'hello world!,this is a test about libeit packages' | ./test_libedit_el_wpush")


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_libedit_el_wpush', 'test_libedit_el_wpush.c']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")