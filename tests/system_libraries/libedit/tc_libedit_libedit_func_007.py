#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libedit_libedit_func_007.py
@Time:      2024/03/15 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
import pexpect
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libedit_libedit_func_007.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libedit libedit-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_libedit_el_wgets.c << EOF
#include <histedit.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>

// Prompt function
const wchar_t *prompt(EditLine *e) {
    return L"Input> ";
}

int main() {
    // Set the locale to the user\'s default (usually specified in the LANG environment variable)
    setlocale(LC_ALL, "");

    // Initialize EditLine
    EditLine *el = el_init("myprog", stdin, stdout, stderr);
    if (!el) {
        fwprintf(stderr, L"Failed to initialize EditLine\n");
        return EXIT_FAILURE;
    }

    // Set the prompt function
    el_set(el, EL_PROMPT_ESC, prompt, L'\1');

    // Process user input
    int count;
    const wchar_t *line;
    while ((line = el_wgets(el, &count)) != NULL) {
        if (count > 0) {
            // Echo the wide-character line back
            wprintf(L"You entered: %ls", line);
        } else if (count == 0) {
            // User pressed Ctrl+D on an empty line
            wprintf(L"\nCtrl+D pressed. Exiting.\n");
            break;
        }
    }

    // Clean up
    el_end(el);
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_libedit_el_wgets test_libedit_el_wgets.c -ledit")

    def test(self):
        self.cmd("echo -e '1\n2\n3\n4\n5\n6' | ./test_libedit_el_wgets")
        self.cmd("echo -e 'h\ne\nl\nl\no' | ./test_libedit_el_wgets")
        self.cmd("./test_libedit_el_wgets <<<'t\nh\ni\ns\n \na\n \nt\ne\ns\nt\n \na\nb\no\nu\nt\n \nl\ni\nb\ne\nd\ni\nt'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_libedit_el_wgets', 'test_libedit_el_wgets.c']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")