#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libedit_libedit_func_003.py
@Time:      2024/03/15 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libedit_libedit_func_003.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libedit libedit-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_libedit_el_gets.c << EOF
#include <histedit.h>
#include <stdio.h>
#include <wchar.h>
#include <stdlib.h>

int main() {
    // Initialize EditLine and the associated history
    EditLine *el = el_init("myprog", stdin, stdout, stderr);
    History *hist = history_init();
    if (!el || !hist) {
        fprintf(stderr, "EditLine or history initialization failed\n");
        return EXIT_FAILURE;
    }

    // Configure history
    HistEvent ev;
    history(hist, &ev, H_SETSIZE, 100);
    el_set(el, EL_HIST, history, hist);

    // Prompt and read a line from the user
    int count;
    const char *line = el_gets(el, &count);

    // Display the entered line
    if (line) {
        printf("You entered: %s", line);
    } else {
        printf("No input or error\n");
    }

    // Retrieve and display the current line buffer and cursor position
    const LineInfo *li = el_line(el);
    if (li) {
        printf("Current line buffer: %.*s\n", (int)(li->lastchar - li->buffer), li->buffer);
        printf("Cursor position: %td\n", li->cursor - li->buffer);
    }

    // Clean up
    history_end(hist);
    el_end(el);
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_libedit_el_gets test_libedit_el_gets.c -ledit")

    def test(self):
        self.cmd("echo | ./test_libedit_el_gets")
        self.cmd("./test_libedit_el_gets <<<'1234567'")
        self.cmd("echo abcde| ./test_libedit_el_gets")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_libedit_el_gets', 'test_libedit_el_gets.c']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")
