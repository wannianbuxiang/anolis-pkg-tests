#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libedit_libedit_func_001.py
@Time:      2024/03/15 14:20:00
@Author:    smj01095381
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    smj01095381
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libedit_libedit_func_001.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libedit libedit-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_libedit_histedit.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <histedit.h>
#include <string.h>

const char* prompt(EditLine *e) {
    return "input> ";
}

int main(int argc, char *argv[]) {
    EditLine *el;
    History *myhistory;
    HistEvent ev;
    const char *line;
    int count;

    // 初始化 EditLine
    el = el_init("test", stdin, stdout, stderr);
    el_set(el, EL_PROMPT, &prompt);
    el_set(el, EL_EDITOR, "emacs"); // 或 "vi"

    // 初始化 History
    myhistory = history_init();
    if (myhistory == 0) {
        fprintf(stderr, "History could not be initialized.\n");
        return 1;
    }

    history(myhistory, &ev, H_SETSIZE, 100); // 设置历史大小
    el_set(el, EL_HIST, history, myhistory); // 关联历史到 EditLine

    // 利用 pipe 实现非交互性测试
    int fds[2];
    if (pipe(fds) != 0) {
        perror("pipe");
        exit(1);
    }

    // 写入测试命令到管道的写端
    const char *test_command = argv[1];
    write(fds[1], test_command, strlen(test_command));

    // 重定向管道的读端到 stdin
    dup2(fds[0], STDIN_FILENO);
    close(fds[1]); // 关闭写端

    // 读取输入并处理
    while ((line = el_gets(el, &count)) != NULL && count > 0) {
        printf("Got: %s", line);
        history(myhistory, &ev, H_ENTER, line);
    }

    // 清理资源
    history_end(myhistory);
    el_end(el);
    close(fds[0]); // 关闭读端

    return 0;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_libedit_histedit test_libedit_histedit.c -ledit -Wno-implicit-function-declaration")

    def test(self):
        ret_c, _ = self.cmd("echo hello |./test_libedit_histedit 'echo TEST001\n'", ignore_status=True)
        self.assertEqual(0, ret_c)
        ret_c, _ = self.cmd("echo 111 |./test_libedit_histedit 'echo TEST002\n'", ignore_status=True)
        self.assertEqual(0, ret_c)
        ret_c, _ = self.cmd("echo 龙蜥 |./test_libedit_histedit 'echo TEST003\n'", ignore_status=True)
        self.assertEqual(0, ret_c)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_libedit_histedit', 'test_libedit_histedit.c']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")