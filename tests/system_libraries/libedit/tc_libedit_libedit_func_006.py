#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libedit_libedit_func_006.py
@Time:      2024/03/15 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
import pexpect
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libedit_libedit_func_006.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libedit libedit-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_libedit_el_deletestr.c << EOF
#include <histedit.h>
#include <stdio.h>
#include <stdlib.h>

// Prompt function
const char *prompt(EditLine *e) {
    return "Input> ";
}

int main() {
    // Initialize EditLine
    EditLine *el = el_init("myprog", stdin, stdout, stderr);
    if (!el) {
        fprintf(stderr, "Failed to initialize EditLine\n");
        return EXIT_FAILURE;
    }

    // Set the prompt function
    el_set(el, EL_PROMPT, prompt);

    // Insert an initial string that the user will be editing
    const char *initial_str = "initial string";
    if (el_insertstr(el, initial_str) == -1) {
        fprintf(stderr, "Failed to insert string\n");
        el_end(el);
        return EXIT_FAILURE;
    }

    // Allow the user to edit the line
    int count;
    const char *line = el_gets(el, &count);
    if (count > 0) {
        printf("Before deletion: %s", line);

        // Assume we want to delete the last 3 characters entered by the user
        // Move the cursor to the end minus the number of characters to delete
        if (el_cursor(el, count - 3) == -1) {
            fprintf(stderr, "Failed to move cursor\n");
            el_end(el);
            return EXIT_FAILURE;
        }

        // Delete the last 3 characters
        el_deletestr(el, 3);

        // Get the current content of the line
        const LineInfo *li = el_line(el);
        printf("After deletion: %.*s", (int)(li->lastchar - li->buffer), li->buffer);
    } else {
        // User pressed Ctrl+D on an empty line
        printf("\nCtrl+D pressed. Exiting.\n");
    }

    // Clean up
    el_end(el);
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_libedit_el_deletestr test_libedit_el_deletestr.c -ledit")

    def test(self):
        self.cmd("echo -e '11111111\n' | ./test_libedit_el_deletestr")
        self.cmd("echo -e 'hello world\n' | ./test_libedit_el_deletestr")
        self.cmd("./test_libedit_el_deletestr <<<'this a test about libedit\n'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_libedit_el_deletestr', 'test_libedit_el_deletestr.c']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")