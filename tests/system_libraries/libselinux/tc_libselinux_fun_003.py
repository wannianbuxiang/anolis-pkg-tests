#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libselinux_fun_003.py
@Time:      2024/04/14 10:12:50
@Author:    hongfeng
@Version:   1.0
@Contact:   wb-zhf668260@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    hongfeng
"""

from common.basetest import LocalTest
class Test(LocalTest):
    """
    See tc_libselinux_fun_003.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libselinux-devel gcc"}
    enforce = ""
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > test03.c <<EOF
#include <selinux/selinux.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>

#define TEST_FILE "test_file"
#define TEST_OPERATION "read"

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Usage: %s  'file_context'\\n", argv[0]);
        printf("Where:\\n");
        printf("   - The file_context, like: 'user_u:object_r:test_file_t:s0';\\n");
        return 1;
    }

    size_t arg_length = strlen(argv[1]) + 1;
    char file_context[arg_length];
    strcpy(file_context, argv[1]);

    int ret;
    char *current_context;
    
    // Create test files and set their contexts
    int fd = open(TEST_FILE, O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR);
    if (fd == -1) {
        perror("Failed to create test_file_1");
        return 1;
    }
    if (fsetfilecon(fd, file_context) != 0) {
        fprintf(stderr, "Failed to set context for test_file\\n");
        close(fd);
        unlink(TEST_FILE);
        return 1;
    }
    close(fd);

    ret = getcon(&current_context);
    if (ret != 0) {
        perror("getcon() failed");
        exit(EXIT_FAILURE);
    }

    printf("Current process context: %s\\n", current_context);

    // Check access permissions
    ret = selinux_check_access(current_context, file_context, "file", TEST_OPERATION, NULL);
    printf("Access check for %s on %s: %s\\n", TEST_OPERATION, TEST_FILE, ret == 0 ? "Allowed" : "Denied");

    // Clean up
    freecon(current_context);
    unlink(TEST_FILE);

    return 0;
}
EOF"""
        self.cmd(cmdline)
        self.cmd("gcc -o test03 test03.c -lselinux")
        ret_c, ret_o = self.cmd("getenforce")
        self.enforce = ret_o.strip()

    def test(self):
        code,selinux_result=self.cmd("sestatus")
        if selinux_result.endswith("disabled"):
            self.skip("SELinux is disabled,skip the case")
        else:
            self.cmd("setenforce 0")
            ret_c, ret_o = self.cmd("./test03 user_u:object_r:test_file_t:s0")
            self.assertTrue("Access check for read on test_file: Allowed" in ret_o, 'check output error.')
		    
            ret_c, ret_o = self.cmd("./test03 system_u:object_r:secret_file_t:s0", ignore_status=True)
            self.assertTrue("Access check for read on test_file: Allowed" in ret_o, 'check output error.')
		    
            self.cmd("setenforce 1")
            ret_c, ret_o = self.cmd("./test03 user_u:object_r:test_file_t:s0")
            self.assertTrue("Access check for read on test_file: Allowed" in ret_o, 'check output error.')
		    
            ret_c, ret_o = self.cmd("./test03 system_u:object_r:secret_file_t:s0", ignore_status=True)
            self.assertTrue("Failed to set context for test_file" in ret_o, 'check output error.')
            self.assertTrue("Access check for read on test_file: Allowed" not in ret_o, 'check output error.')

        
    def tearDown(self):
        enforce_dict = {"Permissive": "0", "Enforcing": "1"}
        enforce = enforce_dict[self.enforce]
        self.cmd(f"setenforce {enforce}" )
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm test03.c test03")