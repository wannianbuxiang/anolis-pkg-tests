#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libselinux_fun_002.py
@Time:      2024/04/14 10:12:50
@Author:    hongfeng
@Version:   1.0
@Contact:   wb-zhf668260@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    hongfeng
"""

from common.basetest import LocalTest
class Test(LocalTest):
    """
    See tc_libselinux_fun_002.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libselinux-devel gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > test02.c <<EOF
#include <selinux/selinux.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

#define TEST_FILE_PATH "test_file02.txt"

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Usage: %s  'context'\\n", argv[0]);
        printf("Where:\\n");
        printf("   - The context, like: 'user_u:object_r:test_file_t:s0';\\n");
    }

    size_t arg_length = strlen(argv[1]) + 1;
    char new_context[arg_length];
    strcpy(new_context, argv[1]);

    int ret;
    char *current_context = NULL;
    security_context_t file_context;

    // 创建测试文件
    FILE *test_file = fopen(TEST_FILE_PATH, "w");
    if (!test_file) {
        fprintf(stderr, "Failed to create test file: %s\\n", strerror(errno));
        return EXIT_FAILURE;
    }
    fclose(test_file);

    // 获取当前文件的安全上下文
    ret = getfilecon(TEST_FILE_PATH, &current_context);
    if (ret < 0) {
        fprintf(stderr, "Failed to get file context: %s\\n", strerror(errno));
        return EXIT_FAILURE;
    } else {
        printf("Current file context: %s\\n", current_context);
        freecon(current_context);
    }

    // 设置文件的安全上下文
    ret = setfilecon(TEST_FILE_PATH, new_context);
    if (ret < 0) {
        fprintf(stderr, "Failed to set file context: %s\\n", strerror(errno));
        return EXIT_FAILURE;
    }

    // 验证文件上下文是否已成功更新
    ret = getfilecon(TEST_FILE_PATH, &current_context);
    if (ret < 0) {
        fprintf(stderr, "Failed to get updated file context: %s\\n", strerror(errno));
        return EXIT_FAILURE;
    } else {
        if (strcmp(current_context, new_context) == 0) {
            printf("File context successfully updated to: %s\\n", current_context);
        } else {
            fprintf(stderr, "Error: File context not updated correctly.\\n");
            freecon(current_context);
            return EXIT_FAILURE;
        }
        freecon(current_context);
    }

    // 使用security_context_t类型进行上下文操作
    if (getfilecon_raw(TEST_FILE_PATH, &file_context) < 0) {
        fprintf(stderr, "Failed to get file context using security_context_t: %s\\n", strerror(errno));
        return EXIT_FAILURE;
    } else {
        printf("File context (security_context_t): %s\\n", file_context);
        freecon(file_context);
    }

    // 清理：删除测试文件
    if (remove(TEST_FILE_PATH) != 0) {
        fprintf(stderr, "Failed to remove test file: %s\\n", strerror(errno));
        return EXIT_FAILURE;
    }

    printf("All test cases passed.\\n");
    return EXIT_SUCCESS;
}
EOF"""
        self.cmd(cmdline)
        self.cmd("gcc -o test02 test02.c -lselinux")

    def test(self):
        code, selinux_result = self.cmd("sestatus")
        if selinux_result.endswith("disabled"):
            self.skip("SELinux is disabled,skip the case")
        else:
            ret_c, ret_o = self.cmd("./test02 'user_u:object_r:test_file_t:s0'")
            self.assertTrue("All test cases passed" in ret_o ,'check output error.')

            ret_c, ret_o = self.cmd("./test02 'unconfined_u:user_r:ssh_home_t:s0'")
            self.assertTrue("All test cases passed" in ret_o ,'check output error.')

            ret_c, ret_o = self.cmd("./test02 'system_u:sysadm_r:httpd_sys_content_t:s0'")
            self.assertTrue("All test cases passed" in ret_o ,'check output error.')
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm test02.c test02")
