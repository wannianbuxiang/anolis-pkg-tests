#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libselinux_fun_001.py
@Time:      2024/04/14 10:12:50
@Author:    hongfeng
@Version:   1.0
@Contact:   wb-zhf668260@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    hongfeng
"""

from common.basetest import LocalTest
class Test(LocalTest):
    """
    See tc_libselinux_fun_001.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libselinux-devel gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > test01.c <<EOF
#include <selinux/selinux.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

/* Assume that `selinux_getenforce()` is already defined and implemented elsewhere. */

int main() {
    int enforce_status;
    int result;

    /* Call the selinux_getenforce() function to retrieve the enforcement status */
    result = selinux_getenforcemode(&enforce_status);

    /* Print the result based on the returned value */
    switch (enforce_status) {
        case 0:
            printf("SELinux is currently in 'Permissive' mode.\\n");
            break;
        case 1:
            printf("SELinux is currently in 'Enforcing' mode.\\n");
            break;
        case -1:
            printf("SELinux is currently in 'Disabled' mode.\\n");
            break;
        default:
            fprintf(stderr, "Unknown SELinux enforcement status returned: %d\\n", enforce_status);
            return 1;
    }

    return 0;
}
EOF"""
        self.cmd(cmdline)
        self.cmd("gcc -o test01 test01.c -lselinux")
        self.cmd("cp /etc/selinux/config /etc/selinux/config.bak")

    def test(self):
        # 将SELINUX设为permissive，用代码验证
        self.cmd("sed -i 's/^SELINUX=.*/SELINUX=permissive/' /etc/selinux/config")
        ret_c, ret_o = self.cmd("./test01")
        self.assertTrue("Permissive" in ret_o, 'check output error.')

        # 将SELINUX设为enforcing，用代码验证
        self.cmd("sed -i 's/^SELINUX=.*/SELINUX=enforcing/' /etc/selinux/config")
        ret_c, ret_o = self.cmd("./test01")
        self.assertTrue("Enforcing" in ret_o, 'check output error.')

        # 将SELINUX设为disabled，用代码验证
        self.cmd("sed -i 's/^SELINUX=.*/SELINUX=disabled/' /etc/selinux/config")
        ret_c, ret_o = self.cmd("./test01")
        self.assertTrue("Disabled" in ret_o, 'check output error.')
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("mv -f /etc/selinux/config.bak /etc/selinux/config")
        self.cmd("rm test01.c test01")
