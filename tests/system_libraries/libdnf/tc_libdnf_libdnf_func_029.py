#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libdnf_libdnf_func_029.py
@Time:      2024/03/26 14:08:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libdnf_libdnf_func_029.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libdnf libdnf-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_dnf_package_get_regular_requires.c << EOF
#include <stdio.h>
#include <glib.h>
#include <libdnf/libdnf.h>

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <package-name>\n", argv[0]);
        return 1;
    }

    const char *package_name = argv[1];
    g_autoptr(GError) error = NULL;
    g_autoptr(DnfSack) sack = dnf_sack_new();

    // Load system repository
    if (!dnf_sack_load_system_repo(sack, NULL, 0, &error)) {
        fprintf(stderr, "Error loading system repo: %s\n", error->message);
        return 1;
    }

    // Create a query to search for the provided package name
    HyQuery query = hy_query_create(sack);
    hy_query_filter(query, HY_PKG_NAME, HY_EQ, package_name);

    // Execute the query and get the list of packages that match
    GPtrArray *packages = hy_query_run(query);
    if (packages->len == 0) {
        fprintf(stderr, "No package named '%s' found.\n", package_name);
        hy_query_free(query);
        g_ptr_array_unref(packages);
        return 1;
    }

    // Retrieve the regular dependencies for the first package found
    DnfPackage *pkg = g_ptr_array_index(packages, 0);
    DnfReldepList *requires = dnf_package_get_regular_requires(pkg);

    if (requires && dnf_reldep_list_count(requires) > 0) {
        printf("Package '%s' has the following regular (non-weak) dependencies:\n", package_name);
        for (int i = 0; i < dnf_reldep_list_count(requires); i++) {
            DnfReldep *reldep = dnf_reldep_list_index(requires, i);
            const char *str = dnf_reldep_to_string(reldep);
            printf("  - %s\n", str);
        }
        printf("\n");
    } else {
        printf("No regular (non-weak) dependencies found for package '%s'.\n", package_name);
    }

    // Cleanup
    hy_query_free(query);
    g_ptr_array_unref(packages);

    return 0;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_dnf_package_get_regular_requires test_dnf_package_get_regular_requires.c `pkg-config --cflags --libs libdnf gobject-2.0`")

    def test(self):
        self.cmd("./test_dnf_package_get_regular_requires make")
        self.cmd("./test_dnf_package_get_regular_requires bash")
        self.cmd("./test_dnf_package_get_regular_requires gcc")
        self.cmd("./test_dnf_package_get_regular_requires libdnf")
        self.cmd("./test_dnf_package_get_regular_requires python3")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_dnf_package_get_regular_requires', 'test_dnf_package_get_regular_requires.c']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")
