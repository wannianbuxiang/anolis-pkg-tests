#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libdnf_libdnf_func_049.py
@Time:      2024/03/26 14:08:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libdnf_libdnf_func_049.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libdnf libdnf-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_hy_selector_matches.c << EOF
#include <glib.h>
#include <libdnf/libdnf.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <package_name> <version>\n", argv[0]);
        return EXIT_FAILURE;
    }
    const char *package_name = argv[1];
    const char *version = argv[2]; // Version might need to include the release number

    DnfSack *sack = dnf_sack_new();
    if (!sack) {
        fprintf(stderr, "Failed to create DnfSack\n");
        return EXIT_FAILURE;
    }

    // Load system repository
    if (!dnf_sack_load_system_repo(sack, NULL, 0, NULL)) {
        fprintf(stderr, "Failed to load system repository\n");
        g_object_unref(sack);
        return EXIT_FAILURE;
    }

    // Create a selector for the package
    HySelector selector = hy_selector_create(sack);
    if (!selector) {
        fprintf(stderr, "Failed to create selector\n");
        g_object_unref(sack);
        return EXIT_FAILURE;
    }

    // Set selection criteria for package name
    if (hy_selector_set(selector, HY_PKG_NAME, HY_EQ, package_name)) {
        fprintf(stderr, "Failed to set package name selector\n");
        hy_selector_free(selector);
        g_object_unref(sack);
        return EXIT_FAILURE;
    }

    // Set selection criteria for package version
    // Assume version includes both the version and the release number (e.g., "8.2-1.el8")
    if (hy_selector_set(selector, HY_PKG_EVR, HY_EQ, version)) {
        fprintf(stderr, "Failed to set package version selector\n");
        hy_selector_free(selector);
        g_object_unref(sack);
        return EXIT_FAILURE;
    }

    // Get matching packages
    GPtrArray *matches = hy_selector_matches(selector);
    if (!matches) {
        fprintf(stderr, "Failed to get matches\n");
        hy_selector_free(selector);
        g_object_unref(sack);
        return EXIT_FAILURE;
    }

    // Display results
    printf("Found %u matching packages:\n", matches->len);
    for (guint i = 0; i < matches->len; i++) {
        DnfPackage *pkg = g_ptr_array_index(matches, i);
        printf("Match: %s-%s-%s.%s\n",
               dnf_package_get_name(pkg),
               dnf_package_get_version(pkg),
               dnf_package_get_release(pkg),
               dnf_package_get_arch(pkg));
    }

    // Cleanup
    g_ptr_array_unref(matches);
    hy_selector_free(selector);
    g_object_unref(sack);

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_hy_selector_matches test_hy_selector_matches.c `pkg-config --cflags --libs libdnf gobject-2.0`")

    def test(self):
        self.cmd("./test_hy_selector_matches bash `rpm -q --queryformat '%{VERSION}\n' bash`")
        self.cmd("./test_hy_selector_matches make `rpm -q --queryformat '%{VERSION}\n' make`")
        self.cmd("./test_hy_selector_matches gcc `rpm -q --queryformat '%{VERSION}\n' gcc`")
        self.cmd("./test_hy_selector_matches libdnf `rpm -q --queryformat '%{VERSION}\n' libdnf`")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_hy_selector_matches', 'test_hy_selector_matches.c']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")
