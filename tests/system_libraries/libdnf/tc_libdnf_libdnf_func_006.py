#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libdnf_libdnf_func_006.py
@Time:      2024/03/26 14:08:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libdnf_libdnf_func_006.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libdnf libdnf-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        gen_dnf_conf = r'''cat <<EOF > dnf006.conf
[main]
rpm_verbosity=info
EOF'''
        code = r'''cat > test_dnf_context_get_rpm_verbosity.c << EOF
#include <libdnf/libdnf.h>
#include <stdio.h>
#include <stdlib.h>
#include <glib.h>

int main(int argc, char **argv) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <DNF-CONFIG-FILE-PATH>\n", argv[0]);
        return 1;
    }

    // 创建 GKeyFile 对象以读取配置文件
    GKeyFile *key_file = g_key_file_new();
    GError *error = NULL;

    // 加载 DNF 配置文件
    if (!g_key_file_load_from_file(key_file, argv[1], G_KEY_FILE_NONE, &error)) {
        fprintf(stderr, "Failed to load config file: %s\n", error ? error->message : "Unknown error");
        if (error) g_error_free(error);
        g_key_file_free(key_file);
        return 1;
    }

    // 获取 'rpm_verbosity' 配置项
    gchar *rpm_verbosity = g_key_file_get_string(key_file, "main", "rpm_verbosity", &error);
    if (error != NULL) {
        fprintf(stderr, "Failed to get 'rpm_verbosity' from config file: %s\n", error->message);
        g_error_free(error);
        g_key_file_free(key_file);
        return 1;
    }

    // 初始化 DnfContext
    DnfContext *context = dnf_context_new();
    if (!context) {
        fprintf(stderr, "Failed to create DnfContext.\n");
        g_free(rpm_verbosity);
        g_key_file_free(key_file);
        return 1;
    }

    // 获取RPM详细信息级别
    const gchar *obtained_verbosity = dnf_context_get_rpm_verbosity(context);
    if (obtained_verbosity == NULL) {
        dnf_context_set_rpm_verbosity(context, rpm_verbosity);
    }

    printf("RPM verbosity: %s\n", dnf_context_get_rpm_verbosity(context));
    // 清理资源
    g_object_unref(context);
    g_free(rpm_verbosity);
    g_key_file_free(key_file);
    return 0;
}        
EOF'''
        self.cmd(gen_dnf_conf)
        self.cmd(code)
        self.cmd("gcc -o test_dnf_context_get_rpm_verbosity test_dnf_context_get_rpm_verbosity.c `pkg-config --cflags --libs libdnf gobject-2.0`")

    def test(self):
        self.cmd("./test_dnf_context_get_rpm_verbosity dnf006.conf")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_dnf_context_get_rpm_verbosity', 'test_dnf_context_get_rpm_verbosity.c', 'dnf006.conf']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")
