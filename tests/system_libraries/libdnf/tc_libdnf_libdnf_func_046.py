#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libdnf_libdnf_func_046.py
@Time:      2024/03/26 14:08:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libdnf_libdnf_func_046.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libdnf libdnf-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_hy_query_filter_reldep.c << EOF
#include <libdnf/libdnf.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <depstring>\n", argv[0]);
        return EXIT_FAILURE;
    }
    const char *depstring_const = argv[1];
    DnfSack *sack;
    HyQuery query;
    GPtrArray *packages;
    DnfReldepList *reldep_list;
    DnfReldep *reldep;

    // Duplicate the depstring to make it mutable
    char *depstring = strdup(depstring_const);
    if (!depstring) {
        fprintf(stderr, "Failed to duplicate depstring\n");
        return EXIT_FAILURE;
    }
    // Create and initialize DnfSack object
    sack = dnf_sack_new();
    if (!sack) {
        fprintf(stderr, "Failed to create DnfSack\n");
        return EXIT_FAILURE;
    }

    // Set cache directory
    dnf_sack_set_cachedir(sack, "/var/cache/dnf");

    // Load system repository
    if (!dnf_sack_load_system_repo(sack, NULL, 0, NULL)) {
        fprintf(stderr, "Failed to load system repository\n");
        g_object_unref(sack);
        return EXIT_FAILURE;
    }

    // Create DnfReldepList object
    reldep_list = dnf_reldep_list_new(sack);
    if (!reldep_list) {
        fprintf(stderr, "Failed to create DnfReldepList\n");
        g_object_unref(sack);
        return EXIT_FAILURE;
    }

    // Create and add DnfReldep object to list
    // NOTE: Assuming depstring is in the form "name >= version"
    char *name = strtok(depstring, " ");
    char *cmp_type_str = strtok(NULL, " ");
    char *version = strtok(NULL, " ");

    int cmp_type = HY_EQ; // Default comparison type
    if (cmp_type_str) {
        if (strcmp(cmp_type_str, ">=") == 0) cmp_type = HY_GT;
        else if (strcmp(cmp_type_str, "<=") == 0) cmp_type = HY_LT;
        else if (strcmp(cmp_type_str, ">") == 0) cmp_type = HY_GT;
        else if (strcmp(cmp_type_str, "<") == 0) cmp_type = HY_LT;
        else if (strcmp(cmp_type_str, "=") == 0) cmp_type = HY_EQ;
        else if (strcmp(cmp_type_str, "!=") == 0) cmp_type = HY_NEQ;
        // Add more comparison types if needed
    }

    reldep = dnf_reldep_new(sack, name, cmp_type, version);
    if (!reldep) {
        fprintf(stderr, "Failed to create DnfReldep\n");
        g_object_unref(reldep_list);
        g_object_unref(sack);
        return EXIT_FAILURE;
    }
    dnf_reldep_list_add(reldep_list, reldep);
    

    // Create query
    query = hy_query_create(sack);
    hy_query_filter_reldep(query, HY_PKG_REQUIRES, reldep);

    // Run query
    packages = hy_query_run(query);
    if (!packages) {
        fprintf(stderr, "Failed to run query\n");
        hy_query_free(query);
        g_object_unref(reldep_list);
        g_object_unref(sack);
        return EXIT_FAILURE;
    }

    // Print query results
    for (guint i = 0; i < packages->len; i++) {
        DnfPackage *pkg = g_ptr_array_index(packages, i);
        printf("%s-%s-%s.%s\n",
               dnf_package_get_name(pkg),
               dnf_package_get_version(pkg),
               dnf_package_get_release(pkg),
               dnf_package_get_arch(pkg));
    }

    // Clean up resources
    free(depstring);
    
    g_ptr_array_unref(packages);
    // g_object_unref(reldep_list);
    // g_object_unref(sack);
    hy_query_free(query);
    // g_object_unref(reldep); // The reldep_list holds its own reference
    
    
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_hy_query_filter_reldep test_hy_query_filter_reldep.c `pkg-config --cflags --libs libdnf gobject-2.0`")

    def test(self):
        self.cmd("./test_hy_query_filter_reldep make")
        self.cmd("./test_hy_query_filter_reldep bash")
        self.cmd("./test_hy_query_filter_reldep gcc")
        self.cmd("./test_hy_query_filter_reldep libdnf")
        self.cmd("./test_hy_query_filter_reldep python3")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_hy_query_filter_reldep', 'test_hy_query_filter_reldep.c']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")
