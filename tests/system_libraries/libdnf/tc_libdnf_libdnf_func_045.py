#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libdnf_libdnf_func_045.py
@Time:      2024/03/26 14:08:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libdnf_libdnf_func_045.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libdnf libdnf-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_hy_query_filter_latest.c << EOF
#include <libdnf/libdnf.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    DnfSack *sack;
    HyQuery query;
    GPtrArray *packages;

    if (argc != 2) {
        fprintf(stderr, "Usage: %s <package_name>\n", argv[0]);
        return EXIT_FAILURE;
    }

    const char *package_name = argv[1];

    // 创建并初始化 DnfSack 对象
    sack = dnf_sack_new();
    if (!sack) {
        fprintf(stderr, "Failed to create DnfSack\n");
        return EXIT_FAILURE;
    }

    // 设置缓存目录
    // dnf_sack_set_cachedir(sack, "/var/cache/dnf");

    // 载入系统仓库
    if (!dnf_sack_load_system_repo(sack, NULL, 0, NULL)) {
        fprintf(stderr, "Failed to load system repository\n");
        g_object_unref(sack);
        return EXIT_FAILURE;
    }

    // 创建 HyQuery 对象
    query = hy_query_create(sack);
    if (!query) {
        fprintf(stderr, "Failed to create query\n");
        g_object_unref(sack);
        return EXIT_FAILURE;
    }

    // 筛选提供指定名称的软件包
    hy_query_filter_provides(query, HY_EQ, package_name, NULL);

    // 过滤出最新版本的软件包（不考虑架构）
    hy_query_filter_latest(query, 1);

    // 运行查询
    packages = hy_query_run(query);
    if (!packages) {
        fprintf(stderr, "Failed to run query\n");
        hy_query_free(query);
        g_object_unref(sack);
        return EXIT_FAILURE;
    }

    // 打印查询结果
    for (guint i = 0; i < packages->len; i++) {
        DnfPackage *pkg = g_ptr_array_index(packages, i);
        printf("%s-%s-%s.%s\n",
               dnf_package_get_name(pkg),
               dnf_package_get_version(pkg),
               dnf_package_get_release(pkg),
               dnf_package_get_arch(pkg));
    }

    // 清理资源
    g_ptr_array_unref(packages);
    hy_query_free(query);
    g_object_unref(sack);

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_hy_query_filter_latest test_hy_query_filter_latest.c `pkg-config --cflags --libs libdnf gobject-2.0`")

    def test(self):
        self.cmd("./test_hy_query_filter_latest make")
        self.cmd("./test_hy_query_filter_latest bash")
        self.cmd("./test_hy_query_filter_latest gcc")
        self.cmd("./test_hy_query_filter_latest libdnf")
        self.cmd("./test_hy_query_filter_latest python3")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_hy_query_filter_latest', 'test_hy_query_filter_latest.c']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")
