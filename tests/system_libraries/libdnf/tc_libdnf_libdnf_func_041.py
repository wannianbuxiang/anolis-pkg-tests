#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libdnf_libdnf_func_041.py
@Time:      2024/03/26 14:08:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libdnf_libdnf_func_041.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libdnf libdnf-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_dnf_package_cmp.c << EOF
#include <stdio.h>
#include <glib.h>
#include <libdnf/libdnf.h>

DnfPackage *get_package(DnfSack *sack, const char *package_name) {
    HyQuery query = hy_query_create(sack);
    hy_query_filter(query, HY_PKG_NAME, HY_EQ, package_name);
    GPtrArray *packages = hy_query_run(query);
    DnfPackage *pkg = NULL;
    
    if (packages->len > 0) {
        // Just pick the first package found with the given name
        pkg = g_object_ref(g_ptr_array_index(packages, 0));
    } else {
        fprintf(stderr, "No package found with name '%s'.\n", package_name);
    }

    hy_query_free(query);
    g_ptr_array_unref(packages);

    return pkg;
}

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <package1-name> <package2-name>\n", argv[0]);
        return 1;
    }

    const char *package1_name = argv[1];
    const char *package2_name = argv[2];

    g_autoptr(GError) error = NULL;
    g_autoptr(DnfSack) sack = dnf_sack_new();

    // Load system repository
    if (!dnf_sack_load_system_repo(sack, NULL, 0, &error)) {
        fprintf(stderr, "Error loading system repo: %s\n", error->message);
        return 1;
    }

    DnfPackage *pkg1 = get_package(sack, package1_name);
    if (!pkg1) {
        return 1; // Package not found, exit with error
    }

    DnfPackage *pkg2 = get_package(sack, package2_name);
    if (!pkg2) {
        g_object_unref(pkg1);
        return 1; // Package not found, exit with error
    }

    int cmp_result = dnf_package_cmp(pkg1, pkg2);
    if (cmp_result < 0) {
        printf("'%s' is less than '%s'\n", package1_name, package2_name);
    } else if (cmp_result > 0) {
        printf("'%s' is greater than '%s'\n", package1_name, package2_name);
    } else {
        printf("'%s' is equal to '%s'\n", package1_name, package2_name);
    }

    // Clean up
    g_object_unref(pkg1);
    g_object_unref(pkg2);

    return 0;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_dnf_package_cmp test_dnf_package_cmp.c `pkg-config --cflags --libs libdnf gobject-2.0`")

    def test(self):
        self.cmd("./test_dnf_package_cmp make bash")
        self.cmd("./test_dnf_package_cmp bash bash")
        self.cmd("./test_dnf_package_cmp libdnf python3")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_dnf_package_cmp', 'test_dnf_package_cmp.c']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")
