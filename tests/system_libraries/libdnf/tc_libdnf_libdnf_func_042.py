#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libdnf_libdnf_func_042.py
@Time:      2024/03/26 14:08:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libdnf_libdnf_func_042.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libdnf libdnf-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_dnf_reldep_get_info.c << EOF
#include <stdio.h>
#include <glib.h>
#include <libdnf/libdnf.h>

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <package-name>\n", argv[0]);
        return 1;
    }
    const char *package_name = argv[1];

    g_autoptr(GError) error = NULL;
    g_autoptr(DnfSack) sack = dnf_sack_new();

    // Load system repository
    if (!dnf_sack_load_system_repo(sack, NULL, 0, &error)) {
        fprintf(stderr, "Error loading system repo: %s\n", error->message);
        return 1;
    }

    // Create a query to search for the provided package name
    HyQuery query = hy_query_create(sack);
    hy_query_filter(query, HY_PKG_NAME, HY_EQ, package_name);

    // Execute the query and get the list of packages that match
    GPtrArray *pkgs = hy_query_run(query);
    if (pkgs->len == 0) {
        fprintf(stderr, "No package named '%s' found.\n", package_name);
        g_ptr_array_unref(pkgs);
        hy_query_free(query);
        return 1;
    }

    // Get the first package that matches the query (assuming one version per name)
    DnfPackage *pkg = g_ptr_array_index(pkgs, 0);

    // Get the list of pre-dependencies for the package
    DnfReldepList *reldeps_pre = dnf_package_get_requires_pre(pkg);
    for (guint i = 0; i < dnf_reldep_list_count(reldeps_pre); ++i) {
        DnfReldep *reldep = dnf_reldep_list_index(reldeps_pre, i);
        const char *dep_name = dnf_reldep_get_name(reldep);
        Id reldep_id = dnf_reldep_get_id(reldep);
        // const char *relation = dnf_reldep_get_relation(reldep);
        // const char *version = dnf_reldep_get_version(reldep);
        printf("===Pre-dependency name: %s ;Pre-dependency ID: %d\n", dep_name, reldep_id);
    }

    // Cleanup
    // g_object_unref(reldeps_pre);
    // g_ptr_array_unref(pkgs);
    hy_query_free(query);

    return 0;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_dnf_reldep_get_info test_dnf_reldep_get_info.c `pkg-config --cflags --libs libdnf gobject-2.0`")

    def test(self):
        self.cmd("./test_dnf_reldep_get_info bash")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_dnf_reldep_get_info', 'test_dnf_reldep_get_info.c']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")
