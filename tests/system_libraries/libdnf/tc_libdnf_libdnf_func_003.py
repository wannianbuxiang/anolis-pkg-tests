#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libdnf_libdnf_func_003.py
@Time:      2024/03/26 14:08:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libdnf_libdnf_func_003.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libdnf libdnf-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        gen_dnf_conf = r'''cat <<EOF > dnf003.conf
[main]
repo_dir=/etc/yum.repos.d
EOF'''
        code = r'''cat > test_dnf_context_get_repo_dir.c << EOF
#include <libdnf/dnf-context.h>
#include <stdio.h>
#include <glib.h>

int main(int argc, char **argv) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <DNF-CONFIG-FILE-PATH>\n", argv[0]);
        return 1;
    }

    // 创建KeyFile对象以读取配置文件
    GKeyFile *config_file = g_key_file_new();
    GError *error = NULL;
    gboolean key_file_load_success = g_key_file_load_from_file(config_file, argv[1], G_KEY_FILE_NONE, &error);
    if (!key_file_load_success) {
        fprintf(stderr, "Failed to load config file: %s\n", error ? error->message : "Unknown error");
        if (error) g_error_free(error);
        g_key_file_free(config_file);
        return 1;
    }

    // 从配置文件中获取'repo_dir'信息
    gchar *get_repo_dir = g_key_file_get_string(config_file, "main", "repo_dir", &error);
    if (error != NULL) {
        fprintf(stderr, "Failed to get 'repo_dir' from config file: %s\n", error->message);
        g_error_free(error);
        g_key_file_free(config_file);
        return 1;
    }

    // 初始化 DnfContext
    DnfContext *context = dnf_context_new();
    if (!context) {
        fprintf(stderr, "Failed to create DnfContext.\n");
        return 1;
    }

    // 指定 DNF 配置文件路径
    dnf_context_set_config_file_path(argv[1]);
    dnf_context_set_solv_dir(context, "/var/dnf/solv");

    // 初始化 DnfContext，使其读取配置文件
    if (!dnf_context_setup(context, NULL, &error)) {
        fprintf(stderr, "Failed to setup DnfContext: %s\n", error ? error->message : "Unknown error");
        if (error) g_error_free(error);
        g_object_unref(context);
        return 1;
    }

    // 获取仓库目录
    const gchar *repo_dir = dnf_context_get_repo_dir(context);
    if (repo_dir == NULL) {
        dnf_context_set_repo_dir(context, get_repo_dir);
    }

    printf("Repository directory: %s\n", dnf_context_get_repo_dir(context));
    // 清理资源
    g_object_unref(context);
    return 0;
}       
EOF'''
        self.cmd(gen_dnf_conf)
        self.cmd(code)
        self.cmd("gcc -o test_dnf_context_get_repo_dir test_dnf_context_get_repo_dir.c `pkg-config --cflags --libs libdnf gobject-2.0`")

    def test(self):
        self.cmd("./test_dnf_context_get_repo_dir dnf003.conf")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_dnf_context_get_repo_dir', 'test_dnf_context_get_repo_dir.c', 'dnf003.conf']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")
