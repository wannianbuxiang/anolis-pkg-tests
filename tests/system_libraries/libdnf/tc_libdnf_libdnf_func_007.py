#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libdnf_libdnf_func_007.py
@Time:      2024/03/26 14:08:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libdnf_libdnf_func_007.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libdnf libdnf-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        gen_dnf_conf = r'''cat <<EOF > dnf007.conf
[main]
install_root=/var/root
EOF'''
        code = r'''cat > test_dnf_context_get_install_root.c << EOF
#include <libdnf/libdnf.h>
#include <stdio.h>
#include <stdlib.h>
#include <glib.h>

int main(int argc, char **argv) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <DNF-CONFIG-FILE-PATH>\n", argv[0]);
        return 1;
    }

    // 创建 GKeyFile 对象以读取配置文件
    GKeyFile *key_file = g_key_file_new();
    GError *error = NULL;

    // 加载配置文件
    if (!g_key_file_load_from_file(key_file, argv[1], G_KEY_FILE_NONE, &error)) {
        fprintf(stderr, "Failed to load config file: %s\n", error ? error->message : "Unknown error");
        if (error) g_error_free(error);
        g_key_file_free(key_file);
        return 1;
    }

    // 从配置文件中获取 'install_root' 配置项
    gchar *install_root = g_key_file_get_string(key_file, "main", "install_root", &error);
    if (error != NULL) {
        fprintf(stderr, "Failed to get 'install_root' from config file: %s\n", error->message);
        g_error_free(error);
        g_key_file_free(key_file);
        return 1;
    }

    // 初始化 DnfContext
    DnfContext *context = dnf_context_new();
    if (!context) {
        fprintf(stderr, "Failed to create DnfContext.\n");
        g_free(install_root);
        g_key_file_free(key_file);
        return 1;
    }

    // 获取并打印安装根目录信息
    const gchar *obtained_install_root = dnf_context_get_install_root(context);
    if (obtained_install_root == NULL) {
        dnf_context_set_install_root(context, install_root);
    }
    printf("Install root: %s\n", dnf_context_get_install_root(context));
    // 清理资源
    g_object_unref(context);
    g_free(install_root);
    g_key_file_free(key_file);
    return 0;
}       
EOF'''
        self.cmd(gen_dnf_conf)
        self.cmd(code)
        self.cmd("gcc -o test_dnf_context_get_install_root test_dnf_context_get_install_root.c `pkg-config --cflags --libs libdnf gobject-2.0`")

    def test(self):
        self.cmd("./test_dnf_context_get_install_root dnf007.conf")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_dnf_context_get_install_root', 'test_dnf_context_get_install_root.c', 'dnf007.conf']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")
