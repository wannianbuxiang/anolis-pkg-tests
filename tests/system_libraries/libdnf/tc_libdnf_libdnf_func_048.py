#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libdnf_libdnf_func_048.py
@Time:      2024/03/26 14:08:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libdnf_libdnf_func_048.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libdnf libdnf-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        ret_c, self.repo_id = self.cmd("yum repolist|awk 'NR>1{print $1}'", ignore_status=True)
        self.assertEqual(0, ret_c, "Failed to get repo id")
        code = r'''cat > test_dnf_repo_get_cost.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <libdnf/libdnf.h>

int main(int argc, char *argv[]) {
    DnfContext *context;
    DnfRepo *repo;
    GError *error = NULL;

    if (argc != 2) {
        fprintf(stderr, "Usage: %s <repo_id>\n", argv[0]);
        return EXIT_FAILURE;
    }

    const char *repo_id = argv[1];

    // Initialize the context object
    context = dnf_context_new();
    if (!context) {
        fprintf(stderr, "Failed to create DnfContext\n");
        return EXIT_FAILURE;
    }

    dnf_context_set_solv_dir(context, "/var/dnf/solv");
    // Load the repositories
    if (!dnf_context_setup(context, NULL, &error)) {
        fprintf(stderr, "Error setting up context: %s\n", error->message);
        g_clear_error(&error);
        g_object_unref(context);
        return EXIT_FAILURE;
    }

    // Get the repo loader
    DnfRepoLoader *repo_loader = dnf_context_get_repo_loader(context);
    if (!repo_loader) {
        fprintf(stderr, "Failed to get DnfRepoLoader\n");
        g_object_unref(context);
        return EXIT_FAILURE;
    }

    // Find the repo with the specified ID
    repo = dnf_repo_loader_get_repo_by_id(repo_loader, repo_id, &error);
    if (!repo) {
        fprintf(stderr, "Repository %s not found: %s\n", repo_id, error->message);
        g_clear_error(&error);
        g_object_unref(repo_loader);
        g_object_unref(context);
        return EXIT_FAILURE;
    }

    // Get the cost of the repository
    int cost = dnf_repo_get_cost(repo);

    // Print the cost
    printf("The cost of repository '%s' is %d\n", repo_id, cost);

    // Cleanup
    // g_object_unref(repo_loader);
    // g_object_unref(context);
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_dnf_repo_get_cost test_dnf_repo_get_cost.c `pkg-config --cflags --libs libdnf gobject-2.0`")

    def test(self):
        for id in self.repo_id.split('\n'):
            self.cmd(f"./test_dnf_repo_get_cost {id}")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_dnf_repo_get_cost', 'test_dnf_repo_get_cost.c']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")
