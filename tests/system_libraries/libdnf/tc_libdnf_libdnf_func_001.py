#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libdnf_libdnf_func_001.py
@Time:      2024/03/26 14:08:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libdnf_libdnf_func_001.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libdnf libdnf-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_libdnf_dnf_context_get_repos.c << EOF
#include <libdnf/dnf-context.h>
#include <libdnf/dnf-repo.h>
#include <stdio.h>
#include <glib.h>

int main() {
    GError *error = NULL;
    DnfContext *context;
    GPtrArray *repos;
    DnfRepo *repo;
    guint i;

    // 初始化 DnfContext
    context = dnf_context_new();
    // Set the solv_dir for the context; adjust the path as needed
    dnf_context_set_solv_dir(context, "/var/cache/dnf/solv");
    if (error) {
        fprintf(stderr, "Error setting solv_dir: %s\n", error->message);
        g_error_free(error);
        g_object_unref(context);
        return EXIT_FAILURE;
    }

    if (!dnf_context_setup(context, NULL, &error)) {
        // 错误处理
        fprintf(stderr, "Error setting up DNF context: %s\n", error->message);
        g_clear_error(&error);
        g_object_unref(context);
        return 1;
    }
    // 加载仓库
    if (!dnf_context_get_repos(context)) {
        // 错误处理
        fprintf(stderr, "Error loading repositories: %s\n", error->message);
        g_clear_error(&error);
        g_object_unref(context);
        return 1;
    }
    // 获取仓库列表
    repos = dnf_context_get_repos(context);
    // 遍历并打印仓库信息
    for (i = 0; i < repos->len; i++) {
        repo = g_ptr_array_index(repos, i);
        printf("dnf_repo_get_id: %s\n", dnf_repo_get_id(repo));
        printf("dnf_repo_get_location: %s\n", dnf_repo_get_location(repo));
        printf("dnf_repo_get_filename: %s\n", dnf_repo_get_filename(repo));
        printf("dnf_repo_get_packages: %s\n", dnf_repo_get_packages(repo));
        printf("dnf_repo_get_description: %s\n", dnf_repo_get_description(repo));
        printf("\n\n");
    }
    // 清理资源
    g_object_unref(context);
    return 0;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_libdnf_dnf_context_get_repos test_libdnf_dnf_context_get_repos.c `pkg-config --cflags --libs libdnf glib-2.0 gobject-2.0`")

    def test(self):
        self.cmd("./test_libdnf_dnf_context_get_repos")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_libdnf_dnf_context_get_repos', 'test_libdnf_dnf_context_get_repos.c']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")
