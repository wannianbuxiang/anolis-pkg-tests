#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libdnf_libdnf_func_009.py
@Time:      2024/03/26 14:08:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libdnf_libdnf_func_009.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libdnf libdnf-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        gen_dnf_conf = r'''cat <<EOF > dnf009.conf
[main]
# 缓存有效期设置（以秒为单位）
metadata_expire=3600
EOF'''
        code = r'''cat > test_dnf_context_get_cache_age.c << EOF
#include <libdnf/libdnf.h>
#include <glib.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <DNF-CONFIG-FILE-PATH>\n", argv[0]);
        return 1;
    }

    // 创建 GKeyFile 对象以读取配置文件
    GKeyFile *key_file = g_key_file_new();
    GError *error = NULL;

    // 加载配置文件
    if (!g_key_file_load_from_file(key_file, argv[1], G_KEY_FILE_NONE, &error)) {
        fprintf(stderr, "Failed to load config file: %s\n", error ? error->message : "Unknown error");
        if (error) g_error_free(error);
        g_key_file_free(key_file);
        return 1;
    }

    // 从配置文件中获取 'metadata_expire' 配置项
    guint cache_age = g_key_file_get_uint64(key_file, "main", "metadata_expire", &error);
    if (error != NULL) {
        fprintf(stderr, "Failed to get 'metadata_expire' from config file: %s\n", error->message);
        g_error_free(error);
        g_key_file_free(key_file);
        return 1;
    }

    // 初始化 DnfContext
    DnfContext *context = dnf_context_new();
    if (!context) {
        fprintf(stderr, "Failed to create DnfContext.\n");
        g_key_file_free(key_file);
        return 1;
    }

    // 获取并打印缓存有效期信息
    guint obtained_cache_age = dnf_context_get_cache_age(context);
    if (!obtained_cache_age){
        // 设置缓存有效期到DnfContext
        dnf_context_set_cache_age(context, cache_age);
    }

    printf("Cache age: %u seconds\n", dnf_context_get_cache_age(context));
    // 清理资源
    g_object_unref(context);
    g_key_file_free(key_file);
    return 0;
}       
EOF'''
        self.cmd(gen_dnf_conf)
        self.cmd(code)
        self.cmd("gcc -o test_dnf_context_get_cache_age test_dnf_context_get_cache_age.c `pkg-config --cflags --libs libdnf gobject-2.0`")

    def test(self):
        self.cmd("./test_dnf_context_get_cache_age dnf009.conf")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_dnf_context_get_cache_age', 'test_dnf_context_get_cache_age.c', 'dnf009.conf']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")
