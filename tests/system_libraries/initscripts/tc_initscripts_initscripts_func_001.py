#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_initscripts_initscripts_func_001.py
@Time:      2024/03/22 16:05:00
@Author:    smj01095381
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    smj01095381
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_initscripts_initscripts_func_001.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "initscripts initscripts-service systemd"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("systemd-run --unit=init_ser_test /bin/sh -c 'while true; do echo Service is running; sleep 5; done'")

    def test(self):
        self.cmd('service init_ser_test status')
        self.cmd('service init_ser_test start')
        self.cmd('service init_ser_test restart')
        self.cmd('service init_ser_test try-restart')
        self.cmd('service init_ser_test force-reload')
        self.cmd('service init_ser_test reload-or-restart')
           
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('service init_ser_test stop')
        # self.cmd('systemctl disable init_ser_test', ignore_status=True)
        # self.cmd('systemctl reset-failed init_ser_test', ignore_status=True)
        self.cmd('systemctl daemon-reload', ignore_status=True)
