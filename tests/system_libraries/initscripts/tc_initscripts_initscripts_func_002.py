#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_initscripts_initscripts_func_002.py
@Time:      2024/03/22 16:05:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_initscripts_initscripts_func_002.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "initscripts initscripts-service systemd"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        gen_ser_conf = r'''cat << EOF > /etc/systemd/system/init_ser_test_002.service
[Unit]
Description=My Custom Service
After=network.target

[Service]
Type=forking
ExecStart=/etc/init.d/init_ser_test_002 start
ExecStop=/etc/init.d/init_ser_test_002 stop
ExecReload=/etc/init.d/init_ser_test_002 restart
PIDFile=/var/run/init_ser_test_002.pid

[Install]
WantedBy=multi-user.target   
EOF'''
        gen_ser_scripts = r'''cat << "EOF" > /etc/init.d/init_ser_test_002
#!/bin/bash

PIDFILE="/var/run/init_ser_test_002.pid"

start() {
    echo "Starting init_ser_test_002 service..."
    nohup watch -n 5 date > /tmp/init_ser_test_002.log 2>&1 &
    echo $! > "$PIDFILE"
}

stop() {
    if [ -f "$PIDFILE" ]; then
        PID=$(cat "$PIDFILE")
        echo "Stopping init_ser_test_002 service..."
        kill "$PID"
        rm "$PIDFILE"
    else
        echo "PID file not found. Is the service running?"
    fi
}

status() {
    if [ -f "$PIDFILE" ]; then
        PID=$(cat "$PIDFILE")
        if ps -p "$PID" > /dev/null; then
           echo "Service is running (PID: $PID)"
        else
           echo "Service not running but pid file exists"
        fi
    else
        echo "Service is not running."
    fi
}

case "$1" in 
    start)
        start
        ;;
    stop)
        stop
        ;;
    restart)
        stop
        start
        ;;
    status)
        status
        ;;
    *)
        echo "Usage: $0 {start|stop|restart|status}"
esac    
EOF'''
        self.cmd(gen_ser_conf)
        self.cmd(gen_ser_scripts)
        self.cmd('mkdir -p /var/run/')
        self.cmd('chmod +x /etc/init.d/init_ser_test_002')

    def test(self):
        self.cmd('service init_ser_test_002 status')
        self.cmd('service init_ser_test_002 start')
        self.cmd('service init_ser_test_002 restart')
        self.cmd('service init_ser_test_002 stop')

           
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('systemctl daemon-reload', ignore_status=True)
        for file in ['/etc/systemd/system/init_ser_test_002.service', '/etc/init.d/init_ser_test_002', '/var/run/init_ser_test_002.pid', '/tmp/init_ser_test_002.log']:
            if os.path.exists(file):
                self.cmd(f'rm -rf {file}')
