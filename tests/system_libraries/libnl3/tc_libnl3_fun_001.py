#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libnl3_fun_001.py
@Time:      2024/04/24 15:32:50
@Author:    hongfeng
@Version:   1.0
@Contact:   wb-zhf668260@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    hongfeng
"""

from common.basetest import LocalTest
import re
class Test(LocalTest):
    """
    See tc_libnl3_fun_001.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libnl3-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > test01.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include <netlink/netlink.h>
#include <netlink/route/link.h>

#define NL_BUF_SIZE 8192

static int print_link_info(struct nl_object *obj, void *arg)
{
    struct rtnl_link *link = (struct rtnl_link *) obj;

    printf("Interface Name: %s\\n", rtnl_link_get_name(link));

    return NL_OK; 
}

int main(void)
{
    struct nl_sock *sock;
    struct nl_cache *link_cache;
    int err;

    /* Step 1: 初始化 Netlink 套接字 */
    sock = nl_socket_alloc();
    if (!sock) {
        fprintf(stderr, "Failed to allocate Netlink socket\\n");
        return EXIT_FAILURE;
    }

    /* 设置消息缓冲区大小 */
    nl_socket_set_buffer_size(sock, NL_BUF_SIZE, NL_BUF_SIZE);

    /* 连接到 Netlink 路由子系统 */
    err = nl_connect(sock, NETLINK_ROUTE);
    if (err < 0) {
        fprintf(stderr, "Failed to connect to Netlink: %s\\n", nl_geterror(err));
        nl_socket_free(sock);
        return EXIT_FAILURE;
    }

    /* Step 2: 获取网络接口缓存 */
    err = rtnl_link_alloc_cache(sock, AF_UNSPEC, &link_cache);
    if (err < 0) {
        fprintf(stderr, "Failed to allocate link cache: %s\\n", nl_geterror(err));
        nl_socket_free(sock);
        return EXIT_FAILURE;
    }

    /* Step 3: 遍历并打印网络接口信息 */
    nl_cache_foreach(link_cache, print_link_info, NULL);

    /* Step 4: 清理资源 */
    nl_cache_free(link_cache);
    nl_socket_free(sock);

    return EXIT_SUCCESS;
}
EOF"""
        self.cmd(cmdline)
        self.cmd("gcc -o test01 test01.c `pkg-config --cflags --libs libnl-3.0 libnl-route-3.0`")

    def test(self):
        # 验证执行成功
        ret_c, ret_o = self.cmd("./test01")
        interfaces = re.findall(r'Interface Name: (\w+)', ret_o)
        # 验证个数一致
        command = "ifconfig -s | awk '{print $1}' | grep -v Iface | wc -l"
        ret_c, ret_o = self.cmd(command)
        self.assertEqual(ret_o, str(len(interfaces)), "The number of interfaces %s is not equal to the number %s" % (ret_o, len(interfaces)))
        for interface in interfaces:
            print("Interface Name:", interface)
            # 用ifconfig验证Interface Name
            command = "ifconfig -s | awk '{print $1}' | grep -v Iface | grep " +  interface
            self.cmd(command)

        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm test01.c test01")
        
