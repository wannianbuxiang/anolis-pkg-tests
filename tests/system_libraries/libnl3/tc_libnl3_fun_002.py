#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libnl3_fun_002.py
@Time:      2024/04/24 15:32:50
@Author:    hongfeng
@Version:   1.0
@Contact:   wb-zhf668260@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    hongfeng
"""

from common.basetest import LocalTest
class Test(LocalTest):
    """
    See tc_libnl3_fun_002.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libnl3-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > test02.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <netlink/netlink.h>
#include <netlink/route/route.h>
#include <netlink/route/link.h>

#define DESTINATION "192.168.100.0/24"
#define GATEWAY "1.1.1.1"
#define INTERFACE "lo"

int main()
{
    struct nl_sock *sock;
    struct rtnl_route *route;
    struct rtnl_nexthop *nexthop;
    struct nl_addr *dst_addr, *gw_addr;
    int ret;
    int err;

    // 创建并连接 Netlink 套接字
    sock = nl_socket_alloc();
    if (!sock) {
        fprintf(stderr, "Failed to allocate Netlink socket\\n");
        return EXIT_FAILURE;
    }

    err = nl_connect(sock, NETLINK_ROUTE);
    if (err < 0) {
        fprintf(stderr, "Error connecting to netlink: %s\\n", nl_geterror(err));
        nl_socket_free(sock);
        return EXIT_FAILURE;
    }

    // 解析并设置目标地址
    nl_addr_parse(DESTINATION, AF_INET, &dst_addr);

    // 创建路由对象
    route = rtnl_route_alloc();
    if (!route) {
        fprintf(stderr, "Failed to allocate route object\\n");
        nl_addr_put(dst_addr);
        nl_socket_free(sock);
        return EXIT_FAILURE;
    }

    // 设置路由属性
    rtnl_route_set_family(route, AF_INET);
    rtnl_route_set_table(route, RT_TABLE_MAIN);
    rtnl_route_set_type(route, RTN_UNICAST);
    rtnl_route_set_protocol(route, RTPROT_STATIC);
    rtnl_route_set_scope(route, RT_SCOPE_UNIVERSE);
    rtnl_route_set_dst(route, dst_addr);

    // 解析并设置下一跳地址
    nl_addr_parse(GATEWAY, AF_INET, &gw_addr);

    // 创建下一跳对象
    nexthop = rtnl_route_nh_alloc();
    if (!nexthop) {
        fprintf(stderr, "Failed to allocate next hop object\\n");
        nl_addr_put(dst_addr);
        nl_addr_put(gw_addr);
        nl_socket_free(sock);
        return EXIT_FAILURE;
    }

    // 设置下一跳属性
    rtnl_route_nh_set_gateway(nexthop, gw_addr);

    // 获取接口索引
    int ifindex = rtnl_link_name2i(sock, INTERFACE);
    if (ifindex < 0) {
        fprintf(stderr, "Failed to find interface index for '%s'\\n", INTERFACE);
        nl_addr_put(dst_addr);
        nl_addr_put(gw_addr);
        nl_socket_free(sock);
        return EXIT_FAILURE;
    }

    // 设置下一跳接口
    rtnl_route_nh_set_ifindex(nexthop, ifindex);

    // 将下一跳添加到路由
    rtnl_route_add_nexthop(route, nexthop);

    // 添加路由
    ret = rtnl_route_add(sock, route, NLM_F_CREATE | NLM_F_REPLACE);
    if (ret < 0) {
        fprintf(stderr, "Failed to add route: %s\\n", nl_geterror(ret));
    } else {
        printf("Route added successfully\\n");
    }

    // 清理资源
    nl_addr_put(dst_addr);
    nl_addr_put(gw_addr);
    rtnl_route_put(route);
    nl_close(sock);
    nl_socket_free(sock);

    return ret < 0 ? EXIT_FAILURE : EXIT_SUCCESS;
}
EOF"""
        self.cmd(cmdline)
        self.cmd("gcc -o test02 test02.c `pkg-config --cflags --libs libnl-3.0 libnl-route-3.0`")

    def test(self):
        self.cmd("ip addr add 1.1.1.1/32 dev lo")
        # 验证执行成功，路由表增加成功
        ret_c, ret_o = self.cmd("./test02")
        self.assertTrue("Route added successfully" in ret_o, 'check output error.')
        ret_c, ret_o = self.cmd("route")
        self.assertTrue("192.168.100.0" in ret_o, 'check output error.')
        
    def tearDown(self):
        self.cmd("ip route del 192.168.100.0/24 via 1.1.1.1 dev lo")
        self.cmd("ip addr del 1.1.1.1/32 dev lo")
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm test02.c test02")
        

