#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libnl3_fun_003.py
@Time:      2024/04/24 15:32:50
@Author:    hongfeng
@Version:   1.0
@Contact:   wb-zhf668260@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    hongfeng
"""

from common.basetest import LocalTest
class Test(LocalTest):
    """
    See tc_libnl3_fun_003.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libnl3-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > test03.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include <netlink/netlink.h>
#include <linux/rtnetlink.h>
#include <netlink/genl/genl.h>
#include <netlink/genl/ctrl.h>
 
int main() {
    struct nl_sock *socket;
    struct nl_msg *msg;
    int retval = -1;

    // 初始化 Netlink socket
    socket = nl_socket_alloc();
    if (!socket) {
        printf("Failed to allocate netlink socket.\\n");
        return -1;
    }

    // 连接到 Netlink socket
    retval = nl_connect(socket, NETLINK_GENERIC);
    if (retval < 0) {
        printf("Failed to connect to netlink socket: %s\\n", nl_geterror(retval));
        nl_socket_free(socket);
        return -1;
    }

    // 创建 Netlink 消息
    msg = nlmsg_alloc();
    if (!msg) {
        printf("Failed to allocate netlink message.\\n");
        nl_socket_free(socket);
        return -1;
    }
    
    // 发送查询消息并等待结果
    struct nlmsghdr *request;
    request = nlmsg_alloc();
    if (!request) {
        printf("Failed to allocate memory for the query message.\\n");
        goto cleanup;
    }
    
    // 设置query消息头部信息
    memset(request, 0, NLMSG_SPACE(GENL_HDRLEN));
    request->nlmsg_len = GENL_HDRLEN + sizeof(struct rtgenmsg);
    request->nlmsg_type = RTM_GETROUTE;
    request->nlmsg_flags = NLM_F_REQUEST | NLM_F_DUMP;
    request->nlmsg_seq = 123456789;
    request->nlmsg_pid = getpid();
    
    // 将query消息发送到内核
    retval = nl_send_auto(socket, request);
    if (retval < 0) {
        printf("Failed to send the query message to kernel.\\n");
        goto cleanup;
    }
    printf("Message sent to kernel successfully.\\n");
    
    // 处理返回的结果
    while ((retval = nl_recvmsgs_default(socket)) > 0) {
        struct nlmsghdr *response;
        
        response = nlmsg_next(request, &retval);
        printf("response: %s\\n", response);
        do {
            switch (response->nlmsg_type) {
                case NLMSG_DONE:
                    break;
                default:
                    printf("Received an unexpected message type %d\\n", response->nlmsg_type);
                    break;
            }
            
            response = nlmsg_next(response, &retval);
        } while (response && retval >= 0);
    }
    
cleanup:
    // 释放资源
    nl_close(socket);
    nl_socket_free(socket);
    
    return EXIT_SUCCESS;
}
EOF"""
        self.cmd(cmdline)
        self.cmd("gcc -o test03 test03.c `pkg-config --cflags --libs libnl-3.0 libnl-route-3.0`")

    def test(self):
        ret_c, ret_o = self.cmd("./test03")
        self.assertTrue("Message sent to kernel successfully" in ret_o, 'check output error.')
     
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm test03.c test03")
        

