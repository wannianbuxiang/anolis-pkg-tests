#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libcap-ng_fun_002.py
@Time:      2024/04/18 13:32:50
@Author:    hongfeng
@Version:   1.0
@Contact:   wb-zhf668260@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    hongfeng
"""

from common.basetest import LocalTest
class Test(LocalTest):
    """
    See tc_libcap-ng_fun_002.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libcap-ng-devel libcap-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > test02.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include <cap-ng.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <errno.h>
#include <sys/capability.h>

#define TEST_CAP CAP_NET_BIND_SERVICE

// 尝试在端口上绑定网络服务
int test_bind(int port) {
    const int TEST_PORT = port;
    struct sockaddr_in server_addr;
    int listen_sock = socket(AF_INET, SOCK_STREAM, 0);
    if (listen_sock == -1) {
        perror("Failed to create socket");
        return -1;
    }

    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(TEST_PORT);
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(listen_sock, (struct sockaddr *)&server_addr, sizeof(server_addr)) == -1) {
        printf("Bind to port %d failed.\\n", TEST_PORT);
        fflush(stdout);
        close(listen_sock);
        return -1;
    } else {
        printf("Bind to port %d successfully.\\n", TEST_PORT);
        fflush(stdout);
        close(listen_sock);
        return 0;
    }
}

int main() {
    pid_t pid_child;
    int ret;
    int has_cap;

    // 创建一个子进程
    pid_child = fork();
    if (pid_child == 0) {
        // 清理 CAP_BIND 能力
        capng_clear(CAPNG_SELECT_BOTH);
           
        // DROP掉CAP_NET_BIND_SERVICE
        if ((ret = capng_update(CAPNG_DROP, CAPNG_EFFECTIVE | CAPNG_PERMITTED , TEST_CAP)) != 0) {
            fprintf(stderr, "Failed to drop network capabilities: %d\\n", ret);
            return EXIT_FAILURE;
        }
        if (capng_apply(CAPNG_SELECT_BOTH)) {
            int errnum = errno;
            const char *errmsg = strerror(errnum);
            fprintf(stderr, "Failed to apply capability changes to the current process1: %s (errno %d)\\n", errmsg, errnum);
        }
        test_bind(400);
        return 0;
    }
    
    // 主进程
    // 清理 CAP_BIND 能力
    capng_clear(CAPNG_SELECT_BOTH);

    // ADD CAP_NET_BIND_SERVICE
    if ((ret = capng_update(CAPNG_ADD, CAPNG_EFFECTIVE | CAPNG_PERMITTED , TEST_CAP)) != 0) {
        fprintf(stderr, "Failed to add capability to effective set\\n");
        return EXIT_FAILURE;
    }

    // 应用设置到当前进程
    if (capng_apply(CAPNG_SELECT_BOTH)) {
        int errnum = errno;
        const char *errmsg = strerror(errnum);
        fprintf(stderr, "Failed to apply capability changes to the current process2: %s (errno %d)\\n", errmsg, errnum);
    }
    
    // 验证进程是否已成功获取TEST_CAP能力
    capng_results_t result_type;
    has_cap = capng_have_capability(CAPNG_EFFECTIVE, TEST_CAP);
    if (!has_cap) {
        fprintf(stderr, "Failed to verify capability: not present in effective set\\n");
        return EXIT_FAILURE;
    }

    printf("Successfully set and verified capability %s for parent process.\\n", capng_capability_to_name(TEST_CAP));

    // 可在此处进行实际需要使用该能力的操作，例如尝试在低于1024的端口上绑定网络服务
    test_bind(401);

    int status;
    wait(&status);

    return EXIT_SUCCESS;
}
EOF"""
        self.cmd(cmdline)
        self.cmd("gcc -o test02 test02.c -lcap-ng")

    def test(self):
        ret_c, ret_o = self.cmd("./test02")
        # 验证在子进程上的端口bind失败
        self.assertTrue("Bind to port 400 failed." in ret_o, 'check output error.')
        # 验证在主进程上的端口bind成功
        self.assertTrue("Bind to port 401 successfully." in ret_o, 'check output error.')
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm test02.c test02")
        

