#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libcap-ng_fun_001.py
@Time:      2024/04/18 13:32:50
@Author:    hongfeng
@Version:   1.0
@Contact:   wb-zhf668260@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    hongfeng
"""

from common.basetest import LocalTest
class Test(LocalTest):
    """
    See tc_libcap-ng_fun_001.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libcap-ng-devel libcap-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > test01.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include <cap-ng.h>
#include <string.h>
#include <errno.h>
#include <sys/capability.h>

int main() {
    if (capng_have_capability(CAPNG_EFFECTIVE, CAP_SETUID | CAP_SETGID) == CAPNG_NONE) {
        printf("Process does not have effective CAP_SETUID and/or CAP_SETGID.\\n");
    } else {
        printf("Process has effective CAP_SETUID and/or CAP_SETGID.\\n");
    }

    if (setuid(0)) {
        printf("insufficient privilege to setuid.\\n");
    }
    if (setgid(0)) {
        printf("insufficient privilege to setgid.\\n");
    }

    capng_update(CAPNG_ADD, CAPNG_EFFECTIVE | CAPNG_PERMITTED, CAP_SETUID | CAP_SETGID);
    if (capng_apply(CAPNG_SELECT_BOTH) != 0) {
        perror("capng_apply() failed");
    }
    
    if (capng_have_capability(CAPNG_EFFECTIVE, CAP_SETUID | CAP_SETGID) == CAPNG_NONE) {
        printf("Process does not have effective CAP_SETUID and/or CAP_SETGID.\\n");
    } else {
        printf("Process has effective CAP_SETUID and/or CAP_SETGID.\\n");
    }

    if (setuid(0)) {
        printf("insufficient privilege to setuid.\\n");
    }
    if (setgid(0)) {
        printf("insufficient privilege to setgid.\\n");
    }
}
EOF"""
        self.cmd(cmdline)
        self.cmd("gcc -o test01 test01.c -lcap-ng")
        self.cmd("useradd test-user01")

    def test(self):
        # 验证root用户有权限
        ret_c, ret_o = self.cmd("./test01")
        self.assertTrue("Process has effective CAP_SETUID and/or CAP_SETGID" in ret_o, 'check output error.')

        # 验证test-user01用户无权限
        ret_c, ret_o = self.cmd("su test-user01 -c './test01'")
        self.assertTrue("insufficient privilege to setuid" in ret_o, 'check output error.')
        self.assertTrue("insufficient privilege to setgid" in ret_o, 'check output error.')
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("userdel -rf test-user01")
        self.cmd("rm test01.c test01")
        
