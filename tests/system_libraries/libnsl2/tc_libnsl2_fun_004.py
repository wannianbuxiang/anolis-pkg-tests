# -*- encoding: utf-8 -*-

"""
@File:      tc_libpipeline_fun004.py
@Time:      2024-04-18 11:22:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
import time
import os
import subprocess
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libpipeline_fun004.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    server_context = r"""
#include <stdio.h>
#include <rpc/rpc.h>
#include <stdlib.h>
#include <rpc/pmap_clnt.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

#define PING_PROG 0x20000001
#define PING_VERS 1
#define PING_PROC 1

void ping_proc(struct svc_req *rqstp, register SVCXPRT *transp) {
    char *arg;
    static char *result = "Pong";

    // 获取参数
    if (!svc_getargs(transp, (xdrproc_t)xdr_wrapstring, (caddr_t)&arg)) {
        // 无法解码参数
        svcerr_decode(transp);
        return;
    }
    
    // 打印接收到的信息
    printf("Server received: %s\n", arg);
    fflush(stdout);

    // 回复客户端
    if (!svc_sendreply(transp, (xdrproc_t)xdr_wrapstring, &result)) {
        // 无法发送回复
        svcerr_systemerr(transp);
    }

}

int main() {
    register SVCXPRT *transp;
    int sockfd;
    struct sockaddr_in addr;
    int len = sizeof(addr);

    // 注销旧服务
    pmap_unset(PING_PROG, PING_VERS);

    // 创建 socket
    sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (sockfd < 0) {
        perror("socket creation failed");
        exit(1);
    }

    // 配置 socket 地址
    memset(&addr, 0, len);
    addr.sin_family = AF_INET;
    addr.sin_port = htons(62510);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);

    // 绑定 socket
    if (bind(sockfd, (struct sockaddr *)&addr, len) == -1) {
        perror("bind failed");
        close(sockfd);
        exit(1);
    }

    // 创建 RPC 服务端
    transp = svcudp_create(sockfd);
    if (transp == NULL) {
        fprintf(stderr, "cannot create udp service.\n");
        close(sockfd);
        exit(1);
    }

    // 注册 RPC 程序
    if (!svc_register(transp, PING_PROG, PING_VERS, ping_proc, IPPROTO_UDP)) {
        fprintf(stderr, "unable to register (PING_PROG, PING_VERS, udp).\n");
        close(sockfd);
        exit(1);
    }

    // 运行服务
    svc_run();
    fprintf(stderr, "svc_run returned\n");
    close(sockfd);
    exit(1);
}
"""
    client_context = r"""
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <rpc/rpc.h>

#define PING_PROG 0x20000001
#define PING_VERS 1
#define PING_PROC 1

int main(int argc, char *argv[]) {
    CLIENT *cl;
    char *server;
    char *arg = "Ping";
    char *result;

    if (argc != 2) {
        fprintf(stderr, "Usage: %s <hostname>\n", argv[0]);
        exit(1);
    }

    server = argv[1];

    // 创建客户端句柄使用 UDP 协议
    cl = clnt_create(server, PING_PROG, PING_VERS, "udp");
    if (cl == NULL) {
        clnt_pcreateerror(server);
        exit(1);
    }

    struct timeval timeout;
    timeout.tv_sec = 5;  // 设置超时时间为 5 秒
    timeout.tv_usec = 0;

    // 调用 RPC 函数
    if (clnt_call(cl, PING_PROC, (xdrproc_t)xdr_wrapstring, (caddr_t)&arg,
                  (xdrproc_t)xdr_wrapstring, (caddr_t)&result, timeout) != RPC_SUCCESS) {
        // 如果 RPC 调用失败，打印错误信息
        clnt_perror(cl, server);
    } 

    // 清理环境
    clnt_destroy(cl);
    return 0;
}
"""

    PARAM_DIC = {"pkg_name": "libnsl2 libnsl2-devel rpcbind"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.port = 62510
        self.message = "Ping!"
        self.server = "server.c"
        self.exec_server = "server"
        self.client = "client.c"
        self.exec_client = "client"
        server_cmdline = f"""cat > {self.server} <<EOF
        {self.server_context}
EOF
"""
        self.cmd(server_cmdline)
        client_cmdline = f"""cat > {self.client} <<EOF
        {self.client_context}
EOF
"""
        self.cmd(client_cmdline)

    def test(self):
        # 先编译服务端和客户端程序
        self.cmd(f"gcc -o {self.exec_server} {self.server} -I /usr/include/tirpc -ltirpc -lnsl")
        self.cmd(f"gcc -o {self.exec_client} {self.client} -I /usr/include/tirpc -ltirpc -lnsl")
        
        # 检查生成的程序是否为executable
        code, server_result = self.cmd(f"file {self.exec_server}")
        self.assertIn("executable", server_result)
        code, client_result = self.cmd(f"file {self.exec_client}")
        self.assertIn("executable", client_result)
        
        # 指定服务器日志文件名并打开该文件等待写入
        server_output_file = "server_output.log"
        server_log_file = open(server_output_file, "w+")
        
        # 启动RPC服务器，立即将其输出重定向到文件
        server_proc = subprocess.Popen(f"./{self.exec_server}",
                                       stdout=server_log_file,
                                       stderr=subprocess.STDOUT,
                                       shell=True)
        
        # 等待一段时间以确保服务器启动
        time.sleep(2)
        
        # 获取启动的服务器进程的PID
        self.server_pid = server_proc.pid
        self.log.info(f"Server started with PID: {self.server_pid}")
        
        # 运行客户端程序将发送请求到服务器
        client_proc = subprocess.run(f"./{self.exec_client} 127.0.0.1", shell=True)
        time.sleep(1)  # 留出时间让服务器处理请求和更新日志
        
        # 刷新和关闭文件以确保输出已经被写入
        server_log_file.flush()
        os.fsync(server_log_file.fileno())
        server_log_file.close()
        
        # 读取服务器日志文件查看预期的输出是否存在
        found = False
        with open(server_output_file, "r") as server_log_file:
            for line in server_log_file:
                if "Server received: Ping" in line:
                    found = True
                    break
        
        # 断言是否成功找到预期的输出
        self.assertTrue(found, "'Server received: Ping' not found in server log")
        
        # 杀掉服务器进程以结束测试
        server_proc.kill()
        
        # 在脚本结束后清理掉记录log的文件
        if os.path.exists(server_output_file):
            os.remove(server_output_file)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f"rm -rf {self.exec_server} {self.server} {self.exec_client} {self.client}")
        # 再次尝试关闭服务端程序
        self.cmd(f"kill -9 {self.server_pid}", ignore_status=True)
