# -*- encoding: utf-8 -*-

"""
@File:      tc_libpipeline_fun001.py
@Time:      2024-04-15 11:22:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libpipeline_fun001.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    file_context = r"""
#include <stdio.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h> // 包含inet_ntoa函数的头文件
#include <tirpc/rpc/rpc.h> // 如果没有tirpc, 使用 #include <rpc/rpc.h>

int main() {
    struct sockaddr_in my_address;
    
    // Initialize to zero before calling get_myaddress
    memset(&my_address, 0, sizeof(my_address));

    // Call the function
    int result = get_myaddress(&my_address);

    if (result == 0) {
        printf("get_myaddress call succeeded.\n");
        printf("My address is %s\n", inet_ntoa(my_address.sin_addr));
    } else {
        fprintf(stderr, "get_myaddress call failed.\n");
        return 1;
    }
    
    return 0;
}
"""

    PARAM_DIC = {"pkg_name": "libnsl2 libnsl2-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.c_file = "libnsl2_fun_001.c"
        self.exec_file = "libnsl2_fun_001"
        cmdline = f"""cat > {self.c_file } <<EOF
        {self.file_context}
EOF
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd(f"gcc -o {self.exec_file} {self.c_file} -I /usr/include/tirpc -ltirpc -lnsl")
        code, result = self.cmd(f"file {self.exec_file}")
        self.assertIn("executable", result)
        code, result = self.cmd(f"./{self.exec_file}")
        self.assertIn("get_myaddress call succeeded", result)
        self.assertIn("127.0.0.1", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f"rm -rf {self.c_file} {self.exec_file}")