# -*- encoding: utf-8 -*-

"""
@File:      tc_libpipeline_fun002.py
@Time:      2024-04-16 11:22:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libpipeline_fun002.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    file_context = r"""
#include <stdio.h>
#include <rpc/rpc.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    char *host;
    int prognum, versnum, protocol;

    if (argc != 5) {
        fprintf(stderr, "Usage: %s <host> <prognum> <versnum> <protocol>\n", argv[0]);
        exit(1);
    }

    host = argv[1];
    prognum = atoi(argv[2]);
    versnum = atoi(argv[3]);
    protocol = atoi(argv[4]); // IPPROTO_UDP or IPPROTO_TCP

    int port = getrpcport(host, prognum, versnum, protocol);
    if (port == 0) {
        fprintf(stderr, "Can't get port number for %s %d %d %d\n", host, prognum, versnum, protocol);
        exit(1);
    } else {
        printf("RPC server on host %s for prognum %d, versnum %d, protocol %d is listening on port %d\n",
               host, prognum, versnum, protocol, port);
    }

    return 0;
}
"""

    PARAM_DIC = {"pkg_name": "libnsl2 libnsl2-devel rpcbind"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.c_file = "libnsl2_fun_002.c"
        self.exec_file = "libnsl2_fun_002"
        cmdline = f"""cat > {self.c_file } <<EOF
        {self.file_context}
EOF
"""
        self.cmd(cmdline)

    def test(self):
        rpc_prognum = 100000
        versnum = 2
        protocol = 6
        IP = '127.0.0.1'
        self.cmd(f"gcc -o {self.exec_file} {self.c_file} -I /usr/include/tirpc -ltirpc -lnsl")
        code, result = self.cmd(f"file {self.exec_file}")
        self.assertIn("executable", result)
        code, result = self.cmd(f"./{self.exec_file} {IP} {rpc_prognum} {versnum} {protocol}")
        self.assertIn(f"RPC server on host {IP} for prognum {rpc_prognum}, versnum {versnum}, protocol {protocol} is listening on port 111", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f"rm -rf {self.c_file} {self.exec_file}")