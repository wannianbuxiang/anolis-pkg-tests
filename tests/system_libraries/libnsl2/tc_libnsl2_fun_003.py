# -*- encoding: utf-8 -*-

"""
@File:      tc_libpipeline_fun003.py
@Time:      2024-04-17 11:22:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
import time
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libpipeline_fun003.yaml for details

    :avocado: tags=P2,noarch,local,python,fix
    """

    file_context = r"""
#include <stdio.h>
#include <rpc/rpc.h>
#include <stdlib.h>
#include <rpc/pmap_clnt.h>
#include <unistd.h> 

#define PING_PROG 0x20000001
#define PING_VERS 1
#define PING_PROC 1

// 服务器端 ping 服务处理函数。
void ping_proc(struct svc_req *rqstp, register SVCXPRT *transp) {
    char *arg;
    // 我们将传递一个字符串给 ping 服务，并返回字符串。
    static char *result;

    // 获取参数
    if (!svc_getargs(transp, (xdrproc_t)xdr_string, (caddr_t)&arg)) {
        svcerr_decode(transp);
        return;
    }

    printf("Server received: %s\n", arg);

    result = (char *)malloc(sizeof(char) * 20);
    strcpy(result, "Pong");

    // 发送回复
    if (!svc_sendreply(transp, (xdrproc_t)xdr_string, result)) {
        svcerr_systemerr(transp);
    }

    // 释放参数资源
    if (!svc_freeargs(transp, (xdrproc_t)xdr_string, (caddr_t)&arg)) {
        fprintf(stderr, "Unable to free arguments\n");
        exit(1);
    }

    free(result);
}

int main() {
    register SVCXPRT *transp;
    int sockfd;
    struct sockaddr_in addr;
    int len = sizeof(struct sockaddr_in);

    // 注销之前的版本
    pmap_unset(PING_PROG, PING_VERS);

    // 创建 UDP 套接字
    sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (sockfd < 0) {
        perror("socket creation failed");
        exit(1);
    }

    // 初始化地址结构
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(60152);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);

    // 绑定套接字到特定端口
    if (bind(sockfd, (struct sockaddr *)&addr, len) == -1) {
        perror("bind failed");
        close(sockfd);
        exit(1);
    }

    // 使用绑定的套接字创建 RPC 服务
    transp = svcudp_create(sockfd);
    if (transp == NULL) {
        fprintf(stderr, "cannot create udp service.\n");
        close(sockfd);
        exit(1);
    }

    // 注册服务
    if (!svc_register(transp, PING_PROG, PING_VERS, ping_proc, IPPROTO_UDP)) {
        fprintf(stderr, "unable to register (PING_PROG, PING_VERS, udp).\n");
        close(sockfd);
        exit(1);
    }

    svc_run();  // 进入服务处理循环
    fprintf(stderr, "svc_run returned\n");
    close(sockfd);
    exit(1);
}
"""

    PARAM_DIC = {"pkg_name": "libnsl2 libnsl2-devel  rpcbind nmap-ncat lsof"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.port = 60152
        self.message="Ping!"
        self.c_file = "libnsl2_fun_003.c"
        self.exec_file = "libnsl2_fun_003"
        cmdline = f"""cat > {self.c_file } <<EOF
        {self.file_context}
EOF
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd(f"gcc -o {self.exec_file} {self.c_file} -I /usr/include/tirpc -ltirpc -lnsl")
        code, result = self.cmd(f"file {self.exec_file}")
        self.assertIn("executable", result)
        #生成RPC server
        self.cmd(f"./{self.exec_file} > /dev/null 2>&1 &")
        time.sleep(1)
        code, pid = self.cmd(f"lsof|grep {self.port}|awk '{{print $2}}'")
        self.log.info(f"Server started with PID: {pid}")
        # 模拟客户端发送消息到服务端
        self.log.info(f"Sending message to server: {self.message}")
        self.cmd(f"echo {self.message} | nc -u -w1 127.0.0.1 {self.port}")
        # 关闭服务端程序
        self.cmd(f"kill -9 {pid}")
        
        

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f"rm -rf {self.c_file} {self.exec_file}")