#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

"""
@File:      tc_libksba_libksba_fun_001.py
@Time:      2024/08/05 14:07:20
@Author:    wangge
@Version:   1.0
@Contact:   YS.wangge@h3c.com
@License:   Mulan PSL v2
@Modify:    wangge
"""
import subprocess
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libksba_libksba_fun_001.yaml for details

    :avocado: tags=P0,noarch,local
    """
    PARAM_DIC = {"pkg_name": "libksba gnupg2 openssl"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        

    def test(self):

        result = subprocess.run("ldd -v /usr/bin/gpgsm | grep libksba", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.assertEqual(result.returncode, 0, "The libksba.so.8 link in the /usr/bin/gpgsm directory does not exist")

        result = subprocess.run("ldd -v /usr/bin/kbxutil | grep libksb", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.assertEqual(result.returncode, 0, "The libksba.so.8 link in the /usr/bin/gpgsm directory does not exist")

        result = subprocess.run("ldd -v /usr/bin/kbxutil | grep libksba", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.assertEqual(result.returncode, 0, "The libksba.so.8 link in the /usr/bin/kbxutil directory does not exist")

        result = subprocess.run(
            "openssl req -new -x509 -days 3650 -keyout CARoot.key -out CARoot.crt -nodes -subj '/CN=china/L=wuhan/O=uniontech/CN=www.chinauos.com/'",
            shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )
        self.assertEqual(result.returncode, 0, "Failed to generate private key file and public key certificate using 509 certificate")
        
        result = subprocess.run("test -f CARoot.crt && test -f CARoot.key", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.assertEqual(result.returncode, 0, "The private key file or public key certificate does not exist")

        result = subprocess.run("gpgsm --help 2>&1 | grep verbose", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.assertEqual(result.returncode, 0, "Gpgsm failed to view help")
        self.assertIn("verbose", result.stdout.decode(), "Gpgsm help does not contain 'verbose'")

        result = subprocess.run("kbxutil --help 2>&1 | grep verbose", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.assertEqual(result.returncode, 0, "Kbxutil failed to view help")
        self.assertIn("verbose", result.stdout.decode(), "Kbxutil help does not contain 'verbose'")
        

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        subprocess.run("rm -rf CARoot.key CARoot.crt",shell=True,check=True)
        
    

 
