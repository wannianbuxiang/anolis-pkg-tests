# -*- encoding: utf-8 -*-

"""
@File:      tc_libksba_libksba_fun003.py
@Time:      2024/3/19 15:47:20
@Author:    wangyaru
@Version:   1.0
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

import subprocess
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libksba_libksba_fun003.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libksba libksba-devel libgpg-error libgpg-error-devel openssl gcc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        # issuer异常的证书数据
        cmdline = '''cat >mycert.pem<<"EOF"
-----BEGIN CERTIFICATE-----
MIIDUjCCAjqgAwIBAgIUGRzSLr8VxPNDHX1jQMaezs6cePMwDQYJKoZIhvcNAQEL
BQAwADAeFw0yNDAzMjEwMjUwMzJaFw0yNTAzMjEwMjUwMzJaMHExCzAJBgNVBAYT
AlVTMRMwEQYDVQQIDApDYWxpZm9ybmlhMRYwFAYDVQQHDA1TYW4gRnJhbmNpc2Nv
MQ4wDAYDVQQKDAVNeU9yZzEPMA0GA1UECwwGTXlEZXB0MRQwEgYDVQQDDAtleGFt
cGxlLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAI2pBzPwRuDV
2Sf3scU/bBP15qztxU1ivfxQ+jUOSXHUTWpmd/TU4e+e4TFslwasbDYNuu09Sghh
9HSALSTdB6u5ZIQj9AVJY2f0CLkUSWkJnUXyOSh1w4c+wzIXthXk3ekfPP3cvgf3
uwEWCIiWpv6rooEqPBvE4xcsFQpRpVnZI1gLxB1w7IYcGzpwCpOEYRtEsM3ZG4mT
BhX13WZB0CmZqwO3/p8y9v/4dQF7XC6b/aM5NYOXP8yLbfNN3HAEeSzqhvgQL0Ut
ZWvB8QteKKiXHXaDjyHwFRxKUAnVbAvu7+e7pEki9VizA1oZIyBZ0M1Pv+yQ3fiU
nJnABso4AOMCAwEAAaNTMFEwHQYDVR0OBBYEFJJTOlcNx4eFPpP4K/LxRQ81Qd+Y
MB8GA1UdIwQYMBaAFJJTOlcNx4eFPpP4K/LxRQ81Qd+YMA8GA1UdEwEB/wQFMAMB
Af8wDQYJKoZIhvcNAQELBQADggEBAA6l6a0FswlptM1qVmd1R0uY2C7R8PethEcT
uK6hlM0o/ZzNaQvrL4hniO7hkyJiVC0aTmkQjX2LM55PjRM/p2ECJgIGF6yBtbUP
9gkoe5cVfU8pZdxDj8FbPIEIZZg/eVYrkUVatqPS5u6VYVbkrMmGl63lmk2ryHVT
PG3lC0M30oHkhv8K/oOBGnuc8DUBEcvp/kR1rAcLpw/05JJlgESjhdwEtZxJg69t
3d1Q5vZTXtSAaLbmonSyQEjEE39josDbW0BEOIjQAHa/tGnnqBwbpJqDNKmXHQ+m
JRArsReEOSDKIPLuLHMKKsTOq9BQRvrfy1GjXwvuaJoiXEhRk7w=
-----END CERTIFICATE-----
EOF'''
        self.cmd(cmdline)
        self.cmd("openssl x509 -in mycert.pem -outform der -out mycert.der")
        cmdline = '''cat >parse_invalidcertificate.c<<"EOF"
#include <ksba.h>
#include <stdio.h>
#include <stdlib.h>

void print_cert_info(ksba_cert_t cert) {
    char *subject = NULL;
    char *issuer = NULL;
    gpg_error_t err;
	int idx = 0;

    subject = ksba_cert_get_subject(cert, idx);
    printf("Subject: %s\\n", subject);
    ksba_free(subject); // 释放返回的字符串

	issuer = ksba_cert_get_issuer(cert, idx);
    printf("Issuer: %s\\n", issuer);
    ksba_free(issuer); // 释放返回的字符串
}

int main() {
    ksba_cert_t cert;
    ksba_reader_t reader;
    gpg_error_t err;
    FILE *file;

    file = fopen("mycert.pem", "rb");
    if (!file) {
        perror("Unable to open file");
        return 1;
    }

    err = ksba_reader_new(&reader);
    if (err) {
        fprintf(stderr, "Failed to create reader: %s\\n", gpg_strerror(err));
        fclose(file);
        return 1;
    }

    err = ksba_reader_set_file(reader, file);
    if (err) {
        fprintf(stderr, "Failed to set file for reader: %s\\n", gpg_strerror(err));
        ksba_reader_release(reader);
        fclose(file);
        return 1;
    }

    err = ksba_cert_new(&cert);
    err = ksba_cert_read_der(cert, reader);
    if (err) {
        fprintf(stderr, "Failed to read certificate: %s\\n", gpg_strerror(err));
        // Even if the certificate is corrupt, we can try to extract some information
        print_cert_info(cert);
        // Release resources even if we encountered an error
        ksba_cert_release(cert);
        ksba_reader_release(reader);
        fclose(file);
        return 1;
    }
    printf("parse cert: %s\\n", gpg_strerror(err));

    // Print certificate information
    print_cert_info(cert);

    // Release resources
    ksba_cert_release(cert);
    ksba_reader_release(reader);
    fclose(file);

    return 0;
}
EOF'''
        self.cmd(cmdline)
    def test(self):
        self.cmd("gcc -o parse_invalidcertificate parse_invalidcertificate.c -lksba -lgpg-error")
        code, result = self.cmd("./parse_invalidcertificate")
        self.assertIn("parse cert: Success", result)
        self.assertIn("Subject", result)
        self.assertIn("Issuer", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf parse_invalidcertificate* mycert*")