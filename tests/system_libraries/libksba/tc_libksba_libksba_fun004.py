# -*- encoding: utf-8 -*-

"""
@File:      tc_libksba_libksba_fun004.py
@Time:      2024/3/120 15:47:20
@Author:    wangyaru
@Version:   1.0
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libksba_libksba_fun004.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libksba libksba-devel libgpg-error libgpg-error-devel openssl gcc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("touch empty_certificate.der")
        cmdline = '''cat >parse_empty_cert.c<<"EOF"
#include <ksba.h>
#include <stdio.h>
#include <stdlib.h>

void handle_error(gpg_error_t err, const char *msg) {
    if (err) {
        fprintf(stderr, "%s: %s\\n", msg, gpg_strerror(err));
        exit(EXIT_FAILURE);
    }
}

int main() {
    ksba_cert_t cert;
    ksba_reader_t reader;
    gpg_error_t err;
    FILE *file;

    // Open the certificate file
    file = fopen("empty_certificate.der", "rb");
    if (!file) {
        perror("Unable to open file");
        return 1;
    }

    // Create a reader object
    err = ksba_reader_new(&reader);
    handle_error(err, "Failed to create reader object");

    ksba_reader_set_file(reader, file);
    handle_error(err, "Failed to set file for the reader");

    // Attempt to create a new certificate object from the reader
    err = ksba_cert_new(&cert);
    if (err) {
        fprintf(stderr, "Failed to read certificate: %s\\n", gpg_strerror(err));
    } else {
        printf("Unexpected success: An empty file should not contain a valid certificate.\\n");
        ksba_cert_release(cert);
    }

    // Release resources
    ksba_reader_release(reader);
    fclose(file);

    return 0;
}
EOF'''
        self.cmd(cmdline)
    def test(self):
        self.cmd("gcc -o parse_empty_cert parse_empty_cert.c -lksba -lgpg-error")
        code, result = self.cmd("./parse_empty_cert | iconv -f ISO-8859-1 -t UTF-8")
        self.assertIn("An empty file should not contain a valid certificate", result)


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf parse_empty_cert* empty_certificate*")