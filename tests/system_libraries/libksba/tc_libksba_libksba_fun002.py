# -*- encoding: utf-8 -*-

"""
@File:      tc_libksba_libksba_fun002.py
@Time:      2024/3/18 10:10:20
@Author:    wangyaru
@Version:   1.0
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

import pexpect
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libksba_libksba_fun002.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libksba libksba-devel libgpg-error libgpg-error-devel gcc openssl"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >mycrl.pem<<"EOF"
-----BEGIN X509 CRL-----
MIIBtDCBnQIBATANBgkqhkiG9w0BAQsFADAyMRMwEQYDVQQDDApFeGFtcGxlIENB
MQ4wDAYDVQQKDAVNeU9yZzELMAkGA1UEBhMCVVMXDTI0MDMxODAyMzgzNFoXDTI0
MDQxNzAyMzgzNFowJzAlAhR9hdbVH4YGut3XvlEizd4tmIVmXxcNMjQwMzE4MDIz
NjQ4WqAOMAwwCgYDVR0UBAMCAQEwDQYJKoZIhvcNAQELBQADggEBABwkpqa+B0Hg
QWylf1uC5zDh21LeIJXUXeMDL4vmObisAK/cfaJFHb9YK/9PW40yDVlCea77GUii
1ln9OlqP88ayw1V3bB9Q8p2o/dp3f5TqYqHqxeP96TLnIErgmtVtn52rqeHYulBa
D3MiC0lJ8POwQM5LM3iVyrXq8vDYKkjU6oRt/jtg9mYfbvNOgJtd5eG/t4ytdi5v
gO/LdcXEnCmzu4os4M3ih4SKJM4t/vkzmYGfzcRJFUwOVM+slEilOn2V2BAnU17b
jOYBe2LZZ5ozng+7H0h4IPHOgEGcm/aJBETikbiJA7c0QKHrBQC7USQSjnKdHB1Q
rgq4R4nQmBU=
-----END X509 CRL-----
EOF'''
        self.cmd(cmdline)
        self.cmd("openssl crl -in mycrl.pem -outform DER -out mycrl_file.der")
        cmdline = '''cat >parse_crl.c<<"EOF"
#include <ksba.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
    ksba_crl_t crl;
    ksba_reader_t reader;
    FILE *fp;
    gpg_error_t err;
    const char *digest_algo;
    char *issuer = NULL;
    int idx = 0; // 用于遍历扩展的索引

    // 打开 DER 格式的 CRL 文件
    fp = fopen("mycrl_file.der", "rb");
    if (!fp) {
        perror("Failed to open CRL file");
        return 1;
    }

    // 创建一个 reader 对象
    err = ksba_reader_new(&reader);
    if (err) {
        fprintf(stderr, "Failed to create reader object: %s\\n", gpg_strerror(err));
        fclose(fp);
        return 1;
    }
    ksba_reader_set_file(reader, fp);

    // 创建一个新的 CRL 对象
    err = ksba_crl_new(&crl);
    if (err) {
        fprintf(stderr, "Failed to create CRL object: %s\\n", gpg_strerror(err));
        ksba_reader_release(reader);
        fclose(fp);
        return 1;
    }

    // 将 reader 关联到 CRL 对象
    ksba_crl_set_reader(crl, reader);

    // 解析 CRL 文件
    ksba_stop_reason_t stopreason;
    err = ksba_crl_parse(crl, &stopreason);
    if (err) {
        fprintf(stderr, "Failed to parse CRL: %s\\n", gpg_strerror(err));
        ksba_crl_release(crl);
        ksba_reader_release(reader);
        fclose(fp);
        return 1;
    } else {
        printf("CRL parsed successfully.\\n");

        // 获取并打印 CRL 的摘要算法
        digest_algo = ksba_crl_get_digest_algo(crl);
        if (digest_algo) {
            printf("Digest Algorithm: %s\\n", digest_algo);
        }

        // 获取并打印 CRL 的发行者
        err = ksba_crl_get_issuer(crl, &issuer);
        if (!err) {
            printf("Issuer: %s\\n", issuer);
            ksba_free(issuer);
        }

        // 获取并打印 CRL 扩展信息
        char const *oid;
        int critical;
        unsigned char const *der;
        size_t derlen;
        while (!(err = ksba_crl_get_extension(crl, idx, &oid, &critical, &der, &derlen))) {
            printf("Extension OID: %s, Critical: %d, DER Length: %zu\\n", oid, critical, derlen);
            idx++;
        }
    }

    // 清理工作
    ksba_crl_release(crl);
    ksba_reader_release(reader);
    fclose(fp);

    return 0;
}
EOF'''
        self.cmd(cmdline)
    def test(self):
        self.cmd("gcc -o parse_crl parse_crl.c -lksba -lgpg-error")
        code, result = self.cmd("./parse_crl")
        self.assertIn("CRL parsed successfully", result)
        self.assertIn("Digest Algorithm", result)
        self.assertIn("Issuer", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf mycrl* parse_crl*")
