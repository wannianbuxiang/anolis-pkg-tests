#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

"""
@File:      tc_libtiff_libtiff_fun_001.py
@Time:      2024/07/17 11:07:20
@Author:    wangge
@Version:   1.0
@Contact:   YS.wangge@h3c.com
@License:   Mulan PSL v2
@Modify:    wangge
"""
import os
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libtiff_libtiff_fun_001.yaml for details

    :avocado: tags=P0,noarch,local
    """
    PARAM_DIC = {"pkg_name": "gcc libtiff-devel libtiff"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        

    def test(self):
        script_dir = os.path.dirname(__file__)
        code, _ = self.cmd(f"gcc {script_dir}/test_tiff.c -o test -ltiff")
        self.assertEquals(0, code)
        
        code, _ = self.cmd('test -e test')
        self.assertEquals(0, code)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf test")
        