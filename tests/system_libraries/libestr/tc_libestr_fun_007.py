# -*- encoding: utf-8 -*-

"""
@File:      tc_libestr_fun_007.py
@Time:      2024-03-25 13:30:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libestr_fun_007.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "libestr libestr-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.c_file = "libestr_fun_007.c"
        self.exec_file = "libestr_fun_007"
        cmdline = """cat > libestr_fun_007.c <<EOF
#include <stdio.h>
#include <string.h>
#include <libestr.h>


// 获取字符串内容的宏
#define es_getStrDataPtr(es_str) ((char*)((es_str) + 1))

// 主测试程序
int main() {
    const char *text1 = "This is a simple Test string.";
    es_str_t *str1 = es_newStrFromCStr(text1, strlen(text1));

    const char *text2 = "Test";
    es_str_t *str2 = es_newStrFromCStr(text2, strlen(text2));

    const char *text3 = "test";  // 小写版本
    es_str_t *str3 = es_newStrFromCStr(text3, strlen(text3));

    // 测试 es_strContains
    int contains = es_strContains(str1, str2);
    printf("'%s' contains '%s': %s\\n", es_getStrDataPtr(str1), es_getStrDataPtr(str2),
           contains ? "YES" : "NO");

    // 测试 es_strCaseContains
    int caseContains = es_strCaseContains(str1, str3);
    printf("Case insensitive: '%s' contains '%s': %s\\n", es_getStrDataPtr(str1), es_getStrDataPtr(str3),
           caseContains ? "YES" : "NO");

    // 清理资源
    es_deleteStr(str1);
    es_deleteStr(str2);
    es_deleteStr(str3);

    return 0;
}
EOF
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd(f"gcc -o {self.exec_file} {self.c_file} -lestr")
        code, result = self.cmd(f"file {self.exec_file}")
        self.assertIn("executable", result)
        code, result = self.cmd(f"./{self.exec_file}")
        self.assertIn("'This is a simple Test string.' contains 'Test': YES", result)
        self.assertIn("Case insensitive: 'This is a simple Test string.' contains 'test': YES", result)


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f"rm -rf {self.c_file} {self.exec_file}")
