# -*- encoding: utf-8 -*-

"""
@File:      tc_libestr_fun_009.py
@Time:      2024-03-25 13:30:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libestr_fun_009.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "libestr libestr-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.c_file = "libestr_fun_009.c"
        self.exec_file = "libestr_fun_009"
        cmdline = """cat > libestr_fun_009.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libestr.h"

// 使用从 libestr.h 头文件获得的宏获取字符串长度
#define es_getStrLen(es_str) ((es_str)->lenStr)

// 主测试函数
int main() {
    const char *content = "Test string";
    es_str_t *str = es_newStrFromCStr(content, strlen(content));
    
    // 使用 libestr.h 中声明的 es_getBufAddr 函数获取缓冲区地址
    unsigned char *bufAddr = es_getBufAddr(str);
    
    // 计算预期地址的手动方式
    unsigned char *expectedAddr = ((unsigned char*) str) + sizeof(es_str_t);
    
    // 检查返回地址是否与预期地址匹配
    if (bufAddr == expectedAddr) {
        printf("es_getBufAddr test passed: Got correct buffer address.\\n");
    }
    
    // 清理资源
    es_deleteStr(str);
    return 0;
}
EOF
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd(f"gcc -o {self.exec_file} {self.c_file} -lestr")
        code, result = self.cmd(f"file {self.exec_file}")
        self.assertIn("executable", result)
        code, result = self.cmd(f"./{self.exec_file}")
        self.assertIn("es_getBufAddr test passed: Got correct buffer address.", result)
        

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f"rm -rf {self.c_file} {self.exec_file}")
