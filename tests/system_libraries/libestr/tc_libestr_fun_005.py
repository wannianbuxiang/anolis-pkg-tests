# -*- encoding: utf-8 -*-

"""
@File:      tc_libestr_fun_005.py
@Time:      2024-03-25 13:30:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libestr_fun_005.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "libestr libestr-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.c_file = "libestr_fun_005.c"
        self.exec_file = "libestr_fun_005"
        cmdline = """cat > libestr_fun_005.c <<EOF
#include <stdio.h>
#include <string.h>
#include <libestr.h>

// 获取字符串内容的宏
#define es_getStrDataPtr(es_str) ((char*)((es_str) + 1))

// 主测试代码
int main() {
    const char *originalText = "This is the original string.";
    // 使用 libestr 提供的 es_newStrFromCStr 函数创建原始 es_str_t 字符串
    es_str_t *originalStr = es_newStrFromCStr(originalText, strlen(originalText));
    printf("Original string: '%s'\\n", es_getStrDataPtr(originalStr));

    // 调用 es_strdup，进行复制
    es_str_t *duplicatedStr = es_strdup(originalStr);
    printf("Duplicated string: '%s'\\n", es_getStrDataPtr(duplicatedStr));

    // 检查是否成功复制了字符串内容，并且内存地址不同
    if (strcmp(es_getStrDataPtr(originalStr), es_getStrDataPtr(duplicatedStr)) == 0 &&
        originalStr != duplicatedStr) {
        printf("es_strdup successfully duplicated the string.\\n");
    } else {
        printf("es_strdup failed to duplicate the string.\\n");
    }

    // 清理所有字符串对象
    es_deleteStr(originalStr);
    es_deleteStr(duplicatedStr);

    return 0;
}
EOF
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd(f"gcc -o {self.exec_file} {self.c_file} -lestr")
        code, result = self.cmd(f"file {self.exec_file}")
        self.assertIn("executable", result)
        code, result = self.cmd(f"./{self.exec_file}")
        self.assertIn("Original string: 'This is the original string.'", result)
        self.assertIn("Duplicated string: 'This is the original string.'", result)
        self.assertIn("es_strdup successfully duplicated the string.", result)


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f"rm -rf {self.c_file} {self.exec_file}")
