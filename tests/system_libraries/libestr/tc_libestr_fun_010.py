# -*- encoding: utf-8 -*-

"""
@File:      tc_libestr_fun_010.py
@Time:      2024-03-25 13:30:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libestr_fun_010.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "libestr libestr-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.c_file = "libestr_fun_010.c"
        self.exec_file = "libestr_fun_010"
        cmdline = """cat > libestr_fun_010.c <<EOF
#include <stdio.h>
#include <libestr.h>

char *es_version(void);

// 主测试程序
int main() {
    // 调用 es_version 获取版本字符串
    char *version = es_version();

    // 打印版本字符串
    printf("libestr version: %s\\n", version);

    return 0;
}
EOF
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd(f"gcc -o {self.exec_file} {self.c_file} -lestr")
        code, result = self.cmd(f"file {self.exec_file}")
        self.assertIn("executable", result)
        code, result = self.cmd(f"./{self.exec_file}")
        self.assertIn("libestr version", result)

        

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f"rm -rf {self.c_file} {self.exec_file}")
