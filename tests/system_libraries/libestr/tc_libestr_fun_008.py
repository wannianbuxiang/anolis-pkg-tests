# -*- encoding: utf-8 -*-

"""
@File:      tc_libestr_fun_008.py
@Time:      2024-03-25 13:30:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libestr_fun_008.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "libestr libestr-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.c_file = "libestr_fun_008.c"
        self.exec_file = "libestr_fun_008"
        cmdline = """cat > libestr_fun_008.c <<EOF
#include <stdio.h>
#include <string.h>
#include <libestr.h>

// 获取字符串内容的宏
#define es_getStrDataPtr(es_str) ((char*)((es_str) + 1))
#define es_getStrLen(es_str) ((es_str)->lenStr)

// 主测试函数
int main() {
    // 创建一个非空的字符串
    const char *content = "Test string";
    es_str_t *my_str = es_newStrFromCStr(content, strlen(content));

    // 打印原始字符串长度
    printf("Original string length: %zu\\n", es_getStrLen(my_str));

    // 将字符串清空
    es_emptyStr(my_str);

    // 打印清空后的字符串长度
    printf("String length after es_emptyStr: %zu\\n", es_getStrLen(my_str));
    
    // 检查字符串长度是否被正确设置为0
    if (es_getStrLen(my_str) == 0) {
        printf("es_emptyStr function test passed: String length is 0.\\n");
    } else {
        printf("es_emptyStr function test failed: String length is not 0.\\n");
    }

    // 清理资源
    es_deleteStr(my_str);

    return 0;
}
EOF
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd(f"gcc -o {self.exec_file} {self.c_file} -lestr")
        code, result = self.cmd(f"file {self.exec_file}")
        self.assertIn("executable", result)
        code, result = self.cmd(f"./{self.exec_file}")
        self.assertIn("Original string length: 11", result)
        self.assertIn("String length after es_emptyStr: 0", result)
        self.assertIn("es_emptyStr function test passed: String length is 0.", result)
        

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f"rm -rf {self.c_file} {self.exec_file}")
