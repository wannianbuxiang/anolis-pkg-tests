# -*- encoding: utf-8 -*-

"""
@File:      tc_libestr_fun_004.py
@Time:      2024-03-25 13:09:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libestr_fun_004.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "libestr libestr-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.c_file = "libestr_fun_004.c"
        self.exec_file = "libestr_fun_004"
        cmdline = """cat > libestr_fun_004.c <<EOF
#include <stdio.h>
#include <string.h>
#include <libestr.h>

// 获取字符串长度和内容的宏
#define es_getStrDataPtr(es_str) ((char*)((es_str) + 1))

// 主测试程序
int main() {
    // 创建两个 es_str_t 对象
    const char *text1 = "Hello, World!";
    const char *text2 = "HELLO, WORLD!";
    es_size_t len1 = strlen(text1);
    es_size_t len2 = strlen(text2);

    es_str_t *str1 = es_newStrFromCStr(text1, len1);
    es_str_t *str2 = es_newStrFromCStr(text2, len2);
    if (str1 == NULL || str2 == NULL) {
        fprintf(stderr, "Failed to create es_str_t objects.\\n");
        if (str1) es_deleteStr(str1);
        if (str2) es_deleteStr(str2);
        return -1;
    }
    
    // 测试 es_strcmp
    int cmp = es_strcmp(str1, str2);
    printf("es_strcmp result: %d\\n", cmp);

    // 测试 es_strbufcmp
    cmp = es_strbufcmp(str1, (const unsigned char *)text2, len2);
    printf("es_strbufcmp result: %d\\n", cmp);

    // 测试 es_strcasebufcmp
    cmp = es_strcasebufcmp(str1, (const unsigned char *)text2, len2);
    printf("es_strcasebufcmp result: %d\\n", cmp);
  
    // 清理资源
    es_deleteStr(str1);
    es_deleteStr(str2);

    return 0;
}
EOF
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd(f"gcc -o {self.exec_file} {self.c_file} -lestr")
        code, result = self.cmd(f"file {self.exec_file}")
        self.assertIn("executable", result)
        code, result = self.cmd(f"./{self.exec_file}")
        self.assertIn("es_strcmp result: 32", result)
        self.assertIn("es_strbufcmp result: 32", result)
        self.assertIn("es_strcasebufcmp result: 0", result)


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f"rm -rf {self.c_file} {self.exec_file}")
