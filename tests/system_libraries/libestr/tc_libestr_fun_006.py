# -*- encoding: utf-8 -*-

"""
@File:      tc_libestr_fun_006.py
@Time:      2024-03-25 13:30:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libestr_fun_006.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "libestr libestr-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.c_file = "libestr_fun_006.c"
        self.exec_file = "libestr_fun_006"
        cmdline = """cat > libestr_fun_006.c <<EOF
#include <stdio.h>
#include <string.h>
#include <ctype.h> // For islower and isspace function declarations
#include <libestr.h>

// 假设下面的函数已在libestr库中定义
void es_tolower(es_str_t *s);
void es_deleteStr(es_str_t *s);

// 获取字符串内容的宏
#define es_getStrDataPtr(es_str) ((char*)((es_str) + 1))

// 检查字符串是否全为小写字符的辅助函数
int is_all_lowercase(const char *s) {
    while (*s) {
        if (!islower((unsigned char)*s) && !isspace((unsigned char)*s)) {
            return 0; // 发现一个大写字符
        }
        s++;
    }
    return 1; // 所有字符均为小写
}

// 主测试程序
int main() {
    // 创建一个包含大写和小写字符的初始字符串
    const char *mixedCaseStr = "This is a Test STRING";
    es_str_t *str = es_newStrFromCStr(mixedCaseStr, strlen(mixedCaseStr)); // 假设 es_newStrFromCStr 在 libestr 里定义
    printf("Original string: '%s'\\n", es_getStrDataPtr(str));

    // 转换字符串为小写
    es_tolower(str);
    printf("String after es_tolower: '%s'\\n", es_getStrDataPtr(str));

    // 检查转换后的字符串是否为全小写
    if (is_all_lowercase(es_getStrDataPtr(str))) {
        printf("es_tolower function test passed: all characters are lowercase.\\n");
    } else {
        printf("es_tolower function test failed: not all characters are lowercase.\\n");
    }

    // 清理
    es_deleteStr(str);

    return 0;
}
EOF
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd(f"gcc -o {self.exec_file} {self.c_file} -lestr")
        code, result = self.cmd(f"file {self.exec_file}")
        self.assertIn("executable", result)
        code, result = self.cmd(f"./{self.exec_file}")
        self.assertIn("Original string: 'This is a Test STRING'", result)
        self.assertIn("String after es_tolower: 'this is a test string'", result)
        self.assertIn("es_tolower function test passed: all characters are lowercase.", result)


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f"rm -rf {self.c_file} {self.exec_file}")
