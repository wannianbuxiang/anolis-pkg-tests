# -*- encoding: utf-8 -*-

"""
@File:      tc_libestr_fun_001.py
@Time:      2024-03-21 13:09:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libestr_fun_001.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libestr libestr-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.c_file = 'libestr_fun_001.c'
        self.exec_file = 'libestr_fun_001'
        cmdline='''cat > libestr_fun_001.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include "libestr.h"

int main() {
    const es_size_t lenhint = 64;  // 指定一个长度提示
    es_str_t *myStr = es_newStr(lenhint);  // 创建一个新字符串
    
    // 检查字符串是否成功创建
    if (myStr != NULL) {
        printf("New string created with buffer length of %lu.\\n", (unsigned long)myStr->lenBuf);
        

        // 删除字符串
        es_deleteStr(myStr);
        printf("String deleted successfully.\\n");
    } else {
        fprintf(stderr, "Failed to create new string.\\n");
        return -1;
    }
    
    return 0;
}
EOF
'''
        self.cmd(cmdline)

    def test(self):
        self.cmd(f"gcc -o {self.exec_file} {self.c_file} -lestr")
        code, result = self.cmd(f"file {self.exec_file}")
        self.assertIn("executable", result)
        code, result = self.cmd(f"./{self.exec_file}")
        self.assertIn("New string created with buffer length of 64", result)
        
                
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f"rm -rf {self.c_file} {self.exec_file}")