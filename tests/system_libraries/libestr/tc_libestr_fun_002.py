# -*- encoding: utf-8 -*-

"""
@File:      tc_libestr_fun_002.py
@Time:      2024-03-21 13:09:50
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libestr_fun_002.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "libestr libestr-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.c_file = "libestr_fun_002.c"
        self.exec_file = "libestr_fun_002"
        cmdline = """cat > libestr_fun_002.c <<EOF
#include <stdio.h>
#include <string.h>
#include <libestr.h>

// 获取字符串内容的宏
#define es_getStrDataPtr(es_str) ((char*)((es_str) + 1))


// 主测试程序
int main() {
    // 从C字符串创建测试
    const char *testCStr = "Test C String";
    es_str_t *strFromCStr = es_newStrFromCStr(testCStr, strlen(testCStr));
    printf("Test es_newStrFromCStr: '%s'\\n", es_getStrDataPtr(strFromCStr));
    es_deleteStr(strFromCStr);

    // 从SubStr创建测试
    es_str_t *str = es_newStrFromCStr("Create SubString from this", strlen("Create SubString from this"));
    es_str_t *subStr = es_newStrFromSubStr(str, 7, 9); // "SubString"
    printf("Test es_newStrFromSubStr: '%s'\\n", es_getStrDataPtr(subStr));
    es_deleteStr(subStr);
    es_deleteStr(str);

    // 从数字创建测试
    long long testNumber = 1234567890LL;
    es_str_t *strFromNumber = es_newStrFromNumber(testNumber);
    printf("Test es_newStrFromNumber: '%s'\\n", es_getStrDataPtr(strFromNumber));
    es_deleteStr(strFromNumber);

    // 从缓冲区创建测试
    unsigned char testBuf[] = { 'B', 'u', 'f', 'f', 'e', 'r', 0 };
    es_str_t *strFromBuf = es_newStrFromBuf(testBuf, sizeof(testBuf) - 1);
    printf("Test es_newStrFromBuf: '%s'\\n", es_getStrDataPtr(strFromBuf));
    es_deleteStr(strFromBuf);

    return 0;
}
EOF
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd(f"gcc -o {self.exec_file} {self.c_file} -lestr")
        code, result = self.cmd(f"file {self.exec_file}")
        self.assertIn("executable", result)
        code, result = self.cmd(f"./{self.exec_file}")
        self.assertIn("Test es_newStrFromCStr: 'Test C String'", result)
        self.assertIn("Test es_newStrFromSubStr: 'SubStringring'", result)
        self.assertIn("Test es_newStrFromNumber: '1234567890ing'", result)
        self.assertIn("Test es_newStrFromBuf: 'Buffer'", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f"rm -rf {self.c_file} {self.exec_file}")
