# -*- encoding: utf-8 -*-

"""
@File:      tc_libestr_fun_003.py
@Time:      2024-03-22 13:30:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libestr_fun_003.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "libestr libestr-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.c_file = "libestr_fun_003.c"
        self.exec_file = "libestr_fun_003"
        cmdline = """cat > libestr_fun_003.c <<EOF
#include <stdio.h>
#include <string.h>
#include <libestr.h>

// 获取字符串内容和长度的宏
#define es_getStrDataPtr(es_str) ((char*)((es_str) + 1))

// 主测试程序
int main() {
    // 创建一个初始字符串对象
    const char *cStr = "Hello, ";
    es_str_t *str = es_newStrFromCStr(cStr, strlen(cStr));
    printf("Initial string: '%s'\\n", es_getStrDataPtr(str));

    // 测试 es_extendBuf
    int res = es_extendBuf(&str, 50); // 假设扩展到50字符
    if (res != 0) {
        fprintf(stderr, "Error: Failed to extend buffer.\\n");
        es_deleteStr(str);
        return -1;
    }
    printf("Buffer after extension to 50: '%s'\\n", es_getStrDataPtr(str));

    // 测试 es_addChar
    const unsigned char extraChar = 'X';
    res = es_addChar(&str, extraChar);
    if (res != 0) {
        fprintf(stderr, "Error: Failed to add character 'X'.\\n");
        es_deleteStr(str);
        return -1;
    }
    printf("String after adding character 'X': '%s'\\n", es_getStrDataPtr(str));

    // 测试 es_addStr
    const char *suffix = "world!";
    es_str_t *strSuffix = es_newStrFromCStr(suffix, strlen(suffix));
    res = es_addStr(&str, strSuffix);
    if (res != 0) {
        fprintf(stderr, "Error: Failed to add string 'world!'.\\n");
        es_deleteStr(str);
        es_deleteStr(strSuffix);
        return -1;
    }
    printf("String after adding 'world!': '%s'\\n", es_getStrDataPtr(str));
    es_deleteStr(strSuffix);

    // 测试 es_addBuf
    const char *additionalText = " Have a nice day.";
    res = es_addBuf(&str, additionalText, strlen(additionalText));
    if (res != 0) {
        fprintf(stderr, "Error: Failed to add buffer.\\n");
        es_deleteStr(str);
        return -1;
    }
    printf("String after adding buffer: '%s'\\n", es_getStrDataPtr(str));

    // 清理资源
    es_deleteStr(str);

    return 0;
}
EOF
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd(f"gcc -o {self.exec_file} {self.c_file} -lestr")
        code, result = self.cmd(f"file {self.exec_file}")
        self.assertIn("executable", result)
        code, result = self.cmd(f"./{self.exec_file}")
        self.assertIn("Initial string: 'Hello, '", result)
        self.assertIn("Buffer after extension to 50: 'Hello, '", result)
        self.assertIn("String after adding character 'X': 'Hello, X'", result)
        self.assertIn("String after adding 'world!': 'Hello, Xworld!'", result)
        self.assertIn("String after adding buffer: 'Hello, Xworld! Have a nice day.'", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f"rm -rf {self.c_file} {self.exec_file}")
