#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_liburing_func001.py
@Time:      2024/03/06 17:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_liburing_func001.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "liburing-devel"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > test_io_uring.c <<EOF
#include <liburing.h>
#include <stdio.h>

int main() {
    struct io_uring ring;
    int ret;

    // 初始化一个具有32个缓冲区的输入/输出循环
    ret = io_uring_queue_init(32, &ring, 0);
    if (ret < 0) { 
        perror("io_uring_queue_init");
        return -1;
    }

    // 关闭输入/输出循环
    io_uring_queue_exit(&ring);

    return 0;
}
"""
        self.cmd(cmdline)
    
    def test(self):
        self.cmd("gcc -o test_io_uring test_io_uring.c -luring -std=c99 -Wall")
        ret_c, ret_o = self.cmd("./test_io_uring")
        if ret_c == 0:
            self.log.info('Program exited successfully.')
        else:
            self.log.info('Program did not exit successfully.')

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm test_io_uring.c test_io_uring")
