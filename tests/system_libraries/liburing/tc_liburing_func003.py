#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_liburing_func003.py
@Time:      2024/03/08 13:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_liburing_func003.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "liburing-devel"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > testliburing003.c <<EOF
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <liburing.h>

#define FILENAME "output.txt"
#define BUFFER_SIZE 4096
#define DATA_TO_WRITE "Hello, io_uring!"

int main() {
    struct io_uring ring;
    struct io_uring_sqe *sqe;
    struct io_uring_cqe *cqe;
    char buffer[BUFFER_SIZE];
    int ret, fd;

    // 初始化io_uring实例
    ret = io_uring_queue_init(8, &ring, 0);
    if (ret) {
        perror("io_uring_queue_init");
        return -1;
    }

    // 打开或创建文件用于写入
    fd = open(FILENAME, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    if (fd == -1) {
        perror("open");
        return -1;
    }

    // 准备并提交写入请求
    strncpy(buffer, DATA_TO_WRITE, sizeof(buffer));
    sqe = io_uring_get_sqe(&ring);
    if (!sqe) {
        fprintf(stderr, "io_uring_get_sqe: no sqes available\\n");
        goto cleanup;
    }
    io_uring_prep_write(sqe, fd, buffer, strlen(DATA_TO_WRITE), 0); 

    // 将SQE提交到队列
    io_uring_submit(&ring);

    // 等待完成并处理CQE
    ret = io_uring_wait_cqes(&ring, &cqe, 1, NULL, NULL);
    if (ret < 0) {
        perror("io_uring_wait_cqes");
        goto cleanup;
    }

    // 检查操作结果
    if (cqe->res < 0) { 
        perror("write failed");
    } else {
        printf("Wrote %d bytes to file\\n", cqe->res);
    }

cleanup:
    // 清理资源
    close(fd);
    if (cqe)
        io_uring_cqe_seen(&ring, cqe);
    io_uring_queue_exit(&ring);

    return 0;
}
"""
        self.cmd(cmdline)
    
    def test(self):
        self.cmd("gcc -o test_liburing003 testliburing003.c -luring")
        self.cmd("./test_liburing003")
        ret_c, ret_o = self.cmd("cat output.txt")
        self.assertTrue("Hello, io_uring!" in ret_o, "check output error.")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm testliburing003.c test_liburing003 output.txt")