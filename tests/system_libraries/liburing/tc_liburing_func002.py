#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_liburing_func002.py
@Time:      2024/03/07 17:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_liburing_func002.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "liburing-devel"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > testliburing.c <<EOF
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <liburing.h>

#define FILENAME "/etc/passwd"

int main() {
    struct io_uring ring;
    struct io_uring_sqe *sqe;
    struct io_uring_cqe *cqe;
    char buffer[4096];
    int ret, fd;

    /* 初始化 io_uring */
    ret = io_uring_queue_init(8, &ring, 0);
    if (ret) {
        perror("io_uring_queue_init");
        return -1;
    }

    /* 打开文件 */
    fd = open(FILENAME, O_RDONLY);
    if (fd == -1) {
        perror("open");
        return -1;
    }

    /* 准备提交的SQE（Submission Queue Entry） */
    sqe = io_uring_get_sqe(&ring);
    if (!sqe) {
        fprintf(stderr, "io_uring_get_sqe: no sqes available\\n");
        return -1;
    }

    io_uring_prep_read(sqe, fd, buffer, sizeof(buffer), 0); // 从文件开始位置读取

    /* 将SQE提交到队列 */
    io_uring_submit(&ring);

    /* 等待完成并处理CQE（Completion Queue Entry） */
    ret = io_uring_wait_cqes(&ring, &cqe, 1, NULL, NULL);
    if (ret < 0) {
        perror("io_uring_wait_cqes");
        return -1;
    }

    if (cqe->res < 0) { // 检查操作结果
        perror("read failed");
    } else {
        printf("Read %d bytes from file:\\n", cqe->res);
        write(STDOUT_FILENO, buffer, cqe->res); // 输出读取的内容
    }

    /* 清理资源 */
    close(fd);
    io_uring_cqe_seen(&ring, cqe);
    io_uring_queue_exit(&ring);

    return 0;
}
"""
        self.cmd(cmdline)
    
    def test(self):
        self.cmd("gcc -o test_liburing testliburing.c -luring")
        ret_c, ret_o = self.cmd("./test_liburing")
        self.assertTrue("bytes from file" in ret_o, "check output error.")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm testliburing.c test_liburing")
