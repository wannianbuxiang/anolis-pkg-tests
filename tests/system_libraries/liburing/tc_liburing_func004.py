#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_liburing_func004.py
@Time:      2024/03/08 15:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_liburing_func004.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "liburing-devel"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > testliburing004.c <<EOF
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <liburing.h>

int main() {
    struct io_uring ring;
    int ret;

    // 初始化io_uring实例
    ret = io_uring_queue_init(8, &ring, IORING_SETUP_IOPOLL);
    if (ret < 0) {
        fprintf(stderr, "Failed to initialize io_uring: %s\\n", strerror(-ret));
        return ret;
    }

    // 假设这里尝试添加一个读取操作，并捕获错误
    struct io_uring_sqe *sqe = io_uring_get_sqe(&ring);
    if (!sqe) {
        fprintf(stderr, "Failed to get SQE: ring is full or not initialized\\n");
        goto exit_ring; // 直接退出，跳转到清理部分
    }

    // 提交请求
    ret = io_uring_submit(&ring);
    if (ret <= 0) {
        fprintf(stderr, "Error submitting requests to io_uring: %s\\n", strerror(-ret));
        goto exit_ring;
    }

    // 等待完成事件
    struct io_uring_cqe *cqe;
    ret = io_uring_wait_cqe(&ring, &cqe);
    if (ret < 0) {
        fprintf(stderr, "Error waiting for completion event: %s\\n", strerror(-ret));
        goto exit_ring;
    }

    // 检查完成事件的结果
    if (cqe->res < 0) {
        fprintf(stderr, "IO operation failed with error: %s\\n", strerror(-cqe->res));
    } else {
        printf("IO operation succeeded\\n");
    }
    
    io_uring_cqe_seen(&ring, cqe); // 标记CQE为已处理

exit_ring:
    io_uring_queue_exit(&ring); // 清理io_uring实例
    return ret < 0 ? ret : 0; // 返回最终的错误码
}
"""
        self.cmd(cmdline)
    
    def test(self):
        self.cmd("gcc -o test_liburing004 testliburing004.c -luring")
        ret_c, ret_o = self.cmd("./test_liburing004")
        self.log.info(ret_o)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm testliburing004.c test_liburing004")