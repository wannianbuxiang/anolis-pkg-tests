
#! /usr/bin/bash

check_cmd() {
    if ! command -v "$1" &>/dev/null;then 
        echo "Error: $1 not found";exit 1
    fi
}

delete_entries() {
    check_cmd ldapdelete
    cat << EOF > /tmp/delete_entries.ldif
dn: ou=groups,${BASE//\"/}
changetype: delete

dn: ou=users,${BASE//\"/}
changetype: delete

dn: ${BASE//\"/}
changetype: delete
EOF
    echo -e "===ldapmodify -x -D ${DN_BASE} -f /tmp/delete_entries.ldif -w root\n"
    ldapmodify -x -D ${DN_BASE//\"/} -f /tmp/delete_entries.ldif -w root
    # ldapdelete -x -D "cn=admin,dc=domain,dc=com" -w root "ou=users,dc=domain,dc=com"
    # ldapdelete -x -D "cn=admin,dc=domain,dc=com" -w root "ou=groups,dc=domain,dc=com"
    # ldapdelete -x -D "cn=admin,dc=domain,dc=com" -w root "dc=domain,dc=com"
}

func_step1() {
    # 安装服务包，并停掉服务
    if ! yum install openldap openldap-devel openldap-servers openldap-clients -y -q;then
        echo "Failed to install openldap* by yum!";exit 1
    fi
    sleep 0.5

    if [ "`systemctl is-active slapd`" = "active" ];then
        systemctl stop slapd
        systemctl status slapd
    fi
}

func_step2() {
    # 生成管理员密码
    check_cmd slappasswd
    ADMIN_PW=root
    DEFAULT_PW=`slappasswd -h {SSHA} -s $ADMIN_PW` 
}

func_step3() {
    # 修改配置
    LDAP_CONF_P="/etc/openldap/slapd.d/cn=config"
    DN_BASE='"cn=admin,dc=domain,dc=com"'
    BASE='"dc=domain,dc=com"'
    if [ -f ${LDAP_CONF_P}/'olcDatabase={1}monitor.ldif' ];then
        \cp -avrf ${LDAP_CONF_P}/'olcDatabase={1}monitor.ldif' /tmp/monitor.ldif
        sed -r -i 's/dn.base="(.*)"/dn.base='$DN_BASE'/g' ${LDAP_CONF_P}/'olcDatabase={1}monitor.ldif'
        sed -r -i '/^#\s+CRC32.*/d' ${LDAP_CONF_P}/'olcDatabase={1}monitor.ldif'      
    else
        echo "Error: no such ${LDAP_CONF_P}/'olcDatabase={1}monitor.ldif'";exit 1
    fi

    if [ -f ${LDAP_CONF_P}/'olcDatabase={2}mdb.ldif' ];then
        \cp -avrf ${LDAP_CONF_P}/'olcDatabase={2}mdb.ldif' /tmp/mdb.ldif
        sed -r -i  '/^olcSuffix:/ s/(olcSuffix:\s+).*/\1'${BASE//\"/}'/; /^olcRootDN:/ s/(olcRootDN:\s+).*/\1'${DN_BASE//\"/}'/' ${LDAP_CONF_P}/'olcDatabase={2}mdb.ldif'
        sed -i '/^olcRootPW:.*/d' ${LDAP_CONF_P}/'olcDatabase={2}mdb.ldif'
        sed -r -i '/^#\s+CRC32.*/d' ${LDAP_CONF_P}/'olcDatabase={2}mdb.ldif'
        echo "olcRootPW: ${DEFAULT_PW}" >> ${LDAP_CONF_P}/'olcDatabase={2}mdb.ldif'      
    else
        echo "Error: no such ${LDAP_CONF_P}/'olcDatabase={2}mdb.ldif'";exit 1
    fi

    echo -e "===cat ${LDAP_CONF_P}/'olcDatabase={2}mdb.ldif' ${LDAP_CONF_P}/'olcDatabase={2}mdb.ldif'\n" 
    cat ${LDAP_CONF_P}/'olcDatabase={2}mdb.ldif' ${LDAP_CONF_P}/'olcDatabase={2}mdb.ldif'
    check_cmd slaptest
    slaptest -u
}

func_setp4() {
    cat > /tmp/user_base.ldif <<EOF
dn: ${BASE//\"/}
objectClass: dcObject
objectClass: organization
o: domain.com
dc: domain

dn: ou=users,${BASE//\"/}
objectClass: organizationalUnit
objectClass: top
ou: users

dn: ou=groups,${BASE//\"/}                                               
objectClass: organizationalUnit
objectClass: top
ou: groups
EOF
    sleep 0.5
    systemctl start slapd || { echo "Failed to start slapd";exit 1; }
    check_cmd ldapadd
    systemctl status slapd
    echo -e "===ldapsearch -x -D ${DN_BASE//\"/} -b ${BASE//\"/} \"(objectClass=*)\" -w root\n"
    if ! ldapsearch -x -D ${DN_BASE//\"/} -b ${BASE//\"/} "(objectClass=*)" -w root;then
        echo -e "===[search is not ok]\n" 
        echo -e "===ldapadd -x -D ${DN_BASE//\"/} -f /tmp/user_base.ldif -w root\n"
        ldapadd -x -D ${DN_BASE//\"/} -f /tmp/user_base.ldif -w root
    else
        echo -e "[search is ok]\n"
        delete_entries
        echo -e "===ldapadd -x -D ${DN_BASE//\"/} -f /tmp/user_base.ldif -w root\n"
        ldapadd -x -D ${DN_BASE//\"/} -f /tmp/user_base.ldif -w root
    fi
}

main() {
    func_step1
    func_step2
    func_step3
    func_setp4
}

main