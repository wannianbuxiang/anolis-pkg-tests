#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_openldap_openldap_func_003.py
@Time:      2024/03/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_openldap_openldap_func_003.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc openldap openldap-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > openldap_ldap_whoami.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <ldap.h>

int main(int argc, char **argv) {
    if (argc != 4) {
        fprintf(stderr, "Usage: %s <ldap_uri> <bind_dn> <password>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    LDAP *ld;
    int rc, version = LDAP_VERSION3;
    char *ldap_uri = argv[1];
    char *bind_dn = argv[2];
    char *password = argv[3];
    int msgid;

    // 初始化LDAP库并设置版本
    ldap_initialize(&ld, ldap_uri);
    if (ld == NULL) {
        fprintf(stderr, "ldap_initialize failed\n");
        exit(EXIT_FAILURE);
    }
    
    ldap_set_option(ld, LDAP_OPT_PROTOCOL_VERSION, &version);

    // 绑定到LDAP服务器
    struct berval cred;
    cred.bv_val = password;
    cred.bv_len = strlen(password);
    
    rc = ldap_sasl_bind_s(ld, bind_dn, LDAP_SASL_SIMPLE, &cred, NULL, NULL, NULL);
    if (rc != LDAP_SUCCESS) {
        ldap_unbind_ext_s(ld, NULL, NULL);
        fprintf(stderr, "ldap_sasl_bind_s: %s\n", ldap_err2string(rc));
        exit(EXIT_FAILURE);
    }
    
    // 进行Who am I?操作
    rc = ldap_whoami(ld, NULL, NULL, &msgid);
    if (rc != LDAP_SUCCESS) {
        ldap_unbind_ext_s(ld, NULL, NULL);
        fprintf(stderr, "ldap_whoami: %s\n", ldap_err2string(rc));
        exit(EXIT_FAILURE);
    }
    
    // 等待返回结果
    LDAPMessage *res;
    struct berval *authzid;
    
    rc = ldap_result(ld, msgid, LDAP_MSG_ALL, NULL, &res);
    if (rc == -1) {
        ldap_unbind_ext_s(ld, NULL, NULL);
        fprintf(stderr, "ldap_result failed\n");
        exit(EXIT_FAILURE);
    }
    rc = ldap_parse_whoami(ld, res, &authzid);
    if (rc != LDAP_SUCCESS) {
        ldap_unbind_ext_s(ld, NULL, NULL);
        fprintf(stderr, "ldap_parse_whoami: %s\n", ldap_err2string(rc));
        exit(EXIT_FAILURE);
    }
    ldap_msgfree(res);

    printf("Authorized identity: %s\n", authzid->bv_val);
    ber_bvfree(authzid);

    // 清理
    ldap_unbind_ext_s(ld, NULL, NULL);

    return 0;
}
EOF'''
        self.cmd(code)
        self.pwd = os.path.realpath(os.path.dirname(__file__))
        self.cmd(f'bash {self.pwd}/openldap_pre.sh')
        self.cmd("gcc -o openldap_ldap_whoami openldap_ldap_whoami.c -lldap -llber")

    def test(self):
        self.cmd("./openldap_ldap_whoami 'ldap://localhost:389' 'cn=admin,dc=domain,dc=com' 'root'")


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f'bash {self.pwd}/openldap_post.sh')
        for f in ['openldap_ldap_whoami.c', 'openldap_ldap_whoami']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")