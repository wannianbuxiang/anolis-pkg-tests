#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_openldap_openldap_func_010.py
@Time:      2024/03/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_openldap_openldap_func_010.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc openldap openldap-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > openldap_ldap_search_ext.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <ldap.h>

int main(int argc, char **argv) {
    if (argc < 6) {
        fprintf(stderr, "Usage: %s <ldap_uri> <search_base> <search_scope> <search_filter> <attributes...>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    LDAP *ld;
    LDAPMessage *result, *entry;
    int rc, ldap_version = LDAP_VERSION3;
    char **values;
    int msgid, search_scope;
    struct timeval timeout = {10, 0}; // 10秒超时

    // 初始化和设置LDAP会话参数
    rc = ldap_initialize(&ld, argv[1]);
    if (rc != LDAP_SUCCESS) {
        fprintf(stderr, "ldap_initialize: %s\n", ldap_err2string(rc));
        return 1;
    }
    ldap_set_option(ld, LDAP_OPT_PROTOCOL_VERSION, &ldap_version);

    char *search_base = argv[2];
    char *search_scope_str = argv[3];
    char *search_filter = argv[4];
    char **attrs = &argv[5];

    if (strcmp(search_scope_str, "base") == 0) search_scope = LDAP_SCOPE_BASE;
    else if (strcmp(search_scope_str, "one") == 0) search_scope = LDAP_SCOPE_ONELEVEL;
    else search_scope = LDAP_SCOPE_SUBTREE;

    // 发起搜索请求
    rc = ldap_search_ext(ld, search_base, search_scope, search_filter, attrs, 0, NULL, NULL, &timeout, 0, &msgid);
    if (rc != LDAP_SUCCESS) {
        fprintf(stderr, "ldap_search_ext: %s\n", ldap_err2string(rc));
        ldap_unbind_ext(ld, NULL, NULL);
        return 1;
    }

    // 等待并获取搜索结果
    rc = ldap_result(ld, msgid, LDAP_MSG_ALL, &timeout, &result);
    if (rc == -1) {
        fprintf(stderr, "ldap_result: %s\n", ldap_err2string(rc));
        ldap_msgfree(result);
        ldap_unbind_ext(ld, NULL, NULL);
        return 1;
    }

    // 遍历所有的搜索结果条目
    for (entry = ldap_first_entry(ld, result); entry != NULL; entry = ldap_next_entry(ld, entry)) {
        char *dn = ldap_get_dn(ld, entry);
        printf("DN: %s\n", dn);
        ldap_memfree(dn);

        // 遍历所有请求的属性
        for (int i = 0; attrs[i] != NULL; i++) {
            values = ldap_get_values(ld, entry, attrs[i]);
            if (values != NULL) {
                for (int j = 0; values[j] != NULL; j++) {
                    printf("%s: %s\n", attrs[i], values[j]);
                }
                ldap_value_free(values);
            }
        }
    }

    // 释放资源
    ldap_msgfree(result);
    ldap_unbind_ext(ld, NULL, NULL);
    return 0;
}
EOF'''
        self.cmd(code)
        self.pwd = os.path.realpath(os.path.dirname(__file__))
        self.cmd(f'bash {self.pwd}/openldap_pre.sh')     
        self.cmd("gcc -o openldap_ldap_search_ext openldap_ldap_search_ext.c -lldap -Wno-implicit-function-declaration -Wno-int-conversion")

    def test(self):
        self.cmd("./openldap_ldap_search_ext 'ldap://localhost' 'dc=domain,dc=com' 'sub' '(objectclass=*)' 'cn' 'mail'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f'bash {self.pwd}/openldap_post.sh')        
        for f in ['openldap_ldap_search_ext.c', 'openldap_ldap_search_ext']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")