#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_openldap_openldap_func_005.py
@Time:      2024/03/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_openldap_openldap_func_005.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc openldap openldap-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > openldap_ldap_get_dn.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <ldap.h>

int main(int argc, char **argv) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <ldap_uri> <search_base>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    LDAP *ld;
    LDAPMessage *result, *e;
    char *ldap_uri = argv[1];
    char *search_base = argv[2];
    int rc, version = LDAP_VERSION3;
    char *dn;

    // 初始化LDAP连接
    rc = ldap_initialize(&ld, ldap_uri);
    if (rc != LDAP_SUCCESS) {
        fprintf(stderr, "ldap_initialize failed: %s\n", ldap_err2string(rc));
        exit(EXIT_FAILURE);
    }
    ldap_set_option(ld, LDAP_OPT_PROTOCOL_VERSION, &version);

    // 执行搜索操作
    rc = ldap_search_ext_s(ld, search_base, LDAP_SCOPE_SUBTREE, "(objectClass=*)",
                           NULL, 0, NULL, NULL, NULL, LDAP_NO_LIMIT, &result);
    if (rc != LDAP_SUCCESS) {
        ldap_unbind_ext(ld, NULL, NULL);
        fprintf(stderr, "ldap_search_ext_s failed: %s\n", ldap_err2string(rc));
        exit(EXIT_FAILURE);
    }

    // 遍历搜索结果
    for (e = ldap_first_entry(ld, result); e != NULL; e = ldap_next_entry(ld, e)) {
        // 使用ldap_get_dn来获取DN
        dn = ldap_get_dn(ld, e);
        if (dn != NULL) {
            printf("DN: %s\n", dn);
            // 释放为dn分配的内存
            ldap_memfree(dn);
        } else {
            // 处理获取DN失败的情况（如内存不足等）
            fprintf(stderr, "ldap_get_dn failed\n");
        }
    }

    // 释放搜索结果
    ldap_msgfree(result);
    // 关闭LDAP连接
    ldap_unbind_ext(ld, NULL, NULL);

    return 0;
}
EOF'''
        self.cmd(code)
        self.pwd = os.path.realpath(os.path.dirname(__file__))
        self.cmd(f'bash {self.pwd}/openldap_pre.sh')    
        self.cmd("gcc -o openldap_ldap_get_dn openldap_ldap_get_dn.c -lldap")

    def test(self):
        self.cmd("./openldap_ldap_get_dn 'ldap://localhost' 'dc=domain,dc=com'")


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f'bash {self.pwd}/openldap_post.sh')
        for f in ['openldap_ldap_get_dn.c', 'openldap_ldap_get_dn']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")