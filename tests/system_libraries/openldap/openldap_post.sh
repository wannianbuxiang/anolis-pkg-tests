#! /usr/bin/bash

clean() {
    LDAP_CONF_P="/etc/openldap/slapd.d/cn=config"
    systemctl is-active slapd && systemctl stop slapd
    if [ -f "/tmp/monitor.ldif" ];then
        [ -f ${LDAP_CONF_P}/'olcDatabase={1}monitor.ldif' ] && rm -rf ${LDAP_CONF_P}/'olcDatabase={1}monitor.ldif'
        mv "/tmp/monitor.ldif" ${LDAP_CONF_P}/'olcDatabase={1}monitor.ldif'
    fi 
    if [ -f "/tmp/mdb.ldif" ];then
        [ -f ${LDAP_CONF_P}/'olcDatabase={2}mdb.ldif' ] && rm -rfv ${LDAP_CONF_P}/'olcDatabase={2}mdb.ldif'
        mv /tmp/mdb.ldif ${LDAP_CONF_P}/'olcDatabase={2}mdb.ldif'
    fi

    [ -f /tmp/user_base.ldif ] && rm -rfv /tmp/user_base.ldif
    [ -f /tmp/delete_entries.ldif ] && rm -rfv /tmp/delete_entries.ldif
    yum remove openldap-devel openldap-servers openldap-clients -y -q
}

clean