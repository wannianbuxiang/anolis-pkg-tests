#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_openldap_openldap_func_011.py
@Time:      2024/03/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_openldap_openldap_func_011.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc openldap openldap-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > openldap_ldap_url_desc2str.c << EOF
#include <stdio.h>
#include <ldap.h>

int main(int argc, char **argv) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <ldap_url>\n", argv[0]);
        return 1;
    }

    LDAPURLDesc *ludp;
    int rc = ldap_url_parse(argv[1], &ludp);
    if (rc != LDAP_SUCCESS) {
        fprintf(stderr, "Failed to parse LDAP URL: %s\n", argv[1]);
        return 1;
    }

    char *urlStr = ldap_url_desc2str(ludp);
    if (urlStr == NULL) {
        fprintf(stderr, "Failed to convert LDAP URLDesc to string\n");
        ldap_free_urldesc(ludp);
        return 1;
    }

    printf("Parsed LDAP URL: %s\n", urlStr);
    ldap_memfree(urlStr); // Use ldap_memfree as per LDAP memory management rules
    ldap_free_urldesc(ludp);

    return 0;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o openldap_ldap_url_desc2str openldap_ldap_url_desc2str.c -lldap")

    def test(self):
        self.cmd("./openldap_ldap_url_desc2str 'ldap://localhost:389/dc=domain,dc=com'")
        self.cmd("./openldap_ldap_url_desc2str 'ldap://localhost:389/dc=example,dc=com'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['openldap_ldap_url_desc2str.c', 'openldap_ldap_url_desc2str']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")