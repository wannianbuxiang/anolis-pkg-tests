#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_openldap_openldap_func_001.py
@Time:      2024/03/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_openldap_openldap_func_001.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc openldap openldap-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > openldap_set_option.c << EOF
#include <stdio.h>
#include <ldap.h>

int main() {
    LDAP *ld;
    int version = LDAP_VERSION3;
    int rc;
    
    // 初始化LDAP结构
    rc = ldap_initialize(&ld, "ldap:///");
    if (rc != LDAP_SUCCESS) {
        fprintf(stderr, "ldap_initialize: %s\n", ldap_err2string(rc));
        return 1;
    }
    fprintf(stdout, "ldap was successfully initialized\n");
	
    // 设置LDAP版本
    rc = ldap_set_option(ld, LDAP_OPT_PROTOCOL_VERSION, &version);
    if (rc != LDAP_SUCCESS) {
        ldap_unbind_ext(ld, NULL, NULL);
        fprintf(stderr, "ldap_set_option: %s\n", ldap_err2string(rc));
        return 1;
    }
	fprintf(stdout, "Succeeded in setting the LDAP version\n");
   
    //清理LDAP结构
    ldap_unbind_ext(ld, NULL, NULL);
    return 0;
}
EOF'''  
        self.cmd(code)
        self.cmd("gcc -o openldap_set_option openldap_set_option.c -lldap")

    def test(self):
        self.cmd("./openldap_set_option")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['openldap_set_option.c', 'openldap_set_option']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")