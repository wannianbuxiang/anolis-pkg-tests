#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_openldap_openldap_func_007.py
@Time:      2024/03/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_openldap_openldap_func_007.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc openldap openldap-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > openldap_ldap_get_entry_controls.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <ldap.h>

int main(int argc, char *argv[]) {
    if (argc < 7) {
        fprintf(stderr, "Usage: %s <ldap_uri> <bind_dn> <password> <search_base> <search_filter> <attribute>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    const char *ldap_uri = argv[1];
    const char *bind_dn = argv[2];
    const char *password = argv[3];
    const char *search_base = argv[4];
    const char *search_filter = argv[5];
    char *attrs[] = { argv[6], NULL };

    LDAP *ld = NULL;
    LDAPMessage *result = NULL, *entry = NULL;
    LDAPControl **entryControls = NULL;
    int rc, version = LDAP_VERSION3;

    // Initialize LDAP connection
    rc = ldap_initialize(&ld, ldap_uri);
    if (rc != LDAP_SUCCESS) {
        fprintf(stderr, "ldap_initialize: %s\n", ldap_err2string(rc));
        exit(EXIT_FAILURE);
    }

    // Set option for LDAP version
    ldap_set_option(ld, LDAP_OPT_PROTOCOL_VERSION, &version);

    // 绑定到LDAP服务器
    struct berval cred;
    cred.bv_val = password;
    cred.bv_len = strlen(password);
    
    rc = ldap_sasl_bind_s(ld, bind_dn, LDAP_SASL_SIMPLE, &cred, NULL, NULL, NULL);
    if (rc != LDAP_SUCCESS) {
        ldap_unbind_ext_s(ld, NULL, NULL);
        fprintf(stderr, "ldap_sasl_bind_s: %s\n", ldap_err2string(rc));
        exit(EXIT_FAILURE);
    }

    // Perform search
    rc = ldap_search_ext_s(ld, search_base, LDAP_SCOPE_SUBTREE, search_filter, attrs, 0, NULL, NULL, NULL, 0, &result);
    if (rc != LDAP_SUCCESS) {
        ldap_unbind_ext(ld, NULL, NULL);
        fprintf(stderr, "ldap_search_ext_s: %s\n", ldap_err2string(rc));
        exit(EXIT_FAILURE);
    }

    for (entry = ldap_first_entry(ld, result); entry != NULL; entry = ldap_next_entry(ld, entry)) {
        char *dn = ldap_get_dn(ld, entry);
        printf("Processing entry: %s\n", dn);
        ldap_memfree(dn);  // Free DN after printing

        rc = ldap_get_entry_controls(ld, entry, &entryControls);
        if (rc != LDAP_SUCCESS) {
            fprintf(stderr, "ldap_get_entry_controls: %s\n", ldap_err2string(rc));
            continue;
        }

        if (entryControls) {
            printf("Entry controls found for the above DN\n");
            // Process and display control information here...
            ldap_controls_free(entryControls);
        } else {
            // printf("No entry controls found for the above DN\n");
        }
    }

    // Clean up memory used by the search result
    ldap_msgfree(result);
    ldap_unbind_ext(ld, NULL, NULL);
    return 0;
}
EOF'''
        self.cmd(code)
        self.pwd = os.path.realpath(os.path.dirname(__file__))
        self.cmd(f'bash {self.pwd}/openldap_pre.sh')  
        self.cmd("gcc -o openldap_ldap_get_entry_controls openldap_ldap_get_entry_controls.c -lldap -Wno-discarded-qualifiers")

    def test(self):
        self.cmd("./openldap_ldap_get_entry_controls 'ldap://localhost' 'cn=admin,dc=domain,dc=com' 'root' 'dc=domain,dc=com' '(objectclass=*)' 'dc'")


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f'bash {self.pwd}/openldap_post.sh')
        for f in ['openldap_ldap_get_entry_controls.c', 'openldap_ldap_get_entry_controls']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")