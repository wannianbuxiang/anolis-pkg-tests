#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_openldap_openldap_func_009.py
@Time:      2024/03/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_openldap_openldap_func_009.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc openldap openldap-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > openldap_ldap_is_ldap_url.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <ldap.h>

int main(int argc, char **argv) {
    if (argc < 2) {
        fprintf(stderr, "Usage: %s <url1> [url2 ...]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    for (int i = 1; i < argc; i++) {
        int is_ldap_url = ldap_is_ldap_url(argv[i]);
        printf("%s is %sa valid LDAP URL\n", argv[i], is_ldap_url ? "" : "not ");
    }

    return 0;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o openldap_ldap_is_ldap_url openldap_ldap_is_ldap_url.c -lldap")

    def test(self):
        self.cmd("./openldap_ldap_is_ldap_url ldap://localhost:389 ldaps://localhost:636 http://example.com")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['openldap_ldap_is_ldap_url.c', 'openldap_ldap_is_ldap_url']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")