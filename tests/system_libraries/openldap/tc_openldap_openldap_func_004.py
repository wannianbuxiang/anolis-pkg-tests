#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_openldap_openldap_func_004.py
@Time:      2024/03/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_openldap_openldap_func_004.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc openldap openldap-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > openldap_ldap_url_parse.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <ldap.h>

int main(int argc, char **argv) {
    LDAPURLDesc *lud;
    int result;

    if (argc != 2) {
        fprintf(stderr, "Usage: %s ldap://host:port/dn[?attributes[?scope[?filter[?exts]]]]]\n", argv[0]);
        return EXIT_FAILURE;
    }

    result = ldap_url_parse(argv[1], &lud);
    if (result != LDAP_SUCCESS) {
        fprintf(stderr, "ldap_url_parse() failed: %s\n", ldap_err2string(result));
        return EXIT_FAILURE;
    }

    // Print the components of the LDAP URL
    printf("Host: %s\n", lud->lud_host);
    printf("Port: %d\n", lud->lud_port);
    printf("DN: %s\n", lud->lud_dn);
    printf("Scope: %d\n", lud->lud_scope);

    // Attributes
    if (lud->lud_attrs) {
        printf("Attributes: ");
        for (int i = 0; lud->lud_attrs[i]; i++) {
            printf("%s ", lud->lud_attrs[i]);
        }
        printf("\n");
    }

    // Filter
    if (lud->lud_filter) {
        printf("Filter: %s\n", lud->lud_filter);
    }

    // Extensions
    if (lud->lud_exts) {
        printf("Extensions: ");
        for (int i = 0; lud->lud_exts[i]; i++) {
            printf("%s ", lud->lud_exts[i]);
        }
        printf("\n");
    }

    ldap_free_urldesc(lud);
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o openldap_ldap_url_parse openldap_ldap_url_parse.c -lldap -llber")

    def test(self):
        self.cmd("./openldap_ldap_url_parse 'ldap://localhost:389/dc=domain,dc=com?uid?sub?(objectClass=*)'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['openldap_ldap_url_parse.c', 'openldap_ldap_url_parse']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")