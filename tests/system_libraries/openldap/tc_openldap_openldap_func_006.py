#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_openldap_openldap_func_006.py
@Time:      2024/03/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_openldap_openldap_func_006.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc openldap openldap-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > openldap_ldap_parse_ldif_record.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <ldap.h>

int main(int argc, char **argv){
    if (argc != 2) {
        fprintf(stderr, "Usage: %s '<ldif_record>'\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    LDAP *ld;
    LDAPMessage *result = NULL;
    int rc;
    char *ldif_record = argv[1];
    struct berval rbuf;
    rbuf.bv_val = ldif_record;
    rbuf.bv_len = strlen(ldif_record);

    LDIFRecord lr;
    unsigned long linenum = 1; // 假设是从第一行开始的
    const char *errstr = NULL;
    unsigned int flags = 0; // 需要根据实际情况设置

    // 初始化LDAP连接
    ldap_initialize(&ld, "ldap://yourldapserver.com");
    if(ld == NULL) {
        fprintf(stderr, "ldap_initialize failed\n");
        return 1;
    }

    // 进行LDIF解析
    rc = ldap_parse_ldif_record(&rbuf, linenum, &lr, errstr, flags);
    if(rc != LDAP_SUCCESS){
        fprintf(stderr, "LDIF record parse error: %s\n", ldap_err2string(rc));
        ldap_unbind_ext(ld, NULL, NULL);
        return 1;
    }

    // 输出解析后的信息
    printf("Operation: %ld\n", (long)lr.lr_op);
    printf("DN: %s\n", lr.lr_dn.bv_val);
    // 释放LDAP资源
    ldap_unbind_ext(ld, NULL, NULL);

    return 0;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o openldap_ldap_parse_ldif_record openldap_ldap_parse_ldif_record.c -lldap")

    def test(self):
        self.cmd("./openldap_ldap_parse_ldif_record 'dn: cn=John Doe,dc=example,dc=com\nchangetype: modify\nreplace: mail\nmail: jdoe@example.com\n-\n'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['openldap_ldap_parse_ldif_record.c', 'openldap_ldap_parse_ldif_record']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")