#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_openldap_openldap_func_008.py
@Time:      2024/03/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_openldap_openldap_func_008.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc openldap openldap-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > openldap_ldap_get_values.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <ldap.h>

int main(int argc, char *argv[]) {
    LDAP *ld;
    LDAPMessage *result, *entry;
    char *ldap_uri;
    char *search_base;
    char *search_filter;
    char *attribute;
    char **values;
    int rc, ldap_version = LDAP_VERSION3;
    
    if (argc != 5) {
        fprintf(stderr, "Usage: %s ldap_uri search_base search_filter target_attribute\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    
    ldap_uri = argv[1];
    search_base = argv[2];
    search_filter = argv[3];
    attribute = argv[4];

    // 初始化一个LDAP会话
    if ((rc = ldap_initialize(&ld, ldap_uri)) != LDAP_SUCCESS) {
        fprintf(stderr, "ldap_initialize: %s\n", ldap_err2string(rc));
        exit(EXIT_FAILURE);
    }
    
    // 设置LDAP协议版本
    ldap_set_option(ld, LDAP_OPT_PROTOCOL_VERSION, &ldap_version);
    
    // 进行简单绑定（如果需要）
    // rc = ldap_simple_bind_s(ld, binddn, password);
    // 检查错误情况...

    // 执行搜索操作
    rc = ldap_search_ext_s(ld, search_base, LDAP_SCOPE_SUBTREE, search_filter, NULL, 0, NULL, NULL, NULL, 0, &result);
    if (rc != LDAP_SUCCESS) {
        fprintf(stderr, "ldap_search_ext_s: %s\n", ldap_err2string(rc));
        ldap_unbind_ext(ld, NULL, NULL);
        exit(EXIT_FAILURE);
    }
    
    // 遍历搜索结果
    entry = ldap_first_entry(ld, result);
    while (entry != NULL) {
        // 获取属性的值
        values = ldap_get_values(ld, entry, attribute);
        
        if (values != NULL) {
            // 打印所有的值
            for (int i = 0; values[i] != NULL; i++) {
                printf("%s: %s\n", attribute, values[i]);
            }
            // 释放获取到的值
            ldap_value_free(values);
        } else {
            // printf("No attribute %s found for this entry\n", attribute);
        }
        
        // 移动到下条记录
        entry = ldap_next_entry(ld, entry);
    }
    
    // 释放搜索结果
    ldap_msgfree(result);
    
    // 解除绑定并关闭会话
    ldap_unbind_ext_s(ld, NULL, NULL);
    
    return 0;
}
EOF'''
        self.cmd(code)
        self.pwd = os.path.realpath(os.path.dirname(__file__))
        self.cmd(f'bash {self.pwd}/openldap_pre.sh')      
        self.cmd("gcc -o openldap_ldap_get_values openldap_ldap_get_values.c -lldap")

    def test(self):
        self.cmd("./openldap_ldap_get_values 'ldap://localhost' 'dc=domain,dc=com' '(objectclass=*)' 'dc'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd(f'bash {self.pwd}/openldap_post.sh')
        for f in ['openldap_ldap_get_values.c', 'openldap_ldap_get_values']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")