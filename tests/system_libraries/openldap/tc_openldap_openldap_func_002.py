#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_openldap_openldap_func_002.py
@Time:      2024/03/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_openldap_openldap_func_002.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc openldap openldap-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > openldap_ldap_control_find.c << EOF
#include <stdio.h>
#include <ldap.h>

int main(int argc, char *argv[]) {
    if (argc < 2) {
        fprintf(stderr, "Usage: %s <OID>\n", argv[0]);
        return 1;
    }

    // 模拟传入的LDAP控制数组
    // struct berval val1 = {0, NULL};
    LDAPControl ctrl1 = {argv[1], 1, {0, NULL}}; // 分页控制的示例OID
    // 假设有更多控制...
    LDAPControl *ctrls[] = {&ctrl1, NULL}; // 控制数组结束应该用NULL保持显式

    // 从命令行参数获取 OID
    char *search_oid = argv[1];
    LDAPControl **next_ctrl = NULL; // 如果你想获取后续的控制，这里应该分配内存

    // 查找控制
    LDAPControl *found_ctrl = ldap_control_find(search_oid, ctrls, &next_ctrl);
    
    if (found_ctrl != NULL) {
        printf("Found control with OID: %s\n", found_ctrl->ldctl_oid);
    } else {
        printf("Control with OID %s not found.\n", search_oid);
    }

    return 0;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o openldap_ldap_control_find openldap_ldap_control_find.c -lldap -llber")

    def test(self):
        self.cmd("./openldap_ldap_control_find '1.2.840.113556.1.4.319'")
        self.cmd("./openldap_ldap_control_find 'ctr000000'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['openldap_ldap_control_find.c', 'openldap_ldap_control_find']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")