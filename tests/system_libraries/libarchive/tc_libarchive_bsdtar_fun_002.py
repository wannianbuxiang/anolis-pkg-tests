#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

"""
@File:      tc_libarchive_bsdtar_fun_002.py
@Time:      2024/08/05 18:07:20
@Author:    wangge
@Version:   1.0
@Contact:   YS.wangge@h3c.com
@License:   Mulan PSL v2
@Modify:    wangge
"""
import subprocess
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libarchive_bsdtar_fun_002.yaml for details

    :avocado: tags=P0,noarch,local
    """
    PARAM_DIC = {"pkg_name": "bsdtar"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        

    def test(self):

        result = subprocess.run('echo "111" >testfile1', shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        self.assertEqual(result.returncode,0,"check create file fail")

        result = subprocess.run('bsdtar -czvf test1.tar.gz testfile1', shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        self.assertEqual(result.returncode,0,"tar file fail")

        result = subprocess.run('test -f test1.tar.gz', shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        self.assertEqual(result.returncode,0,"the file not exist")

        result = subprocess.run('bsdtar --help|grep bsdtar', shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        self.assertEqual(result.returncode,0,"check command fail")

        result = subprocess.run('bsdtar --version|grep libarchive', shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        self.assertEqual(result.returncode,0,"check command fail")
        
        

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        subprocess.run("rm -rf testfile1 test1.tar.gz",shell=True,check=True)
        
    

 