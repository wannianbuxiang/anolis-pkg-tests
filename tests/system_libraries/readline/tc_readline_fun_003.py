# -*- encoding: utf-8 -*-

"""
@File:      tc_readline_fun_003.py
@Time:      2024-02-29 15:30:00
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_readline_fun_003.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "readline readline-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline='''cat > readline_fun_003.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include <readline/readline.h>
#include <readline/history.h>

int main(void) {
    using_history(); // 初始化历史功能

    // 预定义一个要添加到历史记录的命令列表
    char* commands[] = {
        "ls -l",
        "echo Hello, World!",
        NULL // 结束标记
    };

    // 遍历列表并逐个添加命令到历史记录
    for (int i = 0; commands[i] != NULL; ++i) {
        add_history(commands[i]);
    }

    // 打印当前历史记录
    for (int i = 0; history_get(i+1) != NULL; ++i) {
        printf("%d: %s\\n", i + 1, history_get(i+1)->line);
    }

    // 清理
    clear_history();
    return 0;
}
EOF
'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o readline_fun_003 readline_fun_003.c -lhistory -lreadline")
        code, result = self.cmd("file readline_fun_003")
        self.assertIn("executable", result)
        code, result = self.cmd("./readline_fun_003")
        self.assertIn("ls -l", result)
        self.assertIn("echo Hello, World!", result)
         
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf readline_fun_003 readline_fun_003.c")