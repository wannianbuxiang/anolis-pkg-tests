#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_librepo_librepo_func_004.py
@Time:      2024/02/26 15:15:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_librepo_librepo_func_004.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc librepo librepo-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        repoconf = r'''cat > repoconf.repo << EOF
[repositoryid]
name=
baseurl=
enabled=
gpgcheck=
gpgkey=
metalink=
mirrorlist=
failovermethod=
priority=
exclude=  #与includepkgs互斥
includepkgs=
sslverify=
proxy=
timeout=
skip_if_unavailable=
enabled_metadata=
EOF'''
        code = r'''cat > test_librepo_repoconfs_parse.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <librepo/librepo.h>

int main(int argc, char **argv) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <path_to_repo_file>\n", argv[0]);
        return EXIT_FAILURE;
    }

    const char *repo_file_path = argv[1];
    GError *error = NULL;
    LrYumRepoConfs *confs = lr_yum_repoconfs_init();
    
    if (!confs) {
        fprintf(stderr, "Cannot initialize YUM repo configs\n");
        return EXIT_FAILURE;
    }

    // Parse the specified .repo file
    if (!lr_yum_repoconfs_parse(confs, repo_file_path, &error)) {
        fprintf(stderr, "Cannot parse YUM repo config file '%s': %s\n", repo_file_path, error->message);
        g_error_free(error);
        lr_yum_repoconfs_free(confs);
        return EXIT_FAILURE;
    }
    printf("Successfully parsed YUM repo config file '%s'\n", repo_file_path);
    lr_yum_repoconfs_free(confs);
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(repoconf)
        self.cmd(code)
        self.cmd("gcc -o test_librepo_repoconfs_parse test_librepo_repoconfs_parse.c `pkg-config --cflags --libs librepo`")

    def test(self):
        self.cmd(f"./test_librepo_repoconfs_parse 'repoconf.repo'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['test_librepo_repoconfs_parse.c', 'test_librepo_repoconfs_parse', 'repoconf.repo']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}", ignore_status=True)
