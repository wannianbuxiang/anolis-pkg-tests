#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_librepo_librepo_func_005.py
@Time:      2024/02/26 15:15:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_librepo_librepo_func_005.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc librepo librepo-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        repomod_file = r'''cat << EOF > repomd_005.xml
<?xml version="1.0" encoding="UTF-8"?>
<repomd xmlns="http://linux.duke.edu/metadata/repo">
  <revision>1590492027</revision>
  <data type="primary">
    <checksum type="sha256">1234567890abcdef1234567890abcdef1234567890abcdef1234567890abcdef</checksum>
    <location href="repodata/1234567890abcdef-primary.xml.gz"/>
    <timestamp>123456789</timestamp>
    <size>1234</size>
    <database_version>1</database_version>
  </data>
  <data type="filelists">
    <checksum type="sha256">abcdef1234567890abcdef1234567890abcdef1234567890abcdef1234567890</checksum>
    <location href="repodata/abcdef1234567890-filelists.xml.gz"/>
    <timestamp>123456789</timestamp>
    <size>5678</size>
    <database_version>1</database_version>
  </data>
</repomd>
EOF'''
        code = r'''cat > test_librepo_repomd_parse_file.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <librepo/librepo.h>
#include <glib.h>

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <repomd.xml path>\n", argv[0]);
        return 1;
    }

    const char *repomd_path = argv[1];
    GError *err = NULL;
    LrYumRepoMd *repomd = lr_yum_repomd_init();
    int fd = open(repomd_path, O_RDONLY);
    if (fd == -1) {
        perror("Error opening file");
        lr_yum_repomd_free(repomd);
        return 1;
    }

    // 解析repomd.xml文件
    if (!lr_yum_repomd_parse_file(repomd, fd, NULL, NULL, &err)) {
        fprintf(stderr, "Error parsing repomd.xml: %s\n", err->message);
        g_error_free(err);
        close(fd);
        lr_yum_repomd_free(repomd);
        return 1;
    }

    // 输出解析结果
    if (repomd->records == NULL) {
        printf("No records found in repomd.xml\n");
    } else {
        for (GList *elem = repomd->records; elem; elem = g_list_next(elem)) {
            LrYumRepoMdRecord *record = elem->data;
            printf("\n==========\n");
            printf("Type: %s\n", record->type);
            printf("Location: %s\n", record->location_href);
            printf("size: %ld\n", record->size);
            printf("timestamp: %ld\n", record->timestamp);
            printf("database_version: %d\n", record->db_version);
        }
    }

    // 清理工作
    close(fd);
    lr_yum_repomd_free(repomd);
    return 0;
}
EOF'''
        self.cmd(repomod_file)
        self.cmd(code)
        self.cmd("gcc -o test_librepo_repomd_parse_file test_librepo_repomd_parse_file.c `pkg-config --cflags --libs librepo` -Wno-int-conversion -Wno-incompatible-pointer-types")

    def test(self):
        self.cmd(f"./test_librepo_repomd_parse_file repomd_005.xml")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['test_librepo_repomd_parse_file.c', 'test_librepo_repomd_parse_file', 'repomd_005.xml']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}", ignore_status=True)
