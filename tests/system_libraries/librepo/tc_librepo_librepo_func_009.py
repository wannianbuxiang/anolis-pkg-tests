#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_librepo_librepo_func_009.py
@Time:      2024/02/26 15:15:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_librepo_librepo_func_009.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc librepo librepo-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        mirrorlist_file = r'''cat <<EOF > metalink.xml
<?xml version="1.0" encoding="utf-8"?>
<metalink version="3.0" xmlns="http://www.metalinker.org/">
  <files>
    <file name="examplefile.tar.gz">
      <verification>
        <hash type="sha256">e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855</hash>
      </verification>
      <resources>
        <url type="http" preference="100">http://mirror1.example.com/examplefile.tar.gz</url>
        <url type="ftp" preference="80">ftp://mirror2.example.com/examplefile.tar.gz</url>
      </resources>
    </file>
  </files>
</metalink>
EOF'''
        code = r'''cat > test_librepo_metalink_parse_file.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <librepo/librepo.h>
#include <glib.h>

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <metalink file path>\n", argv[0]);
        return 1;
    }
    
    const char *metalink_path = argv[1];
    GError *err = NULL;
    LrMetalink *metalink = NULL; // 用于保存解析后的 Metalink 数据
    int fd = open(metalink_path, O_RDONLY);
    
    if (fd == -1) {
        perror("Error opening Metalink file");
        return 1;
    }
    
    // 解析 Metalink 文件
    metalink = lr_metalink_init();
    if (!lr_metalink_parse_file(metalink, fd, "examplefile.tar.gz", NULL, NULL, &err)) {
        fprintf(stderr, "Error parsing Metalink file: %s\n", err->message);
        g_error_free(err);
        close(fd);
        lr_metalink_free(metalink);
        return 1;
    }
    
    // 关闭文件描述符
    close(fd);
    
    // 打印解析后的数据
    printf("Filename: %s\n", metalink->filename);
    printf("Timestamp: %lld\n", (long long) metalink->timestamp);
    printf("Size: %lld\n", (long long) metalink->size);

    // 遍历GSList并打印每个LrMetalinkUrl的详细信息
    for (GSList *elem = metalink->urls; elem; elem = g_slist_next(elem)) {
        LrMetalinkUrl *data = (LrMetalinkUrl *)elem->data;
        if (data) {
            printf("URL: %s\n", data->url);
            printf("Protocol: %s\n", data->protocol);
            printf("Type: %s\n", data->type);
            printf("Location: %s\n", data->location);
            printf("Preference: %d\n\n", data->preference);
        }
    }
    
    // 清理工作
    lr_metalink_free(metalink);
    
    return 0;
}
EOF'''
        self.cmd(mirrorlist_file)
        self.cmd(code)
        self.cmd("gcc -o test_librepo_metalink_parse_file test_librepo_metalink_parse_file.c `pkg-config --cflags --libs librepo`")

    def test(self):
        self.cmd(f"./test_librepo_metalink_parse_file metalink.xml")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['test_librepo_metalink_parse_file.c', 'test_librepo_metalink_parse_file', 'metalink.xml']:
          if os.path.exists(f):
              self.cmd(f"rm -rf {f}", ignore_status=True) 