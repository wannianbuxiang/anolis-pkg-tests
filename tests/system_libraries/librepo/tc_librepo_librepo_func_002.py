#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_librepo_librepo_func_002.py
@Time:      2024/02/26 15:15:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_librepo_librepo_func_002.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc librepo librepo-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_librepo_get_info.c << EOF
#include <librepo/librepo.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    if (argc < 2) {
        fprintf(stderr, "Usage: %s <repository_url>\n", argv[0]);
        return 1;
    }

    GError *error = NULL;
    LrHandle *handle = lr_handle_init();
    char *value = NULL;
    long long_value;
    GHashTable *varsub = NULL;

    if (handle == NULL) {
        fprintf(stderr, "无法初始化 librepo 句柄\n");
        return 1;
    }

    // 从命令行参数设置仓库URL
    const char *urls[] = {argv[1], NULL};
    // 为句柄设置一些选项
    lr_handle_setopt(handle, NULL, LRO_URLS, urls);
    lr_handle_setopt(handle, NULL, LRO_REPOTYPE, LR_YUMREPO);

    // 获取镜像列表地址
    if (!lr_handle_getinfo(handle, &error, LRO_MIRRORLIST, &value)) {
        fprintf(stderr, "获取镜像列表地址失败: %s\n", error->message);
    } else {
        printf("镜像列表地址: %s\n", value);
        g_free(value);
    }

    // 获取代理类型
    if (!lr_handle_getinfo(handle, &error, LRO_PROXYTYPE, &long_value)) {
        fprintf(stderr, "获取代理类型失败: %s\n", error->message);
    } else {
        printf("代理类型: %ld\n", long_value);
    }

    // 获取最大下载速度
    if (!lr_handle_getinfo(handle, &error, LRO_MAXSPEED, &long_value)) {
        fprintf(stderr, "获取最大下载速度失败: %s\n", error->message);
    } else {
        printf("最大下载速度: %ld\n", long_value);
    }

    // 获取目标目录
    if (!lr_handle_getinfo(handle, &error, LRO_DESTDIR, &value)) {
        fprintf(stderr, "获取目标目录失败: %s\n", error->message);
    } else {
        printf("目标目录: %s\n", value);
        g_free(value);
    }

    // 获取连接超时时间
    if (!lr_handle_getinfo(handle, &error, LRO_CONNECTTIMEOUT, &long_value)) {
        fprintf(stderr, "获取连接超时时间失败: %s\n", error->message);
    } else {
        printf("连接超时时间: %ld\n", long_value);
    }

    // 获取变量替换表
    if (!lr_handle_getinfo(handle, &error, LRI_VARSUB, &varsub)) {
        fprintf(stderr, "获取变量替换表失败: %s\n", error->message);
    } else {
        if (varsub) { // 确保varsub不是NULL
            printf("变量替换表:\n");
            GHashTableIter iter;
            gpointer key, value;
            g_hash_table_iter_init(&iter, varsub);
            while (g_hash_table_iter_next(&iter, &key, &value)) {
                printf("  %s = %s\n", (char *)key, (char *)value);
            }
            // 注意:这里不释放varsub,因为它是内部数据结构的引用
        } else {
            printf("变量替换表: NULL\n");
        }
    }
    return 0;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_librepo_get_info test_librepo_get_info.c `pkg-config --cflags --libs librepo`")

    def test(self):
        self.cmd(f"./test_librepo_get_info https://mirrors.openanolis.cn/")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['test_librepo_get_info.c', 'test_librepo_get_info']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}", ignore_status=True)    
