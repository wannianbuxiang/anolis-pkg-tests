#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_librepo_librepo_func_008.py
@Time:      2024/02/26 15:15:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_librepo_librepo_func_008.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc librepo librepo-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        mirrorlist_file = r'''cat <<EOF > mirrorlist.txt
# Official CentOS 7 Base repository mirrorlist
http://mirror.centos.org/centos/7/os/x86_64/
http://ftp.heanet.ie/pub/centos/7/os/x86_64/
http://mirror.facebook.net/centos/7/os/x86_64/
# This is a comment line - it is ignored
https://mirrors.kernel.org/centos/7/os/x86_64/
ftp://mirror.lstn.net/centos/7/os/x86_64/
EOF'''
        code = r'''cat > test_librepo_mirrorlist_parse_file.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <librepo/librepo.h>
#include <glib.h>

int main(int argc, char **argv) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <path_to_mirrorlist>\n", argv[0]);
        return EXIT_FAILURE;
    }

    GError *err = NULL;
    int fd;
    LrMirrorlist *mirrorlist;

    // Open the mirrorlist file for reading
    fd = open(argv[1], O_RDONLY);
    if (fd == -1) {
        perror("Error opening mirrorlist file");
        return EXIT_FAILURE;
    }

    // Initialize the LrMirrorlist structure
    mirrorlist = lr_mirrorlist_init();
    if (!mirrorlist) {
        fprintf(stderr, "Failed to initialize mirrorlist\n");
        close(fd);
        return EXIT_FAILURE;
    }

    // Parse the mirrorlist file
    if (!lr_mirrorlist_parse_file(mirrorlist, fd, &err)) {
        fprintf(stderr, "Error parsing mirrorlist file: %s\n", err->message);
        g_error_free(err);
        close(fd);
        lr_mirrorlist_free(mirrorlist);
        return EXIT_FAILURE;
    }

    // Print the URLs from the mirrorlist
    GSList *iter = mirrorlist->urls;
    while (iter != NULL) {
        char *url = (char *)iter->data; // 获取当前节点的数据，即 URL
        printf("%s\n", url); // 打印 URL
        iter = g_slist_next(iter); // 移动到下一个节点
    }

    // Cleanup resources
    close(fd);
    lr_mirrorlist_free(mirrorlist);
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(mirrorlist_file)
        self.cmd(code)
        self.cmd("gcc -o test_librepo_mirrorlist_parse_file test_librepo_mirrorlist_parse_file.c `pkg-config --cflags --libs librepo`")

    def test(self):
        self.cmd(f"./test_librepo_mirrorlist_parse_file mirrorlist.txt")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['test_librepo_mirrorlist_parse_file.c', 'test_librepo_mirrorlist_parse_file', 'mirrorlist.txt']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}", ignore_status=True)