#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_librepo_librepo_func_007.py
@Time:      2024/02/26 15:15:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_librepo_librepo_func_007.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc librepo librepo-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        repomod_file = r'''cat << EOF > repomd_007.xml
<?xml version="1.0" encoding="UTF-8"?>
<repomd xmlns="http://linux.duke.edu/metadata/repo">
  <revision>1590492027</revision>
  <data type="primary">
    <timestamp>123456789</timestamp>
  </data>
  <data type="filelists">
    <timestamp>123556789</timestamp>
  </data>
  <data type="updates">
    <timestamp>987654321</timestamp>
  </data>
  <data type="otother_db">
    <timestamp>123466789</timestamp>
  </data>
  <data type="group">
    <timestamp>987554321</timestamp>
  </data>
</repomd>
EOF'''
        code = r'''cat > test_librepo_repomd_get_highest_timestamp.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <librepo/librepo.h>
#include <glib.h>

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <repomd.xml path>\n", argv[0]);
        return EXIT_FAILURE;
    }
    const char *repomd_path = argv[1];
    GError *err = NULL;

    // Initialize the LrYumRepoMd structure
    LrYumRepoMd *repomd = lr_yum_repomd_init();
    if (!repomd) {
        fprintf(stderr, "Failed to initialize repomd\n");
        return EXIT_FAILURE;
    }

    // Open the repomd.xml file
    int fd = open(repomd_path, O_RDONLY);
    if (fd == -1) {
        perror("Error opening file");
        lr_yum_repomd_free(repomd);
        return EXIT_FAILURE;
    }

    // Parse the repomd.xml file
    if (!lr_yum_repomd_parse_file(repomd, fd, NULL, NULL, &err)) {
        fprintf(stderr, "Error parsing repomd.xml: %s\n", err->message);
        g_error_free(err);
        close(fd);
        lr_yum_repomd_free(repomd);
        return EXIT_FAILURE;
    }

    // Get the highest timestamp from the repomd
    gint64 highest_timestamp = lr_yum_repomd_get_highest_timestamp(repomd, &err);
    if (err) {
        fprintf(stderr, "Error getting highest timestamp: %s\n", err->message);
        g_error_free(err);
        close(fd);
        lr_yum_repomd_free(repomd);
        return EXIT_FAILURE;
    }

    // Print the highest timestamp
    printf("Highest timestamp in repomd.xml: %ld\n", (long)highest_timestamp);

    // Cleanup resources
    close(fd);
    lr_yum_repomd_free(repomd);
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(repomod_file)
        self.cmd(code)
        self.cmd("gcc -o test_librepo_repomd_get_highest_timestamp test_librepo_repomd_get_highest_timestamp.c `pkg-config --cflags --libs librepo` -Wno-int-conversion -Wno-incompatible-pointer-types")

    def test(self):
        self.cmd(f"./test_librepo_repomd_get_highest_timestamp repomd_007.xml")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['test_librepo_repomd_get_highest_timestamp.c', 'test_librepo_repomd_get_highest_timestamp', 'repomd_007.xml']:
          if os.path.exists(f):
              self.cmd(f"rm -rf {f}", ignore_status=True)