#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_librepo_librepo_func_001.py
@Time:      2024/02/26 15:15:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_librepo_librepo_func_001.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc librepo librepo-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        ret_c, self.arch = self.cmd("arch")
        self.assertEqual(0, ret_c)
        self.cmd("mkdir -p /tmp/librepo/", ignore_status=True)
        code = r'''cat > test_librepo_download_meta_data.c << EOF
#include <librepo/librepo.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <repository_url> <destination_directory>\n", argv[0]);
        return EXIT_FAILURE;
    }

    LrHandle *handle = NULL;
    LrResult *result = NULL;
    GError *err = NULL;
    gboolean ret = FALSE;

    // Create a new handle
    handle = lr_handle_init();
    if (handle == NULL) {
        fprintf(stderr, "Cannot create librepo handle\n");
        return EXIT_FAILURE;
    }

    // Set the repository URL from command line argument
    const char *url[] = {argv[1], NULL};
    ret = lr_handle_setopt(handle, NULL, LRO_URLS, url);
    if (!ret) {
        fprintf(stderr, "Cannot set URL option\n");
        goto cleanup;
    }

    // Set the destination directory for the metadata from command line argument
    ret = lr_handle_setopt(handle, &err, LRO_DESTDIR, argv[2]);
    if (!ret) {
        fprintf(stderr, "Cannot set destination directory option, %s\n", err->message);
        goto cleanup;
    }

    // Set the repository type to YUM
    ret = lr_handle_setopt(handle, NULL, LRO_REPOTYPE, LR_YUMREPO);
    if (!ret) {
        fprintf(stderr, "Cannot set repository type option\n");
        goto cleanup;
    }

    // Initialize the result object
    result = lr_result_init();
    if (result == NULL) {
        fprintf(stderr, "Cannot create librepo result\n");
        goto cleanup;
    }

    // Download repository metadata
    ret = lr_handle_perform(handle, result, &err);
    if (!ret) {
        fprintf(stderr, "Error downloading metadata: %s\n", err->message);
        goto cleanup;
    }

    // Successfully downloaded, you can add checks on the actual metadata here
    printf("Repository metadata successfully downloaded to %s\n", argv[2]);

cleanup:
    if (err) {
        g_error_free(err);
    }
    lr_handle_free(handle);
    lr_result_free(result);
    return ret ? EXIT_SUCCESS : EXIT_FAILURE;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_librepo_download_meta_data test_librepo_download_meta_data.c `pkg-config --cflags --libs librepo glib-2.0`")

    def test(self):
        self.cmd(f"./test_librepo_download_meta_data https://mirrors.openanolis.cn/anolis/epao/23/{self.arch}/ /tmp/librepo/")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['test_librepo_download_meta_data.c', 'test_librepo_download_meta_data', '/tmp/librepo']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}", ignore_status=True)
