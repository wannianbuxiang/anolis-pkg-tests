#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_librepo_librepo_func_003.py
@Time:      2024/02/26 15:15:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_librepo_librepo_func_003.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc librepo librepo-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_librepo_repoconfs_load_dir.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <librepo/librepo.h>

int main(int argc, char **argv) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <path_to_repo_configs>\n", argv[0]);
        return EXIT_FAILURE;
    }

    const char *repo_config_dir = argv[1];
    GError *error = NULL;
    LrYumRepoConfs *confs = lr_yum_repoconfs_init();
    
    if (!confs) {
        fprintf(stderr, "Cannot initialize YUM repo configs\n");
        return EXIT_FAILURE;
    }

    // 加载指定目录下的 yum 仓库配置
    if (!lr_yum_repoconfs_load_dir(confs, repo_config_dir, &error)) {
        fprintf(stderr, "Cannot load YUM repo configs from directory '%s': %s\n",
                repo_config_dir, error->message);
        g_error_free(error);
        lr_yum_repoconfs_free(confs);
        return EXIT_FAILURE;
    }

    // 从 LrYumRepoConfs 对象获取加载的配置列表
    GPtrArray *repoconfs = lr_yum_repoconfs_get_list(confs, &error);
    if (!repoconfs) {
        fprintf(stderr, "Cannot get YUM repo configs list: %s\n", error->message);
        g_error_free(error);
        lr_yum_repoconfs_free(confs);
        return EXIT_FAILURE;
    }

    // 打印加载的配置数量
    printf("Loaded %u YUM repo configs.\n", repoconfs->len);

    // 清理资源
    lr_yum_repoconfs_free(confs);
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_librepo_repoconfs_load_dir test_librepo_repoconfs_load_dir.c `pkg-config --cflags --libs librepo`")

    def test(self):
        self.cmd(f"./test_librepo_repoconfs_load_dir /etc/yum.repos.d/")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['test_librepo_repoconfs_load_dir.c', 'test_librepo_repoconfs_load_dir']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}", ignore_status=True)
