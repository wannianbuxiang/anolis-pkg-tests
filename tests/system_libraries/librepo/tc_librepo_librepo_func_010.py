#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_librepo_librepo_func_010.py
@Time:      2024/02/26 15:15:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_librepo_librepo_func_010.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc librepo librepo-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        mirrorlist_file = r'''cat <<EOF > checksum.txt
123456789
EOF'''
        code = r'''cat > test_librepo_checksum_fd_compare.c << EOF
#include <librepo/librepo.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <glib.h>

int main(int argc, char **argv) {
    if (argc != 4) {
        fprintf(stderr, "Usage: %s <file_path> <expected_checksum> <checksum_type>\n", argv[0]);
        return EXIT_FAILURE;
    }

    const char *file_path = argv[1];
    const char *expected_checksum = argv[2];
    const char *checksum_type_str = argv[3];
    LrChecksumType checksum_type = lr_checksum_type(checksum_type_str);
    gboolean matches;
    gchar *calculated_checksum = NULL;
    GError *err = NULL;

    // Check if the checksum type is valid
    if (checksum_type == LR_CHECKSUM_UNKNOWN) {
        fprintf(stderr, "Unknown checksum type: %s\n", checksum_type_str);
        return EXIT_FAILURE;
    }

    // Open the file for reading
    int fd = open(file_path, O_RDONLY);
    if (fd == -1) {
        perror("Error opening file");
        return EXIT_FAILURE;
    }

    // Compare the checksum
    if (!lr_checksum_fd_compare(checksum_type, fd, expected_checksum, TRUE, &matches, &calculated_checksum, &err)) {
        fprintf(stderr, "Error calculating checksum: %s\n", err->message);
        g_error_free(err);
        close(fd);
        return EXIT_FAILURE;
    }

    // Output the result
    if (matches) {
        printf("%s checksum matches the expected value.\n", file_path);
    } else {
        printf("%s checksum does NOT match the expected value.\n", file_path);
        printf("Calculated checksum: %s\n", calculated_checksum);
        printf("Expected checksum: %s\n", expected_checksum);
    }

    // Clean up
    g_free(calculated_checksum);
    close(fd);

    return matches ? EXIT_SUCCESS : EXIT_FAILURE;
}
EOF'''
        self.cmd(mirrorlist_file)
        self.cmd(code)
        self.cmd("gcc -o test_librepo_checksum_fd_compare test_librepo_checksum_fd_compare.c `pkg-config --cflags --libs librepo`")

    def test(self):
        self.cmd(f"./test_librepo_checksum_fd_compare checksum.txt 6d78392a5886177fe5b86e585a0b695a2bcd01a05504b3c4e38bc8eeb21e8326 sha256")
        self.cmd(f"./test_librepo_checksum_fd_compare checksum.txt b2cfa4183267af678ea06c7407d4d6d8 md5")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['test_librepo_checksum_fd_compare.c', 'test_librepo_checksum_fd_compare', 'checksum.txt']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}", ignore_status=True)