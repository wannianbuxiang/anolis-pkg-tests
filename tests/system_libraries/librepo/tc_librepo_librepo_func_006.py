#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_librepo_librepo_func_006.py
@Time:      2024/02/26 15:15:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_librepo_librepo_func_006.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc librepo librepo-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        repomod_file = r'''cat << EOF > repomd_006.xml
<?xml version="1.0" encoding="UTF-8"?>
<repomd xmlns="http://linux.duke.edu/metadata/repo">
  <revision>1590492027</revision>
  <data type="primary">
    <checksum type="sha256">1234567890abcdef1234567890abcdef1234567890abcdef1234567890abcdef</checksum>
    <location href="repodata/1234567890abcdef-primary.xml.gz"/>
    <timestamp>123456789</timestamp>
    <size>1234</size>
    <database_version>10</database_version>
  </data>
  <data type="filelists">
    <checksum type="sha256">abcdef1234567890abcdef1234567890abcdef1234567890abcdef1234567890</checksum>
    <location href="repodata/abcdef1234567890-filelists.xml.gz"/>
    <timestamp>987654321</timestamp>
    <size>5678</size>
    <database_version>20</database_version>
  </data>
</repomd>
EOF'''
        code = r'''cat > test_librepo_repomd_get_record.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <librepo/librepo.h>
#include <glib.h>

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <repomd.xml path>\n", argv[0]);
        return EXIT_FAILURE;
    }
    const char *repomd_path = argv[1];
    GError *err = NULL;

    // Initialize the LrYumRepoMd structure
    LrYumRepoMd *repomd = lr_yum_repomd_init();
    if (!repomd) {
        fprintf(stderr, "Failed to initialize repomd\n");
        return EXIT_FAILURE;
    }

    // Open the repomd.xml file
    int fd = open(repomd_path, O_RDONLY);
    if (fd == -1) {
        perror("Error opening file");
        lr_yum_repomd_free(repomd);
        return EXIT_FAILURE;
    }

    // Parse the repomd.xml file
    if (!lr_yum_repomd_parse_file(repomd, fd, NULL, NULL, &err)) {
        fprintf(stderr, "Error parsing repomd.xml: %s\n", err->message);
        g_error_free(err);
        close(fd);
        lr_yum_repomd_free(repomd);
        return EXIT_FAILURE;
    }

    // Get the primary record from repomd
    LrYumRepoMdRecord *primary_record = lr_yum_repomd_get_record(repomd, "primary");
    if (primary_record) {
        printf("Record type: %s\n", primary_record->type);
        printf("Record location: %s\n", primary_record->location_href);
        printf("Record size: %ld\n", primary_record->size);
        printf("Record timestamp: %ld\n", primary_record->timestamp);
        printf("Record database_version: %ld\n", primary_record->db_version);
    } else {
        fprintf(stderr, "Failed to get 'primary' record\n");
    }

    LrYumRepoMdRecord *filelists_record = lr_yum_repomd_get_record(repomd, "filelists");
    if (filelists_record) {
        printf("Record type: %s\n", filelists_record->type);
        printf("Record location: %s\n", filelists_record->location_href);
        printf("Record size: %ld\n", filelists_record->size);
        printf("Record timestamp: %ld\n", filelists_record->timestamp);
        printf("Record database_version: %ld\n", filelists_record->db_version);

    } else {
        fprintf(stderr, "Failed to get 'filelists' record\n");
    }

    // Cleanup resources
    close(fd);
    lr_yum_repomd_free(repomd);
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(repomod_file)
        self.cmd(code)
        self.cmd("gcc -o test_librepo_repomd_get_record test_librepo_repomd_get_record.c `pkg-config --cflags --libs librepo` -Wno-int-conversion -Wno-incompatible-pointer-types")

    def test(self):
        self.cmd(f"./test_librepo_repomd_get_record repomd_006.xml")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['test_librepo_repomd_get_record.c', 'test_librepo_repomd_get_record', 'repomd_006.xml']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}", ignore_status=True)
