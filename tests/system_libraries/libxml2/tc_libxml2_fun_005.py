#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libxml2_fun_005.py
@Time:      2024/02/23 11:12:50
@Author:    hongfeng
@Version:   1.0
@Contact:   wb-zhf668260@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    hongfeng
"""

from common.basetest import LocalTest
class Test(LocalTest):
    """
    See tc_libxml2_fun_005.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "libxml2-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > create_html.c <<EOF
#include <stdio.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

int main() {
    // 初始化libxml2
    xmlInitParser();

    // 创建一个新的XML文档
    xmlDocPtr doc = xmlNewDoc(BAD_CAST "1.0");
    if (doc == NULL) {
        fprintf(stderr, "Error creating document\\n");
        return 1;
    }

    // 设置文档类型为HTML5
    xmlDtdPtr dtd = xmlCreateIntSubset(doc, BAD_CAST "html", NULL, "-//W3C//DTD HTML 5.0//EN");
    if (dtd == NULL) {
        // 对于HTML5，实际上不需要DTD，因为它是SGML-free的
        // 这里仅作为演示，实际开发中可以忽略这一步
    }

    // 创建根节点（HTML）
    xmlNodePtr html_node = xmlNewNode(NULL, BAD_CAST "html");
    xmlDocSetRootElement(doc, html_node);

    // 创建头部（head）节点
    xmlNodePtr head_node = xmlNewChild(html_node, NULL, BAD_CAST "head", NULL);
    xmlNewProp(head_node, BAD_CAST "lang", BAD_CAST "en");

    // 在头部添加一个title元素
    xmlNodePtr title_node = xmlNewChild(head_node, NULL, BAD_CAST "title", BAD_CAST "Example Page");
    
    // 创建正文（body）节点
    xmlNodePtr body_node = xmlNewChild(html_node, NULL, BAD_CAST "body", NULL);

    // 在正文中添加一段文本
    xmlNodePtr p_node = xmlNewChild(body_node, NULL, BAD_CAST "p", BAD_CAST "Hello, World!");

    // 保存HTML文档到文件
    int result = htmlSaveFileEnc("created.html", doc, "UTF-8");
    if (result < 0) {
        fprintf(stderr, "Error saving HTML to file\\n");
    }

    // 清理并释放资源
    xmlFreeDoc(doc);
    xmlCleanupParser();

    return 0;
}
"""
        self.cmd(cmdline)
        self.cmd("gcc -o create_html create_html.c `xml2-config --cflags --libs`")

    def test(self):
        self.cmd("./create_html")
        ret_c,ret_o = self.cmd("cat created.html")
        self.assertTrue('<!DOCTYPE html SYSTEM "-//W3C//DTD HTML 5.0//EN">' in ret_o, 'check output error.')
        self.assertTrue("<p>Hello, World!</p>" in ret_o, 'check output error.')
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm create_html.c create_html created.html")
