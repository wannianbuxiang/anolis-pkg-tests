#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libxml2_fun_004.py
@Time:      2024/02/22 11:12:50
@Author:    hongfeng
@Version:   1.0
@Contact:   wb-zhf668260@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    hongfeng
"""

import os
from common.basetest import LocalTest
class Test(LocalTest):
    """
    See tc_libxml2_fun_003.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "libxml2-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > delete_xml.c <<EOF
#include <stdio.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

// 删除指定名称的元素节点
void removeNodesByName(xmlNodePtr rootNode, const xmlChar* nodeName) {
    xmlNode *cur_node = NULL;

    for (cur_node = rootNode; cur_node; cur_node = cur_node->next) {
        if (cur_node->type == XML_ELEMENT_NODE) {
            printf("Element name: %s\\n", cur_node->name);
            if (cur_node->type == XML_ELEMENT_NODE && !xmlStrcmp(cur_node->name, nodeName)) {
                // 解除节点与DOM树的链接
                xmlUnlinkNode(cur_node);
                // 释放节点资源
                xmlFreeNode(cur_node);
            }
        }
        // 递归处理子节点
        removeNodesByName(cur_node->children, nodeName);
    }
}

int main() {
    // 初始化和解析XML文档...
    xmlDocPtr doc = xmlReadFile("test_004.xml", NULL, 0);
    xmlNodePtr root = xmlDocGetRootElement(doc);

    // 删除所有名为"to"的元素节点
    removeNodesByName(root, BAD_CAST "to");

    // 保存修改后的文档
    int result = xmlSaveFormatFileEnc("deleted.xml", doc, "UTF-8", 1);
    if (result < 0) {
        fprintf(stderr, "Error saving XML to file\\n");
    }

    // 清理...
    xmlFreeDoc(doc);
    xmlCleanupParser();

    return 0;
}
"""
        self.cmd(cmdline)
        self.cmd("gcc -o delete_xml delete_xml.c `xml2-config --cflags --libs`")
        file_dir = os.path.dirname(__file__)
        self.cmd(f"cp {file_dir}/test.xml ./test_004.xml")

    def test(self):
        self.cmd("./delete_xml")
        ret_c,ret_o = self.cmd("cat deleted.xml")
        self.assertTrue("to" not in ret_o, 'check output error.')
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm delete_xml.c delete_xml test_004.xml deleted.xml")
