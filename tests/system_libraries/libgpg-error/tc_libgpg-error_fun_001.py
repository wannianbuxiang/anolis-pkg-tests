#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libgpg-error_fun_001.py
@Time:      2024/04/22 10:32:50
@Author:    hongfeng
@Version:   1.0
@Contact:   wb-zhf668260@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    hongfeng
"""

from common.basetest import LocalTest
class Test(LocalTest):
    """
    See tc_libgpg-error_fun_001.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libgpg-error-devel gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > test01.c <<EOF
#include <gpg-error.h>
#include <assert.h>

int main() {
    // 使用实际API调用返回的错误代码
    gpg_error_t err = gpg_error(GPG_ERR_GENERAL);

    const char *msg = gpg_strerror(err);
    const char *src = gpg_strsource(err);

    printf("Error message: %s\\n", msg);
    printf("Error source: %s\\n", src);

    gpg_error_t base_err = gpg_error(GPG_ERR_GENERAL);
    gpg_error_t wrapped_err = gpg_err_make(GPG_ERR_SOURCE_USER_1, base_err);

    // 验证错误代码未变
    assert(gpg_err_code(wrapped_err) == base_err);
    // 验证来源已更改为USER_1
    assert(gpg_err_source(wrapped_err) == GPG_ERR_SOURCE_USER_1); 
}
EOF"""
        self.cmd(cmdline)
        self.cmd("gcc -o test01 test01.c -lgpg-error")

    def test(self):
        # 验证执行成功
        self.cmd("./test01")
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm test01.c test01")
        
