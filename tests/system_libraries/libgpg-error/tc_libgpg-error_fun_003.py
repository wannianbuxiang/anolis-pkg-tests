#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libgpg-error_fun_003.py
@Time:      2024/04/22 10:32:50
@Author:    hongfeng
@Version:   1.0
@Contact:   wb-zhf668260@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    hongfeng
"""

from common.basetest import LocalTest
class Test(LocalTest):
    """
    See tc_libgpg-error_fun_003.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libgpg-error-devel gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > test03.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <gpg-error.h>

int main(void) {
    // 自定义error代码
    int custom_error_code = 5; 
    // 自定义error来源
    gpg_err_source_t custom_error_source = GPG_ERR_SOURCE_USER_1; 

    // 使用gpg_err_make()创建gpg_error_t.
    gpg_error_t custom_gpg_error = gpg_err_make(custom_error_source, custom_error_code);

    printf("Custom gpg_error_t value: %lu\\n", (unsigned long) custom_gpg_error);

    // Extract the error source from the custom gpg_error_t value.
    gpg_err_source_t extracted_error_source = gpg_err_source(custom_gpg_error);
    gpg_err_code_t extracted_error_code = gpg_err_code(custom_gpg_error);

    printf("Extracted error source: %d\\n", extracted_error_source);
    printf("Extracted error source: %d\\n", extracted_error_code);

    // Compare the original and extracted error sources.
    if (custom_error_source == extracted_error_source) {
        printf("Error source extraction successful.\\n");
    } else {
        printf("Error source extraction failed.\\n");
    }

    return 0;
}
EOF"""
        self.cmd(cmdline)
        self.cmd("gcc -o test03 test03.c -lgpg-error")

    def test(self):
        # 验证执行成功, 并且输出正确
        ret_c, ret_o = self.cmd("./test03")
        self.assertTrue("Extracted error source: 32" in ret_o, 'check output error.')
        self.assertTrue("Extracted error source: 5" in ret_o, 'check output error.')
        self.assertTrue("Error source extraction successful" in ret_o, 'check output error.')
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm test03.c test03")
        
