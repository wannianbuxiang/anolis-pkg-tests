#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libgpg-error_fun_002.py
@Time:      2024/04/22 10:32:50
@Author:    hongfeng
@Version:   1.0
@Contact:   wb-zhf668260@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    hongfeng
"""

from common.basetest import LocalTest
class Test(LocalTest):
    """
    See tc_libgpg-error_fun_002.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libgpg-error-devel libgcrypt-devel gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > test02.c <<EOF
#include <gcrypt.h>
#include <gpg-error.h>
#include <assert.h>

#define CIPHER_ALGO GCRY_CIPHER_AES256 // 使用AES-256算法

int main() {
    gcry_cipher_hd_t hd;
    gcry_error_t gpg_err;

    // 没有指定cipher algorithm
    gpg_err = gcry_cipher_open(&hd, NULL, GCRY_CIPHER_MODE_CBC, 200);
    printf("%s\\n", gpg_strerror(gpg_err));
}
EOF"""
        self.cmd(cmdline)
        self.cmd("gcc -o test02 test02.c -lgpg-error -lgcrypt")

    def test(self):
        # 验证执行成功
        ret_c, ret_o = self.cmd("./test02")
        self.assertTrue("Invalid cipher algorithm" in ret_o, 'check output error.')
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm test02.c test02")
        
