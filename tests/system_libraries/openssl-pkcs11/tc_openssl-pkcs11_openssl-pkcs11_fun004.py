# -*- encoding: utf-8 -*-

"""
@File:      tc_openssl-pkcs11_openssl-pkcs11_fun004.py
@Time:      2024/4/15 15:17:25
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

import os
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_openssl-pkcs11_openssl-pkcs11_fun004.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "openssl-pkcs11 openssl-devel openssl opensc softhsm"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        file_dir = os.path.dirname(__file__)
        self.cmd(f"bash {file_dir}/set_openssl.cnf.sh")
        cmdline = '''cat >test_cert.pem<<"EOF"
-----BEGIN CERTIFICATE-----
MIIEujCCAyKgAwIBAgIIbhg6RK4FMoMwDQYJKoZIhvcNAQELBQAwbjEOMAwGA1UE
BhMFQ0hJTkExDTALBgNVBAgTBFhpYW4xDTALBgNVBAcTBFhpYW4xFjAUBgNVBAsT
DUlUIERlcGFydG1lbnQxEDAOBgNVBAoTB3Rlc3QwMDExFDASBgNVBAMTC3Rlc3R1
c2VyMDAxMCAXDTI0MDMxNTA0MDQ0N1oYDzIwNjMwNDA1MTcwMDAwWjBuMQ4wDAYD
VQQGEwVDSElOQTENMAsGA1UECBMEWGlhbjENMAsGA1UEBxMEWGlhbjEWMBQGA1UE
CxMNSVQgRGVwYXJ0bWVudDEQMA4GA1UEChMHdGVzdDAwMTEUMBIGA1UEAxMLdGVz
dHVzZXIwMDEwggGiMA0GCSqGSIb3DQEBAQUAA4IBjwAwggGKAoIBgQD3th8TSCdR
2VfeJ8vBCKf5+dOQ8w8ltX0E6gnYHpt84UhvdKS5HGklaGRLbiGb/mm4YYqAIwTy
C1mkkh4eoV69Gw6jtSN0M7lN/1HoDUlGXwsZyrMTDoj1oGuJCaYuEmubcno369t/
Y1e8ajCPTMyL6VZFk1FmgSIw3XLFNgF/sx9BFKEjMZKD8vphJl7xBF1thi4LhvEp
5lMQjVPYKld5Y5tzL7UKFjpgFECKrnZhJIFILsAhCPdrGwlQokYyxN5u/RHyhU0Q
Oy17b/tjCE9FpNn/B5bxIwCk7TCHSo3Kz3CZr5AIKqN2L4hfTlbNICRYqExv0Zbp
ZQZKo8IcpInvFKdLDIID8zq9+xpUgjwV10YimBmTEBOj4vLEreQ4KCuGPEnJnFse
ansUt1X+9pmhZdA+TjX5gsBm9bxUopB8h+c+uhnar3VbHLRPiWxR7IuPFFmN4jpa
VH6BLVIKrcl4uNkA+Iv1DDgIjL8l3Gqcj4fkOE34+EVn6x33xYpBC0sCAwEAAaNa
MFgwIgYDVR0RBBswGYEXdGVzdHVzZXIwMDFAZXhhbXBsZS5jb20wEQYKKwYBBAHa
RwICAQQDAQH/MA8GA1UdEwEB/wQFMAMBAf8wDgYDVR0PAQH/BAQDAgTwMA0GCSqG
SIb3DQEBCwUAA4IBgQAKHkheT1sZSYGuVRMlwTmBe2mC6N3bhs/iL4umqPcLaOL9
CJbDAWpeoF6b6AoHCangXYs/Xg76PGiEfc8LMTCUpb82OXJuMa60qVaa7dM0FKUo
RUhmdfoC6H4/mDLzCQs1nsjJ8i3AMxQlGv/g/sEAihP4lqnY3vtpGx36eplNqtZU
2pdpZZG2DR6U2X/kbj40lm/a+w+tY49S3z2qJq/IUUY5bozst4QIBuBF4uyi1IbL
G3KvrQhIL7+PEhZLPxmFOpECL/7SmI8tiPmu76zxYMZtcP2MylVohJbuD1jDoq1s
N/Hfz8wVzaqAYe/1cHA088gayIycMKSm9qdB1/+BHWUe80wcWsXm4S3UBvqsVEqX
X/xx0c8ZvYQPqzIPmTEn4Q1tQdOkRqvKZW1wnL5WlC8O7G6s+0Edy+bGdokrk5XW
fWXk4g49yRYFVYVMbQYprXEb6YsvwBofXIlINdqb5/5Mq//Cb7WKCkEJ5ULgNTpe
kckNNpDcwImYCuuCzgI=
-----END CERTIFICATE-----
EOF'''
        self.cmd(cmdline)

    def test(self):
        # 指定 PKCS#11 模块的路径
        module_path = "/usr/lib64/pkcs11/libsofthsm2.so"
        # PKCS#11 引擎所在路径
        engine_path = "/usr/lib64/engines-3/pkcs11.so"
        key_label = "TestKeyPair"
        key_id = 10
        key_pin = 123456
        cer_file = "test_cert.pem"
        self.cmd(f'softhsm2-util --init-token --slot 0 --label {key_label} --so-pin {key_pin} --pin {key_pin}') # 初始化令牌
        code, result = self.cmd(f"pkcs11-tool --module {module_path} --login --pin {key_pin} --keypairgen --key-type rsa:2048 --id {key_id} --label {key_label}")
        # 关联证书与私钥
        code, result = self.cmd(f'pkcs11-tool --module {module_path} --login --pin {key_pin} --write-object {cer_file} --type cert --id {key_id} --label "{key_label}"')
        self.assertIn("Created certificate", result)
        
        # 删除密钥对及证书
        code, result = self.cmd(f"pkcs11-tool --module {module_path} --login --pin {key_pin} --delete-object --type privkey --id {key_id}")
        code, result = self.cmd(f"pkcs11-tool --module {module_path} --login --pin {key_pin} --delete-object --type pubkey --id {key_id}")
        code, result = self.cmd(f"pkcs11-tool --module {module_path} --login --pin {key_pin} --delete-object --type cert --id {key_id}")
        self.assertNotIn("TestKeyPair", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /var/lib/softhsm/tokens/*")
