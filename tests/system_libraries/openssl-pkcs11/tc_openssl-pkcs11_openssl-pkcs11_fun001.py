# -*- encoding: utf-8 -*-

"""
@File:      tc_openssl-pkcs11_openssl-pkcs11_fun001.py
@Time:      2024/4/12 15:20:05
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

import os
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_openssl-pkcs11_openssl-pkcs11_fun001.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "openssl-pkcs11 openssl-devel openssl opensc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        file_dir = os.path.dirname(__file__)
        self.cmd(f"bash {file_dir}/set_openssl.cnf.sh")

    def test(self):
        # PKCS#11 引擎所在路径
        engine_path = "/usr/lib64/engines-3/pkcs11.so"
        flag = 0
        if not os.path.exists(engine_path):
            flag == 1
        self.assertTrue(flag == 0, msg="PKCS#11 引擎文件不存在")
        code, result = self.cmd(f"openssl engine -t -c pkcs11")
        self.assertIn("(pkcs11) pkcs11 engine", result)
        self.assertIn("available", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
