# -*- encoding: utf-8 -*-

"""
@File:      tc_openssl-pkcs11_openssl-pkcs11_fun003.py
@Time:      2024/4/15 15:17:25
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

import os
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_openssl-pkcs11_openssl-pkcs11_fun003.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "openssl-pkcs11 openssl-devel openssl opensc softhsm"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        file_dir = os.path.dirname(__file__)
        self.cmd(f"bash {file_dir}/set_openssl.cnf.sh")

    def test(self):
        # 指定 PKCS#11 模块的路径
        module_path = "/usr/lib64/pkcs11/libsofthsm2.so"
        pkcs11_token_label = "TestKeyPair"
        key_pin = 123456
        key_id = 10
        key_label = "TestKeyPair"
        data_file = "test_data.txt"
        signature_file = "test_signature.dat"
        with open(data_file, 'w') as file:
            file.write('This is some test data!')
        self.cmd(f'softhsm2-util --init-token --slot 0 --label {key_label} --so-pin {key_pin} --pin {key_pin}') # 初始化令牌
        code, result = self.cmd(f"pkcs11-tool --module {module_path} --login --pin {key_pin} --keypairgen --key-type rsa:2048 --id {key_id} --label {key_label}")

        self.log.info("使用 PKCS#11 令牌私钥进行数据签名……")
        code, result = self.cmd(f"pkcs11-tool --module {module_path} --id {key_id} --sign --pin {key_pin} \
            --input-file {data_file} --output-file {signature_file} --mechanism SHA256-RSA-PKCS")
        self.assertTrue(code == 0, msg="私钥进行数据签名失败")

        self.log.info("将 PKCS#11 令牌公钥导出到文件并用公钥签名")
        public_key_file = "test_public_key.pem"
        self.cmd(f"pkcs11-tool --module {module_path} --label {key_label} --read-object --type pubkey --output-file {public_key_file}")
        # 使用导出的公钥验证签名
        coed, result = self.cmd(f"openssl dgst -verify {public_key_file} -keyform DER -signature {signature_file} {data_file}")
        self.assertIn("Verified OK", result)

        # 删除密钥
        code, result = self.cmd(f"pkcs11-tool --module {module_path} --login --pin {key_pin} --delete-object --type privkey --id {key_id}")
        code, result = self.cmd(f"pkcs11-tool --module {module_path} --login --pin {key_pin} --delete-object --type pubkey --id {key_id}")
        self.assertNotIn("TestKeyPair", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /var/lib/softhsm/tokens/*")
