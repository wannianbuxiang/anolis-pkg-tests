#!/bin/bash

if ! grep -q "[ ]*engine_section" /etc/ssl/openssl.cnf; then
    sed -i "/^ssl_conf = ssl_module/a \engines = engine_section\n\[engine_section\]\npkcs11 = pkcs11_section\n\[pkcs11_section\]\nengine_id = pkcs11\ndynamic_path = \/usr\/lib64\/engines-3\/pkcs11.so\nMODULE_PATH = \/usr\/lib64\/pkcs11\/libsofthsm2.so\n" /etc/ssl/openssl.cnf
fi