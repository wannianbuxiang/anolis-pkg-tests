# -*- encoding: utf-8 -*-

"""
@File:      tc_openssl-pkcs11_openssl-pkcs11_fun002.py
@Time:      2024/4/15 14:33:25
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

import os
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_openssl-pkcs11_openssl-pkcs11_fun002.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "openssl-pkcs11 openssl-devel openssl opensc softhsm"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        file_dir = os.path.dirname(__file__)
        self.cmd(f"bash {file_dir}/set_openssl.cnf.sh")

    def test(self):
        # 指定 PKCS#11 模块的路径
        module_path = "/usr/lib64/pkcs11/libsofthsm2.so"
        flag = 0
        if not os.path.exists(module_path):
            flag == 1
        self.assertTrue(flag == 0, msg="PKCS#11 模块的路径不存在")
        
        # 创建密钥对
        key_label = "TestKeyPair"
        key_id = 10
        key_pin = 123456
        self.cmd(f'softhsm2-util --init-token --slot 0 --label {key_label} --so-pin {key_pin} --pin {key_pin}') # 初始化令牌
        code, result = self.cmd(f"pkcs11-tool --module {module_path} --login --pin {key_pin} --keypairgen --key-type rsa:2048 --id {key_id} --label {key_label}")
        
        # 列出令牌中的所有对象
        code, result = self.cmd(f"pkcs11-tool --module {module_path} --login --pin {key_pin} --list-objects")
        self.assertIn("TestKeyPair", result)
        self.assertIn("10", result)

        # 删除秘钥
        code, result = self.cmd(f"pkcs11-tool --module {module_path} --login --pin {key_pin} --delete-object --type privkey --id {key_id}")
        code, result = self.cmd(f"pkcs11-tool --module {module_path} --login --pin {key_pin} --delete-object --type pubkey --id {key_id}")
        self.assertNotIn("TestKeyPair", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /var/lib/softhsm/tokens/*")
