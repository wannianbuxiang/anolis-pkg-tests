#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_gpgme_func001.py
@Time:      2024/04/22 11:31:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_gpgme_func001.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gpgme-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_gpgme.c <<EOF
#include <gpgme.h>
#define REQUIRED_VERSION "1.1.0"

int main(){
    const char *actual_version = gpgme_check_version(REQUIRED_VERSION);
    
    if (actual_version)
    {
        printf("The actual GPGME version is at least %s\\n", REQUIRED_VERSION);
        printf("Version from header: %s (0x%06x)\\n",
               GPGME_VERSION, GPGME_VERSION_NUMBER);
        printf("Version from binary: %s\\n", gpgme_check_version(NULL));
        printf("Copyright blurb ...:%s\\n", gpgme_check_version("\\x01\\x01"));
        
        // 如果实际版本大于等于所需版本，则返回0，表示成功
        return 0;
    }
    else
    {
        printf("Error: The actual GPGME version is less than %s\\n", REQUIRED_VERSION);
        return 1; // 返回非零值表示版本检查失败
    }
}
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o /tmp/ghm/test_gpgme /tmp/ghm/test_gpgme.c -lgpgme")
        ret_c, ret_o = self.cmd("/tmp/ghm/test_gpgme")
        self.assertIn("Version from header: 1.21.0", ret_o)
        self.assertIn("Version from binary: 1.21.0", ret_o)
        self.assertIn("This is GPGME 1.21.0 - The GnuPG Made Easy library", ret_o)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")
  