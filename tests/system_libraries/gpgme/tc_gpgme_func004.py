#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_gpgme_func004.py
@Time:      2024/04/24 10:31:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""
import subprocess
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_gpgme_func004.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gpgme-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_gpgme.c <<EOF
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <locale.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <gpgme.h>
#define PGM "test_gpgme"

#define fail_if_err(err)					\
  do								\
    {								\
      if (err)							\
        {							\
          fprintf (stderr, PGM": file %s line %d: <%s> %s\\n",	\
                   __FILE__, __LINE__, gpgme_strsource (err),	\
		   gpgme_strerror (err));			\
          exit (1);						\
        }							\
    }								\
  while (0)

static int got_errors;

static const char test_text1[] = "Hallo Leute!\\n";
static const char test_text1f[]= "Hallo Leute?\\n";
static const char test_sig1[] =
"-----BEGIN CMS OBJECT-----\\n"
"MIAGCSqGSIb3DQEHAqCAMIACAQExCzAJBgUrDgMCGgUAMIAGCSqGSIb3DQEHAQAA\\n"
"MYIBOTCCATUCAQEwcDBrMQswCQYDVQQGEwJERTETMBEGA1UEBxQKRPxzc2VsZG9y\\n"
"ZjEWMBQGA1UEChMNZzEwIENvZGUgR21iSDEZMBcGA1UECxMQQWVneXB0ZW4gUHJv\\n"
"amVjdDEUMBIGA1UEAxMLdGVzdCBjZXJ0IDECAQAwBwYFKw4DAhqgJTAjBgkqhkiG\\n"
"9w0BCQQxFgQU7FC/ibH3lC9GE24RJJxa8zqP7wEwCwYJKoZIhvcNAQEBBIGAA3oC\\n"
"DUmKERmD1eoJYFw38y/qnncS/6ZPjWINDIphZeK8mzAANpvpIaRPf3sNBznb89QF\\n"
"mRgCXIWcjlHT0DTRLBf192Ve22IyKH00L52CqFsSN3a2sajqRUlXH8RY2D+Al71e\\n"
"MYdRclgjObCcoilA8fZ13VR4DiMJVFCxJL4qVWI=\\n"
"-----END CMS OBJECT-----\\n";

void
print_data (gpgme_data_t dh)
{
#define BUF_SIZE 512
  char buf[BUF_SIZE + 1];
  int ret;

  ret = gpgme_data_seek (dh, 0, SEEK_SET);
  if (ret)
    fail_if_err (gpgme_err_code_from_errno (errno));
  while ((ret = gpgme_data_read (dh, buf, BUF_SIZE)) > 0)
    fwrite (buf, ret, 1, stdout);
  if (ret < 0)
    fail_if_err (gpgme_err_code_from_errno (errno));
}

void
init_gpgme_basic (void)
{
  gpgme_check_version (NULL);
  setlocale (LC_ALL, "");
  gpgme_set_locale (NULL, LC_CTYPE, setlocale (LC_CTYPE, NULL));
#ifndef HAVE_W32_SYSTEM
  gpgme_set_locale (NULL, LC_MESSAGES, setlocale (LC_MESSAGES, NULL));
#endif
}

void
init_gpgme (gpgme_protocol_t proto)
{
  gpg_error_t err;

  init_gpgme_basic ();
  err = gpgme_engine_check_version (proto);
  fail_if_err (err);
}

static void
check_result (gpgme_verify_result_t result, int summary, const char *fpr,
	      gpgme_error_t status, gpgme_validity_t validity)
{
  gpgme_signature_t sig;

  sig = result->signatures;
  if (!sig || sig->next)
    {
      fprintf (stderr, "%s:%i: Unexpected number of signatures\\n",
	       __FILE__, __LINE__);
      got_errors = 1;
      if (!sig)
        return;
    }
  if (sig->summary != summary)
    {
      fprintf (stderr, "%s:%i: Unexpected signature summary: "
               "want=0x%x have=0x%x\\n",
	       __FILE__, __LINE__, summary, sig->summary);
      got_errors = 1;
    }
  if (sig->fpr && strcmp (sig->fpr, fpr))
    {
      fprintf (stderr, "%s:%i: Unexpected fingerprint: %s\\n",
	       __FILE__, __LINE__, sig->fpr);
      got_errors = 1;
    }
  if (gpgme_err_code (sig->status) != status)
    {
      fprintf (stderr, "%s:%i: Unexpected signature status: %s\\n",
	       __FILE__, __LINE__, gpgme_strerror (sig->status));
      got_errors = 1;
    }
  if (sig->notations)
    {
      fprintf (stderr, "%s:%i: Unexpected notation data\\n",
	       __FILE__, __LINE__);
      got_errors = 1;
    }
  if (sig->wrong_key_usage)
    {
      fprintf (stderr, "%s:%i: Unexpectedly wrong key usage\\n",
	       __FILE__, __LINE__);
      got_errors = 1;
    }
  if (sig->validity != validity)
    {
      fprintf (stderr, "%s:%i: Unexpected validity: %i\\n",
	       __FILE__, __LINE__, sig->validity);
      got_errors = 1;
    }
  if (gpgme_err_code (sig->validity_reason) != GPG_ERR_NO_ERROR)
    {
      fprintf (stderr, "%s:%i: Unexpected validity reason: %s\\n",
	       __FILE__, __LINE__, gpgme_strerror (sig->validity_reason));
      got_errors = 1;
    }
}


static void
show_auditlog (gpgme_ctx_t ctx)
{
  gpgme_error_t err;
  gpgme_data_t data;

  err = gpgme_data_new (&data);
  fail_if_err (err);
  err = gpgme_op_getauditlog (ctx, data, 0);
  if (err)
    {
      fprintf (stderr, "%s:%i: Can't get audit log: %s\\n",
	       __FILE__, __LINE__, gpgme_strerror (err));
      if (gpgme_err_code (err) != GPG_ERR_ASS_UNKNOWN_CMD)
	got_errors = 1;
    }
  print_data (data);
  gpgme_data_release (data);
}


int
main (void)
{
  gpgme_ctx_t ctx;
  gpgme_error_t err;
  gpgme_data_t sig, text;
  gpgme_verify_result_t result;

  init_gpgme (GPGME_PROTOCOL_CMS);

  err = gpgme_new (&ctx);
  fail_if_err (err);
  gpgme_set_protocol (ctx, GPGME_PROTOCOL_CMS);

  /* Checking a valid message.  */
  err = gpgme_data_new_from_mem (&text, test_text1, strlen (test_text1), 0);
  fail_if_err (err);
  err = gpgme_data_new_from_mem (&sig, test_sig1, strlen (test_sig1), 0);
  fail_if_err (err);

  err = gpgme_op_verify (ctx, sig, text, NULL);
  fail_if_err (err);
  result = gpgme_op_verify_result (ctx);
  check_result (result, GPGME_SIGSUM_VALID | GPGME_SIGSUM_GREEN,
		"A782EB7CA56212CF3DEF6088FB6708AE59FAD36E",
		GPG_ERR_NO_ERROR, GPGME_VALIDITY_FULL);

  show_auditlog (ctx);

  /* Checking a manipulated message.  */
  gpgme_data_release (text);
  err = gpgme_data_new_from_mem (&text, test_text1f, strlen (test_text1f), 0);
  fail_if_err (err);
  gpgme_data_seek (sig, 0, SEEK_SET);
  err = gpgme_op_verify (ctx, sig, text, NULL);
  fail_if_err (err);
  result = gpgme_op_verify_result (ctx);
  check_result (result, GPGME_SIGSUM_RED,
		"A782EB7CA56212CF3DEF6088FB6708AE59FAD36E",
		GPG_ERR_BAD_SIGNATURE, GPGME_VALIDITY_UNKNOWN);

  show_auditlog (ctx);

  gpgme_data_release (text);
  gpgme_data_release (sig);
  gpgme_release (ctx);
  return got_errors? 1 : 0;
}
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o /tmp/ghm/test_gpgme /tmp/ghm/test_gpgme.c -lgpgme")
        ret_o = subprocess.run("/tmp/ghm/test_gpgme", capture_output=True, shell=True)
        self.log.info(ret_o)
        self.assertIn(b'Data verification succeeded', ret_o.stdout)
        self.assertIn(b'Data available', ret_o.stdout)
        self.assertIn(b'Signature available:', ret_o.stdout)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")
  