#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_gpgme_func003.py
@Time:      2024/04/23 09:31:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_gpgme_func003.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gpgme-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_gpgme.c <<EOF
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <gpgme.h>

#define PGM "test_gpgme"

#define fail_if_err(err)					\
  do								\
    {								\
      if (err)							\
        {							\
          fprintf (stderr, PGM": file %s line %d: <%s> %s\\n",	\
                   __FILE__, __LINE__, gpgme_strsource (err),	\
		   gpgme_strerror (err));			\
          exit (1);						\
        }							\
    }								\
  while (0)

void
init_gpgme_basic (void)
{
  gpgme_check_version (NULL);
  setlocale (LC_ALL, "");
  gpgme_set_locale (NULL, LC_CTYPE, setlocale (LC_CTYPE, NULL));
#ifndef HAVE_W32_SYSTEM
  gpgme_set_locale (NULL, LC_MESSAGES, setlocale (LC_MESSAGES, NULL));
#endif
}

void
init_gpgme (gpgme_protocol_t proto)
{
  gpg_error_t err;

  init_gpgme_basic ();
  err = gpgme_engine_check_version (proto);
  fail_if_err (err);
}

void
print_data (gpgme_data_t dh)
{
#define BUF_SIZE 512
  char buf[BUF_SIZE + 1];
  int ret;

  ret = gpgme_data_seek (dh, 0, SEEK_SET);
  if (ret)
    fail_if_err (gpgme_err_code_from_errno (errno));
  while ((ret = gpgme_data_read (dh, buf, BUF_SIZE)) > 0)
    fwrite (buf, ret, 1, stdout);
  if (ret < 0)
    fail_if_err (gpgme_err_code_from_errno (errno));
}

int
main (void)
{
  gpgme_ctx_t ctx;
  gpgme_error_t err;
  gpgme_data_t out;
  const char *pattern1[] = { "DFN Top Level Certification Authority", NULL };
  const char *pattern2[] = { "A782EB7CA56212CF3DEF6088FB6708AE59FAD36E",
                             "DFN Server Certification Authority",
                             NULL };

  init_gpgme (GPGME_PROTOCOL_CMS);

  err = gpgme_new (&ctx);
  fail_if_err (err);
  gpgme_set_protocol (ctx, GPGME_PROTOCOL_CMS);

  gpgme_set_armor (ctx, 1);

  /* Check exporting of one certificate. */
  err = gpgme_data_new (&out);
  fail_if_err (err);
  err = gpgme_op_export_ext (ctx, pattern1, 0, out);
  fail_if_err (err);

  fflush (NULL);
  fputs ("Begin Result:\\n", stdout);
  print_data (out);
  fputs ("End Result.\\n", stdout);

  gpgme_data_release (out);

  /* Check exporting of 2 certificates. */
  err = gpgme_data_new (&out);
  fail_if_err (err);
  err = gpgme_op_export_ext (ctx, pattern2, 0, out);
  fail_if_err (err);

  fflush (NULL);
  fputs ("Begin Result:\\n", stdout);
  print_data (out);
  fputs ("End Result.\\n", stdout);

  gpgme_data_release (out);


  gpgme_release (ctx);

  return 0;
}
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o /tmp/ghm/test_gpgme /tmp/ghm/test_gpgme.c -lgpgme")
        ret_c, ret_o = self.cmd("/tmp/ghm/test_gpgme")
        self.log.info(ret_o)
        self.assertIn("Begin Result", ret_o)
        self.assertIn("End Result", ret_o)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")
  