#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_gpgme_func002.py
@Time:      2024/04/22 16:31:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_gpgme_func002.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gpgme-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_gpgme.c <<EOF
#include <gpgme.h>

int main (void){
    gpgme_engine_info_t info;
    gpgme_error_t err;

    gpgme_check_version (NULL);

    {
        const char *keys[] = {
            "homedir",
            "sysconfdir",
            "bindir",
            "libexecdir",
            "libdir",
            "datadir",
            "localedir",
            "socketdir",
            "agent-socket",
            "agent-ssh-socket",
            "dirmngr-socket",
            "uiserver-socket",
            "gpgconf-name",
            "gpg-name",
            "gpgsm-name",
            "g13-name",
            "keyboxd-name",
            "agent-name",
            "scdaemon-name",
            "dirmngr-name",
            "pinentry-name",
            "gpg-wks-client-name",
            "gpgtar-name",
            NULL
        };
        const char *s;
        int i;

        for (i = 0; keys[i]; i++)
        {
            if ((s = gpgme_get_dirinfo (keys[i])))
                fprintf (stdout, "dirinfo: %s='%s'\\n", keys[i], s);
        }
    }

    err = gpgme_get_engine_info (&info);
    if (err)
    {
        fprintf (stderr, "%s:%d: gpgme_error_t %s\\n", __FILE__, __LINE__, gpgme_strerror (err));
        return 1;
    }

    for (; info; info = info->next)
        fprintf (stdout, "protocol=%d engine='%s' v='%s' (min='%s') home='%s'\\n",
                 info->protocol, info->file_name, info->version, info->req_version,
                 info->home_dir ? info->home_dir : "[default]");

    return 0;
}
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o /tmp/ghm/test_gpgme /tmp/ghm/test_gpgme.c -lgpgme")
        ret_c, ret_o = self.cmd("/tmp/ghm/test_gpgme")
        self.log.info(ret_o)
        self.assertIn("dirinfo: homedir", ret_o)
        self.assertIn("dirinfo: sysconfdir", ret_o)
        self.assertIn("dirinfo: bindir", ret_o)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")
  