#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

"""
@File:      tc_pixman_pixman_fun_001.py
@Time:      2024/07/18 10:07:20
@Author:    wangge
@Version:   1.0
@Contact:   YS.wangge@h3c.com
@License:   Mulan PSL v2
@Modify:    wangge
"""
import os
import subprocess
import shlex
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_pixman_pixman_fun_001.yaml for details

    :avocado: tags=P0,noarch,local
    """
    PARAM_DIC = {"pkg_name": "gcc pixman-devel pixman"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        

    def test(self):
        script_dir = os.path.dirname(__file__)
        cflags_result = subprocess.run(
            ["pkg-config", "--cflags", "pixman-1"],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True
        )
        libs_result = subprocess.run(
            ["pkg-config", "--libs", "pixman-1"],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True
        )
        cflags = cflags_result.stdout.strip()
        libs = libs_result.stdout.strip()
        gcc_command = f"gcc -o {script_dir}/test11 {script_dir}/test.c -g {cflags} {libs}"
        command = shlex.split(gcc_command)
        result = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        self.assertEqual(result.returncode, 0, f"Compilation failed: {result.stderr}")

        os.chdir(script_dir)
        subprocess.run("./test11")
        code, output = self.cmd("test -e aaa-with-watermark.bmp")
        self.assertEquals(code, 0, f"Output file 'aaa-with-watermark.bmp' does not exist: {output}")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf aaa-with-watermark.bmp test11")
        