# -*- encoding: utf-8 -*-

"""
@File:      tc_libpsl_libpsl_fun009.py
@Time:      2024/3/8 13:44:05
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libpsl_libpsl_fun009.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libpsl libpsl-devel gcc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >test_psl.c<<"EOF"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libpsl.h>
#include <pthread.h>

// 线程执行的函数原型
void* thread_func(void *arg);

// 线程数目
#define NUM_THREADS 10

int main() {
    pthread_t threads[NUM_THREADS];
    const psl_ctx_t *psl;
    int i, rc;

    // 加载内置的公共后缀列表
    psl = psl_builtin();
    if (!psl) {
        fprintf(stderr, "Error: Could not load the public suffix list\\n");
        return EXIT_FAILURE;
    }

    // 创建线程并执行查询
    for (i = 0; i < NUM_THREADS; ++i) {
        rc = pthread_create(&threads[i], NULL, thread_func, (void *)psl);
        if (rc) {
            fprintf(stderr, "Error: unable to create thread, %d\\n", rc);
            exit(EXIT_FAILURE);
        }
    }

    // 等待所有线程完成
    for (i = 0; i < NUM_THREADS; ++i) {
        pthread_join(threads[i], NULL);
    }

    // 释放公共后缀列表资源 - 在这个例子中不需要，因为我们使用的是内置的psl
    //psl_free((psl_ctx_t *)psl);

    return EXIT_SUCCESS;
}

void* thread_func(void *arg) {
    const psl_ctx_t *psl = (const psl_ctx_t *)arg;
    const char *test_domain = "www.example.com";
    const char *registrable_domain;

    // 线程安全的查询
    registrable_domain = psl_registrable_domain(psl, test_domain);
    if (registrable_domain) {
        printf("[%ld] Registrable domain of '%s' is '%s'.\\n", (long)pthread_self(), test_domain, registrable_domain);
    } else {
        printf("[%ld] No registrable domain found for '%s'.\\n", (long)pthread_self(), test_domain);
    }

    return NULL;
}
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o test_psl test_psl.c $(pkg-config --libs --cflags libpsl) -lpthread")
        code, result = self.cmd("./test_psl")
        self.assertTrue(code == 0, msg="10线程并发验证失败!")
            
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf test_psl*")