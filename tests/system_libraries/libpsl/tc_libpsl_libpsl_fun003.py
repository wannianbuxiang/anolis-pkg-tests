# -*- encoding: utf-8 -*-

"""
@File:      tc_libpsl_libpsl_fun003.py
@Time:      2024/3/8 10:30:25
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libpsl_libpsl_fun003.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libpsl libpsl-devel gcc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >test_psl.c<<"EOF"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libpsl.h>

// 判断是否隐式 TLD
int is_implicit_tld(const psl_ctx_t *psl, const char *domain) {
    // 隐式 TLD 不应该包含点号
    if (strchr(domain, '.') != NULL) {
        return 0;
    }

    // 如果 domain 不是已知的公共后缀，则将其视为隐式 TLD
    if (!psl_is_public_suffix(psl, domain)) {
        return 1;
    }

    return 0;
}

int main() {
    const char *domains[] = {"com", "example", "local", "newtld", NULL};
    const psl_ctx_t *psl;
    int i;

    // 加载内置的公共后缀列表
    psl = psl_builtin();
    if (psl == NULL) {
        fprintf(stderr, "Error: Could not load the public suffix list\\n");
        return EXIT_FAILURE;
    }

    // 测试 domains 是否为隐式 TLD
    for (i = 0; domains[i]; i++) {
        if (is_implicit_tld(psl, domains[i])) {
            printf("'%s' is an implicit TLD.\\n", domains[i]);
        } else {
            printf("'%s' is not an implicit TLD.\\n", domains[i]);
        }
    }

    // 释放公共后缀列表资源
    psl_free((psl_ctx_t *)psl);

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(cmdline)
    def test(self):
        self.cmd("gcc -o test_psl test_psl.c -lpsl")
        code, result = self.cmd("./test_psl")
        self.assertTrue(code == 0, msg="隐式TLD验证失败！")
            
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf test_psl*")