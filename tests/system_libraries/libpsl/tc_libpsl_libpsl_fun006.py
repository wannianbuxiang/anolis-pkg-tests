# -*- encoding: utf-8 -*-

"""
@File:      tc_libpsl_libpsl_fun006.py
@Time:      2024/3/8 11:00:25
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libpsl_libpsl_fun006.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libpsl libpsl-devel gcc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >test_psl.c<<"EOF"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libpsl.h>

int main() {
    // 边界和异常测试用例
    const char *test_cases[] = {
        "www.example.com",         // 正常情况
        "example",                 // 没有后缀的域名
        "com",                     // 只有后缀的域名
        ".com",                    // 只有点和后缀的域名
        "",                        // 空字符串
        ".............................................................................."
        ".............................................................................."
        ".............................................................................."
        ".............................................................................."
        "example.com",            // 非常长的域名
        NULL
    };
    
    const psl_ctx_t *psl;
    const char *registrable_domain;
    int i;

    // 加载内置的公共后缀列表
    psl = psl_builtin();
    if (!psl) {
        fprintf(stderr, "Error: Could not load the public suffix list\\n");
        return EXIT_FAILURE;
    }

    // 对每个测试用例进行检查
    for (i = 0; test_cases[i]; ++i) {
        const char *test_domain = test_cases[i];
        printf("Testing '%s':\\n", test_domain);

        registrable_domain = psl_registrable_domain(psl, test_domain);

        if (registrable_domain) {
            printf("\tRegistrable domain: '%s'\\n", registrable_domain);
        } else {
            printf("\tNo registrable domain found.\\n");
        }

        // 检查是否是公共后缀
        if (psl_is_public_suffix(psl, test_domain)) {
            printf("\tIs a public suffix.\\n");
        } else {
            printf("\tIs not a public suffix.\\n");
        }
    }

    // 释放公共后缀列表资源
    psl_free((psl_ctx_t *)psl);

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(cmdline)
    def test(self):
        self.cmd("gcc -o test_psl test_psl.c -lpsl")
        code, result = self.cmd("./test_psl")
        self.assertTrue(code == 0, msg="边界情况处理异常！")
            
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf test_psl*")