# -*- encoding: utf-8 -*-

"""
@File:      tc_libpsl_libpsl_fun007.py
@Time:      2024/3/8 11:00:25
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libpsl_libpsl_fun007.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libpsl libpsl-devel gcc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >test_psl.c<<"EOF"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libpsl.h>

//使用外部的公共后缀列表文件来更新 psl_ctx_t 上下文，并验证更新是否正确
int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s path_to_latest_psl_file\\n", argv[0]);
        return EXIT_FAILURE;
    }

    const char *latest_psl_file_path = argv[1];
    const psl_ctx_t *psl;
    
    // 加载最新的公共后缀列表文件
    psl = psl_load_file(latest_psl_file_path);
    if (!psl) {
        fprintf(stderr, "Error: Could not load the public suffix list from provided file\\n");
        return EXIT_FAILURE;
    }

    // 在此处添加测试用例以验证更新是否正确
    // 例如，可以用一些预期会受到列表更新影响的域名进行测试
    const char *test_domain = "some.example.newtld";
    const char *registrable_domain = psl_registrable_domain(psl, test_domain);

    if (registrable_domain) {
        printf("Registrable domain of '%s' is '%s'.\\n", test_domain, registrable_domain);
    } else {
        printf("No registrable domain found for '%s'.\\n", test_domain);
    }

    // 释放公共后缀列表资源
    psl_free((psl_ctx_t *)psl);

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(cmdline)
    def test(self):
        # 获取最新公共后缀
        self.cmd("wget https://publicsuffix.org/list/public_suffix_list.dat -O public_suffix_list.dat")
        self.cmd("gcc -o test_psl test_psl.c -lpsl")
        code, result = self.cmd("./test_psl public_suffix_list.dat")
        self.assertEqual("Registrable domain of 'some.example.newtld' is 'example.newtld'.", result)
            
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf test_psl* public_suffix_list.dat")