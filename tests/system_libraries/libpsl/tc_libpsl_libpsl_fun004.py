# -*- encoding: utf-8 -*-

"""
@File:      tc_libpsl_libpsl_fun004.py
@Time:      2024/3/7 11:07:25
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libpsl_libpsl_fun004.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libpsl libpsl-devel gcc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >test_psl.c<<"EOF"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libpsl.h>

int main() {
    // 定义要测试的无效域名
    const char *domain = "this..is..invalid";
    
    // 加载内置的公共后缀列表
    const psl_ctx_t *psl = psl_builtin();
    
    // 检查是否成功加载公共后缀列表
    if (psl == NULL) {
        fprintf(stderr, "Error: Could not load the PSL data.\\n");
        return EXIT_FAILURE;
    }
    
    // 检查给定的域名是否有效
    if (strchr(domain, '.') == NULL || strstr(domain, "..") != NULL) {
        printf("Error: '%s' is an invalid domain name.\\n", domain);
    } else {
        // 检查给定的域名是否是公共后缀
        if (psl_is_public_suffix(psl, domain)) {
            printf("%s is a public suffix.\\n", domain);
        } else {
            printf("%s is not a public suffix.\\n", domain);
        }
    }
    
    // 释放公共后缀列表资源
    psl_free((psl_ctx_t *)psl);
    
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(cmdline)
    def test(self):
        self.cmd("gcc -o test_psl test_psl.c -lpsl")
        code, result = self.cmd("./test_psl")
        self.assertEqual("Error: 'this..is..invalid' is an invalid domain name.", result)
            
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf test_psl*")