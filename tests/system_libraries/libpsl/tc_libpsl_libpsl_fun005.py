# -*- encoding: utf-8 -*-

"""
@File:      tc_libpsl_libpsl_fun005.py
@Time:      2024/3/7 11:07:25
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libpsl_libpsl_fun005.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libpsl libpsl-devel gcc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >test_psl.c<<"EOF"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libpsl.h>

int main() {
    const char *domain_to_test = "sub.example.com";
    const psl_ctx_t *psl;
    const char *registrable_domain;

    // 加载内置的公共后缀列表
    psl = psl_builtin();
    if (!psl) {
        fprintf(stderr, "Error: Could not load the public suffix list\\n");
        return EXIT_FAILURE;
    }

    // 提取域名的可注册部分
    registrable_domain = psl_registrable_domain(psl, domain_to_test);
    if (registrable_domain) {
        printf("The registrable domain of '%s' is '%s'.\\n", domain_to_test, registrable_domain);
    } else {
        printf("No registrable domain found for '%s'.\\n", domain_to_test);
    }

    // 释放公共后缀列表资源
    psl_free((psl_ctx_t *)psl);

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(cmdline)
    def test(self):
        self.cmd("gcc -o test_psl test_psl.c -lpsl")
        code, result = self.cmd("./test_psl")
        self.assertEqual("The registrable domain of 'sub.example.com' is 'example.com'.", result)
            
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf test_psl*")