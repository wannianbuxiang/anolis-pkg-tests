# -*- encoding: utf-8 -*-

"""
@File:      tc_libpsl_libpsl_fun008.py
@Time:      2024/3/8 11:14:05
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libpsl_libpsl_fun008.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libpsl libpsl-devel gcc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >test_psl_file.dat<<"EOF"
example.test
aaaaaaa.com
examplewrfe.com
testtest.cnn
EOF'''
        self.cmd(cmdline)
        cmdline = '''cat >test_psl.c<<"EOF"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libpsl.h>

//使用自定义的公共后缀列表，并进行一些基础测试来确认自定义列表被正确加载和使用
int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s path_to_custom_psl_file\\n", argv[0]);
        return EXIT_FAILURE;
    }

    const char *custom_psl_file_path = argv[1];
    const psl_ctx_t *psl;
    const char *test_domains[] = {
        "example.test",    // 假设这是自定义后缀中的一项
        "example.com",     // 已知的公共后缀，用来做对照
        NULL
    };
    
    // 从文件加载自定义的公共后缀列表
    psl = psl_load_file(custom_psl_file_path);
    if (!psl) {
        fprintf(stderr, "Error: Could not load the custom public suffix list from provided file\\n");
        return EXIT_FAILURE;
    }

    // 测试自定义公共后缀列表
    for (int i = 0; test_domains[i]; ++i) {
        if (psl_is_public_suffix(psl, test_domains[i])) {
            printf("The domain '%s' is a public suffix in the custom list.\\n", test_domains[i]);
        } else {
            printf("The domain '%s' is NOT a public suffix in the custom list.\\n", test_domains[i]);
        }
    }

    // 释放自定义公共后缀列表资源
    psl_free((psl_ctx_t *)psl);

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o test_psl test_psl.c -lpsl")
        code, result = self.cmd("./test_psl test_psl_file.dat")
        self.assertIn("The domain 'example.test' is a public suffix in the custom list.", result)
        self.assertIn("The domain 'example.com' is NOT a public suffix in the custom list.", result)
            
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf test_psl*")