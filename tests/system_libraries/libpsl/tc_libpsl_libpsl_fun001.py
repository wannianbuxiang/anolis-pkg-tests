# -*- encoding: utf-8 -*-

"""
@File:      tc_libpsl_libpsl_fun001.py
@Time:      2024/3/8 10:07:25
@Author:    wangyaru
@Version:   1.0
@Contact:   wb-wyr940190@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    wangyaru
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libpsl_libpsl_fun001.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libpsl libpsl-devel gcc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = '''cat >test_psl.c<<"EOF"
#include <stdio.h>
#include <stdlib.h>
#include <libpsl.h>

int main() {
    // 已知的公共后缀列表
    const char *known_suffixes[] = {".com", ".org", ".co.uk", NULL};
    const psl_ctx_t *psl;
    int i;

    // 加载内置的公共后缀列表
    psl = psl_builtin();

    if (psl == NULL) {
        fprintf(stderr, "Error: Could not load the public suffix list\\n");
        return EXIT_FAILURE;
    }

    // 遍历已知的公共后缀，检查它们是否被识别
    for (i = 0; known_suffixes[i]; ++i) {
        const char *suffix = known_suffixes[i];
        if (psl_is_public_suffix(psl, suffix)) {
            printf("The suffix '%s' is a known public suffix.\\n", suffix);
        } else {
            printf("The suffix '%s' is NOT recognized as a public suffix.\\n", suffix);
        }
    }

    // 释放公共后缀列表资源
    psl_free((psl_ctx_t *)psl);

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(cmdline)
    def test(self):
        self.cmd("gcc -o test_psl test_psl.c -lpsl")
        code, result = self.cmd("./test_psl")
        self.assertIn("The suffix '.com' is a known public suffix.", result)
        self.assertIn("The suffix '.org' is a known public suffix.", result)
        self.assertIn("The suffix '.co.uk' is a known public suffix.", result)
            
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf test_psl*")
