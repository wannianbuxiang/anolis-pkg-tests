#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libtasn1_libtasn1_func_001.py
@Time:      2024/04/25 14:20:00
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

import os
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libtasn1_libtasn1_func_001.yaml for details
    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libtasn1 libtasn1-devel gcc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = r'''cat > tasn1_test1.c << EOF
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "libtasn1.h"

unsigned char data[256];      // 分配256字节的缓存来保存DER编码的数据
int data_size = sizeof(data); // 缓存大小

int main()
{
    int result = 0;
    asn1_node definitions = NULL;                           // ASN.1定义结构的指针
    asn1_node asn1_element = NULL;                          // ASN.1元素结构的指针
    char errorDescription[ASN1_MAX_ERROR_DESCRIPTION_SIZE]; // 错误描述
    const char *treefile = "tasn1_test1.asn";                 // ASN.1描述文件的名称

    // 将ASN.1描述文件解析为定义结构
    result = asn1_parser2tree(treefile, &definitions, errorDescription);
    // 检查是否解析成功
    if (result != ASN1_SUCCESS)
    {
        asn1_perror(result);                              // 打印ASN.1错误信息
        printf("解析错误描述: %s\n\n", errorDescription); // 输出错误详细信息
        exit(1);                                          // 解析失败退出程序
    }

    // 根据定义结构创建一个元素
    result = asn1_create_element(definitions, "TEST_TREE.Koko", &asn1_element);
    // 检查元素是否创建成功
    if (result != ASN1_SUCCESS)
    {
        fprintf(stderr, "创建元素出错: ");
        asn1_perror(result); // 打印ASN.1错误信息
        exit(1);             // 创建失败退出程序
    }

    // 向seqint这个序列字段添加一个新的整数
    result = asn1_write_value(asn1_element, "seqint", "NEW", 1);
    if (result != ASN1_SUCCESS)
    {
        fprintf(stderr, "写入seqint值出错: ");
        asn1_perror(result); // 打印ASN.1错误信息
        exit(1);             // 写入失败退出程序
    }

    // 在seqint序列的末尾添加一个整数值1234
    result = asn1_write_value(asn1_element, "seqint.?LAST", "1234", 0);
    if (result != ASN1_SUCCESS)
    {
        fprintf(stderr, "写入seqint.?LAST值出错: ");
        asn1_perror(result); // 打印ASN.1错误信息
        exit(1);             // 写入失败退出程序
    }

    // 向int字段写入一个整数值(注意，此值是用字节字符串直接表示的)
    result = asn1_write_value(asn1_element, "int", "\x0f\xff\x01", 3);
    if (result != ASN1_SUCCESS)
    {
        fprintf(stderr, "写入int值出错: ");
        asn1_perror(result); // 打印ASN.1错误信息
        exit(1);             // 写入失败退出程序
    }

    // 向str字段写入一个OCTET STRING类型的字符串"string"
    result = asn1_write_value(asn1_element, "str", "string", 6);
    if (result != ASN1_SUCCESS)
    {
        fprintf(stderr, "写入str值出错: ");
        asn1_perror(result); // 打印ASN.1错误信息
        exit(1);             // 写入失败退出程序
    }

    // 删除ASN.1定义结构
    asn1_delete_structure(&definitions);

    // 对ASN.1元素进行DER编码
    result = asn1_der_coding(asn1_element, "", data, &data_size, NULL);
    if (result != ASN1_SUCCESS)
    {
        fprintf(stderr, "DER编码出错.\n");
        asn1_perror(result); // 打印ASN.1错误信息
        exit(1);             // 编码失败退出程序
    }

    // 对之前编码的DER数据进行解码，重建ASN.1元素结构
    result = asn1_der_decoding(&asn1_element, data, data_size, NULL);
    if (result != ASN1_SUCCESS)
    {
        fprintf(stderr, "DER解码出错.\n");
        asn1_perror(result); // 打印ASN.1错误信息
        exit(1);             // 解码失败退出程序
    }

    // 删除ASN.1元素结构
    asn1_delete_structure(&asn1_element);

    // 如果正确执行到这里，程序输出详细的成功信息
    printf("编码和解码操作均成功完成。\n");
    exit(0); // 程序正常退出
}
EOF'''
        self.cmd(cmdline)

        cmdline = r'''cat > tasn1_test1.asn << EOF
TEST_TREE { }

DEFINITIONS IMPLICIT TAGS ::=

BEGIN

Koko ::= SEQUENCE {
   seqint SEQUENCE OF INTEGER,
   int INTEGER,
   str OCTET STRING
}
END
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o tasn1_test1 tasn1_test1.c -ltasn1")
        code, tasn1_result = self.cmd("./tasn1_test1")
        self.assertIn('编码和解码操作均成功完成', tasn1_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf tasn1_test1 tasn1_test1.c tasn1_test1.asn')

