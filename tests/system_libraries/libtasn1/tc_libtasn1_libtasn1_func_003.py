#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libtasn1_libtasn1_func_003.py
@Time:      2024/04/25 14:20:00
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

import os
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libtasn1_libtasn1_func_003.yaml for details
    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libtasn1 libtasn1-devel gcc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = r'''cat > tasn1_test3.c << EOF
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "libtasn1.h"

// 测试数据结构，记录每个测试用例的详细信息
struct tv
{
    int bitlen;         // 位字符串的长度
    const char *bitstr; // 位字符串
    int derlen;         // DER 编码的长度
    const char *der;    // 预期的 DER 编码
};

// 测试用例数组
static const struct tv tv[] = {
    {0, "", 2, "\x01\x00"},
    {1, "\x00", 3, "\x02\x07\x00"},
    {2, "\x00", 3, "\x02\x06\x00"},
    {3, "\x00", 3, "\x02\x05\x00"},
    {4, "\x00", 3, "\x02\x04\x00"},
    {5, "\x00", 3, "\x02\x03\x00"},
    {6, "\x00", 3, "\x02\x02\x00"},
    {7, "\x00", 3, "\x02\x01\x00"},
    {8, "\x00\x00", 3, "\x02\x00\x00"},
    {9, "\x00\x00", 4, "\x03\x07\x00\x00"},
    {10, "\x00\x00", 4, "\x03\x06\x00\x00"},
    {11, "\x00\x00", 4, "\x03\x05\x00\x00"},
    {12, "\x00\x00", 4, "\x03\x04\x00\x00"},
    {13, "\x00\x00", 4, "\x03\x03\x00\x00"},
    {14, "\x00\x00", 4, "\x03\x02\x00\x00"},
    {15, "\x00\x00", 4, "\x03\x01\x00\x00"},
    {16, "\x00\x00", 4, "\x03\x00\x00\x00"},
    {17, "\x00\x00\x00", 5, "\x04\x07\x00\x00\x00"},
    {18, "\x00\x00\x00", 5, "\x04\x06\x00\x00\x00"},
    {19, "\x00\x00\x00", 5, "\x04\x05\x00\x00\x00"},
    {1, "\xFF", 3, "\x02\x07\x80"},
    {2, "\xFF", 3, "\x02\x06\xc0"},
    {3, "\xFF", 3, "\x02\x05\xe0"},
    {4, "\xFF", 3, "\x02\x04\xf0"},
    {5, "\xFF", 3, "\x02\x03\xf8"},
    {6, "\xFF", 3, "\x02\x02\xfc"},
    {7, "\xFF", 3, "\x02\x01\xfe"},
    {8, "\xFF\xFF", 3, "\x02\x00\xff"},
    {9, "\xFF\xFF", 4, "\x03\x07\xff\x80"},
    {10, "\xFF\xFF", 4, "\x03\x06\xff\xc0"},
    {11, "\xFF\xFF", 4, "\x03\x05\xff\xe0"},
    {12, "\xFF\xFF", 4, "\x03\x04\xff\xf0"},
    {13, "\xFF\xFF", 4, "\x03\x03\xff\xf8"},
    {14, "\xFF\xFF", 4, "\x03\x02\xff\xfc"},
    {15, "\xFF\xFF", 4, "\x03\x01\xff\xfe"},
    {16, "\xFF\xFF", 4, "\x03\x00\xff\xff"},
    {17, "\xFF\xFF\xFF", 5, "\x04\x07\xff\xff\x80"},
    {18, "\xFF\xFF\xFF", 5, "\x04\x06\xff\xff\xc0"},
    {19, "\xFF\xFF\xFF", 5, "\x04\x05\xff\xff\xe0"},
};

int main()
{
    int result;
    unsigned char der[100]; // 用于存放 DER 编码的缓冲区
    unsigned char str[100]; // 用于存放转换回来的位字符串的缓冲区
    int der_len = sizeof(der);
    int str_size = sizeof(str);
    int ret_len, bit_len;
    size_t i;

    // 空数据测试，验证空情况下的错误处理
    asn1_bit_der(NULL, 0, der, &der_len);
    result = asn1_get_bit_der(der, 0, &ret_len, str, str_size, &bit_len);
    if (result != ASN1_GENERIC_ERROR)
    {
        fprintf(stderr, "解码测试失败于测试用例 空数据\n");
        return 1;
    }

    // 对于每个测试向量进行编码和解码以验证操作是否正确
    for (i = 0; i < sizeof(tv) / sizeof(tv[0]); i++)
    {
        // 编码位字符串到DER编码
        asn1_bit_der((const unsigned char *)tv[i].bitstr, tv[i].bitlen, der, &der_len);

        // 校验编码结果是否与预期一致
        if (der_len != tv[i].derlen || memcmp(der, tv[i].der, der_len) != 0)
        {
            fprintf(stderr, "编码测试失败于测试用例 #%lu\n", (unsigned long)i);
            return 1;
        }

        // 解码DER编码回位字符串
        result = asn1_get_bit_der(der, der_len, &ret_len, str, str_size, &bit_len);

        // 校验解码结果是否与预期一致
        if (result != ASN1_SUCCESS || ret_len != tv[i].derlen || bit_len != tv[i].bitlen)
        {
            fprintf(stderr, "解码测试失败于测试用例 #%lu\n", (unsigned long)i);
            return 1;
        }
    }

    printf("所有的 %lu 个测试用例均成功通过。\n", (unsigned long)(sizeof(tv) / sizeof(tv[0])));

    return 0;
}
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o tasn1_test3 tasn1_test3.c -ltasn1")
        code, tasn1_result = self.cmd("./tasn1_test3")
        self.assertIn('所有的 39 个测试用例均成功通过', tasn1_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf tasn1_test3 tasn1_test3.c')

