#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libtasn1_libtasn1_func_002.py
@Time:      2024/04/25 14:20:00
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

import os
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libtasn1_libtasn1_func_002.yaml for details
    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libtasn1 libtasn1-devel gcc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = r'''cat > tasn1_test2.c << EOF
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include "libtasn1.h"

int main()
{

    // 测试超过 long 类型范围的值是否被拒绝
    {
        unsigned char der[] = "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF";
        long l;
        int len;
        l = asn1_get_length_der(der, sizeof der, &len);

        if (l == -2L)
        {
            printf("测试成功: asn1_get_length_der 正确拒绝大数.\n");
        }
        else
        {
            printf("测试失败: asn1_get_length_der 没有拒绝大数 (l %ld len %d)\n", l, len);
            return 1;
        }
    }

    // 测试超过 int 类型但不超过 long 类型范围的值是否被拒绝
    if (LONG_MAX > INT_MAX)
    {
        unsigned long num = ((long)UINT_MAX) << 2;
        unsigned char der[20];
        int der_len;
        long l;
        int len;
        asn1_length_der(num, der, &der_len);
        l = asn1_get_length_der(der, der_len, &len);

        if (l == -2L)
        {
            printf("测试成功: asn1_get_length_der 正确拒绝超过 int 类型的数.\n");
        }
        else
        {
            printf("测试失败: asn1_get_length_der 没有拒绝超过 int 类型的数 (l %ld len %d)\n", l, len);
            return 1;
        }
    }

    // 测试输入字符串太小是否被拒绝
    {
        unsigned long num = 64;
        unsigned char der[20];
        int der_len;
        long l;
        int len;
        asn1_length_der(num, der, &der_len);
        // 故意设置 der_len 为 der 的实际大小，而不是 asn1_length_der 函数生成的长度
        der_len = sizeof(der);
        l = asn1_get_length_der(der, der_len, &len);

        if (l == -4L)
        {
            printf("测试成功: asn1_get_length_der 正确处理输入字符串过小的情况.\n");
        }
        else
        {
            printf("测试失败: asn1_get_length_der 处理输入字符串过小时出错 (l %ld len %d)\n", l, len);
            return 1;
        }
    }

    // 针对不同的大数值进行测试，确保输入字符串过小时被拒绝
    {
        unsigned long num = 1073741824; // 一个很大的数值
        unsigned char der[20];
        int der_len;
        long l;
        int len;
        asn1_length_der(num, der, &der_len);
        // 故意设置 der_len 为 der 的实际大小，而不是 asn1_length_der 函数生成的长度
        der_len = sizeof(der);
        l = asn1_get_length_der(der, der_len, &len);

        if (l == -4L)
        {
            printf("测试成功: asn1_get_length_der 正确处理大整数值的输入字符串过小的情况.\n");
        }
        else
        {
            printf("测试失败: asn1_get_length_der 处理大整数值的输入字符串过小时出错 (l %ld len %d)\n", l, len);
            return 1;
        }
    }

    // 所有测试通过后，输出测试成功
    printf("所有测试均通过！\\n");

    return 0; // 程序退出
}
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o tasn1_test2 tasn1_test2.c -ltasn1")
        code, tasn1_result = self.cmd("./tasn1_test2")
        self.assertIn('所有测试均通过', tasn1_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf tasn1_test2 tasn1_test2.c')

