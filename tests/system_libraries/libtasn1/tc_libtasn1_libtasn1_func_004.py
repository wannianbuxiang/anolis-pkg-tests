#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libtasn1_libtasn1_func_004.py
@Time:      2024/04/25 14:20:00
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

import os
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libtasn1_libtasn1_func_004.yaml for details
    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libtasn1 libtasn1-devel gcc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = r'''cat > tasn1_test4.c << EOF
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "libtasn1.h"

// 定义了用于测试 DER 编解码的结构体
struct tv
{
    unsigned int etype;   // 字符串的 ASN.1 类型
    unsigned int str_len; // 未编码字符串的长度
    const void *str;      // 未编码的字符串
    unsigned int der_len; // 预期的 DER 编码长度
    const void *der;      // 预期的 DER 编码
};

// 定义测试矢量数组
static const struct tv tv[] = {
    {ASN1_ETYPE_IA5_STRING, 20,
     "\x63\x73\x63\x61\x40\x70\x61\x73\x73\x70\x6f\x72\x74\x2e\x67\x6f\x76\x2e\x67\x72",
     22,
     "\x16\x14\x63\x73\x63\x61\x40\x70\x61\x73\x73\x70\x6f\x72\x74\x2e\x67\x6f\x76\x2e\x67\x72"},
    {ASN1_ETYPE_PRINTABLE_STRING, 5, "\x4e\x69\x6b\x6f\x73",
     7, "\x13\x05\x4e\x69\x6b\x6f\x73"},
    {ASN1_ETYPE_UTF8_STRING, 12, "Αττική",
     14, "\x0c\x0c\xce\x91\xcf\x84\xcf\x84\xce\xb9\xce\xba\xce\xae"},
    {ASN1_ETYPE_TELETEX_STRING, 15,
     "\x53\x69\x6d\x6f\x6e\x20\x4a\x6f\x73\x65\x66\x73\x73\x6f\x6e",
     17,
     "\x14\x0f\x53\x69\x6d\x6f\x6e\x20\x4a\x6f\x73\x65\x66\x73\x73\x6f\x6e"},
    {ASN1_ETYPE_OCTET_STRING, 36,
     "\x30\x22\x80\x0F\x32\x30\x31\x31\x30\x38\x32\x31\x30\x38\x30\x30\x30\x36\x5A\x81\x0F\x32\x30\x31\x31\x30\x38\x32\x33\x32\x30\x35\x39\x35\x39\x5A",
     38,
     "\x04\x24\x30\x22\x80\x0F\x32\x30\x31\x31\x30\x38\x32\x31\x30\x38\x30\x30\x30\x36\x5A\x81\x0F\x32\x30\x31\x31\x30\x38\x32\x33\x32\x30\x35\x39\x35\x39\x5A"}};

int main()
{
    int ret;
    unsigned char tl[ASN1_MAX_TL_SIZE];
    unsigned int tl_len, der_len, str_len;
    const unsigned char *str;
    unsigned int i;

    // 对于每个测试用例进行测试
    for (i = 0; i < sizeof(tv) / sizeof(tv[0]); i++)
    {
        // 编码测试，将字符串编码为 DER 格式
        tl_len = sizeof(tl);
        ret = asn1_encode_simple_der(tv[i].etype, tv[i].str, tv[i].str_len, tl, &tl_len);
        if (ret != ASN1_SUCCESS)
        {
            fprintf(stderr, "编码错误，在测试 #%u: %s\n", i, asn1_strerror(ret));
            return 1;
        }

        // 验证编码结果的长度和内容是否与预期相符
        der_len = tl_len + tv[i].str_len;
        if (der_len != tv[i].der_len || memcmp(tl, tv[i].der, tl_len) != 0)
        {
            fprintf(stderr, "DER 编码结果不匹配，在测试 #%u (实际长度: %u, 预期长度: %u)\n", i, der_len, tv[i].der_len);
            return 1;
        }

        // 解码测试，将 DER 编码解码回字符串
        ret = asn1_decode_simple_der(tv[i].etype, tv[i].der, tv[i].der_len, &str, &str_len);
        if (ret != ASN1_SUCCESS)
        {
            fprintf(stderr, "解码错误，在测试 #%u: %s\n", i, asn1_strerror(ret));
            return 1;
        }

        // 验证解码结果的长度和内容是否与原始字符串相符
        if (str_len != tv[i].str_len || memcmp(str, tv[i].str, str_len) != 0)
        {
            fprintf(stderr, "DER 解码后的数据不匹配，在测试 #%u (实际长度: %u, 预期长度: %u)\n", i, str_len, tv[i].str_len);
            return 1;
        }
    }

    // 所有测试用例都通过了
    printf("通过了所有的 %u 个编解码测试用例。\n", (unsigned int)(sizeof(tv) / sizeof(tv[0])));

    return 0;
}
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o tasn1_test4 tasn1_test4.c -ltasn1")
        code, tasn1_result = self.cmd("./tasn1_test4")
        self.assertIn('通过了所有的 5 个编解码测试用例', tasn1_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf tasn1_test4 tasn1_test4.c')

