#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libtasn1_libtasn1_func_005.py
@Time:      2024/04/25 14:20:00
@Author:    chenchunhu
@Version:   1.0
@Contact:   wb-cch358909@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    chenchunhu
"""

import os
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libtasn1_libtasn1_func_005.yaml for details
    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libtasn1 libtasn1-devel gcc"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = r'''cat > tasn1_test5.c << EOF
#include "libtasn1.h"

// 函数主入口
int main() {
    int ec = 0; // 错误码，从0开始
    const char *errstr; // 用于接收错误信息字符串的指针
    
    // 循环，提取并打印所有的错误码和对应的错误字符串
    do {
        // 获取当前错误码的错误字符串
        errstr = asn1_strerror(ec);
        // 输出当前错误码和对应的错误字符串
        asn1_perror(ec);
        // 错误码递增，尝试获取下一个错误信息
        ec++;
    } while (errstr); // 当 errstr 不再返回有效信息时，结束循环
    
    // 如果全部错误信息都正确显示，则输出测试成功的消息
    printf("\n测试 libtasn1 asn1_strerror 函数成功。共展示 %d 个错误信息。\n", ec-1);
    return 0; // 返回 0，表示程序成功结束
}
EOF'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o tasn1_test5 tasn1_test5.c -ltasn1")
        code, tasn1_result = self.cmd("./tasn1_test5")
        self.assertIn('测试 libtasn1 asn1_strerror 函数成功。共展示 20 个错误信息', tasn1_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('rm -rf tasn1_test5 tasn1_test5.c')

