#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libldb_libldb_func_004.py
@Time:      2024/04/12 15:08:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libldb_libldb_func_004.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libldb libldb-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_libldb_ldb_binary_encode.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <talloc.h>
#include <ldb.h>

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <hex_string>\n", argv[0]);
        return EXIT_FAILURE;
    }

    // 创建 talloc 内存上下文。
    TALLOC_CTX *mem_ctx = talloc_new(NULL);
    if (!mem_ctx) {
        fprintf(stderr, "Failed to create talloc context.\n");
        return EXIT_FAILURE;
    }

    // 解析十六进制字符串到一个字节数组。
    const char *hex_string = argv[1];
    size_t hex_len = strlen(hex_string);
    if (hex_len % 2 != 0) {
        fprintf(stderr, "Invalid hex string length, must be an even number.\n");
        talloc_free(mem_ctx);
        return EXIT_FAILURE;
    }

    size_t bytes_len = hex_len / 2;
    unsigned char *bytes = talloc_array(mem_ctx, unsigned char, bytes_len);
    if (!bytes) {
        fprintf(stderr, "Failed to allocate memory for bytes array.\n");
        talloc_free(mem_ctx);
        return EXIT_FAILURE;
    }

    for (size_t i = 0; i < bytes_len; i++) {
        if (sscanf(&hex_string[i * 2], "%2hhx", &bytes[i]) != 1) {
            fprintf(stderr, "Invalid hex string.\n");
            talloc_free(mem_ctx);
            return EXIT_FAILURE;
        }
    }

    // 使用 ldb_binary_encode 对数据进行编码。
    struct ldb_val val;
    val.data = bytes;
    val.length = bytes_len;

    char *encoded = ldb_binary_encode(mem_ctx, val);
    if (encoded) {
        printf("Encoded string: %s\n", encoded);
    } else {
        fprintf(stderr, "Failed to encode binary data.\n");
        talloc_free(mem_ctx);
        return EXIT_FAILURE;
    }

    // 清理并退出。
    talloc_free(mem_ctx);
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_libldb_ldb_binary_encode test_libldb_ldb_binary_encode.c -lldb -ltalloc")

    def test(self):
        _, res = self.cmd("./test_libldb_ldb_binary_encode 68656c6c6f776f726c64", ignore_status=True)
        self.assertTrue('helloworld' in res, 'Failed to binary encode for hexadecimal str 68656c6c6f776f726c64(helloworld)')
                   

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_libldb_ldb_binary_encode', 'test_libldb_ldb_binary_encode.c']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")
