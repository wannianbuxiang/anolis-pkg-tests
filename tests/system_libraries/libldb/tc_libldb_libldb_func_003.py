#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libldb_libldb_func_003.py
@Time:      2024/04/12 15:08:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libldb_libldb_func_003.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libldb libldb-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_ldb_binary_encode_string.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <talloc.h>
#include <ldb.h>

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "用法: %s <string>\n", argv[0]);
        return EXIT_FAILURE;
    }

    // 创建一个新的 talloc 上下文
    TALLOC_CTX *mem_ctx = talloc_new(NULL);
    if (mem_ctx == NULL) {
        fprintf(stderr, "无法创建 talloc 上下文\n");
        return EXIT_FAILURE;
    }

    // 获取命令行参数中的字符串
    const char *input_string = argv[1];

    // 使用 libldb 函数进行二进制编码
    char *encoded_string = ldb_binary_encode_string(mem_ctx, input_string);
    if (encoded_string == NULL) {
        fprintf(stderr, "二进制编码失败\n");
        talloc_free(mem_ctx);
        return EXIT_FAILURE;
    }

    // 输出编码后的字符串
    printf("%s\n", encoded_string);

    // 释放内存
    talloc_free(mem_ctx);
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_ldb_binary_encode_string test_ldb_binary_encode_string.c -lldb -ltalloc")

    def test(self):
        _, res = self.cmd("./test_ldb_binary_encode_string 'hello world'", ignore_status=True)
        self.assertTrue('hello\\20world' in res, 'Failed to binary encode to string for str "hello world"')
                   

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_ldb_binary_encode_string', 'test_ldb_binary_encode_string.c']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")
