#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libldb_libldb_func_002.py
@Time:      2024/04/12 15:08:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libldb_libldb_func_002.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libldb libldb-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_libldb_ldb_base64_encode.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <talloc.h>
#include <ldb.h>

// 将十六进制字符串转换为二进制数据
unsigned char* hexstring_to_binary(TALLOC_CTX *mem_ctx, const char* hexstring, size_t* len) {
    if (!hexstring || !len) return NULL;

    size_t hexstring_len = strlen(hexstring);
    if (hexstring_len % 2 != 0) return NULL; // 长度必须为偶数

    *len = hexstring_len / 2;
    unsigned char* buffer = talloc_zero_array(mem_ctx, unsigned char, *len);
    if (!buffer) return NULL;

    for (size_t i = 0; i < *len; i++) {
        sscanf(&hexstring[i * 2], "%2hhx", &buffer[i]);
    }

    return buffer;
}

int main(int argc, char* argv[]) {
    if (argc != 2) {
        fprintf(stderr, "用法: %s <十六进制字符串>\n", argv[0]);
        return EXIT_FAILURE;
    }

    TALLOC_CTX *mem_ctx = NULL;
    mem_ctx = talloc_new(NULL);
    if (!mem_ctx) {
        fprintf(stderr, "内存分配失败\n");
        return EXIT_FAILURE;
    }

    const char* hexstring = argv[1];
    size_t binary_len = 0;
    unsigned char* binary = hexstring_to_binary(mem_ctx, hexstring, &binary_len);

    if (!binary) {
        fprintf(stderr, "无效的十六进制字符串\n");
        talloc_free(mem_ctx);
        return EXIT_FAILURE;
    }

    char* base64_encoded = ldb_base64_encode(mem_ctx, (const char*)binary, (int)binary_len);
    if (base64_encoded) {
        printf("Base64 encoded res: %s\n", base64_encoded);
        talloc_free(base64_encoded);
    } else {
        fprintf(stderr, "Base64编码失败\n");
    }

    talloc_free(mem_ctx);
    return base64_encoded ? EXIT_SUCCESS : EXIT_FAILURE;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_libldb_ldb_base64_encode test_libldb_ldb_base64_encode.c -lldb -ltalloc")

    def test(self):
        _, res = self.cmd("./test_libldb_ldb_base64_encode 726f6f74", ignore_status=True)
        self.assertTrue('cm9vdA==' in res, 'Failed to base64 encode for hexadecimal str 726f6f74(root)')
        _, res = self.cmd("./test_libldb_ldb_base64_encode 74657374303031", ignore_status=True)
        self.assertTrue('dGVzdDAwMQ==' in res, 'Failed to base64 encode for hexadecimal str 726f6f74(test001)')
        _, res = self.cmd("./test_libldb_ldb_base64_encode 31313131", ignore_status=True)
        self.assertTrue('MTExMQ==' in res, 'Failed to base64 encode for hexadecimal str 31313131(1111)')                     

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_libldb_ldb_base64_encode', 'test_libldb_ldb_base64_encode.c']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")
