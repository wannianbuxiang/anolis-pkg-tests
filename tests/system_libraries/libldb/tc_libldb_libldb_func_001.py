#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libldb_libldb_func_001.py
@Time:      2024/04/12 15:08:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libldb_libldb_func_001.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libldb libldb-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_libldb_ldb_base64_decode.c << EOF
#include <ldb.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    // 检查是否提供了一个命令行参数（Base64编码的字符串）
    if (argc != 2) {
        fprintf(stderr, "用法: %s <Base64字符串>\n", argv[0]);
        return EXIT_FAILURE;
    }

    // 获取Base64编码字符串参数
    char *base64_encoded_string = argv[1];

    // 输出原始的Base64编码字符串
    printf("原始Base64编码字符串: %s\n", base64_encoded_string);

    // 解码Base64编码字符串
    int bytes_decoded = ldb_base64_decode(base64_encoded_string);

    if (bytes_decoded < 0) {
        fprintf(stderr, "解码失败\n");
        return EXIT_FAILURE;
    } else {
        printf("解码成功: %d 字节\n", bytes_decoded);
        // 打印解码后的字符串
        // 注意：Base64解码后的数据可能不是一个以NULL结尾的字符串
        // 所以我们不能使用 %s 格式化字符串直接打印
        fwrite(base64_encoded_string, 1, bytes_decoded, stdout);
        printf("\n");
    }
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_libldb_ldb_base64_decode test_libldb_ldb_base64_decode.c -lldb")

    def test(self):
        _, res = self.cmd("./test_libldb_ldb_base64_decode cm9vdAo=", ignore_status=True)
        self.assertTrue('root' in res, 'Failed to base64 decode cm9vdAo=')
        _, res = self.cmd("./test_libldb_ldb_base64_decode dGVzdAo=", ignore_status=True)
        self.assertTrue('test' in res, 'Failed to base64 decode dGVzdAo=')
        _, res = self.cmd("./test_libldb_ldb_base64_decode MTExMQo=", ignore_status=True)
        self.assertTrue('1111' in res, 'Failed to base64 decode MTExMQo=')
        _, res = self.cmd("./test_libldb_ldb_base64_decode dGVzdDAwMQo=", ignore_status=True)
        self.assertTrue('test001' in res, 'Failed to base64 decode dGVzdDAwMQo=')

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_libldb_ldb_base64_decode', 'test_libldb_ldb_base64_decode.c']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")