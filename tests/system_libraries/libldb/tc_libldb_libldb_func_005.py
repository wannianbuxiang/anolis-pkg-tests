#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libldb_libldb_func_005.py
@Time:      2024/04/12 15:08:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libldb_libldb_func_005.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libldb libldb-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_libldb_ldb_dn_get_component_name.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <talloc.h>
#include <ldb.h>

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <distinguished_name>\n", argv[0]);
        return EXIT_FAILURE;
    }

    // 创建 talloc 内存上下文
    TALLOC_CTX *mem_ctx = talloc_new(NULL);
    if (mem_ctx == NULL) {
        fprintf(stderr, "Failed to create talloc context.\n");
        return EXIT_FAILURE;
    }

    // 初始化ldb context
    struct ldb_context *ldb = ldb_init(mem_ctx, 0);
    if (ldb == NULL) {
        fprintf(stderr, "Failed to create ldb context.\n");
        talloc_free(mem_ctx);
        return EXIT_FAILURE;
    }

    // 初始化 LDB DN 结构
    struct ldb_dn *dn = ldb_dn_new(mem_ctx, ldb, argv[1]);
    if (dn == NULL || ldb_dn_validate(dn) == false) {
        fprintf(stderr, "Invalid DN specified.\n");
        talloc_free(mem_ctx);
        return EXIT_FAILURE;
    }

    // 检索并打印每个组件的名称
    unsigned int comp_count = ldb_dn_get_comp_num(dn);
    for (unsigned int i = 0; i < comp_count; ++i) {
        const char *comp_name = ldb_dn_get_component_name(dn, i);
        if (comp_name) {
            printf("Component %u: %s\n", i, comp_name);
        } else {
            fprintf(stderr, "Failed to get component name for component #%u\n", i);
            talloc_free(mem_ctx);
            return EXIT_FAILURE;
        }
    }
    // 释放资源
    talloc_free(mem_ctx);
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_libldb_ldb_dn_get_component_name test_libldb_ldb_dn_get_component_name.c -lldb -ltalloc")

    def test(self):
        self.cmd('./test_libldb_ldb_dn_get_component_name cn=admin,dc=domain,dc=com')
        self.cmd('./test_libldb_ldb_dn_get_component_name cn=xxx,dc=xxx,dc=xxx')

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_libldb_ldb_dn_get_component_name', 'test_libldb_ldb_dn_get_component_name.c']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")
