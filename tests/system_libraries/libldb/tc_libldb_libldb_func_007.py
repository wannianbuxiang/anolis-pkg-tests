#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libldb_libldb_func_007.py
@Time:      2024/04/12 15:08:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libldb_libldb_func_007.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libldb libldb-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_libldb_ldb_msg_find_val.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <talloc.h>
#include <ldb.h>
#include <string.h>

int main(int argc, char *argv[]) {
    struct ldb_message_element el;
    struct ldb_val val_to_find;
    struct ldb_val *val;
    const char *attr_name = "cn"; // 假设我们要搜索的属性名是 "cn"
    const char *attr_value; // 我们要找的值将是命令行参数传入的值

    if (argc != 2) {
        fprintf(stderr, "Usage: %s <value-to-find>\n", argv[0]);
        return EXIT_FAILURE;
    }
    attr_value = argv[1];

    // 这里我们会填充消息元素 el
    // （注意：这正常是由 ldb 或应用程序逻辑完成的）
    // 为了示例，我们将创建一个元素，其中包含几个硬编码的值
    el.name = attr_name;
    el.num_values = 4;
    el.values = talloc_array(NULL, struct ldb_val, el.num_values);

    // 填充一些查找值
    el.values[0].data = (uint8_t *)"John Doe";
    el.values[0].length = sizeof("John Doe") - 1;
    el.values[1].data = (uint8_t *)"Jane Doe";
    el.values[1].length = sizeof("Jane Doe") - 1;
    el.values[2].data = (uint8_t *)"admin";
    el.values[2].length = sizeof("admin") - 1;
    el.values[3].data = (uint8_t *)"root";
    el.values[3].length = sizeof("root") - 1;

    // 现在我们设置要搜索的值
    val_to_find = (struct ldb_val){
        .data = (uint8_t *)attr_value,
        .length = strlen(attr_value)
    };

    // 使用 ldb_msg_find_val 查找值
    val = ldb_msg_find_val(&el, &val_to_find);

    // 输出结果
    if (val) {
        printf("Found value: %.*s\n", (int)val->length, (char *)val->data);
    } else {
        printf("Value not found.\n");
    }

    // 清理
    talloc_free(el.values);

    return 0;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_libldb_ldb_msg_find_val test_libldb_ldb_msg_find_val.c -lldb -ltalloc")

    def test(self):
        self.cmd("./test_libldb_ldb_msg_find_val 'John Doe'")
        self.cmd("./test_libldb_ldb_msg_find_val 'Jane Doe'")
        self.cmd("./test_libldb_ldb_msg_find_val 'admin'")
        self.cmd("./test_libldb_ldb_msg_find_val 'root'")
                 
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_libldb_ldb_msg_find_val', 'test_libldb_ldb_msg_find_val.c']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")
