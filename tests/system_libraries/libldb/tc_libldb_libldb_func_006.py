#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libldb_libldb_func_006.py
@Time:      2024/04/12 15:08:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libldb_libldb_func_006.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libldb libldb-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        gen_ldif = r''' cat > tmp_test.ldif << EOF
dn: dc=domain,dc=com
objectClass: dcObject
objectClass: organization
o: domain.com
dc: domain
EOF'''
        code = r'''cat > test_libldb_ldb_ldif_read.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <talloc.h>
#include <ldb.h>

// 一个自定义的文件读取函数，匹配 ldb_ldif_read 需要的 fgetc_fn 函数签名
static int my_fgetc_fn(void *private_data) {
    FILE *file = (FILE *)private_data;
    return fgetc(file);
}

int main(int argc, char *argv[]) {
    TALLOC_CTX *mem_ctx;
    struct ldb_context *ldb;
    struct ldb_ldif *ldif;
    FILE *file;

    if (argc != 2) {
        fprintf(stderr, "Usage: %s <ldif-file>\n", argv[0]);
        return EXIT_FAILURE;
    }

    // 为 ldb 设置一个 memory context
    mem_ctx = talloc_new(NULL);
    if (mem_ctx == NULL) {
        fprintf(stderr, "Failed to allocate memory context.\n");
        return EXIT_FAILURE;
    }

    // 初始化 ldb
    ldb = ldb_init(mem_ctx, 0);
    if (ldb == NULL) {
        fprintf(stderr, "Failed to initialize ldb context.\n");
        talloc_free(mem_ctx);
        return EXIT_FAILURE;
    }

    // 打开 LDIF 文件
    file = fopen(argv[1], "r");
    if (file == NULL) {
        perror("Cannot open LDIF file");
        talloc_free(mem_ctx);
        return EXIT_FAILURE;
    }

    // 使用自定义的文件读取函数读取 LDIF 数据
    ldif = ldb_ldif_read(ldb, my_fgetc_fn, file);
    if (ldif == NULL) {
        fprintf(stderr, "Failed to read LDIF data.\n");
        fclose(file);
        talloc_free(ldb);
        talloc_free(mem_ctx);
        return EXIT_FAILURE;
    }

    // 打印读取到的 LDIF 数据
    // 注意：这里仅为演示，实际情况需要处理 ldb_ldif中可能包含的多个block的情况
    printf("Read LDIF Data:\n%s\n",  ldb_dn_get_linearized(ldif->msg->dn));

    // 最终释放 ldb_ldif 结构
    ldb_ldif_read_free(ldb, ldif);

    // 关闭文件并清理
    fclose(file);
    talloc_free(ldb);
    talloc_free(mem_ctx);

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(gen_ldif)
        self.cmd(code)
        self.cmd("gcc -o test_libldb_ldb_ldif_read test_libldb_ldb_ldif_read.c -lldb -ltalloc")

    def test(self):
       self.cmd('./test_libldb_ldb_ldif_read tmp_test.ldif')


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_libldb_ldb_ldif_read', 'test_libldb_ldb_ldif_read.c', 'tmp_test.ldif']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")
