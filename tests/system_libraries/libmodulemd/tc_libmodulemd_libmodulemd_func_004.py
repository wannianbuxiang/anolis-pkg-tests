#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libmodulemd_libmodulemd_func_004.py
@Time:      2024/03/05 09:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libmodulemd_libmodulemd_func_004.yaml for details
    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libmodulemd libmodulemd-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > libmodulemd_modulemd_build_config_validate.c << EOF
#include <glib.h>
#include <modulemd-2.0/modulemd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    g_autoptr(ModulemdBuildConfig) build_config = NULL;
    g_autoptr(GError) error = NULL;
    gboolean valid;

    // Ensure the correct number of command-line arguments
    if (argc != 4) {
        fprintf(stderr, "Usage: %s <context> <platform> <buildtime_component>\n", argv[0]);
        return EXIT_FAILURE;
    }

    // Create a ModulemdBuildConfig object
    build_config = modulemd_build_config_new();

    // Set properties from command-line arguments
    modulemd_build_config_set_context(build_config, argv[1]);
    modulemd_build_config_set_platform(build_config, argv[2]);
    modulemd_build_config_add_buildtime_requirement(build_config, argv[3], "1.0-1");

    // Validate the ModulemdBuildConfig object
    valid = modulemd_build_config_validate(build_config, &error);

    if (!valid) {
        // If validation failed, print the error message
        fprintf(stderr, "Validation failed: %s\n", error->message);
        return EXIT_FAILURE;
    }

    // If validation succeeded, print a success message
    printf("Validation succeeded!\n");

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o libmodulemd_modulemd_build_config_validate libmodulemd_modulemd_build_config_validate.c `pkg-config --cflags --libs modulemd-2.0 glib-2.0`")

    def test(self):
        self.cmd("./libmodulemd_modulemd_build_config_validate c0ffee42 f33 bash")
        self.cmd("./libmodulemd_modulemd_build_config_validate mytest an23 gcc")
        self.cmd("./libmodulemd_modulemd_build_config_validate context platform1 component")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['libmodulemd_modulemd_build_config_validate.c', 'libmodulemd_modulemd_build_config_validate']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")
