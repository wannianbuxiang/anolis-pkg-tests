#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libmodulemd_libmodulemd_func_001.py
@Time:      2024/03/05 09:20:00
@Author:    smj01095381
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    smj01095381
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libmodulemd_libmodulemd_func_001.yaml for details
    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libmodulemd libmodulemd-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > libmodulemd_modulemd_build_config_get_platform.c << EOF
#include <glib.h>
#include <modulemd-2.0/modulemd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    g_autoptr(ModulemdBuildConfig) build_config = NULL;
    const gchar *platform;
    const gchar *context;
    GStrv runtime_modules = NULL;
    const gchar *runtime_stream = NULL;

    // Initialize a new ModulemdBuildConfig
    build_config = modulemd_build_config_new();
    if (!build_config) {
        fprintf(stderr, "Could not create ModulemdBuildConfig\n");
        return EXIT_FAILURE;
    }

    // Set the context for this build configuration
    modulemd_build_config_set_context(build_config, "abcd1234");

    // Set the platform for this build configuration
    modulemd_build_config_set_platform(build_config, argv[1]);

    // Add a runtime requirement
    modulemd_build_config_add_runtime_requirement(build_config, "runtime_dependency", "stable");

    // Validate the build configuration
    g_autoptr(GError) error = NULL;
    if (!modulemd_build_config_validate(build_config, &error)) {
        fprintf(stderr, "Build config validation failed: %s\n", error->message);
        return EXIT_FAILURE;
    }

    // Retrieve and print the context
    context = modulemd_build_config_get_context(build_config);
    printf("Context: %s\n", context);

    // Retrieve and print the platform
    platform = modulemd_build_config_get_platform(build_config);
    printf("Platform: %s\n", platform);

    // Retrieve and print the runtime requirements
    runtime_modules = modulemd_build_config_get_runtime_modules_as_strv(build_config);
    for (gsize i = 0; runtime_modules[i]; i++) {
        runtime_stream = modulemd_build_config_get_runtime_requirement_stream(build_config, runtime_modules[i]);
        printf("Runtime Module: %s\n", runtime_modules[i]);
        printf("Runtime Stream: %s\n", runtime_stream);
    }

    // Free the runtime_modules array
    g_strfreev(runtime_modules);

    // Clean up and exit
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o libmodulemd_modulemd_build_config_get_platform libmodulemd_modulemd_build_config_get_platform.c `pkg-config --cflags --libs modulemd-2.0 glib-2.0` -g")

    def test(self):
        self.cmd("./libmodulemd_modulemd_build_config_get_platform f33")
        self.cmd("./libmodulemd_modulemd_build_config_get_platform an23")
        self.cmd("./libmodulemd_modulemd_build_config_get_platform an8")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['libmodulemd_modulemd_build_config_get_platform.c', 'libmodulemd_modulemd_build_config_get_platform']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")
