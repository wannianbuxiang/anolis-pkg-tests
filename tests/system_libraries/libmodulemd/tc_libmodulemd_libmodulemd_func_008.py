#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libmodulemd_libmodulemd_func_008.py
@Time:      2024/03/05 09:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libmodulemd_libmodulemd_func_008.yaml for details
    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libmodulemd libmodulemd-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        gen_yaml_f = r'''cat > gen_yaml_to_get_repos.yaml <<EOF
---
document: modulemd
version: 2
data:
  name: mymodule
  stream: mystream
  version: 1
  context: c0ffee43
  arch: noarch
  summary: An example module
  description: >-
    A simple module for demonstration purposes.
  license:
    module: [ MIT ]
  components:
    rpms:
      examplerpm:
        rationale: Required for the example module
        ref: main
        repository: https://example.com/repositories/examplerpm
        cache: https://example.com/cache
        buildorder: 10
EOF'''
        code = r'''cat > libmodulemd_modulemd_component_rpm_get_repository.c << EOF
#include <glib.h>
#include <modulemd-2.0/modulemd.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
    g_autoptr(GError) error = NULL;
    g_autoptr(ModulemdModuleIndex) index = NULL;
    g_autoptr(GPtrArray) failures = NULL;
    ModulemdModule *module = NULL;
    ModulemdModuleStream *stream = NULL;
    ModulemdComponentRpm *component_rpm = NULL;
    const gchar *repository = NULL;
    FILE *yaml_stream = NULL;
    guint64 version;
    const gchar *context;
    const gchar *arch;

    // Ensure the correct number of command-line arguments
    if (argc != 7) {
        fprintf(stderr, "Usage: %s <YAML_FILE_PATH> <MODULE_NAME> <STREAM_NAME> <VERSION> <CONTEXT> <ARCH>\n", argv[0]);
        return EXIT_FAILURE;
    }

    const gchar *yaml_file_path = argv[1];
    const gchar *module_name = argv[2];
    const gchar *stream_name = argv[3];
    version = g_ascii_strtoull(argv[4], NULL, 10); // Assuming version is passed as a string
    context = argv[5];
    arch = argv[6];

    // Check if the YAML file exists and is readable
    if (access(yaml_file_path, R_OK) != 0) {
        perror("Error accessing the specified YAML file");
        return EXIT_FAILURE;
    }

    // Open the YAML file as a stream
    yaml_stream = fopen(yaml_file_path, "rb");
    if (!yaml_stream) {
        fprintf(stderr, "Failed to open the YAML file: %s\n", yaml_file_path);
        return EXIT_FAILURE;
    }

    // Create a new module index
    index = modulemd_module_index_new();
    if (!index) {
        fprintf(stderr, "Could not create a new ModulemdModuleIndex object.\n");
        fclose(yaml_stream);
        return EXIT_FAILURE;
    }

    // Update the index with the contents of the YAML file
    if (!modulemd_module_index_update_from_stream(index, yaml_stream, TRUE, &failures, &error)) {
        fprintf(stderr, "Failed to update the module index: %s\n", error->message);
        fclose(yaml_stream);
        g_clear_error(&error);
        return EXIT_FAILURE;
    }

    // Close the YAML file stream
    fclose(yaml_stream);

    // Retrieve the specified module from the index
    module = modulemd_module_index_get_module(index, module_name);
    if (!module) {
        fprintf(stderr, "Module '%s' could not be found in the index.\n", module_name);
        return EXIT_FAILURE;
    }

    // Retrieve the specific stream of the module
    stream = modulemd_module_get_stream_by_NSVCA(module, stream_name, version, context, arch, &error);
    if (!stream) {
        fprintf(stderr, "Stream '%s' for module '%s' with version '%lu', context '%s', and architecture '%s' could not be found: %s\n",
                stream_name, module_name, (unsigned long)version, context, arch, error->message);
        g_clear_error(&error);
        return EXIT_FAILURE;
    }

    // Retrieve the specific RPM component from the stream
    const gchar *rpm_component_name = "examplerpm"; // Replace with actual component name if needed.
    component_rpm = modulemd_module_stream_v2_get_rpm_component(MODULEMD_MODULE_STREAM_V2(stream), rpm_component_name);
    if (!component_rpm) {
        fprintf(stderr, "RPM component '%s' could not be found in stream '%s' of module '%s'.\n", rpm_component_name, stream_name, module_name);
        return EXIT_FAILURE;
    }

    // Get repository information from the RPM component
    repository = modulemd_component_rpm_get_repository(component_rpm);
    printf("RPM Component: %s, Repository: %s\n", rpm_component_name, (repository ? repository : "(not available)"));
    
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(gen_yaml_f)
        self.cmd(code)
        self.cmd("gcc -o libmodulemd_modulemd_component_rpm_get_repository libmodulemd_modulemd_component_rpm_get_repository.c `pkg-config --cflags --libs modulemd-2.0`")

    def test(self):
        self.cmd("./libmodulemd_modulemd_component_rpm_get_repository gen_yaml_to_get_repos.yaml mymodule mystream 1 c0ffee43 noarch")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['libmodulemd_modulemd_component_rpm_get_repository.c', 'libmodulemd_modulemd_component_rpm_get_repository', 'gen_yaml_to_get_repos.yaml']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")
