#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libmodulemd_libmodulemd_func_005.py
@Time:      2024/03/05 09:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libmodulemd_libmodulemd_func_005.yaml for details
    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libmodulemd libmodulemd-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > libmodulemd_modulemd_buildopts_get_rpm_macros.c << EOF
#include <glib.h>
#include <modulemd-2.0/modulemd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    g_autoptr(ModulemdBuildopts) buildopts = NULL;
    FILE *popen_result = NULL; // Use a regular pointer instead of g_autoptr
    const gchar *rpm_macros;
    char macro_buffer[256];

    // Ensure the correct number of command-line arguments
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <rpm_macro_command>\n", argv[0]);
        return EXIT_FAILURE;
    }

    // Create a ModulemdBuildopts object
    buildopts = modulemd_buildopts_new();

    // Execute the user provided RPM command to get macros and read the output
    popen_result = popen(argv[1], "r");
    if (!popen_result) {
        fprintf(stderr, "Failed to run rpm command\n");
        return EXIT_FAILURE;
    }

    // Read the output from the RPM command into the buffer
    if (fgets(macro_buffer, sizeof(macro_buffer), popen_result) == NULL) {
        fprintf(stderr, "Failed to read rpm macros\n");
        pclose(popen_result); // Make sure to close the popen result
        return EXIT_FAILURE;
    }

    // Close the popen result to avoid resource leaks
    pclose(popen_result);

    // Remove any trailing newline character from the command output
    macro_buffer[strcspn(macro_buffer, "\n")] = 0;

    // Set the RPM macros property using the output from the user provided command
    modulemd_buildopts_set_rpm_macros(buildopts, macro_buffer);

    // Retrieve and print out the RPM macros
    rpm_macros = modulemd_buildopts_get_rpm_macros(buildopts);
    printf("The RPM macros for the build options are: %s\n", rpm_macros);

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o libmodulemd_modulemd_buildopts_get_rpm_macros libmodulemd_modulemd_buildopts_get_rpm_macros.c `pkg-config --cflags --libs modulemd-2.0 glib-2.0`")

    def test(self):
        self.cmd("./libmodulemd_modulemd_buildopts_get_rpm_macros 'rpm -E %{?dist}'")
        self.cmd("./libmodulemd_modulemd_buildopts_get_rpm_macros 'rpm -E %{getenv:HOME}'")
        self.cmd("./libmodulemd_modulemd_buildopts_get_rpm_macros 'rpm -E %{u2p:%{_sourcedir}}'")
        self.cmd("./libmodulemd_modulemd_buildopts_get_rpm_macros 'rpm -E %{u2p:%{_builddir}}'")
        self.cmd("./libmodulemd_modulemd_buildopts_get_rpm_macros 'rpm -E %{u2p:%{buildroot}}'")
        self.cmd("./libmodulemd_modulemd_buildopts_get_rpm_macros 'rpm -E %{u2p:%{_host_cpu}}'")
        self.cmd("./libmodulemd_modulemd_buildopts_get_rpm_macros 'rpm -E %{_target_cpu}'")
        self.cmd("./libmodulemd_modulemd_buildopts_get_rpm_macros 'rpm -E %{__mkdir_p}'")
        self.cmd("./libmodulemd_modulemd_buildopts_get_rpm_macros 'rpm -E %{python3}'")
        

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['libmodulemd_modulemd_buildopts_get_rpm_macros.c', 'libmodulemd_modulemd_buildopts_get_rpm_macros']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")
