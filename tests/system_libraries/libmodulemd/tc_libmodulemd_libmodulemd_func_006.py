#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libmodulemd_libmodulemd_func_006.py
@Time:      2024/03/05 09:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libmodulemd_libmodulemd_func_006.yaml for details
    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libmodulemd libmodulemd-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > libmodulemd_modulemd_buildopts_get_rpm_whitelist_as_strv.c << EOF
#include <glib.h>
#include <modulemd-2.0/modulemd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    g_autoptr(ModulemdBuildopts) buildopts = NULL;
    g_auto(GStrv) whitelist = NULL;
    const gchar *rpm;

    // Ensure at least one RPM package is provided as a command-line argument
    if (argc < 2) {
        fprintf(stderr, "Usage: %s <rpm_package> [rpm_package...]\n", argv[0]);
        return EXIT_FAILURE;
    }

    // Create a ModulemdBuildopts object
    buildopts = modulemd_buildopts_new();

    // Add RPM packages to the whitelist from the command-line arguments
    for (int i = 1; i < argc; i++) {
        rpm = argv[i];
        modulemd_buildopts_add_rpm_to_whitelist(buildopts, rpm);
    }

    // Retrieve and print out the RPM whitelist
    whitelist = modulemd_buildopts_get_rpm_whitelist_as_strv(buildopts);
    printf("RPM whitelist:\n");
    for (gchar **rpm_ptr = whitelist; rpm_ptr && *rpm_ptr; rpm_ptr++) {
        printf(" - %s\n", *rpm_ptr);
    }

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o libmodulemd_modulemd_buildopts_get_rpm_whitelist_as_strv libmodulemd_modulemd_buildopts_get_rpm_whitelist_as_strv.c `pkg-config --cflags --libs modulemd-2.0 glib-2.0`")

    def test(self):
        self.cmd("./libmodulemd_modulemd_buildopts_get_rpm_whitelist_as_strv gcc make bash cmake")


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['libmodulemd_modulemd_buildopts_get_rpm_whitelist_as_strv.c', 'libmodulemd_modulemd_buildopts_get_rpm_whitelist_as_strv']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")
