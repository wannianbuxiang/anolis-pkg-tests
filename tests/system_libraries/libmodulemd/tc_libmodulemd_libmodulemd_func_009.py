#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libmodulemd_libmodulemd_func_009.py
@Time:      2024/03/05 09:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libmodulemd_libmodulemd_func_009.yaml for details
    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libmodulemd libmodulemd-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > libmodulemd_modulemd_component_rpm_get_srpm_buildroot.c << EOF
#include <glib.h>
#include <modulemd-2.0/modulemd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    g_autoptr(ModulemdComponentRpm) component_rpm = NULL;
    gboolean buildroot;

    // Ensure the correct number of command-line arguments
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <RPM_KEY> <BuildrootFlag>\n", argv[0]);
        fprintf(stderr, "BuildrootFlag should be 'true' or 'false'.\n");
        return EXIT_FAILURE;
    }

    // Get the RPM_KEY from the command-line argument
    const gchar *key = argv[1];

    // Parse the BuildrootFlag from the command-line argument
    if (g_strcmp0(argv[2], "true") == 0) {
        buildroot = TRUE;
    } else if (g_strcmp0(argv[2], "false") == 0) {
        buildroot = FALSE;
    } else {
        fprintf(stderr, "Invalid BuildrootFlag value: %s\n", argv[2]);
        return EXIT_FAILURE;
    }

    // Create a ModulemdComponentRpm object with the key
    component_rpm = modulemd_component_rpm_new(key);
    if (!component_rpm) {
        fprintf(stderr, "Failed to create ModulemdComponentRpm object\n");
        return EXIT_FAILURE;
    }

    // Set the SRPM buildroot flag
    modulemd_component_rpm_set_srpm_buildroot(component_rpm, buildroot);

    // Output the buildroot flag to confirm it is set correctly
    gboolean current_buildroot = modulemd_component_rpm_get_srpm_buildroot(component_rpm);
    printf("The SRPM buildroot for component RPM '%s' is set to: %s\n",
           key, current_buildroot ? "true" : "false");

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o libmodulemd_modulemd_component_rpm_get_srpm_buildroot libmodulemd_modulemd_component_rpm_get_srpm_buildroot.c `pkg-config --cflags --libs modulemd-2.0 glib-2.0`")

    def test(self):
        self.cmd("./libmodulemd_modulemd_component_rpm_get_srpm_buildroot pkg true")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['libmodulemd_modulemd_component_rpm_get_srpm_buildroot.c', 'libmodulemd_modulemd_component_rpm_get_srpm_buildroot']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")
