#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libmodulemd_libmodulemd_func_015.py
@Time:      2024/03/05 09:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libmodulemd_libmodulemd_func_015.yaml for details
    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libmodulemd libmodulemd-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        gen_yaml_f = r'''cat > gen_yaml_to_stream_read_stream.yaml <<EOF
---
document: modulemd
version: 2
data:
  name: examplemodule
  stream: stable
  version: 1
  context: 12345678
  summary: An example module
  arch: x86_64|aarch64|noarch
  description: >-
    This is an example module that demonstrates module metadata with RPM artifacts.
  license:
    module:
      - MIT
EOF'''
        code = r'''cat > libmodulemd_modulemd_module_stream_read_stream.c << EOF
#include <glib.h>
#include <modulemd-2.0/modulemd.h>
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

int main(int argc, char *argv[]) {
    g_autoptr(GError) error = NULL;
    ModulemdModuleStream *mstream = NULL;
    FILE *yaml_file = NULL;
    gboolean valid;

    // Check command-line arguments
    if (argc != 5) {
        fprintf(stderr, "Usage: %s <YAML_FILE_PATH> <STRICT_MODE> <MODULE_NAME> <MODULE_STREAM>\n", argv[0]);
        return EXIT_FAILURE;
    }

    const gchar *yaml_file_path = argv[1];
    gboolean strict = g_ascii_strcasecmp(argv[2], "TRUE") == 0; // Convert string to gboolean
    const gchar *module_name = argv[3];
    const gchar *stream_name = argv[4];

    // Open the YAML file as a stream
    yaml_file = fopen(yaml_file_path, "rb");
    if (!yaml_file) {
        fprintf(stderr, "Error opening file: %s\n", yaml_file_path);
        return EXIT_FAILURE;
    }

    // Read the module stream from the file stream
    mstream = modulemd_module_stream_read_stream(yaml_file, strict, module_name, stream_name, &error);
    fclose(yaml_file); // Close the file stream

    if (!mstream) {
        // Handle the error if the Module Stream couldn not be read
        fprintf(stderr, "Failed to read module stream: %s\n", error->message);
        return EXIT_FAILURE;
    } else {
        // Validate the Module Stream
        valid = modulemd_module_stream_validate(mstream, &error);
        if (!valid) {
            // Handle the error if the Module Stream is not valid
            fprintf(stderr, "Module stream did not validate: %s\n", error->message);
            g_clear_object(&mstream);
            return EXIT_FAILURE;
        }
        printf("Module stream is valid.\n");
        // Module Stream was read successfully
        printf("Successfully read module stream for '%s' in stream '%s'\n", module_name, stream_name);
        
        // Output additional module information
        guint64 version = modulemd_module_stream_get_version(mstream);
        const gchar *context = modulemd_module_stream_get_context(mstream);
        const gchar *arch = modulemd_module_stream_get_arch(mstream);
        printf("Version: %" G_GUINT64_FORMAT "\n", version);
        printf("Context: %s\n", context);
        printf("Architecture: %s\n", arch);

        // You can now use the `mstream` object for further processing
    }

    // Clean up the `ModulemdModuleStream` object when done
    g_clear_object(&mstream);

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(gen_yaml_f)
        self.cmd(code)
        self.cmd("gcc -o libmodulemd_modulemd_module_stream_read_stream libmodulemd_modulemd_module_stream_read_stream.c `pkg-config --cflags --libs modulemd-2.0 glib-2.0`")

    def test(self):
        self.cmd("./libmodulemd_modulemd_module_stream_read_stream gen_yaml_to_stream_read_stream.yaml TRUE examplemodule stable")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['libmodulemd_modulemd_module_stream_read_stream.c', 'libmodulemd_modulemd_module_stream_read_stream', 'gen_yaml_to_stream_read_stream.yaml']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")
