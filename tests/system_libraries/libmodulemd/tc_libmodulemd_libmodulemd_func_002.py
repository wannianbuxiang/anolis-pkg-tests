#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libmodulemd_libmodulemd_func_002.py
@Time:      2024/03/05 09:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libmodulemd_libmodulemd_func_002.yaml for details
    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libmodulemd libmodulemd-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > libmodulemd_modulemd_component_get_name.c << EOF
#include <glib.h>
#include <modulemd-2.0/modulemd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    ModulemdComponentRpm *rpm_component;
    const gchar *component_name;

    // Ensure the correct number of command-line arguments
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <component_name>\n", argv[0]);
        return EXIT_FAILURE;
    }

    // Create a ModulemdComponentRpm object using the provided key (component name)
    rpm_component = modulemd_component_rpm_new(argv[1]);

    // Since ModulemdComponentRpm is a subclass of ModulemdComponent, we can use
    // modulemd_component_get_name to retrieve the name (key) of the component
    component_name = modulemd_component_get_name(MODULEMD_COMPONENT(rpm_component));
    printf("The component name is: %s\n", component_name);

    // Clean up resources
    g_object_unref(rpm_component); // Or if you are using g_autoptr: g_clear_object(&rpm_component);

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o libmodulemd_modulemd_component_get_name libmodulemd_modulemd_component_get_name.c `pkg-config --cflags --libs modulemd-2.0 glib-2.0`")

    def test(self):
        self.cmd("./libmodulemd_modulemd_component_get_name COMPONENT_NAME")
        self.cmd("./libmodulemd_modulemd_component_get_name component_name")
        self.cmd("./libmodulemd_modulemd_component_get_name component::name")


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['libmodulemd_modulemd_component_get_name.c', 'libmodulemd_modulemd_component_get_name']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")
