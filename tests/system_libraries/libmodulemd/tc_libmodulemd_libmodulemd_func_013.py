#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libmodulemd_libmodulemd_func_013.py
@Time:      2024/03/05 09:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libmodulemd_libmodulemd_func_013.yaml for details
    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libmodulemd libmodulemd-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        gen_yaml_f = r'''cat > gen_yaml_to_stream_read_s.yaml <<EOF
---
document: modulemd
version: 2
data:
  name: examplemodule
  stream: stable
  version: 1
  context: 12345678
  summary: An example module
  arch: x86_64
  description: >-
    This is an example module that demonstrates module metadata with RPM artifacts.
  license:
    module:
      - MIT
EOF'''
        code = r'''cat > libmodulemd_modulemd_module_stream_read_string.c << EOF
#include <glib.h>
#include <modulemd-2.0/modulemd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    g_autoptr(GError) error = NULL;
    ModulemdModuleStream *mstream = NULL;
    gchar *yaml_content = NULL;
    gsize length = 0;

    // Check command-line arguments
    if (argc != 5) {
        fprintf(stderr, "Usage: %s <YAML_STRING_FILE_PATH> <STRICT_MODE> <MODULE_NAME> <MODULE_STREAM>\n", argv[0]);
        return EXIT_FAILURE;
    }

    const gchar *yaml_file_path = argv[1];
    gboolean strict = g_ascii_strcasecmp(argv[2], "TRUE") == 0; // Convert string to gboolean
    const gchar *module_name = argv[3];
    const gchar *stream_name = argv[4];

    // Read the entire YAML file content into a string
    if (!g_file_get_contents(yaml_file_path, &yaml_content, &length, &error)) {
        fprintf(stderr, "Failed to read file '%s': %s\n", yaml_file_path, error->message);
        return EXIT_FAILURE;
    }

    // Read the module stream from the YAML string
    mstream = modulemd_module_stream_read_string(yaml_content, strict, module_name, stream_name, &error);
    g_free(yaml_content); // Free the file content string

    if (!mstream) {
        // Handle the error if the Module Stream couldn not be read
        fprintf(stderr, "Failed to read module stream: %s\n", error->message);
        return EXIT_FAILURE;
    } else {
        // Module Stream was read successfully
        printf("Successfully read module stream for '%s' in stream '%s'\n", module_name, stream_name);
        // You can now use the `mstream` object for further processing
    }

    // Clean up the `ModulemdModuleStream` object when done
    g_clear_object(&mstream);

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(gen_yaml_f)
        self.cmd(code)
        self.cmd("gcc -o libmodulemd_modulemd_module_stream_read_string libmodulemd_modulemd_module_stream_read_string.c `pkg-config --cflags --libs modulemd-2.0 ` -Wno-deprecated-declarations")

    def test(self):
        self.cmd("./libmodulemd_modulemd_module_stream_read_string gen_yaml_to_stream_read_s.yaml TRUE examplemodule stable")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['libmodulemd_modulemd_module_stream_read_string.c', 'libmodulemd_modulemd_module_stream_read_string', 'gen_yaml_to_stream_read_s.yaml']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")
