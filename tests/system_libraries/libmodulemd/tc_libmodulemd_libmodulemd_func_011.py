#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libmodulemd_libmodulemd_func_011.py
@Time:      2024/03/05 09:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libmodulemd_libmodulemd_func_011.yaml for details
    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libmodulemd libmodulemd-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        gen_yaml_f = r'''cat > gen_yaml_to_get_content.yaml <<EOF
---
document: modulemd
version: 2
data:
  name: examplemodule
  stream: latest
  version: 2
  context: 00000001
  summary: Another example module
  description: Another module for the demonstration.
  license:
    module: MIT
  dependencies:
    - buildrequires:
        platform: f30
      requires:
        platform: f30
  api:
    rpms:
      - baz
      - baz-tools
  components:
    rpms:
      baz:
        rationale: Part of the demonstration.
        repository: https://pagure.io/baz.git
        cache: https://example.com/cache
        ref: master
---
document: modulemd-defaults
version: 1
data:
  module: examplemodule
  stream: latest
EOF'''
        code = r'''cat > libmodulemd_modulemd_module_index_dump_to_string.c << EOF
#include <glib.h>
#include <modulemd-2.0/modulemd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    g_autoptr(GError) error = NULL;
    g_autoptr(ModulemdModuleIndex) index = NULL;
    g_autoptr(GPtrArray) failures = NULL;
    gchar *yaml_content = NULL;

    // Check command-line arguments
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <YAML_FILE_PATH>\n", argv[0]);
        return EXIT_FAILURE;
    }

    // Create a new ModuleIndex
    index = modulemd_module_index_new();
    if (!index) {
        fprintf(stderr, "Could not create module index object.\n");
        return EXIT_FAILURE;
    }

    // Initialize the array for collecting failures
    failures = g_ptr_array_new_with_free_func(g_object_unref);

    // Read the YAML file and update the index
    if (!modulemd_module_index_update_from_file(index, argv[1], TRUE, &failures, &error)) {
        fprintf(stderr, "Error parsing module metadata: %s\n", error->message);
        // Handle each parsing failure (if any)
        if (failures != NULL) {
            for (guint i = 0; i < failures->len; i++) {
                ModulemdSubdocumentInfo *failure = (ModulemdSubdocumentInfo *)g_ptr_array_index(failures, i);
                fprintf(stderr, "Subdocument failed to parse: %s\n", modulemd_subdocument_info_get_gerror(failure)->message);
            }
        }
        return EXIT_FAILURE;
    }

    // Serialize the ModuleIndex back into a YAML string
    yaml_content = modulemd_module_index_dump_to_string(index, &error);
    if (!yaml_content) {
        fprintf(stderr, "Error serializing module metadata back to string: %s\n", error->message);
        return EXIT_FAILURE;
    }

    // Output the entire YAML content
    printf("%s\n", yaml_content);

    // Clean up
    g_free(yaml_content);

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(gen_yaml_f)
        self.cmd(code)
        self.cmd("gcc -o libmodulemd_modulemd_module_index_dump_to_string libmodulemd_modulemd_module_index_dump_to_string.c `pkg-config --cflags --libs modulemd-2.0 glib-2.0`")

    def test(self):
        self.cmd("./libmodulemd_modulemd_module_index_dump_to_string gen_yaml_to_get_content.yaml")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['libmodulemd_modulemd_module_index_dump_to_string.c', 'libmodulemd_modulemd_module_index_dump_to_string', 'gen_yaml_to_get_content.yaml']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")
