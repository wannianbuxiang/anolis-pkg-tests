#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libmodulemd_libmodulemd_func_016.py
@Time:      2024/03/05 09:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libmodulemd_libmodulemd_func_016.yaml for details
    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libmodulemd libmodulemd-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        gen_yaml_f = r'''cat > gen_yaml_to_load_str_get_all.yaml <<EOF
---
document: modulemd
version: 2
data:
  name: examplemodule
  stream: stable
  version: 1
  context: 12345678
  summary: An example module
  arch: x86_64|aarch64|noarch
  description: >-
    This is an example module that demonstrates module metadata with RPM artifacts.
  license:
    module:
      - MIT
EOF'''
        code = r'''cat > libmodulemd_modulemd_load_string_to_get_all.c << EOF
#include <glib.h>
#include <modulemd-2.0/modulemd.h>
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

int main(int argc, char *argv[]) {
    g_autoptr(GError) error = NULL;
    g_autoptr(ModulemdModuleIndex) index = NULL;
    gchar *yaml_content = NULL;
    gsize length = 0;
    GStrv module_names = NULL;
    ModulemdModule *module = NULL;
    g_autoptr(GPtrArray) streams = NULL;
    ModulemdModuleStream *stream = NULL;

    // Check command-line arguments
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <YAML_FILE_PATH>\n", argv[0]);
        return EXIT_FAILURE;
    }

    const gchar *yaml_file_path = argv[1];

    // Read the entire YAML file content into a string
    if (!g_file_get_contents(yaml_file_path, &yaml_content, &length, &error)) {
        fprintf(stderr, "Failed to read file '%s': %s\n", yaml_file_path, error->message);
        return EXIT_FAILURE;
    }

    // Load the module index from the YAML string
    index = modulemd_load_string(yaml_content, &error);
    g_free(yaml_content); // Free the file content string

    if (!index) {
        // Handle the error if the Module Index couldn't be loaded
        fprintf(stderr, "Failed to load module index: %s\n", error->message);
        return EXIT_FAILURE;
    }

    // Module Index was loaded successfully
    printf("Successfully loaded module index from '%s'\n", yaml_file_path);

    // Get the list of all module names in the index
    module_names = modulemd_module_index_get_module_names_as_strv(index);

    // Iterate over all module names and print their details
    for (GStrv name = module_names; name && *name; name++) {
        module = modulemd_module_index_get_module(index, *name);
        if (!module) {
            fprintf(stderr, "Could not retrieve module '%s'\n", *name);
            continue;
        }

        // Print module name
        printf("Module: %s\n", *name);

        // Retrieve all versions of this module's streams and print their details
        streams = modulemd_module_get_all_streams(module);
        for (guint j = 0; j < streams->len; j++) {
            stream = g_ptr_array_index(streams, j);

            // Print stream name and version
            const gchar *stream_name = modulemd_module_stream_get_stream_name(stream);
            guint64 version = modulemd_module_stream_get_version(stream);
            printf("  Stream: %s (Version: %" PRIu64 ")\n", stream_name, version);
            printf("  Stream: %s (Architecture: %s)\n", stream_name, modulemd_module_stream_get_arch(stream));
        }
    }

    // Free the list of module names when done
    g_strfreev(module_names);

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(gen_yaml_f)
        self.cmd(code)
        self.cmd("gcc -o libmodulemd_modulemd_load_string_to_get_all libmodulemd_modulemd_load_string_to_get_all.c `pkg-config --cflags --libs modulemd-2.0`")

    def test(self):
        self.cmd("./libmodulemd_modulemd_load_string_to_get_all gen_yaml_to_load_str_get_all.yaml")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['libmodulemd_modulemd_load_string_to_get_all.c', 'libmodulemd_modulemd_load_string_to_get_all', 'gen_yaml_to_load_str_get_all.yaml']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")
