#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libmodulemd_libmodulemd_func_012.py
@Time:      2024/03/05 09:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libmodulemd_libmodulemd_func_012.yaml for details
    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libmodulemd libmodulemd-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        gen_yaml_f = r'''cat > gen_yaml_to_get_streams.yaml <<EOF
---
document: modulemd
version: 2
data:
  name: examplemodule
  stream: latest
  version: 2
  context: 00000001
  summary: Another example module
  description: Another module for the demonstration.
  arch: noarch
  license:
    module: MIT
EOF'''
        code = r'''cat > libmodulemd_modulemd_module_index_search_streams.c << EOF
#include <glib.h>
#include <modulemd-2.0/modulemd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    g_autoptr(GError) error = NULL;
    g_autoptr(ModulemdModuleIndex) index = NULL;
    g_autoptr(GPtrArray) failures = NULL;
    g_autoptr(GPtrArray) results = NULL;
    ModulemdModuleStream *stream = NULL;

    // Check command-line arguments
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <YAML_FILE_PATH>\n", argv[0]);
        return EXIT_FAILURE;
    }

    // Create a new ModuleIndex
    index = modulemd_module_index_new();
    if (!index) {
        fprintf(stderr, "Could not create module index object.\n");
        return EXIT_FAILURE;
    }

    // Initialize the array for collecting failures
    failures = g_ptr_array_new_with_free_func(g_object_unref);

    // Read the YAML file and update the index
    if (!modulemd_module_index_update_from_file(index, argv[1], TRUE, &failures, &error)) {
        fprintf(stderr, "Error parsing module metadata: %s\n", error->message);
        return EXIT_FAILURE;
    }

    // Search for streams in the index
    // You can replace the NULLs below with actual search criteria if needed
    results = modulemd_module_index_search_streams(index, NULL, NULL, NULL, NULL, NULL);
    if (!results) {
        fprintf(stderr, "No module streams found.\n");
        return EXIT_FAILURE;
    }

    // Iterate over the search results and print stream information
    for (guint i = 0; i < results->len; i++) {
        stream = g_ptr_array_index(results, i);
        printf("Module Name: %s\n", modulemd_module_stream_get_module_name(stream));
        printf("Stream Name: %s\n", modulemd_module_stream_get_stream_name(stream));
        printf("Version: %u\n", modulemd_module_stream_get_version(stream));
        printf("Context: %s\n", modulemd_module_stream_get_context(stream));
        printf("Architecture: %s\n", modulemd_module_stream_get_arch(stream));
        printf("NSVC: %s\n", modulemd_module_stream_get_nsvc_as_string(stream));
        printf("NSVCA: %s\n", modulemd_module_stream_get_NSVCA_as_string(stream));   
        printf("---\n");
    }

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(gen_yaml_f)
        self.cmd(code)
        self.cmd("gcc -o libmodulemd_modulemd_module_index_search_streams libmodulemd_modulemd_module_index_search_streams.c `pkg-config --cflags --libs modulemd-2.0 glib-2.0`")

    def test(self):
        self.cmd("./libmodulemd_modulemd_module_index_search_streams gen_yaml_to_get_streams.yaml")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['libmodulemd_modulemd_module_index_search_streams.c', 'libmodulemd_modulemd_module_index_search_streams', 'gen_yaml_to_get_streams.yaml']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")
