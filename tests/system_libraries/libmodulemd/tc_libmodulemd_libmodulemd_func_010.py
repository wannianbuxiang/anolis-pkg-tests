#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libmodulemd_libmodulemd_func_010.py
@Time:      2024/03/05 09:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libmodulemd_libmodulemd_func_010.yaml for details
    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libmodulemd libmodulemd-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        gen_yaml_f = r'''cat > gen_yaml_to_get_module_name.yaml <<EOF
---
document: modulemd
version: 2
data:
  name: libmodulemd
  stream: stable
  version: 1
  context:
  summary:
  description:
  license:
    module:
      - MIT
  dependencies:
    - buildrequires:
        platform:
      requires:
        platform:
  references:
    community:
    documentation:
    tracker:
  profiles:
    default:
      description:
      rpms:
        - libmodulemd
  api:
    rpms:
      - libmodulemd
  components:
    rpms:
      libmodulemd:
        rationale: 
        repository: 
        cache: 
        ref: main

---
document: modulemd-defaults
version: 1
data:
  module: libmodulemd
  stream: stable
EOF'''
        code = r'''cat > libmodulemd_modulemd_defaults_get_module_name.c << EOF
#include <glib.h>
#include <modulemd-2.0/modulemd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    g_autoptr(GError) error = NULL;
    g_autoptr(ModulemdModuleIndex) index = NULL;
    g_autoptr(GPtrArray) failures = NULL;
    GHashTable *default_streams = NULL;
    const gchar *module_name;
    const gchar *default_stream;
    GHashTableIter iter;
    gpointer key, value;

    // Check command-line arguments
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <YAML_FILE_PATH>\n", argv[0]);
        return EXIT_FAILURE;
    }

    // Create a new ModuleIndex
    index = modulemd_module_index_new();
    if (!index) {
        fprintf(stderr, "Could not create module index object.\n");
        return EXIT_FAILURE;
    }

    // Initialize the array for collecting failures
    failures = g_ptr_array_new_with_free_func(g_object_unref);

    // Read the YAML file and update the index
    if (!modulemd_module_index_update_from_file(index, argv[1], TRUE, &failures, &error)) {
        fprintf(stderr, "Error parsing module metadata: %s\n", error->message);
        // Handle each parsing failure (if any)
        if (failures != NULL) {
            for (guint i = 0; i < failures->len; i++) {
                ModulemdSubdocumentInfo *failure = (ModulemdSubdocumentInfo *)g_ptr_array_index(failures, i);
                fprintf(stderr, "Subdocument failed to parse: %s\n", modulemd_subdocument_info_get_gerror(failure)->message);
            }
        }
        return EXIT_FAILURE;
    }

    // Get the default streams as a hash table
    default_streams = modulemd_module_index_get_default_streams_as_hash_table(index, NULL);
    if (!default_streams) {
        fprintf(stderr, "Could not retrieve default streams hash table.\n");
        return EXIT_FAILURE;
    }

    // Iterate over the hash table and print the module names and default streams
    g_hash_table_iter_init(&iter, default_streams);
    while (g_hash_table_iter_next(&iter, &key, &value)) {
        module_name = (const gchar *)key;
        default_stream = (const gchar *)value;
        printf("Module name: %s, Default stream: %s\n", module_name, default_stream);
    }

    // Clean up
    g_hash_table_unref(default_streams);

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(gen_yaml_f)
        self.cmd(code)
        self.cmd("gcc -o libmodulemd_modulemd_defaults_get_module_name libmodulemd_modulemd_defaults_get_module_name.c `pkg-config --cflags --libs modulemd-2.0 glib-2.0`")

    def test(self):
        self.cmd("./libmodulemd_modulemd_defaults_get_module_name gen_yaml_to_get_module_name.yaml")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['libmodulemd_modulemd_defaults_get_module_name.c', 'libmodulemd_modulemd_defaults_get_module_name', 'gen_yaml_to_get_module_name.yaml']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")
