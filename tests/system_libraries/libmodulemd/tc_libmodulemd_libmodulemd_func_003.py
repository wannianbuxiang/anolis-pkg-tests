#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libmodulemd_libmodulemd_func_003.py
@Time:      2024/03/05 09:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libmodulemd_libmodulemd_func_003.yaml for details
    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libmodulemd libmodulemd-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > libmodulemd_modulemd_build_config_compare.c << EOF
#include <glib.h>
#include <modulemd-2.0/modulemd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    g_autoptr(ModulemdBuildConfig) build_config_1 = NULL;
    g_autoptr(ModulemdBuildConfig) build_config_2 = NULL;
    gint comparison_result;

    // Ensure the correct number of command-line arguments
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <config1_name> <config2_name>\n", argv[0]);
        return EXIT_FAILURE;
    }

    // Create two ModulemdBuildConfig objects and set their names
    build_config_1 = modulemd_build_config_new();
    modulemd_build_config_set_context(build_config_1, argv[1]);

    build_config_2 = modulemd_build_config_new();
    modulemd_build_config_set_context(build_config_2, argv[2]);

    // Compare the two ModulemdBuildConfig objects
    comparison_result = modulemd_build_config_compare(build_config_1, build_config_2);

    // Print the comparison result
    if (comparison_result < 0) {
        printf("BuildConfig '%s' is less than BuildConfig '%s'\n", argv[1], argv[2]);
    } else if (comparison_result > 0) {
        printf("BuildConfig '%s' is greater than BuildConfig '%s'\n", argv[1], argv[2]);
    } else {
        printf("BuildConfig '%s' is equal to BuildConfig '%s'\n", argv[1], argv[2]);
    }

    // Cleanup is automatic due to the use of g_autoptr
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o libmodulemd_modulemd_build_config_compare libmodulemd_modulemd_build_config_compare.c `pkg-config --cflags --libs modulemd-2.0 glib-2.0`")

    def test(self):
        self.cmd("./libmodulemd_modulemd_build_config_compare config1 config2")
        self.cmd("./libmodulemd_modulemd_build_config_compare test test")
        self.cmd("./libmodulemd_modulemd_build_config_compare test test123")
        self.cmd("./libmodulemd_modulemd_build_config_compare 123 123")
        self.cmd("./libmodulemd_modulemd_build_config_compare 123 1234")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['libmodulemd_modulemd_build_config_compare.c', 'libmodulemd_modulemd_build_config_compare']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")
