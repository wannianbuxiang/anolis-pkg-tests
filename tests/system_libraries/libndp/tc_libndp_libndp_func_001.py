#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libndp_libndp_func_001.py
@Time:      2024/04/15 14:18:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libndp_libndp_func_001.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libndp libndp-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_libndp_ndp_get_log_priority.c << EOF
#include <ndp.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    struct ndp *ndp_instance;
    int log_priority;
    
    // 检查命令行参数
    if (argc != 2) {
        fprintf(stderr, "用法: %s <日志优先级数字>\n", argv[0]);
        return EXIT_FAILURE;
    }
    
    // 打开NDP库
    if (ndp_open(&ndp_instance) < 0) {
        fprintf(stderr, "无法打开NDP库\n");
        return EXIT_FAILURE;
    }
    // 接检索日志优先级
    log_priority = ndp_get_log_priority(ndp_instance);
    
    printf("当前的NDP日志优先级是: %d\n", log_priority);
    ndp_close(ndp_instance);   
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_libndp_ndp_get_log_priority test_libndp_ndp_get_log_priority.c -lndp")

    def test(self):
        self.cmd("./test_libndp_ndp_get_log_priority 1")


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_libndp_ndp_get_log_priority', 'test_libndp_ndp_get_log_priority.c']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")