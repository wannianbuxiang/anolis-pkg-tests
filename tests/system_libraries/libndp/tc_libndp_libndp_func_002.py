#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libndp_libndp_func_002.py
@Time:      2024/04/15 14:18:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libndp_libndp_func_002.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libndp libndp-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_libndp_ndp_msg_opt_prefix_valid_time.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <ndp.h>

int main(int argc, char *argv[]) {
    struct ndp_msg *msg = NULL;
    uint32_t valid_time;
    int offset;

    if (argc != 3) {
        fprintf(stderr, "Usage: %s <NDP message type> <offset>\n", argv);
        return EXIT_FAILURE;
    }

    enum ndp_msg_type msg_type = atoi(argv);
    offset = atoi(argv);

    printf("Input: NDP message type = %d, offset = %d\n", msg_type, offset);

    if (ndp_msg_new(&msg, msg_type) != 0) {
        perror("Failed to create ndp_msg");
        fprintf(stderr, "Failed to create ndp_msg for type %d.\n", msg_type);
        return EXIT_FAILURE;
    }

    printf("Created NDP message of type %d.\n", msg_type);

    valid_time = ndp_msg_opt_prefix_valid_time(msg, offset);

    if (valid_time == 0) {
        fprintf(stderr, "No valid prefix time found at offset %d.\n", offset);
    } else {
        printf("Prefix valid time at offset %d is %u seconds.\n", offset, valid_time);
    }

    ndp_msg_destroy(msg);
    printf("Destroyed NDP message.\n");

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_libndp_ndp_msg_opt_prefix_valid_time test_libndp_ndp_msg_opt_prefix_valid_time.c -lndp")

    def test(self):
        # 134：这是NDP_MSG_RA（路由器广告）消息的示例值，实际值取决于您处理的特定消息类型。
        # 16：这是示例偏移量，实际值应该基于您的NDP消息中前缀信息选项的位置。
        self.cmd("./test_libndp_ndp_msg_opt_prefix_valid_time 134 16")


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_libndp_ndp_msg_opt_prefix_valid_time', 'test_libndp_ndp_msg_opt_prefix_valid_time.c']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")