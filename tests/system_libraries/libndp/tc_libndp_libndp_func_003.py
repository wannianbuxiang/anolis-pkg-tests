#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libndp_libndp_func_003.py
@Time:      2024/04/15 14:18:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libndp_libndp_func_003.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libndp libndp-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_libndp_ndp_msg_send.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <ndp.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/if.h> // For if_nametoindex()

int main(int argc, char *argv[]) {
    struct ndp *ndp;
    struct ndp_msg *msg;
    int ret;

    // 命令行参数检查
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <interface_name> <IPv6_target_address>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    char *interface_name = argv[1];
    char *ipv6_target_address_str = argv[2];
    struct in6_addr ipv6_target_address;

    // 将字符串格式的IPv6地址转换为in6_addr结构
    if (inet_pton(AF_INET6, ipv6_target_address_str, &ipv6_target_address) != 1) {
        fprintf(stderr, "Error converting IPv6 address\n");
        return EXIT_FAILURE;
    }

    // 打开libndp上下文
    ret = ndp_open(&ndp);
    if (ret != 0) {
        fprintf(stderr, "Failed to open libndp handle\n");
        return EXIT_FAILURE;
    }

    // 创建邻居请求消息
    if (ndp_msg_new(&msg, NDP_MSG_NS) != 0) {
        fprintf(stderr, "Failed to create an NDP message\n");
        ndp_close(ndp);
        return EXIT_FAILURE;
    }

    // 设置目标IPv6地址
    ndp_msg_target_set(msg, &ipv6_target_address);

    // 根据接口名称获取接口索引，该索引可用于某些网络操作
    int if_index = if_nametoindex(interface_name);
    if (if_index == 0) {
        fprintf(stderr, "Failed to get the interface index for %s\n", interface_name);
        ndp_msg_destroy(msg);
        ndp_close(ndp);
        return EXIT_FAILURE;
    }

    // 发送NDP消息
    ret = ndp_msg_send(ndp, msg);
    if (ret < 0) {
        fprintf(stderr, "Failed to send NDP message\n");
    } else {
        printf("NDP message sent successfully.\n");
    }

    // 清理
    ndp_msg_destroy(msg);
    ndp_close(ndp);

    return (ret >= 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_libndp_ndp_msg_send test_libndp_ndp_msg_send.c -lndp")

    def test(self):
        self.cmd("./test_libndp_ndp_msg_send lo ::1")


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_libndp_ndp_msg_send', 'test_libndp_ndp_msg_send.c']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")