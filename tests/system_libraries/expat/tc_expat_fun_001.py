#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_expat_fun_001.py
@Time:      2024/04/02 11:12:50
@Author:    hongfeng
@Version:   1.0
@Contact:   wb-zhf668260@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    hongfeng
"""

import os
from common.basetest import LocalTest
class Test(LocalTest):
    """
    See tc_expat_fun_001.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "expat-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > expat.c <<EOF
#include <expat.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

void XMLCALL startElement(void *userData, const char *name, const char **atts) {
    // 处理逻辑
    printf("startElement name: %s\\n", name);
}

void XMLCALL endElement(void *userData, const char *name) {
    // 处理逻辑
    printf("endElement name: %s\\n", name);
}

void XMLCALL characterDataHandler(void *userData, const char *s, int len) {
    if(len == 0){
        return;
    }

    // 创建一个新的缓冲区用于存储处理过的字符数据
    char* processed_data = malloc(len + 1);
    int new_len = 0;

    for (int i = 0; i < len; ++i) {
        if (s[i] != '\\r' && s[i] != '\\n' && !isspace((unsigned char)s[i])) {  // 忽略回车、换行符以及所有空格字符
            processed_data[new_len++] = s[i];
        }
    }

    // 添加字符串结束符
    processed_data[new_len] = '\\0';
    if(new_len != 0) {
        printf("characterDataHandler: %s\\n", processed_data);
    }

    // 记得在适当的时候释放 processed_data 的内存
    free(processed_data);
}

int main() {
    char buf[1024];
    int done;
    XML_Parser parser = XML_ParserCreate(NULL);
    if (!parser) {
        fprintf(stderr, "Unable to create parser\\n");
        return 1;
    }

    XML_SetUserData(parser, NULL); // 设置用户数据指针
    XML_SetElementHandler(parser, startElement, endElement);
    XML_SetCharacterDataHandler(parser, characterDataHandler);

    FILE *fp = fopen("test.xml", "r");
    do {
        int len = (int)fread(buf, 1, sizeof(buf), fp);
        done = len < sizeof(buf);
        if (XML_Parse(parser, buf, len, done) == XML_STATUS_ERROR) {
            fprintf(stderr, "Error: %s at line %d\\n",
                XML_ErrorString(XML_GetErrorCode(parser)),
                (int)XML_GetCurrentLineNumber(parser));
            return 1;
        }
    } while (!done);

    XML_ParserFree(parser);
    return 0;
}
EOF"""
        self.cmd(cmdline)
        self.cmd("gcc -o expat expat.c `pkg-config --cflags --libs expat`")
        file_dir = os.path.dirname(__file__)
        self.cmd(f"cp {file_dir}/test.xml ./test.xml")

    def test(self):
        ret_c, ret_o = self.cmd("./expat")
        
        # 期望解析的结果
        expected_sequence = [
            ("startElement name", "note"),
            ("startElement name", "from"),
            ("characterDataHandler", "A"),
            ("endElement name", "from"),
            ("startElement name", "to"),
            ("characterDataHandler", "B"),
            ("endElement name", "to"),
            ("startElement name", "heading"),
            ("characterDataHandler", "Test"),
            ("endElement name", "heading"),
            ("startElement name", "body"),
            ("characterDataHandler", "Helloworld!"),
            ("endElement name", "body"),
            ("endElement name", "note")
        ]
        
        # 将输出字符串分割为行
        output_lines = ret_o.split("\n")
        self.assertEqual(len(expected_sequence), len(output_lines), "%s should equal to %s" % (len(expected_sequence), len(output_lines)))

        # 遍历每一行, 检查输出是按照解析顺序打印的
        current_position = 0
        for line in output_lines:
            # 去除末尾空格并按冒号拆分以获取事件类型和数据
            event_info = line.strip().split(":")
            event_type, data = event_info[0].strip(), event_info[1].strip() if len(event_info) > 1 else None
            expected_event_type, expected_data = expected_sequence[current_position]

            self.assertEqual(expected_event_type, event_type, "%s should equal to %s" % (expected_event_type, event_type))
            self.assertEqual(expected_data, data, "%s should equal to %s" % (expected_data, data))
            current_position += 1
            
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm expat.c expat test.xml")
