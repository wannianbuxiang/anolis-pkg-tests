#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libseccomp_fun_003.py
@Time:      2024/04/29 10:12:50
@Author:    hongfeng
@Version:   1.0
@Contact:   wb-zhf668260@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    hongfeng
"""

from common.basetest import LocalTest
class Test(LocalTest):
    """
    See tc_libseccomp_fun_003.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libseccomp-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > test03.c <<EOF
#include <stdio.h>
#include <seccomp.h>

int main() {
    int rc;

    // 创建一个新的过滤上下文
    scmp_filter_ctx ctx = seccomp_init(SCMP_ACT_KILL);
    if (ctx == NULL) {
        fprintf(stderr, "seccomp_init failed\\n");
        return 1;
    }

    // 允许read(), write(), exit(), newfstatat系统调用
    rc = seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 0);
    if (rc != 0) {
        fprintf(stderr, "seccomp_rule_add for read() failed: %d\\n", rc);
        seccomp_release(ctx);
        return 1;
    }

    rc = seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(write), 0);
    if (rc != 0) {
        fprintf(stderr, "seccomp_rule_add for write() failed: %d\\n", rc);
        seccomp_release(ctx);
        return 1;
    }

    rc = seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(exit), 0);
    if (rc != 0) {
        fprintf(stderr, "seccomp_rule_add for exit() failed: %d\\n", rc);
        seccomp_release(ctx);
        return 1;
    }
    rc = seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(newfstatat), 0);
    if (rc < 0) {
        fprintf(stderr, "seccomp_rule_add for newfstatat() failed: %d\\n", rc);
        seccomp_release(ctx);
        return 1;
    }
    rc = seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(exit_group), 0);
    if (rc != 0) {
        fprintf(stderr, "seccomp_rule_add for exit_group() failed: %d\\n", rc);
        seccomp_release(ctx);
        return 1;
    }

    // clone系统调用
    rc = seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(clone), 0);
    if (rc != 0) {
        fprintf(stderr, "seccomp_rule_add for exit() failed: %d\\n", rc);
        seccomp_release(ctx);
        return 1;
    }

    // 设置默认动作为禁止（KILL进程）
    rc = seccomp_load(ctx);
    if (rc != 0) {
        fprintf(stderr, "seccomp_load failed: %d\\n", rc);
        seccomp_release(ctx);
        return 1;
    }

    printf("Seccomp filter successfully installed.\\n");

    // 在这里您可以添加您的程序逻辑，现在它将在受限的系统调用环境中运行
    // 尝试一个未被允许的系统调用，比如 fork()
    fork(); // 注释这行以避免程序因违反seccomp规则而崩溃

    printf("fork is allowed.\\n");
    
    // 在程序结束时释放过滤上下文
    seccomp_release(ctx);

    return 0;
}
EOF"""
        self.cmd(cmdline)
        self.cmd("gcc -o test03 test03.c `pkg-config --cflags --libs libseccomp`")

    def test(self):
        ret_c, ret_o = self.cmd("./test03")
        self.assertTrue("Seccomp filter successfully installed" in ret_o, 'check output error.')
        self.assertTrue("fork is allowed" in ret_o, 'check output error.')
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm test03.c test03")

