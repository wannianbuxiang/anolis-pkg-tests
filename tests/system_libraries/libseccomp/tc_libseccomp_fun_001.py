#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libseccomp_fun_001.py
@Time:      2024/04/29 10:12:50
@Author:    hongfeng
@Version:   1.0
@Contact:   wb-zhf668260@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    hongfeng
"""

from common.basetest import LocalTest
class Test(LocalTest):
    """
    See tc_libseccomp_fun_001.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libseccomp-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > test01.c <<EOF
#include <stdio.h>
#include <seccomp.h>

int main() {
    int rc;

    // 创建一个新的过滤上下文
    scmp_filter_ctx ctx = seccomp_init(SCMP_ACT_ALLOW);
    if (ctx == NULL) {
        fprintf(stderr, "seccomp_init failed\\n");
        return 1;
    }

    rc = seccomp_load(ctx);
    if (rc != 0) {
        fprintf(stderr, "seccomp_load failed: %d\\n", rc);
        seccomp_release(ctx);
        return 1;
    }

    printf("Seccomp filter successfully installed.\\n");

    // 尝试一个未seccomp_rule_add的系统调用，比如 fork()，验证可以运行
    fork(); 

    printf("fork is allowed.\\n");
    
    // 在程序结束时释放过滤上下文
    seccomp_release(ctx);

    return 0;
}
EOF"""
        self.cmd(cmdline)
        self.cmd("gcc -o test01 test01.c `pkg-config --cflags --libs libseccomp`")

    def test(self):
        ret_c, ret_o = self.cmd("./test01")
        self.assertTrue("fork is allowed." in ret_o, 'check output error.')
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm test01.c test01")
