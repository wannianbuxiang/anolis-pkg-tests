#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libverto_libverto_func_001.py
@Time:      2024/02/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libverto_libverto_func_001.yaml for details
    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libverto libverto-devel libevent libevent-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_verto_check_event_is_supported.c << EOF
#include <verto.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <backend>\n", argv[0]);
        return EXIT_FAILURE;
    }

    const char *backend = argv[1];
    verto_ctx *ctx = verto_default(backend, VERTO_EV_TYPE_NONE); // 使用 VERTO_EV_TYPE_NONE，因为我们只是在检查后端

    if (ctx != NULL) {
        printf("%s backend is supported.\n", backend);
        verto_free(ctx);
    } else {
        printf("%s backend is not supported or could not be initialized.\n", backend);
    }
    return 0;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_verto_check_event_is_supported test_verto_check_event_is_supported.c `pkg-config --cflags --libs libverto libevent`")

    def test(self):
        self.cmd("./test_verto_check_event_is_supported libevent")
        self.cmd("./test_verto_check_event_is_supported glib")
        self.cmd("./test_verto_check_event_is_supported libev")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['test_verto_check_event_is_supported.c', 'test_verto_check_event_is_supported']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")
