# -*- encoding: utf-8 -*-

"""
@File:      tc_npth_fun_006.py
@Time:      2024-03-11 10:09:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_npth_fun_006.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "npth npth-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline='''cat > npth_fun_006.c <<EOF
#include <npth.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#define FILENAME "npth_testfile.txt"
// 写任务
void* write_task(void* arg) {
    char* text = (char*)arg;
    int fd = open(FILENAME, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
    if (fd == -1) {
        perror("Open file for write");
        return NULL;
    }
    write(fd, text, strlen(text));
    printf("Write task: Data written to file.\\n");
    close(fd);
    return NULL;
}
// 读任务
void* read_task(void* arg) {
    int fd;
    char buffer[1024];
    ssize_t bytes_read;
    sleep(1); // 确保写任务先运行
    fd = open(FILENAME, O_RDONLY);
    if (fd == -1) {
        perror("Open file for read");
        return NULL;
    }
    bytes_read = read(fd, buffer, sizeof(buffer));
    if (bytes_read == -1) {
        perror("Read failed");
    } else {
        buffer[bytes_read] = '\\0';
        printf("Read task: Data read from file: %s\\n", buffer);
    }
    close(fd);
    return NULL;
}
int main(void) {
    npth_t tid1, tid2;
    const char* text = "Hello, npth async I/O!";
    // 初始化 npth
    npth_init();
    // 创建写任务线程
    if (npth_create(&tid1, NULL, write_task, (void*)text)) {
        fprintf(stderr, "Failed to create write task thread\\n");
        return EXIT_FAILURE;
    }
    // 创建读任务线程
    if (npth_create(&tid2, NULL, read_task, NULL)) {
        fprintf(stderr, "Failed to create read task thread\\n");
        return EXIT_FAILURE;
    }
    // 等待线程结束
    npth_join(tid1, NULL);
    npth_join(tid2, NULL);
    remove(FILENAME);
    return 0;
}
EOF
'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o npth_fun_006 npth_fun_006.c -lnpth")
        code, result = self.cmd("file npth_fun_006")
        self.assertIn("executable", result)
        code, result = self.cmd("./npth_fun_006")
        self.assertIn("Write task: Data written to file", result)
        self.assertIn("Read task: Data read from file", result)
 

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf npth_fun_006 npth_fun_006.c")