# -*- encoding: utf-8 -*-

"""
@File:      tc_npth_fun_003.py
@Time:      2024-03-11 10:09:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_npth_fun_003.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "npth npth-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline='''cat > npth_fun_002.c <<EOF
#include <npth.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// 第一个任务函数
void task1() {
    printf("线程任务1,任务开始。\\n");
    sleep(1); // 模拟任务运行，这里简单休眠1秒
    printf("线程任务1,任务结束。\\n");
}

// 第二个任务函数
void task2() {
    printf("线程任务2,任务开始。\\n");
    sleep(1); // 模拟任务运行，这里简单休眠1秒
    printf("线程任务2,任务结束。\\n");
}

// 线程执行函数
void *thread_function(void *arg) {
    printf("单线程, 开始执行任务...\\n");
    task1(); // 执行任务1
    task2(); // 执行任务2
    printf("单线程,所有任务执行完毕！\\n");
    return NULL;
}

int main(void) {
    // 初始化 npth
    npth_init();

    // 创建线程
    npth_t thread;
    int rc = npth_create(&thread, NULL, thread_function, NULL);

    if (rc) {
        fprintf(stderr, "无法创建线程,npth_create 返回码：%d\\n", rc);
        exit(EXIT_FAILURE);
    }

    // 等待线程结束
    rc = npth_join(thread, NULL);
    if (rc) {
        fprintf(stderr, "无法加入线程,npth_join 返回码：%d\\n", rc);
        exit(EXIT_FAILURE);
    }

    return 0;
}
EOF
'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o npth_fun_002 npth_fun_002.c -lnpth")
        code, result = self.cmd("file npth_fun_002")
        self.assertIn("executable", result)
        code, result = self.cmd("./npth_fun_002")
        self.assertIn("单线程, 开始执行任务", result)
        self.assertIn("线程任务1,任务开始", result)
        self.assertIn("线程任务1,任务结束", result)
        self.assertIn("线程任务2,任务开始", result)
        self.assertIn("线程任务2,任务结束", result)
        self.assertIn("单线程,所有任务执行完毕！", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf npth_fun_002 npth_fun_002.c")