# -*- encoding: utf-8 -*-

"""
@File:      tc_npth_fun_002.py
@Time:      2024-03-11 10:09:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_npth_fun_002.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "npth npth-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline='''cat > npth_fun_002.c <<EOF
#include <npth.h>
#include <stdio.h>
#include <stdlib.h>

#define THREAD_COUNT 2

// 线程任务函数1
void* thread_task1(void* arg) {
    long thread_id = (long)arg;
    printf("线程 #%ld,执行任务1。\\n", thread_id);
    return NULL;
}

// 线程任务函数2
void* thread_task2(void* arg) {
    long thread_id = (long)arg;
    printf("线程 #%ld,执行任务2。\\n", thread_id);
    return NULL;
}

// ...

int main() {
    npth_t threads[THREAD_COUNT];
    int rc;
    long t;

    npth_init(); // 初始化 npth

    // 创建多个线程执行不同的任务
    for(t = 0; t < THREAD_COUNT; t++) {
        if (t % 2 == 0) { // 根据索引决定执行哪个任务
            printf("主程序,创建线程 #%ld 以执行任务1。\\n", t);
            rc = npth_create(&threads[t], NULL, thread_task1, (void*)t);
        } else {
            printf("主程序,创建线程 #%ld 以执行任务2。\\n", t);
            rc = npth_create(&threads[t], NULL, thread_task2, (void*)t);
        }
        if (rc) {
            fprintf(stderr, "ERROR; npth_create() 返回 %d\\n", rc);
            exit(EXIT_FAILURE); // 如果线程创建失败，则退出
        }
    }

    // 等待所有线程结束
    for(t = 0; t < THREAD_COUNT; t++) {
        npth_join(threads[t], NULL);
    }

    printf("主程序,所有线程已经完成。\\n");

    return 0;
}
EOF
'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o npth_fun_002 npth_fun_002.c -lnpth")
        code, result = self.cmd("file npth_fun_002")
        self.assertIn("executable", result)
        code, result = self.cmd("./npth_fun_002")
        self.assertIn("主程序,创建线程 #0 以执行任务1", result)
        self.assertIn("主程序,创建线程 #1 以执行任务2", result)
        self.assertIn("线程 #0,执行任务1", result)
        self.assertIn("线程 #1,执行任务2", result)
        self.assertIn("主程序,所有线程已经完成。", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf npth_fun_002 npth_fun_002.c")