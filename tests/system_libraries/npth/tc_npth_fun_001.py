# -*- encoding: utf-8 -*-

"""
@File:      tc_npth_fun_001.py
@Time:      2024-03-11 10:09:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_npth_fun_001.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "npth npth-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline='''cat > npth_fun_001.c <<EOF
#include <npth.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// 任务函数
void *task_function(void *arg) {
    sleep(1); // 模拟任务执行，这里只是简单地休眠1秒
    printf("单线程任务：任务执行完毕！\\n");
    return NULL;
}

int main(void) {
    // 初始化 npth
    npth_init();

    // 创建线程
    npth_t thread;
    int rc = npth_create(&thread, NULL, task_function, NULL);

    if (rc) {
        fprintf(stderr, "无法创建线程，npth_create 返回码：%d\\n", rc);
        exit(EXIT_FAILURE);
    }

    // 等待线程结束
    rc = npth_join(thread, NULL);
    if (rc) {
        fprintf(stderr, "无法加入线程，npth_join 返回码：%d\\n", rc);
        exit(EXIT_FAILURE);
    }

    return 0;
}
EOF
'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o npth_fun_001 npth_fun_001.c -lnpth")
        code, result = self.cmd("file npth_fun_001")
        self.assertIn("executable", result)
        code, result = self.cmd("./npth_fun_001")
        self.assertIn("单线程任务：任务执行完毕！", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf npth_fun_001 npth_fun_001.c")