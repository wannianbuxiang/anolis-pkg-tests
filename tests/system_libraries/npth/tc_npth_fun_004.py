# -*- encoding: utf-8 -*-

"""
@File:      tc_npth_fun_004.py
@Time:      2024-03-11 10:09:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_npth_fun_004.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "npth npth-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline='''cat > npth_fun_004.c <<EOF
#include <npth.h>
#include <stdio.h>
#include <stdlib.h>

#define THREAD_COUNT 10
#define INCREMENT_TIMES 1000

// 共享资源
int counter = 0;
npth_mutex_t counter_mutex;

// 线程任务函数
void *increment_counter(void *arg) {
    for (int i = 0; i < INCREMENT_TIMES; ++i) {
        npth_mutex_lock(&counter_mutex); // 获取互斥锁
        counter++;  // 修改共享资源
        npth_mutex_unlock(&counter_mutex); // 释放互斥锁
    }
    return NULL;
}

int main() {
    npth_t threads[THREAD_COUNT];
    int rc;

    // 初始化互斥锁
    rc = npth_mutex_init(&counter_mutex, NULL);
    if (rc != 0) {
        perror("npth_mutex_init failed");
        exit(EXIT_FAILURE);
    }

    npth_init(); // 初始化 npth

    // 创建线程
    for (int t = 0; t < THREAD_COUNT; ++t) {
        rc = npth_create(&threads[t], NULL, increment_counter, NULL);
        if (rc != 0) {
            perror("npth_create failed");
            exit(EXIT_FAILURE);
        }
    }

    // 等待所有线程结束
    for (int t = 0; t < THREAD_COUNT; ++t) {
        rc = npth_join(threads[t], NULL);
        if (rc != 0) {
            perror("npth_join failed");
            exit(EXIT_FAILURE);
        }
    }

    // 清理互斥锁
    npth_mutex_destroy(&counter_mutex);

    // 输出结果
    printf("预期counter值应该为%d，实际为：%d\\n",
           THREAD_COUNT * INCREMENT_TIMES, counter);

    return 0;
}
EOF
'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o npth_fun_004 npth_fun_004.c -lnpth")
        code, result = self.cmd("file npth_fun_004")
        self.assertIn("executable", result)
        code, result = self.cmd("./npth_fun_004")
        self.assertIn("预期counter值应该为10000，实际为：10000", result)
 

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf npth_fun_004 npth_fun_004.c")