# -*- encoding: utf-8 -*-

"""
@File:      tc_npth_fun_005.py
@Time:      2024-03-11 10:09:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_npth_fun_005.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "npth npth-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline='''cat > npth_fun_005.c <<EOF
#include <npth.h>
#include <stdio.h>
#include <stdlib.h>

// 共享资源和同步原语
npth_mutex_t lock;
npth_cond_t cond_var;
int shared_data = 0;
int condition_met = 0;

// 线程函数1
void* task1(void* arg) {
    // 设置某项数据（类似于生产数据）
    npth_mutex_lock(&lock);
    shared_data = 42; // 设置数据
    condition_met = 1; // 设置条件标志
    npth_cond_signal(&cond_var); // 通知等待的线程
    npth_mutex_unlock(&lock);
    return NULL;
}

// 线程函数2
void* task2(void* arg) {
    npth_mutex_lock(&lock);
    // 等待条件标志
    while (condition_met == 0) {
        npth_cond_wait(&cond_var, &lock);
    }
    printf("线程收到数据：%d\\n", shared_data); // 类似于消费数据
    npth_mutex_unlock(&lock);
    return NULL;
}

int main() {
    npth_t thread1, thread2;

    // 初始化 npth, 锁和条件变量
    npth_init();
    npth_mutex_init(&lock, NULL);
    npth_cond_init(&cond_var, NULL);

    // 创建两个线程
    npth_create(&thread1, NULL, task1, NULL);
    npth_create(&thread2, NULL, task2, NULL);

    // 等待线程结束
    npth_join(thread1, NULL);
    npth_join(thread2, NULL);

    // 清理
    npth_cond_destroy(&cond_var);
    npth_mutex_destroy(&lock);

    return 0;
}
EOF
'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o npth_fun_005 npth_fun_005.c -lnpth")
        code, result = self.cmd("file npth_fun_005")
        self.assertIn("executable", result)
        code, result = self.cmd("./npth_fun_005")
        self.assertIn("线程收到数据：42", result)
 

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf npth_fun_005 npth_fun_005.c")