#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libgcrypt_fun_001.py
@Time:      2024/04/7 10:12:50
@Author:    hongfeng
@Version:   1.0
@Contact:   wb-zhf668260@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    hongfeng
"""

from common.basetest import LocalTest
class Test(LocalTest):
    """
    See tc_libgcrypt_fun_001.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libgcrypt-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > aes.c <<EOF
#include <stdio.h>
#include <string.h>
#include <gcrypt.h>

#define CIPHER_ALGO GCRY_CIPHER_AES256 // 使用AES-256算法

void aes_encrypt(const char *plaintext, const char *key, const char *iv, char **ciphertext) {
    gcry_error_t err;
    gcry_cipher_hd_t hd;

    // 初始化加密上下文
    err = gcry_cipher_open(&hd, CIPHER_ALGO, GCRY_CIPHER_MODE_CBC, 0);
    if (err != GPG_ERR_NO_ERROR) {
        fprintf(stderr, "Error opening cipher: %s\\n", gcry_strerror(err));
        exit(1);
    }

    // 设置密钥和初始化向量
    err = gcry_cipher_setkey(hd, key, 32);
    if (err != GPG_ERR_NO_ERROR) {
        fprintf(stderr, "Error setting key: %s\\n", gcry_strerror(err));
        gcry_cipher_close(hd);
        exit(1);
    }
    err = gcry_cipher_setiv(hd, iv, 16);
    if (err != GPG_ERR_NO_ERROR) {
        fprintf(stderr, "Error setting IV: %s\\n", gcry_strerror(err));
        gcry_cipher_close(hd);
        exit(1);
    }

    // 确保数据长度是块大小的整数倍，并为最后一块添加必要的填充
    size_t plaintext_len = strlen(plaintext);
    size_t block_size = gcry_cipher_get_algo_blklen(CIPHER_ALGO);
    size_t last_block_size = strlen(plaintext) % block_size;
    if (last_block_size > 0) {
        // 需要填充到下一个完整块
        plaintext_len += block_size - last_block_size;
    }
    *ciphertext = malloc(plaintext_len + 1);

    /* 执行加密 */
    err = gcry_cipher_encrypt(hd, *ciphertext, plaintext_len, plaintext, plaintext_len);
    if (err != GPG_ERR_NO_ERROR) {
        fprintf(stderr, "Error encrypting  %s\\n", gcry_strerror(err));
        free(*ciphertext);
        gcry_cipher_close(hd);
        exit(1);
    }

    // 添加NULL终止符
    (*ciphertext)[plaintext_len] = '\\0';
    gcry_cipher_close(hd);
}

void aes_decrypt(const char *ciphertext, const char *key, const char *iv, char **plaintext) {
    gcry_error_t err;
    gcry_cipher_hd_t hd;

    // 初始化加密上下文
    err = gcry_cipher_open(&hd, CIPHER_ALGO, GCRY_CIPHER_MODE_CBC, 0);
    if (err != GPG_ERR_NO_ERROR) {
        fprintf(stderr, "Error opening cipher for decryption: %s\\n", gcry_strerror(err));
        exit(1);
    }

    // 设置密钥和初始化向量
    err = gcry_cipher_setkey(hd, key, 32);
    if (err != GPG_ERR_NO_ERROR) {
        fprintf(stderr, "Error setting decryption key: %s\\n", gcry_strerror(err));
        gcry_cipher_close(hd);
        exit(1);
    }
    err = gcry_cipher_setiv(hd, iv, 16);
    if (err != GPG_ERR_NO_ERROR) {
        fprintf(stderr, "Error setting decryption IV: %s\\n", gcry_strerror(err));
        gcry_cipher_close(hd);
        exit(1);

    }

    // 解密操作需要与加密相同的缓冲区长度
    size_t plaintext_len = strlen(ciphertext);
    // 额外空间存放NULL终止符
    *plaintext = malloc(plaintext_len + 1);

    // 执行解密
    err = gcry_cipher_decrypt(hd, *plaintext, plaintext_len, ciphertext, plaintext_len);
    if (err != GPG_ERR_NO_ERROR) {
        fprintf(stderr, "Error decrypting  %s\\n", gcry_strerror(err));
        free(*plaintext);
        gcry_cipher_close(hd);
        exit(1);
    }

    // 添加NULL终止符
    (*plaintext)[plaintext_len] = '\\0';
    gcry_cipher_close(hd);
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s \\n", argv[0]);
        return 1;
    }

    char key[32] = {
        0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6,
        0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c,
        0xed, 0xa3, 0xeb, 0xf4, 0xa1, 0x7b, 0x4a, 0x31,
        0xc9, 0x47, 0xae, 0x53, 0x7b, 0x39, 0x31, 0x0
    };
    char iv[16] = {
        0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6,
        0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x0
    };

    char *encrypted_text = NULL;
    char *decrypted_text = NULL;

    size_t arg_length = strlen(argv[1]) + 1;
    char plaintext[arg_length];
    strcpy(plaintext, argv[1]);

    aes_encrypt(plaintext, key, iv, &encrypted_text);
    printf("Encrypted Text: %02X\\n", encrypted_text);

    aes_decrypt(encrypted_text, key, iv, &decrypted_text);
    printf("Decrypted Text: %s\\n", decrypted_text);

    free(encrypted_text);
    free(decrypted_text);

    return 0;
}
EOF"""
        self.cmd(cmdline)
        self.cmd("gcc -o aes aes.c `libgcrypt-config --cflags --libs`")

    def test(self):
        ret_c,ret_o = self.cmd("./aes 'This is a test message for aes256!'")
        self.assertTrue("Decrypted Text: This is a test message for aes256!" in ret_o, 'check output error.')
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm aes.c aes")
