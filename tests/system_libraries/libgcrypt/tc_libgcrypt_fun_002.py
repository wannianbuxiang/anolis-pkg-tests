#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libgcrypt_fun_002.py
@Time:      2024/04/08 11:12:50
@Author:    hongfeng
@Version:   1.0
@Contact:   wb-zhf668260@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    hongfeng
"""

from common.basetest import LocalTest
class Test(LocalTest):
    """
    See tc_libgcrypt_fun_002.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libgcrypt-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > hash_sha256.c <<EOF
#include <stdio.h>
#include <string.h>
#include <gcrypt.h>

#define BUFFER_SIZE 1024

void hexlify(char* dest, const void* src, size_t src_len) {
    static const char hexdigits[] = "0123456789abcdef";
    for (size_t i = 0; i < src_len; ++i) {
        unsigned char byte = ((unsigned char*)src)[i];
        dest[i * 2 + 1] = hexdigits[byte & 0x0F];
    }
    // 添加终止符
    dest[src_len * 2] = '\\0';
}

void test_hash_function(const char *message, const char *algo_name) {
    gcry_error_t err;
    gcry_md_hd_t hd;
    unsigned char hash[gcry_md_get_algo_dlen(GCRY_MD_SHA256)];
    char computed_hash_hex[BUFFER_SIZE];

    // 初始化 libgcrypt
    if (!gcry_check_version(NULL)) {
        fprintf(stderr, "Error: libgcrypt not initialized properly\\n");
        return;
    }

    // 设置哈希算法
    err = gcry_md_open(&hd, algo_name, 0);
    if (err != GPG_ERR_NO_ERROR) {
        fprintf(stderr, "Error setting up %s: %s\\n", algo_name, gcry_strerror(err));
        return;
    }

    // 计算哈希值
    gcry_md_write(hd, message, strlen(message));

    // 获取哈希结果
    gcry_md_final(hd);
    // 对于SHA-256，使用这个长度
    memcpy(hash, gcry_md_read(hd, 0), gcry_md_get_algo_dlen(GCRY_MD_SHA256)); 

    // 打印哈希结果
    char hex_digest[2 * gcry_md_get_algo_dlen(GCRY_MD_SHA256) + 1];
    for (size_t i = 0; i < gcry_md_get_algo_dlen(GCRY_MD_SHA256); ++i) {
        snprintf(hex_digest + 2*i, 3, "%02x", hash[i]);
    }
    // 添加终止符
    hex_digest[2 * gcry_md_get_algo_dlen(GCRY_MD_SHA256)] = '\\0'; 
    printf("%s\\n", hex_digest);

    // 清理资源
    gcry_md_close(hd);
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s \\n", argv[0]);
        return 1;
    }

    size_t arg_length = strlen(argv[1]) + 1;
    char test_string[arg_length];
    strcpy(test_string, argv[1]);
    
    // 测试SHA-256
    test_hash_function(test_string, GCRY_MD_SHA256); 

    return 0;
}
EOF"""
        self.cmd(cmdline)
        self.cmd("gcc -o hash_sha256 hash_sha256.c `libgcrypt-config --cflags --libs`")

    def test(self):
        ret_c,ret_o = self.cmd("./hash_sha256 'This is a test message.'")
        expected_hash_value = "0668b515bfc41b90b6a90a6ae8600256e1c76a67d17c78a26127ddeb9b324435"
        self.assertTrue(ret_o == expected_hash_value, 'The generated hash value is not correct.')
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm hash_sha256.c hash_sha256")
