#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libgcrypt_fun_003.py
@Time:      2024/04/09 10:12:50
@Author:    hongfeng
@Version:   1.0
@Contact:   wb-zhf668260@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    hongfeng
"""

from common.basetest import LocalTest
class Test(LocalTest):
    """
    See tc_libgcrypt_fun_003.yaml for details

    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libgcrypt-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > random.c <<EOF
#include <stdio.h>
#include <string.h>
#include <gcrypt.h>
#include <errno.h>

int main() {
    // 初始化Libgcrypt
    if (!gcry_check_version(NULL)) {
        fprintf(stderr, "Error: libgcrypt version check failed\\n");
        return 1;
    }
    gcry_control(GCRYCTL_INITIALIZATION_FINISHED, 0);

    // 定义一个缓冲区来存放随机数据
    unsigned char random_data[32]; 

    // 使用GCRY_VERY_STRONG_RANDOM作为随机质量级别
    gcry_randomize(random_data, sizeof(random_data), GCRY_STRONG_RANDOM);
    
    // 成功生成随机数据，将其打印出来（这里转换为十六进制以便阅读）
    for (size_t i = 0; i < sizeof(random_data); ++i) {
        printf("%02X", random_data[i]);
    }
    printf("\\n");

    return 0;
}
EOF"""
        self.cmd(cmdline)
        self.cmd("gcc -o random random.c `libgcrypt-config --cflags --libs`")

    def test(self):
        ret_c, random1 = self.cmd("./random")
        ret_c, random2 = self.cmd("./random")
        ret_c, random3 = self.cmd("./random")
        self.assertTrue(random1 != random2 and random2 != random3 and random1 != random3, "The generated random numbers are the same.")
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm random.c random")
