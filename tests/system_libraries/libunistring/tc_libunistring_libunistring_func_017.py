#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libunistring_libunistring_func_017.py
@Time:      2024/02/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libunistring_libunistring_func_017.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "gcc libunistring libunistring-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_unistring_u16_startswith.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistr.h> // For u8_to_u16 and u16_startswith

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <string> <prefix>\n", argv[0]);
        return EXIT_FAILURE;
    }

    // 将传入的 UTF-8 字符串转换为 UTF-16
    const char *utf8_str = argv[1];
    const char *utf8_prefix = argv[2];
    size_t utf16_str_len = 0, utf16_prefix_len = 0;

    uint16_t *utf16_str = u8_to_u16((const uint8_t *)utf8_str, strlen(utf8_str), NULL, &utf16_str_len);
    if (!utf16_str) {
        fprintf(stderr, "Failed to convert main string from UTF-8 to UTF-16.\n");
        return EXIT_FAILURE;
    }

    if (u16_check(utf16_str, utf16_str_len) != NULL){
        perror("u16 check");
        return 1;
    }

    uint16_t *utf16_prefix = u8_to_u16((const uint8_t *)utf8_prefix, strlen(utf8_prefix), NULL, &utf16_prefix_len);
    if (!utf16_prefix) {
        fprintf(stderr, "Failed to convert prefix from UTF-8 to UTF-16.\n");
        free(utf16_str);
        return EXIT_FAILURE;
    }

    if (u16_check(utf16_prefix, utf16_prefix_len) != NULL){
        perror("u16 check");
        return 1;
    }

    // 使用 u16_startswith 检查字符串是否以给定的前缀开头
    int starts_with = u16_startswith(utf16_str, utf16_prefix);
    if (starts_with) {
        printf("The string \"%s\" starts with the prefix \"%s\".\n", utf8_str, utf8_prefix);
    } else {
        printf("The string \"%s\" does not start with the prefix \"%s\".\n", utf8_str, utf8_prefix);
    }

    // 释放内存
    free(utf16_str);
    free(utf16_prefix);

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)    
        self.cmd(f"gcc -o test_unistring_u16_startswith test_unistring_u16_startswith.c -lunistring")

    def test(self):
        self.cmd("./test_unistring_u16_startswith 'LONGXI' 'lon'")
        self.cmd("./test_unistring_u16_startswith 'LONGXI' 'LON'")
        self.cmd("./test_unistring_u16_startswith 'LONGXI123' 'LONGXI'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_unistring_u16_startswith.c', 'test_unistring_u16_startswith']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}", ignore_status=True)
