#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libunistring_libunistring_func_013.py
@Time:      2024/02/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libunistring_libunistring_func_013.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "gcc libunistring libunistring-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_unistring_u16_cmp2.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistr.h>
#include <uniconv.h>

// Function to convert a space-separated hex string to a uint16_t array
size_t hex_to_u16(const char *hex_str, uint16_t *u16_buf) {
    size_t u16_len = 0;
    while (*hex_str) {
        sscanf(hex_str, "%4hx", &u16_buf[u16_len++]);
        hex_str += 4; // Advance past the current 16-bit hex value
        if (*hex_str == ' ') hex_str++; // Skip spaces if present
    }
    return u16_len;
}

// Function to print a UTF-16 string as a UTF-8 string
void print_u16_as_utf8(const uint16_t *u16_str, size_t u16_len) {
    // Convert UTF-16 to UTF-8
    uint8_t *u8_str;
    size_t u8_len;
    u8_str = u16_to_u8(u16_str, u16_len, NULL, &u8_len);
    
    // Check for conversion error
    if (u8_str == NULL) {
        perror("Conversion from UTF-16 to UTF-8 failed");
        return;
    }
    
    // Print the UTF-8 string and free the memory
    fwrite(u8_str, 1, u8_len, stdout);
    free(u8_str);
}

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <utf16_hex_string1> <utf16_hex_string2>\n", argv[0]);
        return EXIT_FAILURE;
    }

    // Assume the input strings are space-separated hex values representing UTF-16 data
    const char *hex_str1 = argv[1];
    const char *hex_str2 = argv[2];

    // Allocate buffers to hold the UTF-16 data based on the input string lengths
    size_t buf_size1 = strlen(hex_str1) / 4; // 4 hex digits per UTF-16 character
    size_t buf_size2 = strlen(hex_str2) / 4;
    uint16_t *u16_str1 = (uint16_t *)malloc(buf_size1 * sizeof(uint16_t));
    uint16_t *u16_str2 = (uint16_t *)malloc(buf_size2 * sizeof(uint16_t));

    if (!u16_str1 || !u16_str2) {
        fprintf(stderr, "Memory allocation failed\n");
        free(u16_str1);
        free(u16_str2);
        return EXIT_FAILURE;
    }

    // Convert the hex strings to UTF-16 arrays
    size_t n1 = hex_to_u16(hex_str1, u16_str1);
    size_t n2 = hex_to_u16(hex_str2, u16_str2);

    if (u16_check(hex_str1, n1) != NULL){
        perror("u16 check");
        return 1;
    }

    if (u16_check(hex_str2, n2) != NULL){
        perror("u16 check");
        return 1;
    }

    // Compare the two UTF-16 strings
    int cmp_result = u16_cmp2(u16_str1, n1, u16_str2, n2);

    // Output the result of the comparison and the UTF-8 representation of the strings
    printf("String 1: ");
    print_u16_as_utf8(u16_str1, n1);
    printf("\nString 2: ");
    print_u16_as_utf8(u16_str2, n2);
    printf("\nComparison result: ");

    if (cmp_result < 0) {
        printf("String 1 is lexicographically less than String 2\n");
    } else if (cmp_result > 0) {
        printf("String 1 is lexicographically greater than String 2\n");
    } else {
        printf("String 1 is lexicographically equal to String 2\n");
    }

    // Clean up
    free(u16_str1);
    free(u16_str2);

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)    
        self.cmd(f"gcc -o test_unistring_u16_cmp2 test_unistring_u16_cmp2.c -lunistring -Wno-incompatible-pointer-types")

    def test(self):
        self.cmd("./test_unistring_u16_cmp2 '0041 0062 0063' '0041 0062 0064'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_unistring_u16_cmp2.c', 'test_unistring_u16_cmp2']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}", ignore_status=True)