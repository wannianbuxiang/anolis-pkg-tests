#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libunistring_libunistring_func_026.py
@Time:      2024/02/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libunistring_libunistring_func_026.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "gcc libunistring libunistring-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code_p = os.path.dirname(os.path.abspath(__file__))
        code = r'''cat > test_unistring_u32_chr.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <unistr.h>
#include <locale.h>
#include <string.h>

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <utf8_string> <ucs4_character_in_hex>\n", argv[0]);
        return EXIT_FAILURE;
    }

    // Set locale to ensure proper handling of multi-byte characters
    setlocale(LC_ALL, "");

    // Convert the command-line argument from UTF-8 to UTF-32
    const char *utf8_str = argv[1];
    size_t utf32_len;
    uint32_t *utf32_str = u8_to_u32((const uint8_t *)utf8_str, strlen(utf8_str), NULL, &utf32_len);
    if (u32_check(utf32_str, utf32_len) != NULL){
        perror("u32 check");
        return 1;
    }
    if (utf32_str == NULL) {
        perror("Conversion from UTF-8 to UTF-32 failed");
        return EXIT_FAILURE;
    }
    // Parse the UCS-4 character from the hexadecimal command-line argument
    char *endptr;
    ucs4_t ucs4_char = strtoul(argv[2], &endptr, 16);
    if (*endptr != '\0') {
        fprintf(stderr, "Invalid UCS-4 character: %s\n", argv[2]);
        free(utf32_str);
        return EXIT_FAILURE;
    }

    // Search for the UCS-4 character in the UTF-32 string
    uint32_t *result = u32_chr(utf32_str, utf32_len, ucs4_char);
    if (result != NULL) {
        printf("Character U+%04X found in the string \"%s\".\n", ucs4_char, utf8_str);
    } else {
        printf("Character U+%04X not found in the string \"%s\".\n", ucs4_char, utf8_str);
    }

    free(utf32_str);
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)    
        self.cmd(f"gcc -o test_unistring_u32_chr test_unistring_u32_chr.c -lunistring")

    def test(self):
        self.cmd("./test_unistring_u32_chr 'hello world' '0077'")
        self.cmd("./test_unistring_u32_chr 'hello world' '0x77'")
        self.cmd("./test_unistring_u32_chr 'hello world' '0x68'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_unistring_u32_chr.c', 'test_unistring_u32_chr']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}", ignore_status=True)