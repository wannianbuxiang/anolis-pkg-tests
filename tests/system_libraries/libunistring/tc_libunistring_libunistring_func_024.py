#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libunistring_libunistring_func_024.py
@Time:      2024/02/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libunistring_libunistring_func_024.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "gcc libunistring libunistring-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code_p = os.path.dirname(os.path.abspath(__file__))
        code = r'''cat > test_unistring_u32_startswith.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistr.h> // For u8_to_u32 and u32_startswith

// 成功执行返回 true，失败返回 false
bool prepare_utf32_from_utf8(const char *utf8_str, uint32_t **utf32_str, size_t *utf32_len) {
    if (!utf8_str || !utf32_str || !utf32_len) {
        return false;
    }  
    size_t utf8_len = strlen(utf8_str);
    *utf32_len = 0;
    *utf32_str = u8_to_u32((const uint8_t *)utf8_str, utf8_len, NULL, utf32_len);
    if (u32_check(utf32_str, utf32_len) != NULL){
        perror("u32 check");
        return 1;
    }
    return *utf32_str != NULL;
}

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <string> <prefix>\n", argv[0]);
        return EXIT_FAILURE;
    }

    // 将 UTF-8 字符串转换为 UTF-32
    uint32_t *utf32_str = NULL;
    uint32_t *utf32_prefix = NULL;
    size_t utf32_str_len = 0, utf32_prefix_len = 0;
    
    if (!prepare_utf32_from_utf8(argv[1], &utf32_str, &utf32_str_len) ||
        !prepare_utf32_from_utf8(argv[2], &utf32_prefix, &utf32_prefix_len)) {
        fprintf(stderr, "Failed to convert UTF-8 to UTF-32.\n");
        free(utf32_str); // 函数内部可能已经分配了内存
        return EXIT_FAILURE;
    }

    // 检查是否以 prefix 开头
    bool starts_with = u32_startswith(utf32_str, utf32_prefix);
    printf("The string \"%s\" %s with the prefix \"%s\".\n", argv[1], starts_with ? "starts" : "does not start", argv[2]);

    // 释放内存
    free(utf32_str);
    free(utf32_prefix);
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)    
        self.cmd(f"gcc -o test_unistring_u32_startswith test_unistring_u32_startswith.c -lunistring -Wno-incompatible-pointer-types -Wno-int-conversion")

    def test(self):
        self.cmd("./test_unistring_u32_startswith 'LONGXI' 'lon'")
        self.cmd("./test_unistring_u32_startswith 'LONGXI' 'LON'")
        self.cmd("./test_unistring_u32_startswith 'LONGXI123' 'LONGXI'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_unistring_u32_startswith.c', 'test_unistring_u32_startswith']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}", ignore_status=True)
