#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libunistring_libunistring_func_021.py
@Time:      2024/02/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libunistring_libunistring_func_021.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "gcc libunistring libunistring-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_unistring_u32_cmp2.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistr.h>

// Function to convert a string of space-separated hexadecimal code points to a UTF-32 array
size_t hex_to_u32(const char *hex_str, uint32_t **u32_buf) {
    size_t count = 0;
    const char *temp_ptr = hex_str;

    // Count the number of code points in the string
    while (*temp_ptr) {
        if (*temp_ptr == ' ') {
            count++;
        }
        temp_ptr++;
    }
    count++; // Count the last code point

    // Allocate memory for the UTF-32 array
    *u32_buf = (uint32_t *)malloc(count * sizeof(uint32_t));
    if (!(*u32_buf)) {
        perror("Memory allocation failed");
        exit(EXIT_FAILURE);
    }

    // Convert the string to UTF-32 code points
    size_t index = 0;
    const char *start = hex_str;
    char *end;
    while (*start) {
        (*u32_buf)[index++] = (uint32_t)strtoul(start, &end, 16);
        start = end;
        if (*start == ' ') {
            start++;
        }
    }

    return count; // Return the number of code points
}

// Function to print a UTF-32 array as UTF-8
void print_u32_as_utf8(const uint32_t *u32_str, size_t length) {
    uint8_t *u8_str;
    size_t u8_len;
    u8_str = u32_to_u8(u32_str, length, NULL, &u8_len);
    if (!u8_str) {
        perror("Conversion from UTF-32 to UTF-8 failed");
        exit(EXIT_FAILURE);
    }
    fwrite(u8_str, 1, u8_len, stdout);
    free(u8_str);
}

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <utf32_hex_string1> <utf32_hex_string2>\n", argv[0]);
        return EXIT_FAILURE;
    }

    // Convert the command-line arguments to UTF-32 arrays
    uint32_t *u32_str1, *u32_str2;
    size_t n1 = hex_to_u32(argv[1], &u32_str1);
    size_t n2 = hex_to_u32(argv[2], &u32_str2);

    // Compare the UTF-32 strings
    int cmp_result = u32_cmp2(u32_str1, n1, u32_str2, n2);

    // Output the comparison results and the UTF-8 representations of the strings
    printf("String 1: ");
    print_u32_as_utf8(u32_str1, n1);
    printf("\nString 2: ");
    print_u32_as_utf8(u32_str2, n2);
    printf("\nComparison result: ");
    if (cmp_result < 0) {
        printf("String 1 is lexicographically less than String 2.\n");
    } else if (cmp_result > 0) {
        printf("String 1 is lexicographically greater than String 2.\n");
    } else {
        printf("String 1 is lexicographically equal to String 2.\n");
    }

    // Clean up allocated memory
    free(u32_str1);
    free(u32_str2);

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)    
        self.cmd(f"gcc -o test_unistring_u32_cmp2 test_unistring_u32_cmp2.c -lunistring")

    def test(self):
        self.cmd("./test_unistring_u32_cmp2 '0000041 00000C2 0000263' '0000041 00000C2 0000263'")
        self.cmd("./test_unistring_u32_cmp2 '00000041 000000C2 00000102' '00000041 000000C2 00000102'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_unistring_u32_cmp2.c', 'test_unistring_u32_cmp2']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}", ignore_status=True)