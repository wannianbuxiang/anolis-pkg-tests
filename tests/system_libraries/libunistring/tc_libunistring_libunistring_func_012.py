#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libunistring_libunistring_func_012.py
@Time:      2024/02/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libunistring_libunistring_func_012.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "gcc libunistring libunistring-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_unistring_u8_cmp2.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistr.h>

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <string1> <string2>\n", argv[0]);
        return EXIT_FAILURE;
    }

    // These are the UTF-8 encoded strings provided as command-line arguments
    const uint8_t *s1 = (const uint8_t *)argv[1];
    const uint8_t *s2 = (const uint8_t *)argv[2];

    // Calculate the byte lengths of the input strings
    size_t n1 = strlen((const char *)s1);
    size_t n2 = strlen((const char *)s2);

    // utf-8字符串检查
    if (u8_check(s1, n1) != NULL){
        perror("u8 check");
        return 1;
    }

    if (u8_check(s2, n2) != NULL){
        perror("u8 check");
        return 1;
    }

    // Compare the two strings using u8_cmp2
    int cmp_result = u8_cmp2(s1, n1, s2, n2);

    // Output the result of the comparison
    if (cmp_result < 0) {
        printf("'%s' is lexicographically less than '%s'\n", s1, s2);
    } else if (cmp_result > 0) {
        printf("'%s' is lexicographically greater than '%s'\n", s1, s2);
    } else {
        printf("'%s' is lexicographically equal to '%s'\n", s1, s2);
    }

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)    
        self.cmd(f"gcc -o test_unistring_u8_cmp2 test_unistring_u8_cmp2.c -lunistring")

    def test(self):
        self.cmd("./test_unistring_u8_cmp2 'long' 'xi'")
        self.cmd("./test_unistring_u8_cmp2 'abc123' 'abc012'")
        self.cmd("./test_unistring_u8_cmp2 'abc' 'cde'")
        self.cmd("./test_unistring_u8_cmp2 'longxi' 'longxi'")
        self.cmd("./test_unistring_u8_cmp2 'Longxi' 'longxi'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_unistring_u8_cmp2.c', 'test_unistring_u8_cmp2']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}", ignore_status=True)