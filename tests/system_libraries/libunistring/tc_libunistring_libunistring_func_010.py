#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libunistring_libunistring_func_010.py
@Time:      2024/02/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libunistring_libunistring_func_010.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "gcc libunistring libunistring-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_unistring_u8_chr.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistr.h>

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <utf8_string> <ucs4_character_in_hex>\n", argv[0]);
        return EXIT_FAILURE;
    }

    const uint8_t *utf8_str = (const uint8_t *)argv[1];
    size_t n = strlen(argv[1]); // Length of the string in bytes
    if (u8_check(utf8_str, n) != NULL){
        perror("u8 check");
        return 1;
    }
    // Convert the hexadecimal UCS-4 character from the command line to ucs4_t
    char *endptr;
    ucs4_t uc = strtoul(argv[2], &endptr, 16);
    if (*endptr != '\0') {
        fprintf(stderr, "Invalid UCS-4 character: %s\n", argv[2]);
        return EXIT_FAILURE;
    }

    // Search for the UCS-4 character in the UTF-8 string
    uint8_t *result = u8_chr(utf8_str, n, uc);
    if (result != NULL) {
        printf("Character U+%04X found in the string \"%s\".\n", uc, (char *)utf8_str);
    } else {
        printf("Character U+%04X not found in the string \"%s\".\n", uc, (char *)utf8_str);
    }
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)    
        self.cmd(f"gcc -o test_unistring_u8_chr test_unistring_u8_chr.c -lunistring")

    def test(self):
        self.cmd("./test_unistring_u8_chr 'hello world' '006C'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_unistring_u8_chr.c', 'test_unistring_u8_chr']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}", ignore_status=True)