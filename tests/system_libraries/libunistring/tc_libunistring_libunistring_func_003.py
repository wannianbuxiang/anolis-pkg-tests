#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libunistring_libunistring_func_003.py
@Time:      2024/02/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libunistring_libunistring_func_003.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "gcc libunistring libunistring-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code_p = os.path.dirname(os.path.abspath(__file__))
        code = r'''cat > test_unistring_u8_cmp.c << EOF
#include <unistr.h>
#include <stdio.h>
#include <string.h>
#include "CODE_P/verify_utf8.h"

int main(int argc, char *argv[]) {
    // 需要二个参数
    if (argc < 3) {
        printf("Error:At least two parameter is required.\n");
        return 1;
    }
    // 比较两个字符串
    const char *s1 = argv[1];
    const char *s2 = argv[2];
    if (!isValidUtf8(s1)) {
        printf("Error: The string is NOT valid UTF-8--->(%s)\n", s1);
        return 1;
    }
    if (!isValidUtf8(s2)) {
        printf("Error: The string is NOT valid UTF-8--->(%s)\n", s2);
        return 1;
    } 
    int result = u8_strcmp(s1, s2);
    if (result < 0) {
        printf("%s < %s\n", s1, s2);
    } else if (result > 0) {
        printf("%s > %s\n", s1, s2);
    } else {
        printf("%s == %s\n", s1, s2);
    }
    return 0;
}
EOF'''
        self.cmd(code)
        self.cmd(f"sed -i 's|CODE_P|{code_p}|g' test_unistring_u8_cmp.c")
        self.cmd(f"gcc -o test_unistring_u8_cmp test_unistring_u8_cmp.c {code_p}/verify_utf8.c -lunistring")

    def test(self):
        self.cmd("./test_unistring_u8_cmp abc adc")
        self.cmd("./test_unistring_u8_cmp 123 1234")
        self.cmd("./test_unistring_u8_cmp ac123 ac1234")
        self.cmd("./test_unistring_u8_cmp 123 123")
        self.cmd("./test_unistring_u8_cmp abc abc")
        self.cmd("./test_unistring_u8_cmp 10 1")
        self.cmd("./test_unistring_u8_cmp b a")


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_unistring_u8_cmp', 'test_unistring_u8_cmp.c']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}")
