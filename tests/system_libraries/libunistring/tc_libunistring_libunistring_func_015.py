#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libunistring_libunistring_func_015.py
@Time:      2024/02/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libunistring_libunistring_func_015.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "gcc libunistring libunistring-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_unistring_u16_strstr.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistr.h>

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <haystack_utf8> <needle_utf8>\n", argv[0]);
        return EXIT_FAILURE;
    }

    // Convert the UTF-8 command-line arguments to UTF-16
    size_t haystack_len, needle_len;
    uint16_t *haystack_u16 = u8_to_u16((uint8_t *)argv[1], strlen(argv[1]), NULL, &haystack_len);
    uint16_t *needle_u16 = u8_to_u16((uint8_t *)argv[2], strlen(argv[2]), NULL, &needle_len);

    if (haystack_u16 == NULL || needle_u16 == NULL) {
        fprintf(stderr, "Failed to convert UTF-8 to UTF-16.\n");
        free(haystack_u16);
        free(needle_u16);
        return EXIT_FAILURE;
    }

    if (u16_check(haystack_u16, haystack_len) != NULL){
        perror("u16 check");
        return 1;
    }

    if (u16_check(needle_u16, needle_len) != NULL){
        perror("u16 check");
        return 1;
    }
    // Use u16_strstr to search for the needle in the haystack
    uint16_t *result = u16_strstr(haystack_u16, needle_u16);

    if (result != NULL) {
        // Calculate the offset in terms of UTF-16 units (not UTF-8 bytes)
        size_t offset = result - haystack_u16;
        printf("Needle found at UTF-16 offset: %zu\n", offset);
    } else {
        printf("Needle not found in haystack.\n");
    }

    // Clean up
    free(haystack_u16);
    free(needle_u16);

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)    
        self.cmd(f"gcc -o test_unistring_u16_strstr test_unistring_u16_strstr.c -lunistring")

    def test(self):
        self.cmd("./test_unistring_u16_strstr 'This is a simple haystack', 'test'")
        self.cmd("./test_unistring_u16_strstr 'This is a simple haystack', 'is'")
        self.cmd("./test_unistring_u16_strstr 'This is a simple haystack', 'simple'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_unistring_u16_strstr.c', 'test_unistring_u16_strstr']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}", ignore_status=True)