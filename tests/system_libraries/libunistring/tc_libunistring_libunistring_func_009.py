#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libunistring_libunistring_func_009.py
@Time:      2024/02/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libunistring_libunistring_func_009.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "gcc libunistring libunistring-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_unistring_u8_endswith.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistr.h>
int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <string> <suffix>\n", argv[0]);
        return EXIT_FAILURE;
    }

    const uint8_t *str = (const uint8_t *)argv[1];
    const uint8_t *suffix = (const uint8_t *)argv[2];
    size_t str_len = sizeof(str);
    size_t suffix_len = sizeof(suffix);

    // utf-8字符串检查
    if (u8_check(str, str_len) != NULL){
        perror("u8 check");
        return 1;
    }

    if (u8_check(suffix, suffix_len) != NULL){
        perror("u8 check");
        return 1;
    }
    // 检查 str 是否以 suffix 结尾
    int ends_with = u8_endswith(str, suffix);

    printf("The string \"%s\" %s with the suffix \"%s\".\n", argv[1], ends_with ? "ends" : "does not end", argv[2]);
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)    
        self.cmd(f"gcc -o test_unistring_u8_endswith test_unistring_u8_endswith.c -lunistring")

    def test(self):
        self.cmd("./test_unistring_u8_endswith 'LONGXI' 'xi'")
        self.cmd("./test_unistring_u8_endswith 'LONGXI' 'XI'")
        self.cmd("./test_unistring_u8_endswith 'LONGXI123' '123'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_unistring_u8_endswith.c', 'test_unistring_u8_endswith']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}", ignore_status=True)
