#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libunistring_libunistring_func_016.py
@Time:      2024/02/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libunistring_libunistring_func_016.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "gcc libunistring libunistring-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_unistring_u16_tolower.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>
#include <locale.h>
#include <unicase.h>
#include <unistr.h>
#include <uninorm.h>

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <string_to_lowercase>\n", argv[0]);
        return EXIT_FAILURE;
    }

    // 设置 locale 以支持宽字符输出
    setlocale(LC_ALL, "");

    // 将传入的 UTF-8 字符串转换为 UTF-16
    const char *utf8_str = argv[1];
    size_t utf8_len = strlen(utf8_str);
    size_t utf16_len = 0;
    uint16_t *utf16_str = u8_to_u16((const uint8_t *)utf8_str, utf8_len, NULL, &utf16_len);
    if (!utf16_str) {
        fprintf(stderr, "Failed to convert UTF-8 to UTF-16.\n");
        return EXIT_FAILURE;
    }

    if (u16_check(utf16_str, utf16_len) != NULL){
        perror("u16 check");
        return 1;
    }
    // 准备结果缓冲区
    uint16_t *resultbuf = (uint16_t *)malloc((utf16_len + 1) * sizeof(uint16_t));
    if (!resultbuf) {
        perror("malloc");
        free(utf16_str);
        return EXIT_FAILURE;
    }

    // 将 UTF-16 字符串转换为小写
    size_t lengthp = 0;
    uint16_t *lower_str = u16_tolower(utf16_str, utf16_len, NULL, UNINORM_NFC, resultbuf, &lengthp);
    if (!lower_str) {
        fprintf(stderr, "Failed to convert UTF-16 to lowercase.\n");
        free(utf16_str);
        free(resultbuf);
        return EXIT_FAILURE;
    }

    // 将 UTF-16 小写字符串转换回 UTF-8
    size_t utf8_lower_len = 0;
    uint8_t *utf8_lower_str = u16_to_u8(lower_str, lengthp, NULL, &utf8_lower_len);
    if (!utf8_lower_str) {
        fprintf(stderr, "Failed to convert lowercase UTF-16 back to UTF-8.\n");
        free(utf16_str);
        free(resultbuf);
        return EXIT_FAILURE;
    }

    // 打印小写 UTF-8 字符串
    printf("lowercase string: %s\n", (char *)utf8_lower_str);

    // 释放资源
    free(utf16_str);
    free(resultbuf);
    free(utf8_lower_str);

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)    
        self.cmd(f"gcc -o test_unistring_u16_tolower test_unistring_u16_tolower.c -lunistring")

    def test(self):
        self.cmd("./test_unistring_u16_tolower 'LONGXI'")
        self.cmd("./test_unistring_u16_tolower 'UTF-8 ENcoded STRING'")
        self.cmd("./test_unistring_u16_tolower 'TEST 123 teSt'")


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_unistring_u16_tolower.c', 'test_unistring_u16_tolower']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}", ignore_status=True)
