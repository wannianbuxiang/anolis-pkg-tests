#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libunistring_libunistring_func_006.py
@Time:      2024/02/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libunistring_libunistring_func_006.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "gcc libunistring libunistring-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_unistring_u8_tolower.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <unistr.h>
#include <unicase.h>

int main(int argc, char *argv[]) {
    // 检查命令行参数是否正确
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <string>\n", argv[0]);
        return EXIT_FAILURE;
    }
    // UTF-8 encoded string "xxx" in a uint8_t array
    uint8_t *str = (uint8_t *)argv[1];
    size_t len = sizeof(str); // The length includes the null terminator
    if (u8_check(str, len) != NULL){
        perror("u8 check");
        return 1;
    }
    size_t lengthp;  // The length of the result

    // Allocate memory for the result buffer
    uint8_t *resultbuf = (uint8_t *)malloc(len);
    if (resultbuf == NULL) {
        perror("malloc");
        return EXIT_FAILURE;
    }
    // Convert the string to uppercase
    uint8_t *result = u8_tolower(str, len - 1, "en", UNINORM_NFC, resultbuf, &lengthp);
    if (result) {
        // Ensure the result is null-terminated
        resultbuf[lengthp] = '\0';
        // Print the uppercase string
        printf("Lowercase string: %s\n", resultbuf);
    } else {
        // Handle the error
        printf("Error: Failed to converted lowercase string(%s).\n", str);
    }
    // Clean up
    free(resultbuf);
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)    
        self.cmd(f"gcc -o test_unistring_u8_tolower test_unistring_u8_tolower.c -lunistring")

    def test(self):
        self.cmd("./test_unistring_u8_tolower LONGXI")
        self.cmd("./test_unistring_u8_tolower LongXI")
        self.cmd("./test_unistring_u8_tolower AbC1")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_unistring_u8_tolower.c', 'test_unistring_u8_tolower']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}", ignore_status=True)
