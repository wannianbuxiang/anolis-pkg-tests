#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libunistring_libunistring_func_019.py
@Time:      2024/02/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libunistring_libunistring_func_019.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "gcc libunistring libunistring-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_unistring_u16_chr.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <unistr.h>  // libunistring 库中的函数声明
#include <locale.h>  // 用于 setlocale

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <utf8_string> <ucs4_character>\n", argv[0]);
        return EXIT_FAILURE;
    }

    // 设置 locale 以确保正确处理多字节字符
    setlocale(LC_ALL, "");

    // 参数：UTF-8 编码的字符串和 UCS-4 字符
    const char *utf8_str = argv[1];
    ucs4_t ucs4_char = strtol(argv[2], NULL, 16); // 假设命令行参数是十六进制数字

    // 将 UTF-8 转换为 UTF-16
    size_t utf16_len;
    uint16_t *utf16_str = u8_to_u16((uint8_t *)utf8_str, strlen(utf8_str), NULL, &utf16_len);
    
    if (u16_check(utf16_str, utf16_len) != NULL){
        perror("u16 check");
        return 1;
    }

    if (utf16_str == NULL) {
        perror("Failed to convert UTF-8 to UTF-16");
        return EXIT_FAILURE;
    }

    // 使用 u16_chr 在 UTF-16 字符串中搜索 UCS-4 字符
    uint16_t *found = u16_chr(utf16_str, utf16_len, ucs4_char);

    if (found != NULL) {
        printf("Character U+%04X found in string '%s'\n", ucs4_char, utf8_str);
    } else {
        printf("Character U+%04X not found in string '%s'\n", ucs4_char, utf8_str);
    }

    free(utf16_str);
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)    
        self.cmd(f"gcc -o test_unistring_u16_chr test_unistring_u16_chr.c -lunistring")

    def test(self):
        self.cmd("./test_unistring_u16_chr 'hello world' '006C'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_unistring_u16_chr.c', 'test_unistring_u16_chr']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}", ignore_status=True)
