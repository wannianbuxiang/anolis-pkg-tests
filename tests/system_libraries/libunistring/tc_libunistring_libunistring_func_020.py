#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libunistring_libunistring_func_020.py
@Time:      2024/02/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libunistring_libunistring_func_020.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "gcc libunistring libunistring-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code_p = os.path.dirname(os.path.abspath(__file__))
        code = r'''cat > test_unistring_u32_strstr.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistr.h>

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <haystack_utf8> <needle_utf8>\n", argv[0]);
        return EXIT_FAILURE;
    }

    // Convert the UTF-8 command-line arguments to UTF-32
    size_t haystack_len, needle_len;
    uint32_t *haystack_u32 = u8_to_u32((uint8_t *)argv[1], strlen(argv[1]), NULL, &haystack_len);
    uint32_t *needle_u32 = u8_to_u32((uint8_t *)argv[2], strlen(argv[2]), NULL, &needle_len);

    if (u32_check(haystack_u32, haystack_len) != NULL){
        perror("u32 check");
        return 1;
    }

    if (u32_check(needle_u32, needle_len) != NULL){
        perror("u32 check");
        return 1;
    }

    if (haystack_u32 == NULL || needle_u32 == NULL) {
        fprintf(stderr, "Failed to convert UTF-8 to UTF-32.\n");
        free(haystack_u32); // It is safe to call free on NULL
        free(needle_u32);
        return EXIT_FAILURE;
    }

    // Use u32_strstr to search for the needle in the haystack
    uint32_t *result = u32_strstr(haystack_u32, needle_u32);

    if (result != NULL) {
        // Calculate the offset in terms of UTF-32 code units (not UTF-8 bytes)
        size_t offset = result - haystack_u32;
        printf("Needle found at UTF-32 offset: %zu\n", offset);
    } else {
        printf("Needle not found in haystack.\n");
    }

    // Clean up
    free(haystack_u32);
    free(needle_u32);

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)    
        self.cmd(f"gcc -o test_unistring_u32_strstr test_unistring_u32_strstr.c -lunistring")

    def test(self):
        self.cmd("./test_unistring_u32_strstr 'This is a simple haystack', 'test'")
        self.cmd("./test_unistring_u32_strstr 'This is a simple haystack', 'is'")
        self.cmd("./test_unistring_u32_strstr 'This is a simple haystack', 'simple'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_unistring_u32_strstr.c', 'test_unistring_u32_strstr']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}", ignore_status=True)
