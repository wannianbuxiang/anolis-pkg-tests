#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libunistring_libunistring_func_011.py
@Time:      2024/02/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libunistring_libunistring_func_011.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "gcc libunistring libunistring-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_unistring_u8_strstr.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <unistr.h>

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <haystack> <needle>\n", argv[0]);
        return EXIT_FAILURE;
    }

    // The haystack and needle are assumed to be UTF-8 encoded strings
    const uint8_t *haystack = (const uint8_t *)argv[1];
    const uint8_t *needle = (const uint8_t *)argv[2];
    size_t haystack_len = sizeof(haystack);
    size_t needle_len = sizeof(needle);

    // utf-8字符串检查
    if (u8_check(haystack, haystack_len) != NULL){
        perror("u8 check");
        return 1;
    }
    if (u8_check(needle, needle_len) != NULL){
        perror("u8 check");
        return 1;
    }

    // Use u8_strstr to find the needle in the haystack
    const uint8_t *result = u8_strstr(haystack, needle);

    if (result != NULL) {
        // Convert the result pointer to a character index relative to the start of haystack
        size_t index = result - haystack;
        printf("Needle found at byte index: %zu\n", index);
    } else {
        printf("Needle not found in haystack.\n");
    }
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)    
        self.cmd(f"gcc -o test_unistring_u8_strstr test_unistring_u8_strstr.c -lunistring")

    def test(self):
        self.cmd("./test_unistring_u8_strstr 'This is a simple haystack', 'test'")
        self.cmd("./test_unistring_u8_strstr 'This is a simple haystack', 'is'")
        self.cmd("./test_unistring_u8_strstr 'This is a simple haystack', 'simple'")
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_unistring_u8_strstr.c', 'test_unistring_u8_strstr']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}", ignore_status=True)