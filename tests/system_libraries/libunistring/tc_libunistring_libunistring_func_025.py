#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libunistring_libunistring_func_025.py
@Time:      2024/02/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libunistring_libunistring_func_025.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "gcc libunistring libunistring-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code_p = os.path.dirname(os.path.abspath(__file__))
        code = r'''cat > test_unistring_u32_endtswith.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistr.h>  // For u8_to_u32 function

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <string> <suffix>\n", argv[0]);
        return EXIT_FAILURE;
    }

    // Convert the UTF-8 arguments to UTF-32
    size_t str_len, suffix_len;
    uint32_t *str_u32 = u8_to_u32((uint8_t*)argv[1], strlen(argv[1]), NULL, &str_len);
    uint32_t *suffix_u32 = u8_to_u32((uint8_t*)argv[2], strlen(argv[2]), NULL, &suffix_len);

    if (u32_check(str_u32, str_len) != NULL){
        perror("u32 check");
        return 1;
    }

    if (u32_check(suffix_u32, suffix_len) != NULL){
        perror("u32 check");
        return 1;
    }

    if (str_u32 == NULL || suffix_u32 == NULL) {
        fprintf(stderr, "Error converting UTF-8 to UTF-32.\n");
        free(str_u32);
        free(suffix_u32);
        return EXIT_FAILURE;
    }

    // Check if the UTF-32 string ends with the given suffix
    int result = u32_endswith(str_u32, suffix_u32);

    printf("The string \"%s\" %s with the suffix \"%s\".\n",
           argv[1],
           result ? "ends" : "does not end",
           argv[2]);

    // Clean up
    free(str_u32);
    free(suffix_u32);

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)    
        self.cmd(f"gcc -o test_unistring_u32_endtswith test_unistring_u32_endtswith.c -lunistring")

    def test(self):
        self.cmd("./test_unistring_u32_endtswith 'LONGXI' 'xi'")
        self.cmd("./test_unistring_u32_endtswith 'LONGXI' 'XI'")
        self.cmd("./test_unistring_u32_endtswith 'LONGXI123' '123'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_unistring_u32_endtswith.c', 'test_unistring_u32_endtswith']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}", ignore_status=True)
