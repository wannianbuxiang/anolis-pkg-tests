#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libunistring_libunistring_func_008.py
@Time:      2024/02/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libunistring_libunistring_func_008.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "gcc libunistring libunistring-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_unistring_u8_startswith.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistr.h> // 如果存在的话，此头文件应该包含 u8_startswith 的声明

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <string> <prefix>\n", argv[0]);
        return EXIT_FAILURE;
    }

    const uint8_t *str = (const uint8_t *)argv[1];
    const uint8_t *prefix = (const uint8_t *)argv[2];
    size_t str_len = sizeof(str);
    size_t prefix_len = sizeof(prefix);

    // utf-8字符串检查
    if (u8_check(str, str_len) != NULL){
        perror("u8 check");
        return 1;
    }

    if (u8_check(prefix, prefix_len) != NULL){
        perror("u8 check");
        return 1;
    }
    // 检查str是否以prefix开头
    int starts_with = u8_startswith(str, prefix);
    printf("The string \"%s\" %s with the prefix \"%s\".\n", argv[1], starts_with ? "starts" : "does not start", argv[2]);
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)    
        self.cmd(f"gcc -o test_unistring_u8_startswith test_unistring_u8_startswith.c -lunistring")

    def test(self):
        self.cmd("./test_unistring_u8_startswith 'LONGXI' 'lon'")
        self.cmd("./test_unistring_u8_startswith 'LONGXI' 'LON'")
        self.cmd("./test_unistring_u8_startswith 'LONGXI123' 'LONGXI'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_unistring_u8_startswith.c', 'test_unistring_u8_startswith']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}", ignore_status=True)
