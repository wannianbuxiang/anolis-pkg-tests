#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libunistring_libunistring_func_018.py
@Time:      2024/02/22 14:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libunistring_libunistring_func_018.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "gcc libunistring libunistring-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code_p = os.path.dirname(os.path.abspath(__file__))
        code = r'''cat > test_unistring_u16_endtswith.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistr.h>
#include <stdbool.h>

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <string> <suffix>\n", argv[0]);
        return EXIT_FAILURE;
    }

    // 将命令行参数从UTF-8转换为UTF-16
    size_t str_utf8_len = strlen(argv[1]);
    size_t suffix_utf8_len = strlen(argv[2]);
    size_t str_utf16_len, suffix_utf16_len;

    // 转换主字符串到UTF-16
    uint16_t *str_utf16 = (uint16_t *)malloc(sizeof(uint16_t) * (str_utf8_len + 1));
    if (!str_utf16) {
        perror("Failed to allocate memory for UTF-16 string");
        return EXIT_FAILURE;
    }
    size_t ret = u8_to_u16((uint8_t *)argv[1], str_utf8_len, str_utf16, &str_utf16_len);
    if (ret == (size_t)-1) {
        fprintf(stderr, "Failed to convert main string to UTF-16\n");
        free(str_utf16);
        return EXIT_FAILURE;
    }

    if (u16_check(ret, str_utf16_len) != NULL){
        perror("u16 check");
        return 1;
    }
    str_utf16[str_utf16_len] = 0; // Null-terminate the UTF-16 string

    // 转换后缀到UTF-16
    uint16_t *suffix_utf16 = (uint16_t *)malloc(sizeof(uint16_t) * (suffix_utf8_len + 1));
    if (!suffix_utf16) {
        perror("Failed to allocate memory for UTF-16 suffix");
        free(str_utf16);
        return EXIT_FAILURE;
    }
    ret = u8_to_u16((uint8_t *)argv[2], suffix_utf8_len, suffix_utf16, &suffix_utf16_len);
    if (ret == (size_t)-1) {
        fprintf(stderr, "Failed to convert suffix to UTF-16\n");
        free(str_utf16);
        free(suffix_utf16);
        return EXIT_FAILURE;
    }

    if (u16_check(ret, suffix_utf16_len) != NULL){
        perror("u16 check");
        return 1;
    }
    suffix_utf16[suffix_utf16_len] = 0; // Null-terminate the UTF-16 suffix

    // 检查字符串是否以后缀结束
    bool ends_with = u16_endswith(str_utf16, suffix_utf16);

    printf("The string \"%s\" %s with the suffix \"%s\".\n",
           argv[1], ends_with ? "ends" : "does not end", argv[2]);

    // 清理
    free(str_utf16);
    free(suffix_utf16);
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)    
        self.cmd(f"gcc -o test_unistring_u16_endtswith test_unistring_u16_endtswith.c -lunistring -Wno-int-conversion")

    def test(self):
        self.cmd("./test_unistring_u16_endtswith 'LONGXI' 'xi'")
        self.cmd("./test_unistring_u16_endtswith 'LONGXI' 'XI'")
        self.cmd("./test_unistring_u16_endtswith 'LONGXI123' '123'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for file in ['test_unistring_u16_endtswith.c', 'test_unistring_u16_endtswith']:
            if os.path.exists(file):
                self.cmd(f"rm -rf {file}", ignore_status=True)
