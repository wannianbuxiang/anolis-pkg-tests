#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libevent_func002.py
@Time:      2024/04/12 10:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""
from common.basetest import LocalTest

class Test(LocalTest): 
    """
    See tc_libevent_func002.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libevent libevent-devel"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_libevent.c <<EOF
#include <sys/types.h>
#include <event2/event-config.h>
#include <sys/stat.h>
#ifndef _WIN32
#include <sys/queue.h>
#include <unistd.h>
#include <sys/time.h>
#else
#include <winsock2.h>
#include <windows.h>
#endif
#include <signal.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <signal.h>

#include <event2/event.h>

int called = 0;

static void
signal_cb(evutil_socket_t fd, short event, void *arg)
{
    struct event *signal = arg;

    printf("signal_cb: got signal %d\\n", event_get_signal(signal));

    if (called >= 2)
        event_del(signal);

    called++;
}

void signal_handler(int signum)
{
    printf("Caught SIGINT signal in-process simulation\\n");
	return 0;
}

static void
timeout_cb(evutil_socket_t fd, short what, void *arg)
{
    printf("Timeout reached, exiting event loop...\\n");
    event_base_loopexit((struct event_base *)arg, NULL);
}

int main(int argc, char **argv)
{
    struct event *signal_int = NULL;
    struct event_base* base = NULL;
    struct event *timeout_event = NULL;
    int ret = 0;
#ifdef _WIN32
    WORD wVersionRequested;
    WSADATA wsaData;

    wVersionRequested = MAKEWORD(2, 2);

    (void) WSAStartup(wVersionRequested, &wsaData);
#endif

    /* Initialize the event library */
    base = event_base_new();
    if (!base) {
        ret = 1;
        goto out;
    }

    /* Initialize one event */
    signal_int = evsignal_new(base, SIGINT, signal_cb, event_self_cbarg());
    if (!signal_int) {
        ret = 2;
        goto out;
    }
    event_add(signal_int, NULL);

	// 注册SIGINT信号处理函数
    signal(SIGINT, signal_handler);
    // 模拟接收到SIGINT信号
    raise(SIGINT);

	// 将base作为timeout_cb的参数传递
    timeout_event = evtimer_new(base, timeout_cb, base); 
    if (!timeout_event) {
        printf("Timeout initialization failed\\n");
        ret = 3; // 添加新的错误返回值，标识超时事件初始化失败
        goto out;
    }

    struct timeval timeout = {5, 0}; // 5秒超时
    evtimer_add(timeout_event, &timeout);
    event_base_dispatch(base);
	printf("Program continues after handling SIGINT\\n");

out:
    if (timeout_event)
        event_free(timeout_event);
    if (signal_int)
        event_free(signal_int);
    if (base)
        event_base_free(base);
    return ret;
}
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o /tmp/ghm/test_libevent /tmp/ghm/test_libevent.c -levent -lpthread")
        ret_c1, ret_o1 = self.cmd("/tmp/ghm/test_libevent")
        self.assertIn("Caught SIGINT signal in-process simulation", ret_o1)
        self.assertIn("Timeout reached, exiting event loop...", ret_o1)
        self.assertIn("Program continues after handling SIGINT", ret_o1)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")