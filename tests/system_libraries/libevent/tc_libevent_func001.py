#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libevent_func001.py
@Time:      2024/04/11 13:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""
from common.basetest import LocalTest

class Test(LocalTest): 
    """
    See tc_libevent_func001.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libevent libevent-devel"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm/")
        cmdline = """cat > /tmp/ghm/test_libevent.c <<EOF
#include <event2/event.h>
#include <stdio.h>
#include <stdlib.h>

static void timer_cb(evutil_socket_t fd, short what, void *arg) {
    printf("Timer event triggered.\\n");
    event_base_loopbreak((struct event_base*)arg);
}

int main() {
    struct event_base *base = event_base_new();
    struct event *timer = event_new(base, -1, 0, timer_cb, base);
    struct timeval tv = { .tv_sec = 1, .tv_usec = 0 }; // 1秒后触发

    if (event_add(timer, &tv) == -1) {
        perror("event_add");
        return 1;
    }

    event_base_dispatch(base);
    event_free(timer);
    event_base_free(base);
    return 0;
}
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o /tmp/ghm/test_libevent /tmp/ghm/test_libevent.c -levent -lpthread")
        ret_c1, ret_o1 = self.cmd("/tmp/ghm/test_libevent")
        self.assertIn("Timer event triggered.", ret_o1)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm/")