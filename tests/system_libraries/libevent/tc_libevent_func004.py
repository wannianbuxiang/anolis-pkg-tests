#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libevent_func003.py
@Time:      2024/04/12 15:12:50
@Author:    gaohongmei
@Version:   1.0
@Contact:   wb-ghm935099@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    gaohongmei
"""

from common.basetest import LocalTest
import pexpect
import os
class Test(LocalTest): 
    """
    See tc_libevent_func003.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "libevent libevent-devel telnet"}
    def setUp(self): 
        super().setUp(self.PARAM_DIC)
        self.cmd("mkdir /tmp/ghm")
        cmdline = """cat > /tmp/ghm/test_libevent.c <<EOF
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <event2/event.h>
#include <event2/event_struct.h>
#include <event2/util.h>

static void read_cb(evutil_socket_t fd, short which, void *arg)
{
    char buffer[1024];
    ssize_t nread;

    nread = read(fd, buffer, sizeof(buffer)-1);
    if (nread <= 0) {
        if (nread == 0 || (nread == -1 && errno != EAGAIN)) {
            // 关闭读事件，清理资源（实际应用中可能还需要关闭连接等）
            struct event *ev = (struct event *)arg;
            event_del(ev);
            close(fd);
            printf("Connection closed.\\n");
        } else {
            perror("read");
        }
    } else {
        buffer[nread] = '\\0';
        printf("Received data: %s\\n", buffer);

        // 检查是否收到 "quit" 消息
        if (strstr(buffer, "quit") != NULL) {
            struct event *ev = (struct event *)arg;
            struct event_base *base = event_get_base(ev);
            event_del(ev);
            event_base_loopbreak(base);
            printf("Quit message received, shutting down server.\\n");
        }
    }
}

// 写事件回调函数（这里仅作示例，实际应用场景可能更为复杂）
static void write_cb(evutil_socket_t fd, short which, void *arg)
{
    const char *data_to_send = "Server response.";
    ssize_t nwritten;

    nwritten = write(fd, data_to_send, strlen(data_to_send));
    if (nwritten < 0) {
        if (errno != EAGAIN) {
            // 关闭写事件，清理资源
            struct event *ev = (struct event *)arg;
            event_del(ev);
        }
        perror("write");
    } else {
        printf("Sent data: %.*s\\n", (int)nwritten, data_to_send);
    }
}

// 新连接事件回调函数
static void new_conn_cb(evutil_socket_t listener, short what, void *arg)
{
    struct event_base *base = (struct event_base*)arg;
    struct sockaddr_storage ss;
    socklen_t slen = sizeof(ss);
    evutil_socket_t new_sock = accept(listener, (struct sockaddr*)&ss, &slen);
    if (new_sock < 0) {
        perror("accept");
        return;
    }

    // 设置新连接为非阻塞模式
    fcntl(new_sock, F_SETFL, O_NONBLOCK);

    // 创建读事件
    struct event *read_ev = event_new(base, new_sock, EV_READ | EV_PERSIST, read_cb, event_self_cbarg());
    if (!read_ev) {
        perror("event_new (read)");
        close(new_sock);
        return;
    }
    event_add(read_ev, NULL);

    // 创建写事件（根据实际需求决定是否立即添加写事件）
    // struct event *write_ev = event_new(base, new_sock, EV_WRITE | EV_PERSIST, write_cb, event_self_cbarg());
    // if (!write_ev) {
    //     perror("event_new (write)");
    //     event_del(read_ev);
    //     event_free(read_ev);
    //     close(new_sock);
    //     return;
    // }
    // event_add(write_ev, NULL);
}

int main()
{
    struct event_base *base;
    struct event *listener_event;
    int listener_sock;
    struct sockaddr_in serv_addr;
    socklen_t sin_len = sizeof(serv_addr);
    int enable_reuseaddr = 1;

    // 创建监听套接字并设置重用地址选项
    listener_sock = socket(AF_INET, SOCK_STREAM, 0);
    setsockopt(listener_sock, SOL_SOCKET, SO_REUSEADDR, &enable_reuseaddr, sizeof(enable_reuseaddr));

    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(12345); // 示例端口

    bind(listener_sock, (struct sockaddr*)&serv_addr, sin_len);
    listen(listener_sock, 10); // 最大并发连接数为10

    base = event_base_new();
    if (!base) {
        perror("event_base_new");
        return 1;
    }

    // 创建监听连接事件
    listener_event = event_new(base, listener_sock, EV_READ | EV_PERSIST, new_conn_cb, base);
    if (!listener_event) {
        perror("event_new (listener)");
        return 1;
    }
    event_add(listener_event, NULL);

    // 开始事件循环
    event_base_dispatch(base);

    // 清理资源（这里假设程序正常结束，实际上可能需要在其他适当地方清理）
    event_free(listener_event);
    close(listener_sock);
    event_base_free(base);

    return 0;
}
"""
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o /tmp/ghm/test_libevent /tmp/ghm/test_libevent.c -levent -lpthread")
        os.system("/tmp/ghm/test_libevent > /tmp/ghm/my.txt 2>&1 &")
        con = pexpect.spawn("telnet localhost 12345")
        con.sendline("hello")
        con.sendline("quit")
        self.cmd("cat /tmp/ghm/my.txt |grep hello")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/ghm")