#! /usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File:      tc_gnupg_gpg_func_002.py
@Time:      2024年02月29日16:06:08
@Author:    Liujiang <wb-lj931787@alibaba-inc.com>
@License:   Mulan PSL v2
@Modified:  Liujiang <wb-lj931787@alibaba-inc.com>
"""

from common.basetest import LocalTest
from common.gpg import Gnupg2Manager

class Test(LocalTest):
    """
    Verify the GnuPG key key import/export function
    See tc_gnupg_gpg_func_002.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    
    PARAM_DIC = {"pkg_name": "gnupg2 expect"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline_generate_script = """cat > export_private_key.sh <<EOF
#!/usr/bin/expect -f
log_user 0
set arg1 [lindex $argv 0]
spawn /usr/bin/bash
expect {
    "# " {send "gpg --export-secret-keys -a \$arg1 > myprivatekey.asc\\r"}
}
expect {
    "Passphrase: " { send "mysecretpassphrase\\r";}
}
expect {
    "# " {send "\\r"}
}
set sensitive_data ""
expect eof
exit
EOF
"""
        self.cmd(cmdline_generate_script)

    def test(self):
        # Generate the key pair
        keypair_id = Gnupg2Manager.generate_fixed_parameter_key_pair(
            self,
            name="John Doe",
            email="johndoe_002@example.com",
            testpasswd="mysecretpassphrase",)
        
        # Export public and private keys of self
        ## Export public
        statu = Gnupg2Manager.export_public_key(
            self, keypair_id, publickey_file="mypublickey.asc"
        )
        self.assertTrue(statu, msg="Export public failed.")

        ## Export private
        self.cmd("chmod +x export_private_key.sh; ./export_private_key.sh %s" %keypair_id)
        code, publickey_res = self.cmd("file mypublickey.asc")
        code, privatekey_res = self.cmd("file myprivatekey.asc")
        self.assertIn("PGP public key block", publickey_res)
        self.assertIn("PGP private key block", privatekey_res)

        # Delete key pair
        keypair_id = Gnupg2Manager.get_keypair_id(self, user="johndoe_002@example.com")
        statu = Gnupg2Manager.delete_key_pair(self, keypair_id, email="johndoe_002@example.com")
        self.assertTrue(statu, msg="Delete keypair failed.")

        # Import public and private key of self
        ## Import public key
        statu = Gnupg2Manager.import_public_key(
            self, publickey_file="mypublickey.asc", email="johndoe_002@example.com"
        )
        self.assertTrue(statu, msg="Import public key failed.")

        ## Import private key
        statu = Gnupg2Manager.import_private_key(
            self, privatekey_file="myprivatekey.asc", email="johndoe_002@example.com"
        )
        self.assertTrue(statu, msg="Import private key failed.")
        Gnupg2Manager.get_keypair_id(self, user="johndoe_002@example.com")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf key_params.txt export_private_key.sh mypublickey.asc myprivatekey.asc")
        keypair_id = Gnupg2Manager.get_keypair_id(self, user="johndoe_002@example.com")
        statu = Gnupg2Manager.delete_key_pair(self, keypair_id, email="johndoe_002@example.com")
        self.assertTrue(statu, msg="Delete keypair failed.")