#! /usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File:      tc_gnupg_gpg_func_007.py
@Time:      2024年02月29日16:06:08
@Author:    Liujiang <wb-lj931787@alibaba-inc.com>
@License:   Mulan PSL v2
@Modified:  Liujiang <wb-lj931787@alibaba-inc.com>
"""

from common.basetest import LocalTest
from common.gpg import Gnupg2Manager

class Test(LocalTest):
    """
    Verify if the default key setting in the gpg.conf file is 
    correctly applied to GPG operations.
    See tc_gnupg_gpg_func_007.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    
    PARAM_DIC = {"pkg_name": "gnupg2 expect"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
#         cmdline_generate_params = """cat >key_params.txt<<EOF
# Key-Type: RSA
# Key-Length: 2048
# Subkey-Type: RSA
# Subkey-Length: 2048
# Name-Real: John Doe
# Name-Email: johndoe@example.com
# Expire-Date: 0
# Passphrase: mysecretpassphrase
# EOF
# """
        cmdline_generate_test_file = """cat >signature.txt<<EOF
This is a test file.
EOF
"""
        cmdline_generate_detached_signature = """cat >detached_signature.sh<<EOF
#!/usr/bin/expect -f
spawn /usr/bin/bash
expect {
    "# " {send "gpg --armor --output signature.txt.sig --detach-sign signature.txt\\r"}
}
expect {
    "Passphrase: " {send "mysecretpassphrase\\r"}
}
expect eof
exit
EOF
"""
        # self.cmd(cmdline_generate_params)
        self.cmd(cmdline_generate_test_file)
        self.cmd(cmdline_generate_detached_signature)

    def test(self):
        # Generate the key pair
        keypair_id = Gnupg2Manager.generate_fixed_parameter_key_pair(
            self,
            name="John Doe",
            email="johndoe_007@example.com",
            testpasswd="mysecretpassphrase",)
               
        # Create signature and assert configuration taking effect.
        code, signature_output = self.cmd(
            "chmod +x detached_signature.sh; ./detached_signature.sh")
        self.assertNotIn("as default secret key for signing" , signature_output)
        self.cmd("rm -rf signature.txt.sig")

        # Update Configuration
        self.cmd("echo default-key %s >> /root/.gnupg/gpg.conf" %keypair_id)
        self.cmd("gpg-connect-agent reloadagent /bye")
        self.cmd("gpgconf --reload gpg-agent")
        
        # Create signature and assert configuration taking effect.
        code, signature_output = self.cmd(
            "chmod +x detached_signature.sh; ./detached_signature.sh")
        self.assertIn("as default secret key for signing" , signature_output)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("sed -i '/default-key/d' /root/.gnupg/gpg.conf")
        self.cmd("gpg-connect-agent reloadagent /bye")
        self.cmd("gpgconf --reload gpg-agent")
        self.cmd("rm -rf key_params.txt signature.txt \
                 detached_signature.sh signature.txt.sig")
        keypair_id = Gnupg2Manager.get_keypair_id(self, 
                    user="johndoe_007@example.com")
        statu = Gnupg2Manager.delete_key_pair(self, keypair_id, 
                    email="johndoe_007@example.com")
        self.assertTrue(statu, msg="Delete keypair failed.")

