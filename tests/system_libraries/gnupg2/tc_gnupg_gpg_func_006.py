#! /usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File:      tc_gnupg_gpg_func_006.py
@Time:      2024年02月29日16:06:08
@Author:    Liujiang <wb-lj931787@alibaba-inc.com>
@License:   Mulan PSL v2
@Modified:  Liujiang <wb-lj931787@alibaba-inc.com>
"""

from common.basetest import LocalTest
from common.gpg import Gnupg2Manager

class Test(LocalTest):
    """
    Test whether the GPG can use a private key for 
    data signing and verify it using a public key.
    See tc_gnupg_gpg_func_006.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    
    PARAM_DIC = {"pkg_name": "gnupg2"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        # Generate the key pair
        keypair_id = Gnupg2Manager.generate_fixed_parameter_key_pair(
            self,
            name="John Doe",
            email="johndoe_006@example.com",
            testpasswd="mysecretpassphrase",)
        
        # Create signature
        ## Detached signature
        self.cmd("echo 'This is a test file' > file.txt")
        statu = Gnupg2Manager.make_detached_signature(
            self,
            user="johndoe_006@example.com",
            testpasswd="mysecretpassphrase",
            signature_file="file.txt.detached.gpg",
            file="file.txt"
        )
        self.assertTrue(statu, msg="make detached signature faild.")

        ## Cleartext Signature
        statu = Gnupg2Manager.make_clearsign_signature(
            self,
            user="johndoe_006@example.com",
            testpasswd="mysecretpassphrase",
            signature_file="file.txt.clearsign.gpg",
            file="file.txt"
        )
        self.assertTrue(statu, msg="make clearsign signature faild.")
        self.cmd("cat file.txt.clearsign.gpg |grep 'This is a test file'")

        # Verify signature
        ## Detached signature
        context = Gnupg2Manager.verify_detached_signature(
            self,
            user="johndoe_006@example.com",
            file="file.txt",
            signature_file="file.txt.detached.gpg",
        )
        self.assertEqual(context[0], 0, msg="verify detached signature failed.")

        ## Cleartext Signature
        context = Gnupg2Manager.verify_cleartext_signature(
            self,
            user="johndoe_006@example.com",
            signature_file="file.txt.clearsign.gpg",
        )
        self.assertEqual(context[0], 0, msg="verify cleartext signature failed.")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf file.txt file.txt.clearsign.gpg file.txt.detached.gpg")
        keypair_id = Gnupg2Manager.get_keypair_id(self, 
                    user="johndoe_006@example.com")
        statu = Gnupg2Manager.delete_key_pair(self, keypair_id, 
                    email="johndoe_006@example.com")
        self.assertTrue(statu, msg="Delete keypair failed.")


