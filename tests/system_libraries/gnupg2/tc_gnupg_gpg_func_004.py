#! /usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File:      tc_gnupg_gpg_func_004.py
@Time:      2024年02月29日16:06:08
@Author:    Liujiang <wb-lj931787@alibaba-inc.com>
@License:   Mulan PSL v2
@Modified:  Liujiang <wb-lj931787@alibaba-inc.com>
"""

from common.basetest import LocalTest
from common.gpg import Gnupg2Manager

class Test(LocalTest):
    """
    Test key addition or removal of user ID and signature functionality
    See tc_gnupg_gpg_func_004.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    
    PARAM_DIC = {"pkg_name": "gnupg2 expect"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline_generate_add_userid_script = """cat >add_userID_script.sh<<EOF
#!/usr/bin/expect -f
set arg1 [lindex \$argv 0]
spawn /usr/bin/bash
expect {
    "# " {send "gpg --edit-key \$arg1 \\r"}
}
expect {
    "gpg> " { send "adduid\\r";}
}
expect {
    "Real name: " {send "Klay Thompson\\r"}
}
expect {
    "Email address: " {send "Thompson@example.com\\r"}
}
expect {
    "Comment: " {send "This is a test\\r"}
}
expect {
    "Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit?" {send "O\\r"}
}
expect {
    "Passphrase: " {send "mysecretpassphrase\\r"}
}
expect {
    "gpg> " {send "save\\r"}
}
expect eof
exit
EOF
"""
        cmdline_generate_remove_sign_script = '''cat >remove_sign_script.sh<<EOF
#!/usr/bin/expect -f
set arg1 [lindex \$argv 0]
spawn /usr/bin/bash
expect {
    "# " {send "gpg --edit-key \$arg1 \\r"}
}
expect {
    "gpg> " { send "uid 1\\r";}
}
expect {
    "gpg> " { send "revsig\\r";}
}
expect {
    "Create a revocation certificate for this signature? (y/N) " { send "y\\r";}
}
expect {
    "Really create the revocation certificates? (y/N) " { send "y\\r";}
}
expect {
    "Your decision? " { send "0\\r";}
}
expect {
    "> " {send "\\r"}
}
expect {
    "Is this okay? (y/N) " {send "y\\r"}
}
expect {
    "Passphrase: " {send "mysecretpassphrase\\r"}
}
expect {
    "gpg> " {send "delsig\\r"}
}
expect {
    "Delete this good signature? (y/N/q)" {send "y\\r"}
}
expect {
    "Really delete this self-signature? (y/N)" {send "y\\r"}
}
expect {
    "Delete this good signature? (y/N/q)" {send "y\\r"}
}
expect {
    "Really delete this self-signature? (y/N)" {send "y\\r"}
}
expect {
    "gpg> " {send "save\\r"}
}
expect eof
exit
EOF
'''
        cmdline_generate_add_sign_script = '''cat >add_sign_script.sh<<EOF
#!/usr/bin/expect -f
set arg1 [lindex \$argv 0]
spawn /usr/bin/bash
expect {
    "# " {send "gpg --edit-key \$arg1 \\r"}
}
expect {
    "gpg> " { send "uid 2\\r";}
}
expect {
    "gpg> " { send "sign\\r";}
}
expect {
    "Really sign? (y/N) " { send "y\\r";}
}
expect {
    "Passphrase: " {send "mysecretpassphrase\\r"}
}
expect {
    "gpg> " {send "save\\r"}
}
expect eof
exit
EOF
'''
        cmdline_generate_remove_userid_script = '''cat >remove_userID_script.sh<<EOF
#!/usr/bin/expect -f
set arg1 [lindex \$argv 0]
spawn /usr/bin/bash
expect {
    "# " {send "gpg --edit-key \$arg1 \\r"}
}
expect {
    "gpg> " { send "uid 1\\r";}
}
expect {
    "gpg> " {send "revuid\\r"}
}
expect {
    "Really revoke this user ID? (y/N) " {send "y\\r"}
}
expect {
    "Your decision? " {send "0\\r"}
}
expect {
    "> " {send "\\r"}
}
expect {
    "Is this okay? (y/N) " {send "y\\r"}
}
expect {
    "Passphrase: " {send "mysecretpassphrase\\r"}
}
expect {
    "gpg> " {send "save\\r"}
}
expect eof
exit
EOF
'''
        self.cmd(cmdline_generate_add_userid_script)
        self.cmd(cmdline_generate_remove_sign_script)
        self.cmd(cmdline_generate_add_sign_script)
        self.cmd(cmdline_generate_remove_userid_script)

    def test(self):
        # Generate the key pair
        keypair_id = Gnupg2Manager.generate_fixed_parameter_key_pair(
            self,
            name="John Doe",
            email="johndoe_004@example.com",
            testpasswd="mysecretpassphrase",)

        # Add user IDs for the key.
        self.cmd("chmod +x add_userID_script.sh; ./add_userID_script.sh %s" %(keypair_id))
        code, list_keys = self.cmd("gpg --list-keys")
        self.assertIn("Klay Thompson", list_keys)
        code, list_secret_keys = self.cmd("gpg --list-secret-keys")
        self.assertIn("Klay Thompson", list_secret_keys)

        # Remove
        code, list_sign = self.cmd("gpg --list-sigs |grep 'Thompson@example.com' -A 1")
        self.assertIn("sig", list_sign)
        self.cmd("chmod +x remove_sign_script.sh; ./remove_sign_script.sh %s" %(keypair_id))
        code, list_sign = self.cmd("gpg --list-sigs |grep 'Thompson@example.com' -A 1")
        self.assertNotIn("sig", list_sign)

        # Add
        self.cmd("chmod +x add_sign_script.sh; ./add_sign_script.sh %s" %(keypair_id))
        code, list_sign = self.cmd("gpg --list-sigs |grep 'Thompson@example.com' -A 1")
        self.assertIn("sig", list_sign)
        
        # remove user IDs for the key.
        self.cmd("chmod +x remove_userID_script.sh; ./remove_userID_script.sh %s" %(keypair_id))
        code, list_keys = self.cmd("gpg --list-keys")
        self.assertNotIn("Klay Thompson", list_keys)
        code, list_secret_keys = self.cmd("gpg --list-secret-keys")
        self.assertNotIn("Klay Thompson", list_secret_keys)
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf key_params.txt add_userID_script.sh remove_userID_script.sh "+
                "remove_sign_script.sh add_sign_script.sh")
        keypair_id = Gnupg2Manager.get_keypair_id(self, 
                    user="johndoe_004@example.com")
        statu = Gnupg2Manager.delete_key_pair(self, keypair_id, 
                    email="johndoe_004@example.com")
        self.assertTrue(statu, msg="Delete keypair failed.")