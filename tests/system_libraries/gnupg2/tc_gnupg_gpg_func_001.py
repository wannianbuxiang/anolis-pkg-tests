#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_gnupg_gpg_func_001.py
@Time:      2024年02月29日16:06:08
@Author:    Liujiang <wb-lj931787@alibaba-inc.com>
@License:   Mulan PSL v2
@Modified:  Liujiang <wb-lj931787@alibaba-inc.com>
"""

from common.basetest import LocalTest
from common.gpg import Gnupg2Manager

class Test(LocalTest):
    """
    Ensure that gnupg2 can successfully generate a new key pair, 
    encrypt the file with the generated public key, 
    and then decrypt the file with the corresponding private key.
    See tc_gnupg_gpg_func_001.yaml for details
    
    :avocado: tags=P1,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "gnupg2"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        # Generate the key pair
        keypair_id = Gnupg2Manager.generate_fixed_parameter_key_pair(
            self,
            name="John Doe",
            email="johndoe_001@example.com",
            testpasswd="mysecretpassphrase",)
        
        # Encrypt and decrypt files
        ## encrypt
        self.cmd("echo 'This is a test' > test.txt")
        statu = Gnupg2Manager.encryption_data(
            self, recipient='johndoe_001@example.com', file="test.txt")
        self.assertTrue(statu, msg="Encryption failed.")

        ## decrypt
        statu = Gnupg2Manager.decryption_data(
            self, decrypted_file="test.txt.gpg", testpasswd="mysecretpassphrase")
        self.assertTrue(statu, msg="Encryption failed.")

        ## Compare whether the file encryption and decryption are 
        ## consistent before and after comparison
        self.cmd("diff test.txt test.txt.gpg.txt")
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf test.txt test.txt.gpg test.txt.gpg.txt ")
        keypair_id = Gnupg2Manager.get_keypair_id(self, user="johndoe_001@example.com")
        statu = Gnupg2Manager.delete_key_pair(self, keypair_id, email="johndoe_001@example.com")
        self.assertTrue(statu, msg="Delete keypair failed.")
