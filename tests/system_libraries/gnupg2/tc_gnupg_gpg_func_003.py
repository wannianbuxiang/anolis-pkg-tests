#! /usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File:      tc_gnupg_gpg_func_003.py
@Time:      2024年02月29日16:06:08
@Author:    Liujiang <wb-lj931787@alibaba-inc.com>
@License:   Mulan PSL v2
@Modified:  Liujiang <wb-lj931787@alibaba-inc.com>
"""

from common.basetest import LocalTest
from datetime import datetime
from common.gpg import Gnupg2Manager

class Test(LocalTest):
    """
    Verify whether gpg can add and update the validity period of keys normally
    See tc_gnupg_gpg_func_003.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    
    PARAM_DIC = {"pkg_name": "gnupg2 expect"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline_generate_script = """cat >update_expire_script.sh<<EOF
#!/usr/bin/expect -f
log_user 0
set arg1 [lindex \$argv 0]
set arg2 [lindex \$argv 1]
spawn /usr/bin/bash
expect {
    "# " {send "gpg --edit-key \$arg1 \\r"}
}
expect {
    "gpg> " { send "expire\\r";}
}
expect {
    "Key is valid for\? \(0\) " {send "\$arg2\\r"}
}
expect {
    "Is this correct\? \(y/N\) " {send "y\\r"}
}
expect {
    "Passphrase: " {send "mysecretpassphrase\\r"}
}
expect {
    "gpg> " {send "save\\r"}
}
set sensitive_data ""
expect eof
exit
EOF
"""
        self.cmd(cmdline_generate_script)

    def test(self):
        # Generate the key pair
        keypair_id = Gnupg2Manager.generate_fixed_parameter_key_pair(
            self,
            name="John Doe",
            email="johndoe_003@example.com",
            testpasswd="mysecretpassphrase",)
        
        # Add the validity period of the key
        expire_value = 1
        coed, create_date = self.cmd(
            "gpg --list-keys johndoe_003@example.com |awk '/pub/ {print $3}'")
        
        self.cmd("chmod +x update_expire_script.sh; ./update_expire_script.sh %s %s" 
                 %(keypair_id, expire_value))
        code, expires_date = self.cmd(
            "gpg --list-keys johndoe_003@example.com |awk -F']|[[:space:]]' '/pub/ {print $9}'")
        datetime1 = datetime.strptime(create_date, "%Y-%m-%d")
        datetime2 = datetime.strptime(expires_date, "%Y-%m-%d")
        date_diff = datetime2 - datetime1
        self.assertEquals(date_diff.days, expire_value)

        # Update the validity period of the key
        expire_value = 2
        self.cmd("./update_expire_script.sh %s %s" %(keypair_id, expire_value))
        code, update_date = self.cmd(
            "gpg --list-keys johndoe_003@example.com |awk -F']|[[:space:]]' '/pub/ {print $9}'")
        datetime3 = datetime.strptime(update_date, "%Y-%m-%d")
        date_diff = datetime3 - datetime2
        self.assertEquals(date_diff.days, expire_value-1)
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf key_params.txt update_expire_script.sh")
        keypair_id = Gnupg2Manager.get_keypair_id(self, user="johndoe_003@example.com")
        statu = Gnupg2Manager.delete_key_pair(self, keypair_id, email="johndoe_003@example.com")
        self.assertTrue(statu, msg="Delete keypair failed.")

