#! /usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File:      tc_gnupg_gpg_func_008.py
@Time:      2024年02月29日16:06:08
@Author:    Liujiang <wb-lj931787@alibaba-inc.com>
@License:   Mulan PSL v2
@Modified:  Liujiang <wb-lj931787@alibaba-inc.com>
"""

from common.basetest import LocalTest
from common.gpg import Gnupg2Manager

class Test(LocalTest):
    """
    Test the trust level functionality of adding and modifying keys
    See tc_gnupg_gpg_func_008.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    
    PARAM_DIC = {"pkg_name": "gnupg2 expect"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline_generate_publickey = """cat >publickey.asc<<EOF
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v2.0.22 (GNU/Linux)

mQENBGXoRIsBCACl9KDdqNqyNlFfvlfPI0EEVXWub6lMgdG8ik2FhoOj8/r9zW+1
Sa9muad8qBjxLs2jEkzWg6AiQWNpUrgdXUrd9sSCq+OHVol/f8RIc11nb2QKd14Z
YBeCXjD+AGfmG24c/yshHKIJ/HkZO8H1XfEsNoI45rKQcr0qtGp5ke221SZMRS0G
p1n7iVL6UQ+gs1sSIGB0FP0rENK6yu5mLDG9dRXUcXb2G9r6ZdNOHtuuZfPoIpMF
PDfWKN7AM8Zz72aljgpuK/SbkOhSmyPUECo15aobp8wePJuB0/gPwzwEQhlzL32s
Bldfy/s6gcIUuDiV3X930oaooV0Q/LNmwJedABEBAAG0FWFubGl1IDxhbmxpdUAx
MjMuY29tPokBOQQTAQIAIwUCZehEiwIbAwcLCQgHAwIBBhUIAgkKCwQWAgMBAh4B
AheAAAoJEPx5R28D/TLeXtUH/2xX+aX1FIwJ23PqpCNFlEXlWXgMXUxHVrmiWLuw
6HXKdf2LBPIIsplNhHugNuy+OwTJOIhCZP/XPVKXrxPSH8ICG/FfcM8naq2OGwzl
Je+Qf6PfWC/nkJ/ewSIYYGYG4fMNa81mYOh9S4wxGOARcYBF3hl0h03et/G55gAC
Yt0PaTH1h6ns1O7/1s0xZ0dh/dikGJawkeS9qyq3Lv+pp/cyrUUNwxesT1CpBmat
ReNlRBiK1phyLUwbJcnaNNerW6l4DpSTpcBtM/9hKcgJZENi0oScpEeerZn6UOyX
awUYReDsScaaISlw94QovvhfOVSQ9ZufAJ1a0SGyW9MNgR65AQ0EZehEiwEIANe0
r898Gaj6C9I16384A/UiyuRbXW0PuCG0W45uVboAJVuayfiidwwlUsYSeD6kBptd
DV9cXUHXnEhQwpkgOwccz+wuJtuv29y0ikCrY8TisU2YvjjZnecwZYzZqLQmRP1S
5dZHuzBel6HXboxd9nIfiBfoHFbPgRMLx4QpkbhIPj+NckfD7KzJ5jNolfyE1RRY
9F7rU74DutnNFMh/mmHvgvz4wozp3RC3/7sQBM4dmbAnx0qG9C1N9bLT2FdCn87x
P+BJNmne/iNK67jd3tp64TvJU7QlGtVWQAFvx4gdmYduLeMJNivVx4gjY8hom0ys
NI8j8vaUkadtFgUUBesAEQEAAYkBHwQYAQIACQUCZehEiwIbDAAKCRD8eUdvA/0y
3jsuB/0exM0Es3SFtzPI+yigicACtbAlAJfkuENhGdnyYjrMMm/CsT8k3r1QN7es
LqO9U4Pn4np5hLIyWpYVuBkJE0E4GGyXhy5Bv+Zm5CKc9rs8K0GFYcr0LLPC3PO8
uNZvk/zRUfmq/JLc2w0an2oCAFYyZh16+Dp4CUlnatg/7rEW10Jkii2GR/d/3IoW
I7dVJ96DEKgZLpz6Xb6zUzKP6rXcVqQ3RkmPiZQ6s32wtzNXLjuccw4+YvkQQATy
Y6IdDZV21VjH77psPq86isNOAbWJuDnSpqnYTbNjEH5ygu+9AFSXjJI6AFjtuaYx
v0JrrsVNPvkoiif4yYHzB1aWDPD0
=hbcR
-----END PGP PUBLIC KEY BLOCK-----

EOF
"""
        cmdline_generate_update_trust_script = '''cat >update_trust_script.sh<<EOF
#!/usr/bin/expect -f
set arg1 [lindex \$argv 0]
spawn /usr/bin/bash
expect {
    "# " {send "gpg --edit-key \$arg1\\r"}
}
expect {
    "gpg> " {send "trust\\r"}
}
expect {
    "Your decision? " {send "5\\r"}
}
expect {
    "Do you really want to set this key to ultimate trust? (y/N) " {send "y\\r"}
}
expect {
    "gpg> " {send "save\\r"}
}
expect eof
exit
EOF
'''
        self.cmd(cmdline_generate_publickey)
        self.cmd(cmdline_generate_update_trust_script)

    def test(self):
        statu = Gnupg2Manager.import_public_key(
            self,
            publickey_file="publickey.asc", 
            email="anliu@123.com")
        self.assertTrue(statu, msg="import public key failed.")

        # Get the key_id of the key pair
        keypair_id = Gnupg2Manager.get_keypair_id(self, user="anliu@123.com")
        
        # Check trust level
        code, list_keys = self.cmd("gpg --list-keys")
        self.assertIn("[ unknown]" , list_keys)
        # Update trust level
        self.cmd("chmod +x update_trust_script.sh; ./update_trust_script.sh %s" %(keypair_id))
        # Check trust level
        code, list_keys = self.cmd("gpg --list-keys")
        self.assertIn("[ultimate]" , list_keys)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf key_params.txt publickey.asc update_trust_script.sh")
        keypair_id = Gnupg2Manager.get_keypair_id(self, 
                    user="anliu@123.com")
        statu = Gnupg2Manager.delete_key_pair(self, keypair_id, 
                    email="anliu@123.com")
        self.assertTrue(statu, msg="Delete keypair failed.")
