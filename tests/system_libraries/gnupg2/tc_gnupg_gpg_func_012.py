#! /usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File:      tc_gnupg_gpg_func_012.py
@Time:      2024年02月29日16:06:08
@Author:    Liujiang <wb-lj931787@alibaba-inc.com>
@License:   Mulan PSL v2
@Modified:  Liujiang <wb-lj931787@alibaba-inc.com>
"""

from common.basetest import LocalTest
from common.gpg import Gnupg2Manager
import random
import time

class Test(LocalTest):
    """
    See tc_gnupg_gpg_func_012.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    
    PARAM_DIC = {"pkg_name": "gnupg2 expect"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline_generate_params = """cat >key_params.txt<<EOF
Key-Type: RSA
Key-Length: 2048
Name-Real: John Doe
Name-Email: johndoe_012@example.com
Expire-Date: 0
Passphrase: mysecretpassphrase
EOF
"""
        cmdline_generate_add_sub_keys_script = """cat >add_sub_keys_script.sh<<EOF
#!/usr/bin/expect -f
log_user 0
set timeout 5
set arg1 [lindex \$argv 0]
set arg2 [lindex \$argv 1]
set arg3 [lindex \$argv 2]
set arg4 [lindex \$argv 3]
spawn /usr/bin/bash
expect {
    "# " {send "gpg --edit-key \$arg1 \\r"}
}
expect {
    "gpg> " { send "addkey\\r";}
}
expect {
    "Your selection? " { send "\$arg2\\r";}
}
expect {
    "Your selection? " { send "\$arg3\\r";}
}
expect {
    "What keysize do you want? " { send "\$arg4\\r";}
}
expect {
    "Key is valid for? (0) " { send "\\r";}
}
expect {
    "Is this correct? (y/N) " { send "y\\r";}
}
expect {
    "Really create? (y/N) " { send "y\\r";}
}
expect {
    "Passphrase: " {send "mysecretpassphrase\\r"}
}
expect {
    "gpg> " { send "save\\r";}
}
expect eof
set sensitive_data ""
exit
EOF
""" 
        cmdline_generate_test_file = """cat >test.txt<<EOF
This is a test file.
EOF
"""
        cmdline_generate_del_sub_keys_script = """cat >del_sub_keys_script.sh<<EOF
#!/usr/bin/expect -f
log_user 0
set timeout 5
set arg1 [lindex \$argv 0]
spawn /usr/bin/bash
expect {
    "# " {send "gpg --edit-key \$arg1 \\r"}
}
expect {
    "gpg> " { send "key 1\\r";}
}
expect {
    "gpg> " { send "delkey\\r";}
}
expect {
    "Do you really want to delete this key? (y/N) " { send "y\\r";}
}
expect {
    "Passphrase: " {send "mysecretpassphrase\\r"}
}
expect {
    "gpg> " { send "save\\r";}
}
set sensitive_data ""
expect eof
exit
EOF
"""
        self.cmd(cmdline_generate_params)
        self.cmd(cmdline_generate_add_sub_keys_script)
        self.cmd(cmdline_generate_del_sub_keys_script)
        self.cmd(cmdline_generate_test_file)

    def test(self):
        # Generate the key pair
        code, gpg_result = self.cmd("gpg --batch --gen-key key_params.txt 2>&1")
        self.assertIn("gpg: revocation certificate stored as", gpg_result)
        code, list_keys = self.cmd("gpg --list-keys")
        self.assertIn("<johndoe_012@example.com>", list_keys)
        code, list_secret_keys = self.cmd("gpg --list-secret-keys")
        self.assertIn("<johndoe_012@example.com>", list_secret_keys)
        # Get the key_id of the key pair
        code, key_id = self.cmd("gpg --list-keys |grep 'johndoe_012@example.com' -B 1 | \
                                awk '/^[[:space:]]/ {print $1}'")
        self.key_id = key_id

        # subkey Summary:
        # (3) DSA (sign only)
        # (4) RSA (sign only)
        # (5) Elgamal (encrypt only)
        # (6) RSA (encrypt only)
        # (10) ECC (sign only)
        #   (1) Curve 25519 *default*
        #   (4) NIST P-384
        #   (6) Brainpool P-256
        # (12) ECC (encrypt only)
        #   (1) Curve 25519 *default*
        #   (4) NIST P-384
        #   (6) Brainpool P-256
        # (14) Existing key from card
        # Note: Error reported for DAS specified key length of 1024:
        # gpg: Note: third-party key signatures using the SHA1 algorithm are rejected
        # gpg: (use option "--allow-weak-key-signatures" to override)
        # gpg: signing failed: Invalid digest algorithm
        # gpg: make_keysig_packet failed for backsig: Invalid digest algorithm
        # gpg: Key generation failed: Invalid digest algorithm
        # debug: https://bugzilla.openanolis.cn/show_bug.cgi?id=8546#c0

        self.subkey_type_lang_map = {
            "DSA": [(3, 0, 1024), (3, 0, 2048), (3, 0, 3072)],
            "RSA": [(4, 0, 1024), (4, 0, 2048), (4, 0, 3072), (4, 0, 4096),
                    (6, 0, 1024), (6, 0, 2048), (6, 0, 3072), (6, 0, 4096)],
            "ElG": [(5, 0, 1024), (5, 0, 2048), (5, 0, 3072)],
            "ECC": [(10, 1, 0), (10, 4, 0), (10, 6, 0),
                    (12, 1, 0), (12, 4, 0), (12, 6,0)],
        }
        # self.subkey_type_lang_map = {
        #     "ElG": [(5, 0, 3072)],
        # }
        # Construct random parameters
        element = lambda gen_map : random.choice(list(gen_map.keys())) 
        subkey_type = element(self.subkey_type_lang_map)
        subkey_context = random.choice(self.subkey_type_lang_map.get(subkey_type))

        # add sub keys
        self.cmd("chmod +x add_sub_keys_script.sh;" +
                 "./add_sub_keys_script.sh {} {} {} {}".format(
                    key_id, subkey_context[0], 
                    subkey_context[1], int(subkey_context[2]))
                )
        if subkey_context[2] != 0: 
            check_code = "%s%s"%(subkey_type.lower(), subkey_context[2]) 
        else:
            if subkey_context[1] == 1: check_code = "25519"
            if subkey_context[1] == 4: check_code = "nistp384"
            if subkey_context[1] == 6: check_code = "brainpoolP256r1"
        code, list_sub_keys = self.cmd(
            "gpg --keyid-format long --list-keys |grep 'sub'", 
            ignore_status=True)
        max_retries = 20
        initial_delay = 1
        backoff_factor = 1.5
        for attempt in range(max_retries):
            try:
                if list_sub_keys == "":
                    attempt += 1
                    self.cmd("chmod +x add_sub_keys_script.sh;" +
                 "./add_sub_keys_script.sh {} {} {} {}".format(
                    key_id, subkey_context[0], 
                    subkey_context[1], int(subkey_context[2]), ignore_status=True)
                    )
                    code, list_sub_keys = self.cmd(
                        "gpg --keyid-format long --list-keys |grep 'sub'", 
                        ignore_status=True)
                    time.sleep(initial_delay * (backoff_factor ** attempt))
                else:
                    break
            except Exception as e:
                self.log.info(f"Attempt {attempt} failed: {e}")
                time.sleep(initial_delay * (backoff_factor ** attempt)) 
        self.assertIn(check_code, list_sub_keys, msg="Failed to add sub key")

        # Verify sub key capability
        ## get sub key id
        subkey_id = Gnupg2Manager.get_subkey_id(self, 
                                    user="johndoe_012@example.com")
        ## get sub key capability identification
        code, subkey_capability_id = self.cmd(
            "gpg --with-colons --list-keys johndoe_012@example.com "+
            "|awk -F':' '/sub/ {print $12}'"
        )
        ## data encryption
        self.log.info("Execute encrypted data")
        self.cmd("echo 'This is a test file' > test.txt")
        statu = Gnupg2Manager.encryption_data(
            self, recipient=subkey_id, file="test.txt")
        if "e" in subkey_capability_id:
            self.assertTrue(statu, msg="Data encryption failed")
        else:
            self.assertFalse(statu, 
                msg="Key does not have encryption function, expected failure")
        self.log.info("Data encryption successfully")

        ## signature
        self.log.info("Execute signature data")
        statu = Gnupg2Manager.make_signature(self,
                pubkeyid=subkey_id,
                testpasswd="mysecretpassphrase",
                file="test.txt",
                signature_file="test.txt.asc")
        if "s" in subkey_capability_id:
            self.assertTrue(statu, msg="Data signature failed")
        else:
            self.assertFalse(statu, 
                msg="Key does not have signature function, expected failure")
        self.log.info("Data signature successfully")
        
        # delete sub key
        self.cmd("chmod +x del_sub_keys_script.sh;"+
                 "./del_sub_keys_script.sh %s" %subkey_id)
        code, sub_message = self.cmd(
            "gpg --with-colons --list-keys johndoe_012@example.com")
        self.assertNotIn("sub", sub_message)
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf key_params.txt add_sub_keys_script.sh " +
                 "del_sub_keys_script.sh test.txt test.txt.gpg test.txt.asc")
        statu = Gnupg2Manager.delete_key_pair(self, self.key_id, 
                    email="johndoe_012@example.com")
        self.assertTrue(statu, msg="Delete keypair failed.")
