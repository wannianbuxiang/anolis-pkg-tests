#! /usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File:      tc_gnupg_gpg_func_011.py
@Time:      2024年02月29日16:06:08
@Author:    Liujiang <wb-lj931787@alibaba-inc.com>
@License:   Mulan PSL v2
@Modified:  Liujiang <wb-lj931787@alibaba-inc.com>
"""

from common.basetest import LocalTest
from common.gpg import Gnupg2Manager
import random
import time

class Test(LocalTest):
    """
    See tc_gnupg_gpg_func_011.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    
    PARAM_DIC = {"pkg_name": "gnupg2 expect"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline_generate_add_keypair_script = """cat >add_keypair_script.sh<<EOF
#!/usr/bin/expect -f
log_user 0
set timeout 5
set arg1 [lindex \$argv 0]
set arg2 [lindex \$argv 1]
set arg3 [lindex \$argv 2]
spawn /usr/bin/bash
expect {
    "# " {send "gpg --full-generate-key\\r"}
}
expect {
    "Your selection? " { send "\$arg1\\r";}
}
expect {
    "Your selection? " { send "\$arg2\\r";}
}
expect {
    "What keysize do you want? " { send "\$arg3\\r";}
}
expect {
    "Key is valid for? (0) " { send "\\r";}
}
expect {
    "Is this correct? (y/N) " { send "y\\r";}
}
expect {
    "Real name: " { send "John Doe\\r";}
}
expect {
    "Email address: " { send "johndoe_011@example.com\\r";}
}
expect {
    "Comment: " { send "\\r";}
}
expect {
    "Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit? " { send "O\\r";}
}
expect {
    "Passphrase: " {send "mysecretpassphrase\\r"}
}
expect {
    "<Take this one anyway>" {send "\\r"}
}
expect {
    "Passphrase: " {send "mysecretpassphrase\\r"}
}
set sensitive_data ""
expect eof
exit
EOF
"""
        self.cmd(cmdline_generate_add_keypair_script)

    def test(self):
        # subkey Summary:
        # (1) RSA and RSA
        # (2) DSA and Elgamal
        # (3) DSA (sign only)
        # (4) RSA (sign only)
        # (9) ECC (sign and encrypt) *default*
            # (1) Curve 25519 *default*
            # (4) NIST P-384
            # (6) Brainpool P-256
        # (10) ECC (sign only)
            # (1) Curve 25519 *default*
            # (4) NIST P-384
            # (6) Brainpool P-256
        # Note: Error reported for DAS specified key length of 1024:
        # gpg: Note: third-party key signatures using the SHA1 algorithm are rejected
        # gpg: (use option "--allow-weak-key-signatures" to override)
        # gpg: signing failed: Invalid digest algorithm
        # gpg: make_keysig_packet failed for backsig: Invalid digest algorithm
        # gpg: Key generation failed: Invalid digest algorithm
        # debug: https://bugzilla.openanolis.cn/show_bug.cgi?id=8546#c0
        self.keypair_type_lang_map = {
            "RSA": [(1, 0, 1024), (1, 0, 2048), (1, 0, 3072), (1, 0, 4096),
                    (4, 0, 1024), (4, 0, 2048), (4, 0, 3072), (4, 0, 4096)], 
            "DSA": [(2, 0, 1024), (2, 0, 2048), (2, 0, 3072), 
                    (3, 0, 1024), (3, 0, 2048), (3, 0, 3072)],
            "ECC": [(9, 1, 0), (9, 4, 0), (9, 6, 0), 
                    (10, 1,0), (10, 4, 0), (10, 6, 0)]
            }
        
        # Construct random parameters
        element = lambda gen_map : random.choice(list(gen_map.keys())) 
        keypair_type = element(self.keypair_type_lang_map)
        # keypair_type = random.choice(list(self.keypair_type_lang_map.keys()))
        # for keypair_context in self.keypair_type_lang_map.get(keypair_type):
        keypair_context = random.choice(
            self.keypair_type_lang_map.get(keypair_type))

        # Skip DSA with 1024 key length
        if keypair_type == "DSA" and keypair_context[2] == 1024:
            self.skip("Skipping DSA with 1024 key length due to known issues:https://bugzilla.openanolis.cn/show_bug.cgi?id=8546#c0")
            return
        
        # Generate Keypair
        self.cmd(
            "chmod +x add_keypair_script.sh;" +
            "./add_keypair_script.sh {} {} {}".format(
            keypair_context[0], keypair_context[1], int(keypair_context[2]))
            )
        if keypair_context[2] != 0:
            check_code = "%s%s"%(keypair_type.lower(), keypair_context[2]) 
        else:
            if keypair_context[1] == 1: check_code = "25519"
            if keypair_context[1] == 4: check_code = "nistp384"
            if keypair_context[1] == 6: check_code = "brainpoolP256r1"

        self.log.info("generate a keypair, type: %s, length: %s" 
            %(keypair_type, keypair_context[2] if keypair_context[2] else check_code))
        
        code, list_sub_keys = self.cmd(
            "gpg --keyid-format long --list-keys |grep 'pub'", ignore_status=True
            )
        max_retries = 20
        initial_delay = 1
        backoff_factor = 1.5
        for attempt in range(max_retries):
            try:
                if list_sub_keys == "":
                    attempt += 1
                    self.cmd(
                    "chmod +x add_keypair_script.sh;" +
                    "./add_keypair_script.sh {} {} {}".format(
                    keypair_context[0], keypair_context[1], int(keypair_context[2]), ignore_status=True
                    ))
                    code, list_sub_keys = self.cmd(
                        "gpg --keyid-format long --list-keys |grep 'pub'", 
                        ignore_status=True)
                    time.sleep(initial_delay * (backoff_factor ** attempt))
                else:
                    break
            except Exception as e:
                self.log.info(f"Attempt {attempt} failed: {e}")
                time.sleep(initial_delay * (backoff_factor ** attempt))
        self.assertIn(check_code, list_sub_keys, msg="Failed to generate keypair")
        ## Get the key_id of the key pair
        Gnupg2Manager.get_keypair_id(self, user="johndoe_011@example.com")

        ## generate fiexed parmeter key pair
        test_keypair_id = Gnupg2Manager.generate_fixed_parameter_key_pair(
            self,
            name="John Doe",
            email="other@example.com",
            testpasswd="mysecretpassphrase",)
        self.log.info("test keypair id is: %s" %test_keypair_id)

        # Verify pub key capability
        ## get pub_id
        pubkey_id = Gnupg2Manager.get_pubkey_id(self, user="johndoe_011@example.com")
        self.log.info("public key id is: %s" %pubkey_id)
        
        ## get pub_id of test
        test_pubkey_id = Gnupg2Manager.get_pubkey_id(self, user="other@example.com")
        self.log.info("test public key id is: %s" %test_pubkey_id)
        
        ## get pub key capability identification
        code, pubkey_capability_id = self.cmd(
            "gpg --with-colons --list-keys johndoe_011@example.com " +
            "|awk -F':' '/pub/ {print $12}'"
            )
        self.log.info("public capability id is: %s" %pubkey_capability_id)

        ## data encryption
        self.log.info("Execute encrypted data")
        self.cmd("echo 'This is a test file' > test.txt")
        statu = Gnupg2Manager.encryption_data(self, recipient=pubkey_id, file="test.txt")
        if "e" in pubkey_capability_id:
            self.assertTrue(statu, msg="Data encryption failed")
        else:
            self.assertFalse(statu, msg="Key does not have encryption function, expected failure")
        self.log.info("Data encryption successfully")

        ## signature
        self.log.info("Execute signature data")
        statu = Gnupg2Manager.make_signature(self,
                pubkeyid=pubkey_id,
                testpasswd="mysecretpassphrase",
                file="test.txt",
                signature_file="test.txt.asc")
        if "s" in pubkey_capability_id:
            self.assertTrue(statu, msg="Data signature failed")
        else:
            self.assertFalse(statu, msg="Key does not have signature function, expected failure")
        self.log.info("Data signature successfully")

        ## certify
        self.log.info("Execute certify keys")
        context = Gnupg2Manager.sign_other_keys(self,
            pubkeyid=pubkey_id,
            other_pubkeyid=test_pubkey_id,
            testpasswd="mysecretpassphrase")
        if "c" in pubkey_capability_id:
            self.assertEqual(context[0], 0,  msg="Certliy other keys failed")
        else:
            self.assertNotEqual(context[0], 0,  
                msg="Key does not have certliy function, expected failure")
        self.log.info("Certliy other keys successfully")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf add_keypair_script.sh test.txt test.txt..gpg test.txt.asc")
        keypair_id = Gnupg2Manager.get_keypair_id(self, 
                    user="johndoe_011@example.com")
        statu = Gnupg2Manager.delete_key_pair(self, keypair_id, 
                    email="johndoe_011@example.com")
        self.assertTrue(statu, msg="Delete keypair failed.")
        other_keypair_id = Gnupg2Manager.get_keypair_id(self, 
                    user="other@example.com")
        statu = Gnupg2Manager.delete_key_pair(self, other_keypair_id, 
                    email="other@example.com")
        self.assertTrue(statu, msg="Delete keypair failed.")
