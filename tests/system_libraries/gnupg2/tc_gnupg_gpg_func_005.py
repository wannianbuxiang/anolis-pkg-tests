#! /usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File:      tc_gnupg_gpg_func_005.py
@Time:      2024年02月29日16:06:08
@Author:    Liujiang <wb-lj931787@alibaba-inc.com>
@License:   Mulan PSL v2
@Modified:  Liujiang <wb-lj931787@alibaba-inc.com>
"""

from common.basetest import LocalTest
from common.gpg import Gnupg2Manager

class Test(LocalTest):
    """
    Test revocation key and generate revocation certificate functionality
    See tc_gnupg_gpg_func_005.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    
    PARAM_DIC = {"pkg_name": "gnupg2 expect"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        # Generate the key pair
        keypair_id = Gnupg2Manager.generate_fixed_parameter_key_pair(
            self,
            name="John Doe",
            email="johndoe_005@example.com",
            testpasswd="mysecretpassphrase",)

        # Generate Revocation Certificate
        statu = Gnupg2Manager.generate_revocation_certificate(
            self,
            user="johndoe_005@example.com",
            certificate_file="revoke_certificate.crs"
        )
        self.assertTrue(statu, msg="Generate revocation failed.")

        # Revoke Key
        code, list_keys = self.cmd("gpg --list-secret-keys")
        self.assertNotIn("revoked:", list_keys)
        statu = Gnupg2Manager.revoke_key_with_certificate(
            self,
            certificate="revoke_certificate.crs"
        )
        self.assertTrue(statu, msg="revoke key failed.")

        # signature file
        self.cmd("echo 'This is a test file' > file.txt")
        statu = Gnupg2Manager.make_detached_signature(
            self,
            user="johndoe_005@example.com",
            testpasswd="mysecretpassphrase",
            signature_file="file.txt.gpg",
            file="file.txt"
        )
        self.assertFalse(statu,
            msg="The expected result is that the key has been revoked and" +
            "the file signature has failed.")
        self.log.info("Key revocation and file signing failure are expected.")
 
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf key_params.txt generate_certificate.sh \
                 revoke_key_script.sh revoke_certificate.crs")
        keypair_id = Gnupg2Manager.get_keypair_id(self, 
                    user="johndoe_005@example.com")
        statu = Gnupg2Manager.delete_key_pair(self, keypair_id, 
                    email="johndoe_005@example.com")
        self.assertTrue(statu, msg="Delete keypair failed.")