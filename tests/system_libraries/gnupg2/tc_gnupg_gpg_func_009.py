#! /usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File:      tc_gnupg_gpg_func_009.py
@Time:      2024年02月29日16:06:08
@Author:    Liujiang <wb-lj931787@alibaba-inc.com>
@License:   Mulan PSL v2
@Modified:  Liujiang <wb-lj931787@alibaba-inc.com>
"""

from common.basetest import LocalTest
from common.gpg import Gnupg2Manager

class Test(LocalTest):
    """
    Test whether the signature can be correctly verified 
    after importing the public key
    See tc_gnupg_gpg_func_009.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    
    PARAM_DIC = {"pkg_name": "gnupg2"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline_generate_publickey = """cat >publickey.asc<<EOF
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v2.0.22 (GNU/Linux)

mQENBGXoRIsBCACl9KDdqNqyNlFfvlfPI0EEVXWub6lMgdG8ik2FhoOj8/r9zW+1
Sa9muad8qBjxLs2jEkzWg6AiQWNpUrgdXUrd9sSCq+OHVol/f8RIc11nb2QKd14Z
YBeCXjD+AGfmG24c/yshHKIJ/HkZO8H1XfEsNoI45rKQcr0qtGp5ke221SZMRS0G
p1n7iVL6UQ+gs1sSIGB0FP0rENK6yu5mLDG9dRXUcXb2G9r6ZdNOHtuuZfPoIpMF
PDfWKN7AM8Zz72aljgpuK/SbkOhSmyPUECo15aobp8wePJuB0/gPwzwEQhlzL32s
Bldfy/s6gcIUuDiV3X930oaooV0Q/LNmwJedABEBAAG0FWFubGl1IDxhbmxpdUAx
MjMuY29tPokBOQQTAQIAIwUCZehEiwIbAwcLCQgHAwIBBhUIAgkKCwQWAgMBAh4B
AheAAAoJEPx5R28D/TLeXtUH/2xX+aX1FIwJ23PqpCNFlEXlWXgMXUxHVrmiWLuw
6HXKdf2LBPIIsplNhHugNuy+OwTJOIhCZP/XPVKXrxPSH8ICG/FfcM8naq2OGwzl
Je+Qf6PfWC/nkJ/ewSIYYGYG4fMNa81mYOh9S4wxGOARcYBF3hl0h03et/G55gAC
Yt0PaTH1h6ns1O7/1s0xZ0dh/dikGJawkeS9qyq3Lv+pp/cyrUUNwxesT1CpBmat
ReNlRBiK1phyLUwbJcnaNNerW6l4DpSTpcBtM/9hKcgJZENi0oScpEeerZn6UOyX
awUYReDsScaaISlw94QovvhfOVSQ9ZufAJ1a0SGyW9MNgR65AQ0EZehEiwEIANe0
r898Gaj6C9I16384A/UiyuRbXW0PuCG0W45uVboAJVuayfiidwwlUsYSeD6kBptd
DV9cXUHXnEhQwpkgOwccz+wuJtuv29y0ikCrY8TisU2YvjjZnecwZYzZqLQmRP1S
5dZHuzBel6HXboxd9nIfiBfoHFbPgRMLx4QpkbhIPj+NckfD7KzJ5jNolfyE1RRY
9F7rU74DutnNFMh/mmHvgvz4wozp3RC3/7sQBM4dmbAnx0qG9C1N9bLT2FdCn87x
P+BJNmne/iNK67jd3tp64TvJU7QlGtVWQAFvx4gdmYduLeMJNivVx4gjY8hom0ys
NI8j8vaUkadtFgUUBesAEQEAAYkBHwQYAQIACQUCZehEiwIbDAAKCRD8eUdvA/0y
3jsuB/0exM0Es3SFtzPI+yigicACtbAlAJfkuENhGdnyYjrMMm/CsT8k3r1QN7es
LqO9U4Pn4np5hLIyWpYVuBkJE0E4GGyXhy5Bv+Zm5CKc9rs8K0GFYcr0LLPC3PO8
uNZvk/zRUfmq/JLc2w0an2oCAFYyZh16+Dp4CUlnatg/7rEW10Jkii2GR/d/3IoW
I7dVJ96DEKgZLpz6Xb6zUzKP6rXcVqQ3RkmPiZQ6s32wtzNXLjuccw4+YvkQQATy
Y6IdDZV21VjH77psPq86isNOAbWJuDnSpqnYTbNjEH5ygu+9AFSXjJI6AFjtuaYx
v0JrrsVNPvkoiif4yYHzB1aWDPD0
=hbcR
-----END PGP PUBLIC KEY BLOCK-----
EOF
"""
        cmdline_generate_test_file = """cat >document.txt<<EOF
This is a test
EOF
"""

        cmdline_generate_signature_file = """cat >document.txt.sig<<EOF
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v2.0.22 (GNU/Linux)

iQEcBAABAgAGBQJl79C6AAoJEPx5R28D/TLeY4cH/jft3Px5mkxLplXkBu9wSOmI
DWrkBw/FTO9HfEI+Gx/G2bpsInRnZmUkkLFMxMGGs2JWUos4WVgXhiDTzBANbmrY
Iac1ULMGNwfx5WOh++xGyfQ7g4QCeTwCy0NiWde9PDGpzr6G/iZnMRNLE2ptWmNT
TVwOqRtjsUnAkEy4Z/9uZVA0J+KnUb9bctqhxdACZ/REISKA7PnebkzBhQ/zXMeE
69wH+XtXFFUO0hJE3nXavp2arSUN5NSOWoXvaldsMpO37mvXtsu10CGre8a9kmrF
Qy0/korQCvqJfDLnG7qR0yUnIC9WgkW9pir/CLjCdsMLK1dQ02MwqVWll5bKVCQ=
=jzel
-----END PGP SIGNATURE-----
EOF
"""
        self.cmd(cmdline_generate_publickey)
        self.cmd(cmdline_generate_test_file)
        self.cmd(cmdline_generate_signature_file)

    def test(self):
        statu = Gnupg2Manager.import_public_key(
            self,
            publickey_file="publickey.asc", 
            email="anliu@123.com")

        # Verify signature
        context = Gnupg2Manager.verify_detached_signature(self,
                user="anliu@123.com",
                file="document.txt",
                signature_file="document.txt.sig",
                )
        self.assertEqual(context[0], 0, 
                         msg="verify detached signature failed.")

        # Tampering the original file
        self.cmd("sed -i 's/$/./' document.txt")

        # Verify signature
        context = Gnupg2Manager.verify_detached_signature(self,
                user="anliu@123.com",
                file="document.txt",
                signature_file="document.txt.sig",
                ignore_status=True
                )
        self.assertNotEqual(context[0], 0, 
                msg="Tampering the signature original file," +
                            " expected verification result failed")
        self.assertIn('BAD signature from "anliu <anliu@123.com>"', context[1],
                msg="The verification result does not meet expectations, " +
                      "please manually check")
        
        # ampering signature files
        self.cmd("sed -i 's/i/1/' document.txt.sig")
        # Verify signature
        context = Gnupg2Manager.verify_detached_signature(self,
                user="anliu@123.com",
                file="document.txt",
                signature_file="document.txt.sig",
                ignore_status=True
                )
        self.assertNotEqual(context[0], 0, 
                msg="Tampering the signature file," +
                            " expected verification result failed.")
        self.assertIn('the signature could not be verified', context[1],
                msg="The result does not meet expectations, " +
                      "please manually check")
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf publickey.asc document.txt document.txt.sig")
        keypair_id = Gnupg2Manager.get_keypair_id(self, 
                    user="anliu@123.com")
        statu = Gnupg2Manager.delete_key_pair(self, keypair_id, 
                    email="anliu@123.com")
        self.assertTrue(statu, msg="Delete keypair failed.")

