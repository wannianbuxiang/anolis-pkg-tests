#! /usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File:      tc_gnupg_gpg_func_010.py
@Time:      2024年02月29日16:06:08
@Author:    Liujiang <wb-lj931787@alibaba-inc.com>
@License:   Mulan PSL v2
@Modified:  Liujiang <wb-lj931787@alibaba-inc.com>
"""

from common.basetest import LocalTest
from common.gpg import Gnupg2Manager

class Test(LocalTest):
    """
    Test password protected message encryption and decryption
    See tc_gnupg_gpg_func_010.yaml for details

    :avocado: tags=P1,noarch,local,fix
    """
    
    PARAM_DIC = {"pkg_name": "gnupg2"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        self.cmd("echo 'This is a test message.' > testmessage.txt")
        
        # Symmetric encryption of messages
        context = Gnupg2Manager.symmetric_encryption_messages(self,
                messages_file="testmessage.txt",
                testpasswd="mysecretpassphrase"
                )
        self.assertEqual(context[0], 0, 
                msg="Asynchronous message encryption failed")
        code, encryption_file = self.cmd("file testmessage.txt.gpg")
        self.assertIn('PGP symmetric key encrypted data', encryption_file,
                      msg="The encrypted data has not been generated," +
                      " please manually check")

        # Attempt to decrypt message
        context = Gnupg2Manager.symmetric_decrypt_messages(self,
                encryption_messages_file="testmessage.txt.gpg",
                decrypt_messages_file="decryptedmessage.txt",
                testpasswd="mysecretpassphrase"
                )
        # self.cmd("gpg --batch --yes --passphrase 'mysecretpassphrase' \
        #          --decrypt testmessage.txt.gpg > decryptedmessage.txt")
        code, decryption_file = self.cmd("file decryptedmessage.txt")
        self.assertIn('ASCII text', decryption_file)

        # Compare decryption results
        self.cmd("diff testmessage.txt decryptedmessage.txt")
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf testmessage.txt decryptedmessage.txt testmessage.txt.gpg")