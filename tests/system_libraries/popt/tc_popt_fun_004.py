# -*- encoding: utf-8 -*-

"""
@File:      tc_popt_fun_004.py
@Time:      2024-02-27 09:12:00
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_popt_fun_004.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "popt popt-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline='''cat > popt_fun_004.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include <popt.h>

int main(int argc, const char **argv) {
    int intOption = 0;
    
    struct poptOption optionsTable[] = {
        { "int", 'i', POPT_ARG_INT, &intOption, 0, "An integer option", "INT" },
        POPT_AUTOHELP
        { NULL, 0, 0, NULL, 0, NULL, NULL }
    };

    poptContext optCon = poptGetContext(NULL, argc, argv, optionsTable, 0);
    int rc;

    // 注意,添加了POPT_AUTOHELP后,不需要手动处理--help或-h参数。
    if ((rc = poptGetNextOpt(optCon)) < -1) {
        fprintf(stderr, "Argument Error: %s\\n", poptStrerror(rc));
        poptFreeContext(optCon);
        return 1;
    }

    poptFreeContext(optCon);
    return 0;
}
EOF
'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o popt_fun_004 popt_fun_004.c -lpopt")
        code, result = self.cmd("file popt_fun_004")
        self.assertIn("executable", result)
        code, result = self.cmd("./popt_fun_004 --help") 
        self.assertIn("Usage: popt", result)
        code, result = self.cmd("./popt_fun_004 -?")
        self.assertIn("Usage: popt", result)
        code, result = self.cmd("./popt_fun_004 --usage")  
        self.assertIn("Usage: popt", result)
        code, result = self.cmd("./popt_fun_004 -h", ignore_status=True)  
        self.assertIn("Argument Error: unknown option", result)


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf popt_fun_004 popt_fun_004.c")