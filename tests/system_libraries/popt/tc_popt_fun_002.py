# -*- encoding: utf-8 -*-

"""
@File:      tc_popt_fun_002.py
@Time:      2024-02-26 11:09:55
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_popt_fun_002.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "popt popt-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline='''cat > popt_fun_002.c <<EOF
#include <stdio.h>
#include <popt.h>

int main(int argc, char *argv[]) {
    int versionFlag = 0; // 标志位，指示是否显示版本信息
    poptContext ctx;     // popt 库的上下文

    struct poptOption options[] = {
        { "version", 'v', POPT_ARG_NONE, &versionFlag, 0, "Show version information", NULL },
        POPT_AUTOHELP
        { NULL, 0, 0, NULL, 0, NULL, NULL }
    };

    // 用 NULL 终止的 argv 和 options 初始化上下文
    ctx = poptGetContext(NULL, argc, (const char**)argv, options, 0);
    int rc;

    // 处理选项并更新标志位
    while ((rc = poptGetNextOpt(ctx)) >= 0) {
    }

    // 如果发生错误输出错误信息
    if (rc < -1) {
        fprintf(stderr, "%s: %s\\n", poptBadOption(ctx, POPT_BADOPTION_NOALIAS), poptStrerror(rc));
        return 1;
    }

    // 根据标志位输出版本
    if (versionFlag) {
        printf("Version: 1.0.0\\n");
    }

    // 清理 popt 上下文
    poptFreeContext(ctx);
    return 0;
}
EOF
'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o popt_fun_002 popt_fun_002.c -lpopt")
        code, result = self.cmd("file popt_fun_002")
        self.assertIn("executable", result)
        code, result = self.cmd("./popt_fun_002 --version")
        self.assertIn("Version: 1.0.0", result)
        code, result = self.cmd("./popt_fun_002 -v")
        self.assertIn("Version: 1.0.0", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf popt_fun_002 popt_fun_002.c")
