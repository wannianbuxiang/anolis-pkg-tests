# -*- encoding: utf-8 -*-

"""
@File:      tc_popt_fun_005.py
@Time:      2024-02-27 11:09:00
@Author:    qintingting
@Version:   1.0
@Contact:   wb-qtt862918@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    qintingting
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_popt_fun_005.yaml for details

    :avocado: tags=P2,noarch,local,fixed
    """
    PARAM_DIC = {"pkg_name": "popt popt-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline='''cat > popt_fun_005.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include <popt.h>

// 枚举定义
typedef enum { DEFAULT, RED, GREEN, BLUE } Color;

int main(int argc, char *argv[]) {
    int intOption = 42;        // 默认整数值
    float floatOption = 3.14f; // 默认浮点数值
    double doubleOption = 2.71828; // 默认双精度值
    Color colorOption = DEFAULT; // 默认枚举值
    const char *strOption = "default"; // 默认字符串值

    struct poptOption colorOptions[] = {
        { "default", 'd', POPT_ARG_VAL, &colorOption, DEFAULT, "Default color", "Default" },
        { "red", 'r', POPT_ARG_VAL, &colorOption, RED, "Red color", "Red" },
        { "green", 'g', POPT_ARG_VAL, &colorOption, GREEN, "Green color", "Green" },
        { "blue", 'b', POPT_ARG_VAL, &colorOption, BLUE, "Blue color", "Blue" },
        POPT_TABLEEND
    };

    struct poptOption optionsTable[] = {
        { "integer", 'i', POPT_ARG_INT, &intOption, 0, "An integer option", "INT" },
        { "float", 'f', POPT_ARG_FLOAT, &floatOption, 0, "A float option", "FLOAT" },
        { "double", 'D', POPT_ARG_DOUBLE, &doubleOption, 0, "A double option", "DOUBLE" },
        { "string", 's', POPT_ARG_STRING, &strOption, 0, "A string option", "STRING" },
        { "color", 'c', POPT_ARG_INCLUDE_TABLE, colorOptions, 0, "Choose a color", "COLOR" },
        POPT_AUTOHELP
        POPT_TABLEEND
    };

    poptContext optCon;
    int rc;

    // 初始化 popt 上下文
    optCon = poptGetContext(NULL, argc, (const char **)argv, optionsTable, 0);
    if ((rc = poptGetNextOpt(optCon)) < -1) {
        fprintf(stderr, "%s: %s\\n", poptBadOption(optCon, POPT_BADOPTION_NOALIAS), poptStrerror(rc));
        poptFreeContext(optCon);
        return 1;
    }

    printf("Integer option: %d\\n", intOption);
    printf("Float option: %f\\n", floatOption);
    printf("Double option: %lf\\n", doubleOption);
    printf("String option: %s\\n", strOption);
    printf("Color option: %s\\n", 
           colorOption == DEFAULT ? "DEFAULT" :
           colorOption == RED ? "RED" :
           colorOption == GREEN ? "GREEN" :
           "BLUE");

    poptFreeContext(optCon);
    return 0;
}
EOF
'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o popt_fun_005 popt_fun_005.c -lpopt")
        code, result = self.cmd("file popt_fun_005")
        self.assertIn("executable", result)
        code, result = self.cmd("./popt_fun_005")
        if code == 0:
            print("命令解析成功")
        else:
            raise Exception("命令解析失败")
        self.assertIn("Integer option: 42", result)
        self.assertIn("Float option: 3.140000", result)
        self.assertIn("Double option: 2.718280", result)
        self.assertIn("String option: default", result)
        self.assertIn("Color option: DEFAULT", result)


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf popt_fun_005 popt_fun_005.c")