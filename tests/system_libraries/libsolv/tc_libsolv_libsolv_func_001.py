#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libsolv_libsolv_func_001.py
@Time:      2024/03/04 11:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libsolv_libsolv_func_001.yaml for details
    :avocado: tags=P3,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libsolv libsolv-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > libsolv_pool_get_rootdir_test.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <solv/pool.h>

int main() {
    Pool *pool = pool_create();
    pool_set_rootdir(pool, "/tmp");

    // 获取目录
    const char *rootdir = pool_get_rootdir(pool);
    if (rootdir != NULL) {
        printf("The root directory is set to: %s\n", rootdir);
    } else {
        printf("No root directory is set.\n");
    }

    // 清理资源
    pool_free(pool);
    return 0;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o libsolv_pool_get_rootdir_test libsolv_pool_get_rootdir_test.c `pkg-config --cflags --libs libsolv`")

    def test(self):
        self.cmd("./libsolv_pool_get_rootdir_test")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['libsolv_pool_get_rootdir_test.c', 'libsolv_pool_get_rootdir_test']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")
