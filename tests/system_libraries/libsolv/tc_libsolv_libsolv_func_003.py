#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libsolv_libsolv_func_003.py
@Time:      2024/03/04 11:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libsolv_libsolv_func_003.yaml for details
    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libsolv libsolv-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_libsolv_repo_search.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <solv/pool.h>
#include <solv/repo.h>
#include <solv/repodata.h>
#include <solv/solvable.h>

// 回调函数，每找到一个匹配的软件包就会被调用
int search_callback(void *cbdata, Solvable *s, Repodata *data, Repokey *key, KeyValue *kv) {
    // 增加计数器
    int *found_count = (int *)cbdata;
    (*found_count)++;

    printf("Found package: %s-%s\n", pool_id2str(s->repo->pool, s->name), pool_id2str(s->repo->pool, s->evr));
    return 0; // 返回0继续搜索
}

// 创建一个具有假数据的仓库
Repo *create_test_repo(Pool *pool) {
    Repo *repo = repo_create(pool, "test_repo");
    Repodata *data = repo_add_repodata(repo, 0);

    // 添加假的软件包到仓库
    for (int i = 0; i < 10; ++i) {
        Solvable *s = pool_id2solvable(pool, repo_add_solvable(repo));
        char name[32];
        sprintf(name, "package%d", i);
        s->name = pool_str2id(pool, name, 1);
        s->evr = pool_str2id(pool, "1.0.0", 1);
        s->arch = pool_str2id(pool, "noarch", 1);
        repodata_add_poolstr_array(data, s - pool->solvables, SOLVABLE_SUMMARY, "A dummy package");
    }

    repodata_internalize(data);
    return repo;
}

int main() {
    // 创建库池
    Pool *pool = pool_create();

    // 创建并填充一个测试仓库
    Repo *repo = create_test_repo(pool);

    // 初始化找到的软件包数量为0
    int found_count = 0;

    // 为搜索准备库池
    pool_createwhatprovides(pool);

    // 执行搜索，这里我们搜索名称中包含 "package" 的软件包
    repo_search(repo, 0, SOLVABLE_NAME, "package", SEARCH_SUBSTRING, search_callback, &found_count);

    // 检查搜索结果
    if (found_count == 0) {
        fprintf(stderr, "Error: No packages found matching the search criteria.\n");
    }

    // 释放资源
    repo_free(repo, 0);
    pool_free(pool);

    return found_count == 0 ? EXIT_FAILURE : EXIT_SUCCESS; // 如果未找到软件包，则返回错误
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_libsolv_repo_search test_libsolv_repo_search.c `pkg-config --cflags --libs libsolv`")

    def test(self):
        self.cmd("./test_libsolv_repo_search")


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['test_libsolv_repo_search.c', 'test_libsolv_repo_search']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")
