#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libsolv_libsolv_func_005.py
@Time:      2024/03/04 11:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libsolv_libsolv_func_005.yaml for details
    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libsolv libsolv-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_libsolv_pool_match_dep.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <solv/pool.h>
#include <solv/repo.h>
#include <solv/solvable.h>

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <provided_dependency> <required_dependency>\n", argv[0]);
        return EXIT_FAILURE;
    }

    const char *provided_dep_str = argv[1];
    const char *required_dep_str = argv[2];
    Pool *pool;
    Repo *repo;
    Solvable *s1, *s2;
    Id dep1, dep2;
    int match;

    // Initialize the pool
    pool = pool_create();
    if (!pool) {
        fprintf(stderr, "Failed to create pool.\n");
        return EXIT_FAILURE;
    }

    // Create a repository
    repo = repo_create(pool, "test_repo");
    if (!repo) {
        fprintf(stderr, "Failed to create repository.\n");
        pool_free(pool);
        return EXIT_FAILURE;
    }

    // Add two solvables to the repo
    s1 = pool_id2solvable(pool, repo_add_solvable(repo));
    s2 = pool_id2solvable(pool, repo_add_solvable(repo));
    if (!s1 || !s2) {
        fprintf(stderr, "Failed to add solvables to the repository.\n");
        repo_free(repo, 0);
        pool_free(pool);
        return EXIT_FAILURE;
    }

    // Add a provided feature to the first solvable (e.g., a package name and version)
    dep1 = pool_str2id(pool, provided_dep_str, 1);
    if (!dep1) {
        fprintf(stderr, "Failed to create provided dependency.\n");
        repo_free(repo, 0);
        pool_free(pool);
        return EXIT_FAILURE;
    }
    s1->provides = repo_addid_dep(repo, s1->provides, dep1, 0);

    // Add a required feature to the second solvable (e.g., a dependency)
    dep2 = pool_str2id(pool, required_dep_str, 1);
    if (!dep2) {
        fprintf(stderr, "Failed to create required dependency.\n");
        repo_free(repo, 0);
        pool_free(pool);
        return EXIT_FAILURE;
    }
    s2->requires = repo_addid_dep(repo, s2->requires, dep2, 0);

    // Check if the dependency provided by s1 matches the requirement of s2
    match = pool_match_dep(pool, dep1, dep2);
    printf("The dependencies match: %s(req:%s pro:%s)\n", match ? "Yes" : "No", required_dep_str, provided_dep_str);

    // Clean up
    repo_free(repo, 0);
    pool_free(pool);
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_libsolv_pool_match_dep test_libsolv_pool_match_dep.c `pkg-config --cflags --libs libsolv`")

    def test(self):
        self.cmd("./test_libsolv_pool_match_dep libfoo libfoo")
        self.cmd("./test_libsolv_pool_match_dep libfoo-1.0.1 libfoo")
        self.cmd("./test_libsolv_pool_match_dep libfoo libfoo-1.0.1")
        self.cmd("./test_libsolv_pool_match_dep 'libfoo>=1.0.1' 'libfoo>=1.0.1'")


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['test_libsolv_pool_match_dep.c', 'test_libsolv_pool_match_dep']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")
