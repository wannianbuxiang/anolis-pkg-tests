#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libsolv_libsolv_func_007.py
@Time:      2024/03/04 11:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libsolv_libsolv_func_007.yaml for details
    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libsolv libsolv-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_libsolv_pool_whatmatchesdep.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <solv/pool.h>
#include <solv/repo.h>
#include <solv/queue.h>
#include <sys/utsname.h>

int main(int argc, char **argv) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <keyname> <dependency>\n", argv[0]);
        return EXIT_FAILURE;
    }

    struct utsname uname_data;
    if (uname(&uname_data) < 0) {
        perror("uname");
        return EXIT_FAILURE;
    }

    // Initialize the libsolv pool
    Pool *pool = pool_create();
    // Set the system architecture, if relevant
    pool_setarch(pool, uname_data.machine);

    // Create and add a repo to the pool (this is not covered here)
    // You must load actual data from a repository for the pool to have solvables to match against
    Repo *repo = repo_create(pool, "test_repo");

    // Prepare the pool for dependency resolution
    pool_createwhatprovides(pool);

    // Convert the command line arguments to Id types
    Id keyname_id = pool_str2id(pool, argv[1], 0);
    Id dep_id = pool_str2id(pool, argv[2], 0);

    // Initialize the queue to store the result
    Queue result;
    queue_init(&result);

    // Find matches for the provided dependency
    pool_whatmatchesdep(pool, keyname_id, dep_id, &result, 0);

    // Iterate through the results and print each solvable that matches the dependency
    for (int i = 0; i < result.count; i++) {
        Solvable *s = pool_id2solvable(pool, result.elements[i]);
        printf("Matching solvable: %s\n", pool_solvable2str(pool, s));
    }

    // Clean up
    queue_free(&result);
    repo_free(repo, 0);
    pool_free(pool);

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_libsolv_pool_whatmatchesdep test_libsolv_pool_whatmatchesdep.c `pkg-config --cflags --libs libsolv`")

    def test(self):
        self.cmd("./test_libsolv_pool_whatmatchesdep 'SOLVABLE_PROVIDES' 'openssl(libcrypto.so.1.1)(64bit)'")


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['test_libsolv_pool_whatmatchesdep.c', 'test_libsolv_pool_whatmatchesdep']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")
