#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libsolv_libsolv_func_002.py
@Time:      2024/03/04 11:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libsolv_libsolv_func_002.yaml for details
    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libsolv libsolv-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > libsolv_get_pkginfo_test.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <solv/pool.h>
#include <solv/repo.h>

int main() {
    // 创建 pool
    Pool *pool = pool_create();
    if (!pool) {
        fprintf(stderr, "Failed to create the pool.\n");
        return EXIT_FAILURE;
    }
    
    // 添加一个软件包到pool
    Repo *repo = repo_create(pool, "example_repo");
    if (!repo) {
        fprintf(stderr, "Failed to create the repo.\n");
        pool_free(pool);
        return EXIT_FAILURE;
    }
    Solvable *s = pool_id2solvable(pool, repo_add_solvable(repo));
    if (!s) {
        fprintf(stderr, "Failed to add a solvable to the repo.\n");
        repo_free(repo, 0);
        pool_free(pool);
        return EXIT_FAILURE;
    }
    s->name = pool_str2id(pool, "example_package", 1);
    s->evr = pool_str2id(pool, "1.0.0", 1);
    s->arch = pool_str2id(pool, "noarch", 1);
    
    // 将名称、版本、架构的Id转换为字符串并打印
    const char *name_str = pool_id2str(pool, s->name);
    const char *version_str = pool_id2str(pool, s->evr);
    const char *arch_str = pool_id2str(pool, s->arch);
    if (!name_str || !version_str || !arch_str) {
        fprintf(stderr, "Failed to convert Ids to strings.\n");
        repo_free(repo, 0);
        pool_free(pool);
        return EXIT_FAILURE;
    }
    printf("Package: %s, Version: %s, Architecture: %s\n", name_str, version_str, arch_str);
    
    // 将Solvable的Id转换为字符串并打印整个Solvable
    Id solvable_id = s - pool->solvables;
    const char *solvable_str = pool_solvid2str(pool, solvable_id);
    if (!solvable_str) {
        fprintf(stderr, "Failed to convert solvable Id to string.\n");
        repo_free(repo, 0);
        pool_free(pool);
        return EXIT_FAILURE;
    }
    printf("Solvable: %s\n", solvable_str);
    
    // 清理资源
    repo_free(repo, 0);
    pool_free(pool);
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o libsolv_get_pkginfo_test libsolv_get_pkginfo_test.c `pkg-config --cflags --libs libsolv`")

    def test(self):
        self.cmd("./libsolv_get_pkginfo_test")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['libsolv_get_pkginfo_test.c', 'libsolv_get_pkginfo_test']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")
