#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libsolv_libsolv_func_012.py
@Time:      2024/03/04 11:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libsolv_libsolv_func_012.yaml for details
    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libsolv libsolv-devel libsolv-tools"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_libsolv_repodata_search.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <solv/pool.h>
#include <solv/repo.h>
#include <solv/repodata.h>
#include <solv/solvable.h>

#ifndef SOLVID_ALL
#define SOLVID_ALL -1
#endif

int callback(void *cbdata, Solvable *s, Repodata *data, Repokey *key, KeyValue *kv) {
    const char *keyname = pool_id2str(data->repo->pool, key->name);
    const char *value = kv->str;
    printf("Solvable ID: %d, Name: %s, Key: %s, Value: %s\n",
           s - data->repo->pool->solvables, pool_solvable2str(data->repo->pool, s), keyname, value);
    return 0;
}

int main(int argc, char **argv) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <.solv file>\n", argv[0]);
        return 1;
    }
    const char *solv_file = argv[1];
    Pool *pool = pool_create();
    Repo *repo = repo_create(pool, "test_repo");
    Repodata *data = repo_add_repodata(repo, 0);

    // 创建solvable对象并设置键值对
    Solvable *s = pool_id2solvable(pool, repo_add_solvable(repo));
    s->name = pool_str2id(pool, "example-package", 1);
    s->evr = pool_str2id(pool, "1.0-1", 1);
    s->arch = pool_str2id(pool, "noarch", 1);
    repodata_set_str(data, s - pool->solvables, pool_str2id(pool, "summary", 1), "Example package summary");
    repodata_set_str(data, s - pool->solvables, pool_str2id(pool, "description", 1), "An example package description.");
    printf("Created solvable: %s\n", pool_solvable2str(pool, s));

    // 写入.solv文件
    repodata_internalize(data);
    FILE *fp = fopen(solv_file, "wb");
    if (!fp) {
        perror("Failed to open file for writing");
        pool_free(pool);
        return 1;
    }
    repo_write(repo, fp);
    fclose(fp);
    printf("Data written to .solv file.\n");

    // printf("Dumping .solv file contents:\n");
    // system("dumpsolv repo.solv"); // 确保dumpsolv命令在您的系统路径上
    // 重新加载.solv数据
    repo_empty(repo, 1);
    fp = fopen(solv_file, "rb");
    if (!fp) {
        perror("Failed to open file for reading");
        pool_free(pool);
        return 1;
    }
    repo_add_solv(repo, fp);
    fclose(fp);
    printf("Data reloaded from .solv file.\n");

    // 获取最新的 repodata 对象
    data = repo_last_repodata(repo);
    // 检查repodata对象是否包含summary键
    printf("Dumping repodata keys:\n");
    for (int i = 1; i < data->nkeys; ++i) {
        printf("Key[%d]: %s\n", i, pool_id2str(pool, data->keys[i].name));
    }

    // 搜索键值对
    Id keyname_id = pool_str2id(pool, "summary", 1);
    if (keyname_id) {
        printf("Searching for key: summary\n");
        repodata_search(data, SOLVID_ALL, keyname_id, SEARCH_STRING, callback, NULL);
    } else {
        fprintf(stderr, "Key 'summary' does not exist in pool.\n");
    }

    pool_free(pool);
    return 0;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_libsolv_repodata_search test_libsolv_repodata_search.c `pkg-config --cflags --libs libsolv` -Wno-implicit-function-declaration")

    def test(self):
        self.cmd("./test_libsolv_repodata_search 'repo.solv'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['test_libsolv_repodata_search.c', 'test_libsolv_repodata_search', 'repo.solv']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")
