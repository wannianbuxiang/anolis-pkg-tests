#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libsolv_libsolv_func_009.py
@Time:      2024/03/04 11:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libsolv_libsolv_func_009.yaml for details
    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libsolv libsolv-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_libsolv_pool_whatcontainsdep.c << EOF
#include <solv/pool.h>
#include <solv/repo.h>
#include <solv/queue.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/utsname.h>

int main(int argc, char **argv) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <keyname> <dependency>\n", argv[0]);
        return EXIT_FAILURE;
    }

    struct utsname uname_data;
    if (uname(&uname_data) < 0) {
        perror("uname");
        return EXIT_FAILURE;
    }

    // Read the arguments
    const char *keyname_str = argv[1];
    const char *dep_str = argv[2];

    // Create a pool and a repo
    Pool *pool = pool_create();
    pool_setarch(pool, uname_data.machine); // For example, you may adjust the architecture as needed
    Repo *repo = repo_create(pool, "test_repo");

    // Load the repository data into the pool
    // In a real-world application, you would load actual data from a repository here.
    // For this example, we will assume that the data is already present.
    // ...

    // Prepare the pool for dependency resolution
    pool_createwhatprovides(pool);

    // Convert the keyname and dependency strings to their respective Ids
    Id keyname_id = pool_str2id(pool, keyname_str, 0);
    if (keyname_id == ID_EMPTY) {
        fprintf(stderr, "Invalid keyname provided.\n");
        pool_free(pool);
        return EXIT_FAILURE;
    }

    Id dep_id = pool_str2id(pool, dep_str, 0);
    if (dep_id == ID_EMPTY) {
        fprintf(stderr, "Invalid dependency provided.\n");
        pool_free(pool);
        return EXIT_FAILURE;
    }

    // Create a queue to hold the result
    Queue result;
    queue_init(&result);

    // Find all solvables that contain the specified dependency
    pool_whatcontainsdep(pool, keyname_id, dep_id, &result, 0);

    // Print the solvables
    printf("Solvables containing the dependency %s:\n", dep_str);
    for (int i = 0; i < result.count; i++) {
        Solvable *s = pool_id2solvable(pool, result.elements[i]);
        printf("  - %s\n", pool_solvable2str(pool, s));
    }

    // Cleanup
    queue_free(&result);
    repo_free(repo, 0);
    pool_free(pool);

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_libsolv_pool_whatcontainsdep test_libsolv_pool_whatcontainsdep.c `pkg-config --cflags --libs libsolv`")

    def test(self):
        self.cmd("./test_libsolv_pool_whatcontainsdep 'rpm:provides' 'libm.so.6'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['test_libsolv_pool_whatcontainsdep.c', 'test_libsolv_pool_whatcontainsdep']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")
