#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libsolv_libsolv_func_008.py
@Time:      2024/03/04 11:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libsolv_libsolv_func_008.yaml for details
    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libsolv libsolv-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_libsolv_pool_queuetowhatprovides.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <solv/pool.h>
#include <solv/repo.h>
#include <solv/queue.h>
#include <sys/utsname.h>

int main(int argc, char **argv) {
    if (argc < 2) {
        fprintf(stderr, "Usage: %s <dependency1> [<dependency2> ...]\n", argv[0]);
        return EXIT_FAILURE;
    }

    struct utsname uname_data;
    if (uname(&uname_data) < 0) {
        perror("uname");
        return EXIT_FAILURE;
    }

    // Initialize the pool
    Pool *pool = pool_create();
    pool_setarch(pool, uname_data.machine); // Set architecture for the pool

    // Create a repository
    Repo *repo = repo_create(pool, "test_repo");

    // ** NOTE: In real-world usage, you need to load repository data here **

    // Prepare a queue for the dependencies
    Queue dep_queue;
    queue_init(&dep_queue);

    // Add command line arguments (dependencies) to the queue
    for (int i = 1; i < argc; ++i) {
        Id dep_id = pool_str2id(pool, argv[i], 1);
        queue_push(&dep_queue, dep_id);
    }

    // Convert the queue of dependencies to a queue of 'whatprovides' IDs
    Queue whatprovides_queue;
    queue_init(&whatprovides_queue);
    pool_queuetowhatprovides(pool, &dep_queue);

    // Prepare the pool for dependency resolution
    pool_createwhatprovides(pool);

    // Iterate through the 'whatprovides' queue and print the names of solvables providing the dependencies
    for (int i = 0; i < whatprovides_queue.count; ++i) {
        Id prov_id = whatprovides_queue.elements[i];
        Id *wp = pool_whatprovides_ptr(pool, prov_id);
        for (; *wp; wp++) {
            Solvable *s = pool_id2solvable(pool, *wp);
            printf("Solvable providing dependency '%s': %s\n", argv[i + 1], pool_solvable2str(pool, s));
        }
    }

    // Cleanup
    queue_free(&whatprovides_queue);
    queue_free(&dep_queue);
    repo_free(repo, 0);
    pool_free(pool);

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_libsolv_pool_queuetowhatprovides test_libsolv_pool_queuetowhatprovides.c `pkg-config --cflags --libs libsolv`")

    def test(self):
        self.cmd("./test_libsolv_pool_queuetowhatprovides 'libc.so.6()(64bit)' 'libm.so.6()(64bit)'")
        self.cmd("./test_libsolv_pool_queuetowhatprovides 'libtest.so' 'libtest.so'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['test_libsolv_pool_queuetowhatprovides.c', 'test_libsolv_pool_queuetowhatprovides']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")
