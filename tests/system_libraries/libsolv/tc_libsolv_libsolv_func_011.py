#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libsolv_libsolv_func_011.py
@Time:      2024/03/04 11:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libsolv_libsolv_func_011.yaml for details
    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libsolv libsolv-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_libsolv_repodata_chk2str.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <solv/pool.h>
#include <solv/repo.h>

// Helper function to convert a hex character to its integer value
int hex_to_int(char c) {
    if (c >= '0' && c <= '9') return c - '0';
    if (c >= 'a' && c <= 'f') return c - 'a' + 10;
    if (c >= 'A' && c <= 'F') return c - 'A' + 10;
    return -1; // Invalid hex character
}

// Helper function to convert a hex string to a byte array
int hexstr_to_bytes(const char *hexstr, unsigned char *bytes, size_t max_bytes) {
    size_t len = strlen(hexstr);
    if (len % 2 != 0) return -1; // Hex string should have an even number of digits
    for (size_t i = 0; i < len / 2; ++i) {
        int high_nibble = hex_to_int(hexstr[2 * i]);
        int low_nibble = hex_to_int(hexstr[2 * i + 1]);
        if (high_nibble < 0 || low_nibble < 0) return -1; // Invalid hex character
        if (i >= max_bytes) return -1; // Byte array too small
        bytes[i] = (high_nibble << 4) | low_nibble;
    }
    return 0; // Success
}

int main(int argc, char **argv) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <checksum_type_id> <checksum_hex>\n", argv[0]);
        return EXIT_FAILURE;
    }

    Id chk_type_id = atoi(argv[1]); // Convert checksum type to Id
    const char *chk_hex = argv[2]; // Checksum hex string

    // Convert the hex string to a byte array
    size_t chk_len = strlen(chk_hex) / 2;
    unsigned char *chk_bytes = malloc(chk_len);
    if (!chk_bytes || hexstr_to_bytes(chk_hex, chk_bytes, chk_len) != 0) {
        fprintf(stderr, "Invalid checksum hex string\n");
        free(chk_bytes);
        return EXIT_FAILURE;
    }

    // Create a dummy pool and repo since repodata_chk2str requires a Repodata pointer
    Pool *pool = pool_create();
    Repo *repo = repo_create(pool, "dummy");
    Repodata *data = repo_add_repodata(repo, 0);

    // Call repodata_chk2str to convert the checksum to a string
    const char *chk_str = repodata_chk2str(data, chk_type_id, chk_bytes);
    printf("Checksum string: %s\n", chk_str);

    // Cleanup
    free(chk_bytes);
    pool_free(pool);

    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_libsolv_repodata_chk2str test_libsolv_repodata_chk2str.c `pkg-config --cflags --libs libsolv` -Wno-int-conversion")

    def test(self):
        self.cmd("./test_libsolv_repodata_chk2str 1 'd41d8cd98f00b204e9800998ecf8427e'")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['test_libsolv_repodata_chk2str.c', 'test_libsolv_repodata_chk2str']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")
