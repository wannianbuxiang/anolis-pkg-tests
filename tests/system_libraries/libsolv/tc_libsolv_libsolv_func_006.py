#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libsolv_libsolv_func_006.py
@Time:      2024/03/04 11:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libsolv_libsolv_func_006.yaml for details
    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libsolv libsolv-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_libsolv_pool_calc_installsizechange.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <solv/pool.h>
#include <solv/repo.h>
#include <solv/solvable.h>
#include <solv/bitmap.h>
#include <solv/repodata.h>

int create_test_repo(Pool *pool, Repo *repo) {
    Repodata *data = repo_add_repodata(repo, 0);
    if (!data) {
        fprintf(stderr, "Failed to add repodata to the repository.\n");
        return -1;
    }

    for (int i = 0; i < 10; ++i) {
        Solvable *s = pool_id2solvable(pool, repo_add_solvable(repo));
        // The ID for the installation size attribute
        Id handle = s - pool->solvables;

        // For simplicity, let\'s assume each package has a size of 100 bytes
        unsigned long long installsize = 100;

        // Set the installation size for the solvable
        repodata_set_num(data, handle, SOLVABLE_INSTALLSIZE, installsize);
    }

    // Internalize the in-memory changes into persistent storage
    repodata_internalize(data);
    return 0;
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        fprintf(stderr, "Usage: %s <id1> <id2> ... (list of installed solvable IDs)\n", argv[0]);
        return EXIT_FAILURE;
    }

    Pool *pool = pool_create();
    if (!pool) {
        fprintf(stderr, "Failed to create pool.\n");
        return EXIT_FAILURE;
    }

    Repo *repo = repo_create(pool, "test_repo");
    if (!repo) {
        fprintf(stderr, "Failed to create repository.\n");
        pool_free(pool);
        return EXIT_FAILURE;
    }

    if (create_test_repo(pool, repo)) {
        fprintf(stderr, "Failed to create test repository with solvables.\n");
        repo_free(repo, 0);
        pool_free(pool);
        return EXIT_FAILURE;
    }

    // Initialize the installed map
    Map installedmap;
    map_init(&installedmap, pool->nsolvables);
    for (int i = 1; i < argc; ++i) {
        Id id = atoi(argv[i]);
        if (id <= 0 || id >= pool->nsolvables) {
            fprintf(stderr, "Invalid solvable ID: %s\n", argv[i]);
            map_free(&installedmap);
            repo_free(repo, 0);
            pool_free(pool);
            return EXIT_FAILURE;
        }
        MAPSET(&installedmap, id);
    }

    long long size_change = pool_calc_installsizechange(pool, &installedmap);
    printf("The installation size change is: %lld bytes\n", size_change);

    // Clean up
    map_free(&installedmap);
    repo_free(repo, 0);
    pool_free(pool);
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_libsolv_pool_calc_installsizechange test_libsolv_pool_calc_installsizechange.c `pkg-config --cflags --libs libsolv`")

    def test(self):
        self.cmd("./test_libsolv_pool_calc_installsizechange 1 2 3")
        self.cmd("./test_libsolv_pool_calc_installsizechange 1 3 3")
        self.cmd("./test_libsolv_pool_calc_installsizechange 1 11")
        self.cmd("./test_libsolv_pool_calc_installsizechange 11 11")
        self.cmd("./test_libsolv_pool_calc_installsizechange 11 11 2 11 4 11")
        self.cmd("./test_libsolv_pool_calc_installsizechange 9 1 2 3 4 5 6 7 8")
        self.cmd("./test_libsolv_pool_calc_installsizechange 9 1 2 3 4 5 6 7 8 9 10 11")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['test_libsolv_pool_calc_installsizechange.c', 'test_libsolv_pool_calc_installsizechange']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")
