#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libsolv_libsolv_func_004.py
@Time:      2024/03/04 11:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libsolv_libsolv_func_004.yaml for details
    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libsolv libsolv-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_libsolv_dataiterator_match.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <solv/pool.h>
#include <solv/repo.h>
#include <solv/dataiterator.h>

int main() {
    // 创建库池
    Pool *pool = pool_create();
    if (!pool) {
        fprintf(stderr, "Failed to create a pool.\n");
        return EXIT_FAILURE;
    }

    // 添加一个软件包到 pool
    Repo *repo = repo_create(pool, "example_repo");
    if (!repo) {
        fprintf(stderr, "Failed to create a repo.\n");
        pool_free(pool);
        return EXIT_FAILURE;
    }

    // 添加一些假的软件包到仓库
    const char *names[] = {"example_package", "another_package", "example_app", NULL};
    for (int i = 0; names[i]; ++i) {
        Solvable *s = pool_id2solvable(pool, repo_add_solvable(repo));
        if (!s) {
            fprintf(stderr, "Failed to add a solvable to the repo.\n");
            repo_free(repo, 0);
            pool_free(pool);
            return EXIT_FAILURE;
        }
        s->name = pool_str2id(pool, names[i], 1);
        s->evr = pool_str2id(pool, "1.0.0", 1);
        s->arch = pool_str2id(pool, "noarch", 1);
        // 检查 pool_str2id 返回的 ID 是否有效
        if (s->name == ID_EMPTY || s->evr == ID_EMPTY || s->arch == ID_EMPTY) {
            fprintf(stderr, "Failed to create solvable details.\n");
            repo_free(repo, 0);
            pool_free(pool);
            return EXIT_FAILURE;
        }
    }

    pool_createwhatprovides(pool);

    // 创建一个 Dataiterator 来搜索名称中包含 "example" 的软件包
    Dataiterator di;
    dataiterator_init(&di, pool, repo, 0, SOLVABLE_NAME, "example", SEARCH_SUBSTRING);
    int found = 0; // 用于跟踪是否找到了软件包
    while (dataiterator_step(&di)) {
        Solvable *s = pool_id2solvable(pool, di.solvid);
        if (s) {
            const char *name = pool_id2str(pool, s->name);
            if (name) {
                printf("Found package: %s\n", name);
                found = 1; // Mark that we found at least one package
            }
        }
    }
    dataiterator_free(&di);

    // 检查是否找到了软件包
    if (!found) {
        fprintf(stderr, "No packages found containing the substring 'example'.\n");
        repo_free(repo, 0);
        pool_free(pool);
        return EXIT_FAILURE;
    }
    
    // 清理资源
    repo_free(repo, 0);
    pool_free(pool);
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_libsolv_dataiterator_match test_libsolv_dataiterator_match.c `pkg-config --cflags --libs libsolv`")

    def test(self):
        self.cmd("./test_libsolv_dataiterator_match")


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['test_libsolv_dataiterator_match.c', 'test_libsolv_dataiterator_match']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")
