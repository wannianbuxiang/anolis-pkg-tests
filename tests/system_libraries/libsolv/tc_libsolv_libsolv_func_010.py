#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libsolv_libsolv_func_010.py
@Time:      2024/03/04 11:20:00
@Author:    suomengjun
@Version:   1.0
@Contact:   smj01095381@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    suomengjun
"""

import os
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_libsolv_libsolv_func_010.yaml for details
    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libsolv libsolv-devel"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        code = r'''cat > test_libsolv_pool_search.c << EOF
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <solv/pool.h>
#include <solv/repo.h>
#include <solv/queue.h>
#include <solv/bitmap.h>
#include <sys/utsname.h>

// Callback function that prints the solvable information
int search_callback(void *cbdata, Solvable *s, Repodata *data, Repokey *key, KeyValue *kv) {
    Pool *pool = (Pool *)cbdata;   
    // Correctly use pool_id2str for both key name and value.
    // Check if the key and value are available before trying to print them.
    const char *keyname = key ? pool_id2str(pool, key->name) : NULL;
    const char *value = kv ? pool_id2str(pool, kv->str) : NULL;
    printf("Found solvable: %s, with key: %s, value: %s\n",
           pool_solvable2str(pool, s), keyname ? keyname : "N/A", value ? value : "N/A");

    return 0; // Returning 0 continues the search
}

int main(int argc, char **argv) {
    if (argc != 4) {
        fprintf(stderr, "Usage: %s <keyname> <match> <flags>\n", argv[0]);
        return EXIT_FAILURE;
    }

    struct utsname uname_data;
    if (uname(&uname_data) < 0) {
        perror("uname");
        return EXIT_FAILURE;
    }

    Id keyname_id = atoi(argv[1]); 
    const char *match = argv[2];
    int flags = atoi(argv[3]);

    // Initialize the libsolv pool
    Pool *pool = pool_create();
    pool_setarch(pool, uname_data.machine); // Set the system architecture if needed

    // Load the repository data into the pool
    // This is just a placeholder. In a real program, you would load actual repository data.
    // ...

    // Prepare the pool for dependency resolution
    pool_createwhatprovides(pool);

    // Perform the search using pool_search
    pool_search(pool, 0 /* search in all solvables */, keyname_id, match, flags, search_callback, pool);

    // Clean up
    pool_free(pool);
    return EXIT_SUCCESS;
}
EOF'''
        self.cmd(code)
        self.cmd("gcc -o test_libsolv_pool_search test_libsolv_pool_search.c `pkg-config --cflags --libs libsolv` -Wno-int-conversion")

    def test(self):
        self.cmd("./test_libsolv_pool_search 1 'openssl' 2")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        for f in ['test_libsolv_pool_search.c', 'test_libsolv_pool_search']:
            if os.path.exists(f):
                self.cmd(f"rm -rf {f}")
