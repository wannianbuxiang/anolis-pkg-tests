#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libsemanage_fun_001.py
@Time:      2024/04/17 10:12:50
@Author:    hongfeng
@Version:   1.0
@Contact:   wb-zhf668260@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    hongfeng
"""

from common.basetest import LocalTest
class Test(LocalTest):
    """
    See tc_libsemanage_fun_001.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libsemanage-devel policycoreutils-python-utils"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > test01.c <<EOF
#include <semanage/fcontext_record.h>
#include <semanage/semanage.h>
#include <semanage/fcontexts_local.h>
#include <sepol/sepol.h>
#include <errno.h>

int main(int argc, char *argv[]) {
    semanage_handle_t *handle;
    semanage_bool_t **records = NULL;
    unsigned int count = 0;
    int rc;

    if (argc != 2) {
        fprintf(stderr, "Usage: test_semanage <name>\\n");
        return 1;
    }
    size_t arg_length = strlen(argv[1]) + 1;
    char test_name[arg_length];
    strcpy(test_name, argv[1]);

    // 1. 创建和初始化 semanage_handle_t
    if ((handle = semanage_handle_create()) == NULL) {
        fprintf(stderr, "Failed to create semanage handle: %s\\n", strerror(errno));
        return 1;
    }

    // 2. 连接到 semanage
    if ((rc = semanage_connect(handle)) < 0) {
        fprintf(stderr, "Failed to connect to semanage: %s\\n", strerror(-rc));
        semanage_handle_destroy(handle);
        return 1;
    }

    // 3: Open the current policy for reading and retrieve boolean records
    semanage_bool_list(handle, &records, &count);

    // 4: Iterate over retrieved boolean records and filter by name
    for (unsigned int i = 0; i < count; ++i) {
        semanage_bool_t *boolean = records[i];
        const char *name = semanage_bool_get_name(boolean);
        int state =  semanage_bool_get_value(boolean);
        // Filter by test_name
        if (strstr(name, test_name) != NULL) {
            printf("%s, %d\\n", name, state);
        }
    }

    return 0;
}
EOF"""
        self.cmd(cmdline)
        self.cmd("gcc -o test01 test01.c -lsemanage")

    def test(self):
        ret_c, ret_o = self.cmd("./test01 ssh_sysadm_login")
        expect_state = "off" if "0" in ret_o else "on"

        # 用命令检查和api取回一致
        ret_c, ret_o = self.cmd("semanage boolean --list | grep ssh_sysadm_login | awk -F'[()]' '{split($2, values, \", \"); print values[1]}'")
        self.assertTrue(expect_state == ret_o, "The state is not correct.")
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm test01.c test01")
