#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_libsemanage_fun_002.py
@Time:      2024/04/17 10:12:50
@Author:    hongfeng
@Version:   1.0
@Contact:   wb-zhf668260@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    hongfeng
"""

from common.basetest import LocalTest
class Test(LocalTest):
    """
    See tc_libsemanage_fun_002.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gcc libsemanage-devel policycoreutils-python-utils"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > test02.c <<EOF
#include <semanage/fcontext_record.h>
#include <semanage/semanage.h>
#include <semanage/fcontexts_local.h>
#include <sepol/sepol.h>
#include <errno.h>

int main(int argc, char *argv[]) {
    semanage_handle_t *handle;
    semanage_fcontext_t *fcontext = NULL;
    semanage_context_t *con = NULL;
    int rc;
    semanage_fcontext_key_t *k;
    int exist = 0;

    if (argc != 4) {
        fprintf(stderr, "Usage: test_fcontext <file_path_pattern> <context_string> <type>\\n");
        return 1;
    }

    const char *file_pattern = argv[1];
    const char *context_str = argv[2];
    int fcontext_type = atoi(argv[3]);  

    // 1. 创建和初始化 semanage_handle_t
    if ((handle = semanage_handle_create()) == NULL) {
        fprintf(stderr, "Failed to create semanage handle: %s\\n", strerror(errno));
        return 1;
    }

    // 2. 连接到 semanage
    if ((rc = semanage_connect(handle)) < 0) {
        fprintf(stderr, "Failed to connect to semanage: %s\\n", strerror(-rc));
        semanage_handle_destroy(handle);
        return 1;
    }

    if ((rc = semanage_begin_transaction(handle)) < 0) {
        fprintf(stderr, "Failed to begin transaction: %s\\n", strerror(-rc));
        goto cleanup;
    }

    if (semanage_fcontext_key_create(handle, argv[2], SEMANAGE_FCONTEXT_REG, &k) < 0) {
        fprintf(stderr, "Could not create key for %s", argv[2]);
        return -1;
    }

    if(semanage_fcontext_exists(handle, k, &exist) < 0) {
        return -1;
    }
    if (exist) {
        return -1;
    }

    // 3. 创建 semanage_fcontext_t 结构体
    if ((rc = semanage_fcontext_create(handle, &fcontext)) < 0) {
        fprintf(stderr, "Failed to create file context object: %s\\n", strerror(-rc));
        goto cleanup;
    }

    // 4. 设置文件路径模式
    if ((rc = semanage_fcontext_set_expr(handle, fcontext, file_pattern)) < 0) {
        fprintf(stderr, "Failed to set file path pattern '%s': %s\\n", file_pattern, strerror(-rc));
        goto cleanup;
    }

    // 5. 解析上下文字符串并设置上下文
    if ((rc = semanage_context_from_string(handle, context_str, &con)) < 0) {
        fprintf(stderr, "Could not create context using '%s' for file context '%s'\\n", context_str, file_pattern);
        goto cleanup;
    }

    if ((rc = semanage_fcontext_set_con(handle, fcontext, con)) < 0) {
        fprintf(stderr, "Could not set file context for '%s'\\n", file_pattern);
        goto cleanup;
    }

    semanage_fcontext_set_type(fcontext, fcontext_type);

    // 7. 将新的文件上下文规则添加到策略数据库（此处仅为示例，实际应用中应根据需要考虑事务提交等操作）
    if(semanage_fcontext_modify_local(handle, k, fcontext) < 0) {
        fprintf(stderr,"Could not add file context for %s", argv[2]);
        return -1;
    }

    if ((rc = semanage_commit(handle)) < 0) {
        fprintf(stderr, "Failed to commit transaction: %s\\n", strerror(-rc));
        return -1;
    }

    printf("Successfully created file context rule for pattern '%s' with context '%s' and type %d\\n",
           file_pattern, context_str, fcontext_type);

cleanup:
    semanage_context_free(con);
    semanage_fcontext_free(fcontext);
    semanage_disconnect(handle);
    semanage_handle_destroy(handle);

    return rc >= 0 ? 0 : 1;
}
EOF"""
        self.cmd(cmdline)
        self.cmd("gcc -o test02 test02.c -lsemanage")

    def test(self):
        ret_c, ret_o = self.cmd("./test02 '/tmp/test02' 'system_u:object_r:httpd_sys_content_t:s0' SEMANAGE_FCONTEXT_REG")
        self.assertTrue("Successfully created file context rule" in ret_o, 'check output error.')

        # 检查设置成功
        ret_c, ret_o = self.cmd("semanage fcontext -l | grep '/tmp/test02'")
        self.assertTrue("system_u:object_r:httpd_sys_content_t:s0" in ret_o, 'check output error.')
        self.cmd("semanage fcontext -d -t 'all files' /tmp/test02")
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm test02.c test02")
