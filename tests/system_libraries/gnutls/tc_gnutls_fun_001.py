#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_gnutls_fun_001.py
@Time:      2024/04/10 09:42:50
@Author:    hongfeng
@Version:   1.0
@Contact:   wb-zhf668260@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    hongfeng
"""

from common.basetest import LocalTest
import os
class Test(LocalTest):
    """
    See tc_gnutls_fun_001.yaml for details

    :avocado: tags=P2,noarch,local,fix
    """
    PARAM_DIC = {"pkg_name": "gnutls-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline = """cat > gnutls_server.c <<EOF
#include <stdio.h>
#include <gnutls/gnutls.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define SERVER_CERTFILE "server.crt"
#define SERVER_KEYFILE "server.key"
#define MAX_BUF 1024
struct linger so_linger;

static void terminate(const char *msg)
{
    gnutls_deinit(NULL);
    fprintf(stderr, "%s\\n", msg);
    exit(1);
}

int main(int argc, char **argv[])
{
    int server_sockfd, client_sockfd;
    struct sockaddr_in server_addr, client_addr;
    socklen_t sin_size;
    char buffer[MAX_BUF];
    ssize_t bytes_read;
    int ret;
    const char *hostname = "localhost";
    gnutls_certificate_credentials_t x509_cred;
    gnutls_session_t session;

    /* 初始化GnuTLS */
    gnutls_global_init();

    /* 创建TCP socket */
    server_sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (server_sockfd == -1) {
        perror("socket");
        return 1;
    }
    // 表示启用LINGER选项
    so_linger.l_onoff = 1; 
    // 表示立即断开连接，不等待数据发送完
    so_linger.l_linger = 0; 
    setsockopt(server_sockfd, SOL_SOCKET, SO_LINGER, (void *)&so_linger, sizeof(so_linger));

    /* 设置服务器地址信息 */
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(12345);
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);

    /* 绑定地址到socket */
    if (bind(server_sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr)) == -1) {
        perror("bind");
        close(server_sockfd);
        return 1;
    }

    /* 开始监听连接 */
    if (listen(server_sockfd, 5) == -1) {
        perror("listen");
        close(server_sockfd);
        return 1;
    }

    while (1) {  
        sin_size = sizeof(client_addr);
        client_sockfd = accept(server_sockfd, (struct sockaddr *)&client_addr, &sin_size);
        if (client_sockfd == -1) {
            perror("accept");
            continue;
        }

        /* 初始化GnuTLS会话 */
        gnutls_certificate_allocate_credentials(&x509_cred);
        gnutls_certificate_set_x509_key_file(x509_cred, SERVER_CERTFILE, SERVER_KEYFILE, GNUTLS_X509_FMT_PEM);
        gnutls_init(&session, GNUTLS_SERVER);
        gnutls_priority_set_direct(session, "NORMAL:+VERS-TLS1.2:+CTYPE-X509", NULL);
        gnutls_credentials_set(session, GNUTLS_CRD_CERTIFICATE, x509_cred);

        gnutls_transport_set_int(session, client_sockfd);

        do {
            ret = gnutls_handshake(session);
            if (ret < 0 && gnutls_error_is_fatal(ret) == 0) {
                printf("Handshake.....\\n");
                continue;
            } else if (ret < 0) {
                terminate("gnutls_handshake");
            }
            printf("Handshake successful.\\n");
            break;
        } while (ret == 0);
  
        while ((bytes_read = gnutls_record_recv(session, buffer, MAX_BUF)) > 0) {
            printf("Received: %s\\n", buffer);
            gnutls_record_send(session, buffer, bytes_read);
        }

        if (bytes_read == 0) {
            printf("Client disconnected\\n");
        } else if (bytes_read < 0) {
            terminate(gnutls_strerror(bytes_read));
        }

        gnutls_bye(session, GNUTLS_SHUT_RDWR);
        
        /* 清理GnuTLS资源 */
        gnutls_deinit(session);

        /* 关闭客户端连接 */
        close(client_sockfd);
    }

    /* 清理全局GnuTLS资源和证书 */
    gnutls_certificate_free_credentials(x509_cred);
    close(server_sockfd);
    gnutls_global_deinit();

    return 0;
}
EOF"""

        self.cmd(cmdline)
        cmdline = """cat > gnutls_client.c <<EOF
#include <stdio.h>
#include <gnutls/gnutls.h>
#include <gnutls/crypto.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define MAX_BUF 1024
#define SERVER_IP "127.0.0.1"   // 替换为服务器IP地址
#define SERVER_PORT 12345       // 使用与服务器相同的端口

static void terminate(const char *msg) {
    gnutls_perror(msg);
    exit(1);
}

int main() {
    int sd;
    struct sockaddr_in server_addr;
    gnutls_certificate_credentials_t cert_cred;
    gnutls_session_t session;
    unsigned char buffer[MAX_BUF + 1];
    ssize_t ret;

    /* 初始化GnuTLS */
    gnutls_global_init();

    /* 设置信任证书链 */
    gnutls_certificate_allocate_credentials(&cert_cred);
    gnutls_certificate_set_x509_trust_file(cert_cred, "server.crt", GNUTLS_X509_FMT_PEM);


    /* 创建TCP套接字 */
    sd = socket(AF_INET, SOCK_STREAM, 0);
    if (sd < 0) {
        perror("socket");
        exit(1);
    }

    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(SERVER_PORT);
    inet_pton(AF_INET, SERVER_IP, &server_addr.sin_addr);

    if (connect(sd, (struct sockaddr*) &server_addr, sizeof(server_addr)) < 0) {
        perror("connect");
        close(sd);
        exit(1);
    }

    /* 初始化一个GnuTLS会话 */
    gnutls_init(&session, GNUTLS_CLIENT);

    /* 设置会话优先级和信任证书 */
    gnutls_priority_set_direct(session, "NORMAL:+VERS-TLS1.3:+CTYPE-X509", NULL);
    gnutls_credentials_set(session, GNUTLS_CRD_CERTIFICATE, cert_cred);

    /* 将套接字与GnuTLS会话关联起来 */
    gnutls_transport_set_int(session, sd);

    do {
        ret = gnutls_handshake(session);
        if (ret < 0 && gnutls_error_is_fatal(ret) == 0) {
            printf("Handshake.....\\n");
            continue;
        } else if (ret < 0) {
            terminate("gnutls_handshake");
        }
        printf("Handshake successful.\\n");
        break;
    } while (ret == 0);

    /* 安全地发送和接收消息 */
    strcpy(buffer, "Hello, Server!");
    gnutls_record_send(session, buffer, strlen(buffer));
    printf("Sending: %s\\n", buffer);

    do {
        ret = gnutls_record_recv(session, buffer, MAX_BUF);
        if (ret == 0) { /* EOS or error */
            break;
        } else if (ret < 0) {
            if (gnutls_error_is_fatal(ret) != 0) {
                terminate("gnutls_record_recv");
            }
            continue;
        }
        buffer[ret] = '\\0';
        printf("Received: %s\\n", buffer);
        break;
    } while (1);

    /* 清理会话资源 */
    gnutls_bye(session, GNUTLS_SHUT_RDWR);
    gnutls_deinit(session);

    /* 清理全局和证书资源 */
    gnutls_certificate_free_credentials(cert_cred);
    gnutls_global_deinit();

    close(sd);

    return 0;
}
EOF"""
        self.cmd(cmdline)
        self.cmd("gcc -o gnutls_server gnutls_server.c `pkg-config --cflags --libs gnutls`")
        self.cmd("gcc -o gnutls_client gnutls_client.c `pkg-config --cflags --libs gnutls`")
        file_dir = os.path.dirname(__file__)
        self.cmd(f"cp {file_dir}/server.crt ./server.crt")
        self.cmd(f"cp {file_dir}/server.key ./server.key")

    def test(self):
        os.system("./gnutls_server &")
        ret_c, ret_o = self.cmd("./gnutls_client")

        self.assertTrue("Handshake successful." in ret_o, "check output error.")
        self.assertTrue("Received: Hello, Server!" in ret_o, "check output error.")
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("killall -9 'gnutls_server'", ignore_status=True)
        self.cmd("rm gnutls_server.c gnutls_server gnutls_client.c gnutls_client server.crt server.key")
