#! /usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File:      tc_libsepol_libsepol_fun_001.py
@Time:      2024年04月01日16:06:08
@Author:    Liujiang <wb-lj931787@alibaba-inc.com>
@License:   Mulan PSL v2
@Modified:  Liujiang <wb-lj931787@alibaba-inc.com>
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libsepol_libsepol_fun_001.yaml for details
    
    :avocado: tags=P1,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "libsepol libsepol-devel libsepol-static selinux-policy selinux-policy-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline_policy_loaded_func ='''cat > test_policy_loaded.c <<EOF
#include <stdio.h>
#include <sepol/sepol.h>
#include <sepol/policydb/policydb.h>
#include <sepol/policydb/conditional.h>
#include <sepol/policydb/avrule_block.h>

int main(void) {
    const char *policy_file = "/etc/selinux/targeted/policy/policy.test";
    sepol_handle_t *handle = sepol_handle_create();
    sepol_policydb_t *policydb = NULL;
    sepol_policy_file_t *pf = NULL;

    if (sepol_policydb_create(&policydb) < 0) {
        fprintf(stderr, "Cannot create policydb structure.");
        goto cleanup;
    }
    
    if (sepol_policy_file_create(&pf) < 0) {
        fprintf(stderr, "Cannot create policy file structure.");
        goto cleanup;
    }
    sepol_policy_file_set_fp(pf, fopen(policy_file, "r"));
    if (sepol_policydb_read(policydb, pf) < 0) {
        fprintf(stderr, "Cannot load policy.");
        goto cleanup;
    }
    printf("Policy loaded successfully.");

cleanup:
    if (policydb) {
        sepol_policydb_free(policydb);
    }
    if (pf) {
        sepol_policy_file_free(pf);
    }
    if (handle) {
        sepol_handle_destroy(handle);
    }
    return 0;
}
EOF
'''
        self.cmd(cmdline_policy_loaded_func)

    def test(self):
        self.cmd("rpm -qa |grep libsepol-devel")
        self.cmd("cp /etc/selinux/targeted/policy/policy.[0-9][0-9] /etc/selinux/targeted/policy/policy.test")
        self.cmd("gcc -o test_policy_loaded test_policy_loaded.c -l:libsepol.a")
        code, result = self.cmd("file test_policy_loaded")
        self.assertIn("dynamically linked", result)
        code, test_init_destroy_output = self.cmd("./test_policy_loaded")
        self.assertIn("successfully", test_init_destroy_output)
 
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf test_policy_loaded.c test_policy_loaded /etc/selinux/targeted/policy/policy.test")