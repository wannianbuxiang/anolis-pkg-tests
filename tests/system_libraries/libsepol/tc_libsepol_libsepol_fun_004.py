#! /usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File:      tc_libsepol_libsepol_fun_004.py
@Time:      2024年04月01日16:06:08
@Author:    Liujiang <wb-lj931787@alibaba-inc.com>
@License:   Mulan PSL v2
@Modified:  Liujiang <wb-lj931787@alibaba-inc.com>
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libsepol_libsepol_fun_004.yaml for details
    
    :avocado: tags=P1,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "libsepol libsepol-devel selinux-policy selinux-policy-devel CUnit CUnit-devel libsepol-static"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline_ebitmap_set_and_get_func ='''cat > test_ebitmap_set_and_get.c <<EOF
#include <CUnit/Basic.h>
#include <stdlib.h>
#include <time.h>
#include <sepol/policydb/ebitmap.h>
#include <sepol/policydb/policydb.h>

static void test_ebitmap_set_and_get(void)
{
	ebitmap_t e;

	ebitmap_init(&e);

	CU_ASSERT(ebitmap_is_empty(&e));
	CU_ASSERT_TRUE(ebitmap_is_empty(&e));
	CU_ASSERT_EQUAL(ebitmap_cardinality(&e), 0);
	CU_ASSERT_EQUAL(ebitmap_highest_set_bit(&e), 0);
	CU_ASSERT_EQUAL(ebitmap_get_bit(&e, 10), 0);

	CU_ASSERT_EQUAL(ebitmap_set_bit(&e, UINT32_MAX, 1), -EINVAL);

	CU_ASSERT_EQUAL(ebitmap_set_bit(&e, 10, 0), 0);
	CU_ASSERT(ebitmap_is_empty(&e));
	CU_ASSERT_EQUAL(ebitmap_cardinality(&e), 0);
	CU_ASSERT_EQUAL(ebitmap_highest_set_bit(&e), 0);
	CU_ASSERT_EQUAL(ebitmap_get_bit(&e, 10), 0);

	CU_ASSERT_EQUAL(ebitmap_set_bit(&e, 10, 1), 0);
	CU_ASSERT_FALSE(ebitmap_is_empty(&e));
	CU_ASSERT_EQUAL(ebitmap_cardinality(&e), 1);
	CU_ASSERT_EQUAL(ebitmap_highest_set_bit(&e), 10);
	CU_ASSERT_EQUAL(ebitmap_get_bit(&e, 10), 1);

	CU_ASSERT_EQUAL(ebitmap_set_bit(&e, 100, 1), 0);
	CU_ASSERT_FALSE(ebitmap_is_empty(&e));
	CU_ASSERT_EQUAL(ebitmap_cardinality(&e), 2);
	CU_ASSERT_EQUAL(ebitmap_highest_set_bit(&e), 100);
	CU_ASSERT_EQUAL(ebitmap_get_bit(&e, 100), 1);

	CU_ASSERT_EQUAL(ebitmap_set_bit(&e, 50, 1), 0);
	CU_ASSERT_FALSE(ebitmap_is_empty(&e));
	CU_ASSERT_EQUAL(ebitmap_cardinality(&e), 3);
	CU_ASSERT_EQUAL(ebitmap_highest_set_bit(&e), 100);
	CU_ASSERT_EQUAL(ebitmap_get_bit(&e, 50), 1);

	CU_ASSERT_EQUAL(ebitmap_set_bit(&e, 1023, 1), 0);
	CU_ASSERT_FALSE(ebitmap_is_empty(&e));
	CU_ASSERT_EQUAL(ebitmap_cardinality(&e), 4);
	CU_ASSERT_EQUAL(ebitmap_highest_set_bit(&e), 1023);
	CU_ASSERT_EQUAL(ebitmap_get_bit(&e, 1023), 1);

	CU_ASSERT_EQUAL(ebitmap_set_bit(&e, 1024, 1), 0);
	CU_ASSERT_FALSE(ebitmap_is_empty(&e));
	CU_ASSERT_EQUAL(ebitmap_cardinality(&e), 5);
	CU_ASSERT_EQUAL(ebitmap_highest_set_bit(&e), 1024);
	CU_ASSERT_EQUAL(ebitmap_get_bit(&e, 1024), 1);

	CU_ASSERT_EQUAL(ebitmap_set_bit(&e, 1050, 1), 0);
	CU_ASSERT_FALSE(ebitmap_is_empty(&e));
	CU_ASSERT_EQUAL(ebitmap_cardinality(&e), 6);
	CU_ASSERT_EQUAL(ebitmap_highest_set_bit(&e), 1050);
	CU_ASSERT_EQUAL(ebitmap_get_bit(&e, 1050), 1);
    
	ebitmap_destroy(&e);
}

int ebitmap_test_init(void)
{
	srandom(time(NULL));
	return 0;
}

int ebitmap_test_cleanup(void)
{
	return 0;
}

int add_tests(void) {
	CU_pSuite suite = NULL;
	/* Add a test suite to the registry */
	suite = CU_add_suite("ebitmap", ebitmap_test_init, ebitmap_test_cleanup);
	if (NULL == suite) {
		CU_cleanup_registry();
		return CU_get_error();
	}
	
	/* Add test cases to the suite */
	if (NULL == CU_add_test(
		suite, "test of test_ebitmap_set_and_get", test_ebitmap_set_and_get)) {
			CU_cleanup_registry();
			return CU_get_error();
		}
		return 0;
	}

int main() {
	/* Initialize CUnit test registry */
	if (CUE_SUCCESS != CU_initialize_registry()) {
		return CU_get_error();
	}
	
	/* Add tests to the registry */
	if (0 != add_tests()) {
		CU_cleanup_registry();
		return CU_get_error();
	}
	
	/* Simple user interface */
	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
	CU_cleanup_registry();
	return CU_get_error();
}
EOF
'''
        self.cmd(cmdline_ebitmap_set_and_get_func)

    def test(self):
        self.cmd("gcc -c -o test_ebitmap_set_and_get.o test_ebitmap_set_and_get.c")
        self.cmd("gcc -o test_ebitmap_set_and_get test_ebitmap_set_and_get.o -l:libsepol.a -lcunit")
        code, result = self.cmd("file test_ebitmap_set_and_get")
        self.assertIn("dynamically linked", result)
        code, test_init_destroy_output = self.cmd("./test_ebitmap_set_and_get")
        self.assertIn("passed", test_init_destroy_output)
 
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf test_ebitmap_set_and_get.c test_ebitmap_set_and_get.o test_ebitmap_set_and_get")
