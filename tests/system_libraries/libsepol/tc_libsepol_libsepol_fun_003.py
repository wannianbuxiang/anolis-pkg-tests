#! /usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File:      tc_libsepol_libsepol_fun_003.py
@Time:      2024年04月01日16:06:08
@Author:    Liujiang <wb-lj931787@alibaba-inc.com>
@License:   Mulan PSL v2
@Modified:  Liujiang <wb-lj931787@alibaba-inc.com>
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libsepol_libsepol_fun_003.yaml for details
    
    :avocado: tags=P1,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "libsepol libsepol-devel libsepol-static selinux-policy selinux-policy-devel CUnit CUnit-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline_ebitmap_cmp_func ='''cat > test_ebitmap_cmp.c <<EOF
#include <CUnit/Basic.h>
#include <stdlib.h>
#include <time.h>
#include <sepol/policydb/ebitmap.h>
#include <sepol/policydb/policydb.h>

static void test_ebitmap_cmp(void)
{
	ebitmap_t e1, e2;

	ebitmap_init(&e1);
	ebitmap_init(&e2);

	CU_ASSERT(ebitmap_cmp(&e1, &e2));

	CU_ASSERT_EQUAL(ebitmap_set_bit(&e1, 10, 1), 0);
	CU_ASSERT_FALSE(ebitmap_cmp(&e1, &e2));
	CU_ASSERT_EQUAL(ebitmap_set_bit(&e2, 10, 1), 0);
	CU_ASSERT(ebitmap_cmp(&e1, &e2));

	CU_ASSERT_EQUAL(ebitmap_set_bit(&e1, 63, 1), 0);
	CU_ASSERT_FALSE(ebitmap_cmp(&e1, &e2));
	CU_ASSERT_EQUAL(ebitmap_set_bit(&e2, 63, 1), 0);
	CU_ASSERT(ebitmap_cmp(&e1, &e2));

	CU_ASSERT_EQUAL(ebitmap_set_bit(&e1, 64, 1), 0);
	CU_ASSERT_FALSE(ebitmap_cmp(&e1, &e2));
	CU_ASSERT_EQUAL(ebitmap_set_bit(&e2, 64, 1), 0);
	CU_ASSERT(ebitmap_cmp(&e1, &e2));

	CU_ASSERT_EQUAL(ebitmap_set_bit(&e1, 1022, 1), 0);
	CU_ASSERT_FALSE(ebitmap_cmp(&e1, &e2));
	CU_ASSERT_EQUAL(ebitmap_set_bit(&e2, 1022, 1), 0);
	CU_ASSERT(ebitmap_cmp(&e1, &e2));

	CU_ASSERT_EQUAL(ebitmap_set_bit(&e1, 1023, 1), 0);
	CU_ASSERT_FALSE(ebitmap_cmp(&e1, &e2));
	CU_ASSERT_EQUAL(ebitmap_set_bit(&e2, 1023, 1), 0);
	CU_ASSERT(ebitmap_cmp(&e1, &e2));

	CU_ASSERT_EQUAL(ebitmap_set_bit(&e1, 1024, 1), 0);
	CU_ASSERT_FALSE(ebitmap_cmp(&e1, &e2));
	CU_ASSERT_EQUAL(ebitmap_set_bit(&e2, 1024, 1), 0);
	CU_ASSERT(ebitmap_cmp(&e1, &e2));

	CU_ASSERT_EQUAL(ebitmap_set_bit(&e1, 1025, 1), 0);
	CU_ASSERT_FALSE(ebitmap_cmp(&e1, &e2));
	CU_ASSERT_EQUAL(ebitmap_set_bit(&e2, 1025, 1), 0);
	CU_ASSERT(ebitmap_cmp(&e1, &e2));

	CU_ASSERT_EQUAL(ebitmap_set_bit(&e1, 255, 1), 0);
	CU_ASSERT_FALSE(ebitmap_cmp(&e1, &e2));
	CU_ASSERT_EQUAL(ebitmap_set_bit(&e2, 255, 1), 0);
	CU_ASSERT(ebitmap_cmp(&e1, &e2));

	CU_ASSERT_EQUAL(ebitmap_set_bit(&e1, 256, 1), 0);
	CU_ASSERT_FALSE(ebitmap_cmp(&e1, &e2));
	CU_ASSERT_EQUAL(ebitmap_set_bit(&e2, 256, 1), 0);
	CU_ASSERT(ebitmap_cmp(&e1, &e2));

	CU_ASSERT_EQUAL(ebitmap_set_bit(&e1, 639, 1), 0);
	CU_ASSERT_FALSE(ebitmap_cmp(&e1, &e2));
	CU_ASSERT_EQUAL(ebitmap_set_bit(&e2, 639, 1), 0);
	CU_ASSERT(ebitmap_cmp(&e1, &e2));

	CU_ASSERT_EQUAL(ebitmap_set_bit(&e1, 640, 1), 0);
	CU_ASSERT_FALSE(ebitmap_cmp(&e1, &e2));
	CU_ASSERT_EQUAL(ebitmap_set_bit(&e2, 640, 1), 0);
	CU_ASSERT(ebitmap_cmp(&e1, &e2));

	CU_ASSERT_EQUAL(ebitmap_set_bit(&e1, 900, 1), 0);
	CU_ASSERT_FALSE(ebitmap_cmp(&e1, &e2));
	CU_ASSERT_EQUAL(ebitmap_set_bit(&e2, 900, 1), 0);
	CU_ASSERT(ebitmap_cmp(&e1, &e2));

	ebitmap_destroy(&e2);

	CU_ASSERT_FALSE(ebitmap_cmp(&e1, &e2));

	ebitmap_destroy(&e1);

	CU_ASSERT(ebitmap_cmp(&e1, &e2));
}

int ebitmap_test_init(void)
{
	srandom(time(NULL));
	return 0;
}

int ebitmap_test_cleanup(void)
{
	return 0;
}

int add_tests(void) {
	CU_pSuite suite = NULL;
	/* Add a test suite to the registry */
	suite = CU_add_suite("ebitmap", ebitmap_test_init, ebitmap_test_cleanup);
	if (NULL == suite) {
		CU_cleanup_registry();
		return CU_get_error();
	}
	
	/* Add test cases to the suite */
	if (NULL == CU_add_test(
		suite, "test of test_ebitmap_cmp", test_ebitmap_cmp)) {
			CU_cleanup_registry();
			return CU_get_error();
		}
		return 0;
	}

int main() {
	/* Initialize CUnit test registry */
	if (CUE_SUCCESS != CU_initialize_registry()) {
		return CU_get_error();
	}
	
	/* Add tests to the registry */
	if (0 != add_tests()) {
		CU_cleanup_registry();
		return CU_get_error();
	}
	
	/* Simple user interface */
	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
	CU_cleanup_registry();
	return CU_get_error();
}
EOF
'''
        self.cmd(cmdline_ebitmap_cmp_func)

    def test(self):
        self.cmd("gcc -c -o test_ebitmap_cmp.o test_ebitmap_cmp.c")
        self.cmd("gcc -o test_ebitmap_cmp test_ebitmap_cmp.o -l:libsepol.a -lcunit")
        code, result = self.cmd("file test_ebitmap_cmp")
        self.assertIn("dynamically linked", result)
        code, test_init_destroy_output = self.cmd("./test_ebitmap_cmp")
        self.assertIn("passed", test_init_destroy_output)
 
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf test_ebitmap_cmp.c test_ebitmap_cmp.o test_ebitmap_cmp")