#! /usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File:      tc_libsepol_libsepol_fun_011.py
@Time:      2024年04月01日16:06:08
@Author:    Liujiang <wb-lj931787@alibaba-inc.com>
@License:   Mulan PSL v2
@Modified:  Liujiang <wb-lj931787@alibaba-inc.com>
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libsepol_libsepol_fun_011.yaml for details
    
    :avocado: tags=P1,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "libsepol libsepol-devel selinux-policy libsepol-static selinux-policy-devel CUnit CUnit-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline_ebitmap_random_func ='''cat > test_ebitmap__random.c <<EOF
#include <CUnit/Basic.h>
#include <stdlib.h>
#include <time.h>
#include <sepol/policydb/ebitmap.h>
#include <sepol/policydb/policydb.h>

#define RANDOM_ROUNDS 10

static int ebitmap_init_random(ebitmap_t *e, unsigned int length, int set_chance)
{
	unsigned int i;
	int rc;

	if (set_chance <= 0 || set_chance > 100)
		return -EINVAL;

	ebitmap_init(e);

	for (i = 0; i < length; i++) {
		if ((random() % 100) < set_chance) {
			rc = ebitmap_set_bit(e, i, 1);
			if (rc)
				return rc;
		}
	}

	return 0;
}

static void test_ebitmap__random_impl(unsigned int length, int set_chance)
{
	ebitmap_t e1, e2, dst_cpy, dst_or, dst_and, dst_xor1, dst_xor2, dst_not1, dst_not2, dst_andnot;
	unsigned int i;

	CU_ASSERT_EQUAL(ebitmap_init_random(&e1, length, set_chance), 0);
	CU_ASSERT_EQUAL(ebitmap_init_random(&e2, length, set_chance), 0);

	CU_ASSERT_EQUAL(ebitmap_or(&dst_or, &e1, &e2), 0);
	for (i = 0; i < length; i++)
		CU_ASSERT_EQUAL(ebitmap_get_bit(&dst_or, i), ebitmap_get_bit(&e1, i) | ebitmap_get_bit(&e2, i));

	CU_ASSERT_EQUAL(ebitmap_and(&dst_and, &e1, &e2), 0);
	for (i = 0; i < length; i++)
		CU_ASSERT_EQUAL(ebitmap_get_bit(&dst_and, i), ebitmap_get_bit(&e1, i) & ebitmap_get_bit(&e2, i));

	CU_ASSERT_EQUAL(ebitmap_xor(&dst_xor1, &e1, &e2), 0);
	for (i = 0; i < length; i++)
		CU_ASSERT_EQUAL(ebitmap_get_bit(&dst_xor1, i), ebitmap_get_bit(&e1, i) ^ ebitmap_get_bit(&e2, i));
	CU_ASSERT_EQUAL(ebitmap_xor(&dst_xor2, &dst_xor1, &e2), 0);
	CU_ASSERT(ebitmap_cmp(&dst_xor2, &e1));

	CU_ASSERT_EQUAL(ebitmap_not(&dst_not1, &e1, length), 0);
	for (i = 0; i < length; i++)
		CU_ASSERT_EQUAL(ebitmap_get_bit(&dst_not1, i), !ebitmap_get_bit(&e1, i));
	CU_ASSERT_EQUAL(ebitmap_not(&dst_not2, &dst_not1, length), 0);
	CU_ASSERT(ebitmap_cmp(&dst_not2, &e1));

	CU_ASSERT_EQUAL(ebitmap_andnot(&dst_andnot, &e1, &e2, length), 0);
	for (i = 0; i < length; i++)
		CU_ASSERT_EQUAL(ebitmap_get_bit(&dst_andnot, i), ebitmap_get_bit(&e1, i) & !ebitmap_get_bit(&e2, i));

	ebitmap_destroy(&dst_andnot);
	ebitmap_destroy(&dst_not2);
	ebitmap_destroy(&dst_not1);
	ebitmap_destroy(&dst_xor2);
	ebitmap_destroy(&dst_xor1);
	ebitmap_destroy(&dst_and);
	ebitmap_destroy(&dst_or);
	ebitmap_destroy(&dst_cpy);
	ebitmap_destroy(&e2);
	ebitmap_destroy(&e1);
}

static void test_ebitmap__random(void)
{
	unsigned int i;

	for (i = 0; i < RANDOM_ROUNDS; i++)
		test_ebitmap__random_impl(5, 10);

	for (i = 0; i < RANDOM_ROUNDS; i++)
		test_ebitmap__random_impl(5, 90);

	for (i = 0; i < RANDOM_ROUNDS; i++)
		test_ebitmap__random_impl(1024, 50);

	for (i = 0; i < RANDOM_ROUNDS; i++)
		test_ebitmap__random_impl(8000, 5);

	for (i = 0; i < RANDOM_ROUNDS; i++)
		test_ebitmap__random_impl(8000, 95);
}

int ebitmap_test_init(void)
{
	srandom(time(NULL));
	return 0;
}

int ebitmap_test_cleanup(void)
{
	return 0;
}

int add_tests(void) {
	CU_pSuite suite = NULL;
	/* Add a test suite to the registry */
	suite = CU_add_suite("ebitmap", ebitmap_test_init, ebitmap_test_cleanup);
	if (NULL == suite) {
		CU_cleanup_registry();
		return CU_get_error();
	}
	
	/* Add test cases to the suite */
	if (NULL == CU_add_test(
		suite, "test of test_ebitmap__random", test_ebitmap__random)) {
			CU_cleanup_registry();
			return CU_get_error();
		}
		return 0;
	}

int main() {
	/* Initialize CUnit test registry */
	if (CUE_SUCCESS != CU_initialize_registry()) {
		return CU_get_error();
	}
	
	/* Add tests to the registry */
	if (0 != add_tests()) {
		CU_cleanup_registry();
		return CU_get_error();
	}
	
	/* Simple user interface */
	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
	CU_cleanup_registry();
	return CU_get_error();
}
EOF
'''
        self.cmd(cmdline_ebitmap_random_func)

    def test(self):
        self.cmd("gcc -c -o test_ebitmap__random.o test_ebitmap__random.c")
        self.cmd("gcc -o test_ebitmap__random test_ebitmap__random.o -l:libsepol.a -lcunit")
        code, result = self.cmd("file test_ebitmap__random")
        self.assertIn("dynamically linked", result)
        code, test_init_destroy_output = self.cmd("./test_ebitmap__random")
        self.assertIn("passed", test_init_destroy_output)
 
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf test_ebitmap__random.c test_ebitmap__random.o test_ebitmap__random")