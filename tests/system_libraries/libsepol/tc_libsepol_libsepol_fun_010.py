#! /usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File:      tc_libsepol_libsepol_fun_010.py
@Time:      2024年04月01日16:06:08
@Author:    Liujiang <wb-lj931787@alibaba-inc.com>
@License:   Mulan PSL v2
@Modified:  Liujiang <wb-lj931787@alibaba-inc.com>
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libsepol_libsepol_fun_010.yaml for details
    
    :avocado: tags=P1,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "libsepol libsepol-devel selinux-policy libsepol-static selinux-policy-devel CUnit CUnit-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline_ebitmap_andnot_func ='''cat > test_ebitmap_andnot.c <<EOF
#include <CUnit/Basic.h>
#include <stdlib.h>
#include <time.h>
#include <sepol/policydb/ebitmap.h>
#include <sepol/policydb/policydb.h>

static void test_ebitmap_andnot(void)
{
		ebitmap_t e1, e2, e12, e3, e4;

		ebitmap_init(&e1);
		ebitmap_init(&e2);
		ebitmap_init(&e12);
		ebitmap_init(&e3);
		ebitmap_init(&e4);

		CU_ASSERT_EQUAL(ebitmap_set_bit(&e1, 10, 1), 0);
		CU_ASSERT_EQUAL(ebitmap_set_bit(&e1, 100, 1), 0);
		CU_ASSERT_EQUAL(ebitmap_set_bit(&e1, 101, 1), 0);
		CU_ASSERT_EQUAL(ebitmap_set_bit(&e1, 430, 1), 0);
		CU_ASSERT_EQUAL(ebitmap_set_bit(&e1, 1013, 1), 0);

		CU_ASSERT_EQUAL(ebitmap_set_bit(&e2, 11, 1), 0);
		CU_ASSERT_EQUAL(ebitmap_set_bit(&e2, 101, 1), 0);
		CU_ASSERT_EQUAL(ebitmap_set_bit(&e2, 665, 1), 0);

		CU_ASSERT_EQUAL(ebitmap_set_bit(&e12, 10, 1), 0);
		CU_ASSERT_EQUAL(ebitmap_set_bit(&e12, 100, 1), 0);
		CU_ASSERT_EQUAL(ebitmap_set_bit(&e12, 430, 1), 0);
		CU_ASSERT_EQUAL(ebitmap_set_bit(&e12, 1013, 1), 0);

		CU_ASSERT_EQUAL(ebitmap_set_bit(&e3, 10, 1), 0);
		CU_ASSERT_EQUAL(ebitmap_set_bit(&e3, 11, 1), 0);
		CU_ASSERT_EQUAL(ebitmap_set_bit(&e3, 100, 1), 0);
		CU_ASSERT_EQUAL(ebitmap_set_bit(&e3, 101, 1), 0);
		CU_ASSERT_EQUAL(ebitmap_set_bit(&e3, 430, 1), 0);
		CU_ASSERT_EQUAL(ebitmap_set_bit(&e3, 665, 1), 0);
		CU_ASSERT_EQUAL(ebitmap_set_bit(&e3, 1013, 1), 0);

		{
			ebitmap_t dst;

			CU_ASSERT_EQUAL(ebitmap_andnot(&dst, &e1, &e1, 1024), 0);
			CU_ASSERT(ebitmap_cmp(&dst, &e4));

			ebitmap_destroy(&dst);
		}

		{
			ebitmap_t dst;

			CU_ASSERT_EQUAL(ebitmap_andnot(&dst, &e2, &e2, 1024), 0);
			CU_ASSERT(ebitmap_cmp(&dst, &e4));

			ebitmap_destroy(&dst);
		}

		{
			ebitmap_t dst;

			CU_ASSERT_EQUAL(ebitmap_andnot(&dst, &e1, &e2, 1024), 0);
			CU_ASSERT(ebitmap_cmp(&dst, &e12));

			ebitmap_destroy(&dst);
		}

		{
			ebitmap_t dst;

			CU_ASSERT_EQUAL(ebitmap_andnot(&dst, &e3, &e3, 1024), 0);
			CU_ASSERT(ebitmap_cmp(&dst, &e4));

			ebitmap_destroy(&dst);
		}

		{
			ebitmap_t dst;

			CU_ASSERT_EQUAL(ebitmap_andnot(&dst, &e1, &e3, 1024), 0);
			CU_ASSERT(ebitmap_cmp(&dst, &e4));

			ebitmap_destroy(&dst);
		}

		{
			ebitmap_t dst;

			CU_ASSERT_EQUAL(ebitmap_andnot(&dst, &e2, &e12, 1024), 0);
			CU_ASSERT(ebitmap_cmp(&dst, &e2));

			ebitmap_destroy(&dst);
		}

		{
			ebitmap_t dst;

			CU_ASSERT_EQUAL(ebitmap_andnot(&dst, &e4, &e4, 1024), 0);
			CU_ASSERT(ebitmap_cmp(&dst, &e4));

			ebitmap_destroy(&dst);
		}

		{
			ebitmap_t dst;

			CU_ASSERT_EQUAL(ebitmap_andnot(&dst, &e3, &e4, 1024), 0);
			CU_ASSERT(ebitmap_cmp(&dst, &e3));

			ebitmap_destroy(&dst);
		}

		ebitmap_destroy(&e4);
		ebitmap_destroy(&e3);
		ebitmap_destroy(&e12);
		ebitmap_destroy(&e2);
		ebitmap_destroy(&e1);
}

int ebitmap_test_init(void)
{
	srandom(time(NULL));
	return 0;
}

int ebitmap_test_cleanup(void)
{
	return 0;
}

int add_tests(void) {
	CU_pSuite suite = NULL;
	/* Add a test suite to the registry */
	suite = CU_add_suite("ebitmap", ebitmap_test_init, ebitmap_test_cleanup);
	if (NULL == suite) {
		CU_cleanup_registry();
		return CU_get_error();
	}
	
	/* Add test cases to the suite */
	if (NULL == CU_add_test(
		suite, "test of test_ebitmap_andnot", test_ebitmap_andnot)) {
			CU_cleanup_registry();
			return CU_get_error();
		}
		return 0;
	}

int main() {
	/* Initialize CUnit test registry */
	if (CUE_SUCCESS != CU_initialize_registry()) {
		return CU_get_error();
	}
	
	/* Add tests to the registry */
	if (0 != add_tests()) {
		CU_cleanup_registry();
		return CU_get_error();
	}
	
	/* Simple user interface */
	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
	CU_cleanup_registry();
	return CU_get_error();
}
EOF
'''
        self.cmd(cmdline_ebitmap_andnot_func)

    def test(self):
        self.cmd("gcc -c -o test_ebitmap_andnot.o test_ebitmap_andnot.c")
        self.cmd("gcc -o test_ebitmap_andnot test_ebitmap_andnot.o -l:libsepol.a -lcunit")
        code, result = self.cmd("file test_ebitmap_andnot")
        self.assertIn("dynamically linked", result)
        code, test_init_destroy_output = self.cmd("./test_ebitmap_andnot")
        self.assertIn("passed", test_init_destroy_output)
 
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf test_ebitmap_andnot.c test_ebitmap_andnot.o test_ebitmap_andnot")