#! /usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File:      tc_libsepol_libsepol_fun_002.py
@Time:      2024年04月01日16:06:08
@Author:    Liujiang <wb-lj931787@alibaba-inc.com>
@License:   Mulan PSL v2
@Modified:  Liujiang <wb-lj931787@alibaba-inc.com>
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libsepol_libsepol_fun_002.yaml for details
    
    :avocado: tags=P1,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "libsepol libsepol-devel libsepol-static selinux-policy selinux-policy-devel CUnit CUnit-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline_ebitmap_init_destroy_func ='''cat > test_ebitmap_init_destroy.c <<EOF
#include <CUnit/Basic.h>
#include <stdlib.h>
#include <time.h>
#include <sepol/policydb/ebitmap.h>
#include <sepol/policydb/policydb.h>

static void test_ebitmap_init_destroy(void)
{
	ebitmap_t e;

	/* verify idempotence */
	ebitmap_init(&e);
	ebitmap_init(&e);
	ebitmap_init(&e);

	CU_ASSERT(ebitmap_is_empty(&e));
	CU_ASSERT_PTR_NULL(ebitmap_startnode(&e));

	/* verify idempotence */
	ebitmap_destroy(&e);
	ebitmap_destroy(&e);
	ebitmap_destroy(&e);

	CU_ASSERT(ebitmap_is_empty(&e));
	CU_ASSERT_PTR_NULL(ebitmap_startnode(&e));
}

int ebitmap_test_init(void)
{
	srandom(time(NULL));
	return 0;
}

int ebitmap_test_cleanup(void)
{
	return 0;
}

int add_tests(void) {
	CU_pSuite suite = NULL;
	/* Add a test suite to the registry */
	suite = CU_add_suite("ebitmap", ebitmap_test_init, ebitmap_test_cleanup);
	if (NULL == suite) {
		CU_cleanup_registry();
		return CU_get_error();
	}
	
	/* Add test cases to the suite */
	if (NULL == CU_add_test(
		suite, "test of test_ebitmap_init_destroy", test_ebitmap_init_destroy)) {
			CU_cleanup_registry();
			return CU_get_error();
		}
		return 0;
	}

int main() {
	/* Initialize CUnit test registry */
	if (CUE_SUCCESS != CU_initialize_registry()) {
		return CU_get_error();
	}
	
	/* Add tests to the registry */
	if (0 != add_tests()) {
		CU_cleanup_registry();
		return CU_get_error();
	}
	
	/* Simple user interface */
	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
	CU_cleanup_registry();
	return CU_get_error();
}
EOF
'''
        self.cmd(cmdline_ebitmap_init_destroy_func)

    def test(self):
        self.cmd("gcc -c -o test_ebitmap_init_destroy.o test_ebitmap_init_destroy.c")
        self.cmd("gcc -o test_ebitmap_init_destroy test_ebitmap_init_destroy.o -l:libsepol.a -lcunit")
        code, result = self.cmd("file test_ebitmap_init_destroy")
        self.assertIn("dynamically linked", result)
        code, test_init_destroy_output = self.cmd("./test_ebitmap_init_destroy")
        self.assertIn("passed", test_init_destroy_output)
 
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf test_ebitmap_init_destroy.c test_ebitmap_init_destroy.o test_ebitmap_init_destroy")