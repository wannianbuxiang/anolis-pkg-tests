#! /usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File:      tc_libsepol_libsepol_fun_012.py
@Time:      2024年04月01日16:06:08
@Author:    Liujiang <wb-lj931787@alibaba-inc.com>
@License:   Mulan PSL v2
@Modified:  Liujiang <wb-lj931787@alibaba-inc.com>
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libsepol_libsepol_fun_012.yaml for details
    
    :avocado: tags=P1,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "libsepol libsepol-devel libsepol-static selinux-policy selinux-policy-devel CUnit CUnit-devel setools-console"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline_Generate_policy_source_file = """cat > test_module.te <<EOF
module test_module 1.0;

require {
    type user_t;
    type unlabeled_t;
    class file { read write };
}

# 定义一个新的类型
type my_new_type_t;

# 创建一个新的文件类型
type my_new_file_type_t;

# 定义一条允许规则
allow user_t my_new_file_type_t:file { read write };
EOF"""

        cmdline_cond_expr_equal_func ='''cat > test_cond_expr_equal.c <<EOF
#include <sepol/policydb/policydb.h>
#include <sepol/policydb/link.h>
#include <sepol/policydb/expand.h>
#include <sepol/policydb/conditional.h>
#include <sepol/policydb/avrule_block.h>
#include <CUnit/Basic.h>

static policydb_t basemod;
static policydb_t base_expanded;

static void test_cond_expr_equal(void)
{
	cond_node_t *a, *b;

	a = base_expanded.cond_list;
	while (a) {
		b = base_expanded.cond_list;
		while (b) {
			if (a == b) {
				CU_ASSERT(cond_expr_equal(a, b));
			} else {
				CU_ASSERT(cond_expr_equal(a, b) == 0);
			}
			b = b->next;
		}
		a = a->next;
	}
}

int cond_test_init(void)
{
	const char *policy_file = "/etc/selinux/targeted/policy/policy.test";
    sepol_handle_t *handle = sepol_handle_create();
    sepol_policydb_t *policydb = NULL;
    sepol_policy_file_t *pf = NULL;

    if (sepol_policydb_create(&policydb) < 0) {
        fprintf(stderr, "Cannot create policydb structure.");
        goto cleanup;
    }
    
    if (sepol_policy_file_create(&pf) < 0) {
        fprintf(stderr, "Cannot create policy file structure.");
        goto cleanup;
    }
    sepol_policy_file_set_fp(pf, fopen(policy_file, "r"));
    if (sepol_policydb_read(policydb, pf) < 0) {
        fprintf(stderr, "Cannot load policy.");
        goto cleanup;
    }
    printf("Policy loaded successfully.");

cleanup:
    if (policydb) {
        sepol_policydb_free(policydb);
    }
    if (pf) {
        sepol_policy_file_free(pf);
    }
    if (handle) {
        sepol_handle_destroy(handle);
    }
    return 0;
}

int cond_test_cleanup(void)
{
	policydb_destroy(&basemod);
	policydb_destroy(&base_expanded);

	return 0;
}

int add_tests(void) {
	CU_pSuite suite = NULL;
	/* Add a test suite to the registry */
	suite = CU_add_suite("ebitmap", cond_test_init, cond_test_cleanup);
	if (NULL == suite) {
		CU_cleanup_registry();
		return CU_get_error();
	}
	
	/* Add test cases to the suite */
	if (NULL == CU_add_test(
		suite, "test of test_cond_expr_equal", test_cond_expr_equal)) {
			CU_cleanup_registry();
			return CU_get_error();
		}
		return 0;
	}

int main() {
	/* Initialize CUnit test registry */
	if (CUE_SUCCESS != CU_initialize_registry()) {
		return CU_get_error();
	}
	
	/* Add tests to the registry */
	if (0 != add_tests()) {
		CU_cleanup_registry();
		return CU_get_error();
	}
	
	/* Simple user interface */
	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
	CU_cleanup_registry();
	return CU_get_error();
}
EOF
'''
        self.cmd(cmdline_cond_expr_equal_func)
        self.cmd(cmdline_Generate_policy_source_file)

    def test(self):
        ## Generate policy file
        self.cmd("checkmodule -M -m -o test_module.mod test_module.te")
        self.cmd("ls -l test_module.mod")
        self.cmd("semodule_package -o test_module.pp -m test_module.mod")
        self.cmd("ls -l test_module.pp")
        # copy source file
        # self.cmd("semodule -e backup -o test_module.pp")
        self.cmd("cd /etc/selinux/targeted/policy/;tar czvf selinux_modules_backup.tar.gz ./*")
		# Compile and load policies
        self.cmd("semodule -i test_module.pp")
        # Check policy module and rules
        self.cmd("semodule -l |grep test_module")
        self.cmd("seinfo -t |grep my_new_file_type_t")
        
		# generate test policy file
        self.cmd("cp /etc/selinux/targeted/policy/policy.[0-9][0-9] /etc/selinux/targeted/policy/policy.test")
        # test cond expr
        self.cmd("gcc -c -o test_cond_expr_equal.o test_cond_expr_equal.c")
        self.cmd("gcc -o test_cond_expr_equal test_cond_expr_equal.o -l:libsepol.a -lcunit")
        code, result = self.cmd("file test_cond_expr_equal")
        self.assertIn("dynamically linked", result)
        code, test_init_destroy_output = self.cmd("./test_cond_expr_equal")
        self.assertIn("successfully", test_init_destroy_output, 
                      msg="Policy loading failed")
        self.assertIn("passed", test_init_destroy_output)
 
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)

        ## remove policy
        self.cmd("semodule -r test_module")
        ## Rollback policy
        self.cmd("cd /etc/selinux/targeted/policy/;"+
                 "tar xf selinux_modules_backup.tar.gz; rm -f selinux_modules_backup.tar.gz")
        self.cmd("rm -rf test_cond_expr_equal.c test_cond_expr_equal.o "+ 
                 "test_cond_expr_equal /etc/selinux/targeted/policy/policy.test")