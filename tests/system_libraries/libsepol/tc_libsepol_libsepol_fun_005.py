#! /usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File:      tc_libsepol_libsepol_fun_005.py
@Time:      2024年04月01日16:06:08
@Author:    Liujiang <wb-lj931787@alibaba-inc.com>
@License:   Mulan PSL v2
@Modified:  Liujiang <wb-lj931787@alibaba-inc.com>
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libsepol_libsepol_fun_005.yaml for details
    
    :avocado: tags=P1,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "libsepol libsepol-devel selinux-policy selinux-policy selinux-policy-devel CUnit CUnit-devel libsepol-static"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline_ebitmap_init_range_func ='''cat > test_ebitmap_init_range.c <<EOF
#include <CUnit/Basic.h>
#include <stdlib.h>
#include <time.h>
#include <sepol/policydb/ebitmap.h>
#include <sepol/policydb/policydb.h>

static void test_ebitmap_init_range(void)
{
	ebitmap_t e1, e2, e3, e4, e5, e6;

	CU_ASSERT_EQUAL(ebitmap_init_range(&e1, 0, 0), 0);
	CU_ASSERT_EQUAL(ebitmap_highest_set_bit(&e1), 0);
	CU_ASSERT_EQUAL(ebitmap_cardinality(&e1), 1);

	CU_ASSERT_EQUAL(ebitmap_init_range(&e2, 0, 5), 0);
	CU_ASSERT_EQUAL(ebitmap_highest_set_bit(&e2), 5);
	CU_ASSERT_EQUAL(ebitmap_cardinality(&e2), 6);

	CU_ASSERT_EQUAL(ebitmap_init_range(&e3, 20, 100), 0);
	CU_ASSERT_EQUAL(ebitmap_highest_set_bit(&e3), 100);
	CU_ASSERT_EQUAL(ebitmap_cardinality(&e3), 81);

	CU_ASSERT_EQUAL(ebitmap_init_range(&e4, 100, 400), 0);
	CU_ASSERT_EQUAL(ebitmap_highest_set_bit(&e4), 400);
	CU_ASSERT_EQUAL(ebitmap_cardinality(&e4), 301);

	CU_ASSERT_EQUAL(ebitmap_init_range(&e5, 10, 5), -EINVAL);
	CU_ASSERT_EQUAL(ebitmap_init_range(&e6, 0, UINT32_MAX), -EOVERFLOW);

	ebitmap_destroy(&e6);
	ebitmap_destroy(&e5);
	ebitmap_destroy(&e4);
	ebitmap_destroy(&e3);
	ebitmap_destroy(&e2);
	ebitmap_destroy(&e1);
}

int ebitmap_test_init(void)
{
	srandom(time(NULL));
	return 0;
}

int ebitmap_test_cleanup(void)
{
	return 0;
}

int add_tests(void) {
	CU_pSuite suite = NULL;
	/* Add a test suite to the registry */
	suite = CU_add_suite("ebitmap", ebitmap_test_init, ebitmap_test_cleanup);
	if (NULL == suite) {
		CU_cleanup_registry();
		return CU_get_error();
	}
	
	/* Add test cases to the suite */
	if (NULL == CU_add_test(
		suite, "test of test_ebitmap_init_range", test_ebitmap_init_range)) {
			CU_cleanup_registry();
			return CU_get_error();
		}
		return 0;
	}

int main() {
	/* Initialize CUnit test registry */
	if (CUE_SUCCESS != CU_initialize_registry()) {
		return CU_get_error();
	}
	
	/* Add tests to the registry */
	if (0 != add_tests()) {
		CU_cleanup_registry();
		return CU_get_error();
	}
	
	/* Simple user interface */
	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
	CU_cleanup_registry();
	return CU_get_error();
}
EOF
'''
        self.cmd(cmdline_ebitmap_init_range_func)

    def test(self):
        self.cmd("gcc -c -o test_ebitmap_init_range.o test_ebitmap_init_range.c")
        self.cmd("gcc -o test_ebitmap_init_range test_ebitmap_init_range.o -l:libsepol.a -lcunit")
        code, result = self.cmd("file test_ebitmap_init_range")
        self.assertIn("dynamically linked", result)
        code, test_init_destroy_output = self.cmd("./test_ebitmap_init_range")
        self.assertIn("passed", test_init_destroy_output)
 
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf test_ebitmap_init_range.c test_ebitmap_init_range.o test_ebitmap_init_range")
