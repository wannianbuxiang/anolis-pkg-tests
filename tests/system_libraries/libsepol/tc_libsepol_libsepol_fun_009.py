#! /usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File:      tc_libsepol_libsepol_fun_009.py
@Time:      2024年04月01日16:06:08
@Author:    Liujiang <wb-lj931787@alibaba-inc.com>
@License:   Mulan PSL v2
@Modified:  Liujiang <wb-lj931787@alibaba-inc.com>
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_libsepol_libsepol_fun_009.yaml for details
    
    :avocado: tags=P1,noarch,local,fix
    """

    PARAM_DIC = {"pkg_name": "libsepol libsepol-devel libsepol-static selinux-policy selinux-policy-devel CUnit CUnit-devel"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline_ebitmap_not_func ='''cat > test_ebitmap_not.c <<EOF
#include <CUnit/Basic.h>
#include <stdlib.h>
#include <time.h>
#include <sepol/policydb/ebitmap.h>
#include <sepol/policydb/policydb.h>

static void test_ebitmap_not(void)
{
		ebitmap_t e1, e2, e3, e4;

		ebitmap_init(&e1);
		ebitmap_init(&e2);
		ebitmap_init(&e3);
		ebitmap_init(&e4);

		CU_ASSERT_EQUAL(ebitmap_set_bit(&e1, 0, 1), 0);
		CU_ASSERT_EQUAL(ebitmap_set_bit(&e1, 1, 1), 0);
		CU_ASSERT_EQUAL(ebitmap_set_bit(&e1, 5, 1), 0);
		CU_ASSERT_EQUAL(ebitmap_set_bit(&e1, 10, 1), 0);

		CU_ASSERT_EQUAL(ebitmap_set_bit(&e2, 2, 1), 0);
		CU_ASSERT_EQUAL(ebitmap_set_bit(&e2, 3, 1), 0);
		CU_ASSERT_EQUAL(ebitmap_set_bit(&e2, 4, 1), 0);
		CU_ASSERT_EQUAL(ebitmap_set_bit(&e2, 6, 1), 0);
		CU_ASSERT_EQUAL(ebitmap_set_bit(&e2, 7, 1), 0);
		CU_ASSERT_EQUAL(ebitmap_set_bit(&e2, 8, 1), 0);
		CU_ASSERT_EQUAL(ebitmap_set_bit(&e2, 9, 1), 0);

		{
			ebitmap_t dst1, dst2;

			CU_ASSERT_EQUAL(ebitmap_not(&dst1, &e3, 10), 0);
			CU_ASSERT_EQUAL(ebitmap_highest_set_bit(&dst1), 9);
			CU_ASSERT_EQUAL(ebitmap_cardinality(&dst1), 10);

			CU_ASSERT_EQUAL(ebitmap_not(&dst2, &dst1, 10), 0);
			CU_ASSERT(ebitmap_cmp(&dst2, &e3));

			ebitmap_destroy(&dst2);
			ebitmap_destroy(&dst1);
		}

		{
			ebitmap_t dst1, dst2;

			CU_ASSERT_EQUAL(ebitmap_not(&dst1, &e1, 11), 0);
			CU_ASSERT(ebitmap_cmp(&dst1, &e2));
			CU_ASSERT_EQUAL(ebitmap_not(&dst2, &dst1, 11), 0);
			CU_ASSERT(ebitmap_cmp(&dst2, &e1));

			ebitmap_destroy(&dst2);
			ebitmap_destroy(&dst1);
		}

		{
			ebitmap_t dst;

			CU_ASSERT_EQUAL(ebitmap_not(&dst, &e1, 8), 0);
			CU_ASSERT_EQUAL(ebitmap_highest_set_bit(&dst), 7);
			CU_ASSERT_EQUAL(ebitmap_cardinality(&dst), 5);

			ebitmap_destroy(&dst);
		}

		{
			ebitmap_t dst;

			CU_ASSERT_EQUAL(ebitmap_not(&dst, &e1, 12), 0);
			CU_ASSERT_EQUAL(ebitmap_highest_set_bit(&dst), 11);
			CU_ASSERT_EQUAL(ebitmap_cardinality(&dst), 8);

			ebitmap_destroy(&dst);
		}

		ebitmap_destroy(&e3);
		ebitmap_destroy(&e2);
		ebitmap_destroy(&e1);
}

int ebitmap_test_init(void)
{
	srandom(time(NULL));
	return 0;
}

int ebitmap_test_cleanup(void)
{
	return 0;
}

int add_tests(void) {
	CU_pSuite suite = NULL;
	/* Add a test suite to the registry */
	suite = CU_add_suite("ebitmap", ebitmap_test_init, ebitmap_test_cleanup);
	if (NULL == suite) {
		CU_cleanup_registry();
		return CU_get_error();
	}
	
	/* Add test cases to the suite */
	if (NULL == CU_add_test(
		suite, "test of test_ebitmap_not", test_ebitmap_not)) {
			CU_cleanup_registry();
			return CU_get_error();
		}
		return 0;
	}

int main() {
	/* Initialize CUnit test registry */
	if (CUE_SUCCESS != CU_initialize_registry()) {
		return CU_get_error();
	}
	
	/* Add tests to the registry */
	if (0 != add_tests()) {
		CU_cleanup_registry();
		return CU_get_error();
	}
	
	/* Simple user interface */
	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
	CU_cleanup_registry();
	return CU_get_error();
}
EOF
'''
        self.cmd(cmdline_ebitmap_not_func)

    def test(self):
        self.cmd("gcc -c -o test_ebitmap_not.o test_ebitmap_not.c")
        self.cmd("gcc -o test_ebitmap_not test_ebitmap_not.o -l:libsepol.a -lcunit")
        code, result = self.cmd("file test_ebitmap_not")
        self.assertIn("dynamically linked", result)
        code, test_init_destroy_output = self.cmd("./test_ebitmap_not")
        self.assertIn("passed", test_init_destroy_output)
 
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf test_ebitmap_not.c test_ebitmap_not.o test_ebitmap_not")