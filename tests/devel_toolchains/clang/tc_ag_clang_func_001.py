#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
#/root/anolis-pkg-tests/tests/devel_toolchains/clang/ 
"""
@File:      tc_ag_clang_func_001.py
@Time:      2022/8/31 10:06:45
@Author:    liuyaqing
@Version:   1.0
@Contact:   lyq01395646@alibaba-inc.com
@License:   Mulan PSL v2
"""
from common.basetest import RemoteTest
from common.os_env import OSType
from common.resource import resource

class ClangTest(RemoteTest):
    """
    See tc_ag_clang_func_001.yaml for details
    :avocado: tags=fix,P1,noarch,remote,baseos_container_default,vhd_img
    """
    """
    Annolis 7.9 does not support clang
    https://bugzilla.openanolis.cn/show_bug.cgi?id=873
    """

    PARAM_DIC = {"pkg_name": "git clang"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        if self.image.ostype == OSType.ANOLIS and self.image.version.startswith('7.9'):
            self.skip('Anolis 7.9 does not support clang')
    def test_clang_compile(self):
        if self.container_id:
            self.skip_non_root_test()
            self.setup_rpm_install("git clang", flag=1)
        self.cmd('rm -rf anolis-sys-tests-res',container_flag=1)
        resource.get_resource(self)
        self.cmd('clang anolis-sys-tests-res/res/hello.c -o hello2',container_flag=1)
        ret_c,output1 = self.cmd('ls | grep hello2',container_flag=1)
        self.assertEqual(output1,"hello2","clang compile failed!")
        self.cmd('chmod +x hello2',container_flag=1)
        ret_c,output2 = self.cmd('./hello2',container_flag=1)
        self.assertEqual(output2,"Hello World!","clang output is not true!")
        self.cmd('rm -rf anolis-sys-tests-res hello2',container_flag=1)
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)