# -*- encoding: utf-8 -*-

"""
@File:      tc_gcc-c++_c++_static_compilation.py
@Time:      2023/3/15
@Author:    sunqingwei
@Version:   1.0
@Contact:   sunqingwei@uniontech.com
@License:   Mulan PSL v2
@Modify:    sunqingwei

"""
from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_gcc-c++_c++_static_compilation.yaml for details

    :avocado: tags=P2,noarch,local
    """
    PARAM_DIC = {"pkg_name": "gcc-c++ glibc-static libstdc++-static"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd('mkdir /tmp/test /tmp/person')
        cmd1 = '''cat > /tmp/test/main.cpp <<-EOF
#include<iostream>
#include "person.h"
using namespace std;

int main(int argc,char* argv[])
{
person p;
cout<<"test g++"<<endl;
return 0;
}
EOF
'''
        cmd2 = '''cat > /tmp/person/person.h <<-EOF
class person
{
public:
person();
};
EOF
'''
        cmd3 = '''cat > /tmp/person/person.cpp <<-EOF
#include "person.h"
#include "stdio.h"

person::person()
{
printf("create person");
}
EOF
'''
        self.cmd(cmd1)
        self.cmd(cmd2)
        self.cmd(cmd3)

    def test(self):
        self.cmd("cd /tmp/person;g++ -c person.cpp")
        code, result = self.cmd("ls /tmp/person | grep 'person.o'")
        self.assertEqual(0, code)
        self.cmd("cd /tmp/test;g++ main.cpp ../person/person.o -I ../person/ -o main")
        code, result = self.cmd('/tmp/test/main')
        self.assertIn('create persontest g++', result)
        code, result = self.cmd('ldd /tmp/test/main')
        self.assertEqual(0, code)
        self.cmd("cd /tmp/test;g++ main.cpp ../person/person.o -o main_static -I ../person/ -static")
        code, result = self.cmd("ls /tmp/test | grep 'main_static'")
        self.assertEqual(0, code)
        self.cmd('LANG="en_US.UTF-8";ldd /tmp/test/main_static > /tmp/test/log.log 2>&1', ignore_status=True)
        self.assertIn("not a dynamic executable", self.cmd('cat /tmp/test/log.log'))

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/test /tmp/person")
