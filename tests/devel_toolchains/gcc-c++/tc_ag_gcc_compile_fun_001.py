#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_ag_gcc_compile_fun_001.py
@Time:      2022/8/31 10:06:45
@Author:    yangshunfeng.ysf
@Version:   1.0
@Contact:   lyq01395646@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liuyaqing
"""
from common.basetest import RemoteTest
from common.resource import resource

class GccTest(RemoteTest):
    """
    See tc_ag_gcc_compile_fun_001.yaml for details
    :avocado: tags=fix,P1,noarch,remote,baseos_container_default,vhd_img
    """
    PARAM_DIC = {"pkg_name": "git gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    def test_gcc_compile(self):
        if self.container_id:
            self.skip_non_root_test()
            # self.cmd('yum install  git gcc -y',container_flag=1)
            self.setup_rpm_install("git gcc", flag=1)
        resource.get_resource(self)
        self.cmd('gcc anolis-sys-tests-res/res/hello.c -o hello1',container_flag=1)
        ret_c,output1 = self.cmd('ls | grep hello1',container_flag=1)
        self.assertEqual(output1,"hello1","gcc compile failed!")
        self.cmd('chmod +x hello1',container_flag=1)
        ret_c,output2 = self.cmd('./hello1',container_flag=1)
        self.assertEqual(output2, "Hello World!","gcc output is not true!")
        self.cmd('rm -rf anolis-sys-tests-res hello1',container_flag=1)
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)