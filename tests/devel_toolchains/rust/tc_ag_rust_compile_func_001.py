#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_ag_rust_compile_func_001.py
@Time:      2022/8/31 10:06:45
@Author:    yangshunfeng.ysf
@Version:   1.0
@Contact:   lyq01395646@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liuyaqing
"""
from common.basetest import RemoteTest
from common.resource import resource
class RustTest(RemoteTest):
    """
    See tc_ag_rust_compile_func_001.yaml for details
    :avocado: tags=fix,p1,noarch,remote,baseos_container_default,vhd_img
    """
    
    PARAM_DIC = {"pkg_name": "git rust"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    def test_rust_compile(self):
        if self.container_id:
            self.skip_non_root_test()
            _, os_type = self.cmd('cat /etc/os-release | grep -w NAME= | awk -F "\\\"" "{print \$2}"', container_flag=1)
            if os_type == "Ubuntu":
                self.setup_rpm_install("git rustc", flag=1)
            # self.cmd("yum install git rust -y ",container_flag=1)
            else:
                self.setup_rpm_install("git rust", flag=1)
        resource.get_resource(self)
        self.cmd('rustc anolis-sys-tests-res/res/helloworld.rs',container_flag=1)
        ret_c,output1 = self.cmd('ls | grep helloworld',container_flag=1)
        self.assertEqual(output1.split('\n')[0],"helloworld","rust compile failed!")
        self.cmd('chmod +x helloworld',container_flag=1)
        ret_c,output2 = self.cmd('./helloworld',container_flag=1)
        self.assertEqual(output2,"Hello World!","rustc output is not true!")
        self.cmd('rm -rf anolis-sys-tests-res helloworld*',container_flag=1)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)