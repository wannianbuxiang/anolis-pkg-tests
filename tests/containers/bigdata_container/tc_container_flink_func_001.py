#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_container_flink_func_001.py
@Time:      2023年11月9日16:38:08
@Author:    liangjian
@License:   Mulan PSL v2
@Modified:  liangjian
"""
from common.basetest import LocalTest

import os,time
class FlinkTest(LocalTest):
    """
    :avocado: tags=P1,noarch,local,flink_big_data_container
    """
    def test(self):
        self.cmd("docker network create flink-network")
        jobmanager_cmd = "docker run --rm --name=jobmanager --network flink-network --publish 8081:8081 --env \
                FLINK_PROPERTIES='jobmanager.rpc.address: jobmanager' %s jobmanager" % self.version
        os.system(jobmanager_cmd + " > output.txt &")
        taskmanager_cmd = "docker run --rm --name=taskmanager --network flink-network  --env \
                FLINK_PROPERTIES='jobmanager.rpc.address: jobmanager' %s taskmanager" % self.version
        os.system(taskmanager_cmd + " > output1.txt &")
        time.sleep(5)
        run_cmd ="timeout -s SIGINT 8 docker run --rm --network=flink-network %s ./bin/flink run -m jobmanager:8081 \
                ./examples/streaming/TopSpeedWindowing.jar" % self.version
        os.system(run_cmd + " > output2.txt &")
        time.sleep(12)
        self.cmd("cat output.txt |grep RUNNING")
    
    def tearDown(self):
        self.cmd("docker stop taskmanager")
        self.cmd("docker stop jobmanager")
        self.cmd("docker network rm flink-network")
        self.cmd("rm -rf outpu*.txt")
        super().tearDown()