#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_container_spark_func_001.py
@Time:      2023年11月9日16:38:33
@Author:    liangjian
@License:   Mulan PSL v2
@Modified:  liangjian
"""
from common.basetest import LocalTest

class SparkTest(LocalTest):
    """
    :avocado: tags=P1,noarch,local,spark_big_data_container
    """
    def test(self):
        _, ret_o = self.cmd("bash -c 'spark-submit --class org.apache.spark.examples.SparkPi $SPARK_HOME/examples/jars/spark-examples*.jar'",container_flag=1)
        self.assertTrue("Pi is roughly 3.1" in ret_o, "check output error.")