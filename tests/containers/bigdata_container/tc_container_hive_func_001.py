#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_container_hive_func_001.py
@Time:      2023年11月9日16:38:22
@Author:    liangjian
@License:   Mulan PSL v2
@Modified:  liangjian
"""
from common.basetest import LocalTest

import time,os
class HiveTest(LocalTest):
    """
    :avocado: tags=P1,noarch,local,hive_big_data_container
    """
    def test(self):
        self.cmd("docker network create hive-network")
        ret_c,imageid = self.cmd("docker run -d --rm --env SERVICE_NAME=hiveserver2 --name hiveserver2 --network hive-network %s " % self.version)
        time.sleep(20)

        os.system("docker exec -it hiveserver2 beeline -u 'jdbc:hive2://hiveserver2:10000/' -e 'SHOW DATABASES;' > output.txt")
        time.sleep(10)
        self.cmd("cat output.txt|grep default")
    
    def tearDown(self):
        self.cmd("docker stop hiveserver2")
        self.cmd("docker network rm hive-network")
        self.cmd("rm -rf output.txt")
        super().tearDown()
        