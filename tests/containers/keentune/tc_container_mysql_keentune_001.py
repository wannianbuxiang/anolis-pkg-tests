#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_container_mysql_keentune_001.py
@Time:      2023/3/3 11:06:45
@Author:    jiaominchao
@Version:   1.0
@License:   Mulan PSL v2
"""
from common.basetest import LocalTest

class ContainMysqlTest(LocalTest):
    """
    :avocado: tags=P1,noarch,local,keentune
    """
    def test_mysql_status(self):
        self.cmd('/bin/bash mysql_keentune.sh',container_flag=1)
        ret_c,output = self.cmd('systemctl status mysqld',container_flag=1)
        self.assertTrue('running' in output, "The status of the application container is abnormal")

