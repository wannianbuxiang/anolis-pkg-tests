#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_container_redis_keentune_001.py
@Time:      2023/3/3 11:06:45
@Author:    jiaominchao
@Version:   1.0
@License:   Mulan PSL v2
"""
from common.basetest import LocalTest

class ContainRedisTest(LocalTest):
    """
    :avocado: tags=P1,noarch,local,keentune
    """
    def test_redis_status(self):
        self.cmd('/bin/bash redis_keentune.sh',container_flag=1)
        ret_c,output = self.cmd('ps -ef|grep redis| grep -v grep',container_flag=1)
        self.assertTrue('redis-server' in output, "The status of the application container is abnormal")

