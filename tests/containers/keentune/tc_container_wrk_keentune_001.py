#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_container_wrk_keentune_001.py
@Time:      2023/3/3 11:06:45
@Author:    jiaominchao
@Version:   1.0
@License:   Mulan PSL v2
"""
from common.basetest import LocalTest

class ContainWrkTest(LocalTest):
    """
    :avocado: tags=P1,noarch,local,keentune
    """
    def test_wrk_status(self):
        ret_c,output = self.cmd('whereis -b wrk',container_flag=1)
        self.assertTrue('/usr/bin/wrk' in output, "The status of the application container is abnormal")

