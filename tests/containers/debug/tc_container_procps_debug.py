#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_container_procps_debug.py
@Time:      2023/07/4 11:12:50
@Modify:    liangjian
"""
from common.basetest import LocalTest

import re
class ContainerDebugTest(LocalTest):
    """
    :avocado: tags=debug_23_baseos_container
    """

    def setUp(self):
        super().setUp()
        _,ret_o = self.cmd('cat /etc/os-release',container_flag=1)
        if "Ubuntu" in ret_o:
            self.skip("Ubuntu container not support.")
        command = self.container_engine+' exec -d '+self.container_id+' '+'ping localhost'
        _, output = self.cmd(command)
        command = self.container_engine+' exec -d '+self.container_id+' '+'sleep 1000'
        _, output = self.cmd(command)

    def test(self):
        _, output = self.cmd("top -h",container_flag=1)
        self.assertTrue("top" in output, 'top cmd output unknown')        
        _, nums = self.cmd("ps -ef |grep 'sleep' |awk '{print $2}'",container_flag=1)
        _, output = self.cmd("free",container_flag=1)
        self.assertTrue("used" in output, 'free cmd output unknown')
        _, output = self.cmd("pgrep -l ping",container_flag=1)
        _, output = self.cmd("pidof  ping",container_flag=1)
        _, output = self.cmd("pmap %s" % nums,container_flag=1)
        self.assertTrue("sleep" in output, 'system cmd output unknown')
        _, output = self.cmd("ps aux",container_flag=1)
        self.assertTrue("ps aux" in output, 'ps cmd output unknown')

        _, output = self.cmd("uptime",container_flag=1)
        self.assertTrue("load average" in output, 'uptime cmd output unknown')
        _, output = self.cmd("vmstat",container_flag=1)
        self.assertTrue("memory" in output, 'vmstat cmd output unknown')
        _, output = self.cmd("w",container_flag=1)
        self.assertTrue("load average" in output, 'w cmd output unknown')
        _, output = self.cmd("watch -h",container_flag=1)
        _, output = self.cmd("sysctl -a",container_flag=1)
        _, output = self.cmd("netstat -anp",container_flag=1)
        _, output = self.cmd("arp -a",container_flag=1)
        _, output = self.cmd("curl www.baidu.com",container_flag=1)
        self.assertTrue("STATUS OK" in output, 'curl cmd output unknown')
        _, output = self.cmd("ifconfig",container_flag=1)
        self.assertTrue("inet" in output, 'ifconfig cmd output unknown')
        _, output = self.cmd("ipmaddr",container_flag=1)
        self.assertTrue("inet" in output, 'ipmaddr cmd output unknown')
        _, output = self.cmd("route",container_flag=1)
        self.assertTrue("Kernel IP routing table" in output, 'route cmd output unknown')
        _, output = self.cmd("strace ls 2>&1",container_flag=1)
        self.assertTrue("execve" in output, 'strace cmd output unknown')
        _, output = self.cmd("tcpdump -c 2 -i lo 2>&1",container_flag=1)
        self.assertTrue("packets captured" in output, 'tcpdump cmd output unknown')
        
        _, output = self.cmd("gstack %s" % nums,container_flag=1)
        self.assertTrue("nanosleep" in output, 'gstack cmd output unknown')
        _, output = self.cmd("sysak ossre_client",container_flag=1)
        self.assertTrue("Total" in output, 'sysak cmd output unknown')

    def tearDown(self):
        super().tearDown()
