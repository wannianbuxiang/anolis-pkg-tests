# -*- encoding: utf-8 -*-

"""
@File:      tc_sample_remote.py
@Time:      2022/05/12 16:25:19
@Author:    your_name
@Version:   1.0
@Contact:   example@email.com
@License:   Mulan PSL v2
"""

from common.basetest import RemoteTest


class Test(RemoteTest):
    """
    Sample testcase running on remote host

    :avocado: tags=P0,noarch,remote,other_custom_tag
    """

    def test(self):
        self.local.cmd('uptime')
        uptime_before = self.remote.cmd('awk "{print \$2}" /proc/uptime')
        self.remote.cmd('reboot')
        self.wait_ssh_connect()
        uptime_after = self.remote.cmd('awk "{print \$2}" /proc/uptime')
        self.assertTrue(float(uptime_after) < float(uptime_before),
                        'uptime should be less after reboot')
