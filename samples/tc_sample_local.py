# -*- encoding: utf-8 -*-

"""
@File:      tc_sample_local.py
@Time:      2022/05/12 16:22:50
@Author:    your_name
@Version:   1.0
@Contact:   example@email.com
@License:   Mulan PSL v2
"""

from common.basetest import LocalTest


class Test(LocalTest):
    """
    Sample testcase running on localhost

    :avocado: tags=P0,noarch,local,other_custom_tag
    """

    def test(self):
        self.log.info('this is an info log in tc_sample_local test')
        self.log.debug('this is a debug log in tc_sample_local test')
        output = self.cmd('ls | wc -l')
        self.assertTrue(int(output) > 0,
                        'ls command should return more than one line')
        output = self.cmd('uname -a | awk "{print \$2}"')
        self.assertTrue(output is not None, 'output should not be None')
        output = self.cmd('cat nonexistent_file', ignore_status=True)
        self.assertTrue('No such file' in output,
                        'output should be stderr log')
